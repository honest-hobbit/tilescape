﻿using System.Diagnostics;
using PicaVoxel;
using Tilescape.Engine;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;
using UnityEngine;

public class VolumeFactory : MonoBehaviour
{
	[SerializeField]
	private GameObject voxelVolumePrefab = null;

	[SerializeField]
	private Material material = null;

	public GameObject Create(Index3D volumeSize, Options options)
	{
		return this.Create(volumeSize, volumeSize, options);
	}

	public GameObject Create(Index3D volumeSize, Index3D chunkSize, Options options)
	{
		this.SharedContracts(volumeSize, chunkSize, options);

		var gameObject = Instantiate(voxelVolumePrefab);
		var volume = this.CreateVolume(gameObject, volumeSize, chunkSize, options);

		var voxels = volume.Frames[0].Voxels;
		foreach (var index in VolumeSelection.Range(volumeSize, options.FillMode, options.FillWidth))
		{
			voxels.SetVoxel(volumeSize, index, options.Voxel);
		}

		this.FinishVolume(volume, options);
		return gameObject;
	}

	public GameObject Create(int sizeExponent, Options options)
	{
		return this.Create(sizeExponent, sizeExponent, options);
	}

	public GameObject Create(int volumeSizeExponent, int chunkSizeExponent, Options options)
	{
		Require.That(volumeSizeExponent >= 0);
		Require.That(chunkSizeExponent >= 0);
		Require.That(options != null);
		Require.That(options.IsValid);

		var volumeSize = new Index3D(MathUtility.PowerOf2(volumeSizeExponent));
		var chunkSize = new Index3D(MathUtility.PowerOf2(chunkSizeExponent));

		var gameObject = Instantiate(voxelVolumePrefab);
		var volume = this.CreateVolume(gameObject, volumeSize, chunkSize, options);

		var voxels = volume.Frames[0].Voxels;
		foreach (var index in VolumeSelection.Range(volumeSize, options.FillMode, options.FillWidth))
		{
			voxels.SetVoxel(volumeSizeExponent, index, options.Voxel);
		}

		this.FinishVolume(volume, options);
		return gameObject;
	}

	public GameObject DeepClone(Volume volume, Options options)
	{
		Require.That(volume != null);
		Require.That(options != null);
		Require.That(options.IsValid);

		var volumeSize = new Index3D(volume.XSize, volume.YSize, volume.ZSize);
		var chunkSize = new Index3D(volume.XChunkSize, volume.YChunkSize, volume.ZChunkSize);

		var gameObject = Instantiate(voxelVolumePrefab);
		var clonedVolume = this.CreateVolume(gameObject, volumeSize, chunkSize, options);

		volume.Frames[0].Voxels.CopyTo(clonedVolume.Frames[0].Voxels, 0);

		this.FinishVolume(clonedVolume, options);
		return gameObject;
	}

	private Volume CreateVolume(GameObject gameObject, Index3D volumeSize, Index3D chunkSize, Options options)
	{
		Require.That(gameObject != null);
		this.SharedContracts(volumeSize, chunkSize, options);

		var volume = gameObject.GetComponent<Volume>();
		////volume.SaveToMeshStore = options.SaveToMeshStore;
		volume.Material = this.material;
		volume.VoxelSize = options.VoxelSize;
		volume.XSize = volumeSize.X;
		volume.YSize = volumeSize.Y;
		volume.ZSize = volumeSize.Z;
		volume.XChunkSize = chunkSize.X;
		volume.YChunkSize = chunkSize.Y;
		volume.ZChunkSize = chunkSize.Z;
		volume.AddFrame(0);
		volume.Frames[0].Voxels = new Voxel[volume.XSize * volume.YSize * volume.ZSize];
		return volume;
	}

	private void FinishVolume(Volume volume, Options options)
	{
		Require.That(volume != null);
		Require.That(options != null);

		foreach (var child in volume.gameObject.GetComponentsInChildren<Transform>())
		{
			child.gameObject.hideFlags = options.HideFlags;
		}
	}

	[Conditional(Require.CompilationSymbol)]
	private void SharedContracts(Index3D volumeSize, Index3D chunkSize, Options options)
	{
		Require.That(volumeSize.X > 0);
		Require.That(volumeSize.Y > 0);
		Require.That(volumeSize.Z > 0);
		Require.That(chunkSize.X > 0);
		Require.That(chunkSize.Y > 0);
		Require.That(chunkSize.Z > 0);
		Require.That(options != null);
		Require.That(options.IsValid);
	}

	public class Options
	{
		public float VoxelSize { get; set; }

		public Voxel Voxel { get; set; }

		public VolumeSelection.Mode FillMode { get; set; }

		public int FillWidth { get; set; }

		////public bool SaveToMeshStore { get; set; }

		public HideFlags HideFlags { get; set; }

		public bool IsValid
		{
			get
			{
				return this.VoxelSize > 0 && Enumeration.IsDefined(this.FillMode) && this.FillWidth >= 0;
			}
		}

		public Options Clone()
		{
			return new Options()
			{
				VoxelSize = this.VoxelSize,
				Voxel = Voxel,
				FillMode = FillMode,
				FillWidth = FillWidth,
				////SaveToMeshStore = SaveToMeshStore,
				HideFlags = HideFlags
			};
		}
	}
}
