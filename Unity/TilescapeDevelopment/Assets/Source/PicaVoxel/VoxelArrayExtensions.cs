﻿using PicaVoxel;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;

public static class VoxelArrayExtensions
{
	#region Frame

	public static void SetVoxel(this Frame frame, Index3D index, Voxel voxel)
	{
		Require.That(frame != null);
		Require.That(frame.Voxels != null);

		frame.Voxels.SetVoxel(new Index3D(frame.XSize, frame.YSize, frame.ZSize), index, voxel);
	}

	public static void SetVoxel(this Frame frame, int x, int y, int z, Voxel voxel)
	{
		Require.That(frame != null);
		Require.That(frame.Voxels != null);

		frame.Voxels.SetVoxel(new Index3D(frame.XSize, frame.YSize, frame.ZSize), x, y, z, voxel);
	}

	public static Voxel GetVoxel(this Frame frame, Index3D index)
	{
		Require.That(frame != null);
		Require.That(frame.Voxels != null);

		return frame.Voxels.GetVoxel(new Index3D(frame.XSize, frame.YSize, frame.ZSize), index);
	}

	public static Voxel GetVoxel(this Frame frame, int x, int y, int z)
	{
		Require.That(frame != null);
		Require.That(frame.Voxels != null);

		return frame.Voxels.GetVoxel(new Index3D(frame.XSize, frame.YSize, frame.ZSize), x, y, z);
	}

	#endregion

	#region Array Index3D dimensions

	public static void SetVoxel(this Voxel[] array, Index3D dimensions, Index3D index, Voxel voxel)
	{
		Require.That(array != null);
		Require.That(dimensions.X > 0);
		Require.That(dimensions.Y > 0);
		Require.That(dimensions.Z > 0);
		Require.That(index.IsIn(Index3D.Zero, dimensions - new Index3D(1)));

		array[index.X + dimensions.X * (index.Y + dimensions.Y * index.Z)] = voxel;
	}

	public static void SetVoxel(this Voxel[] array, Index3D dimensions, int x, int y, int z, Voxel voxel)
	{
		Require.That(array != null);
		Require.That(dimensions.X > 0);
		Require.That(dimensions.Y > 0);
		Require.That(dimensions.Z > 0);
		Require.That(x >= 0 && x < dimensions.X);
		Require.That(y >= 0 && y < dimensions.Y);
		Require.That(z >= 0 && z < dimensions.Z);

		array[x + dimensions.X * (y + dimensions.Y * z)] = voxel;
	}

	public static Voxel GetVoxel(this Voxel[] array, Index3D dimensions, Index3D index)
	{
		Require.That(array != null);
		Require.That(dimensions.X > 0);
		Require.That(dimensions.Y > 0);
		Require.That(dimensions.Z > 0);
		Require.That(index.IsIn(Index3D.Zero, dimensions - new Index3D(1)));

		return array[index.X + dimensions.X * (index.Y + dimensions.Y * index.Z)];
	}

	public static Voxel GetVoxel(this Voxel[] array, Index3D dimensions, int x, int y, int z)
	{
		Require.That(array != null);
		Require.That(dimensions.X > 0);
		Require.That(dimensions.Y > 0);
		Require.That(dimensions.Z > 0);
		Require.That(x >= 0 && x < dimensions.X);
		Require.That(y >= 0 && y < dimensions.Y);
		Require.That(z >= 0 && z < dimensions.Z);

		return array[x + dimensions.X * (y + dimensions.Y * z)];
	}

	#endregion

	#region Array binary size

	public static void SetVoxel(this Voxel[] array, int binarySizeExponent, Index3D index, Voxel voxel)
	{
		Require.That(array != null);
		Require.That(binarySizeExponent >= 0);
		Require.That(index.IsIn(Index3D.Zero, new Index3D(MathUtility.PowerOf2(binarySizeExponent) - 1)));

		array[index.X + ((index.Y + (index.Z << binarySizeExponent)) << binarySizeExponent)] = voxel;
	}

	public static void SetVoxel(this Voxel[] array, int binarySizeExponent, int x, int y, int z, Voxel voxel)
	{
		Require.That(array != null);
		Require.That(binarySizeExponent >= 0);
		Require.That(x >= 0 && x < MathUtility.PowerOf2(binarySizeExponent));
		Require.That(y >= 0 && y < MathUtility.PowerOf2(binarySizeExponent));
		Require.That(z >= 0 && z < MathUtility.PowerOf2(binarySizeExponent));

		array[x + ((y + (z << binarySizeExponent)) << binarySizeExponent)] = voxel;
	}

	public static Voxel GetVoxel(this Voxel[] array, int binarySizeExponent, Index3D index)
	{
		Require.That(array != null);
		Require.That(binarySizeExponent >= 0);
		Require.That(index.IsIn(Index3D.Zero, new Index3D(MathUtility.PowerOf2(binarySizeExponent) - 1)));

		return array[index.X + ((index.Y + (index.Z << binarySizeExponent)) << binarySizeExponent)];
	}

	public static Voxel GetVoxel(this Voxel[] array, int binarySizeExponent, int x, int y, int z)
	{
		Require.That(array != null);
		Require.That(binarySizeExponent >= 0);
		Require.That(x >= 0 && x < MathUtility.PowerOf2(binarySizeExponent));
		Require.That(y >= 0 && y < MathUtility.PowerOf2(binarySizeExponent));
		Require.That(z >= 0 && z < MathUtility.PowerOf2(binarySizeExponent));

		return array[x + ((y + (z << binarySizeExponent)) << binarySizeExponent)];
	}

	#endregion
}
