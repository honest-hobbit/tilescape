﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CubeRotations))]
public class CubeRotationsInspector : Editor
{
	public override void OnInspectorGUI()
	{
		this.DrawDefaultInspector();
		var rotations = (CubeRotations)this.target;

		if (GUILayout.Button("Rotate X Clockwise"))
		{
			rotations.RotateX(clockwise: true);
		}

		//if (GUILayout.Button("Rotate X Counterclockwise"))
		//{
		//	rotations.RotateX(clockwise: false);
		//}

		if (GUILayout.Button("Rotate Y Clockwise"))
		{
			rotations.RotateY(clockwise: true);
		}

		//if (GUILayout.Button("Rotate Y Counterclockwise"))
		//{
		//	rotations.RotateY(clockwise: false);
		//}

		if (GUILayout.Button("Rotate Z Clockwise"))
		{
			rotations.RotateZ(clockwise: true);
		}

		//if (GUILayout.Button("Rotate Z Counterclockwise"))
		//{
		//	rotations.RotateZ(clockwise: false);
		//}

		if (GUILayout.Button("Flip X"))
		{
			rotations.FlipX();
		}

		if (GUILayout.Button("Flip Y"))
		{
			rotations.FlipY();
		}

		if (GUILayout.Button("Flip Z"))
		{
			rotations.FlipZ();
		}

		if (GUILayout.Button("Next Rotation"))
		{
			rotations.NextRotation();
		}

		if (GUILayout.Button("Reset Rotation"))
		{
			rotations.ResetRotation();
		}
	}
}
