﻿using UnityEngine;

////[ExecuteInEditMode]
public class ObjectLifecycle : MonoBehaviour
{
	// these are listed in the order the engine calls them
	private void Awake() => Debug.Log($"{this.name} Awake");

	private void OnEnable() => Debug.Log($"{this.name} OnEnable");

	private void Start() => Debug.Log($"{this.name} Start");

	private void OnDisable() => Debug.Log($"{this.name} OnDisable");

	private void OnDestroy() => Debug.Log($"{this.name} OnDestroy");
}
