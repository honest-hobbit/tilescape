﻿using Tilescape.Utility.Mathematics;
using UnityEngine;

[ExecuteInEditMode]
public class CubeRotations : MonoBehaviour
{
	[SerializeField]
	private float scale = 1;

	private int rotX = 0;

	private int rotY = 0;

	private int rotZ = 0;

	private bool flipX = false;

	private bool flipY = false;

	private bool flipZ = false;

	public void RotateX(bool clockwise = true)
	{
		this.rotX = MathUtility.ActualModulo(this.rotX + (clockwise ? 1 : -1), 4);
		this.ApplyRotation();
	}

	public void RotateY(bool clockwise = true)
	{
		this.rotY = MathUtility.ActualModulo(this.rotY + (clockwise ? 1 : -1), 4);
		this.ApplyRotation();
	}

	public void RotateZ(bool clockwise = true)
	{
		this.rotZ = MathUtility.ActualModulo(this.rotZ + (clockwise ? 1 : -1), 4);
		this.ApplyRotation();
	}

	public void FlipX()
	{
		this.flipX = !this.flipX;
		this.ApplyRotation();
	}

	public void FlipY()
	{
		this.flipY = !this.flipY;
		this.ApplyRotation();
	}

	public void FlipZ()
	{
		this.flipZ = !this.flipZ;
		this.ApplyRotation();
	}

	public void NextRotation()
	{
		this.flipZ = !this.flipZ;
		if (!this.flipZ)
		{
			this.flipY = !this.flipY;
			if (!this.flipY)
			{
				this.flipX = !this.flipX;
				if (!this.flipX)
				{
					this.rotZ++;
					if (this.rotZ == 4)
					{
						this.rotZ = 0;

						this.rotY++;
						if (this.rotY == 4)
						{
							this.rotY = 0;

							this.rotX++;
							if (this.rotX == 4)
							{
								this.rotX = 0;
							}
						}
					}
				}
			}
		}

		this.ApplyRotation();
	}

	public void ResetRotation()
	{
		this.rotX = 0;
		this.rotY = 0;
		this.rotZ = 0;
		this.flipX = false;
		this.flipY = false;
		this.flipZ = false;

		this.ApplyRotation();
	}

	private void ApplyRotation()
	{
		////int number =
		////	(this.flipZ ? 1 : 0) + ((this.flipY ? 1 : 0) * 2) + ((this.flipX ? 1 : 0) * 4) +
		////	(this.rotZ * 8) + (this.rotY * 32) + (this.rotX * 128);

		////Debug.Log($"Case: {number}   Rototation: ({this.rotX}, {this.rotY}, {this.rotZ})   Flip: ({this.flipX}, {this.flipY}, {this.flipZ})");

		this.transform.rotation = Quaternion.Euler(this.rotX * 90, this.rotY * 90, this.rotZ * 90);
		this.transform.localScale = new Vector3(
			this.flipX ? -this.scale : this.scale, this.flipY ? -this.scale : this.scale, this.flipZ ? -this.scale : this.scale);
	}
}
