﻿Shader "Custom/OrthographicDepthDarkening"
{
	Properties
	{
		_MainTex ("Main Texture (Will be auto assigned)", 2D) = "white" {}
		_Threshold ("Threshold", float) = 0
		_LinearWeight ("Linear Weight", float) = 1
		_ExponentialWeight ("Exponential Weight", float) = 1
		_MinSaturation ("Minimum Saturation", float) = 0
	}

	SubShader
    {
        Tags { "RenderType"="Opaque" }
 
        Pass
        {
			ZWrite Off

            CGPROGRAM

            #pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
 
			uniform sampler2D _MainTex; // the already rendered image before applying the depth shading
            uniform sampler2D _CameraDepthTexture; // the depth texture
			float _Threshold;
			float _LinearWeight;
			float _ExponentialWeight;
			float _MinSaturation;

            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 projPos : TEXCOORD1; // Screen position of pos
            };
 
            v2f vert(appdata_base v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.projPos = ComputeScreenPos(o.pos);
 
                return o;
            }
 
            half4 frag(v2f i) : COLOR
            {
				// gets the distance the pixel is from the camera (range of [0, 1])
                float saturation = tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)).r;
				
				saturation += _Threshold;
				if (saturation >= 1)
				{
					// return the original texture, no darkening
					return tex2D(_MainTex, i.projPos);
				}
				else
				{
					saturation = (1 + ((saturation - 1) * _LinearWeight));
					saturation = pow(saturation, _ExponentialWeight);
				}

				if (saturation < _MinSaturation)
				{
					saturation = _MinSaturation;
				}
 
                half4 shading;
                shading.r = saturation;
                shading.g = saturation;
                shading.b = saturation;
                shading.a = 1;

				float4 color = tex2D(_MainTex, i.projPos);
                return color * shading;
            }
 
            ENDCG
        }
    }
}
