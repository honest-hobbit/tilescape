using System;
using System.Collections.Immutable;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using Tilescape.Analyzer.Utility;

namespace Tilescape.Analyzer
{
	[DiagnosticAnalyzer(LanguageNames.CSharp)]
	public class NamespaceAnalyzer : DiagnosticAnalyzer
	{
		private const string Category = "HappyQuadtree.Naming";

		private static readonly DiagnosticDescriptor Rule1000 = new DiagnosticDescriptor(
			"HQ1000",
			Localize(nameof(Resources.HQ1000Title)),
			Localize(nameof(Resources.HQ1000MessageFormat)),
			Category,
			DiagnosticSeverity.Warning,
			isEnabledByDefault: true,
			description: Localize(nameof(Resources.HQ1000Description)));

		private static readonly DiagnosticDescriptor Rule1001 = new DiagnosticDescriptor(
			"HQ1001",
			Localize(nameof(Resources.HQ1001Title)),
			Localize(nameof(Resources.HQ1001MessageFormat)),
			Category,
			DiagnosticSeverity.Warning,
			isEnabledByDefault: true,
			description: Localize(nameof(Resources.HQ1001Description)));

		/// <inheritdoc/>
		public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(Rule1000, Rule1001);

		/// <inheritdoc/>
		public override void Initialize(AnalysisContext context)
		{
			context.RegisterCompilationStartAction(compilationContext =>
			{
				compilationContext.RegisterSyntaxTreeAction(syntaxTreeContext =>
				{
					var semModel = compilationContext.Compilation.GetSemanticModel(syntaxTreeContext.Tree);
					var filePath = syntaxTreeContext.Tree.FilePath;

					if (filePath == null)
					{
						return;
					}

					string expectedNamespace;
					bool expectedNamespaceFound = NamespaceFinder.TryGetNamespaceFromPath(filePath, out expectedNamespace);

					var namespaceNodes =
						syntaxTreeContext.Tree.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>();

					if (expectedNamespaceFound)
					{
						foreach (var namespaceNode in namespaceNodes)
						{
							var symbolInfo = semModel.GetDeclaredSymbol(namespaceNode);
							var namespaceString = symbolInfo.ToDisplayString();

							if (!namespaceString.Equals(expectedNamespace, StringComparison.Ordinal))
							{
								syntaxTreeContext.ReportDiagnostic(Diagnostic.Create(
									Rule1000, namespaceNode.Name.GetLocation(), expectedNamespace));
							}
						}
					}
					else
					{
						foreach (var namespaceNode in namespaceNodes)
						{
							syntaxTreeContext.ReportDiagnostic(Diagnostic.Create(Rule1001, namespaceNode.Name.GetLocation()));
						}
					}
				});
			});
		}

		private static LocalizableString Localize(string resourceName) =>
			new LocalizableResourceString(resourceName, Resources.ResourceManager, typeof(Resources));
	}
}
