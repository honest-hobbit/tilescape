﻿using System;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Test.ConsoleApp.Utility.Indexing
{
	public static class RangeTests
	{
		public static void RangeTest()
		{
			int iterations = 0;
			foreach (var index in new Index3D(0).Range(new Index3D(3)))
			{
				Console.WriteLine($"{index.ToString()} - {index.ToMortonCode().ToString()}");
				iterations++;
			}

			Console.WriteLine("Iterations: " + iterations);
		}
	}
}
