﻿using System;
using System.IO;
using System.Reflection;
using SQLite;
using Tilescape.Utility.Stores;
using Tilescape.Utility.Stores.SQLite;
using Tilescape.Utility.Types;

namespace Tilescape.Test.ConsoleApp.Utility.Stores
{
	public static class SQLiteExperimenting
	{
		public static void Test()
		{
			ExecuteStoreTest(store =>
			{
				var entity = new TestEntity()
				{
					TestInt = 13,
					TestString = "Hello world"
				};

				store.Add(entity);
				var retrieved = store.TryGet<int, TestEntity>(entity.Key);
				var retrievedSelect = store.TryGet<int, TestEntitySelect>(entity.Key);

				Console.WriteLine(retrieved);
				Console.WriteLine(retrievedSelect);

				Console.ReadLine();
			});
		}

		private static string GetDatabasePath() => Path.Combine(
			Path.GetDirectoryName(Uri.UnescapeDataString(new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path)),
			$"{Guid.NewGuid().ToString()}_Test.db");

		private static void ExecuteStoreTest(Action<IStore> testStore)
		{
			var path = GetDatabasePath();
			try
			{
				using (var store = SQLiteStore.Create(path, typeof(TestEntity), typeof(TestEntitySelect)))
				{
					testStore(store);
				}
			}
			finally
			{
				File.Delete(path);
			}
		}

		[Table(nameof(TestEntity))]
		public class TestEntity : IKeyed<int>
		{
			[PrimaryKey, AutoIncrement]
			public int Key { get; set; }

			public int TestInt { get; set; }

			public string TestString { get; set; }

			public override string ToString() => $"Key: {this.Key}, Int: {this.TestInt}, String: {this.TestString}";
		}

		[Table(nameof(TestEntity))]
		public class TestEntitySelect : IKeyed<int>
		{
			[PrimaryKey, AutoIncrement]
			public int Key { get; set; }

			public int TestInt { get; set; }

			public override string ToString() => $"Key: {this.Key}, Int: {this.TestInt}";
		}
	}
}
