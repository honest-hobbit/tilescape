﻿using System.Collections.Generic;
using Tilescape.Test.Utility.Serialization;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Types.Serialization;
using UnityEngine;
using Xunit;

namespace Tilescape.Test.Unity.Types.Serialization
{
	public static class Vector2SerializerTests
	{
		private static readonly int ExpectedLength = Length.OfFloat.InBytes * 2;

		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { new Vector2(float.MinValue, float.MinValue) },
				new object[] { new Vector2(float.MaxValue, float.MaxValue) },
				new object[] { Vector2.zero },
				new object[] { new Vector2(7.13f, 20.4f) },
				new object[] { new Vector2(-7.13f, -20.4f) }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(Vector2 value) => UniformSerDesTests.RunTests(Vector2Serializer.Instance, value, ExpectedLength);
	}
}
