﻿using System.Collections.Generic;
using Tilescape.Test.Utility.Serialization;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Types.Serialization;
using UnityEngine;
using Xunit;

namespace Tilescape.Test.Unity.Types.Serialization
{
	public static class Color32SerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { new Color32(0, 0, 0, 0) },
				new object[] { new Color32(255, 0, 0, 0) },
				new object[] { new Color32(0, 255, 0, 0) },
				new object[] { new Color32(0, 0, 255, 0) },
				new object[] { new Color32(0, 0, 0, 255) },
				new object[] { new Color32(255, 255, 255, 255) },
				new object[] { new Color32(33, 164, 113, 218) }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RGBTests(Color32 value) =>
			UniformSerDesTests.RunTests(Color32Serializer.RGB, new Color32(value.r, value.g, value.b, 255), Length.OfByte.InBytes * 3);

		[Theory]
		[MemberData(nameof(Values))]
		public static void RGBATests(Color32 value) =>
			UniformSerDesTests.RunTests(Color32Serializer.RGBA, value, Length.OfByte.InBytes * 4);
	}
}
