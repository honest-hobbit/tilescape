﻿using System.Collections.Generic;
using Tilescape.Test.Utility.Serialization;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Types.Serialization;
using UnityEngine;
using Xunit;

namespace Tilescape.Test.Unity.Types.Serialization
{
	public static class Vector3SerializerTests
	{
		private static readonly int ExpectedLength = Length.OfFloat.InBytes * 3;

		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { new Vector3(float.MinValue, float.MinValue, float.MinValue) },
				new object[] { new Vector3(float.MaxValue, float.MaxValue, float.MaxValue) },
				new object[] { Vector3.zero },
				new object[] { new Vector3(7.13f, 20.4f, 13.9f) },
				new object[] { new Vector3(-7.13f, -20.4f, -13.9f) }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(Vector3 value) => UniformSerDesTests.RunTests(Vector3Serializer.Instance, value, ExpectedLength);
	}
}
