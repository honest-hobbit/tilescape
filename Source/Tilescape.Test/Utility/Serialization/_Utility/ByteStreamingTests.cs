﻿using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;

namespace Tilescape.Test.Utility.Serialization
{
	public static class ByteStreamingTests
	{
		public static void SerializeAndDeserialize<T>(ISerDes<T> serializer, IEnumerable<T> values)
		{
			Require.That(serializer != null);
			Require.That(values.AllAndSelfNotNull());

			var initialCapacity = 0;
			var stream = new MemoryStream(initialCapacity);

			foreach (var value in values)
			{
				serializer.Serialize(value, stream.WriteByte);
			}

			// move back to the start of the stream to start reading from
			stream.Position = 0;

			foreach (var value in values)
			{
				serializer.Deserialize(stream.TryReadByte).Should().Be(value);
			}
		}
	}
}
