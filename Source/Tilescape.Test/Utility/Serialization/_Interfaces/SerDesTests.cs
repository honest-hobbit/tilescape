﻿using System.Collections.Generic;
using FluentAssertions;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;

namespace Tilescape.Test.Utility.Serialization
{
	public static class SerDesTests
	{
		public static void RunTests<TValue>(ISerDes<TValue> subject, TValue value, int expectedSerializedLength)
		{
			Require.That(subject != null);

			GetSerializedLength(subject, value, expectedSerializedLength);
			SerializeToArray(subject, value, expectedSerializedLength);
			SerializeToArrayMidway(subject, value, expectedSerializedLength);
			SerializeWriteAction(subject, value, expectedSerializedLength);
			DeserializeFromBufferedArray(subject, value);
		}

		private static void GetSerializedLength<TValue>(ISerDes<TValue> subject, TValue value, int expectedSerializedLength)
		{
			Require.That(subject != null);

			subject.GetSerializedLength(value).Should().Be(expectedSerializedLength);
		}

		private static void SerializeToArray<TValue>(ISerDes<TValue> subject, TValue value, int expectedSerializedLength)
		{
			Require.That(subject != null);

			var dataBuffer = subject.Serialize(value);
			dataBuffer.Length.Should().Be(expectedSerializedLength);
			subject.Deserialize(dataBuffer).Should().Be(value);
		}

		private static void SerializeToArrayMidway<TValue>(ISerDes<TValue> subject, TValue value, int expectedSerializedLength)
		{
			SerializeToArrayMidway(subject, value, 0, expectedSerializedLength);
			SerializeToArrayMidway(subject, value, 1, expectedSerializedLength);
			SerializeToArrayMidway(subject, value, 2, expectedSerializedLength);
		}

		private static void SerializeToArrayMidway<TValue>(ISerDes<TValue> subject, TValue value, int startingIndex, int expectedSerializedLength)
		{
			Require.That(subject != null);

			var dataBuffer = new byte[subject.GetSerializedLength(value) + startingIndex];
			int serializedIndex = startingIndex;

			// serialize the object and check the serialized length of the object
			var serializedLength = subject.Serialize(value, dataBuffer, ref serializedIndex);

			serializedLength.Should().Be(expectedSerializedLength);

			// index should have moved to one past the last index the object was serialized into
			serializedIndex.Should().Be(startingIndex + serializedLength);

			// difference between starting and ending index should be the serialized length of the object
			(serializedIndex - startingIndex).Should().Be(serializedLength);

			int deserializedIndex = startingIndex;

			// deserialized object should be logically equivalent to the original object
			var deserializedValue = subject.Deserialize(dataBuffer, ref deserializedIndex);

			deserializedValue.Should().Be(value);

			// index should have moved to one past the last index the object was deserialized from
			deserializedIndex.Should().Be(startingIndex + serializedLength);

			// difference between starting and ending index should be the serialized length of the object
			(deserializedIndex - startingIndex).Should().Be(serializedLength);
		}

		private static void SerializeWriteAction<TValue>(ISerDes<TValue> subject, TValue value, int expectedSerializedLength)
		{
			Require.That(subject != null);

			var buffer = new Queue<byte>(subject.GetSerializedLength(value));
			subject.Serialize(value, buffer.Enqueue).Should().Be(expectedSerializedLength);
			buffer.Count.Should().Be(expectedSerializedLength);

			subject.Deserialize(buffer.TryDequeue).Should().Be(value);
		}

		private static void DeserializeFromBufferedArray<TValue>(ISerDes<TValue> subject, TValue value)
		{
			Require.That(subject != null);

			var buffer = new Queue<byte>(subject.Serialize(value));
			subject.Deserialize(buffer.TryDequeue).Should().Be(value);
		}
	}
}
