﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;

namespace Tilescape.Test.Utility.Serialization
{
	public static class UniformSerDesTests
	{
		public static void RunTests<TValue>(IUniformSerDes<TValue> subject, TValue value, int expectedSerializedLength)
		{
			Require.That(subject != null);

			UniformSerializedLengthTests.RunTests(subject, expectedSerializedLength);
			SerDesTests.RunTests(subject, value, expectedSerializedLength);
		}
	}
}
