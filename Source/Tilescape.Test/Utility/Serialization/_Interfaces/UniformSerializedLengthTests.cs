﻿using FluentAssertions;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;

namespace Tilescape.Test.Utility.Serialization
{
	public static class UniformSerializedLengthTests
	{
		public static void RunTests(IUniformSerializedLength subject, int expectedSerializedLength)
		{
			Require.That(subject != null);

			subject.SerializedLength.Should().Be(expectedSerializedLength);
		}
	}
}
