﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class IntSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { int.MinValue },
				new object[] { int.MaxValue },
				new object[] { -7 },
				new object[] { 7 },
				new object[] { 0 }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(int value) => UniformSerDesTests.RunTests(Serializer.Int, value, Length.OfInt.InBytes);
	}
}
