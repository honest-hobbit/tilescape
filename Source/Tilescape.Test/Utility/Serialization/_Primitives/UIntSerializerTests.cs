﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class UIntSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { uint.MinValue },
				new object[] { uint.MaxValue },
				new object[] { 7 }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(uint value) => UniformSerDesTests.RunTests(Serializer.UInt, value, Length.OfUInt.InBytes);
	}
}
