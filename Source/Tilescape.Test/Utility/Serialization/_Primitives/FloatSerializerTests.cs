﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class FloatSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { float.MinValue },
				new object[] { float.MaxValue },
				new object[] { -7.13 },
				new object[] { 7.13 },
				new object[] { 0 }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(float value) => UniformSerDesTests.RunTests(Serializer.Float, value, Length.OfFloat.InBytes);
	}
}
