﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class ULongSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { ulong.MinValue },
				new object[] { ulong.MaxValue },
				new object[] { 7 }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(ulong value) => UniformSerDesTests.RunTests(Serializer.ULong, value, Length.OfULong.InBytes);
	}
}
