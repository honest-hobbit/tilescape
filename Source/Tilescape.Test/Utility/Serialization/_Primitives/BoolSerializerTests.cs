﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public class BoolSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { true },
				new object[] { false }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(bool value) => UniformSerDesTests.RunTests(Serializer.Bool, value, Length.OfBool.InBytes);
	}
}
