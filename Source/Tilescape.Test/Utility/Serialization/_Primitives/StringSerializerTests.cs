﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class StringSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { string.Empty },
				new object[] { "a" },
				new object[] { "abc" },
				new object[] { "Hello World" },
			};

		public static int GetLengthInclude(string value, int lengthInBytes) => (value.Length * Length.OfChar.InBytes) + lengthInBytes;

		[Theory]
		[MemberData(nameof(Values))]
		public static void LengthAsByteTests(string value) => SerDesTests.RunTests(
			StringSerializer.LengthAsByte, value, GetLengthInclude(value, Length.OfByte.InBytes));

		[Theory]
		[MemberData(nameof(Values))]
		public static void LengthAsUShortTests(string value) => SerDesTests.RunTests(
			StringSerializer.LengthAsUShort, value, GetLengthInclude(value, Length.OfUShort.InBytes));

		[Theory]
		[MemberData(nameof(Values))]
		public static void LengthAsIntTests(string value) => SerDesTests.RunTests(
			StringSerializer.LengthAsInt, value, GetLengthInclude(value, Length.OfInt.InBytes));
	}
}
