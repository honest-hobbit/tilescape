﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class DateTimeSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { DateTime.MinValue },
				new object[] { DateTime.MaxValue },
				new object[] { DateTime.Today },
				new object[] { new DateTime(1989, 12, 7) },
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(DateTime value) => UniformSerDesTests.RunTests(Serializer.DateTime, value, Length.OfLong.InBytes);
	}
}
