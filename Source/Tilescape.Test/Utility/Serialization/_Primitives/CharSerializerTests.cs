﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class CharSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { char.MinValue },
				new object[] { char.MaxValue },
				new object[] { 'c' }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(char value) => UniformSerDesTests.RunTests(Serializer.Char, value, Length.OfChar.InBytes);
	}
}
