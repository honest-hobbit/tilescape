﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class DoubleSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { double.MinValue },
				new object[] { double.MaxValue },
				new object[] { -7.13 },
				new object[] { 7.13 },
				new object[] { 0 }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(double value) => UniformSerDesTests.RunTests(Serializer.Double, value, Length.OfDouble.InBytes);
	}
}
