﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class UShortSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { ushort.MinValue },
				new object[] { ushort.MaxValue },
				new object[] { 7 }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(ushort value) => UniformSerDesTests.RunTests(Serializer.UShort, value, Length.OfUShort.InBytes);
	}
}
