﻿using System.Collections.Generic;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Serialization
{
	public static class LongSerializerTests
	{
		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { long.MinValue },
				new object[] { long.MaxValue },
				new object[] { -7 },
				new object[] { 7 },
				new object[] { 0 }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(long value) => UniformSerDesTests.RunTests(Serializer.Long, value, Length.OfLong.InBytes);
	}
}
