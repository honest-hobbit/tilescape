﻿using System;
using System.IO;
using System.Reflection;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using Ploeh.AutoFixture.Xunit2;

namespace Tilescape.Test.Utility.Stores.SQLite
{
	public class MoqSQLiteData : AutoDataAttribute
	{
		public MoqSQLiteData()
			: base(new Fixture().Customize(new AutoMoqCustomization()))
		{
		}

		public static string GetDatabasePath() => Path.Combine(
			Path.GetDirectoryName(Uri.UnescapeDataString(new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path)),
			$"{Guid.NewGuid().ToString()}_Test.db");
	}
}
