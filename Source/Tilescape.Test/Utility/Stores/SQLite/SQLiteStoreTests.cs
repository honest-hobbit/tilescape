﻿using System;
using System.IO;
using System.Linq;
using FluentAssertions;
using SQLite;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Stores;
using Tilescape.Utility.Stores.SQLite;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Stores.SQLite
{
	// change this class to public when you want to run the tests
	// if these tests fail from a BadImageFormatException go to Test -> Test Settings -> Default Processor Architecture -> X64
	internal static class SQLiteStoreTests
	{
		private static void ExecuteStoreTest<T>(Action<IStore> testStore)
			where T : class, new()
		{
			var path = MoqSQLiteData.GetDatabasePath();
			try
			{
				using (var store = SQLiteStore.Create(path, typeof(T)))
				{
					testStore(store);
				}
			}
			finally
			{
				File.Delete(path);
			}
		}

		public static class AddAndGetTests
		{
			[Theory, MoqSQLiteData]
			public static void ShouldAddEntityToDatabase(TestEntity entity)
			{
				ExecuteStoreTest<TestEntity>(store =>
				{
					store.Add(entity);
					var retrieved = store.TryGet<int, TestEntity>(entity.Key);

					retrieved.HasValue.Should().BeTrue();
					retrieved.Value.ShouldBeEquivalentTo(entity);
				});
			}
		}

		public static class UpdateTests
		{
			[Theory, MoqSQLiteData]
			public static void ShouldUpdateEntityInDatabase(TestEntity entity)
			{
				ExecuteStoreTest<TestEntity>(store =>
				{
					store.Add(entity);

					entity.TestString = "test";
					store.Update(entity);

					var retrieved = store.TryGet<int, TestEntity>(entity.Key);

					retrieved.HasValue.Should().BeTrue();
					retrieved.Value.ShouldBeEquivalentTo(entity);
				});
			}
		}

		public static class AllTests
		{
			[Theory, MoqSQLiteData]
			public static void ShouldRetrieveAllEntitiesFromTable(TestEntity entity1, TestEntity entity2)
			{
				ExecuteStoreTest<TestEntity>(store =>
				{
					store.Add(entity1);
					store.Add(entity2);

					var entities = store.All<TestEntity>();

					entities.Count().Should().Be(2);
					entities.First(x => x.Key == entity1.Key).ShouldBeEquivalentTo(entity1);
					entities.First(x => x.Key == entity2.Key).ShouldBeEquivalentTo(entity2);
				});
			}

			[Fact]
			public static void ShouldReturnEmptyListForEmptyTable()
			{
				ExecuteStoreTest<TestEntity>(store =>
				{
					var entities = store.All<TestEntity>();

					entities.Should().BeEmpty();
				});
			}
		}

		public static class RemoveTests
		{
			[Theory, MoqSQLiteData]
			public static void ShouldRemoveEntityFromDatabase(TestEntity entity)
			{
				ExecuteStoreTest<TestEntity>(store =>
				{
					store.Add(entity);
					store.Remove(entity);

					var entities = store.All<TestEntity>();
					entities.Should().NotContain(entity);
				});
			}
		}

		public static class PerformanceTests
		{
			private static readonly int EntityCount = 100000;

			// this test can be used to time the difference between bulk add and individually adding
			// but warning, even at just 1000 entities it takes 13 seconds to complete
			////[Fact]
			public static void IndividualWrites()
			{
				ExecuteStoreTest<TestEntity>(store =>
				{
					store.All<TestEntity>().Count().Should().Be(0);

					foreach (var entity in Factory.CreateArray(EntityCount, () => new TestEntity()))
					{
						store.Add(entity);
					}

					store.All<TestEntity>().Count().Should().Be(EntityCount);
				});
			}

			[Fact]
			public static void AddAll()
			{
				ExecuteStoreTest<TestEntity>(store =>
				{
					store.All<TestEntity>().Count().Should().Be(0);

					store.AddMany(Factory.CreateMany(EntityCount, () => new TestEntity()));

					store.All<TestEntity>().Count().Should().Be(EntityCount);
				});
			}

			[Fact]
			public static void TransactionBulkAdd()
			{
				ExecuteStoreTest<TestEntity>(store =>
				{
					store.All<TestEntity>().Count().Should().Be(0);

					store.RunInTransaction(transaction =>
					{
						foreach (var entity in Factory.CreateMany(EntityCount, () => new TestEntity()))
						{
							transaction.Add(entity);
						}
					});

					store.All<TestEntity>().Count().Should().Be(EntityCount);
				});
			}
		}

		public static class InternalEntityTests
		{
			[Theory, MoqSQLiteData]
			internal static void InternalVisibilityEntityShouldWork(InternalTestEntity entity)
			{
				ExecuteStoreTest<InternalTestEntity>(store =>
				{
					store.Add(entity);
					var entityRetrieved = store.TryGet<int, InternalTestEntity>(entity.Key);

					entityRetrieved.HasValue.Should().BeTrue();
					entityRetrieved.Value.ShouldBeEquivalentTo(entity);
				});
			}
		}

		public static class PrivateEntityTests
		{
			[Fact]
			internal static void PrivateVisibilityEntityShouldWork()
			{
				ExecuteStoreTest<PrivateTestEntity>(store =>
				{
					var entity = new PrivateTestEntity()
					{
						TestInt = 7,
						TestString = "Hello world"
					};

					store.Add(entity);
					var entityRetrieved = store.TryGet<int, PrivateTestEntity>(entity.Key);

					entityRetrieved.HasValue.Should().BeTrue();
					entityRetrieved.Value.ShouldBeEquivalentTo(entity);
				});
			}

			[Table(nameof(PrivateTestEntity))]
			private class PrivateTestEntity : IKeyed<int>
			{
				[PrimaryKey, AutoIncrement]
				public int Key { get; set; }

				public int TestInt { get; set; }

				public string TestString { get; set; }
			}
		}
	}
}