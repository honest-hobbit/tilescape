﻿using System.Collections.Generic;
using Tilescape.Test.Utility.Serialization;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Indexing.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Indexing.Serialization
{
	public static class Index1DSerializerTests
	{
		private static readonly int ExpectedLength = Length.OfInt.InBytes;

		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { new Index1D(int.MinValue) },
				new object[] { new Index1D(int.MaxValue) },
				new object[] { new Index1D(-7) },
				new object[] { new Index1D(7) },
				new object[] { Index1D.Zero }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(Index1D value) =>
			UniformSerDesTests.RunTests(Index1DSerializer.Instance, value, ExpectedLength);
	}
}
