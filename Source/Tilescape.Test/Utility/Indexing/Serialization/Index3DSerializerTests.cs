﻿using System.Collections.Generic;
using Tilescape.Test.Utility.Serialization;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Indexing.Serialization;
using Tilescape.Utility.Types;
using Xunit;

namespace Tilescape.Test.Utility.Indexing.Serialization
{
	public static class Index3DSerializerTests
	{
		private static readonly int ExpectedLength = Length.OfInt.InBytes * 3;

		public static IEnumerable<object[]> Values() =>
			new object[][]
			{
				new object[] { new Index3D(int.MinValue) },
				new object[] { new Index3D(int.MaxValue) },
				new object[] { new Index3D(-7, 13, 42) },
				new object[] { Index3D.Zero }
			};

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(Index3D value) =>
			UniformSerDesTests.RunTests(Index3DSerializer.Instance, value, ExpectedLength);
	}
}
