﻿using System;

namespace Tilescape.Utility.Types
{
	public interface IIndexed<T>
		where T : struct, IComparable<T>, IEquatable<T>
	{
		T Index { get; }
	}
}
