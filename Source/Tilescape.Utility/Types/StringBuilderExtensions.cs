﻿using System.Text;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Types
{
	/// <summary>
	/// Provides extension methods for use with the <see cref="StringBuilder"/> class.
	/// </summary>
	public static class StringBuilderExtensions
	{
		public static StringBuilder Append<T>(this StringBuilder builder, T value)
		{
			Require.That(builder != null);

			if (value != null)
			{
				builder.Append(value.ToString());
			}

			return builder;
		}

		/// <summary>
		/// Clears all text from the builder.
		/// </summary>
		/// <param name="builder">The builder to clear.</param>
		public static void Clear(this StringBuilder builder)
		{
			Require.That(builder != null);

			builder.Length = 0;
		}
	}
}
