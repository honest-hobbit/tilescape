﻿using System;
using System.Runtime.InteropServices;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Types
{
	[StructLayout(LayoutKind.Explicit)]
	internal struct GuidBytes
	{
		[FieldOffset(0)]
		public readonly byte Byte0;

		[FieldOffset(1)]
		public readonly byte Byte1;

		[FieldOffset(2)]
		public readonly byte Byte2;

		[FieldOffset(3)]
		public readonly byte Byte3;

		[FieldOffset(4)]
		public readonly byte Byte4;

		[FieldOffset(5)]
		public readonly byte Byte5;

		[FieldOffset(6)]
		public readonly byte Byte6;

		[FieldOffset(7)]
		public readonly byte Byte7;

		[FieldOffset(8)]
		public readonly byte Byte8;

		[FieldOffset(9)]
		public readonly byte Byte9;

		[FieldOffset(10)]
		public readonly byte Byte10;

		[FieldOffset(11)]
		public readonly byte Byte11;

		[FieldOffset(12)]
		public readonly byte Byte12;

		[FieldOffset(13)]
		public readonly byte Byte13;

		[FieldOffset(14)]
		public readonly byte Byte14;

		[FieldOffset(15)]
		public readonly byte Byte15;

		[FieldOffset(0)]
		private readonly Guid guid;

		public GuidBytes(Guid guid)
			: this()
		{
			this.guid = guid;
		}

		public void CopyTo(byte[] array, int index)
		{
			Require.That(array != null);
			Require.That(index >= 0);
			Require.That(index + 16 <= array.Length);

			array[index] = this.Byte0;
			array[index + 1] = this.Byte1;
			array[index + 2] = this.Byte2;
			array[index + 3] = this.Byte3;
			array[index + 4] = this.Byte4;
			array[index + 5] = this.Byte5;
			array[index + 6] = this.Byte6;
			array[index + 7] = this.Byte7;
			array[index + 8] = this.Byte8;
			array[index + 9] = this.Byte9;
			array[index + 10] = this.Byte10;
			array[index + 11] = this.Byte11;
			array[index + 12] = this.Byte12;
			array[index + 13] = this.Byte13;
			array[index + 14] = this.Byte14;
			array[index + 15] = this.Byte15;
		}
	}
}
