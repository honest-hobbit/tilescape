﻿using System;

namespace Tilescape.Utility.Types
{
	public static class Struct
	{
		public static bool Equals<TStruct>(TStruct value, object obj)
			where TStruct : struct, IEquatable<TStruct> => (obj is TStruct) ? value.Equals((TStruct)obj) : false;
	}
}
