﻿namespace Tilescape.Utility.Types
{
	public interface INamed
	{
		string Name { get; }
	}
}
