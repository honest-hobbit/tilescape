﻿using System;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Types
{
	/// <summary>
	/// A two way type converter that uses delegates passed into the constructor to perform the type conversions.
	/// </summary>
	/// <typeparam name="TSource">The type of the source.</typeparam>
	/// <typeparam name="TResult">The type of the result.</typeparam>
	public class TwoWayTypeConverter<TSource, TResult> : ITwoWayConverter<TSource, TResult>
	{
		/// <summary>
		/// The converter used to convert from the source type to the result type.
		/// </summary>
		private readonly Func<TSource, TResult> toResult;

		/// <summary>
		/// The converter used to convert from the result type to the source type.
		/// </summary>
		private readonly Func<TResult, TSource> toSource;

		/// <summary>
		/// Initializes a new instance of the <see cref="TwoWayTypeConverter{TSource, TResult}"/> class.
		/// </summary>
		/// <param name="toResult">The converter used to convert from the source type to the result type.</param>
		/// <param name="toSource">The converter used to convert from the result type to the source type.</param>
		public TwoWayTypeConverter(Func<TSource, TResult> toResult, Func<TResult, TSource> toSource)
		{
			Require.That(toResult != null);
			Require.That(toSource != null);

			this.toResult = toResult;
			this.toSource = toSource;
		}

		/// <inheritdoc />
		public TResult Convert(TSource value) => this.toResult(value);

		/// <inheritdoc />
		public TSource Convert(TResult value) => this.toSource(value);
	}
}
