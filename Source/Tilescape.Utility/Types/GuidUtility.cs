﻿using System;
using System.Text.RegularExpressions;

namespace Tilescape.Utility.Types
{
	public static class GuidUtility
	{
		private static readonly Regex Format = new Regex(
			"^[A-Fa-f0-9]{32}$|^({|\\()?[A-Fa-f0-9]{8}-([A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}(}|\\))?$|" +
			"^({)?[0xA-Fa-f0-9]{3,10}(, {0,1}[0xA-Fa-f0-9]{3,6}){2}, {0,1}({)([0xA-Fa-f0-9]{3,4}, {0,1}){7}[0xA-Fa-f0-9]{3,4}(}})$");

		public static bool TryParse(string value, out Guid result)
		{
			if (value.IsNullOrWhiteSpace())
			{
				result = Guid.Empty;
				return false;
			}

			try
			{
				if (Format.IsMatch(value))
				{
					result = new Guid(value);
					return true;
				}
				else
				{
					result = Guid.Empty;
					return false;
				}
			}
			catch (Exception e) when (e is FormatException || e is OverflowException)
			{
				result = Guid.Empty;
				return false;
			}
		}

		public static Guid Parse(string value) => new Guid(value);

		public static Guid FromNullableArray(byte[] array) => array != null ? new Guid(array) : Guid.Empty;

		public static byte[] ToNullableByteArray(this Guid guid) => guid != Guid.Empty ? guid.ToByteArray() : null;

		public static Guid FromNullableString(string value) => !value.IsNullOrWhiteSpace() ? new Guid(value) : Guid.Empty;

		public static string ToNullableString(this Guid guid) => guid != Guid.Empty ? guid.ToString() : null;

		public static void CopyTo(this Guid guid, byte[] array, int index = 0)
		{
			var buffer = new GuidBytes(guid);
			buffer.CopyTo(array, index);
		}

		public static Guid FromBytes(
			byte byte0,
			byte byte1,
			byte byte2,
			byte byte3,
			byte byte4,
			byte byte5,
			byte byte6,
			byte byte7,
			byte byte8,
			byte byte9,
			byte byte10,
			byte byte11,
			byte byte12,
			byte byte13,
			byte byte14,
			byte byte15)
		{
			int a = (byte3 << 24) | (byte2 << 16) | (byte1 << 8) | byte0;
			short b = (short)((byte5 << 8) | byte4);
			short c = (short)((byte7 << 8) | byte6);
			return new Guid(a, b, c, byte8, byte9, byte10, byte11, byte12, byte13, byte14, byte15);
		}
	}
}
