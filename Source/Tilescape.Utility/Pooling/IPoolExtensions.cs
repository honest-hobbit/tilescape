﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;

namespace Tilescape.Utility.Pooling
{
	/// <summary>
	/// Provides extension methods for <see cref="IPool{T}"/>.
	/// </summary>
	public static class IPoolExtensions
	{
		public static bool TryTakeLoan<T>(this IPool<T> pool, out IDisposableValue<T> value)
		{
			Require.That(pool != null);

			if (pool.TryTake(out var result))
			{
				value = new LoanedValue<T>(pool, result);
				return true;
			}
			else
			{
				value = null;
				return false;
			}
		}

		public static IDisposableValue<T> TakeLoan<T>(this IPool<T> pool)
		{
			Require.That(pool != null);

			return new LoanedValue<T>(pool, pool.Take());
		}

		public static IEnumerable<T> TakeAll<T>(this IPool<T> pool)
		{
			Require.That(pool != null);

			while (pool.TryTake(out var value))
			{
				yield return value;
			}
		}

		public static void Give<T>(this IPool<T> pool, T value, Action<T> release)
		{
			Require.That(pool != null);
			Require.That(release != null);

			if (!pool.TryGive(value))
			{
				release(value);
			}
		}

		public static void Give<T>(this IPool<T> pool, T value)
		{
			Require.That(pool != null);

			if (!pool.TryGive(value))
			{
				DisposableFactory.TryDispose(value);
			}
		}

		public static void GiveMany<T>(this IPool<T> pool, IEnumerable<T> values)
		{
			Require.That(pool != null);
			Require.That(values != null);

			foreach (var value in values)
			{
				if (!pool.TryGive(value))
				{
					return;
				}
			}
		}

		public static void GiveUntilFull<T>(this IPool<T> pool, Func<T> factory)
		{
			Require.That(pool != null);
			Require.That(pool.BoundedCapacity != Capacity.Unbounded);
			Require.That(factory != null);

			while (true)
			{
				if (!pool.TryGive(factory()))
				{
					return;
				}
			}
		}

		private class LoanedValue<T> : AbstractDisposableValue<T>
		{
			private readonly IPool<T> pool;

			public LoanedValue(IPool<T> pool, T value)
				: base(value)
			{
				Require.That(pool != null);

				this.pool = pool;
			}

			/// <inheritdoc />
			protected override void ManagedDisposal(T value) => this.pool.Give(value);
		}
	}
}
