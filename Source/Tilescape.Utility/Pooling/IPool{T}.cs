﻿namespace Tilescape.Utility.Pooling
{
	public interface IPool<T>
	{
		int AvailableCount { get; }

		int BoundedCapacity { get; }

		bool TryGive(T value);

		bool TryTake(out T value);

		T Take();
	}
}
