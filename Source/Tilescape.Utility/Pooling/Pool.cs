﻿using System;
using Tilescape.Utility.Collections;

namespace Tilescape.Utility.Pooling
{
	public static class Pool
	{
		public static Pool<T> CreateNew<T>(
			Action<T> initialize = null, Action<T> reset = null, int boundedCapacity = Capacity.Unbounded)
			where T : new() => new Pool<T>(() => new T(), initialize, reset, boundedCapacity);
	}
}
