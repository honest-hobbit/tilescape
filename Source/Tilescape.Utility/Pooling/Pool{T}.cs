﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Pooling
{
	public class Pool<T> : IPool<T>
	{
		private readonly Func<T> factory;

		private readonly Action<T> initialize;

		private readonly Action<T> reset;

		private readonly Stack<T> buffer;

		public Pool(Func<T> factory, Action<T> initialize = null, Action<T> reset = null, int boundedCapacity = Capacity.Unbounded)
		{
			Require.That(factory != null);
			Require.That(boundedCapacity >= 0 || boundedCapacity == Capacity.Unbounded);

			this.factory = factory;
			this.initialize = initialize;
			this.reset = reset;
			this.BoundedCapacity = boundedCapacity;
			this.buffer = boundedCapacity != Capacity.Unbounded ? new Stack<T>(boundedCapacity) : new Stack<T>();
		}

		/// <inheritdoc />
		public int AvailableCount => this.buffer.Count;

		/// <inheritdoc />
		public int BoundedCapacity { get; }

		/// <inheritdoc />
		public bool TryTake(out T value)
		{
			if (this.buffer.TryPop(out value))
			{
				this.initialize?.Invoke(value);
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <inheritdoc />
		public T Take()
		{
			if (this.TryTake(out var value))
			{
				return value;
			}
			else
			{
				var result = this.factory();
				this.initialize?.Invoke(result);
				return result;
			}
		}

		/// <inheritdoc />
		public bool TryGive(T value)
		{
			if (this.BoundedCapacity == Capacity.Unbounded || this.buffer.Count < this.BoundedCapacity)
			{
				this.reset?.Invoke(value);
				this.buffer.Push(value);
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
