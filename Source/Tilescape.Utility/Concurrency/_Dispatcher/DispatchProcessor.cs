﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public class DispatchProcessor : IDispatchProcessor
	{
		private readonly ConcurrentQueue<Action> pending = new ConcurrentQueue<Action>();

		private readonly Stopwatch timer = new Stopwatch();

		private readonly IDispatcher dispatcher;

		public DispatchProcessor()
		{
			this.dispatcher = new DispatcherInstance(this);
		}

		/// <inheritdoc />
		public IDispatcher Dispatcher => this.dispatcher;

		/// <inheritdoc />
		public void InvokeSingle()
		{
			if (this.pending.TryDequeue(out var action))
			{
				action();
			}
		}

		/// <inheritdoc />
		public void InvokeAll()
		{
			while (this.pending.TryDequeue(out var action))
			{
				action();
			}
		}

		/// <inheritdoc />
		public void InvokeMany(TimeSpan duration)
		{
			IDispatchProcessorContracts.InvokeMany(duration);

			if (duration == Duration.Span.InfiniteTimeout)
			{
				this.InvokeAll();
				return;
			}

			this.timer.Reset();
			this.timer.Start();
			while (this.timer.Elapsed < duration)
			{
				if (this.pending.TryDequeue(out var action))
				{
					action();
				}
				else
				{
					return;
				}
			}
		}

		private class DispatcherInstance : IDispatcher
		{
			private readonly DispatchProcessor processor;

			public DispatcherInstance(DispatchProcessor processor)
			{
				Require.That(processor != null);

				this.processor = processor;
			}

			/// <inheritdoc />
			public void Submit(Action action)
			{
				IDispatcherContracts.Submit(action);

				this.processor.pending.Enqueue(action);
			}

			/// <inheritdoc />
			public void Submit(IEnumerable<Action> actions)
			{
				IDispatcherContracts.Submit(actions);

				foreach (var action in actions)
				{
					this.processor.pending.Enqueue(action);
				}
			}
		}
	}
}
