﻿using System;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public static class IDispatcherExtensions
	{
		public static void Submit(this IDispatcher dispatcher, params Action[] actions)
		{
			Require.That(dispatcher != null);

			dispatcher.Submit(actions);
		}

		public static void Submit<T>(this IDispatcher dispatcher, T value, Action<T> action)
		{
			Require.That(dispatcher != null);
			Require.That(action != null);

			dispatcher.Submit(() => action(value));
		}
	}
}
