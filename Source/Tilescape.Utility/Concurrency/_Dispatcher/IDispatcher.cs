﻿using System;
using System.Collections.Generic;

namespace Tilescape.Utility.Concurrency
{
	public interface IDispatcher
	{
		void Submit(Action action);

		void Submit(IEnumerable<Action> actions);
	}
}
