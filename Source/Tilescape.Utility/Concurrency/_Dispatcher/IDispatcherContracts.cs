﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public static class IDispatcherContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Submit(Action action)
		{
			Require.That(action != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Submit(IEnumerable<Action> actions)
		{
			Require.That(actions.AllAndSelfNotNull());
		}
	}
}
