﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public static class IDispatchProcessorContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void InvokeMany(TimeSpan duration)
		{
			Require.That(duration.IsDuration());
		}
	}
}
