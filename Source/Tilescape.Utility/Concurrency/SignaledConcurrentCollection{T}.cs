﻿using System.Collections.Concurrent;
using System.Threading;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public class SignaledConcurrentCollection<T>
	{
		private readonly IProducerConsumerCollection<T> collection;

		private readonly EventWaitHandle signal;

		public SignaledConcurrentCollection(IProducerConsumerCollection<T> collection, EventWaitHandle signal)
		{
			Require.That(collection != null);
			Require.That(signal != null);

			this.collection = collection;
			this.signal = signal;
		}

		public bool TryAdd(T item)
		{
			var result = this.collection.TryAdd(item);
			if (result)
			{
				this.signal.Set();
			}

			return result;
		}
	}
}
