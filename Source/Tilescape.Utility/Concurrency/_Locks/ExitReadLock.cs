﻿using System;
using System.Threading;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public struct ExitReadLock : IDisposable
	{
		private readonly ReaderWriterLockSlim slimLock;

		internal ExitReadLock(ReaderWriterLockSlim slimLock)
		{
			Require.That(slimLock != null);

			this.slimLock = slimLock;
		}

		// ?. because the default struct constructor leaves the field as null
		/// <inheritdoc />
		public void Dispose() => this.slimLock?.ExitReadLock();
	}
}
