﻿using System.Threading;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public static class ReaderWriterLockSlimExtensions
	{
		public static ExitReadLock EnterReadLockInUsingBlock(this ReaderWriterLockSlim slimLock)
		{
			Require.That(slimLock != null);

			slimLock.EnterReadLock();
			return new ExitReadLock(slimLock);
		}

		public static ExitWriteLock EnterWriteLockInUsingBlock(this ReaderWriterLockSlim slimLock)
		{
			Require.That(slimLock != null);

			slimLock.EnterWriteLock();
			return new ExitWriteLock(slimLock);
		}
	}
}
