﻿using System.Threading;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	/// <summary>
	/// Provides extension methods for <see cref="SemaphoreSlim"/>.
	/// </summary>
	public static class SemaphoreSlimExtensions
	{
		public static ReleaseSemaphore WaitInUsingBlock(this SemaphoreSlim semaphore)
		{
			Require.That(semaphore != null);

			semaphore.Wait();
			return new ReleaseSemaphore(semaphore);
		}
	}
}
