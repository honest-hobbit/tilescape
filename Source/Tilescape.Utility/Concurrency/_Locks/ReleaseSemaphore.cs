﻿using System;
using System.Threading;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public struct ReleaseSemaphore : IDisposable
	{
		private readonly SemaphoreSlim semaphore;

		internal ReleaseSemaphore(SemaphoreSlim semaphore)
		{
			Require.That(semaphore != null);

			this.semaphore = semaphore;
		}

		// ?. because the default struct constructor leaves the field as null
		/// <inheritdoc />
		public void Dispose() => this.semaphore?.Release();
	}
}
