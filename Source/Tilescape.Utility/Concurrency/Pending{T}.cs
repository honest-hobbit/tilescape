﻿using System.Threading;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Concurrency
{
	public class Pending<T>
	{
		private readonly object padlock = new object();

		private bool hasValue;

		private T value;

		public Pending()
		{
			this.hasValue = false;
			this.value = default(T);
		}

		public Pending(T value)
		{
			this.hasValue = true;
			this.value = value;
		}

		public T Value
		{
			get
			{
				lock (this.padlock)
				{
					if (this.hasValue)
					{
						return this.value;
					}
					else
					{
						Monitor.Wait(this.padlock);
						return this.value;
					}
				}
			}
		}

		public void SetValue(T value)
		{
			lock (this.padlock)
			{
				Require.That(!this.hasValue);

				this.value = value;
				this.hasValue = true;
				Monitor.PulseAll(this.padlock);
			}
		}

		public bool TryGetValue(T value)
		{
			lock (this.padlock)
			{
				if (this.hasValue)
				{
					value = this.value;
					return true;
				}
				else
				{
					value = default(T);
					return false;
				}
			}
		}
	}
}
