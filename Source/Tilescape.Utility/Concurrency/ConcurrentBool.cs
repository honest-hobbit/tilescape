﻿using System.Threading;

namespace Tilescape.Utility.Concurrency
{
	/// <summary>
	/// A boolean value that can be used as a guard to safely control synchronized access to critical regions.
	/// Toggling the value and discovering if the value was successfully toggled are performed as a single atomic actions
	/// without the need for any locks.
	/// </summary>
	/// <threadsafety static="true" instance="true" />
	public class ConcurrentBool
	{
		private const int True = 1;

		private const int False = 0;

		/// <summary>
		/// The flag value it use as if it were a bool. 0 is false and 1 is true.
		/// </summary>
		/// <remarks>
		/// Use of an int instead of a bool is required because <see cref="Interlocked"/> does not support bool.
		/// </remarks>
		private int flagValue;

		/// <summary>
		/// Initializes a new instance of the <see cref="ConcurrentBool"/> class.
		/// </summary>
		/// <param name="value">The initial value for the bool to begin as.</param>
		public ConcurrentBool(bool value)
		{
			this.flagValue = value ? True : False;
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="ConcurrentBool"/> is currently true or false.
		/// </summary>
		/// <value>
		///   <c>True</c> if the <see cref="ConcurrentBool"/> if currently true; otherwise, <c>false</c>.
		/// </value>
		/// <remarks>
		/// The very instant after checking this value it could be changed by another thread toggling the
		/// <see cref="ConcurrentBool"/>. Algorithms that use this value must be designed accordingly.
		/// </remarks>
		public bool Value
		{
			get
			{
				return InterlockedValue.Read(ref this.flagValue) == True;
			}
		}

		public static implicit operator bool(ConcurrentBool value)
		{
			return value.Value;
		}

		/// <summary>
		/// If the bool is true, it will become false and this method will return true as a single atomic action.
		/// </summary>
		/// <returns>True if the bool was true; otherwise false.</returns>
		public bool TryToggleIfTrue()
		{
			return Interlocked.CompareExchange(ref this.flagValue, False, True) == True;
		}

		/// <summary>
		/// If the bool is false, it will become true and this method will return true as a single atomic action.
		/// </summary>
		/// <returns>True if the bool was false; otherwise false.</returns>
		public bool TryToggleIfFalse()
		{
			return Interlocked.CompareExchange(ref this.flagValue, True, False) == False;
		}

		public void SetTrue()
		{
			this.TryToggleIfFalse();
		}

		public void SetFalse()
		{
			this.TryToggleIfTrue();
		}

		/// <summary>
		/// Toggles the state of the bool. If the bool is true it will become false, otherwise it will become true.
		/// </summary>
		/// <remarks>
		/// This should only be called once after a successful call to <see cref="TryToggleIfTrue"/> or <see cref="TryToggleIfFalse"/>
		/// to reset the bool back to its previous value.
		/// </remarks>
		public void Toggle()
		{
			while (true)
			{
				if (this.TryToggleIfFalse())
				{
					return;
				}

				if (this.TryToggleIfTrue())
				{
					return;
				}
			}
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return this.Value ? bool.TrueString : bool.FalseString;
		}
	}
}
