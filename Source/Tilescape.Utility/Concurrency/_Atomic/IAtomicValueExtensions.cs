﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Concurrency
{
	/// <summary>
	/// Provides extension methods for <see cref="IAtomicValue{T}"/>.
	/// </summary>
	public static class IAtomicValueExtensions
	{
		// the way this method is implemented the variable can temporarily dip below the minimum value
		public static bool TryDecrementClampLower<T>(this IAtomicValue<T> variable, T minimumValue)
			where T : IComparable<T>
		{
			Require.That(variable != null);

			T unused;
			return variable.TryDecrementClampLower(minimumValue, out unused);
		}

		// the way this method is implemented the variable can temporarily dip below the minimum value
		public static bool TryDecrementClampLower<T>(this IAtomicValue<T> variable, T minimumValue, out T result)
			where T : IComparable<T>
		{
			Require.That(variable != null);

			result = variable.Decrement();
			if (result.IsGreaterThanOrEqualTo(minimumValue))
			{
				return true;
			}
			else
			{
				result = variable.Increment();
				return false;
			}
		}

		// the way this method is implemented the variable can temporarily dip below the minimum value
		public static bool TryIncrementClampUpper<T>(this IAtomicValue<T> variable, T maximumValue)
			where T : IComparable<T>
		{
			Require.That(variable != null);

			T unused;
			return variable.TryIncrementClampUpper(maximumValue, out unused);
		}

		// the way this method is implemented the variable can temporarily dip below the minimum value
		public static bool TryIncrementClampUpper<T>(this IAtomicValue<T> variable, T maximumValue, out T result)
			where T : IComparable<T>
		{
			Require.That(variable != null);

			result = variable.Increment();
			if (result.IsLessThanOrEqualTo(maximumValue))
			{
				return true;
			}
			else
			{
				result = variable.Decrement();
				return false;
			}
		}

		public static bool TryWriteIf<T>(this IAtomicValue<T> variable, T value, Func<T, bool> ifCurrentIs)
			where T : IEquatable<T>
		{
			Require.That(variable != null);
			Require.That(ifCurrentIs != null);

			T current = variable.Read();
			while (ifCurrentIs(current))
			{
				T result = variable.CompareExchange(value, current);
				if (result.Equals(current))
				{
					return true;
				}
				else
				{
					current = result;
				}
			}

			return false;
		}

		public static bool TryWriteIfLess<T>(this IAtomicValue<T> variable, T value)
			where T : IEquatable<T>, IComparable<T>
		{
			Require.That(variable != null);

			return variable.TryWriteIf(value, current => value.IsLessThan(current));
		}

		public static bool TryWriteIfGreater<T>(this IAtomicValue<T> variable, T value)
			where T : IEquatable<T>, IComparable<T>
		{
			Require.That(variable != null);

			return variable.TryWriteIf(value, current => value.IsGreaterThan(current));
		}

		public static bool TryWriteIfEqualTo<T>(this IAtomicValue<T> variable, T value, T comparand)
			where T : IEquatable<T>
		{
			Require.That(variable != null);

			return variable.CompareExchange(value, comparand).Equals(comparand);
		}
	}
}
