﻿using System.Threading;

namespace Tilescape.Utility.Concurrency
{
	public class Signal
	{
		private readonly object padlock = new object();

		private bool isSet;

		public Signal(bool isSet = false)
		{
			this.isSet = false;
		}

		public bool IsSet
		{
			get
			{
				lock (this.padlock)
				{
					return this.isSet;
				}
			}
		}

		public void Wait()
		{
			lock (this.padlock)
			{
				while (!this.isSet)
				{
					Monitor.Wait(this.padlock);
				}
			}
		}

		public void Set()
		{
			lock (this.padlock)
			{
				this.isSet = true;
				Monitor.PulseAll(this.padlock);
			}
		}

		public void Reset()
		{
			lock (this.padlock)
			{
				this.isSet = false;
				Monitor.PulseAll(this.padlock);
			}
		}
	}
}
