﻿using System.Threading;

namespace Tilescape.Utility.Concurrency
{
	public static class InterlockedValue
	{
		public static int Read(ref int value)
		{
			return Interlocked.CompareExchange(ref value, 0, 0);
		}

		public static long Read(ref long value)
		{
			return Interlocked.CompareExchange(ref value, 0, 0);
		}

		public static float Read(ref float value)
		{
			return Interlocked.CompareExchange(ref value, 0, 0);
		}

		public static double Read(ref double value)
		{
			return Interlocked.CompareExchange(ref value, 0, 0);
		}

		public static T Read<T>(ref T value)
			where T : class
		{
			return Interlocked.CompareExchange(ref value, null, null);
		}
	}
}
