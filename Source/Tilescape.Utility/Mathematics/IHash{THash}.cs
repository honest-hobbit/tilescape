﻿namespace Tilescape.Utility.Mathematics
{
	public interface IHash<THash>
	{
		int Result { get; }

		THash And<T>(T value);
	}
}
