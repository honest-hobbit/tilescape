﻿using System;
using MiscUtil;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Mathematics
{
	public struct Number<T> : IEquatable<Number<T>>, IComparable<Number<T>>
	{
		public Number(T value)
		{
			this.Value = value;
		}

		public T Value { get; }

		#region Implicit Conversion Operators

		public static implicit operator Number<T>(T value) => new Number<T>(value);

		public static explicit operator T(Number<T> value) => value.Value;

		#endregion

		#region Mathematical Operators

		public static Number<T> operator +(Number<T> lhs, Number<T> rhs) =>
			new Number<T>(Operator.Add(lhs.Value, rhs.Value));

		public static Number<T> operator -(Number<T> lhs, Number<T> rhs) =>
			new Number<T>(Operator.Subtract(lhs.Value, rhs.Value));

		public static Number<T> operator *(Number<T> lhs, Number<T> rhs) =>
			new Number<T>(Operator.Multiply(lhs.Value, rhs.Value));

		public static Number<T> operator /(Number<T> lhs, Number<T> rhs) =>
			new Number<T>(Operator.Divide(lhs.Value, rhs.Value));

		public static Number<T> operator /(Number<T> value, int divisor) =>
			new Number<T>(Operator.DivideInt32(value.Value, divisor));

		public static Number<T> operator -(Number<T> value) =>
			new Number<T>(Operator.Negate(value.Value));

		#endregion

		#region Equality Operators

		public static bool operator ==(Number<T> lhs, Number<T> rhs) =>
			Operator.Equal(lhs.Value, rhs.Value);

		public static bool operator !=(Number<T> lhs, Number<T> rhs) =>
			Operator.NotEqual(lhs.Value, rhs.Value);

		#endregion

		#region Comparison Operators

		public static bool operator >(Number<T> lhs, Number<T> rhs) =>
			Operator.GreaterThan(lhs.Value, rhs.Value);

		public static bool operator <(Number<T> lhs, Number<T> rhs) =>
			Operator.LessThan(lhs.Value, rhs.Value);

		public static bool operator >=(Number<T> lhs, Number<T> rhs) =>
			Operator.GreaterThanOrEqual(lhs.Value, rhs.Value);

		public static bool operator <=(Number<T> lhs, Number<T> rhs) =>
			Operator.LessThanOrEqual(lhs.Value, rhs.Value);

		#endregion

		#region Bitwise Operators

		public static Number<T> operator !(Number<T> value) =>
			new Number<T>(Operator.Not(value.Value));

		public static Number<T> operator |(Number<T> lhs, Number<T> rhs) =>
			new Number<T>(Operator.Or(lhs.Value, rhs.Value));

		public static Number<T> operator &(Number<T> lhs, Number<T> rhs) =>
			new Number<T>(Operator.And(lhs.Value, rhs.Value));

		public static Number<T> operator ^(Number<T> lhs, Number<T> rhs) =>
			new Number<T>(Operator.Xor(lhs.Value, rhs.Value));

		#endregion

		#region Instance Members

		/// <inheritdoc />
		public int CompareTo(Number<T> other)
		{
			if (this == other)
			{
				return 0;
			}

			return this < other ? -1 : 1;
		}

		/// <inheritdoc />
		public bool Equals(Number<T> other) => this == other;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => this.Value.GetHashCode();

		/// <inheritdoc />
		public override string ToString() => this.Value.ToString();

		#endregion
	}
}
