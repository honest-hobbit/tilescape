﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Mathematics
{
	public static class BitMask
	{
		public static int CreateIntWithOnesInLowerBits(int count)
		{
			Require.That(count >= 0);
			Require.That(count <= 31);

			return (1 << count) - 1;
		}

		public static uint CreateUIntWithOnesInLowerBits(int count)
		{
			Require.That(count >= 0);
			Require.That(count <= 31);

			return (((uint)1) << count) - 1;
		}

		public static long CreateLongWithOnesInLowerBits(int count)
		{
			Require.That(count >= 0);
			Require.That(count <= 63);

			return (1L << count) - 1;
		}

		public static ulong CreateULongWithOnesInLowerBits(int count)
		{
			Require.That(count >= 0);
			Require.That(count <= 63);

			return (((ulong)1) << count) - 1;
		}
	}
}
