﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Mathematics
{
	/// <summary>
	/// Provides extension methods for <see cref="IEnumerable{T}"/> of numeric types.
	/// </summary>
	public static class EnumerableNumberExtensions
	{
		public static long SumLong(this IEnumerable<int> values)
		{
			Require.That(values != null);

			long result = 0;
			foreach (var value in values)
			{
				result += value;
			}

			return result;
		}

		public static int Multiply(this IEnumerable<int> values)
		{
			Require.That(values != null);

			int result = 1;
			foreach (var value in values)
			{
				result *= value;
			}

			return result;
		}

		public static long MultiplyLong(this IEnumerable<int> values)
		{
			Require.That(values != null);

			long result = 1;
			foreach (var value in values)
			{
				result *= value;
			}

			return result;
		}
	}
}
