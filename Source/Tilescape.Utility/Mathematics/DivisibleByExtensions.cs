﻿using System;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Mathematics
{
	public static class DivisibleByExtensions
	{
		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if even, false otherwise.
		/// </returns>
		public static bool IsEven(this byte value)
		{
			return value % 2 == 0;
		}

		/// <summary>
		/// Determines whether the specified value is odd.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if odd, false otherwise.
		/// </returns>
		public static bool IsOdd(this byte value)
		{
			return value % 2 != 0;
		}

		/// <summary>
		/// Determines whether the specified value is divisible by the given divisor.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="divisor">The divisor.</param>
		/// <returns>
		/// True if divisible by the divisor, false otherwise.
		/// </returns>
		public static bool IsDivisibleBy(this byte value, int divisor)
		{
			Require.That(divisor != 0);

			return value % divisor == 0;
		}

		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if even, false otherwise.
		/// </returns>
		public static bool IsEven(this sbyte value)
		{
			return value % 2 == 0;
		}

		/// <summary>
		/// Determines whether the specified value is odd.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if odd, false otherwise.
		/// </returns>
		public static bool IsOdd(this sbyte value)
		{
			return value % 2 != 0;
		}

		/// <summary>
		/// Determines whether the specified value is divisible by the given divisor.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="divisor">The divisor.</param>
		/// <returns>
		/// True if divisible by the divisor, false otherwise.
		/// </returns>
		public static bool IsDivisibleBy(this sbyte value, int divisor)
		{
			Require.That(divisor != 0);

			return value % divisor == 0;
		}

		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if even, false otherwise.
		/// </returns>
		public static bool IsEven(this short value)
		{
			return value % 2 == 0;
		}

		/// <summary>
		/// Determines whether the specified value is odd.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if odd, false otherwise.
		/// </returns>
		public static bool IsOdd(this short value)
		{
			return value % 2 != 0;
		}

		/// <summary>
		/// Determines whether the specified value is divisible by the given divisor.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="divisor">The divisor.</param>
		/// <returns>
		/// True if divisible by the divisor, false otherwise.
		/// </returns>
		public static bool IsDivisibleBy(this short value, int divisor)
		{
			Require.That(divisor != 0);

			return value % divisor == 0;
		}

		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if even, false otherwise.
		/// </returns>
		public static bool IsEven(this ushort value)
		{
			return value % 2 == 0;
		}

		/// <summary>
		/// Determines whether the specified value is odd.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if odd, false otherwise.
		/// </returns>
		public static bool IsOdd(this ushort value)
		{
			return value % 2 != 0;
		}

		/// <summary>
		/// Determines whether the specified value is divisible by the given divisor.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="divisor">The divisor.</param>
		/// <returns>
		/// True if divisible by the divisor, false otherwise.
		/// </returns>
		public static bool IsDivisibleBy(this ushort value, int divisor)
		{
			Require.That(divisor != 0);

			return value % divisor == 0;
		}

		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if even, false otherwise.
		/// </returns>
		public static bool IsEven(this int value)
		{
			return value % 2 == 0;
		}

		/// <summary>
		/// Determines whether the specified value is odd.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if odd, false otherwise.
		/// </returns>
		public static bool IsOdd(this int value)
		{
			return value % 2 != 0;
		}

		/// <summary>
		/// Determines whether the specified value is divisible by the given divisor.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="divisor">The divisor.</param>
		/// <returns>
		/// True if divisible by the divisor, false otherwise.
		/// </returns>
		public static bool IsDivisibleBy(this int value, int divisor)
		{
			Require.That(divisor != 0);

			return value % divisor == 0;
		}

		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if even, false otherwise.
		/// </returns>
		public static bool IsEven(this uint value)
		{
			return value % 2 == 0;
		}

		/// <summary>
		/// Determines whether the specified value is odd.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if odd, false otherwise.
		/// </returns>
		public static bool IsOdd(this uint value)
		{
			return value % 2 != 0;
		}

		/// <summary>
		/// Determines whether the specified value is divisible by the given divisor.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="divisor">The divisor.</param>
		/// <returns>
		/// True if divisible by the divisor, false otherwise.
		/// </returns>
		public static bool IsDivisibleBy(this uint value, int divisor)
		{
			Require.That(divisor != 0);

			return value % divisor == 0;
		}

		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if even, false otherwise.
		/// </returns>
		public static bool IsEven(this long value)
		{
			return value % 2 == 0;
		}

		/// <summary>
		/// Determines whether the specified value is odd.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if odd, false otherwise.
		/// </returns>
		public static bool IsOdd(this long value)
		{
			return value % 2 != 0;
		}

		/// <summary>
		/// Determines whether the specified value is divisible by the given divisor.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="divisor">The divisor.</param>
		/// <returns>
		/// True if divisible by the divisor, false otherwise.
		/// </returns>
		public static bool IsDivisibleBy(this long value, long divisor)
		{
			Require.That(divisor != 0);

			return value % divisor == 0;
		}

		/// <summary>
		/// Determines whether the specified value is even.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if even, false otherwise.
		/// </returns>
		public static bool IsEven(this ulong value)
		{
			return value % 2 == 0;
		}

		/// <summary>
		/// Determines whether the specified value is odd.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// True if odd, false otherwise.
		/// </returns>
		public static bool IsOdd(this ulong value)
		{
			return value % 2 != 0;
		}

		/// <summary>
		/// Determines whether the specified value is divisible by the given divisor.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="divisor">The divisor.</param>
		/// <returns>
		/// True if divisible by the divisor, false otherwise.
		/// </returns>
		public static bool IsDivisibleBy(this ulong value, long divisor)
		{
			Require.That(divisor != 0);

			// divisor must be unsigned in order for this to compile, and sign of divisor doesn't matter
			// for the % operator anyway so discarding sign should still produce the same results
			return value % (ulong)Math.Abs(divisor) == 0;
		}
	}
}
