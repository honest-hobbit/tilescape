﻿using System;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Mathematics
{
	/// <summary>
	/// Provides additional constants and static methods for trigonometric, logarithmic, and other common mathematical functions
	/// to be used along with the <see cref="Math"/> class.
	/// </summary>
	public static class MathUtility
	{
		public const double DegreesPerRadian = Math.PI / 180d;

		public const double RadiansPerDegrees = 180d / Math.PI;

		public static int CountDigits(int value)
		{
			if (value == 0)
			{
				return 0;
			}

			return (int)Math.Floor(Math.Log10(value.Abs()) + 1);
		}

		// round up means towards the greater number
		// round down means towards the lesser number
		// this applies for negatives as well
		public static int IntegerMidpoint(int value1, int value2, bool roundUp = false)
		{
			int sum = value1 + value2;
			var result = sum / 2;

			if (sum.IsOdd())
			{
				if (sum > 0)
				{
					if (roundUp)
					{
						result++;
					}
				}
				else
				{
					if (!roundUp)
					{
						result--;
					}
				}
			}

			return result;
		}

		#region Powers

		public static bool IsPerfectSquare(int number)
		{
			Require.That(number >= 0);

			return Math.Sqrt(number) % 1 == 0;
		}

		public static int PowerOf2(int exponent)
		{
			Require.That(exponent >= 0);

			return 1 << exponent;
		}

		public static int IntegerPower(int baseNumber, int exponent)
		{
			Require.That(exponent >= 0);

			int result = 1;
			for (int count = 0; count < exponent; count++)
			{
				result *= baseNumber;
			}

			return result;
		}

		public static long LongPower(long baseNumber, int exponent)
		{
			Require.That(exponent >= 0);

			long result = 1;
			for (int count = 0; count < exponent; count++)
			{
				result *= baseNumber;
			}

			return result;
		}

		#endregion

		#region Divide

		public static int Divide(int value, int divisor, out int remainder)
		{
			Require.That(divisor != 0);

			// Math.DivRem is horribly named and not actually optimized by the JIT compiler yet
			// so this is a slightly optimized alternative
			// https://msdn.microsoft.com/en-us/library/system.math.divrem(v=vs.110).aspx
			// http://stackoverflow.com/questions/6988414/difference-between-math-divrem-and-operator
			int quotient = value / divisor;
			remainder = value - (divisor * quotient);
			return quotient;
		}

		public static long Divide(long value, long divisor, out long remainder)
		{
			Require.That(divisor != 0);

			// Math.DivRem is horribly named and not actually optimized by the JIT compiler yet
			// so this is a slightly optimized alternative
			// https://msdn.microsoft.com/en-us/library/system.math.divrem(v=vs.110).aspx
			// http://stackoverflow.com/questions/6988414/difference-between-math-divrem-and-operator
			long quotient = value / divisor;
			remainder = value - (divisor * quotient);
			return quotient;
		}

		public static int DivideRoundUp(int value, int divisor)
		{
			Require.That(value >= 0);
			Require.That(divisor >= 1);

			return (value + divisor - 1) / divisor;
		}

		public static long DivideRoundUp(long value, long divisor)
		{
			Require.That(value >= 0);
			Require.That(divisor >= 1);

			return (value + divisor - 1) / divisor;
		}

		#endregion

		#region ActualModulo

		/// <summary>
		/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
		/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="mod">The modulo to divide by.</param>
		/// <returns>The result of the modulo division.</returns>
		/// <remarks>
		/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
		/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
		/// for more information about modulo in C#.
		/// </remarks>
		public static int ActualModulo(int value, int mod)
		{
			Require.That(mod != 0);

			mod = Math.Abs(mod);
			int result = value % mod;
			return result < 0 ? result + mod : result;
		}

		/// <summary>
		/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
		/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="mod">The modulo to divide by.</param>
		/// <returns>The result of the modulo division.</returns>
		/// <remarks>
		/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
		/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
		/// for more information about modulo in C#.
		/// </remarks>
		public static long ActualModulo(long value, long mod)
		{
			Require.That(mod != 0);

			mod = Math.Abs(mod);
			long result = value % mod;
			return result < 0 ? result + mod : result;
		}

		/// <summary>
		/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
		/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="mod">The modulo to divide by.</param>
		/// <returns>The result of the modulo division.</returns>
		/// <remarks>
		/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
		/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
		/// for more information about modulo in C#.
		/// </remarks>
		public static float ActualModulo(float value, float mod)
		{
			Require.That(mod != 0);

			mod = Math.Abs(mod);
			float result = value % mod;
			return result < 0 ? result + mod : result;
		}

		/// <summary>
		/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
		/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="mod">The modulo to divide by.</param>
		/// <returns>The result of the modulo division.</returns>
		/// <remarks>
		/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
		/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
		/// for more information about modulo in C#.
		/// </remarks>
		public static double ActualModulo(double value, double mod)
		{
			Require.That(mod != 0);

			mod = Math.Abs(mod);
			double result = value % mod;
			return result < 0 ? result + mod : result;
		}

		/// <summary>
		/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
		/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="mod">The modulo to divide by.</param>
		/// <returns>The result of the modulo division.</returns>
		/// <remarks>
		/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
		/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
		/// for more information about modulo in C#.
		/// </remarks>
		public static decimal ActualModulo(decimal value, decimal mod)
		{
			Require.That(mod != 0);

			mod = Math.Abs(mod);
			decimal result = value % mod;
			return result < 0 ? result + mod : result;
		}

		#endregion

		#region Degrees/Radians

		public static double DegreesToRadians(double degrees)
		{
			return degrees * DegreesPerRadian;
		}

		public static double RadiansToDegrees(double radians)
		{
			return radians * RadiansPerDegrees;
		}

		#endregion
	}
}
