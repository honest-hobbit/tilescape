﻿using MiscUtil;

namespace Tilescape.Utility.Mathematics
{
	public static class Number
	{
		public static Number<T> Zero<T>() => new Number<T>(Operator<T>.Zero);

		public static Number<T> New<T>(T value) => new Number<T>(value);
	}
}
