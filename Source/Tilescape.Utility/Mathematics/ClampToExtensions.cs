﻿namespace Tilescape.Utility.Mathematics
{
	public static class ClampToExtensions
	{
		public static byte ClampToByte(this int value) => (byte)value.Clamp(byte.MinValue, byte.MaxValue);

		public static byte ClampToByte(this long value) => (byte)value.Clamp(byte.MinValue, byte.MaxValue);

		public static byte ClampToByte(this float value) => (byte)value.Clamp(byte.MinValue, byte.MaxValue);

		public static byte ClampToByte(this double value) => (byte)value.Clamp(byte.MinValue, byte.MaxValue);

		public static sbyte ClampToSByte(this int value) => (sbyte)value.Clamp(sbyte.MinValue, sbyte.MaxValue);

		public static sbyte ClampToSByte(this long value) => (sbyte)value.Clamp(sbyte.MinValue, sbyte.MaxValue);

		public static sbyte ClampToSByte(this float value) => (sbyte)value.Clamp(sbyte.MinValue, sbyte.MaxValue);

		public static sbyte ClampToSByte(this double value) => (sbyte)value.Clamp(sbyte.MinValue, sbyte.MaxValue);

		public static short ClampToShort(this int value) => (short)value.Clamp(short.MinValue, short.MaxValue);

		public static short ClampToShort(this long value) => (short)value.Clamp(short.MinValue, short.MaxValue);

		public static short ClampToShort(this float value) => (short)value.Clamp(short.MinValue, short.MaxValue);

		public static short ClampToShort(this double value) => (short)value.Clamp(short.MinValue, short.MaxValue);

		public static ushort ClampToUShort(this int value) => (ushort)value.Clamp(ushort.MinValue, ushort.MaxValue);

		public static ushort ClampToUShort(this long value) => (ushort)value.Clamp(ushort.MinValue, ushort.MaxValue);

		public static ushort ClampToUShort(this float value) => (ushort)value.Clamp(ushort.MinValue, ushort.MaxValue);

		public static ushort ClampToUShort(this double value) => (ushort)value.Clamp(ushort.MinValue, ushort.MaxValue);

		public static int ClampToInt(this long value) => (int)value.Clamp(int.MinValue, int.MaxValue);

		public static int ClampToInt(this float value) => (int)value.Clamp(int.MinValue, int.MaxValue);

		public static int ClampToInt(this double value) => (int)value.Clamp(int.MinValue, int.MaxValue);

		public static uint ClampToUInt(this long value) => (uint)value.Clamp(uint.MinValue, uint.MaxValue);

		public static uint ClampToUInt(this float value) => (uint)value.Clamp(uint.MinValue, uint.MaxValue);

		public static uint ClampToUInt(this double value) => (uint)value.Clamp(uint.MinValue, uint.MaxValue);
	}
}
