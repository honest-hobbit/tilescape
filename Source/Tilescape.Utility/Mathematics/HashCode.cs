﻿using Tilescape.Utility.Types;

namespace Tilescape.Utility.Mathematics
{
	/// <summary>
	/// The preferred order for trying these hash code functions is;
	/// <see cref="OneAtATime"/>, <see cref="FNV"/>, <see cref="ELF"/>, <see cref="Bernstein"/>, <see cref="ShiftAddXor"/>.
	/// </summary>
	/// <seealso href="http://stackoverflow.com/questions/1646807/quick-and-simple-hash-code-combinations"/>
	/// <seealso href="http://www.eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx"/>
	public static class HashCode
	{
		public static BernsteinHash Bernstein => new BernsteinHash(17);

		public static ShiftAddXorHash ShiftAddXor => new ShiftAddXorHash(0);

		public static FNVHash FNV => new FNVHash(2166136261);

		public static OneAtATimeHash OneAtATime => new OneAtATimeHash(0);

		public static ELFHash ELF => new ELFHash(0);

		public static OneAtATimeHash Start<T>(T value) => OneAtATime.And(value);

		public struct BernsteinHash : IHash<BernsteinHash>
		{
			private readonly uint hash;

			public BernsteinHash(uint hash)
			{
				this.hash = hash;
			}

			public BernsteinHash(int hash)
				: this(unchecked((uint)hash))
			{
			}

			/// <inheritdoc />
			public int Result
			{
				get
				{
					return unchecked((int)this.hash);
				}
			}

			public static implicit operator int(BernsteinHash value)
			{
				return value.Result;
			}

			/// <inheritdoc />
			public BernsteinHash And<T>(T value)
			{
				unchecked
				{
					uint hash = this.hash;

					// optimized way to perform currentHash *= 31
					hash <<= 5;
					hash--;

					hash ^= (uint)value.GetHashCodeNullSafe();
					return new BernsteinHash(hash);
				}
			}
		}

		public struct ELFHash : IHash<ELFHash>
		{
			private readonly uint hash;

			public ELFHash(uint hash)
			{
				this.hash = hash;
			}

			public ELFHash(int hash)
				: this(unchecked((uint)hash))
			{
			}

			/// <inheritdoc />
			public int Result
			{
				get
				{
					return unchecked((int)this.hash);
				}
			}

			public static implicit operator int(ELFHash value)
			{
				return value.Result;
			}

			/// <inheritdoc />
			public ELFHash And<T>(T value)
			{
				unchecked
				{
					uint hash = this.hash;

					hash = (hash << 4) + (uint)value.GetHashCodeNullSafe();
					uint g = hash & 0xf0000000;

					if (g != 0)
					{
						hash ^= g >> 24;
					}

					hash &= ~g;

					return new ELFHash(hash);
				}
			}
		}

		public struct FNVHash : IHash<FNVHash>
		{
			private readonly uint hash;

			public FNVHash(uint hash)
			{
				this.hash = hash;
			}

			public FNVHash(int hash)
				: this(unchecked((uint)hash))
			{
			}

			/// <inheritdoc />
			public int Result
			{
				get
				{
					return unchecked((int)this.hash);
				}
			}

			public static implicit operator int(FNVHash value)
			{
				return value.Result;
			}

			/// <inheritdoc />
			public FNVHash And<T>(T value)
			{
				unchecked
				{
					uint hash = this.hash;
					hash = (hash * 16777619) ^ (uint)value.GetHashCodeNullSafe();
					return new FNVHash(hash);
				}
			}
		}

		public struct OneAtATimeHash : IHash<OneAtATimeHash>
		{
			private readonly uint hash;

			public OneAtATimeHash(uint hash)
			{
				this.hash = hash;
			}

			public OneAtATimeHash(int hash)
				: this(unchecked((uint)hash))
			{
			}

			/// <inheritdoc />
			public int Result
			{
				get
				{
					unchecked
					{
						uint hash = this.hash;
						hash += hash << 3;
						hash ^= hash >> 11;
						hash += hash << 15;
						return (int)hash;
					}
				}
			}

			public static implicit operator int(OneAtATimeHash value)
			{
				return value.Result;
			}

			/// <inheritdoc />
			public OneAtATimeHash And<T>(T value)
			{
				unchecked
				{
					uint hash = this.hash;
					hash += (uint)value.GetHashCodeNullSafe();
					hash += hash << 10;
					hash ^= hash >> 6;
					return new OneAtATimeHash(hash);
				}
			}
		}

		public struct ShiftAddXorHash : IHash<ShiftAddXorHash>
		{
			private readonly uint hash;

			public ShiftAddXorHash(uint hash)
			{
				this.hash = hash;
			}

			public ShiftAddXorHash(int hash)
				: this(unchecked((uint)hash))
			{
			}

			/// <inheritdoc />
			public int Result
			{
				get
				{
					return unchecked((int)this.hash);
				}
			}

			public static implicit operator int(ShiftAddXorHash value)
			{
				return value.Result;
			}

			/// <inheritdoc />
			public ShiftAddXorHash And<T>(T value)
			{
				unchecked
				{
					uint hash = this.hash;
					hash ^= (hash << 5) + (hash >> 2) + (uint)value.GetHashCodeNullSafe();
					return new ShiftAddXorHash(hash);
				}
			}
		}
	}
}
