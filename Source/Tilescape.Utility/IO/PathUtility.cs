﻿using System;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.IO
{
	public static class PathUtility
	{
		public static bool IsPathValid(string pathString)
		{
			if (pathString.IsNullOrWhiteSpace())
			{
				return false;
			}

			bool isValidUri = Uri.TryCreate(pathString, UriKind.Absolute, out var pathUri);
			return isValidUri && (pathUri?.IsLoopback ?? false);
		}
	}
}
