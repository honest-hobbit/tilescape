﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Shapes
{
	public interface IIndexShape<TIndex> : ISetView<TIndex>, IIndexingBounds<TIndex>
		where TIndex : struct, IIndex<TIndex>
	{
	}
}
