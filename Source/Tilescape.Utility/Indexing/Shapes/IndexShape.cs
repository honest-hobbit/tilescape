﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Shapes
{
	public static class IndexShape
	{
		public static IndexCircle CreateCircle(int diameter) => IndexCircle.Create(diameter);

		public static IndexSquare CreateSquare(int length) => IndexSquare.Create(length);

		public static IndexSphere CreateSphere(int diameter) => IndexSphere.Create(diameter);

		public static IndexColumn CreateCube(int length) => IndexColumn.CreateCube(length);

		public static IndexColumn CreateColumn(int length, int height) => IndexColumn.Create(length, height);

		public static IndexCylinder CreateCylinder(int diameter, int height) => IndexCylinder.Create(diameter, height);

		public static IIndexShape<Index2D> Create(Shape2D shape, int length)
		{
			Require.That(Enumeration.IsDefined(shape));
			Require.That(length >= 1);

			switch (shape)
			{
				case Shape2D.Circle: return CreateCircle(length);
				case Shape2D.Square: return CreateSquare(length);
				default: throw InvalidEnumArgument.CreateException(nameof(shape), shape);
			}
		}

		public static IIndexShape<Index3D> Create(Shape3D shape, int length)
		{
			Require.That(Enumeration.IsDefined(shape));
			Require.That(length >= 1);

			switch (shape)
			{
				case Shape3D.Column: return CreateColumn(length, length);
				case Shape3D.Cylinder: return CreateCylinder(length, length);
				case Shape3D.Sphere: return CreateSphere(length);
				default: throw InvalidEnumArgument.CreateException(nameof(shape), shape);
			}
		}
	}
}
