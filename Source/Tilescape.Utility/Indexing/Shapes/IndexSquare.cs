﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Utility.Indexing.Shapes
{
	/// <summary>
	///
	/// </summary>
	public abstract class IndexSquare : EquidimensionalIndexShape<Index2D>
	{
		private IndexSquare(int length)
			: base(length)
		{
		}

		public new int Length => base.Length;

		/// <inheritdoc />
		public override int Count => this.Length * this.Length;

		public static bool operator ==(IndexSquare lhs, IndexSquare rhs) => lhs.Equals(rhs);

		public static bool operator !=(IndexSquare lhs, IndexSquare rhs) => !lhs.Equals(rhs);

		public static IndexSquare Create(int length)
		{
			Require.That(length >= 1);

			return length.IsEven() ? (IndexSquare)new EvenSquare(length) : new OddSquare(length);
		}

		/// <inheritdoc />
		public override bool Equals(object obj) => base.Equals(obj);

		/// <inheritdoc />
		public override int GetHashCode() => base.GetHashCode();

		/// <inheritdoc />
		public override bool Contains(Index2D index) => this.IsIndexInBounds(index);

		private class OddSquare : IndexSquare
		{
			public OddSquare(int length)
				: base(length)
			{
			}

			/// <inheritdoc />
			public override IEnumerator<Index2D> GetEnumerator() => ShapeIndices.Square.RangeOdd(this.Layers).GetEnumerator();
		}

		private class EvenSquare : IndexSquare
		{
			public EvenSquare(int length)
				: base(length)
			{
			}

			/// <inheritdoc />
			public override IEnumerator<Index2D> GetEnumerator() => ShapeIndices.Square.RangeEven(this.Layers).GetEnumerator();
		}
	}
}
