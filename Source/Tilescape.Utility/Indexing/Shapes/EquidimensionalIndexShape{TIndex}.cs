﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Shapes
{
	public abstract class EquidimensionalIndexShape<TIndex> : IIndexShape<TIndex>
		where TIndex : struct, IIndex<TIndex>
	{
		protected EquidimensionalIndexShape(int length)
		{
			Require.That(length >= 1);

			this.Dimensions = Index.Create<TIndex>(length);
			this.LowerBounds = Index.Create<TIndex>(-length / 2);
			this.UpperBounds = (TIndex)(Number.New(this.LowerBounds) + this.Dimensions - Index.One<TIndex>());
		}

		/// <inheritdoc />
		public abstract int Count { get; }

		/// <inheritdoc />
		public TIndex Dimensions { get; }

		/// <inheritdoc />
		public TIndex LowerBounds { get; }

		/// <inheritdoc />
		public TIndex UpperBounds { get; }

		protected int Layers => MathUtility.DivideRoundUp(this.Length, 2);

		protected int Length => this.Dimensions[0];

		public static bool operator ==(EquidimensionalIndexShape<TIndex> lhs, EquidimensionalIndexShape<TIndex> rhs) => lhs.Equals(rhs);

		public static bool operator !=(EquidimensionalIndexShape<TIndex> lhs, EquidimensionalIndexShape<TIndex> rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			if (!this.TypesEqualNullSafe(obj))
			{
				return false;
			}

			var other = (EquidimensionalIndexShape<TIndex>)obj;
			return other.Length == this.Length;
		}

		/// <inheritdoc />
		public override int GetHashCode() => this.Length.GetHashCode();

		/// <inheritdoc />
		public abstract bool Contains(TIndex index);

		/// <inheritdoc />
		public abstract IEnumerator<TIndex> GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
