﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Indices
{
	public struct DimensionCoordinatePair : IEquatable<DimensionCoordinatePair>
	{
		public DimensionCoordinatePair(int dimension, int coordinate)
		{
			Require.That(dimension >= 0);

			this.Dimension = dimension;
			this.Coordinate = coordinate;
		}

		public int Dimension { get; }

		public int Coordinate { get; }

		/// <inheritdoc />
		public bool Equals(DimensionCoordinatePair other) => this.Dimension == other.Dimension && this.Coordinate == other.Coordinate;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => HashCode.Start(this.Dimension).And(this.Coordinate);

		/// <inheritdoc />
		public override string ToString() => $"[Dimension {this.Dimension.ToString()}: {this.Coordinate.ToString()}]";
	}
}
