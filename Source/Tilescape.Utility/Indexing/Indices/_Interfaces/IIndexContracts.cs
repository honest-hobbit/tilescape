﻿using System.Diagnostics;
using System.Linq;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Indexing.Indices
{
	public static class IIndexContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void IsIn<TIndex>(TIndex min, TIndex max)
			where TIndex : struct, IIndex<TIndex>
		{
			Require.That(min.All(pair => pair.Coordinate <= max[pair.Dimension]));
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Divide<TIndex>(TIndex divisor)
			where TIndex : struct, IIndex<TIndex>
		{
			Require.That(divisor.Coordinates().All(value => value != 0));
		}

		[Conditional(Require.CompilationSymbol)]
		public static void DivideRoundUp<TIndex>(TIndex instance, TIndex divisor)
			where TIndex : struct, IIndex<TIndex>
		{
			Require.That(instance.Coordinates().All(value => value >= 0));
			Require.That(divisor.Coordinates().All(value => value >= 1));
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Range<TIndex>(TIndex dimensions)
			where TIndex : struct, IIndex<TIndex>
		{
			Require.That(dimensions.Coordinates().All(value => value >= 0));
		}
	}
}
