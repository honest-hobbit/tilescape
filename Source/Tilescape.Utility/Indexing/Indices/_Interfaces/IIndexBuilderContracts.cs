﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Indexing.Indices
{
	public static class IIndexBuilderContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<TIndex>(IIndexBuilder<TIndex> instance, int dimension)
			where TIndex : struct, IIndex<TIndex>
		{
			ICoordinatesContracts.Indexer(instance, dimension);
		}
	}
}
