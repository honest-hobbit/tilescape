﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Indexing.Indices
{
	public static class ICoordinatesContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer(ICoordinates instance, int dimension)
		{
			Require.That(instance != null);
			Require.That(dimension >= 0 && dimension < instance.Rank);
		}
	}
}
