﻿using System;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class IndexableInlineSerializer<TIndex, TValue> : IInlineSerDes<IIndexable<TIndex, TValue>>
		where TIndex : struct, IIndex<TIndex>
	{
		private readonly ISerializer<IIndexableView<TIndex, TValue>> serializer;

		private readonly IInlineDeserializer<IIndexable<TIndex, TValue>> deserializer;

		public IndexableInlineSerializer(IUniformSerDes<TValue> valueSerializer, IndexableSerializerOptions<TIndex> options)
		{
			this.serializer = new IndexableViewSerializer<TIndex, TValue>(valueSerializer, options);
			this.deserializer = new IndexableInlineDeserializer<TIndex, TValue>(valueSerializer, options);
		}

		public int GetSerializedLength(IIndexable<TIndex, TValue> value) =>
			this.serializer.GetSerializedLength(value);

		public int Serialize(IIndexable<TIndex, TValue> value, Action<byte> writeByte) =>
			this.serializer.Serialize(value, writeByte);

		public int Serialize(IIndexable<TIndex, TValue> value, byte[] buffer, ref int index) =>
			this.serializer.Serialize(value, buffer, ref index);

		public void DeserializeInline(Func<Try<byte>> readByte, IIndexable<TIndex, TValue> result) =>
			this.deserializer.DeserializeInline(readByte, result);

		public void DeserializeInline(byte[] buffer, ref int index, IIndexable<TIndex, TValue> result) =>
			this.deserializer.DeserializeInline(buffer, ref index, result);
	}
}
