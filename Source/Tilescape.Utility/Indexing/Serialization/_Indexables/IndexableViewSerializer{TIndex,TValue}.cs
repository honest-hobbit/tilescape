﻿using System;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Serialization;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class IndexableViewSerializer<TIndex, TValue> :
		AbstractIndexableSerializer<TIndex, TValue>, ISerializer<IIndexableView<TIndex, TValue>>
		where TIndex : struct, IIndex<TIndex>
	{
		public IndexableViewSerializer(IUniformSerDes<TValue> valueSerializer, IndexableSerializerOptions<TIndex> options)
			: base(valueSerializer, options)
		{
		}

		/// <inheritdoc />
		public int GetSerializedLength(IIndexableView<TIndex, TValue> indexable)
		{
			ISerializerContracts.GetSerializedLength(indexable);
			this.AdditionalIndexableContracts(indexable);

			int result = this.DimensionsSerializer.SerializedLength;
			result += this.LowerBoundsSerializer.SerializedLength;
			result += indexable.Dimensions.Coordinates().Multiply() * this.ValueSerializer.SerializedLength;
			return result;
		}

		/// <inheritdoc />
		public int Serialize(IIndexableView<TIndex, TValue> indexable, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, indexable, buffer, index);
			this.AdditionalIndexableContracts(indexable);

			int originalArrayIndex = index;

			this.DimensionsSerializer.Serialize(indexable.Dimensions, buffer, ref index);
			this.LowerBoundsSerializer.Serialize(indexable.LowerBounds, buffer, ref index);

			foreach (var valueIndex in indexable.LowerBounds.Range(indexable.Dimensions))
			{
				this.ValueSerializer.Serialize(indexable[valueIndex], buffer, ref index);
			}

			return index - originalArrayIndex;
		}

		/// <inheritdoc />
		public int Serialize(IIndexableView<TIndex, TValue> indexable, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(indexable, writeByte);
			this.AdditionalIndexableContracts(indexable);

			int serializedLength = this.DimensionsSerializer.Serialize(indexable.Dimensions, writeByte);
			serializedLength += this.LowerBoundsSerializer.Serialize(indexable.LowerBounds, writeByte);

			foreach (var valueIndex in indexable.LowerBounds.Range(indexable.Dimensions))
			{
				serializedLength += this.ValueSerializer.Serialize(indexable[valueIndex], writeByte);
			}

			return serializedLength;
		}
	}
}
