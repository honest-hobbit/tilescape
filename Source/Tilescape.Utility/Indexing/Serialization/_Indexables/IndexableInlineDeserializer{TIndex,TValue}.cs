﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class IndexableInlineDeserializer<TIndex, TValue> :
		AbstractIndexableDeserializer<TIndex, TValue>, IInlineDeserializer<IIndexable<TIndex, TValue>>
		where TIndex : struct, IIndex<TIndex>
	{
		public IndexableInlineDeserializer(IUniformSerDes<TValue> valueSerializer, IndexableSerializerOptions<TIndex> options)
			: base(valueSerializer, options)
		{
		}

		/// <inheritdoc />
		public void DeserializeInline(byte[] buffer, ref int index, IIndexable<TIndex, TValue> result)
		{
			IInlineDeserializerContracts.DeserializeInline(buffer, index, result);
			this.AdditionalIndexableContracts(result);

			var dimensions = this.DimensionsSerializer.Deserialize(buffer, ref index);
			var lowerBounds = this.LowerBoundsSerializer.Deserialize(buffer, ref index);

			this.AdditionalDeserializeInlineContracts(result, dimensions, lowerBounds);

			int localIndex = index;
			foreach (var pair in this.DeserializeValues(
				lowerBounds, dimensions, buffer, index, resultIndex => localIndex = resultIndex))
			{
				result[pair.Index] = pair.Value;
			}

			index = localIndex;
		}

		/// <inheritdoc />
		public void DeserializeInline(Func<Try<byte>> readByte, IIndexable<TIndex, TValue> result)
		{
			IInlineDeserializerContracts.DeserializeInline(readByte, result);
			this.AdditionalIndexableContracts(result);

			var dimensions = this.DimensionsSerializer.Deserialize(readByte);
			var lowerBounds = this.LowerBoundsSerializer.Deserialize(readByte);

			this.AdditionalDeserializeInlineContracts(result, dimensions, lowerBounds);

			foreach (var pair in this.DeserializeValues(lowerBounds, dimensions, readByte))
			{
				result[pair.Index] = pair.Value;
			}
		}

		[Conditional(Require.CompilationSymbol)]
		protected void AdditionalDeserializeInlineContracts(
			IIndexable<TIndex, TValue> indexable, TIndex dimensions, TIndex lowerBounds)
		{
			Require.That(indexable != null);
			Require.That(indexable.Dimensions.Equals(dimensions));
			Require.That(indexable.LowerBounds.Equals(lowerBounds));
		}
	}
}
