﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class IndexableDeserializer<TIndex, TValue, TIndexable> :
		AbstractIndexableDeserializer<TIndex, TValue>, IDeserializer<TIndexable>
		where TIndex : struct, IIndex<TIndex>
	{
		private readonly Func<IndexableConstructorArgs, TIndexable> factory;

		public IndexableDeserializer(
			IUniformSerDes<TValue> valueSerializer,
			IndexableSerializerOptions<TIndex> options,
			Func<IndexableConstructorArgs, TIndexable> factory)
			: base(valueSerializer, options)
		{
			Require.That(factory != null);

			this.factory = factory;
		}

		/// <inheritdoc />
		public TIndexable Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			var dimensions = this.DimensionsSerializer.Deserialize(buffer, ref index);
			var lowerBounds = this.LowerBoundsSerializer.Deserialize(buffer, ref index);

			int localIndex = index;
			var values = this.DeserializeValues(lowerBounds, dimensions, buffer, index, resultIndex => localIndex = resultIndex);

			var result = this.factory(new IndexableConstructorArgs(lowerBounds, dimensions, values));
			index = localIndex;
			return result;
		}

		/// <inheritdoc />
		public TIndexable Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			var dimensions = this.DimensionsSerializer.Deserialize(readByte);
			var lowerBounds = this.LowerBoundsSerializer.Deserialize(readByte);
			var values = this.DeserializeValues(lowerBounds, dimensions, readByte);

			return this.factory(new IndexableConstructorArgs(lowerBounds, dimensions, values));
		}

		public struct IndexableConstructorArgs
		{
			internal IndexableConstructorArgs(TIndex? lowerBounds, TIndex dimensions, IEnumerable<IndexValuePair<TIndex, TValue>> values)
			{
				Require.That(values != null);

				this.LowerBounds = lowerBounds;
				this.Dimensions = dimensions;
				this.Values = values;
			}

			public TIndex? LowerBounds { get; }

			public TIndex Dimensions { get; }

			public IEnumerable<IndexValuePair<TIndex, TValue>> Values { get; }
		}
	}
}
