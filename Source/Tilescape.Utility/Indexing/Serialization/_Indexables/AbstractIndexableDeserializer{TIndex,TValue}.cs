﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Serialization
{
	public abstract class AbstractIndexableDeserializer<TIndex, TValue> : AbstractIndexableSerializer<TIndex, TValue>
		where TIndex : struct, IIndex<TIndex>
	{
		public AbstractIndexableDeserializer(IUniformSerDes<TValue> valueSerializer, IndexableSerializerOptions<TIndex> options)
			: base(valueSerializer, options)
		{
		}

		protected IEnumerable<IndexValuePair<TIndex, TValue>> DeserializeValues(
			TIndex lowerBounds, TIndex dimensions, byte[] buffer, int index, Action<int> setIndex)
		{
			Require.That(buffer != null);
			Require.That(setIndex != null);

			return YieldDeserializeValues();
			IEnumerable<IndexValuePair<TIndex, TValue>> YieldDeserializeValues()
			{
				foreach (var valueIndex in lowerBounds.Range(dimensions))
				{
					yield return IndexValuePair.New(valueIndex, this.ValueSerializer.Deserialize(buffer, ref index));
				}

				setIndex(index);
			}
		}

		protected IEnumerable<IndexValuePair<TIndex, TValue>> DeserializeValues(
			TIndex lowerBounds, TIndex dimensions, Func<Try<byte>> readByte)
		{
			Require.That(readByte != null);

			return YieldDeserializeValues();
			IEnumerable<IndexValuePair<TIndex, TValue>> YieldDeserializeValues()
			{
				foreach (var valueIndex in lowerBounds.Range(dimensions))
				{
					yield return IndexValuePair.New(valueIndex, this.ValueSerializer.Deserialize(readByte));
				}
			}
		}
	}
}
