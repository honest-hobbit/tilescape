﻿using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class IndexableSerializerOptions<TIndex>
		where TIndex : struct, IIndex<TIndex>
	{
		public IndexableSerializerOptions(IUniformSerDes<TIndex> indexSerializer)
		{
			Require.That(indexSerializer != null);

			this.LowerBoundsSerializer = indexSerializer;
			this.DimensionsSerializer = indexSerializer;
			this.ConstantLowerBounds = null;
			this.ConstantDimensions = null;
		}

		public IndexableSerializerOptions(IUniformSerDes<TIndex> lowerBoundsSerializer, TIndex dimensions)
		{
			Require.That(lowerBoundsSerializer != null);
			Require.That(dimensions.Coordinates().All(value => value >= 0));

			this.LowerBoundsSerializer = lowerBoundsSerializer;
			this.DimensionsSerializer = new ConstantValueSerializer<TIndex>(dimensions);
			this.ConstantLowerBounds = null;
			this.ConstantDimensions = dimensions;
		}

		public IndexableSerializerOptions(TIndex lowerBounds, IUniformSerDes<TIndex> dimensionsSerializer)
		{
			Require.That(dimensionsSerializer != null);

			this.LowerBoundsSerializer = new ConstantValueSerializer<TIndex>(lowerBounds);
			this.DimensionsSerializer = dimensionsSerializer;
			this.ConstantLowerBounds = lowerBounds;
			this.ConstantDimensions = null;
		}

		public IndexableSerializerOptions(TIndex lowerBounds, TIndex dimensions)
		{
			Require.That(dimensions.Coordinates().All(value => value >= 0));

			this.LowerBoundsSerializer = new ConstantValueSerializer<TIndex>(lowerBounds);
			this.DimensionsSerializer = new ConstantValueSerializer<TIndex>(dimensions);
			this.ConstantLowerBounds = lowerBounds;
			this.ConstantDimensions = dimensions;
		}

		public IUniformSerDes<TIndex> LowerBoundsSerializer { get; }

		public IUniformSerDes<TIndex> DimensionsSerializer { get; }

		public TIndex? ConstantLowerBounds { get; }

		public TIndex? ConstantDimensions { get; }
	}
}
