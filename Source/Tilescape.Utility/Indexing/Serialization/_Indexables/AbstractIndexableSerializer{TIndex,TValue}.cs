﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;

namespace Tilescape.Utility.Indexing.Serialization
{
	public abstract class AbstractIndexableSerializer<TIndex, TValue>
		where TIndex : struct, IIndex<TIndex>
	{
		private readonly IndexableSerializerOptions<TIndex> options;

		public AbstractIndexableSerializer(IUniformSerDes<TValue> valueSerializer, IndexableSerializerOptions<TIndex> options)
		{
			Require.That(valueSerializer != null);
			Require.That(options != null);

			this.ValueSerializer = valueSerializer;
			this.options = options;
		}

		protected IUniformSerDes<TIndex> LowerBoundsSerializer => this.options.LowerBoundsSerializer;

		protected IUniformSerDes<TIndex> DimensionsSerializer => this.options.DimensionsSerializer;

		protected IUniformSerDes<TValue> ValueSerializer { get; }

		[Conditional(Require.CompilationSymbol)]
		protected void AdditionalIndexableContracts(IIndexableView<TIndex, TValue> indexable)
		{
			Require.That(indexable != null);

			var constantLowerBounds = this.options.ConstantLowerBounds;
			Require.That(
				constantLowerBounds.HasValue ? indexable.LowerBounds.Equals(constantLowerBounds.Value) : true,
				() => $"Lower bounds must be {constantLowerBounds.Value.ToString()} because constant lower bounds have been set.");

			var constantDimensions = this.options.ConstantDimensions;
			Require.That(
				constantDimensions.HasValue ? indexable.Dimensions.Equals(constantDimensions.Value) : true,
				() => $"Dimensions must be {constantDimensions.Value.ToString()} because constant dimensions have been set.");
		}
	}
}
