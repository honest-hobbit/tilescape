﻿using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class Index3DSerializer : AbstractCompositeUniformSerializer<Index3D, int, int, int>
	{
		public Index3DSerializer(IUniformSerDes<int> serializer)
			: base(serializer, serializer, serializer)
		{
		}

		public Index3DSerializer(IUniformSerDes<int> xSerializer, IUniformSerDes<int> ySerializer, IUniformSerDes<int> zSerializer)
			: base(xSerializer, ySerializer, zSerializer)
		{
		}

		public static IUniformSerDes<Index3D> Instance { get; } = new Index3DSerializer(Serializer.Int);

		/// <inheritdoc />
		protected override Index3D ComposeValue(int x, int y, int z) => new Index3D(x, y, z);

		/// <inheritdoc />
		protected override void DecomposeValue(Index3D value, out int x, out int y, out int z)
		{
			x = value.X;
			y = value.Y;
			z = value.Z;
		}
	}
}
