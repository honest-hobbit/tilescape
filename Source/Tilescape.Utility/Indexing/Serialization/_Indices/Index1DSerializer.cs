﻿using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class Index1DSerializer : AbstractCompositeUniformSerializer<Index1D, int>
	{
		public Index1DSerializer(IUniformSerDes<int> serializer)
			: base(serializer)
		{
		}

		public static IUniformSerDes<Index1D> Instance { get; } = new Index1DSerializer(Serializer.Int);

		/// <inheritdoc />
		protected override Index1D ComposeValue(int x) => new Index1D(x);

		/// <inheritdoc />
		protected override int DecomposeValue(Index1D value) => value.X;
	}
}
