﻿using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class Index4DSerializer : AbstractCompositeUniformSerializer<Index4D, int, int, int, int>
	{
		public Index4DSerializer(IUniformSerDes<int> serializer)
			: base(serializer, serializer, serializer, serializer)
		{
		}

		public Index4DSerializer(
			IUniformSerDes<int> xSerializer,
			IUniformSerDes<int> ySerializer,
			IUniformSerDes<int> zSerializer,
			IUniformSerDes<int> wSerializer)
			: base(xSerializer, ySerializer, zSerializer, wSerializer)
		{
		}

		public static IUniformSerDes<Index4D> Instance { get; } = new Index4DSerializer(Serializer.Int);

		/// <inheritdoc />
		protected override Index4D ComposeValue(int x, int y, int z, int w) => new Index4D(x, y, z, w);

		/// <inheritdoc />
		protected override void DecomposeValue(Index4D value, out int x, out int y, out int z, out int w)
		{
			x = value.X;
			y = value.Y;
			z = value.Z;
			w = value.W;
		}
	}
}
