﻿using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Serialization;

namespace Tilescape.Utility.Indexing.Serialization
{
	public class Index2DSerializer : AbstractCompositeUniformSerializer<Index2D, int, int>
	{
		public Index2DSerializer(IUniformSerDes<int> serializer)
			: base(serializer, serializer)
		{
		}

		public Index2DSerializer(IUniformSerDes<int> xSerializer, IUniformSerDes<int> ySerializer)
			: base(xSerializer, ySerializer)
		{
		}

		public static IUniformSerDes<Index2D> Instance { get; } = new Index2DSerializer(Serializer.Int);

		/// <inheritdoc />
		protected override Index2D ComposeValue(int x, int y) => new Index2D(x, y);

		/// <inheritdoc />
		protected override void DecomposeValue(Index2D value, out int x, out int y)
		{
			x = value.X;
			y = value.Y;
		}
	}
}
