﻿using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Indexables
{
	public class ProjectionIndexableView<TIndex, TSource, TResult> :
		AbstractProjectionIndexable<TIndex, TSource, TResult, IIndexableView<TIndex, TSource>>
		where TIndex : struct, IIndex<TIndex>
	{
		public ProjectionIndexableView(IIndexableView<TIndex, TSource> indexable, ITwoWayConverter<TSource, TResult> converter)
			: base(indexable, converter)
		{
		}
	}
}
