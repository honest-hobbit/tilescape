﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Indexables
{
	public abstract class AbstractProjectionIndexable<TIndex, TSource, TResult, TIndexable> : IIndexableView<TIndex, TResult>
		where TIndex : struct, IIndex<TIndex>
		where TIndexable : IIndexableView<TIndex, TSource>
	{
		public AbstractProjectionIndexable(TIndexable indexable, ITwoWayConverter<TSource, TResult> converter)
		{
			Require.That(indexable != null);
			Require.That(converter != null);

			this.Indexable = indexable;
			this.Converter = converter;
		}

		public TIndex Dimensions => this.Indexable.Dimensions;

		public TIndex LowerBounds => this.Indexable.LowerBounds;

		public TIndex UpperBounds => this.Indexable.UpperBounds;

		protected TIndexable Indexable { get; }

		protected ITwoWayConverter<TSource, TResult> Converter { get; }

		/// <inheritdoc />
		public TResult this[TIndex index]
		{
			get
			{
				IIndexableViewContracts.IndexerGet(this, index);

				return this.Converter.Convert(this.Indexable[index]);
			}
		}

		/// <inheritdoc />
		public IEnumerator<IndexValuePair<TIndex, TResult>> GetEnumerator() => this.Indexable.Select(
			pair => IndexValuePair.New(pair.Index, this.Converter.Convert(pair.Value))).GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
