﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Indexables
{
	public static class IIndexableContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void IndexerSet<TIndex, TValue>(IIndexable<TIndex, TValue> instance, TIndex index)
			where TIndex : struct, IIndex<TIndex>
		{
			Require.That(instance != null);
			Require.That(instance.IsIndexInBounds(index));
		}
	}
}
