﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Indexables
{
	public static class IIndexableViewContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void IndexerGet<TIndex, TValue>(IIndexableView<TIndex, TValue> instance, TIndex index)
			where TIndex : struct, IIndex<TIndex>
		{
			Require.That(instance != null);
			Require.That(instance.IsIndexInBounds(index));
		}
	}
}
