﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Indexables
{
	public static class IIndexableExtensions
	{
		public static IIndexableView<TIndex, TValue> AsReadOnly<TIndex, TValue>(this IIndexable<TIndex, TValue> indexable)
			where TIndex : struct, IIndex<TIndex>
		{
			Require.That(indexable != null);

			return new IndexableView<TIndex, TValue>(indexable);
		}
	}
}
