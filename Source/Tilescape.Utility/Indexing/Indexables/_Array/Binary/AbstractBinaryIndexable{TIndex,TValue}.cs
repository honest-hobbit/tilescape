﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Utility.Indexing.Indexables
{
	public abstract class AbstractBinaryIndexable<TIndex, TValue> : IIndexable<TIndex, TValue>
		where TIndex : struct, IIndex<TIndex>
	{
		private readonly TValue[] array;

		public AbstractBinaryIndexable(int powerOf2Exponent)
		{
			Require.That(powerOf2Exponent >= 0);

			this.PowerOf2Exponent = powerOf2Exponent;

			var length = MathUtility.PowerOf2(powerOf2Exponent);

			this.Dimensions = Index.Create<TIndex>(length);
			this.UpperBounds = Index.Create<TIndex>(length - 1);

			this.array = new TValue[MathUtility.IntegerPower(length, Index.Rank<TIndex>())];
		}

		/// <inheritdoc />
		public TIndex Dimensions { get; }

		/// <inheritdoc />
		public TIndex LowerBounds => Index.Zero<TIndex>();

		/// <inheritdoc />
		public TIndex UpperBounds { get; }

		protected int PowerOf2Exponent { get; }

		/// <inheritdoc />
		public TValue this[TIndex index]
		{
			get
			{
				IIndexableViewContracts.IndexerGet(this, index);

				return this.array[this.ToArrayIndex(index)];
			}

			set
			{
				IIndexableContracts.IndexerSet(this, index);

				this.array[this.ToArrayIndex(index)] = value;
			}
		}

		/// <inheritdoc />
		public abstract IEnumerator<IndexValuePair<TIndex, TValue>> GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		protected abstract int ToArrayIndex(TIndex index);
	}
}
