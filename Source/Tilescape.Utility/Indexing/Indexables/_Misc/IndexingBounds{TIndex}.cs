﻿using System.Linq;
using MiscUtil;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Indexables
{
	public class IndexingBounds<TIndex> : IIndexingBounds<TIndex>
		where TIndex : struct, IIndex<TIndex>
	{
		public IndexingBounds(TIndex dimensions)
			: this(Index.Zero<TIndex>(), dimensions)
		{
		}

		public IndexingBounds(TIndex lowerBounds, TIndex dimensions)
		{
			Require.That(dimensions.Coordinates().All(value => value >= 1));

			this.Dimensions = dimensions;
			this.LowerBounds = lowerBounds;
			this.UpperBounds = Operator.Add(this.LowerBounds, Operator.Subtract(this.Dimensions, Index.One<TIndex>()));
		}

		/// <inheritdoc />
		public TIndex Dimensions { get; }

		/// <inheritdoc />
		public TIndex LowerBounds { get; }

		/// <inheritdoc />
		public TIndex UpperBounds { get; }
	}
}
