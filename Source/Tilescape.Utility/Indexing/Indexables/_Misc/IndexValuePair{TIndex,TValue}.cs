﻿using System;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Indexing.Indexables
{
	public struct IndexValuePair<TIndex, TValue> : IEquatable<IndexValuePair<TIndex, TValue>>
		where TIndex : struct, IIndex<TIndex>
	{
		public IndexValuePair(TIndex index, TValue value)
		{
			this.Index = index;
			this.Value = value;
		}

		public TIndex Index { get; }

		public TValue Value { get; }

		/// <inheritdoc />
		public bool Equals(IndexValuePair<TIndex, TValue> other) =>
			this.Index.Equals(other.Index) && this.Value.EqualsNullSafe(other.Value);

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => HashCode.Start(this.Index).And(this.Value);

		/// <inheritdoc />
		public override string ToString() => $"[{this.Index.ToString()}: {this.Value.ToString()}]";
	}
}
