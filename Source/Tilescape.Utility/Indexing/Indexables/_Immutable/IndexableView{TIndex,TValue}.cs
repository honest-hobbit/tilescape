﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Indexables
{
	public class IndexableView<TIndex, TValue> : IIndexableView<TIndex, TValue>
		where TIndex : struct, IIndex<TIndex>
	{
		private readonly IIndexable<TIndex, TValue> indexable;

		public IndexableView(IIndexable<TIndex, TValue> indexable)
		{
			Require.That(indexable != null);

			this.indexable = indexable;
		}

		/// <inheritdoc />
		public TIndex Dimensions => this.indexable.Dimensions;

		/// <inheritdoc />
		public TIndex LowerBounds => this.indexable.LowerBounds;

		/// <inheritdoc />
		public TIndex UpperBounds => this.indexable.UpperBounds;

		/// <inheritdoc />
		public TValue this[TIndex index] => this.indexable[index];

		/// <inheritdoc />
		public IEnumerator<IndexValuePair<TIndex, TValue>> GetEnumerator() => this.indexable.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
