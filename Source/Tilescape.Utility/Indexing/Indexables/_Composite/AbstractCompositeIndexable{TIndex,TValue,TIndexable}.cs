﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MiscUtil;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Indexables
{
	public abstract class AbstractCompositeIndexable<TIndex, TValue, TIndexable> : IIndexableView<TIndex, TValue>
		where TIndex : struct, IIndex<TIndex>
		where TIndexable : IIndexableView<TIndex, TValue>
	{
		public AbstractCompositeIndexable(IIndexableView<TIndex, TIndexable> indexables)
		{
			Require.That(indexables.AllAndSelfNotNull());
			Require.That(indexables.Select(pair => pair.Value.Dimensions).AllEqual());
			Require.That(indexables.All(pair => pair.Value.LowerBounds.Equals(Index.Zero<TIndex>())));

			this.Indexables = indexables;
			this.Subdimensions = indexables.First().Value.Dimensions;
		}

		/// <inheritdoc />
		public TIndex Dimensions => Operator.Multiply(this.Subdimensions, this.Indexables.Dimensions);

		/// <inheritdoc />
		public TIndex LowerBounds => Index.Zero<TIndex>();

		/// <inheritdoc />
		public TIndex UpperBounds => Operator.Subtract(this.Dimensions, Index.One<TIndex>());

		protected TIndex Subdimensions { get; }

		protected IIndexableView<TIndex, TIndexable> Indexables { get; }

		/// <inheritdoc />
		public TValue this[TIndex index]
		{
			get
			{
				IIndexableViewContracts.IndexerGet(this, index);

				TIndex subIndex;
				return this.GetIndexable(index, out subIndex)[subIndex];
			}
		}

		/// <inheritdoc />
		public IEnumerator<IndexValuePair<TIndex, TValue>> GetEnumerator() =>
			this.GetIndices().Select(index => IndexValuePair.New(index, this[index])).GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		protected virtual TIndexable GetIndexable(TIndex index, out TIndex subIndex) =>
			this.Indexables[index.Divide(this.Subdimensions, out subIndex)];
	}
}
