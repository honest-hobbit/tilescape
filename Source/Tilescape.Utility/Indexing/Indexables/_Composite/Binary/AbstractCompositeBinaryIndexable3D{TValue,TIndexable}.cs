﻿using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Utility.Indexing.Indexables
{
	public abstract class AbstractCompositeBinaryIndexable3D<TValue, TIndexable> :
		AbstractCompositeIndexable<Index3D, TValue, TIndexable>
		where TIndexable : IIndexableView<Index3D, TValue>
	{
		private readonly int shift;

		private readonly int mask;

		public AbstractCompositeBinaryIndexable3D(IIndexableView<Index3D, TIndexable> indexables, int powerOf2Exponent)
			: base(indexables)
		{
			Require.That(indexables.AllAndSelfNotNull());
			Require.That(powerOf2Exponent >= 0);
			Require.That(indexables.All(pair => pair.Value.Dimensions.Equals(new Index3D(MathUtility.PowerOf2(powerOf2Exponent)))));

			this.shift = powerOf2Exponent;
			this.mask = MathUtility.PowerOf2(powerOf2Exponent) - 1;
		}

		protected override TIndexable GetIndexable(Index3D index, out Index3D subIndex)
		{
			subIndex = index & this.mask;
			return this.Indexables[index >> this.shift];
		}
	}
}
