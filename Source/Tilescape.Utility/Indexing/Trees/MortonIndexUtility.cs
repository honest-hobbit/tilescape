﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Trees
{
	/// <summary>
	///
	/// </summary>
	public static class MortonIndexUtility
	{
		public static BinaryTreeIndex<TIndex> ToBinaryTreeIndex<TIndex>(int mortonCode)
			where TIndex : struct, IIndex<TIndex>
		{
			int rank = Index.Rank<TIndex>();
			int layerLength = 1;
			int layers = 0;

			while (mortonCode >= layerLength)
			{
				mortonCode -= layerLength;
				layerLength <<= rank;
				layers++;
			}

			return new BinaryTreeIndex<TIndex>(layers, Index.MortonEncoder<TIndex>().DecodeIndex(mortonCode));
		}

		public static int GetLayer<TIndex>(int mortonCode)
			where TIndex : struct, IIndex<TIndex> => GetLayer(mortonCode, Index.Rank<TIndex>());

		public static int GetLayer(int mortonCode, int rank)
		{
			Require.That(rank >= 1);

			int layerLength = 1;
			int layers = 0;

			while (mortonCode >= layerLength)
			{
				mortonCode -= layerLength;
				layerLength <<= rank;
				layers++;
			}

			return layers;
		}

		public static void SplitOffsets<TIndex>(int mortonCode, out int layerOffset, out int indexOffset)
			where TIndex : struct, IIndex<TIndex> =>
			SplitOffsets(mortonCode, out layerOffset, out indexOffset, Index.Rank<TIndex>());

		public static void SplitOffsets(int mortonCode, out int layerOffset, out int indexOffset, int rank)
		{
			Require.That(rank >= 1);

			layerOffset = 0;
			indexOffset = 0;

			int combinedLayersLength = 1;
			int nextLayerLength = 1;

			while (mortonCode >= combinedLayersLength)
			{
				nextLayerLength <<= rank;
				combinedLayersLength += nextLayerLength;
			}

			combinedLayersLength -= nextLayerLength;
			layerOffset = combinedLayersLength;
			indexOffset = mortonCode - layerOffset;
		}

		public static int GetLayerLength<TIndex>(int layer)
			where TIndex : struct, IIndex<TIndex> => GetLayerLength(layer, Index.Rank<TIndex>());

		public static int GetLayerLength(int layer, int rank)
		{
			Require.That(layer >= 0);
			Require.That(rank >= 1);

			int result = 1;
			for (int count = 0; count < layer; count++)
			{
				result <<= rank;
			}

			return result;
		}

		public static int GetCombinedLayersLength<TIndex>(int layers)
			where TIndex : struct, IIndex<TIndex> => GetCombinedLayersLength(layers, Index.Rank<TIndex>());

		public static int GetCombinedLayersLength(int layers, int rank)
		{
			Require.That(layers >= 0);
			Require.That(rank >= 1);

			int result = 0;
			for (int count = 0; count < layers; count++)
			{
				result = (result << rank) | 1;
			}

			return result;
		}

		public static int GetLayerOffset<TIndex>(int layer)
			where TIndex : struct, IIndex<TIndex> => GetLayerOffset(layer, Index.Rank<TIndex>());

		public static int GetLayerOffset(int layer, int rank) => GetCombinedLayersLength(layer, rank);
	}
}
