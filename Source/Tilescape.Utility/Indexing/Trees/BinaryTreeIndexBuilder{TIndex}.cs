﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

using static Tilescape.Utility.Indexing.Indices.Index;

namespace Tilescape.Utility.Indexing.Trees
{
	public class BinaryTreeIndexBuilder<TIndex> : IIndexBuilder<BinaryTreeIndex<TIndex>>
		where TIndex : struct, IIndex<TIndex>
	{
		private readonly IIndexBuilder<TIndex> builder = CreateBuilder<TIndex>();

		private int layer;

		public int Layer
		{
			get { return this.layer; }

			set
			{
				Require.That(value >= 0);

				this.layer = value;
			}
		}

		public TIndex Index
		{
			get { return this.builder.ToIndex(); }
			set { this.builder.SetTo(value); }
		}

		/// <inheritdoc />
		public int Rank => this.builder.Rank;

		/// <inheritdoc />
		public int this[int dimension]
		{
			get
			{
				IIndexBuilderContracts.Indexer(this, dimension);

				return this.builder[dimension];
			}

			set
			{
				IIndexBuilderContracts.Indexer(this, dimension);

				this.builder[dimension] = value;
			}
		}

		/// <inheritdoc />
		public override string ToString() => this.ToIndex().ToString();

		/// <inheritdoc />
		public BinaryTreeIndex<TIndex> ToIndex() => new BinaryTreeIndex<TIndex>(this.Layer, this.Index);

		/// <inheritdoc />
		public IEnumerator<DimensionCoordinatePair> GetEnumerator() => this.builder.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
