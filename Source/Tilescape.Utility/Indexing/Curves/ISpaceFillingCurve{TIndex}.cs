﻿using System.Collections.Generic;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Curves
{
	public interface ISpaceFillingCurve<TIndex>
		where TIndex : struct, IIndex<TIndex>
	{
		int EncodeIndex(TIndex index);

		TIndex DecodeIndex(int code);

		IEnumerable<TIndex> GetCurve(int iterations);
	}
}
