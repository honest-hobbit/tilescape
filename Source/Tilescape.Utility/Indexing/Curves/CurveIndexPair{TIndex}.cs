﻿using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Curves
{
	public struct CurveIndexPair<TIndex>
		where TIndex : struct, IIndex<TIndex>
	{
		public CurveIndexPair(int code, TIndex index)
		{
			this.Code = code;
			this.Index = index;
		}

		public int Code { get; }

		public TIndex Index { get; }

		/// <inheritdoc />
		public override string ToString() => $"[{this.Code.ToString()}: {this.Index.ToString()}]";
	}
}
