﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Indexing.Curves
{
	public static class ISpaceFillingCurveContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void GetCurve(int iterations)
		{
			Require.That(iterations >= 0);
		}
	}
}
