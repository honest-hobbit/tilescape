﻿using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Indexing.Curves
{
	/// <summary>
	/// Provides extension methods for <see cref="ISpaceFillingCurve{TIndex}"/>.
	/// </summary>
	public static class ISpaceFillingCurveExtensions
	{
		public static IEnumerable<CurveIndexPair<TIndex>> GetCurveWithCodes<TIndex>(
			this ISpaceFillingCurve<TIndex> encoder, int iterations)
			where TIndex : struct, IIndex<TIndex>
		{
			Require.That(encoder != null);

			return encoder.GetCurve(iterations).Select((index, mortonCode) => CurveIndexPair.New(mortonCode, index));
		}
	}
}
