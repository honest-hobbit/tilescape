﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Disposables
{
	public static class IDisposableValueContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Value<T>(IDisposableValue<T> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}
	}
}
