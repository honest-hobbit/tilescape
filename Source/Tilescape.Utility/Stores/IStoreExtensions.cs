﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Stores
{
	public static class IStoreExtensions
	{
		public static ConfigStore<TEntity> Config<TEntity>(this IStore store)
			where TEntity : class, IConfigEntity, new()
		{
			Require.That(store != null);

			return new ConfigStore<TEntity>(store);
		}

		#region Get Methods

		public static TEntity Get<TKey, TEntity>(this IStore store, TKey key)
			where TEntity : class, IKeyed<TKey>, new()
		{
			GetMethodContracts(store, key);

			if (store.TryGet(key, out TEntity entity))
			{
				return entity;
			}
			else
			{
				throw new InvalidOperationException($"No entity found with key: {key}");
			}
		}

		public static TEntity GetOrNull<TKey, TEntity>(this IStore store, TKey key)
			where TEntity : class, IKeyed<TKey>, new()
		{
			GetMethodContracts(store, key);

			return store.TryGet(key, out TEntity entity) ? entity : null;
		}

		public static Try<TEntity> TryGet<TKey, TEntity>(this IStore store, TKey key)
			where TEntity : class, IKeyed<TKey>, new()
		{
			GetMethodContracts(store, key);

			return store.TryGet(key, out TEntity entity) ? Try.Value(entity) : Try.None<TEntity>();
		}

		#endregion

		#region Params Overloads of 'Many' Methods

		public static void AddMany<TEntity>(this IStore store, params TEntity[] entities)
			where TEntity : class, new()
		{
			ParamsMethodContracts(store, entities);

			store.AddMany(entities);
		}

		public static void AddOrUpdateMany<TEntity>(this IStore store, params TEntity[] entities)
			where TEntity : class, new()
		{
			ParamsMethodContracts(store, entities);

			store.AddOrUpdateMany(entities);
		}

		public static void UpdateMany<TEntity>(this IStore store, params TEntity[] entities)
			where TEntity : class, new()
		{
			ParamsMethodContracts(store, entities);

			store.UpdateMany(entities);
		}

		public static void RemoveMany<TEntity>(this IStore store, params TEntity[] entities)
			where TEntity : class, new()
		{
			ParamsMethodContracts(store, entities);

			store.RemoveMany(entities);
		}

		public static void RemoveManyKeys<TKey, TEntity>(this IStore store, params TKey[] keys)
			where TEntity : class, IKeyed<TKey>, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(keys.AllAndSelfNotNull());

			store.RemoveManyKeys<TKey, TEntity>(keys);
		}

		#endregion

		#region Private Contracts

		[Conditional(Require.CompilationSymbol)]
		private static void GetMethodContracts<TKey>(IStore store, TKey key)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(key != null);
		}

		[Conditional(Require.CompilationSymbol)]
		private static void ParamsMethodContracts<TEntity>(IStore store, TEntity[] entities)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entities.AllAndSelfNotNull());
		}

		#endregion
	}
}
