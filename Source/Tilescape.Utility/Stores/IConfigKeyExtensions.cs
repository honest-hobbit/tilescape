﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Stores
{
	/// <summary>
	/// Provides extension methods for <see cref="IConfigKey{T}"/>.
	/// </summary>
	public static class IConfigKeyExtensions
	{
		public static Try<T> TryDeserialize<T>(this IConfigKey<T> key, string value)
		{
			Require.That(key != null);

			return key.TryDeserialize(value, out var result) ? Try.Value(result) : Try.None<T>();
		}
	}
}
