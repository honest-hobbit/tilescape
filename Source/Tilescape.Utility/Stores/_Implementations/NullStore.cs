﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Stores
{
	public class NullStore : AbstractDisposable, IStore
	{
		/// <inheritdoc />
		public IEnumerable<TEntity> All<TEntity>()
			where TEntity : class, new()
		{
			IStoreContracts.All(this);

			return Enumerable.Empty<TEntity>();
		}

		/// <inheritdoc />
		public IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
			where TEntity : class, new()
		{
			IStoreContracts.Where(this, predicate);

			return Enumerable.Empty<TEntity>();
		}

		/// <inheritdoc />
		public bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.TryGet(this, key);

			entity = null;
			return false;
		}

		/// <inheritdoc />
		public void Add<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Add(this, entity);
		}

		/// <inheritdoc />
		public void AddMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.AddMany(this, entities);
		}

		/// <inheritdoc />
		public void AddOrUpdate<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.AddOrUpdate(this, entity);
		}

		/// <inheritdoc />
		public void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.AddOrUpdateMany(this, entities);
		}

		/// <inheritdoc />
		public void Update<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Update(this, entity);
		}

		/// <inheritdoc />
		public void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.UpdateMany(this, entities);
		}

		/// <inheritdoc />
		public void Remove<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Remove(this, entity);
		}

		/// <inheritdoc />
		public void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.RemoveMany(this, entities);
		}

		/// <inheritdoc />
		public void RemoveAll<TEntity>()
			where TEntity : class, new()
		{
			IStoreContracts.RemoveAll(this);
		}

		/// <inheritdoc />
		public void RemoveKey<TKey, TEntity>(TKey key)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.RemoveKey(this, key);
		}

		/// <inheritdoc />
		public void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.RemoveManyKeys(this, keys);
		}

		/// <inheritdoc />
		public void RunInTransaction(Action<IStore> action)
		{
			IStoreContracts.RunInTransaction(this, action);
		}

		/// <inheritdoc />
		protected override void ManagedDisposal()
		{
		}
	}
}
