﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using Tilescape.Utility.Concurrency;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Stores
{
	public class LockedStore : AbstractDisposable, IStore
	{
		private readonly ReaderWriterLockSlim padlock = new ReaderWriterLockSlim();

		private readonly IStore store;

		public LockedStore(IStore store)
		{
			Require.That(store != null);

			this.store = store;
		}

		/// <inheritdoc />
		public IEnumerable<TEntity> All<TEntity>()
			where TEntity : class, new()
		{
			IStoreContracts.All(this);

			using (this.padlock.EnterReadLockInUsingBlock())
			{
				return this.store.All<TEntity>();
			}
		}

		/// <inheritdoc />
		public IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
			where TEntity : class, new()
		{
			IStoreContracts.Where(this, predicate);

			using (this.padlock.EnterReadLockInUsingBlock())
			{
				return this.store.Where(predicate);
			}
		}

		/// <inheritdoc />
		public bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.TryGet(this, key);

			using (this.padlock.EnterReadLockInUsingBlock())
			{
				return this.store.TryGet(key, out entity);
			}
		}

		/// <inheritdoc />
		public void Add<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Add(this, entity);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.Add(entity);
			}
		}

		/// <inheritdoc />
		public void AddMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.AddMany(this, entities);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.AddMany(entities);
			}
		}

		/// <inheritdoc />
		public void AddOrUpdate<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.AddOrUpdate(this, entity);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.AddOrUpdate(entity);
			}
		}

		/// <inheritdoc />
		public void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.AddOrUpdateMany(this, entities);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.AddOrUpdateMany(entities);
			}
		}

		/// <inheritdoc />
		public void Update<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Update(this, entity);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.Update(entity);
			}
		}

		/// <inheritdoc />
		public void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.UpdateMany(this, entities);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.UpdateMany(entities);
			}
		}

		/// <inheritdoc />
		public void Remove<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Remove(this, entity);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.Remove(entity);
			}
		}

		/// <inheritdoc />
		public void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.RemoveMany(this, entities);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.RemoveMany(entities);
			}
		}

		/// <inheritdoc />
		public void RemoveAll<TEntity>()
			where TEntity : class, new()
		{
			IStoreContracts.RemoveAll(this);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.RemoveAll<TEntity>();
			}
		}

		/// <inheritdoc />
		public void RemoveKey<TKey, TEntity>(TKey key)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.RemoveKey(this, key);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.RemoveKey<TKey, TEntity>(key);
			}
		}

		/// <inheritdoc />
		public void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.RemoveManyKeys(this, keys);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.RemoveManyKeys<TKey, TEntity>(keys);
			}
		}

		/// <inheritdoc />
		public void RunInTransaction(Action<IStore> action)
		{
			IStoreContracts.RunInTransaction(this, action);

			using (this.padlock.EnterWriteLockInUsingBlock())
			{
				this.store.RunInTransaction(action);
			}
		}

		/// <inheritdoc />
		protected override void ManagedDisposal()
		{
			this.store.Dispose();
			this.padlock.Dispose();
		}
	}
}
