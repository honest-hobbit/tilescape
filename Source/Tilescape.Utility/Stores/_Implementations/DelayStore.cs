﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Stores
{
	public class DelayStore : AbstractDisposable, IStore
	{
		private readonly IStore store;

		private readonly int minMilliseconds;

		private readonly int maxMilliseconds;

		public DelayStore(IStore store, int minMilliseconds, int maxMilliseconds)
		{
			Require.That(store != null);
			Require.That(minMilliseconds >= 0);
			Require.That(maxMilliseconds >= minMilliseconds);

			this.store = store;
			this.minMilliseconds = minMilliseconds;
			this.maxMilliseconds = maxMilliseconds;
		}

		/// <inheritdoc />
		public IEnumerable<TEntity> All<TEntity>()
			where TEntity : class, new()
		{
			IStoreContracts.All(this);

			this.Delay();
			return this.store.All<TEntity>();
		}

		/// <inheritdoc />
		public IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
			where TEntity : class, new()
		{
			IStoreContracts.Where(this, predicate);

			this.Delay();
			return this.store.Where(predicate);
		}

		/// <inheritdoc />
		public bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.TryGet(this, key);

			this.Delay();
			return this.store.TryGet(key, out entity);
		}

		/// <inheritdoc />
		public void Add<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Add(this, entity);

			this.Delay();
			this.store.Add(entity);
		}

		/// <inheritdoc />
		public void AddMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.AddMany(this, entities);

			this.Delay();
			this.store.AddMany(entities);
		}

		/// <inheritdoc />
		public void AddOrUpdate<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.AddOrUpdate(this, entity);

			this.Delay();
			this.store.AddOrUpdate(entity);
		}

		/// <inheritdoc />
		public void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.AddOrUpdateMany(this, entities);

			this.Delay();
			this.store.AddOrUpdateMany(entities);
		}

		/// <inheritdoc />
		public void Update<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Update(this, entity);

			this.Delay();
			this.store.Update(entity);
		}

		/// <inheritdoc />
		public void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.UpdateMany(this, entities);

			this.Delay();
			this.store.UpdateMany(entities);
		}

		/// <inheritdoc />
		public void Remove<TEntity>(TEntity entity)
			where TEntity : class, new()
		{
			IStoreContracts.Remove(this, entity);

			this.Delay();
			this.store.Remove(entity);
		}

		/// <inheritdoc />
		public void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			IStoreContracts.RemoveMany(this, entities);

			this.Delay();
			this.store.RemoveMany(entities);
		}

		/// <inheritdoc />
		public void RemoveAll<TEntity>()
			where TEntity : class, new()
		{
			IStoreContracts.RemoveAll(this);

			this.Delay();
			this.store.RemoveAll<TEntity>();
		}

		/// <inheritdoc />
		public void RemoveKey<TKey, TEntity>(TKey key)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.RemoveKey(this, key);

			this.Delay();
			this.store.RemoveKey<TKey, TEntity>(key);
		}

		/// <inheritdoc />
		public void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
			where TEntity : class, IKeyed<TKey>, new()
		{
			IStoreContracts.RemoveManyKeys(this, keys);

			this.Delay();
			this.store.RemoveManyKeys<TKey, TEntity>(keys);
		}

		/// <inheritdoc />
		public void RunInTransaction(Action<IStore> action)
		{
			IStoreContracts.RunInTransaction(this, action);

			this.Delay();
			this.RunInTransaction(action);
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.store.Dispose();

		private void Delay() => Thread.Sleep(ThreadLocalRandom.Instance.Next(this.minMilliseconds, this.maxMilliseconds));
	}
}
