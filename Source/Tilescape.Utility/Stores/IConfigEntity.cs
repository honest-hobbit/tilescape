﻿using Tilescape.Utility.Types;

namespace Tilescape.Utility.Stores
{
	public interface IConfigEntity : IKeyed<string>
	{
		new string Key { get; set; }

		string Value { get; set; }
	}
}
