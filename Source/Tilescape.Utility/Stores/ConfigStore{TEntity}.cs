﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Stores
{
	public struct ConfigStore<TEntity> : IDisposed
		where TEntity : class, IConfigEntity, new()
	{
		private readonly IStore store;

		internal ConfigStore(IStore store)
		{
			Require.That(store != null);

			this.store = store;
		}

		/// <inheritdoc />
		public bool IsDisposed => this.store.IsDisposed;

		public bool TryGet<T>(IConfigKey<T> key, out T result)
		{
			this.SharedContracts(key);

			if (this.store.TryGet(key.Key, out TEntity entity))
			{
				return key.TryDeserialize(entity.Value, out result);
			}
			else
			{
				result = default(T);
				return false;
			}
		}

		public T Get<T>(IConfigKey<T> key)
		{
			Require.That(key != null);

			if (this.TryGet(key, out T value))
			{
				return value;
			}
			else
			{
				throw new InvalidOperationException($"No entity found with key: {key}");
			}
		}

		public Try<T> TryGet<T>(IConfigKey<T> key) => this.TryGet(key, out T value) ? Try.Value(value) : Try.None<T>();

		public T GetOrDefault<T>(IConfigKey<T> key) => this.GetOrDefault(key, default(T));

		public T GetOrDefault<T>(IConfigKey<T> key, T defaultValue) => this.TryGet(key, out T value) ? value : defaultValue;

		public T GetOrAddDefault<T>(IConfigKey<T> key) => this.GetOrAddDefault(key, default(T));

		public T GetOrAddDefault<T>(IConfigKey<T> key, T defaultValue)
		{
			var result = defaultValue;
			this.store.RunInTransaction(store =>
			{
				if (store.TryGet(key.Key, out TEntity entity) && key.TryDeserialize(entity.Value, out result))
				{
					// do nothing here because result has already been assigned by the out parameter of TryDeserialize
				}
				else
				{
					entity = new TEntity()
					{
						Key = key.Key,
						Value = key.Serialize(defaultValue)
					};

					store.AddOrUpdate(entity);
					result = defaultValue;
				}
			});

			return result;
		}

		public void Add<T>(IConfigKey<T> key, T value)
		{
			this.SharedContracts(key);

			var entity = new TEntity()
			{
				Key = key.Key,
				Value = key.Serialize(value)
			};

			this.store.Add(entity);
		}

		public void AddOrUpdate<T>(IConfigKey<T> key, T value)
		{
			this.SharedContracts(key);

			var entity = new TEntity()
			{
				Key = key.Key,
				Value = key.Serialize(value)
			};

			this.store.AddOrUpdate(entity);
		}

		public void Update<T>(IConfigKey<T> key, T value)
		{
			this.SharedContracts(key);

			var entity = new TEntity()
			{
				Key = key.Key,
				Value = key.Serialize(value)
			};

			this.store.Update(entity);
		}

		public void Remove<T>(IConfigKey<T> key)
		{
			this.SharedContracts(key);

			this.store.RemoveKey<string, TEntity>(key.Key);
		}

		public void RemoveAll()
		{
			Require.That(!this.IsDisposed);

			this.store.RemoveAll<TEntity>();
		}

		[Conditional(Require.CompilationSymbol)]
		private void SharedContracts<T>(IConfigKey<T> key)
		{
			Require.That(!this.IsDisposed);
			Require.That(key != null);
			Require.That(!key.Key.IsNullOrWhiteSpace());
		}
	}
}
