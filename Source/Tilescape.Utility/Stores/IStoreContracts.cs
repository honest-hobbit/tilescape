﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Stores
{
	public static class IStoreContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void All(IStore store)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Where<TEntity>(IStore store, Expression<Func<TEntity, bool>> predicate)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(predicate != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void TryGet<TKey>(IStore store, TKey key)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(key != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Add<TEntity>(IStore store, TEntity entity)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entity != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void AddMany<TEntity>(IStore store, IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entities.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void AddOrUpdate<TEntity>(IStore store, TEntity entity)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entity != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void AddOrUpdateMany<TEntity>(IStore store, IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entities.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Update<TEntity>(IStore store, TEntity entity)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entity != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void UpdateMany<TEntity>(IStore store, IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entities.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Remove<TEntity>(IStore store, TEntity entity)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entity != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void RemoveMany<TEntity>(IStore store, IEnumerable<TEntity> entities)
			where TEntity : class, new()
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(entities.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void RemoveAll(IStore store)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void RemoveKey<TKey>(IStore store, TKey key)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(key != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void RemoveManyKeys<TKey>(IStore store, IEnumerable<TKey> keys)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(keys.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void RunInTransaction(IStore store, Action<IStore> action)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(action != null);
		}
	}
}
