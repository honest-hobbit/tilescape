﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using SQLite;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.IO;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Stores.SQLite
{
	public static class SQLiteStore
	{
		public static IStore Open(string databasePath)
		{
			Require.That(File.Exists(databasePath));

			return new Store(new SQLiteConnection(databasePath));
		}

		public static IStore Create(string databasePath, params Type[] entityTypes) =>
			Create(databasePath, (IEnumerable<Type>)entityTypes);

		public static IStore Create(string databasePath, IEnumerable<Type> entityTypes)
		{
			Require.That(PathUtility.IsPathValid(databasePath));
			Require.That(entityTypes.AllAndSelfNotNull());

			var directory = Path.GetDirectoryName(databasePath);
			if (!directory.IsNullOrWhiteSpace())
			{
				Directory.CreateDirectory(directory);
			}

			var connection = new SQLiteConnection(databasePath);
			foreach (var type in entityTypes)
			{
				connection.CreateTable(type);
			}

			return new Store(connection);
		}

		private class Store : AbstractDisposable, IStore
		{
			private readonly SQLiteConnection connection;

			private readonly bool disposeConnection;

			public Store(SQLiteConnection connection)
				: this(connection, disposeConnection: true)
			{
			}

			private Store(SQLiteConnection connection, bool disposeConnection)
			{
				Require.That(connection != null);

				this.connection = connection;
				this.disposeConnection = disposeConnection;
			}

			/// <inheritdoc />
			public IEnumerable<TEntity> All<TEntity>()
				where TEntity : class, new()
			{
				IStoreContracts.All(this);

				return this.connection.Table<TEntity>().AsEnumerableOnly();
			}

			/// <inheritdoc />
			public IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
				where TEntity : class, new()
			{
				IStoreContracts.Where(this, predicate);

				return this.connection.Table<TEntity>().Where(predicate).AsEnumerableOnly();
			}

			/// <inheritdoc />
			public bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
				where TEntity : class, IKeyed<TKey>, new()
			{
				IStoreContracts.TryGet(this, key);

				entity = this.connection.Find<TEntity>(key);
				return entity != null;
			}

			/// <inheritdoc />
			public void Add<TEntity>(TEntity entity)
				where TEntity : class, new()
			{
				IStoreContracts.Add(this, entity);

				this.connection.Insert(entity);
			}

			/// <inheritdoc />
			public void AddMany<TEntity>(IEnumerable<TEntity> entities)
				where TEntity : class, new()
			{
				IStoreContracts.AddMany(this, entities);

				if (entities.Any())
				{
					this.connection.InsertAll(entities);
				}
			}

			/// <inheritdoc />
			public void AddOrUpdate<TEntity>(TEntity entity)
				where TEntity : class, new()
			{
				IStoreContracts.AddOrUpdate(this, entity);

				this.connection.InsertOrReplace(entity);
			}

			/// <inheritdoc />
			public void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
				where TEntity : class, new()
			{
				IStoreContracts.AddOrUpdateMany(this, entities);

				if (entities.Any())
				{
					this.connection.RunInTransaction(() =>
					{
						foreach (var entity in entities)
						{
							this.connection.InsertOrReplace(entity);
						}
					});
				}
			}

			/// <inheritdoc />
			public void Update<TEntity>(TEntity entity)
				where TEntity : class, new()
			{
				IStoreContracts.Update(this, entity);

				this.connection.Update(entity);
			}

			/// <inheritdoc />
			public void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
				where TEntity : class, new()
			{
				IStoreContracts.UpdateMany(this, entities);

				if (entities.Any())
				{
					this.connection.UpdateAll(entities);
				}
			}

			/// <inheritdoc />
			public void Remove<TEntity>(TEntity entity)
				where TEntity : class, new()
			{
				IStoreContracts.Remove(this, entity);

				this.connection.Delete(entity);
			}

			/// <inheritdoc />
			public void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
				where TEntity : class, new()
			{
				IStoreContracts.RemoveMany(this, entities);

				if (entities.Any())
				{
					this.connection.RunInTransaction(() =>
					{
						foreach (var entity in entities)
						{
							this.connection.Delete(entity);
						}
					});
				}
			}

			/// <inheritdoc />
			public void RemoveAll<TEntity>()
				where TEntity : class, new()
			{
				IStoreContracts.RemoveAll(this);

				this.connection.DeleteAll<TEntity>();
			}

			/// <inheritdoc />
			public void RemoveKey<TKey, TEntity>(TKey key)
				where TEntity : class, IKeyed<TKey>, new()
			{
				IStoreContracts.RemoveKey(this, key);

				this.connection.Delete<TEntity>(key);
			}

			/// <inheritdoc />
			public void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
				where TEntity : class, IKeyed<TKey>, new()
			{
				IStoreContracts.RemoveManyKeys(this, keys);

				if (keys.Any())
				{
					this.connection.RunInTransaction(() =>
					{
						foreach (var key in keys)
						{
							this.connection.Delete<TEntity>(key);
						}
					});
				}
			}

			/// <inheritdoc />
			public void RunInTransaction(Action<IStore> action)
			{
				IStoreContracts.RunInTransaction(this, action);

				this.connection.RunInTransaction(() =>
				{
					using (var store = new Store(this.connection, disposeConnection: false))
					{
						action(store);
					}
				});
			}

			/// <inheritdoc />
			protected override void ManagedDisposal()
			{
				if (this.disposeConnection)
				{
					this.connection.Dispose();
				}
			}
		}
	}
}
