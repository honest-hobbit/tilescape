﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Stores.SQLite
{
	public static class SQLiteConfigExtensions
	{
		public static ConfigStore<SQLiteConfigEntity> Config(this IStore store)
		{
			Require.That(store != null);

			return new ConfigStore<SQLiteConfigEntity>(store);
		}
	}
}
