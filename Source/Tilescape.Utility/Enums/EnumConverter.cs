﻿using System;
using System.Linq.Expressions;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Enums
{
	/// <summary>
	/// Provides <see cref="Converter{TSource, TResult}"/> delegates that convert enums to their underlying primitive types.
	/// </summary>
	public static class EnumConverter
	{
		/// <summary>
		/// Gets a type converter for an enum backed by an underlying byte.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <returns>The type converter for the enum.</returns>
		public static Func<TEnum, byte> ToByte<TEnum>()
			where TEnum : struct => GeneratedConverter<TEnum, byte>.Instance;

		/// <summary>
		/// Gets a type converter for an enum backed by an underlying sbyte.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <returns>The type converter for the enum.</returns>
		public static Func<TEnum, sbyte> ToSByte<TEnum>()
			where TEnum : struct => GeneratedConverter<TEnum, sbyte>.Instance;

		/// <summary>
		/// Gets a type converter for an enum backed by an underlying short.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <returns>The type converter for the enum.</returns>
		public static Func<TEnum, short> ToShort<TEnum>()
			where TEnum : struct => GeneratedConverter<TEnum, short>.Instance;

		/// <summary>
		/// Gets a type converter for an enum backed by an underlying ushort.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <returns>The type converter for the enum.</returns>
		public static Func<TEnum, ushort> ToUShort<TEnum>()
			where TEnum : struct => GeneratedConverter<TEnum, ushort>.Instance;

		/// <summary>
		/// Gets a type converter for an enum backed by an underlying int.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <returns>The type converter for the enum.</returns>
		public static Func<TEnum, int> ToInt<TEnum>()
			where TEnum : struct => GeneratedConverter<TEnum, int>.Instance;

		/// <summary>
		/// Gets a type converter for an enum backed by an underlying uint.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <returns>The type converter for the enum.</returns>
		public static Func<TEnum, uint> ToUInt<TEnum>()
			where TEnum : struct => GeneratedConverter<TEnum, uint>.Instance;

		/// <summary>
		/// Gets a type converter for an enum backed by an underlying long.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <returns>The type converter for the enum.</returns>
		public static Func<TEnum, long> ToLong<TEnum>()
			where TEnum : struct => GeneratedConverter<TEnum, long>.Instance;

		/// <summary>
		/// Gets a type converter for an enum backed by an underlying ulong.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <returns>The type converter for the enum.</returns>
		public static Func<TEnum, ulong> ToULong<TEnum>()
			where TEnum : struct => GeneratedConverter<TEnum, ulong>.Instance;

		/// <summary>
		/// Provides singleton instances of <see cref="Converter{TSource, TResult}"/> delegates
		/// that convert enums to their underlying primitive types.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <typeparam name="TResult">The underlying type to convert the enum to.</typeparam>
		private class GeneratedConverter<TEnum, TResult>
			where TEnum : struct
		{
			/// <summary>
			/// The singleton delegate instance used for converting enums to their underlying types.
			/// </summary>
			public static readonly Func<TEnum, TResult> Instance = GenerateConverter();

			/// <summary>
			/// Generates a <see cref="Func{TSource, TResult}" /> that will cast generic enum values
			/// to their underlying primitive values.
			/// </summary>
			/// <returns>
			/// The type converter delegate.
			/// </returns>
			/// <remarks>
			/// The code generated is similar to this;
			/// <code>
			/// int Convert(TEnum value) => (int)value;
			/// </code>
			/// Except that int will be replaced by whatever the underlying type of TEnum is.
			/// </remarks>
			private static Func<TEnum, TResult> GenerateConverter()
			{
				Require.That(typeof(TEnum).IsEnum);

				var valueParameter = Expression.Parameter(typeof(TEnum), "value");
				var convertExpression = Expression.Convert(valueParameter, typeof(TResult));
				return Expression.Lambda<Func<TEnum, TResult>>(convertExpression, new[] { valueParameter }).Compile();
			}
		}
	}
}
