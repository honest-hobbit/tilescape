﻿using System;
using System.ComponentModel;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Enums
{
	/// <summary>
	/// Provides helper methods for creating instances of <see cref="InvalidEnumArgumentException"/>.
	/// </summary>
	public static class InvalidEnumArgument
	{
		public static InvalidEnumArgumentException CreateException<TEnum>(string argumentName, TEnum value)
			where TEnum : struct
		{
			Require.That(!argumentName.IsNullOrEmpty());

			return new InvalidEnumArgumentException(argumentName, Convert.ToInt32(value), typeof(TEnum));
		}
	}
}
