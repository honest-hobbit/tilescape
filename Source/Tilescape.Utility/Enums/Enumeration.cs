﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Enums
{
	/// <summary>
	/// Provides helper methods for Enum types.
	/// </summary>
	public static class Enumeration
	{
		/// <summary>
		/// Determines whether the type is a valid underlying type for an enum.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <returns>True if the type is valid for underlying an enum, otherwise false.</returns>
		public static bool IsValidBackingType<T>()
			where T : struct => CheckType<T>.IsValidBackingType;

		public static bool IsEnum<T>()
			where T : struct => CheckType<T>.IsEnum;

		public static bool IsFlagsEnum<T>()
			where T : struct => CheckType<T>.IsFlagsEnum;

		/// <summary>
		/// Gets the values of an enum as a type-safe read only list.
		/// </summary>
		/// <typeparam name="T">The type of the enum.</typeparam>
		/// <returns>The values of that enum.</returns>
		/// <remarks>
		/// The values are returned in ascending unsigned binary order. For more information see
		/// <see href="https://msdn.microsoft.com/en-us/library/system.enum.getvalues%28v=vs.110%29.aspx">this link</see>.
		/// </remarks>
		public static IListView<T> Values<T>()
			where T : struct => EnumType<T>.Values;

		/// <summary>
		/// Determines whether the specified value would convert to a valid enum value.
		/// </summary>
		/// <typeparam name="T">The type of the enum.</typeparam>
		/// <param name="value">The value to check.</param>
		/// <returns>True if the value would convert to a valid enum value; otherwise false.</returns>
		/// <remarks>This method cannot be used for flags enums.</remarks>
		public static bool IsDefined<T>(T value)
			where T : struct => EnumType<T>.IsDefined(value);

		public static TValue[] CreateIndexableArray<TEnum, TValue>()
			where TEnum : struct => CreateIndexableArray<TEnum, TValue>(default(TValue));

		public static TValue[] CreateIndexableArray<TEnum, TValue>(TValue value)
			where TEnum : struct
		{
			Require.That(IsEnum<TEnum>());
			Require.That(!IsFlagsEnum<TEnum>());
			Require.That(EnumType<TEnum>.IndexableArrayLength.HasValue);

			return Factory.CreateArray(EnumType<TEnum>.IndexableArrayLength.Value, value);
		}

		private static class CheckType<T>
			where T : struct
		{
			static CheckType()
			{
				var type = typeof(T);
				IsEnum = type.IsEnum;
				IsFlagsEnum = type.IsEnum && type.GetCustomAttributes<FlagsAttribute>().Any();
				IsValidBackingType =
					type == typeof(byte) ||
					type == typeof(sbyte) ||
					type == typeof(short) ||
					type == typeof(ushort) ||
					type == typeof(int) ||
					type == typeof(uint) ||
					type == typeof(long) ||
					type == typeof(ulong);
			}

			public static bool IsValidBackingType { get; }

			public static bool IsEnum { get; }

			public static bool IsFlagsEnum { get; }
		}

		private static class EnumType<T>
			where T : struct
		{
			private static readonly HashSet<T> ValuesSet;

			static EnumType()
			{
				var type = typeof(T);
				if (!type.IsEnum)
				{
					throw new NotSupportedException(type.FullName + " is not an Enum.");
				}

				Values = ((T[])Enum.GetValues(type)).AsListView();
				ValuesSet = new HashSet<T>(Values);
				IndexableArrayLength = GetIndexableArrayLength(Values);
			}

			public static IListView<T> Values { get; }

			public static Try<int> IndexableArrayLength { get; }

			public static bool IsDefined(T value)
			{
				Require.That(!IsFlagsEnum<T>());

				return ValuesSet.Contains(value);
			}

			private static Try<int> GetIndexableArrayLength(IListView<T> values)
			{
				Require.That(values != null);

				if (CheckType<T>.IsFlagsEnum)
				{
					return Try.None<int>();
				}

				try
				{
					var toIndex = EnumConverter.ToInt<T>();
					if (values.Select(x => toIndex(x)).Any(x => x < 0))
					{
						return Try.None<int>();
					}

					return values.Select(x => toIndex(x)).Max() + 1;
				}
				catch (Exception)
				{
					return Try.None<int>();
				}
			}
		}
	}
}
