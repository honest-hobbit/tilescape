﻿using System;
using System.Diagnostics;

namespace Tilescape.Utility.Contracts
{
	public static class Require
	{
		public const string CompilationSymbol = "CONTRACTS";

		/// <summary>
		/// Specifies a contract that must be met and throws an exception with the given message if it is violated.
		/// </summary>
		/// <param name="condition">The conditional expression to test.</param>
		/// <param name="message">The message to display if the condition is false.</param>
		/// <exception cref="ContractException">Thrown if the condition is false.</exception>
		[Conditional(CompilationSymbol)]
		public static void That(bool condition, string message = null)
		{
#if CONTRACTS
			if (!condition)
			{
				throw new ContractException(message);
			}
#endif
		}

		/// <summary>
		/// Specifies a contract that must be met and throws an exception with the given message if it is violated.
		/// </summary>
		/// <param name="condition">The conditional expression to test.</param>
		/// <param name="message">
		/// The function used to generate a message to display if the condition is false.
		/// Only executed if the contract is violated.
		/// </param>
		/// <exception cref="ContractException">Thrown if the condition is false.</exception>
		[Conditional(CompilationSymbol)]
		public static void That(bool condition, Func<string> message)
		{
#if CONTRACTS
			That(message != null);

			if (!condition)
			{
				throw new ContractException(message());
			}
#endif
		}
	}
}
