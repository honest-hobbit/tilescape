﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class ByteBuffer
	{
		private readonly List<byte> bytes;

		private readonly Action<byte> writeByte;

		public ByteBuffer()
			: this(Length.OfLongestPrimitive.InBytes)
		{
		}

		public ByteBuffer(int capacity)
		{
			Require.That(capacity >= 0);

			this.bytes = new List<byte>(capacity);
			this.writeByte = x => this.bytes.Add(x);
		}

		public byte[] Serialize<T>(ISerializer<T> serializer, T value)
		{
			Require.That(serializer != null);
			Require.That(value != null);

			this.bytes.Clear();
			serializer.Serialize(value, this.writeByte);
			return this.bytes.ToArray();
		}
	}
}
