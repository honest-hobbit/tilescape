﻿using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	/// <summary>
	///
	/// </summary>
	public static class SerializeInt
	{
		public static IUniformSerDes<int> AsByte { get; } =
			new ConverterUniformSerializer<int, byte>(new IntToByteConverter(), Serializer.Byte);

		public static IUniformSerDes<int> AsSByte { get; } =
			new ConverterUniformSerializer<int, sbyte>(new IntToSByteConverter(), Serializer.SByte);

		public static IUniformSerDes<int> AsShort { get; } =
			new ConverterUniformSerializer<int, short>(new IntToShortConverter(), Serializer.Short);

		public static IUniformSerDes<int> AsUShort { get; } =
			new ConverterUniformSerializer<int, ushort>(new IntToUShortConverter(), Serializer.UShort);

		#region Private Classes

		private class IntToByteConverter : ITwoWayConverter<int, byte>
		{
			/// <inheritdoc />
			public byte Convert(int value) => checked((byte)value);

			/// <inheritdoc />
			public int Convert(byte value) => value;
		}

		private class IntToSByteConverter : ITwoWayConverter<int, sbyte>
		{
			/// <inheritdoc />
			public sbyte Convert(int value) => checked((sbyte)value);

			/// <inheritdoc />
			public int Convert(sbyte value) => value;
		}

		private class IntToShortConverter : ITwoWayConverter<int, short>
		{
			/// <inheritdoc />
			public short Convert(int value) => checked((short)value);

			/// <inheritdoc />
			public int Convert(short value) => value;
		}

		private class IntToUShortConverter : ITwoWayConverter<int, ushort>
		{
			/// <inheritdoc />
			public ushort Convert(int value) => checked((ushort)value);

			/// <inheritdoc />
			public int Convert(ushort value) => value;
		}

		#endregion
	}
}
