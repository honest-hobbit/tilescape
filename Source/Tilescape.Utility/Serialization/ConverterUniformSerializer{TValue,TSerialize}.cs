﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class ConverterUniformSerializer<TValue, TSerialize> : ConverterSerializer<TValue, TSerialize>, IUniformSerDes<TValue>
	{
		public ConverterUniformSerializer(ITwoWayConverter<TValue, TSerialize> converter, IUniformSerDes<TSerialize> serializer)
			: base(converter, serializer)
		{
			Require.That(converter != null);
			Require.That(serializer != null);

			this.SerializedLength = serializer.SerializedLength;
		}

		/// <inheritdoc />
		public int SerializedLength { get; }
	}
}
