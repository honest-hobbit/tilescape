﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	/// <summary>
	/// A serializer that doesn't actually serialize a value.
	/// Instead it always returns the same constant value when deserializing.
	/// </summary>
	/// <typeparam name="T">The type of value to serialize.</typeparam>
	public class ConstantValueSerializer<T> : IUniformSerDes<T>
	{
		public ConstantValueSerializer(T constantValue)
		{
			this.ConstantValue = constantValue;
		}

		public T ConstantValue { get; }

		/// <inheritdoc />
		public int SerializedLength => 0;

		/// <inheritdoc />
		public int GetSerializedLength(T value)
		{
			ISerializerContracts.GetSerializedLength(value);

			return this.SerializedLength;
		}

		/// <inheritdoc />
		public int Serialize(T value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);
			Require.That(value.Equals(this.ConstantValue));

			return this.SerializedLength;
		}

		/// <inheritdoc />
		public int Serialize(T value, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, value, buffer, index);
			Require.That(value.Equals(this.ConstantValue));

			return this.SerializedLength;
		}

		/// <inheritdoc />
		public T Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return this.ConstantValue;
		}

		/// <inheritdoc />
		public T Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			return this.ConstantValue;
		}
	}
}
