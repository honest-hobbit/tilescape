﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class RunLengthEncodedListSerializer<T> :
		RunLengthEncodedSerializer<T>, IInlineSerDes<IList<T>>, ISerDes<IList<T>>, IInlineSerDes<ICollection<T>>, ISerDes<ICollection<T>>
	{
		public RunLengthEncodedListSerializer(ISerDes<T> valueSerializer, IEqualityComparer<T> comparer = null)
			: base(valueSerializer, comparer)
		{
		}

		public RunLengthEncodedListSerializer(
			ISerDes<T> valueSerializer, IUniformSerDes<int> runLengthSerializer, int maxRunLength, IEqualityComparer<T> comparer = null)
			: base(valueSerializer, runLengthSerializer, maxRunLength, comparer)
		{
		}

		/// <inheritdoc />
		int ISerializer<ICollection<T>>.GetSerializedLength(ICollection<T> value) => this.GetSerializedLength(value);

		/// <inheritdoc />
		int ISerializer<IList<T>>.GetSerializedLength(IList<T> value) => this.GetSerializedLength(value);

		/// <inheritdoc />
		int ISerializer<ICollection<T>>.Serialize(ICollection<T> value, byte[] buffer, ref int index) =>
			this.Serialize(value, buffer, ref index);

		/// <inheritdoc />
		int ISerializer<ICollection<T>>.Serialize(ICollection<T> value, Action<byte> writeByte) => this.Serialize(value, writeByte);

		/// <inheritdoc />
		int ISerializer<IList<T>>.Serialize(IList<T> value, byte[] buffer, ref int index) => this.Serialize(value, buffer, ref index);

		/// <inheritdoc />
		int ISerializer<IList<T>>.Serialize(IList<T> value, Action<byte> writeByte) => this.Serialize(value, writeByte);

		/// <inheritdoc />
		ICollection<T> IDeserializer<ICollection<T>>.Deserialize(byte[] buffer, ref int index) => this.Deserialize(buffer, ref index);

		/// <inheritdoc />
		ICollection<T> IDeserializer<ICollection<T>>.Deserialize(Func<Try<byte>> readByte) => this.Deserialize(readByte);

		/// <inheritdoc />
		public new IList<T> Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			var result = new List<T>();
			this.DeserializeInline(buffer, ref index, result);
			return result;
		}

		/// <inheritdoc />
		public new IList<T> Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			var result = new List<T>();
			this.DeserializeInline(readByte, result);
			return result;
		}

		/// <inheritdoc />
		public void DeserializeInline(byte[] buffer, ref int index, ICollection<T> result)
		{
			IInlineDeserializerContracts.DeserializeInline(buffer, index, result);

			int count = 0;
			int localIndex = index;
			foreach (var value in this.DeserializeValues(buffer, index, x => localIndex = x))
			{
				result.Add(value);
				count++;
			}

			index = localIndex;
		}

		/// <inheritdoc />
		public void DeserializeInline(Func<Try<byte>> readByte, ICollection<T> result)
		{
			IInlineDeserializerContracts.DeserializeInline(readByte, result);

			int count = 0;
			foreach (var value in this.DeserializeValues(readByte))
			{
				result.Add(value);
				count++;
			}
		}

		/// <inheritdoc />
		void IInlineDeserializer<IList<T>>.DeserializeInline(byte[] buffer, ref int index, IList<T> result) =>
			this.DeserializeInline(buffer, ref index, result);

		/// <inheritdoc />
		void IInlineDeserializer<IList<T>>.DeserializeInline(Func<Try<byte>> readByte, IList<T> result) =>
			this.DeserializeInline(readByte, result);
	}
}
