﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class RunLengthEncodedArraySerializer<T> : RunLengthEncodedSerializer<T>, IInlineSerDes<T[]>, ISerDes<T[]>
	{
		public RunLengthEncodedArraySerializer(ISerDes<T> valueSerializer, IEqualityComparer<T> comparer = null)
			: base(valueSerializer, comparer)
		{
		}

		public RunLengthEncodedArraySerializer(
			ISerDes<T> valueSerializer, IUniformSerDes<int> runLengthSerializer, int maxRunLength, IEqualityComparer<T> comparer = null)
			: base(valueSerializer, runLengthSerializer, maxRunLength, comparer)
		{
		}

		/// <inheritdoc />
		public int GetSerializedLength(T[] value) => base.GetSerializedLength(value);

		/// <inheritdoc />
		public int Serialize(T[] value, byte[] buffer, ref int index) => base.Serialize(value, buffer, ref index);

		/// <inheritdoc />
		public int Serialize(T[] value, Action<byte> writeByte) => base.Serialize(value, writeByte);

		/// <inheritdoc />
		public new T[] Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			int localIndex = index;
			var result = this.DeserializeValues(buffer, index, x => localIndex = x).ToArray();
			index = localIndex;
			return result;
		}

		/// <inheritdoc />
		public new T[] Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return this.DeserializeValues(readByte).ToArray();
		}

		/// <inheritdoc />
		public void DeserializeInline(byte[] buffer, ref int index, T[] result)
		{
			IInlineDeserializerContracts.DeserializeInline(buffer, index, result);

			int count = 0;
			int localIndex = index;
			foreach (var value in this.DeserializeValues(buffer, index, x => localIndex = x))
			{
				result[count] = value;
				count++;
			}

			index = localIndex;
		}

		/// <inheritdoc />
		public void DeserializeInline(Func<Try<byte>> readByte, T[] result)
		{
			IInlineDeserializerContracts.DeserializeInline(readByte, result);

			int count = 0;
			foreach (var value in this.DeserializeValues(readByte))
			{
				result[count] = value;
				count++;
			}
		}
	}
}
