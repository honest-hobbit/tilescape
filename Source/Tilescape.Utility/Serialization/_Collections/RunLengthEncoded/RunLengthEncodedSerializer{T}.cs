﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class RunLengthEncodedSerializer<T> : ISerDes<IEnumerable<T>>
	{
		private readonly ISerDes<T> valueSerializer;

		private readonly IUniformSerDes<int> runLengthSerializer;

		private readonly int maxRunLength;

		private readonly IEqualityComparer<T> comparer;

		public RunLengthEncodedSerializer(ISerDes<T> valueSerializer, IEqualityComparer<T> comparer = null)
			: this(valueSerializer, SerializeInt.AsByte, byte.MaxValue, comparer)
		{
		}

		public RunLengthEncodedSerializer(
			ISerDes<T> valueSerializer, IUniformSerDes<int> runLengthSerializer, int maxRunLength, IEqualityComparer<T> comparer = null)
		{
			Require.That(valueSerializer != null);
			Require.That(runLengthSerializer != null);
			Require.That(maxRunLength >= 1);

			this.valueSerializer = valueSerializer;
			this.runLengthSerializer = runLengthSerializer;
			this.maxRunLength = maxRunLength;
			this.comparer = comparer ?? EqualityComparer<T>.Default;
		}

		/// <inheritdoc />
		public int GetSerializedLength(IEnumerable<T> enumerable)
		{
			ISerializerContracts.GetSerializedLength(enumerable);

			using (var enumerator = enumerable.GetEnumerator())
			{
				if (!enumerator.MoveNext())
				{
					// serialize run length of 0 to terminate the sequence
					return this.runLengthSerializer.SerializedLength;
				}

				int serializedLength = 0;
				T previous = enumerator.Current;
				int runLength = 1;

				while (enumerator.MoveNext())
				{
					if (this.comparer.Equals(enumerator.Current, previous) && runLength < this.maxRunLength)
					{
						runLength++;
					}
					else
					{
						// run ended, thus serialize the run length and previous value before starting a new run
						serializedLength += this.runLengthSerializer.SerializedLength;
						serializedLength += this.valueSerializer.GetSerializedLength(previous);

						previous = enumerator.Current;
						runLength = 1;
					}
				}

				// enumerable ended and therefor run ended, thus serialize the run length and previous value
				serializedLength += this.runLengthSerializer.SerializedLength;
				serializedLength += this.valueSerializer.GetSerializedLength(previous);

				// enumerable ended so also serialize a run length of 0 to terminate the sequence
				serializedLength += this.runLengthSerializer.SerializedLength;
				return serializedLength;
			}
		}

		/// <inheritdoc />
		public int Serialize(IEnumerable<T> enumerable, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, enumerable, buffer, index);

			using (var enumerator = enumerable.GetEnumerator())
			{
				if (!enumerator.MoveNext())
				{
					// serialize run length of 0 to terminate the sequence
					return this.runLengthSerializer.Serialize(0, buffer, ref index);
				}

				int serializedLength = 0;
				T previous = enumerator.Current;
				int runLength = 1;

				while (enumerator.MoveNext())
				{
					if (this.comparer.Equals(enumerator.Current, previous) && runLength < this.maxRunLength)
					{
						runLength++;
					}
					else
					{
						// run ended, thus serialize the run length and previous value before starting a new run
						serializedLength += this.runLengthSerializer.Serialize(runLength, buffer, ref index);
						serializedLength += this.valueSerializer.Serialize(previous, buffer, ref index);

						previous = enumerator.Current;
						runLength = 1;
					}
				}

				// enumerable ended and therefor run ended, thus serialize the run length and previous value
				serializedLength += this.runLengthSerializer.Serialize(runLength, buffer, ref index);
				serializedLength += this.valueSerializer.Serialize(previous, buffer, ref index);

				// enumerable ended so also serialize a run length of 0 to terminate the sequence
				serializedLength += this.runLengthSerializer.Serialize(0, buffer, ref index);
				return serializedLength;
			}
		}

		/// <inheritdoc />
		public int Serialize(IEnumerable<T> enumerable, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(enumerable, writeByte);

			using (var enumerator = enumerable.GetEnumerator())
			{
				if (!enumerator.MoveNext())
				{
					// serialize run length of 0 to terminate the sequence
					return this.runLengthSerializer.Serialize(0, writeByte);
				}

				int serializedLength = 0;
				T previous = enumerator.Current;
				int runLength = 1;

				while (enumerator.MoveNext())
				{
					if (this.comparer.Equals(enumerator.Current, previous) && runLength < this.maxRunLength)
					{
						runLength++;
					}
					else
					{
						// run ended, thus serialize the run length and previous value before starting a new run
						serializedLength += this.runLengthSerializer.Serialize(runLength, writeByte);
						serializedLength += this.valueSerializer.Serialize(previous, writeByte);

						previous = enumerator.Current;
						runLength = 1;
					}
				}

				// enumerable ended and therefor run ended, thus serialize the run length and previous value
				serializedLength += this.runLengthSerializer.Serialize(runLength, writeByte);
				serializedLength += this.valueSerializer.Serialize(previous, writeByte);

				// enumerable ended so also serialize a run length of 0 to terminate the sequence
				serializedLength += this.runLengthSerializer.Serialize(0, writeByte);
				return serializedLength;
			}
		}

		/// <inheritdoc />
		public IEnumerable<T> Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			int localIndex = index;
			var result = this.DeserializeValues(buffer, index, x => localIndex = x).ToArray();
			index = localIndex;
			return result;
		}

		/// <inheritdoc />
		public IEnumerable<T> Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return this.DeserializeValues(readByte).ToArray();
		}

		// subclasses must force the eager evaluation of the returned enumerable once and only once
		protected IEnumerable<T> DeserializeValues(byte[] buffer, int index, Action<int> setIndex)
		{
			IDeserializerContracts.Deserialize(buffer, index);
			Require.That(setIndex != null);

			int runLength = this.runLengthSerializer.Deserialize(buffer, ref index);
			setIndex(index);
			while (runLength != 0)
			{
				var value = this.valueSerializer.Deserialize(buffer, ref index);
				setIndex(index);
				for (int count = 0; count < runLength; count++)
				{
					yield return value;
				}

				runLength = this.runLengthSerializer.Deserialize(buffer, ref index);
				setIndex(index);
			}
		}

		// subclasses must force the eager evaluation of the returned enumerable once and only once
		protected IEnumerable<T> DeserializeValues(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			int runLength = this.runLengthSerializer.Deserialize(readByte);
			while (runLength != 0)
			{
				var value = this.valueSerializer.Deserialize(readByte);
				for (int count = 0; count < runLength; count++)
				{
					yield return value;
				}

				runLength = this.runLengthSerializer.Deserialize(readByte);
			}
		}
	}
}
