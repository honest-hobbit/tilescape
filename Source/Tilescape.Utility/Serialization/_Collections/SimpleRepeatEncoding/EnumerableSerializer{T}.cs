﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class EnumerableSerializer<T> : AbstractEnumerableSerializer<IEnumerable<T>, T>
	{
		public EnumerableSerializer(ISerDes<T> valueSerializer, IUniformSerDes<int> countSerializer = null)
			: base(valueSerializer, countSerializer)
		{
		}

		/// <inheritdoc />
		protected override IEnumerable<T> DeserializeValues(int countOfValues, Func<Try<byte>> readByte) =>
			this.ValueSerializer.DeserializeMany(countOfValues, readByte).AsEnumerableOnly();

		/// <inheritdoc />
		protected override IEnumerable<T> DeserializeValues(int countOfValues, byte[] buffer, ref int index) =>
			this.ValueSerializer.DeserializeMany(countOfValues, buffer, ref index).AsEnumerableOnly();
	}
}
