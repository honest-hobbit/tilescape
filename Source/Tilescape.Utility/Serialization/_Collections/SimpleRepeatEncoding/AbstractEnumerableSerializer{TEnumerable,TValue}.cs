﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractEnumerableSerializer<TEnumerable, TValue> : ISerDes<TEnumerable>
		where TEnumerable : IEnumerable<TValue>
	{
		private static readonly int NonuniformValueLength = -1;

		private readonly int uniformValueLength;

		public AbstractEnumerableSerializer(ISerDes<TValue> valueSerializer, IUniformSerDes<int> countSerializer)
		{
			Require.That(valueSerializer != null);

			this.ValueSerializer = valueSerializer;
			this.CountSerializer = countSerializer ?? Serializer.Int;

			this.uniformValueLength = (valueSerializer as IUniformSerializedLength)?.SerializedLength ?? NonuniformValueLength;
		}

		protected IUniformSerDes<int> CountSerializer { get; }

		protected ISerDes<TValue> ValueSerializer { get; }

		/// <inheritdoc />
		public virtual int GetSerializedLength(TEnumerable value)
		{
			ISerializerContracts.GetSerializedLength(value);

			if (this.uniformValueLength == NonuniformValueLength)
			{
				int serializedLength = this.CountSerializer.SerializedLength;
				foreach (var enumeratedValue in value)
				{
					serializedLength += this.ValueSerializer.GetSerializedLength(enumeratedValue);
				}

				return serializedLength;
			}
			else
			{
				return this.CountSerializer.SerializedLength + (this.GetCount(value) * this.uniformValueLength);
			}
		}

		/// <inheritdoc />
		public int Serialize(TEnumerable value, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, value, buffer, index);

			int originalArrayIndex = index;
			this.CountSerializer.Serialize(this.GetCount(value), buffer, ref index);

			this.ValueSerializer.SerializeMany(value, buffer, ref index);
			return index - originalArrayIndex;
		}

		/// <inheritdoc />
		public int Serialize(TEnumerable value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			int serializedLength = this.CountSerializer.Serialize(this.GetCount(value), writeByte);

			return this.ValueSerializer.SerializeMany(value, writeByte) + serializedLength;
		}

		/// <inheritdoc />
		public TEnumerable Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			return this.DeserializeValues(this.CountSerializer.Deserialize(buffer, ref index), buffer, ref index);
		}

		/// <inheritdoc />
		public TEnumerable Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return this.DeserializeValues(this.CountSerializer.Deserialize(readByte), readByte);
		}

		protected virtual int GetCount(TEnumerable value) => value.CountExtended();

		protected abstract TEnumerable DeserializeValues(int countOfValues, byte[] buffer, ref int index);

		protected abstract TEnumerable DeserializeValues(int countOfValues, Func<Try<byte>> readByte);
	}
}
