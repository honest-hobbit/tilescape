﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class CollectionSerializer<T> : AbstractEnumerableInlineSerializer<ICollection<T>, T>
	{
		public CollectionSerializer(ISerDes<T> valueSerializer, IUniformSerDes<int> countSerializer = null)
			: base(valueSerializer, countSerializer)
		{
		}

		/// <inheritdoc />
		protected override int GetCount(ICollection<T> value) => value.Count;

		/// <inheritdoc />
		protected override ICollection<T> Create(int countOfValues) => new List<T>(countOfValues);

		/// <inheritdoc />
		protected override void DeserializeValuesInline(int countOfValues, Func<Try<byte>> readByte, ICollection<T> result)
		{
			for (int count = 0; count < countOfValues; count++)
			{
				result.Add(this.ValueSerializer.Deserialize(readByte));
			}
		}

		/// <inheritdoc />
		protected override void DeserializeValuesInline(int countOfValues, byte[] buffer, ref int index, ICollection<T> result)
		{
			for (int count = 0; count < countOfValues; count++)
			{
				result.Add(this.ValueSerializer.Deserialize(buffer, ref index));
			}
		}
	}
}
