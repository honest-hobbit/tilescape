﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class ListViewSerializer<T> : AbstractEnumerableSerializer<IListView<T>, T>
	{
		public ListViewSerializer(ISerDes<T> valueSerializer, IUniformSerDes<int> countSerializer = null)
			: base(valueSerializer, countSerializer)
		{
		}

		/// <inheritdoc />
		protected override int GetCount(IListView<T> value) => value.Count;

		/// <inheritdoc />
		protected override IListView<T> DeserializeValues(int countOfValues, Func<Try<byte>> readByte) =>
			this.ValueSerializer.DeserializeMany(countOfValues, readByte).AsListView();

		/// <inheritdoc />
		protected override IListView<T> DeserializeValues(int countOfValues, byte[] buffer, ref int index) =>
			this.ValueSerializer.DeserializeMany(countOfValues, buffer, ref index).AsListView();
	}
}
