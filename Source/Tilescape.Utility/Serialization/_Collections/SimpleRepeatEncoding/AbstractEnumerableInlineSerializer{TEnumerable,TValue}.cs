﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractEnumerableInlineSerializer<TEnumerable, TValue> :
		AbstractEnumerableSerializer<TEnumerable, TValue>, IInlineSerDes<TEnumerable>
		where TEnumerable : class, IEnumerable<TValue>
	{
		public AbstractEnumerableInlineSerializer(ISerDes<TValue> valueSerializer, IUniformSerDes<int> countSerializer)
			: base(valueSerializer, countSerializer)
		{
		}

		/// <inheritdoc />
		public void DeserializeInline(byte[] buffer, ref int index, TEnumerable result)
		{
			IInlineDeserializerContracts.DeserializeInline(buffer, index, result);

			this.DeserializeValuesInline(this.CountSerializer.Deserialize(buffer, ref index), buffer, ref index, result);
		}

		/// <inheritdoc />
		public void DeserializeInline(Func<Try<byte>> readByte, TEnumerable result)
		{
			IInlineDeserializerContracts.DeserializeInline(readByte, result);

			this.DeserializeValuesInline(this.CountSerializer.Deserialize(readByte), readByte, result);
		}

		/// <inheritdoc />
		protected sealed override TEnumerable DeserializeValues(int countOfValues, byte[] buffer, ref int index)
		{
			TEnumerable result = this.Create(countOfValues);
			this.DeserializeValuesInline(countOfValues, buffer, ref index, result);
			return result;
		}

		/// <inheritdoc />
		protected sealed override TEnumerable DeserializeValues(int countOfValues, Func<Try<byte>> readByte)
		{
			TEnumerable result = this.Create(countOfValues);
			this.DeserializeValuesInline(countOfValues, readByte, result);
			return result;
		}

		protected abstract TEnumerable Create(int countOfValues);

		protected abstract void DeserializeValuesInline(int countOfValues, byte[] buffer, ref int index, TEnumerable result);

		protected abstract void DeserializeValuesInline(int countOfValues, Func<Try<byte>> readByte, TEnumerable result);
	}
}
