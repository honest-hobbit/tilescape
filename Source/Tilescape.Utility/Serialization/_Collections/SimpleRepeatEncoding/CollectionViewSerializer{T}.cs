﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class CollectionViewSerializer<T> : AbstractEnumerableSerializer<ICollectionView<T>, T>
	{
		public CollectionViewSerializer(ISerDes<T> valueSerializer, IUniformSerDes<int> countSerializer = null)
			: base(valueSerializer, countSerializer)
		{
		}

		/// <inheritdoc />
		protected override int GetCount(ICollectionView<T> value) => value.Count;

		/// <inheritdoc />
		protected override ICollectionView<T> DeserializeValues(int countOfValues, Func<Try<byte>> readByte) =>
			this.ValueSerializer.DeserializeMany(countOfValues, readByte).AsCollectionView();

		/// <inheritdoc />
		protected override ICollectionView<T> DeserializeValues(int countOfValues, byte[] buffer, ref int index) =>
			this.ValueSerializer.DeserializeMany(countOfValues, buffer, ref index).AsCollectionView();
	}
}
