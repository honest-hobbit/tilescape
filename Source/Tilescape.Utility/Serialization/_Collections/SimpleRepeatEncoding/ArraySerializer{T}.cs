﻿using System;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class ArraySerializer<T> : AbstractEnumerableInlineSerializer<T[], T>
	{
		public ArraySerializer(ISerDes<T> valueSerializer, IUniformSerDes<int> countSerializer = null)
			: base(valueSerializer, countSerializer)
		{
		}

		/// <inheritdoc />
		protected override int GetCount(T[] value) => value.Length;

		/// <inheritdoc />
		protected override T[] Create(int countOfValues) => new T[countOfValues];

		/// <inheritdoc />
		protected override void DeserializeValuesInline(int countOfValues, Func<Try<byte>> readByte, T[] result) =>
			this.ValueSerializer.DeserializeManyInline(countOfValues, readByte, result);

		/// <inheritdoc />
		protected override void DeserializeValuesInline(int countOfValues, byte[] buffer, ref int index, T[] result) =>
			this.ValueSerializer.DeserializeManyInline(countOfValues, buffer, ref index, result);
	}
}
