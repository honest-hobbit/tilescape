﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Serialization
{
	public static class SerializeCount
	{
		public static IUniformSerDes<int> AsByte => SerializeInt.AsByte;

		public static IUniformSerDes<int> AsSByte => SerializeInt.AsSByte;

		public static IUniformSerDes<int> AsShort => SerializeInt.AsShort;

		public static IUniformSerDes<int> AsUShort => SerializeInt.AsUShort;

		public static IUniformSerDes<int> AsInt => Serializer.Int;

		public static IUniformSerDes<int> AsConstant(int count)
		{
			Require.That(count >= 0);

			return new ConstantValueSerializer<int>(count);
		}
	}
}
