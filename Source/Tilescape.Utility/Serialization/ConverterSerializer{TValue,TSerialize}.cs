﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class ConverterSerializer<TValue, TSerialize> : ISerDes<TValue>
	{
		private readonly ITwoWayConverter<TValue, TSerialize> converter;

		private readonly ISerDes<TSerialize> serializer;

		public ConverterSerializer(ITwoWayConverter<TValue, TSerialize> converter, ISerDes<TSerialize> serializer)
		{
			Require.That(converter != null);
			Require.That(serializer != null);

			this.converter = converter;
			this.serializer = serializer;
		}

		/// <inheritdoc />
		public int GetSerializedLength(TValue value)
		{
			ISerializerContracts.GetSerializedLength(value);

			return this.serializer.GetSerializedLength(this.converter.Convert(value));
		}

		/// <inheritdoc />
		public int Serialize(TValue value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			return this.serializer.Serialize(this.converter.Convert(value), writeByte);
		}

		/// <inheritdoc />
		public int Serialize(TValue value, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, value, buffer, index);

			return this.serializer.Serialize(this.converter.Convert(value), buffer, ref index);
		}

		/// <inheritdoc />
		public TValue Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return this.converter.Convert(this.serializer.Deserialize(readByte));
		}

		/// <inheritdoc />
		public TValue Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			return this.converter.Convert(this.serializer.Deserialize(buffer, ref index));
		}
	}
}
