﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Serialization
{
	/// <summary>
	/// Provides extension methods for the <see cref="IDeserializer{T}"/> interface.
	/// </summary>
	public static class IDeserializerExtensions
	{
		public static T Deserialize<T>(this IDeserializer<T> deserializer, byte[] buffer, int index)
		{
			Require.That(deserializer != null);

			return deserializer.Deserialize(buffer, ref index);
		}

		public static T Deserialize<T>(this IDeserializer<T> deserializer, byte[] buffer)
		{
			Require.That(deserializer != null);

			return deserializer.Deserialize(buffer, 0);
		}
	}
}
