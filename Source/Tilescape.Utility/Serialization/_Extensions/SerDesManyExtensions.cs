﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public static class SerDesManyExtensions
	{
		public static int SerializeMany<T>(this ISerializer<T> serializer, IEnumerable<T> values, byte[] buffer, ref int index)
		{
			Require.That(serializer != null);
			Require.That(values.AllAndSelfNotNull());
			Require.That(buffer != null);

			int startIndex = index;
			foreach (T value in values)
			{
				serializer.Serialize(value, buffer, ref index);
			}

			return index - startIndex;
		}

		public static int SerializeMany<T>(this ISerializer<T> serializer, IEnumerable<T> values, Action<byte> writeByte)
		{
			Require.That(serializer != null);
			Require.That(values.AllAndSelfNotNull());
			Require.That(writeByte != null);

			int serializedLength = 0;
			foreach (T value in values)
			{
				serializedLength += serializer.Serialize(value, writeByte);
			}

			return serializedLength;
		}

		public static T[] DeserializeMany<T>(this IDeserializer<T> deserializer, int count, byte[] buffer, ref int index)
		{
			Require.That(deserializer != null);
			Require.That(count >= 0);
			IDeserializerContracts.Deserialize(buffer, index);

			var result = new T[count];
			DeserializeManyInline(deserializer, count, buffer, ref index, result);
			return result;
		}

		public static T[] DeserializeMany<T>(this IDeserializer<T> deserializer, int count, Func<Try<byte>> readByte)
		{
			Require.That(deserializer != null);
			Require.That(count >= 0);
			IDeserializerContracts.Deserialize(readByte);

			var result = new T[count];
			DeserializeManyInline(deserializer, count, readByte, new T[count]);
			return result;
		}

		public static void DeserializeManyInline<T>(
			this IDeserializer<T> deserializer, int count, byte[] buffer, ref int index, T[] result)
		{
			Require.That(deserializer != null);
			IInlineDeserializerContracts.DeserializeInline(buffer, index, result);
			Require.That(count >= 0);
			Require.That(count <= result.Length);

			for (int currentCount = 0; currentCount < count; currentCount++)
			{
				result[currentCount] = deserializer.Deserialize(buffer, ref index);
			}
		}

		public static void DeserializeManyInline<T>(
			this IDeserializer<T> deserializer, int count, Func<Try<byte>> readByte, T[] result)
		{
			Require.That(deserializer != null);
			IInlineDeserializerContracts.DeserializeInline(readByte, result);
			Require.That(count >= 0);
			Require.That(count <= result.Length);

			for (int currentCount = 0; currentCount < count; currentCount++)
			{
				result[currentCount] = deserializer.Deserialize(readByte);
			}
		}

		public static void DeserializeManyInline<T>(
			this IDeserializer<T> deserializer, int count, byte[] buffer, ref int index, ICollection<T> result)
		{
			Require.That(deserializer != null);
			IInlineDeserializerContracts.DeserializeInline(buffer, index, result);
			Require.That(count >= 0);

			for (int currentCount = 0; currentCount < count; currentCount++)
			{
				result.Add(deserializer.Deserialize(buffer, ref index));
			}
		}

		public static void DeserializeManyInline<T>(
			this IDeserializer<T> deserializer, int count, Func<Try<byte>> readByte, ICollection<T> result)
		{
			Require.That(deserializer != null);
			IInlineDeserializerContracts.DeserializeInline(readByte, result);
			Require.That(count >= 0);

			for (int currentCount = 0; currentCount < count; currentCount++)
			{
				result.Add(deserializer.Deserialize(readByte));
			}
		}
	}
}
