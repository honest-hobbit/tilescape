﻿using System.IO;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public static class StreamExtensions
	{
		public static Try<byte> TryReadByte(this Stream stream)
		{
			Require.That(stream != null);

			var result = stream.ReadByte();
			return result == -1 ? Try.None<byte>() : Try.Value((byte)result);
		}
	}
}
