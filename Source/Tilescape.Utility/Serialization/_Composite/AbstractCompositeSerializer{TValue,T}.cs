﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractCompositeSerializer<TValue, T> : ISerDes<TValue>
	{
		private readonly ISerDes<T> serializer;

		public AbstractCompositeSerializer(ISerDes<T> serializer)
		{
			Require.That(serializer != null);

			this.serializer = serializer;
		}

		/// <inheritdoc />
		public virtual int GetSerializedLength(TValue value)
		{
			ISerializerContracts.GetSerializedLength(value);

			T part = this.DecomposeValue(value);

			return this.serializer.GetSerializedLength(part);
		}

		/// <inheritdoc />
		public int Serialize(TValue value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			T part = this.DecomposeValue(value);

			return this.serializer.Serialize(part, writeByte);
		}

		/// <inheritdoc />
		public int Serialize(TValue value, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, value, buffer, index);

			T part = this.DecomposeValue(value);

			return this.serializer.Serialize(part, buffer, ref index);
		}

		/// <inheritdoc />
		public TValue Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			T part = this.serializer.Deserialize(readByte);

			return this.ComposeValue(part);
		}

		/// <inheritdoc />
		public TValue Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			T part = this.serializer.Deserialize(buffer, ref index);

			return this.ComposeValue(part);
		}

		protected abstract TValue ComposeValue(T part);

		protected abstract T DecomposeValue(TValue value);
	}
}
