﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractCompositeUniformSerializer<TValue, T1, T2, T3, T4, T5, T6, T7> :
		AbstractCompositeSerializer<TValue, T1, T2, T3, T4, T5, T6, T7>, IUniformSerDes<TValue>
	{
		public AbstractCompositeUniformSerializer(
			IUniformSerDes<T1> serializerT1,
			IUniformSerDes<T2> serializerT2,
			IUniformSerDes<T3> serializerT3,
			IUniformSerDes<T4> serializerT4,
			IUniformSerDes<T5> serializerT5,
			IUniformSerDes<T6> serializerT6,
			IUniformSerDes<T7> serializerT7)
			: base(serializerT1, serializerT2, serializerT3, serializerT4, serializerT5, serializerT6, serializerT7)
		{
			Require.That(serializerT1 != null);
			Require.That(serializerT2 != null);
			Require.That(serializerT3 != null);
			Require.That(serializerT4 != null);
			Require.That(serializerT5 != null);
			Require.That(serializerT6 != null);
			Require.That(serializerT7 != null);

			this.SerializedLength =
				serializerT1.SerializedLength +
				serializerT2.SerializedLength +
				serializerT3.SerializedLength +
				serializerT4.SerializedLength +
				serializerT5.SerializedLength +
				serializerT6.SerializedLength +
				serializerT7.SerializedLength;
		}

		/// <inheritdoc />
		public int SerializedLength { get; }

		/// <inheritdoc />
		public sealed override int GetSerializedLength(TValue value) => this.SerializedLength;
	}
}
