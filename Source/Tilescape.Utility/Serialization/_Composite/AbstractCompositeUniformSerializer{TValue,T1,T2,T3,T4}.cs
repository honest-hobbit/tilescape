﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractCompositeUniformSerializer<TValue, T1, T2, T3, T4> :
		AbstractCompositeSerializer<TValue, T1, T2, T3, T4>, IUniformSerDes<TValue>
	{
		public AbstractCompositeUniformSerializer(
			IUniformSerDes<T1> serializerT1,
			IUniformSerDes<T2> serializerT2,
			IUniformSerDes<T3> serializerT3,
			IUniformSerDes<T4> serializerT4)
			: base(serializerT1, serializerT2, serializerT3, serializerT4)
		{
			Require.That(serializerT1 != null);
			Require.That(serializerT2 != null);
			Require.That(serializerT3 != null);
			Require.That(serializerT4 != null);

			this.SerializedLength =
				serializerT1.SerializedLength +
				serializerT2.SerializedLength +
				serializerT3.SerializedLength +
				serializerT4.SerializedLength;
		}

		/// <inheritdoc />
		public int SerializedLength { get; }

		/// <inheritdoc />
		public sealed override int GetSerializedLength(TValue value) => this.SerializedLength;
	}
}
