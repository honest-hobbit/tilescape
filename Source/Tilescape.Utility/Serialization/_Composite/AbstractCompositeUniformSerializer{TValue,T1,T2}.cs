﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractCompositeUniformSerializer<TValue, T1, T2> :
		AbstractCompositeSerializer<TValue, T1, T2>, IUniformSerDes<TValue>
	{
		public AbstractCompositeUniformSerializer(IUniformSerDes<T1> serializerT1, IUniformSerDes<T2> serializerT2)
			: base(serializerT1, serializerT2)
		{
			Require.That(serializerT1 != null);
			Require.That(serializerT2 != null);

			this.SerializedLength = serializerT1.SerializedLength + serializerT2.SerializedLength;
		}

		/// <inheritdoc />
		public int SerializedLength { get; }

		/// <inheritdoc />
		public sealed override int GetSerializedLength(TValue value) => this.SerializedLength;
	}
}
