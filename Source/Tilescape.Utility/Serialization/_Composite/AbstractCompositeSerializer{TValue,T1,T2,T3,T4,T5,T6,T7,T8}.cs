﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractCompositeSerializer<TValue, T1, T2, T3, T4, T5, T6, T7, T8> : ISerDes<TValue>
	{
		private readonly ISerDes<T1> serializerT1;

		private readonly ISerDes<T2> serializerT2;

		private readonly ISerDes<T3> serializerT3;

		private readonly ISerDes<T4> serializerT4;

		private readonly ISerDes<T5> serializerT5;

		private readonly ISerDes<T6> serializerT6;

		private readonly ISerDes<T7> serializerT7;

		private readonly ISerDes<T8> serializerT8;

		public AbstractCompositeSerializer(
			ISerDes<T1> serializerT1,
			ISerDes<T2> serializerT2,
			ISerDes<T3> serializerT3,
			ISerDes<T4> serializerT4,
			ISerDes<T5> serializerT5,
			ISerDes<T6> serializerT6,
			ISerDes<T7> serializerT7,
			ISerDes<T8> serializerT8)
		{
			Require.That(serializerT1 != null);
			Require.That(serializerT2 != null);
			Require.That(serializerT3 != null);
			Require.That(serializerT4 != null);
			Require.That(serializerT5 != null);
			Require.That(serializerT6 != null);
			Require.That(serializerT7 != null);
			Require.That(serializerT8 != null);

			this.serializerT1 = serializerT1;
			this.serializerT2 = serializerT2;
			this.serializerT3 = serializerT3;
			this.serializerT4 = serializerT4;
			this.serializerT5 = serializerT5;
			this.serializerT6 = serializerT6;
			this.serializerT7 = serializerT7;
			this.serializerT8 = serializerT8;
		}

		/// <inheritdoc />
		public virtual int GetSerializedLength(TValue value)
		{
			ISerializerContracts.GetSerializedLength(value);

			T1 part1;
			T2 part2;
			T3 part3;
			T4 part4;
			T5 part5;
			T6 part6;
			T7 part7;
			T8 part8;
			this.DecomposeValue(value, out part1, out part2, out part3, out part4, out part5, out part6, out part7, out part8);

			return this.serializerT1.GetSerializedLength(part1)
				+ this.serializerT2.GetSerializedLength(part2)
				+ this.serializerT3.GetSerializedLength(part3)
				+ this.serializerT4.GetSerializedLength(part4)
				+ this.serializerT5.GetSerializedLength(part5)
				+ this.serializerT6.GetSerializedLength(part6)
				+ this.serializerT7.GetSerializedLength(part7)
				+ this.serializerT8.GetSerializedLength(part8);
		}

		/// <inheritdoc />
		public int Serialize(TValue value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			T1 part1;
			T2 part2;
			T3 part3;
			T4 part4;
			T5 part5;
			T6 part6;
			T7 part7;
			T8 part8;
			this.DecomposeValue(value, out part1, out part2, out part3, out part4, out part5, out part6, out part7, out part8);

			return this.serializerT1.Serialize(part1, writeByte)
				+ this.serializerT2.Serialize(part2, writeByte)
				+ this.serializerT3.Serialize(part3, writeByte)
				+ this.serializerT4.Serialize(part4, writeByte)
				+ this.serializerT5.Serialize(part5, writeByte)
				+ this.serializerT6.Serialize(part6, writeByte)
				+ this.serializerT7.Serialize(part7, writeByte)
				+ this.serializerT8.Serialize(part8, writeByte);
		}

		/// <inheritdoc />
		public int Serialize(TValue value, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, value, buffer, index);

			T1 part1;
			T2 part2;
			T3 part3;
			T4 part4;
			T5 part5;
			T6 part6;
			T7 part7;
			T8 part8;
			this.DecomposeValue(value, out part1, out part2, out part3, out part4, out part5, out part6, out part7, out part8);

			return this.serializerT1.Serialize(part1, buffer, ref index)
				+ this.serializerT2.Serialize(part2, buffer, ref index)
				+ this.serializerT3.Serialize(part3, buffer, ref index)
				+ this.serializerT4.Serialize(part4, buffer, ref index)
				+ this.serializerT5.Serialize(part5, buffer, ref index)
				+ this.serializerT6.Serialize(part6, buffer, ref index)
				+ this.serializerT7.Serialize(part7, buffer, ref index)
				+ this.serializerT8.Serialize(part8, buffer, ref index);
		}

		/// <inheritdoc />
		public TValue Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			T1 part1 = this.serializerT1.Deserialize(readByte);
			T2 part2 = this.serializerT2.Deserialize(readByte);
			T3 part3 = this.serializerT3.Deserialize(readByte);
			T4 part4 = this.serializerT4.Deserialize(readByte);
			T5 part5 = this.serializerT5.Deserialize(readByte);
			T6 part6 = this.serializerT6.Deserialize(readByte);
			T7 part7 = this.serializerT7.Deserialize(readByte);
			T8 part8 = this.serializerT8.Deserialize(readByte);

			return this.ComposeValue(part1, part2, part3, part4, part5, part6, part7, part8);
		}

		/// <inheritdoc />
		public TValue Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			T1 part1 = this.serializerT1.Deserialize(buffer, ref index);
			T2 part2 = this.serializerT2.Deserialize(buffer, ref index);
			T3 part3 = this.serializerT3.Deserialize(buffer, ref index);
			T4 part4 = this.serializerT4.Deserialize(buffer, ref index);
			T5 part5 = this.serializerT5.Deserialize(buffer, ref index);
			T6 part6 = this.serializerT6.Deserialize(buffer, ref index);
			T7 part7 = this.serializerT7.Deserialize(buffer, ref index);
			T8 part8 = this.serializerT8.Deserialize(buffer, ref index);

			return this.ComposeValue(part1, part2, part3, part4, part5, part6, part7, part8);
		}

		protected abstract TValue ComposeValue(T1 part1, T2 part2, T3 part3, T4 part4, T5 part5, T6 part6, T7 part7, T8 part8);

		protected abstract void DecomposeValue(
			TValue value,
			out T1 part1,
			out T2 part2,
			out T3 part3,
			out T4 part4,
			out T5 part5,
			out T6 part6,
			out T7 part7,
			out T8 part8);
	}
}
