﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractCompositeSerializer<TValue, T1, T2> : ISerDes<TValue>
	{
		private readonly ISerDes<T1> serializerT1;

		private readonly ISerDes<T2> serializerT2;

		public AbstractCompositeSerializer(ISerDes<T1> serializerT1, ISerDes<T2> serializerT2)
		{
			Require.That(serializerT1 != null);
			Require.That(serializerT2 != null);

			this.serializerT1 = serializerT1;
			this.serializerT2 = serializerT2;
		}

		/// <inheritdoc />
		public virtual int GetSerializedLength(TValue value)
		{
			ISerializerContracts.GetSerializedLength(value);

			T1 part1;
			T2 part2;
			this.DecomposeValue(value, out part1, out part2);

			return this.serializerT1.GetSerializedLength(part1)
				+ this.serializerT2.GetSerializedLength(part2);
		}

		/// <inheritdoc />
		public int Serialize(TValue value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			T1 part1;
			T2 part2;
			this.DecomposeValue(value, out part1, out part2);

			return this.serializerT1.Serialize(part1, writeByte)
				+ this.serializerT2.Serialize(part2, writeByte);
		}

		/// <inheritdoc />
		public int Serialize(TValue value, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, value, buffer, index);

			T1 part1;
			T2 part2;
			this.DecomposeValue(value, out part1, out part2);

			return this.serializerT1.Serialize(part1, buffer, ref index)
				+ this.serializerT2.Serialize(part2, buffer, ref index);
		}

		/// <inheritdoc />
		public TValue Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			T1 part1 = this.serializerT1.Deserialize(readByte);
			T2 part2 = this.serializerT2.Deserialize(readByte);

			return this.ComposeValue(part1, part2);
		}

		/// <inheritdoc />
		public TValue Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			T1 part1 = this.serializerT1.Deserialize(buffer, ref index);
			T2 part2 = this.serializerT2.Deserialize(buffer, ref index);

			return this.ComposeValue(part1, part2);
		}

		protected abstract TValue ComposeValue(T1 part1, T2 part2);

		protected abstract void DecomposeValue(TValue value, out T1 part1, out T2 part2);
	}
}
