﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Serialization
{
	public abstract class AbstractCompositeUniformSerializer<TValue, T> :
		AbstractCompositeSerializer<TValue, T>, IUniformSerDes<TValue>
	{
		public AbstractCompositeUniformSerializer(IUniformSerDes<T> serializer)
			: base(serializer)
		{
			Require.That(serializer != null);

			this.SerializedLength = serializer.SerializedLength;
		}

		/// <inheritdoc />
		public int SerializedLength { get; }

		/// <inheritdoc />
		public sealed override int GetSerializedLength(TValue value) => this.SerializedLength;
	}
}
