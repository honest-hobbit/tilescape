﻿namespace Tilescape.Utility.Serialization
{
	public interface IUniformSerializer<T> : ISerializer<T>, IUniformSerializedLength
	{
	}
}
