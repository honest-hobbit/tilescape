﻿namespace Tilescape.Utility.Serialization
{
	public interface IUniformSerDes<T> : ISerDes<T>, IUniformSerializer<T>, IUniformDeserializer<T>
	{
	}
}
