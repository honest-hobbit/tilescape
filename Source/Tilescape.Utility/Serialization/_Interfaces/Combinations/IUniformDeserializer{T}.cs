﻿namespace Tilescape.Utility.Serialization
{
	public interface IUniformDeserializer<T> : IDeserializer<T>, IUniformSerializedLength
	{
	}
}
