﻿namespace Tilescape.Utility.Serialization
{
	public interface IUniformInlineDeserializer<T> : IInlineDeserializer<T>, IUniformSerializedLength
	{
	}
}
