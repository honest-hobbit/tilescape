﻿namespace Tilescape.Utility.Serialization
{
	public interface IInlineSerDes<T> : ISerializer<T>, IInlineDeserializer<T>
	{
	}
}
