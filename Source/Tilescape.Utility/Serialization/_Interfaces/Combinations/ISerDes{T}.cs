﻿namespace Tilescape.Utility.Serialization
{
	public interface ISerDes<T> : ISerializer<T>, IDeserializer<T>
	{
	}
}
