﻿namespace Tilescape.Utility.Serialization
{
	public interface IUniformInlineSerDes<T> : IUniformInlineDeserializer<T>, IInlineSerDes<T>
	{
	}
}
