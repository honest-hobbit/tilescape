﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Serialization
{
	public static class ISerializerContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void GetSerializedLength<T>(T value)
		{
			Require.That(value != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Serialize<T>(ISerializer<T> instance, T value, byte[] buffer, int index)
		{
			Require.That(instance != null);
			Require.That(value != null);
			Require.That(buffer != null);
			Require.That(index >= 0);
			Require.That(instance.GetSerializedLength(value) + index <= buffer.Length);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Serialize<T>(T value, Action<byte> writeByte)
		{
			Require.That(value != null);
			Require.That(writeByte != null);
		}
	}
}
