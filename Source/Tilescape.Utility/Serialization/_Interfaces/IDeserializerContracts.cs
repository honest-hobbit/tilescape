﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public static class IDeserializerContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Deserialize(byte[] buffer, int index)
		{
			Require.That(buffer != null);

			// Index must either be a valid index into the buffer or be the very next index after the
			// end of the buffer, which must be interpreted as reaching the end of the buffer, otherwise
			// attempting to access that index will result in an ArgumentOutOfRangeException being thrown.
			// This extra fringe case is allowed to support 0 length serialized objects still being able
			// to be deserialized from the end of the buffer.
			Require.That(buffer.IsIndexInBounds(index) || index == buffer.Length);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Deserialize(Func<Try<byte>> readByte)
		{
			Require.That(readByte != null);
		}
	}
}
