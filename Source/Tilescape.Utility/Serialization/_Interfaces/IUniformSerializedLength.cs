﻿namespace Tilescape.Utility.Serialization
{
	public interface IUniformSerializedLength
	{
		int SerializedLength { get; }
	}
}
