﻿using System;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public interface IDeserializer<T>
	{
		T Deserialize(byte[] buffer, ref int index);

		T Deserialize(Func<Try<byte>> readByte);
	}
}
