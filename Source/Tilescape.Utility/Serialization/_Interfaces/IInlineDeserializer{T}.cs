﻿using System;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public interface IInlineDeserializer<T>
	{
		void DeserializeInline(byte[] buffer, ref int index, T result);

		void DeserializeInline(Func<Try<byte>> readByte, T result);
	}
}
