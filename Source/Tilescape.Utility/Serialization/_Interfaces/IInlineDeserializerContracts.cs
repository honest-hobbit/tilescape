﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public static class IInlineDeserializerContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void DeserializeInline<T>(byte[] buffer, int index, T result)
		{
			IDeserializerContracts.Deserialize(buffer, index);
			Require.That(result != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void DeserializeInline<T>(Func<Try<byte>> readByte, T result)
		{
			IDeserializerContracts.Deserialize(readByte);
			Require.That(result != null);
		}
	}
}
