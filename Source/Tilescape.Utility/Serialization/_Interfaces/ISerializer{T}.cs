﻿using System;

namespace Tilescape.Utility.Serialization
{
	public interface ISerializer<T>
	{
		int GetSerializedLength(T value);

		/// <summary>
		/// Serializes the specified object, storing the result in the specified byte array.
		/// </summary>
		/// <param name="value">The value to serialize.</param>
		/// <param name="buffer">The byte array to store the serialized object in.</param>
		/// <param name="index">Index of the array to begin storing the object at.</param>
		/// <returns>
		/// The length in bytes of the serialized object. This is how many elements of the
		/// <paramref name="buffer"/> are used to store the serialized object.
		/// </returns>
		int Serialize(T value, byte[] buffer, ref int index);

		int Serialize(T value, Action<byte> writeByte);
	}
}
