﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using MiscUtil.Conversion;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public static class Serializer
	{
		public static IUniformSerDes<bool> Bool { get; } = new BoolSerializer();

		public static IUniformSerDes<char> Char { get; } = new CharSerializer();

		public static IUniformSerDes<float> Float { get; } = new FloatSerializer();

		public static IUniformSerDes<double> Double { get; } = new DoubleSerializer();

		public static IUniformSerDes<decimal> Decimal { get; } = new DecimalSerializer();

		public static IUniformSerDes<byte> Byte { get; } = new ByteSerializer();

		public static IUniformSerDes<sbyte> SByte { get; } = new SByteSerializer();

		public static IUniformSerDes<short> Short { get; } = new ShortSerializer();

		public static IUniformSerDes<ushort> UShort { get; } = new UShortSerializer();

		public static IUniformSerDes<int> Int { get; } = new IntSerializer();

		public static IUniformSerDes<uint> UInt { get; } = new UIntSerializer();

		public static IUniformSerDes<long> Long { get; } = new LongSerializer();

		public static IUniformSerDes<ulong> ULong { get; } = new ULongSerializer();

		public static IUniformSerDes<Guid> Guid { get; } = new GuidSerializer();

		public static IUniformSerDes<DateTime> DateTime { get; } = new DateTimeSerializer();

		#region Private Classes

		private abstract class AbstractSerializer : IUniformSerializedLength
		{
			[ThreadStatic]
			private static byte[] threadLocalBuffer = null;

			/// <inheritdoc />
			public abstract int SerializedLength { get; }

			protected static byte[] ThreadLocalBuffer
			{
				get
				{
					if (threadLocalBuffer == null)
					{
						threadLocalBuffer = new byte[Length.OfLongestPrimitive.InBytes];
					}

					return threadLocalBuffer;
				}
			}

			protected static EndianBitConverter Converter => EndianBitConverter.Little;

			// must only ever be called after accessing ThreadLocalBuffer to ensure the ThreadStatic field is initialized
			protected void WriteBytes(Action<byte> writeByte)
			{
				Require.That(writeByte != null);
				Require.That(threadLocalBuffer != null);

				for (int index = 0; index < this.SerializedLength; index++)
				{
					writeByte(threadLocalBuffer[index]);
				}
			}

			protected byte[] BufferNext(Func<Try<byte>> readByte)
			{
				// accessing buffer through ThreadLocalBuffer to ensure the ThreadStatic field is initialized
				var buffer = ThreadLocalBuffer;
				for (int index = 0; index < this.SerializedLength; index++)
				{
					var next = readByte();
					if (next.HasValue)
					{
						buffer[index] = next.Value;
					}
					else
					{
						throw new EndOfStreamException();
					}
				}

				return buffer;
			}
		}

		private class BoolSerializer : AbstractSerializer, IUniformSerDes<bool>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfBool.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(bool value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(bool value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(bool value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				var buffer = ThreadLocalBuffer;
				Converter.CopyBytes(value, buffer, 0);
				writeByte(buffer[0]);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public bool Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToBoolean(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public bool Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToBoolean(this.BufferNext(readByte), 0);
			}
		}

		private class CharSerializer : AbstractSerializer, IUniformSerDes<char>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfChar.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(char value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(char value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(char value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public char Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToChar(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public char Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToChar(this.BufferNext(readByte), 0);
			}
		}

		private class FloatSerializer : AbstractSerializer, IUniformSerDes<float>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfFloat.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(float value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(float value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(float value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public float Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToSingle(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public float Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToSingle(this.BufferNext(readByte), 0);
			}
		}

		private class DoubleSerializer : AbstractSerializer, IUniformSerDes<double>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfDouble.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(double value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(double value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(double value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public double Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToDouble(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public double Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToDouble(this.BufferNext(readByte), 0);
			}
		}

		private class DecimalSerializer : AbstractSerializer, IUniformSerDes<decimal>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfDecimal.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(decimal value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(decimal value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(decimal value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public decimal Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToDecimal(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public decimal Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToDecimal(this.BufferNext(readByte), 0);
			}
		}

		private class ByteSerializer : AbstractSerializer, IUniformSerDes<byte>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfByte.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(byte value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(byte value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				buffer[index] = value;
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(byte value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				writeByte(value);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public byte Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = buffer[index];
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public byte Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return this.BufferNext(readByte)[0];
			}
		}

		private class SByteSerializer : AbstractSerializer, IUniformSerDes<sbyte>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfSByte.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(sbyte value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(sbyte value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				buffer[index] = new ByteUnion(value).AsByte;
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(sbyte value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				writeByte(new ByteUnion(value).AsByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public sbyte Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = new ByteUnion(buffer[index]).AsSByte;
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public sbyte Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return new ByteUnion(this.BufferNext(readByte)[0]).AsSByte;
			}

			[StructLayout(LayoutKind.Explicit, Size = 1)]
			private struct ByteUnion
			{
				[FieldOffset(0)]
				private readonly byte byteField;

				[FieldOffset(0)]
				private readonly sbyte sbyteField;

				public ByteUnion(byte byteField)
				{
					this.sbyteField = 0; // compiler requires this, must come first not to overwrite real value
					this.byteField = byteField;
				}

				public ByteUnion(sbyte sbyteField)
				{
					this.byteField = 0; // compiler requires this, must come first not to overwrite real value
					this.sbyteField = sbyteField;
				}

				public byte AsByte => this.byteField;

				public sbyte AsSByte => this.sbyteField;
			}
		}

		private class ShortSerializer : AbstractSerializer, IUniformSerDes<short>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfShort.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(short value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(short value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(short value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public short Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToInt16(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public short Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToInt16(this.BufferNext(readByte), 0);
			}
		}

		private class UShortSerializer : AbstractSerializer, IUniformSerDes<ushort>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfUShort.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(ushort value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(ushort value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(ushort value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public ushort Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToUInt16(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public ushort Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToUInt16(this.BufferNext(readByte), 0);
			}
		}

		private class IntSerializer : AbstractSerializer, IUniformSerDes<int>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfInt.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(int value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(int value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(int value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToInt32(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public int Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToInt32(this.BufferNext(readByte), 0);
			}
		}

		private class UIntSerializer : AbstractSerializer, IUniformSerDes<uint>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfUInt.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(uint value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(uint value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(uint value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public uint Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToUInt32(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public uint Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToUInt32(this.BufferNext(readByte), 0);
			}
		}

		private class LongSerializer : AbstractSerializer, IUniformSerDes<long>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfLong.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(long value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(long value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(long value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public long Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToInt64(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public long Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToInt64(this.BufferNext(readByte), 0);
			}
		}

		private class ULongSerializer : AbstractSerializer, IUniformSerDes<ulong>
		{
			/// <inheritdoc />
			public override int SerializedLength => Length.OfULong.InBytes;

			/// <inheritdoc />
			public int GetSerializedLength(ulong value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(ulong value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				Converter.CopyBytes(value, buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(ulong value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				Converter.CopyBytes(value, ThreadLocalBuffer, 0);
				this.WriteBytes(writeByte);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public ulong Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = Converter.ToUInt64(buffer, index);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public ulong Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return Converter.ToUInt64(this.BufferNext(readByte), 0);
			}
		}

		private class GuidSerializer : IUniformSerDes<Guid>
		{
			/// <inheritdoc />
			public int SerializedLength => 16;

			/// <inheritdoc />
			public int GetSerializedLength(Guid value) => this.SerializedLength;

			/// <inheritdoc />
			public int Serialize(Guid value, byte[] buffer, ref int index)
			{
				ISerializerContracts.Serialize(this, value, buffer, index);

				value.CopyTo(buffer, index);
				index += this.SerializedLength;
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public int Serialize(Guid value, Action<byte> writeByte)
			{
				ISerializerContracts.Serialize(value, writeByte);

				var buffer = new GuidBytes(value);
				writeByte(buffer.Byte0);
				writeByte(buffer.Byte1);
				writeByte(buffer.Byte2);
				writeByte(buffer.Byte3);
				writeByte(buffer.Byte4);
				writeByte(buffer.Byte5);
				writeByte(buffer.Byte6);
				writeByte(buffer.Byte7);
				writeByte(buffer.Byte8);
				writeByte(buffer.Byte9);
				writeByte(buffer.Byte10);
				writeByte(buffer.Byte11);
				writeByte(buffer.Byte12);
				writeByte(buffer.Byte13);
				writeByte(buffer.Byte14);
				writeByte(buffer.Byte15);
				return this.SerializedLength;
			}

			/// <inheritdoc />
			public Guid Deserialize(byte[] buffer, ref int index)
			{
				IDeserializerContracts.Deserialize(buffer, index);

				var result = GuidUtility.FromBytes(
					buffer[index],
					buffer[index + 1],
					buffer[index + 2],
					buffer[index + 3],
					buffer[index + 4],
					buffer[index + 5],
					buffer[index + 6],
					buffer[index + 7],
					buffer[index + 8],
					buffer[index + 9],
					buffer[index + 10],
					buffer[index + 11],
					buffer[index + 12],
					buffer[index + 13],
					buffer[index + 14],
					buffer[index + 15]);
				index += this.SerializedLength;
				return result;
			}

			/// <inheritdoc />
			public Guid Deserialize(Func<Try<byte>> readByte)
			{
				IDeserializerContracts.Deserialize(readByte);

				return GuidUtility.FromBytes(
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value,
					readByte().Value);
			}
		}

		private class DateTimeSerializer : AbstractCompositeUniformSerializer<DateTime, long>
		{
			public DateTimeSerializer()
				: base(Long)
			{
			}

			/// <inheritdoc />
			protected override DateTime ComposeValue(long ticks) => new DateTime(ticks);

			/// <inheritdoc />
			protected override long DecomposeValue(DateTime value) => value.Ticks;
		}

		#endregion
	}
}
