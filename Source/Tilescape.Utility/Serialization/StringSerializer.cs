﻿using System;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Serialization
{
	public class StringSerializer : AbstractEnumerableSerializer<string, char>
	{
		public StringSerializer(IUniformSerDes<char> charSerializer, IUniformSerDes<int> countSerializer = null)
			: base(charSerializer, countSerializer)
		{
		}

		public static ISerDes<string> LengthAsByte { get; } = new StringSerializer(Serializer.Char, SerializeCount.AsByte);

		public static ISerDes<string> LengthAsUShort { get; } = new StringSerializer(Serializer.Char, SerializeCount.AsUShort);

		public static ISerDes<string> LengthAsInt { get; } = new StringSerializer(Serializer.Char, SerializeCount.AsInt);

		/// <inheritdoc />
		protected override int GetCount(string value) => value.Length;

		/// <inheritdoc />
		protected override string DeserializeValues(int countOfValues, byte[] buffer, ref int index)
		{
			char[] chars = new char[countOfValues];
			for (int count = 0; count < countOfValues; count++)
			{
				chars[count] = this.ValueSerializer.Deserialize(buffer, ref index);
			}

			return new string(chars);
		}

		/// <inheritdoc />
		protected override string DeserializeValues(int countOfValues, Func<Try<byte>> readByte)
		{
			char[] chars = new char[countOfValues];
			for (int count = 0; count < countOfValues; count++)
			{
				chars[count] = this.ValueSerializer.Deserialize(readByte);
			}

			return new string(chars);
		}
	}
}
