﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Caching
{
	public static class ICacheValueContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void AddExpiration<T>(ICacheValue<T> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}
	}
}
