﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Caching
{
	public static class ICacheExtensions
	{
		public static bool IsPinned<TKey, TValue>(this ICache<TKey, TValue> cache, TKey key)
		{
			Require.That(cache != null);
			Require.That(key != null);

			return cache.TryGet(key, out var pin);
		}

		public static int PinCount<TKey, TValue>(this ICache<TKey, TValue> cache, TKey key)
		{
			Require.That(cache != null);
			Require.That(key != null);

			return cache.TryGet(key, out var pin) ? pin.PinCount : 0;
		}

		public static void Pin<TKey, TValue>(this ICache<TKey, TValue> cache, TKey key)
		{
			Require.That(cache != null);
			Require.That(key != null);

			cache.PinAndGet(key);
		}

		public static void Unpin<TKey, TValue>(this ICache<TKey, TValue> cache, TKey key)
		{
			Require.That(cache != null);
			Require.That(key != null);
			Require.That(cache.IsPinned(key));

			cache[key].Unpin();
		}
	}
}
