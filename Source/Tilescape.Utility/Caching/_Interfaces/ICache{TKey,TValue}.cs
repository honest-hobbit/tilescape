﻿namespace Tilescape.Utility.Caching
{
	public interface ICache<TKey, TValue>
	{
		// must already be pinned before calling this (this does not pin)
		ICachePin<TKey, TValue> this[TKey key] { get; }

		ICachePin<TKey, TValue> PinAndGet(TKey key);

		// only returns pin if it already has at least 1 pin
		bool TryGet(TKey key, out ICachePin<TKey, TValue> pin);

		void ClearUnpinned();

		void ClearAll();
	}
}
