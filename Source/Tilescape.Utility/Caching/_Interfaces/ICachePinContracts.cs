﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Caching
{
	public static class ICachePinContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Value<TKey, TValue>(ICachePin<TKey, TValue> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(instance.PinCount > 0);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Pin<TKey, TValue>(ICachePin<TKey, TValue> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Unpin<TKey, TValue>(ICachePin<TKey, TValue> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(instance.PinCount > 0);
		}
	}
}
