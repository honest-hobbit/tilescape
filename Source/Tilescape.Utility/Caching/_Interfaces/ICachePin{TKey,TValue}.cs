﻿using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Caching
{
	public interface ICachePin<TKey, TValue> : IKeyed<TKey>, IDisposed
	{
		TValue Value { get; }

		int PinCount { get; }

		bool IsPinned { get; }

		void Pin();

		void Unpin();
	}
}
