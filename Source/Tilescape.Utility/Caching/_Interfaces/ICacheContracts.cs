﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Caching
{
	public static class ICacheContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<TKey, TValue>(ICache<TKey, TValue> instance, TKey key)
		{
			Require.That(instance != null);
			Require.That(key != null);
			Require.That(instance.IsPinned(key));
		}

		[Conditional(Require.CompilationSymbol)]
		public static void PinAndGet<TKey>(TKey key)
		{
			Require.That(key != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void TryGet<TKey>(TKey key)
		{
			Require.That(key != null);
		}
	}
}
