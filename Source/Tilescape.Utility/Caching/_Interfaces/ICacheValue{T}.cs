﻿using Tilescape.Utility.Disposables;

namespace Tilescape.Utility.Caching
{
	public interface ICacheValue<T> : IDisposableValue<T>
	{
		void AddExpiration(ExpiryToken expiration);
	}
}
