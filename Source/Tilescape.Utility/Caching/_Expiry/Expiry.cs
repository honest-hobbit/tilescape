﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Pooling;

namespace Tilescape.Utility.Caching
{
	public static class Expiry
	{
		public static ExpiryCounter CreateCounter(int maxCount, double expiryCapacityMultiplier = 1)
		{
			Require.That(maxCount >= 0);
			Require.That(expiryCapacityMultiplier >= 1);

			return new ExpiryCounter(maxCount, GetExpiryCapacity(maxCount, expiryCapacityMultiplier));
		}

		public static ExpiryPool<T> CreatePool<T>(
			Func<T> factory,
			int boundedCapacity,
			Action<T> initialize = null,
			Action<T> reset = null,
			double expiryCapacityMultiplier = 1)
		{
			Require.That(factory != null);
			Require.That(boundedCapacity >= 1);
			Require.That(expiryCapacityMultiplier >= 1);

			return new ExpiryPool<T>(
				new Pool<T>(factory, initialize, reset, boundedCapacity),
				GetExpiryCapacity(boundedCapacity, expiryCapacityMultiplier));
		}

		private static int GetExpiryCapacity(int capacity, double multiplier)
		{
			Require.That(capacity >= 1);
			Require.That(multiplier >= 1);

			return (capacity * multiplier).ClampToInt();
		}
	}
}
