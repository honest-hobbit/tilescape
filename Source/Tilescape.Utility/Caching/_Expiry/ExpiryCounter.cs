﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Caching
{
	public class ExpiryCounter : AbstractExpiryQueue
	{
		public ExpiryCounter(int maxCount, int expiryCapacity)
			: base(expiryCapacity)
		{
			Require.That(maxCount >= 0);
			Require.That(expiryCapacity >= maxCount);

			this.MaxCount = maxCount;
			this.Count = maxCount;
		}

		public int MaxCount { get; }

		public int Count { get; private set; }

		public void Increment()
		{
			Require.That(this.Count < this.MaxCount);

			this.Count++;
		}

		public void Decrement()
		{
			while (this.Count <= 0 && this.TryExpireToken())
			{
			}

			this.Count--;
		}

		public override void AddExpiration(ExpiryToken expiration)
		{
			if (this.Count < 0)
			{
				expiration.TryDisposeCacheValue();
				return;
			}

			base.AddExpiration(expiration);
		}
	}
}
