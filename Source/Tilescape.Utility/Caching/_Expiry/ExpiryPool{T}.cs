﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Pooling;

namespace Tilescape.Utility.Caching
{
	public class ExpiryPool<T> : AbstractExpiryQueue, IPool<T>
	{
		private readonly IPool<T> pool;

		public ExpiryPool(IPool<T> pool, int expiryCapacity)
			: base(expiryCapacity)
		{
			Require.That(pool != null);

			this.pool = pool;
		}

		/// <inheritdoc />
		public int AvailableCount => this.pool.AvailableCount;

		/// <inheritdoc />
		public int BoundedCapacity => this.pool.BoundedCapacity;

		/// <inheritdoc />
		public T Take() => this.TryTake(out var value) ? value : this.pool.Take();

		/// <inheritdoc />
		public bool TryTake(out T value)
		{
			while (true)
			{
				if (this.pool.TryTake(out value))
				{
					return true;
				}
				else
				{
					// unable to TryTake from the pool so try disposing a token
					// to release a value back to the pool
					if (!this.TryExpireToken())
					{
						// unable to release any more values back to the pool
						// so TryTake can't succeed
						return false;
					}
				}
			}
		}

		/// <inheritdoc />
		public bool TryGive(T value) => this.pool.TryGive(value);
	}
}
