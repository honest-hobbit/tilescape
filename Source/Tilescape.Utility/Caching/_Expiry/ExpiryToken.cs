﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Caching
{
	public struct ExpiryToken
	{
		private readonly ICacheEntry entry;

		internal ExpiryToken(ICacheEntry entry, int tokenVersion)
		{
			Require.That(entry != null);

			this.entry = entry;
			this.TokenVersion = tokenVersion;
		}

		public bool IsTokenExpired => this.entry.IsTokenExpired(this);

		public int TokenVersion { get; }

		public bool TryDisposeCacheValue() => this.entry.TryDispose(this);
	}
}
