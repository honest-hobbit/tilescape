﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Caching
{
	public abstract class AbstractExpiryQueue
	{
		private readonly CompactingQueue<ExpiryToken> expiry;

		public AbstractExpiryQueue(int expiryCapacity)
		{
			Require.That(expiryCapacity >= 0);

			this.expiry = new CompactingQueue<ExpiryToken>(x => x.IsTokenExpired, expiryCapacity);
		}

		public virtual void AddExpiration(ExpiryToken expiration)
		{
			while (!this.expiry.TryAdd(expiration))
			{
				// new expiration wasn't able to fit in expiry, so dispose an old token to make room
				if (!this.TryExpireToken())
				{
					// no more old tokens could be diposed so dispose new expiration now
					expiration.TryDisposeCacheValue();
					return;
				}
			}
		}

		public void ExpireAll()
		{
			while (this.TryExpireToken())
			{
			}

			this.expiry.Clear();
		}

		protected bool TryExpireToken()
		{
			while (this.expiry.TryTake(out var token))
			{
				if (token.TryDisposeCacheValue())
				{
					return true;
				}
			}

			return false;
		}
	}
}
