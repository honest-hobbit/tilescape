﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;

namespace Tilescape.Utility.Caching
{
	public class Cache<TKey, TValue> : ICache<TKey, TValue>
	{
		private readonly Func<TKey, ICacheValue<TValue>> factory;

		private readonly IDictionary<TKey, Entry> entries;

		public Cache(Func<TKey, ICacheValue<TValue>> factory, IEqualityComparer<TKey> comparer = null)
		{
			Require.That(factory != null);

			this.factory = factory;
			this.entries = new Dictionary<TKey, Entry>(comparer);
		}

		/// <inheritdoc />
		public ICachePin<TKey, TValue> this[TKey key]
		{
			get
			{
				ICacheContracts.Indexer(this, key);

				return this.entries[key];
			}
		}

		/// <inheritdoc />
		public ICachePin<TKey, TValue> PinAndGet(TKey key)
		{
			ICacheContracts.PinAndGet(key);

			if (this.entries.TryGetValue(key, out var entry))
			{
				entry.Pin();
				return entry;
			}
			else
			{
				entry = new Entry(this, key, this.factory(key));
				entry.Pin();
				this.entries[key] = entry;
				return entry;
			}
		}

		/// <inheritdoc />
		public bool TryGet(TKey key, out ICachePin<TKey, TValue> pin)
		{
			ICacheContracts.PinAndGet(key);

			if (this.entries.TryGetValue(key, out var entry) && entry.IsPinned)
			{
				pin = entry;
				return true;
			}
			else
			{
				pin = null;
				return false;
			}
		}

		/// <inheritdoc />
		public void ClearUnpinned()
		{
			var unpinned = new List<Entry>(this.entries.Count);
			foreach (var entry in this.entries.Values)
			{
				if (!entry.IsPinned)
				{
					unpinned.Add(entry);
				}
			}

			foreach (var entry in unpinned)
			{
				this.entries.Remove(entry.Key);
				entry.Dispose();
			}
		}

		/// <inheritdoc />
		public void ClearAll()
		{
			foreach (var entry in this.entries.Values)
			{
				entry.Dispose();
			}

			this.entries.Clear();
		}

		private class Entry : AbstractDisposable, ICacheEntry, ICachePin<TKey, TValue>
		{
			private readonly Cache<TKey, TValue> cache;

			private readonly ICacheValue<TValue> value;

			private int tokenVersion = int.MinValue;

			public Entry(Cache<TKey, TValue> cache, TKey key, ICacheValue<TValue> value)
			{
				Require.That(cache != null);
				Require.That(key != null);
				Require.That(value != null);

				this.cache = cache;
				this.Key = key;
				this.value = value;
			}

			/// <inheritdoc />
			public TKey Key { get; }

			/// <inheritdoc />
			public TValue Value
			{
				get
				{
					ICachePinContracts.Value(this);

					return this.value.Value;
				}
			}

			/// <inheritdoc />
			public int PinCount { get; private set; } = 0;

			/// <inheritdoc />
			public bool IsPinned => this.PinCount > 0;

			/// <inheritdoc />
			public bool IsTokenExpired(ExpiryToken expiration) => expiration.TokenVersion != this.tokenVersion;

			/// <inheritdoc />
			public bool TryDispose(ExpiryToken expiration)
			{
				if (this.IsTokenExpired(expiration))
				{
					return false;
				}

				Assert.That(
					this.PinCount == 0,
					$"{nameof(this.PinCount)} should be 0 because the {nameof(ExpiryToken)} is not expired.");

				this.cache.entries.Remove(this.Key);
				this.Dispose();
				return true;
			}

			/// <inheritdoc />
			public void Pin()
			{
				ICachePinContracts.Pin(this);

				if (this.PinCount == 0)
				{
					this.tokenVersion++;
				}

				this.PinCount++;
			}

			/// <inheritdoc />
			public void Unpin()
			{
				ICachePinContracts.Unpin(this);

				this.PinCount--;
				if (this.PinCount == 0)
				{
					this.value.AddExpiration(new ExpiryToken(this, this.tokenVersion));
				}
			}

			/// <inheritdoc />
			protected override void ManagedDisposal()
			{
				// incrementing token version causes all past tokens to become expired
				// and no new tokens can be given out now that we are disposed
				this.tokenVersion++;
				this.value.Dispose();
			}
		}
	}
}
