﻿namespace Tilescape.Utility.Caching
{
	internal interface ICacheEntry
	{
		bool IsTokenExpired(ExpiryToken expiration);

		bool TryDispose(ExpiryToken expiration);
	}
}
