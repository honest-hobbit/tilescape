﻿using System.Collections.Generic;

namespace Tilescape.Utility.Collections
{
	public interface ILargeCollectionView<T> : IEnumerable<T>
	{
		long Count { get; }
	}
}
