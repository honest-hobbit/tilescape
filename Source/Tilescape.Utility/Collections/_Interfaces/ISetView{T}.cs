﻿namespace Tilescape.Utility.Collections
{
	public interface ISetView<T> : ICollectionView<T>
	{
		bool Contains(T value);
	}
}
