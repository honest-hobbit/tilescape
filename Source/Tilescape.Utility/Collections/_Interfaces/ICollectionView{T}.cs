﻿using System.Collections.Generic;

namespace Tilescape.Utility.Collections
{
	public interface ICollectionView<T> : IEnumerable<T>
	{
		int Count { get; }
	}
}
