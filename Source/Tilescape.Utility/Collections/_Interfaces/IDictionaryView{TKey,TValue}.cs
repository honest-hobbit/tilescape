﻿using System.Collections.Generic;

namespace Tilescape.Utility.Collections
{
	public interface IDictionaryView<TKey, TValue> : ICollectionView<KeyValuePair<TKey, TValue>>
	{
		ISetView<TKey> Keys { get; }

		ICollectionView<TValue> Values { get; }

		TValue this[TKey key] { get; }

		bool ContainsKey(TKey key);

		bool TryGetValue(TKey key, out TValue value);
	}
}
