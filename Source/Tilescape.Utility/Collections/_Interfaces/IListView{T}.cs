﻿namespace Tilescape.Utility.Collections
{
	public interface IListView<T> : ICollectionView<T>
	{
		T this[int index] { get; }
	}
}
