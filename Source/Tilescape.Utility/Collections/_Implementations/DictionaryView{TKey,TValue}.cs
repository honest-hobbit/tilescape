﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public class DictionaryView<TKey, TValue> : IDictionaryView<TKey, TValue>, IDictionary<TKey, TValue>
	{
		private readonly IDictionary<TKey, TValue> dictionary;

		private readonly IDictionaryView<TKey, TValue> view;

		public DictionaryView(IDictionary<TKey, TValue> dictionary)
		{
			Require.That(dictionary != null);

			this.dictionary = dictionary;
			this.view = dictionary.AsDictionaryView();
		}

		public DictionaryView()
			: this(new Dictionary<TKey, TValue>())
		{
		}

		public DictionaryView(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
			: this(new Dictionary<TKey, TValue>(dictionary, comparer))
		{
		}

		public DictionaryView(IEqualityComparer<TKey> comparer)
			: this(new Dictionary<TKey, TValue>(comparer))
		{
		}

		public DictionaryView(int capacity)
			: this(new Dictionary<TKey, TValue>(capacity))
		{
		}

		public DictionaryView(int capacity, IEqualityComparer<TKey> comparer)
			: this(new Dictionary<TKey, TValue>(capacity, comparer))
		{
		}

		/// <inheritdoc />
		public int Count => this.dictionary.Count;

		/// <inheritdoc />
		public ISetView<TKey> Keys => this.view.Keys;

		/// <inheritdoc />
		public ICollectionView<TValue> Values => this.view.Values;

		/// <inheritdoc />
		ICollection<TKey> IDictionary<TKey, TValue>.Keys => this.dictionary.Keys;

		/// <inheritdoc />
		ICollection<TValue> IDictionary<TKey, TValue>.Values => this.dictionary.Values;

		/// <inheritdoc />
		public bool IsReadOnly => this.dictionary.IsReadOnly;

		/// <inheritdoc />
		TValue IDictionaryView<TKey, TValue>.this[TKey key] => this.view[key];

		/// <inheritdoc />
		public TValue this[TKey key]
		{
			get { return this.dictionary[key]; }
			set { this.dictionary[key] = value; }
		}

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

		/// <inheritdoc />
		public bool Contains(KeyValuePair<TKey, TValue> item) => this.dictionary.Contains(item);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value) => this.dictionary.TryGetValue(key, out value);

		/// <inheritdoc />
		public void Add(TKey key, TValue value) => this.dictionary.Add(key, value);

		/// <inheritdoc />
		public void Add(KeyValuePair<TKey, TValue> item) => this.dictionary.Add(item);

		/// <inheritdoc />
		public bool Remove(TKey key) => this.dictionary.Remove(key);

		/// <inheritdoc />
		public bool Remove(KeyValuePair<TKey, TValue> item) => this.dictionary.Remove(item);

		/// <inheritdoc />
		public void Clear() => this.dictionary.Clear();

		/// <inheritdoc />
		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => this.dictionary.CopyTo(array, arrayIndex);

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.dictionary.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
