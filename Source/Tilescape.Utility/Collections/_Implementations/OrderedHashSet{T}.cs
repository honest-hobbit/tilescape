﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public class OrderedHashSet<T> : KeyedCollection<T, T>, ISetView<T>
	{
		public OrderedHashSet()
			: base()
		{
		}

		public OrderedHashSet(IEnumerable<T> values)
			: base()
		{
			Require.That(values != null);

			this.AddMany(values);
		}

		public OrderedHashSet(IEnumerable<T> values, IEqualityComparer<T> comparer)
			: base(comparer)
		{
			Require.That(values != null);

			this.AddMany(values);
		}

		public OrderedHashSet(IEqualityComparer<T> comparer)
			: base(comparer)
		{
		}

		public new bool Add(T item)
		{
			if (this.Contains(item))
			{
				return false;
			}
			else
			{
				base.Add(item);
				return true;
			}
		}

		/// <inheritdoc />
		protected override T GetKeyForItem(T item) => item;
	}
}
