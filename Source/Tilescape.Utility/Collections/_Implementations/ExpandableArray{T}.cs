﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Utility.Collections
{
	public class ExpandableArray<T>
	{
		public ExpandableArray(int initialLength, int maxLength = Capacity.Unbounded)
		{
			Require.That(initialLength >= 0);
			Require.That(maxLength >= initialLength || maxLength == Capacity.Unbounded);

			this.Array = new T[initialLength];
			this.MaxLength = maxLength;
		}

		public T[] Array { get; private set; }

		public int MaxLength { get; }

		// EnsureCapacity will expand the array if needed but never shrink it
		// it does not set the capacity to the exact amount specified, instead it uses a doubling algorithm
		// does not copy values from old array to new one
		public void EnsureCapacity(int capacity)
		{
			Require.That(capacity >= 0);
			Require.That(capacity <= this.MaxLength || this.MaxLength == Capacity.Unbounded);

			if (capacity <= this.Array.Length)
			{
				return;
			}

			int newSize = this.Array.Length;

			if (newSize == 0)
			{
				// default starting size
				newSize = 4;
			}

			do
			{
				newSize *= 2;
			}
			while (newSize < capacity);

			if (this.MaxLength != Capacity.Unbounded)
			{
				newSize = newSize.ClampUpper(this.MaxLength);
			}

			this.Array = new T[newSize];
		}

		public void CopyIn(ICollectionView<T> values) => this.CopyIn(values, values.Count);

		public void CopyIn(ICollection<T> values) => this.CopyIn(values, values.Count);

		public void CopyIn(IEnumerable<T> values) => this.CopyIn(values, values.CountExtended());

		public void CopyIn(IEnumerable<T> values, int count)
		{
			Require.That(values != null);
			Require.That(values.CountExtended() == count);
			Require.That(count <= this.MaxLength || this.MaxLength == Capacity.Unbounded);

			this.EnsureCapacity(count);

			int index = 0;
			foreach (var value in values)
			{
				this.Array[index] = value;
				index++;
			}
		}
	}
}
