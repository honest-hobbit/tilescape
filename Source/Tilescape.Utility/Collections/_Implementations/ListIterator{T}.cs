﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public class ListIterator<T>
	{
		private static readonly int InvalidIndex = -1;

		private T value = default(T);

		public ListIterator(IListView<T> list)
		{
			Require.That(list != null);

			this.List = list;
			if (list.Count >= 1)
			{
				this.SetIndex(0);
			}
		}

		public IListView<T> List { get; }

		public T Value
		{
			get
			{
				Require.That(this.Index != InvalidIndex);

				return this.value;
			}
		}

		public int Index { get; private set; } = InvalidIndex;

		public bool TryMovePrevious()
		{
			if (this.Index > 0)
			{
				this.SetIndex(this.Index - 1);
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool TryMoveNext()
		{
			if (this.Index != InvalidIndex && this.Index < this.List.Count - 1)
			{
				this.SetIndex(this.Index + 1);
				return true;
			}
			else
			{
				return false;
			}
		}

		public void SetIndex(int index)
		{
			Require.That(this.List.IsIndexInViewBounds(index));

			this.Index = index;
			this.value = this.List[index];
		}
	}
}
