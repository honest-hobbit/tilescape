﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public class MultiValueDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
		where TValue : class
	{
		private readonly IDictionary<TKey, object> dictionary;

		public MultiValueDictionary()
		{
			this.dictionary = new Dictionary<TKey, object>();
		}

		public MultiValueDictionary(int capacity)
		{
			Require.That(capacity >= 0);

			this.dictionary = new Dictionary<TKey, object>(capacity);
		}

		public MultiValueDictionary(IEqualityComparer<TKey> comparer)
		{
			Require.That(comparer != null);

			this.dictionary = new Dictionary<TKey, object>(comparer);
		}

		public MultiValueDictionary(int capacity, IEqualityComparer<TKey> comparer)
		{
			Require.That(capacity >= 0);
			Require.That(comparer != null);

			this.dictionary = new Dictionary<TKey, object>(capacity, comparer);
		}

		public void Add(TKey key, TValue value)
		{
			Require.That(key != null);
			Require.That(value != null);

			object obj;
			if (this.dictionary.TryGetValue(key, out obj))
			{
				var typeSafeValue = obj as TValue;
				if (typeSafeValue != null)
				{
					var values = new List<TValue>();
					values.Add(typeSafeValue);
					values.Add(value);
					this.dictionary[key] = values;
				}
				else
				{
					((List<TValue>)obj).Add(value);
				}
			}
			else
			{
				this.dictionary.Add(key, value);
			}
		}

		public void ForEach(TKey key, Action<TValue> action)
		{
			Require.That(key != null);
			Require.That(action != null);

			object obj;
			if (!this.dictionary.TryGetValue(key, out obj))
			{
				return;
			}

			var typeSafeValue = obj as TValue;
			if (typeSafeValue != null)
			{
				action(typeSafeValue);
			}
			else
			{
				((List<TValue>)obj).ForEach(action);
			}
		}

		public bool RemoveAll(TKey key)
		{
			Require.That(key != null);

			return this.dictionary.Remove(key);
		}

		public void Clear() => this.dictionary.Clear();

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			foreach (var pair in this.dictionary)
			{
				var typeSafeValue = pair.Value as TValue;
				if (typeSafeValue != null)
				{
					yield return KeyValuePair.New(pair.Key, typeSafeValue);
				}
				else
				{
					foreach (var value in (List<TValue>)pair.Value)
					{
						yield return KeyValuePair.New(pair.Key, value);
					}
				}
			}
		}

		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
