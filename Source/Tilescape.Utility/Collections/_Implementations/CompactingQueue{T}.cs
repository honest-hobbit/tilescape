﻿using System;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	// CompactingQueue is a fixed sized collection that will never expand to fit more elements.
	public class CompactingQueue<T>
	{
		private static readonly int UninitializedIndex = -1;

		private readonly T[] values;

		private readonly Func<T, bool> isValueStale;

		/// <summary>
		/// The index of the first value in the queue, or <see cref="UninitializedIndex"/> if empty.
		/// </summary>
		private int head = UninitializedIndex;

		/// <summary>
		/// The index of the last value in the queue, or <see cref="UninitializedIndex"/> if empty.
		/// </summary>
		private int tail = UninitializedIndex;

		public CompactingQueue(Func<T, bool> isValueStale, int capacity)
		{
			Require.That(isValueStale != null);
			Require.That(capacity >= 0);

			this.isValueStale = isValueStale;
			this.values = new T[capacity];
		}

		private bool IsEmpty => this.head == UninitializedIndex;

		private bool IsFull => this.Count == this.values.Length;

		private int Count
		{
			get
			{
				if (this.IsEmpty)
				{
					return 0;
				}

				if (this.head <= this.tail)
				{
					return this.tail - this.head + 1;
				}
				else
				{
					return this.values.Length - (this.head - this.tail - 1);
				}
			}
		}

		public void Clear()
		{
			if (this.IsEmpty)
			{
				return;
			}

			int index = this.head;
			while (index != this.tail)
			{
				this.values[index] = default(T);
				this.IncrementIndex(ref index);
			}

			this.values[index] = default(T);

			this.head = UninitializedIndex;
			this.tail = UninitializedIndex;
		}

		// Returns true if the value was stored or if the value was already stale (in which case it is ignored).
		// If the queue is full it will try to compact by removing stale values to make room for the new value.
		// Returns false only if the value could not be stored and the value isn't stale.
		public bool TryAdd(T value)
		{
			if (this.values.Length == 0)
			{
				return false;
			}

			if (this.isValueStale(value))
			{
				// queue still accepts stale values but no need to bother storing them
				// because TryTake would refuse to return it anyway
				return true;
			}

			if (this.IsFull)
			{
				this.Compact();

				if (this.isValueStale(value))
				{
					// compacting can take awhile so check if stale again
					return true;
				}

				if (this.IsFull)
				{
					return false;
				}
			}

			this.IncrementIndex(ref this.tail);
			this.values[this.tail] = value;

			if (this.IsEmpty)
			{
				this.head = 0;
			}

			return true;
		}

		// Guaranteed not to return a stale value. However, in a multi threaded environment, the value returned could become
		// stale the moment after returning it.
		public bool TryTake(out T value)
		{
			while (this.DoTryTake(out value))
			{
				if (!this.isValueStale(value))
				{
					return true;
				}
			}

			// if DoTryTake returns false than value was already assigned default(T)
			return false;
		}

		private bool DoTryTake(out T value)
		{
			if (this.IsEmpty)
			{
				value = default(T);
				return false;
			}

			value = this.values[this.head];
			this.values[this.head] = default(T);

			// update the head and tail indices
			if (this.head == this.tail)
			{
				// the only remaining value was removed
				this.head = UninitializedIndex;
				this.tail = UninitializedIndex;
			}
			else
			{
				this.IncrementIndex(ref this.head);
			}

			return true;
		}

		private void IncrementIndex(ref int index) => index = (index == this.values.Length - 1) ? 0 : index + 1;

		private void Compact()
		{
			bool isEmpty = true;
			int source = this.head - 1;
			int newTail = source;
			int totalCount = this.Count;

			for (int count = 0; count < totalCount; count++)
			{
				this.IncrementIndex(ref source);

				var value = this.values[source];
				if (this.isValueStale(value))
				{
					this.values[source] = default(T);
				}
				else
				{
					isEmpty = false;
					this.IncrementIndex(ref newTail);

					if (newTail != source)
					{
						this.values[newTail] = value;
						this.values[source] = default(T);
					}
				}
			}

			if (isEmpty)
			{
				this.head = UninitializedIndex;
				this.tail = UninitializedIndex;
			}
			else
			{
				this.tail = newTail;
			}
		}
	}
}
