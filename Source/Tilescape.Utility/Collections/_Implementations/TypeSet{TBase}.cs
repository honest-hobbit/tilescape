﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Collections
{
	public class TypeSet<TBase> : IEnumerable<TBase>
	{
		private readonly Dictionary<Type, TBase> values;

		public TypeSet(int count)
		{
			Require.That(count >= 0);

			this.values = new Dictionary<Type, TBase>(count);
		}

		public TypeSet()
		{
			this.values = new Dictionary<Type, TBase>();
		}

		public int Count => this.values.Count;

		public void Clear() => this.values.Clear();

		public bool Contains<T>()
			where T : TBase => this.values.ContainsKey(typeof(T));

		public bool TryAdd<T>(T value)
			where T : TBase => this.values.TryAdd(typeof(T), value);

		public void Add<T>(T value)
			where T : TBase
		{
			Require.That(!this.Contains<T>());

			this.values.Add(typeof(T), value);
		}

		public bool TryGet<T>(out T value)
			where T : TBase
		{
			TBase result;
			if (this.values.TryGetValue(typeof(T), out result))
			{
				value = (T)result;
				return true;
			}
			else
			{
				value = default(T);
				return false;
			}
		}

		public Try<T> TryGet<T>()
			where T : TBase
		{
			T value;
			return this.TryGet(out value) ? Try.Value(value) : Try.None<T>();
		}

		public T Get<T>()
			where T : TBase
		{
			Require.That(this.Contains<T>());

			return (T)this.values[typeof(T)];
		}

		public bool TryRemove<T>(out T value)
			where T : TBase => this.TryGet(out value) ? this.TryRemove<T>() : false;

		public bool TryRemove<T>()
			where T : TBase => this.values.Remove(typeof(T));

		/// <inheritdoc />
		public IEnumerator<TBase> GetEnumerator() => this.values.Values.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
