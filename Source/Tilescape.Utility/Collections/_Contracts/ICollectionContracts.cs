﻿using System.Collections.Generic;
using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides contracts for the <see cref="ICollection{T}"/> interface.
	/// </summary>
	public static class ICollectionContracts
	{
		/// <summary>
		/// Contracts for <see cref="ICollection{T}.Add(T)"/>.
		/// </summary>
		/// <typeparam name="T">The type of value stored in the collection.</typeparam>
		/// <param name="instance">The instance.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void Add<T>(ICollection<T> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
		}

		/// <summary>
		/// Contracts for <see cref="ICollection{T}.Add(T)"/>.
		/// </summary>
		/// <typeparam name="T">The type of value stored in the collection.</typeparam>
		/// <param name="instance">The instance.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void Remove<T>(ICollection<T> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
		}

		/// <summary>
		/// Contracts for <see cref="ICollection{T}.Clear"/>.
		/// </summary>
		/// <typeparam name="T">The type of value stored in the collection.</typeparam>
		/// <param name="instance">The instance.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void Clear<T>(ICollection<T> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
		}

		/// <summary>
		/// Contracts for <see cref="ICollection{T}.CopyTo(T[], int)"/>.
		/// </summary>
		/// <typeparam name="T">The type of value stored in the collection.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="array">The array.</param>
		/// <param name="index">Index of the array.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void CopyTo<T>(ICollection<T> instance, T[] array, int index)
		{
			Require.That(instance != null);
			Require.That(array != null);
			Require.That(index >= 0);
			Require.That(index + instance.Count <= array.Length);
		}
	}
}
