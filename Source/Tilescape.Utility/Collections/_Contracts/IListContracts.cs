﻿using System.Collections.Generic;
using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides contracts for <see cref="IList{T}"/> interface.
	/// </summary>
	public static class IListContracts
	{
		/// <summary>
		/// Contracts for <see cref="IList{T}"/>'s index getter.
		/// </summary>
		/// <typeparam name="T">The type of the values stored in the list.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="index">The index.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void IndexerGet<T>(IList<T> instance, int index)
		{
			Require.That(instance != null);
			Require.That(index >= 0 && index < instance.Count);
		}

		/// <summary>
		/// Contracts for <see cref="IList{T}"/>'s index setter.
		/// </summary>
		/// <typeparam name="T">The type of the values stored in the list.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="index">The index.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void IndexerSet<T>(IList<T> instance, int index)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
			Require.That(index >= 0 && index < instance.Count);
		}

		/// <summary>
		/// Contracts for <see cref="IList{T}.Insert(int, T)"/>.
		/// </summary>
		/// <typeparam name="T">The type of the values stored in the list.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="index">The index.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void Insert<T>(IList<T> instance, int index)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
			Require.That(index >= 0 && index <= instance.Count);
		}

		/// <summary>
		/// Contracts for <see cref="IList{T}.RemoveAt(int)"/>.
		/// </summary>
		/// <typeparam name="T">The type of the values stored in the list.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="index">The index.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void RemoveAt<T>(IList<T> instance, int index)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
			Require.That(index >= 0 && index < instance.Count);
		}
	}
}
