﻿using System.Collections.Generic;
using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides contracts for the <see cref="IDictionary{TKey, TValue}"/> interface.
	/// </summary>
	public static class IDictionaryContracts
	{
		/// <summary>
		/// Contracts for <see cref="IDictionary{TKey, TValue}"/>'s index getter.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="key">The key.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void IndexerGet<TKey, TValue>(IDictionary<TKey, TValue> instance, TKey key)
		{
			Require.That(instance != null);
			Require.That(key != null);
			Require.That(instance.ContainsKey(key));
		}

		/// <summary>
		/// Contracts for <see cref="IDictionary{TKey, TValue}"/>'s index setter.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="key">The key.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void IndexerSet<TKey, TValue>(IDictionary<TKey, TValue> instance, TKey key)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
			Require.That(key != null);
		}

		/// <summary>
		/// Contracts for <see cref="IDictionary{TKey, TValue}.Add(TKey, TValue)"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="key">The key.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void Add<TKey, TValue>(IDictionary<TKey, TValue> instance, TKey key)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
			Require.That(key != null);
			Require.That(!instance.ContainsKey(key));
		}

		/// <summary>
		/// Contracts for <see cref="IDictionary{TKey, TValue}.ContainsKey(TKey)"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <param name="key">The key.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void ContainsKey<TKey>(TKey key)
		{
			Require.That(key != null);
		}

		/// <summary>
		/// Contracts for <see cref="IDictionary{TKey, TValue}.Remove(TKey)"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="key">The key.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void Remove<TKey, TValue>(IDictionary<TKey, TValue> instance, TKey key)
		{
			Require.That(instance != null);
			Require.That(!instance.IsReadOnly);
			Require.That(key != null);
		}

		/// <summary>
		/// Contracts for <see cref="IDictionary{TKey, TValue}.TryGetValue(TKey, out TValue)"/>.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <param name="key">The key.</param>
		[Conditional(Require.CompilationSymbol)]
		public static void TryGetValue<TKey>(TKey key)
		{
			Require.That(key != null);
		}
	}
}
