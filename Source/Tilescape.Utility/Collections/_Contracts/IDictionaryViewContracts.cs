﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public static class IDictionaryViewContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<TKey, TValue>(IDictionaryView<TKey, TValue> instance, TKey key)
		{
			Require.That(instance != null);
			Require.That(key != null);
			Require.That(instance.ContainsKey(key));
		}

		[Conditional(Require.CompilationSymbol)]
		public static void ContainsKey<TKey>(TKey key)
		{
			Require.That(key != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void TryGetValue<TKey>(TKey key)
		{
			Require.That(key != null);
		}
	}
}
