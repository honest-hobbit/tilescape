﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public static class IListViewContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<T>(IListView<T> instance, int index)
		{
			Require.That(instance != null);
			Require.That(index >= 0 && index < instance.Count);
		}
	}
}
