﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides extension methods for one dimensional arrays.
	/// </summary>
	public static class ArrayExtensions
	{
		public static bool HasCapacity(this Array source)
		{
			Require.That(source != null);

			return source.LongLength >= 1;
		}

		/// <summary>
		/// Gets the type-safe enumerator for an array.
		/// </summary>
		/// <typeparam name="T">The type of values to enumerate.</typeparam>
		/// <param name="array">The source array.</param>
		/// <returns>The type-safe enumerator for the array.</returns>
		public static IEnumerator<T> GetEnumerator<T>(this T[] array)
		{
			Require.That(array != null);

			return ((IEnumerable<T>)array).GetEnumerator();
		}

		public static T[] Copy<T>(this T[] array)
		{
			Require.That(array != null);

			T[] result = new T[array.LongLength];
			array.CopyTo(result, 0);
			return result;
		}
	}
}
