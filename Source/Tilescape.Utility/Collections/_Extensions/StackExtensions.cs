﻿using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides extension methods for <see cref="Stack{T}"/>.
	/// </summary>
	public static class StackExtensions
	{
		public static bool TryPop<T>(this Stack<T> stack, out T result)
		{
			Require.That(stack != null);

			if (stack.Count == 0)
			{
				result = default(T);
				return false;
			}
			else
			{
				result = stack.Pop();
				return true;
			}
		}

		public static bool TryPeek<T>(this Stack<T> stack, out T result)
		{
			Require.That(stack != null);

			if (stack.Count == 0)
			{
				result = default(T);
				return false;
			}
			else
			{
				result = stack.Peek();
				return true;
			}
		}

		public static Try<T> TryPop<T>(this Stack<T> stack)
		{
			Require.That(stack != null);

			return stack.Count == 0 ? Try.None<T>() : Try.Value(stack.Pop());
		}

		public static Try<T> TryPeek<T>(this Stack<T> stack)
		{
			Require.That(stack != null);

			return stack.Count == 0 ? Try.None<T>() : Try.Value(stack.Peek());
		}

		public static void PushMany<T>(this Stack<T> stack, params T[] values)
		{
			stack.PushMany((IEnumerable<T>)values);
		}

		public static void PushMany<T>(this Stack<T> stack, IEnumerable<T> values)
		{
			Require.That(stack != null);
			Require.That(values != null);

			foreach (T value in values)
			{
				stack.Push(value);
			}
		}

		public static IEnumerable<T> PopAll<T>(this Stack<T> stack)
		{
			Require.That(stack != null);

			if (stack.Count == 0)
			{
				return Enumerable.Empty<T>();
			}

			var values = stack.ToArray();
			stack.Clear();
			return values;
		}
	}
}
