﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public static class ICollectionViewExtensions
	{
		public static T[] ToArrayFromView<T>(this ICollectionView<T> source)
		{
			Require.That(source != null);

			var result = new T[source.Count];
			source.CopyTo(result);
			return result;
		}

		public static List<T> ToListFromView<T>(this ICollectionView<T> source)
		{
			Require.That(source != null);

			var result = new List<T>(source.Count);
			result.AddMany(source);
			return result;
		}

		public static ICollectionView<T> CopyCollectionView<T>(this ICollectionView<T> source)
		{
			Require.That(source != null);

			return source.ToArrayFromView().AsCollectionView();
		}
	}
}
