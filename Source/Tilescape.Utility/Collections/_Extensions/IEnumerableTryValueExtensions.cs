﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides extension methods for converting the results of some <see cref="Enumerable"/> methods
	/// into <see cref="Try{T}"/>.
	/// </summary>
	public static class IEnumerableTryValueExtensions
	{
		#region [Something]OrNone methods

		public static Try<T> ElementAtOrNone<T>(this IEnumerable<T> values, int index)
		{
			Require.That(values != null);
			Require.That(index >= 0);

			var list = values as IList<T>;
			if (list != null)
			{
				return list.IsIndexInBounds(index) ? Try.Value(list[index]) : Try.None<T>();
			}

			int count = 0;
			foreach (var value in values)
			{
				if (count == index)
				{
					return Try.Value(value);
				}

				count++;
			}

			return Try.None<T>();
		}

		public static Try<T> FirstOrNone<T>(this IEnumerable<T> values)
		{
			Require.That(values != null);

			return values.IsEmpty() ? Try.None<T>() : Try.Value(values.First());
		}

		public static Try<T> FirstOrNone<T>(this IEnumerable<T> values, Func<T, bool> predicate)
		{
			Require.That(values != null);
			Require.That(predicate != null);

			return values.Select(value => Try.Value(value)).FirstOrDefault(value => predicate(value.Value));
		}

		public static Try<T> LastOrNone<T>(this IEnumerable<T> values)
		{
			Require.That(values != null);

			return values.IsEmpty() ? Try.None<T>() : Try.Value(values.Last());
		}

		public static Try<T> LastOrNone<T>(this IEnumerable<T> values, Func<T, bool> predicate)
		{
			Require.That(values != null);
			Require.That(predicate != null);

			return values.Select(value => Try.Value(value)).LastOrDefault(value => predicate(value.Value));
		}

		public static Try<T> SingleOrNone<T>(this IEnumerable<T> values)
		{
			Require.That(values != null);

			return values.IsEmpty() ? Try.None<T>() : Try.Value(values.Single());
		}

		public static Try<T> SingleOrNone<T>(this IEnumerable<T> values, Func<T, bool> predicate)
		{
			Require.That(values != null);
			Require.That(predicate != null);

			return values.Select(value => Try.Value(value)).SingleOrDefault(value => predicate(value.Value));
		}

		public static Try<T> MinOrNone<T>(this IEnumerable<T> values)
		{
			Require.That(values != null);

			return values.IsEmpty() ? Try.None<T>() : Try.Value(values.Min());
		}

		public static Try<T> MaxOrNone<T>(this IEnumerable<T> values)
		{
			Require.That(values != null);

			return values.IsEmpty() ? Try.None<T>() : Try.Value(values.Max());
		}

		#endregion

		#region WhereSelect Try methods (equivalent methods for UniRx.IObservable too in other project)

		public static IEnumerable<T> WhereHasValueSelect<T>(this IEnumerable<Try<T>> values)
		{
			Require.That(values != null);

			return values.Where(x => x.HasValue).Select(x => x.Value);
		}

		public static IEnumerable<KeyValuePair<TKey, TValue>> WhereHasValueSelectPair<TKey, TValue>(
			this IEnumerable<KeyValuePair<TKey, Try<TValue>>> values)
		{
			Require.That(values != null);

			return values.Where(x => x.Value.HasValue).Select(x => KeyValuePair.New(x.Key, x.Value.Value));
		}

		public static IEnumerable<TKey> WhereHasValueSelectKey<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, Try<TValue>>> values)
		{
			Require.That(values != null);

			return values.Where(x => x.Value.HasValue).Select(x => x.Key);
		}

		public static IEnumerable<TKey> WhereNoValueSelectKey<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, Try<TValue>>> values)
		{
			Require.That(values != null);

			return values.Where(x => !x.Value.HasValue).Select(x => x.Key);
		}

		#endregion
	}
}
