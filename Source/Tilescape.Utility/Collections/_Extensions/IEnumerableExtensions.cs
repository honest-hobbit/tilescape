﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides extension methods for the <see cref="IEnumerable"/> and <see cref="IEnumerable{T}"/> interfaces.
	/// </summary>
	public static class IEnumerableExtensions
	{
		#region IsNull/Empty

		/// <summary>
		/// Determines whether the specified enumerable is empty.
		/// </summary>
		/// <typeparam name="T">The type of enumerable objects.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <returns>True if the enumerable is empty, false otherwise.</returns>
		public static bool IsEmpty<T>(this IEnumerable<T> source)
		{
			Require.That(source != null);

			return !source.Any();
		}

		/// <summary>
		/// Determines whether the specified enumerable is null or empty.
		/// </summary>
		/// <typeparam name="T">The type of enumerable objects.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <returns>True if the enumerable is null or empty, false otherwise.</returns>
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> source) => source == null || !source.Any();

		#endregion

		#region IsUnique

		/// <summary>
		/// Determines whether the specified enumerable contains only unique values (no duplicates).
		/// </summary>
		/// <typeparam name="T">The type of the enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <returns>True if the sequences contains at least one duplicate value, otherwise false.</returns>
		public static bool IsUnique<T>(this IEnumerable<T> source)
		{
			return source.IsUnique(EqualityComparer<T>.Default);
		}

		/// <summary>
		/// Determines whether the specified enumerable contains only unique values (no duplicates).
		/// </summary>
		/// <typeparam name="T">The type of the enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <param name="comparer">The comparer to use for equality comparisons.</param>
		/// <returns>True if the sequences contains at least one duplicate value, otherwise false.</returns>
		public static bool IsUnique<T>(this IEnumerable<T> source, IEqualityComparer<T> comparer)
		{
			Require.That(source != null);
			Require.That(comparer != null);

			HashSet<T> uniqueElements = new HashSet<T>(comparer);
			foreach (T element in source)
			{
				if (!uniqueElements.Add(element))
				{
					// a duplicate was found
					return false;
				}
			}

			return true;
		}

		#endregion

		#region AllEqual(To)

		public static bool AllEqual<T>(this IEnumerable<T> source)
		{
			return source.AllEqual(EqualityComparer<T>.Default);
		}

		public static bool AllEqual<T>(this IEnumerable<T> source, IEqualityComparer<T> comparer)
		{
			Require.That(source != null);
			Require.That(comparer != null);

			T equalTo = source.FirstOrDefault();
			return source.All(value => comparer.Equals(value, equalTo));
		}

		public static bool AllEqualTo<T>(this IEnumerable<T> source, T value)
		{
			return source.AllEqualTo(value, EqualityComparer<T>.Default);
		}

		public static bool AllEqualTo<T>(this IEnumerable<T> source, T value, IEqualityComparer<T> comparer)
		{
			Require.That(source != null);
			Require.That(comparer != null);

			return source.All(checking => comparer.Equals(checking, value));
		}

		#endregion

		#region AllAndSelfNotNull (NullSafe)

		public static bool AllAndSelfNotNull<T>(this IEnumerable<T> source)
		{
			if (source == null)
			{
				return false;
			}

			return source.All(value => value != null);
		}

		#endregion

		#region ContainsAny/All

		/// <summary>
		/// Determines if the source enumerable contains any value from the other enumerable sequence.
		/// </summary>
		/// <typeparam name="T">The type of the enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <param name="sequence">
		/// The enumerable containing the values for the source sequence to check for containing any of.
		/// </param>
		/// <returns>True if the source enumerable contains any value from the other enumerable; otherwise false.</returns>
		public static bool ContainsAny<T>(this IEnumerable<T> source, IEnumerable<T> sequence)
		{
			return source.ContainsAny(sequence, EqualityComparer<T>.Default);
		}

		/// <summary>
		/// Determines if the source enumerable contains any value from the other enumerable sequence.
		/// </summary>
		/// <typeparam name="T">The type of the enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <param name="sequence">
		/// The enumerable containing the values for the source sequence to check for containing any of.
		/// </param>
		/// <param name="comparer">The comparer to use for equality comparisons.</param>
		/// <returns>True if the source enumerable contains any value from the other enumerable; otherwise false.</returns>
		public static bool ContainsAny<T>(this IEnumerable<T> source, IEnumerable<T> sequence, IEqualityComparer<T> comparer)
		{
			Require.That(source != null);
			Require.That(sequence != null);
			Require.That(comparer != null);

			return source.Intersect(sequence, comparer).Any();
		}

		/// <summary>
		/// Determines whether the enumerable source contains all the elements of the specified enumerable sequence
		/// in any order, ignoring duplicates.
		/// </summary>
		/// <typeparam name="T">The type of enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <param name="other">The other enumerable.</param>
		/// <returns>True if the sequence contains the same elements in any order ignoring duplicates; otherwise false.</returns>
		public static bool ContainsAll<T>(this IEnumerable<T> source, IEnumerable<T> other)
		{
			return source.ContainsAll(other, EqualityComparer<T>.Default);
		}

		/// <summary>
		/// Determines whether the enumerable source contains all the elements of the specified enumerable sequence
		/// in any order, ignoring duplicates.
		/// </summary>
		/// <typeparam name="T">The type of enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <param name="other">The other enumerable.</param>
		/// <param name="comparer">The comparer to use for equality checks.</param>
		/// <returns>True if the sequence contains the same elements in any order ignoring duplicates; otherwise false.</returns>
		public static bool ContainsAll<T>(this IEnumerable<T> source, IEnumerable<T> other, IEqualityComparer<T> comparer)
		{
			Require.That(source != null);
			Require.That(other != null);
			Require.That(comparer != null);

			var sourceSet = new HashSet<T>(source, comparer);
			foreach (var value in other)
			{
				if (!sourceSet.Contains(value))
				{
					return false;
				}
			}

			return true;
		}

		#endregion

		#region ElementsEqual

		/// <summary>
		/// Determines whether the enumerable source contains only the same elements as the specified enumerable sequence
		/// in any order, ignoring duplicates.
		/// </summary>
		/// <typeparam name="T">The type of enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <param name="other">The other enumerable.</param>
		/// <returns>True if the sequence contains the same elements in any order ignoring duplicates; otherwise false.</returns>
		public static bool ElementsEqual<T>(this IEnumerable<T> source, IEnumerable<T> other)
		{
			return source.ElementsEqual(other, EqualityComparer<T>.Default);
		}

		/// <summary>
		/// Determines whether the enumerable source contains only the same elements as the specified enumerable sequence
		/// in any order, ignoring duplicates.
		/// </summary>
		/// <typeparam name="T">The type of enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <param name="other">The other enumerable.</param>
		/// <param name="comparer">The comparer to use for equality checks.</param>
		/// <returns>True if the sequence contains the same elements in any order ignoring duplicates; otherwise false.</returns>
		public static bool ElementsEqual<T>(this IEnumerable<T> source, IEnumerable<T> other, IEqualityComparer<T> comparer)
		{
			Require.That(source != null);
			Require.That(other != null);
			Require.That(comparer != null);

			return new HashSet<T>(source, comparer).SetEquals(other);
		}

		#endregion

		#region RemoveDuplicates

		/// <summary>
		/// Returns as enumerable sequence with all duplicate values from the source enumerable removed.
		/// </summary>
		/// <typeparam name="T">The type of the enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <returns>An enumerable sequence with only the unique values present.</returns>
		/// <remarks>
		/// Values are yielded on their first unique occurrences in the source enumerable. All duplicates thereafter are ignored.
		/// </remarks>
		public static IEnumerable<T> RemoveDuplicates<T>(this IEnumerable<T> source)
		{
			return source.RemoveDuplicates(EqualityComparer<T>.Default);
		}

		/// <summary>
		/// Returns as enumerable sequence with all duplicate values from the source enumerable removed.
		/// </summary>
		/// <typeparam name="T">The type of the enumerable values.</typeparam>
		/// <param name="source">The source enumerable.</param>
		/// <param name="comparer">The comparer to use for equality comparisons.</param>
		/// <returns>An enumerable sequence with only the unique values present.</returns>
		/// <remarks>
		/// Values are yielded on their first unique occurrences in the source enumerable. All duplicates thereafter are ignored.
		/// </remarks>
		public static IEnumerable<T> RemoveDuplicates<T>(this IEnumerable<T> source, IEqualityComparer<T> comparer)
		{
			Require.That(source != null);
			Require.That(comparer != null);

			HashSet<T> uniqueElements = new HashSet<T>(comparer);
			foreach (T element in source)
			{
				if (uniqueElements.Add(element))
				{
					// element is not a duplicate
					yield return element;
				}
			}
		}

		#endregion

		#region TryGet Methods

		public static bool TryGetCount<T>(this IEnumerable<T> source, out int count)
		{
			Require.That(source != null);

			if (source is ICollection<T> collection)
			{
				count = collection.Count;
				return true;
			}

			if (source is ICollectionView<T> view)
			{
				count = view.Count;
				return true;
			}

			count = 0;
			return false;
		}

		public static bool TryGetSingle<T>(this IEnumerable<T> source, out T result)
		{
			Require.That(source != null);

			int count = 0;
			result = default(T);

			foreach (var value in source)
			{
				count++;
				if (count == 2)
				{
					result = default(T);
					return false;
				}

				result = value;
			}

			return count == 1;
		}

		#endregion

		#region Enumerable Methods "Extended"

		public static int CountExtended<T>(this IEnumerable<T> source)
		{
			Require.That(source != null);

			if (source is ICollectionView<T> view)
			{
				return view.Count;
			}

			return source.Count();
		}

		public static long LongCountExtended<T>(this IEnumerable<T> source)
		{
			Require.That(source != null);

			if (source is ILargeCollectionView<T> largeView)
			{
				return largeView.Count;
			}

			if (source is ICollectionView<T> view)
			{
				return view.Count;
			}

			return source.LongCount();
		}

		public static T[] ToArrayExtended<T>(this IEnumerable<T> source)
		{
			Require.That(source != null);

			if (source is ICollectionView<T> view)
			{
				var result = new T[view.Count];
				source.CopyTo(result);
				return result;
			}

			return source.ToArray();
		}

		public static List<T> ToListExtended<T>(this IEnumerable<T> source)
		{
			Require.That(source != null);

			if (source is ICollectionView<T> view)
			{
				var result = new List<T>(view.Count);
				result.AddMany(source);
				return result;
			}

			return source.ToList();
		}

		#endregion

		#region Misc Methods

		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
		{
			Require.That(source != null);
			Require.That(action != null);

			foreach (var value in source)
			{
				action(value);
			}
		}

		/// <summary>
		/// Copies the elements of the enumerable to an array, starting at a particular array index.
		/// </summary>
		/// <typeparam name="T">The type of the enumerable values.</typeparam>
		/// <param name="source">The source enumerable to copy to an array.</param>
		/// <param name="array">The array to copy the enumerable values to.</param>
		/// <param name="startingIndex">Index of the array to start copying to.</param>
		public static void CopyTo<T>(this IEnumerable<T> source, T[] array, int startingIndex = 0)
		{
			Require.That(source != null);
			Require.That(array != null);
			Require.That(startingIndex >= 0);
			Require.That(source.CountExtended() + startingIndex <= array.Length);

			foreach (T value in source)
			{
				array[startingIndex] = value;
				startingIndex++;
			}
		}

		public static string ToJoinString<T>(this IEnumerable<T> source) => source.ToJoinString(string.Empty);

		public static string ToJoinString<T>(this IEnumerable<T> source, string separator)
		{
			Require.That(source != null);

			using (var enumerator = source.GetEnumerator())
			{
				if (!enumerator.MoveNext())
				{
					return string.Empty;
				}

				var result = new StringBuilder();
				result.Append<T>(enumerator.Current);

				while (enumerator.MoveNext())
				{
					result.Append(separator);
					result.Append<T>(enumerator.Current);
				}

				return result.ToString();
			}
		}

		public static IEnumerable<T> AsEnumerableOnly<T>(this IEnumerable<T> values)
		{
			Require.That(values != null);

			return new EnumerableWrapper<T>(values);
		}

		#endregion

		private class EnumerableWrapper<T> : IEnumerable<T>
		{
			private readonly IEnumerable<T> values;

			public EnumerableWrapper(IEnumerable<T> values)
			{
				Require.That(values != null);

				this.values = values;
			}

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.values.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}
	}
}
