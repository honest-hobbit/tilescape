﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides extension methods for <see cref="IList{T}"/>.
	/// </summary>
	public static class IListExtensions
	{
		public static void SetAllTo<T>(this IList<T> list, T value)
		{
			Require.That(list != null);

			for (int index = 0; index < list.Count; index++)
			{
				list[index] = value;
			}
		}

		public static void SetAllTo<T>(this IList<T> list, Func<int, T> getValue)
		{
			Require.That(list != null);
			Require.That(getValue != null);

			for (int index = 0; index < list.Count; index++)
			{
				list[index] = getValue(index);
			}
		}

		/// <summary>
		/// Shuffles by randomly swapping every index in the list.
		/// Performance will suffer if not used on a list with O(1) access time.
		/// </summary>
		/// <typeparam name="T">The type of values stored in the list.</typeparam>
		/// <param name="list">The list to be shuffled.</param>
		public static void Shuffle<T>(this IList<T> list)
		{
			Require.That(list != null);

			Shuffle(list, ThreadLocalRandom.Instance);
		}

		/// <summary>
		/// Shuffles by randomly swapping every index in the list.
		/// Performance will suffer if not used on a list with O(1) access time.
		/// </summary>
		/// <typeparam name="T">The type of values stored in the list.</typeparam>
		/// <param name="list">The list to be shuffled.</param>
		/// <param name="random">The Random used to determine the shuffling. This can be used to allow deterministic shuffling.</param>
		public static void Shuffle<T>(this IList<T> list, Random random)
		{
			Require.That(list != null);
			Require.That(random != null);

			int n = list.Count;
			while (n > 1)
			{
				n--;
				int k = random.Next(n + 1);
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
		}

		/// <summary>
		/// Inverts the order of the elements in this sequence efficiently.
		/// </summary>
		/// <typeparam name="T">The type of elements in the list.</typeparam>
		/// <param name="list">The list to enumerate backwards.</param>
		/// <returns>A sequence whose elements correspond to those of this sequence in reverse order.</returns>
		public static IEnumerable<T> ReverseEfficient<T>(this IList<T> list)
		{
			Require.That(list != null);

			for (int index = list.Count - 1; index >= 0; index--)
			{
				yield return list[index];
			}
		}

		public static bool IsIndexInBounds<T>(this IList<T> list, int index)
		{
			Require.That(list != null);

			return index >= 0 && index < list.Count;
		}

		public static void ForEachListItem<T>(this IList<T> list, Action<T> action)
		{
			Require.That(list != null);
			Require.That(action != null);

			for (int index = 0; index < list.Count; index++)
			{
				action(list[index]);
			}
		}

		public static void ForEachListItem<T>(this IList<T> list, Action<int, T> action)
		{
			Require.That(list != null);
			Require.That(action != null);

			for (int index = 0; index < list.Count; index++)
			{
				action(index, list[index]);
			}
		}
	}
}
