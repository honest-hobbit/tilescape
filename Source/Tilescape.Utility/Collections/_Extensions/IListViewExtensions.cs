﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public static class IListViewExtensions
	{
		/// <summary>
		/// Inverts the order of the elements in this sequence efficiently.
		/// </summary>
		/// <typeparam name="T">The type of elements in the list.</typeparam>
		/// <param name="list">The list to enumerate backwards.</param>
		/// <returns>A sequence whose elements correspond to those of this sequence in reverse order.</returns>
		public static IEnumerable<T> ReverseViewEfficient<T>(this IListView<T> list)
		{
			Require.That(list != null);

			for (int index = list.Count - 1; index >= 0; index--)
			{
				yield return list[index];
			}
		}

		public static bool IsIndexInViewBounds<T>(this IListView<T> list, int index)
		{
			Require.That(list != null);

			return index >= 0 && index < list.Count;
		}

		public static void ForEachListViewItem<T>(this IListView<T> list, Action<T> action)
		{
			Require.That(list != null);
			Require.That(action != null);

			for (int index = 0; index < list.Count; index++)
			{
				action(list[index]);
			}
		}

		public static void ForEachListViewItem<T>(this IListView<T> list, Action<int, T> action)
		{
			Require.That(list != null);
			Require.That(action != null);

			for (int index = 0; index < list.Count; index++)
			{
				action(index, list[index]);
			}
		}

		public static IListView<T> CopyListView<T>(this IListView<T> source)
		{
			Require.That(source != null);

			return source.ToArrayFromView().AsListView();
		}
	}
}
