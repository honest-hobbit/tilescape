﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Collections
{
	public static class IDictionaryViewExtensions
	{
		public static Try<TValue> TryGetViewValue<TKey, TValue>(this IDictionaryView<TKey, TValue> dictionary, TKey key)
		{
			Require.That(dictionary != null);
			Require.That(key != null);

			return dictionary.TryGetValue(key, out var value) ? Try.Value(value) : Try.None<TValue>();
		}
	}
}
