﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Utility.Collections
{
	public static class ListExtensions
	{
		// EnsureCapacity will expand the list if needed but never shrink it
		public static void EnsureCapacity<T>(this List<T> list, int capacity)
		{
			Require.That(list != null);
			Require.That(capacity >= 0);

			if (capacity > list.Capacity)
			{
				list.Capacity = capacity;
			}
		}

		public static void AddManyMaxCapacity<T>(this List<T> list, IEnumerable<T> values, int maxCapacity)
		{
			Require.That(list != null);
			Require.That(values != null);
			Require.That(maxCapacity >= 0);
			Require.That(list.Count + values.CountExtended() <= maxCapacity);

			if (list.Capacity >= maxCapacity)
			{
				list.AddMany(values);
				return;
			}

			var newCount = list.Count + values.CountExtended();
			if (newCount > list.Capacity)
			{
				int newCapacity = list.Capacity;
				do
				{
					newCapacity = (newCapacity * 2).ClampUpper(maxCapacity);
				}
				while (newCapacity < newCount);

				list.Capacity = newCapacity;
			}

			list.AddMany(values);
		}
	}
}
