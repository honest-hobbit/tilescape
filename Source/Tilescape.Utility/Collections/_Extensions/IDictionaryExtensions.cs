﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides extension methods for <see cref="IDictionary{TKey, TValue}"/>.
	/// </summary>
	public static class IDictionaryExtensions
	{
		public static Try<TValue> TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
		{
			Require.That(dictionary != null);
			Require.That(key != null);

			return dictionary.TryGetValue(key, out var value) ? Try.Value(value) : Try.None<TValue>();
		}

		/// <summary>
		/// Adds the value for the given key if the dictionary does not already contain the key.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="dictionary">The dictionary to possibly add to.</param>
		/// <param name="key">The key of the value to add.</param>
		/// <param name="value">The value to add for the given key.</param>
		/// <returns>True if the key and value were added; otherwise false (the key was already present).</returns>
		public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
		{
			Require.That(dictionary != null);
			Require.That(key != null);

			if (dictionary.ContainsKey(key))
			{
				return false;
			}
			else
			{
				dictionary[key] = value;
				return true;
			}
		}

		/// <summary>
		/// Adds the value for the given key if the dictionary does not already contain the key.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="dictionary">The dictionary to possibly add to.</param>
		/// <param name="key">The key of the value to add.</param>
		/// <param name="createValue">
		/// The function used to create the value to add for the given key. This is only executed if adding the value.
		/// </param>
		/// <returns>True if the key and value were added; otherwise false (the key was already present).</returns>
		public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> createValue)
		{
			Require.That(dictionary != null);
			Require.That(key != null);
			Require.That(createValue != null);

			if (dictionary.ContainsKey(key))
			{
				return false;
			}
			else
			{
				dictionary[key] = createValue(key);
				return true;
			}
		}

		public static bool TryRemove<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, out TValue value)
		{
			Require.That(dictionary != null);
			Require.That(key != null);

			if (dictionary.TryGetValue(key, out value))
			{
				return dictionary.Remove(key);
			}
			else
			{
				return false;
			}
		}

		public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
		{
			Require.That(dictionary != null);
			Require.That(key != null);

			TValue result;
			if (dictionary.TryGetValue(key, out result))
			{
				return result;
			}
			else
			{
				dictionary[key] = value;
				return value;
			}
		}

		public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> createValue)
		{
			Require.That(dictionary != null);
			Require.That(key != null);
			Require.That(createValue != null);

			TValue result;
			if (dictionary.TryGetValue(key, out result))
			{
				return result;
			}
			else
			{
				result = createValue(key);
				dictionary[key] = result;
				return result;
			}
		}

		public static TValue AddOrUpdate<TKey, TValue>(
			this IDictionary<TKey, TValue> dictionary, TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValue)
		{
			Require.That(dictionary != null);
			Require.That(key != null);
			Require.That(updateValue != null);

			TValue value = dictionary.TryGetValue(key, out value) ? updateValue(key, value) : addValue;
			dictionary[key] = value;
			return value;
		}

		public static TValue AddOrUpdate<TKey, TValue>(
			this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> createValue, Func<TKey, TValue, TValue> updateValue)
		{
			Require.That(dictionary != null);
			Require.That(key != null);
			Require.That(createValue != null);
			Require.That(updateValue != null);

			TValue value = dictionary.TryGetValue(key, out value) ? updateValue(key, value) : createValue(key);
			dictionary[key] = value;
			return value;
		}

		public static Guid AddWithAutoGuidKey<T>(this IDictionary<Guid, T> dictionary, T value)
		{
			Require.That(dictionary != null);

			while (true)
			{
				var key = Guid.NewGuid();
				if (dictionary.TryAdd(key, value))
				{
					return key;
				}
			}
		}
	}
}
