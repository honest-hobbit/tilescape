﻿using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Collections
{
	/// <summary>
	/// Provides extension methods for <see cref="Queue{T}"/>.
	/// </summary>
	public static class QueueExtensions
	{
		public static bool TryDequeue<T>(this Queue<T> queue, out T result)
		{
			Require.That(queue != null);

			if (queue.Count == 0)
			{
				result = default(T);
				return false;
			}
			else
			{
				result = queue.Dequeue();
				return true;
			}
		}

		public static bool TryPeek<T>(this Queue<T> queue, out T result)
		{
			Require.That(queue != null);

			if (queue.Count == 0)
			{
				result = default(T);
				return false;
			}
			else
			{
				result = queue.Peek();
				return true;
			}
		}

		public static Try<T> TryDequeue<T>(this Queue<T> queue)
		{
			Require.That(queue != null);

			return queue.Count == 0 ? Try.None<T>() : Try.Value(queue.Dequeue());
		}

		public static Try<T> TryPeek<T>(this Queue<T> queue)
		{
			Require.That(queue != null);

			return queue.Count == 0 ? Try.None<T>() : Try.Value(queue.Peek());
		}

		public static void EnqueueMany<T>(this Queue<T> queue, params T[] values)
		{
			queue.EnqueueMany((IEnumerable<T>)values);
		}

		public static void EnqueueMany<T>(this Queue<T> queue, IEnumerable<T> values)
		{
			Require.That(queue != null);
			Require.That(values != null);

			foreach (T value in values)
			{
				queue.Enqueue(value);
			}
		}

		public static IEnumerable<T> DequeueAll<T>(this Queue<T> queue)
		{
			Require.That(queue != null);

			if (queue.Count == 0)
			{
				return Enumerable.Empty<T>();
			}

			var values = queue.ToArray();
			queue.Clear();
			return values;
		}
	}
}
