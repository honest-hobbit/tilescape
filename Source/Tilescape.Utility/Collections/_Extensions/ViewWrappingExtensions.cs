﻿using System.Collections.Generic;

namespace Tilescape.Utility.Collections
{
	public static class ViewWrappingExtensions
	{
		public static ICollectionView<T> AsCollectionView<T>(this ICollection<T> collection) => CollectionView.From(collection);

		public static ICollectionView<T> WrapInCollectionView<T>(this ICollectionView<T> collection) => CollectionView.FromView(collection);

		public static IListView<T> AsListView<T>(this IList<T> list) => ListView.From(list);

		public static IListView<T> WrapInListView<T>(this IListView<T> list) => ListView.FromView(list);

		public static ISetView<T> AsSetView<T>(this ICollection<T> collection) => SetView.From(collection);

		public static ISetView<T> WrapInSetView<T>(this ISetView<T> set) => SetView.FromView(set);

		public static ISetView<TKey> KeysAsSetView<TKey, TValue>(this IDictionary<TKey, TValue> dictionary) => SetView.FromKeys(dictionary);

		public static IDictionaryView<TKey, TValue> AsDictionaryView<TKey, TValue>(this IDictionary<TKey, TValue> dictionary) =>
			DictionaryView.From(dictionary);

		public static IDictionaryView<TKey, TValue> WrapInDictionaryView<TKey, TValue>(this IDictionaryView<TKey, TValue> dictionary) =>
			DictionaryView.FromView(dictionary);
	}
}
