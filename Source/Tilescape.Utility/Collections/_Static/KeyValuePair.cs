﻿using System.Collections.Generic;

namespace Tilescape.Utility.Collections
{
	public static class KeyValuePair
	{
		public static KeyValuePair<TKey, TValue> New<TKey, TValue>(TKey key, TValue value)
		{
			return new KeyValuePair<TKey, TValue>(key, value);
		}
	}
}
