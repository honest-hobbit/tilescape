﻿using System;
using System.Collections.Generic;

namespace Tilescape.Utility.Collections
{
	public static class StructEquality
	{
		public static EqualityComparer<T> Comparer<T>()
			where T : struct, IEquatable<T> => Singleton<T>.Instance;

		/// <summary>
		/// Provides identity comparison and equality logic for any reference type.
		/// </summary>
		/// <typeparam name="T">The type of the values.</typeparam>
		private class Singleton<T> : EqualityComparer<T>
			where T : struct, IEquatable<T>
		{
			/// <summary>
			/// Prevents a default instance of the <see cref="Singleton{T}"/> class from being created.
			/// </summary>
			private Singleton()
			{
			}

			public static EqualityComparer<T> Instance { get; } = new Singleton<T>();

			/// <inheritdoc />
			public override bool Equals(T a, T b) => a.Equals(b);

			/// <inheritdoc />
			public override int GetHashCode(T obj) => obj.GetHashCode();
		}
	}
}
