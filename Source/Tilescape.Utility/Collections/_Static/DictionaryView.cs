﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public static class DictionaryView
	{
		public static IDictionaryView<TKey, TValue> Empty<TKey, TValue>() => EmptyDictionary<TKey, TValue>.Instance;

		public static IDictionaryView<TKey, TValue> From<TKey, TValue>(IDictionary<TKey, TValue> dictionary) =>
			new DictionaryWrapper<TKey, TValue>(dictionary);

		public static IDictionaryView<TKey, TValue> FromView<TKey, TValue>(IDictionaryView<TKey, TValue> dictionary) =>
			new DictionaryViewWrapper<TKey, TValue>(dictionary);

		public static IDictionaryView<TKey, TResult> ConvertView<TKey, TSource, TResult>(
			IDictionaryView<TKey, TSource> source, Func<TSource, TResult> converter) =>
			new DictionaryViewConverterWrapper<TKey, TSource, TResult>(source, converter);

		public static IDictionaryView<TKey, TResult> Convert<TKey, TSource, TResult>(
			IDictionary<TKey, TSource> source, Func<TSource, TResult> converter) =>
			new DictionaryConverterWrapper<TKey, TSource, TResult>(source, converter);

		#region Private Classes

		private class EmptyDictionary<TKey, TValue> : IDictionaryView<TKey, TValue>
		{
			private EmptyDictionary()
			{
			}

			public static IDictionaryView<TKey, TValue> Instance { get; } = new EmptyDictionary<TKey, TValue>();

			/// <inheritdoc />
			public int Count => 0;

			/// <inheritdoc />
			public ISetView<TKey> Keys => SetView.Empty<TKey>();

			/// <inheritdoc />
			public ICollectionView<TValue> Values => CollectionView.Empty<TValue>();

			/// <inheritdoc />
			public TValue this[TKey key]
			{
				get
				{
					IDictionaryViewContracts.Indexer(this, key);

					throw new KeyNotFoundException();
				}
			}

			/// <inheritdoc />
			public bool ContainsKey(TKey key) => false;

			/// <inheritdoc />
			public bool TryGetValue(TKey key, out TValue value)
			{
				value = default(TValue);
				return false;
			}

			/// <inheritdoc />
			public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
			{
				yield break;
			}

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class DictionaryWrapper<TKey, TValue> : IDictionaryView<TKey, TValue>
		{
			private readonly IDictionary<TKey, TValue> dictionary;

			public DictionaryWrapper(IDictionary<TKey, TValue> dictionary)
			{
				Require.That(dictionary != null);

				this.dictionary = dictionary;
				this.Keys = dictionary.KeysAsSetView();
				this.Values = dictionary.Values.AsCollectionView();
			}

			/// <inheritdoc />
			public int Count => this.dictionary.Count;

			/// <inheritdoc />
			public ISetView<TKey> Keys { get; }

			/// <inheritdoc />
			public ICollectionView<TValue> Values { get; }

			/// <inheritdoc />
			public TValue this[TKey key] => this.dictionary[key];

			/// <inheritdoc />
			public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

			/// <inheritdoc />
			public bool TryGetValue(TKey key, out TValue value) => this.dictionary.TryGetValue(key, out value);

			/// <inheritdoc />
			public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.dictionary.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class DictionaryViewWrapper<TKey, TValue> : IDictionaryView<TKey, TValue>
		{
			private readonly IDictionaryView<TKey, TValue> dictionary;

			public DictionaryViewWrapper(IDictionaryView<TKey, TValue> dictionary)
			{
				Require.That(dictionary != null);

				this.dictionary = dictionary;
			}

			/// <inheritdoc />
			public int Count => this.dictionary.Count;

			/// <inheritdoc />
			public ISetView<TKey> Keys => this.dictionary.Keys;

			/// <inheritdoc />
			public ICollectionView<TValue> Values => this.dictionary.Values;

			/// <inheritdoc />
			public TValue this[TKey key] => this.dictionary[key];

			/// <inheritdoc />
			public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

			/// <inheritdoc />
			public bool TryGetValue(TKey key, out TValue value) => this.dictionary.TryGetValue(key, out value);

			/// <inheritdoc />
			public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.dictionary.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class DictionaryConverterWrapper<TKey, TSource, TResult> : IDictionaryView<TKey, TResult>
		{
			private readonly IDictionary<TKey, TSource> dictionary;

			private readonly Func<TSource, TResult> converter;

			public DictionaryConverterWrapper(IDictionary<TKey, TSource> dictionary, Func<TSource, TResult> converter)
			{
				Require.That(dictionary != null);
				Require.That(converter != null);

				this.dictionary = dictionary;
				this.converter = converter;
				this.Keys = dictionary.KeysAsSetView();
				this.Values = CollectionView.Convert(dictionary.Values, converter);
			}

			/// <inheritdoc />
			public int Count => this.dictionary.Count;

			/// <inheritdoc />
			public ISetView<TKey> Keys { get; }

			/// <inheritdoc />
			public ICollectionView<TResult> Values { get; }

			/// <inheritdoc />
			public TResult this[TKey key] => this.converter(this.dictionary[key]);

			/// <inheritdoc />
			public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

			/// <inheritdoc />
			public bool TryGetValue(TKey key, out TResult value)
			{
				if (this.dictionary.TryGetValue(key, out var source))
				{
					value = this.converter(source);
					return true;
				}
				else
				{
					value = default(TResult);
					return false;
				}
			}

			/// <inheritdoc />
			public IEnumerator<KeyValuePair<TKey, TResult>> GetEnumerator() =>
				this.dictionary.Select(x => KeyValuePair.New(x.Key, this.converter(x.Value))).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class DictionaryViewConverterWrapper<TKey, TSource, TResult> : IDictionaryView<TKey, TResult>
		{
			private readonly IDictionaryView<TKey, TSource> dictionary;

			private readonly Func<TSource, TResult> converter;

			public DictionaryViewConverterWrapper(IDictionaryView<TKey, TSource> dictionary, Func<TSource, TResult> converter)
			{
				Require.That(dictionary != null);
				Require.That(converter != null);

				this.dictionary = dictionary;
				this.converter = converter;
				this.Values = CollectionView.ConvertView(dictionary.Values, converter);
			}

			/// <inheritdoc />
			public int Count => this.dictionary.Count;

			/// <inheritdoc />
			public ISetView<TKey> Keys => this.dictionary.Keys;

			/// <inheritdoc />
			public ICollectionView<TResult> Values { get; }

			/// <inheritdoc />
			public TResult this[TKey key] => this.converter(this.dictionary[key]);

			/// <inheritdoc />
			public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

			/// <inheritdoc />
			public bool TryGetValue(TKey key, out TResult value)
			{
				if (this.dictionary.TryGetValue(key, out var source))
				{
					value = this.converter(source);
					return true;
				}
				else
				{
					value = default(TResult);
					return false;
				}
			}

			/// <inheritdoc />
			public IEnumerator<KeyValuePair<TKey, TResult>> GetEnumerator() =>
				this.dictionary.Select(x => KeyValuePair.New(x.Key, this.converter(x.Value))).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		#endregion
	}
}
