﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Enums;

namespace Tilescape.Utility.Collections
{
	public static class Equality
	{
		public static EqualityComparer<T> ClassComparer<T>()
			where T : class => EqualityComparer<T>.Default;

		public static EqualityComparer<T> StructComparer<T>()
			where T : struct, IEquatable<T> => StructEquality.Comparer<T>();

		public static EqualityComparer<T> EnumComparer<T>()
			where T : struct => EnumEquality.Comparer<T>();

		public static EqualityComparer<T> IdentityComparer<T>()
			where T : class => IdentityEquality.Comparer<T>();
	}
}
