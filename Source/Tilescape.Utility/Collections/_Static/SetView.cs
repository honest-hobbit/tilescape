﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Collections
{
	public static class SetView
	{
		public static ISetView<T> Empty<T>() => EmptySet<T>.Instance;

		public static ISetView<T> From<T>(ICollection<T> collection) => new CollectionWrapper<T>(collection);

		public static ISetView<T> FromView<T>(ISetView<T> set) => new SetViewWrapper<T>(set);

		public static ISetView<TKey> FromKeys<TKey, TValue>(IDictionary<TKey, TValue> dictionary) =>
			new DictionaryWrapper<TKey, TValue>(dictionary);

		public static ISetView<TResult> Convert<TSource, TResult>(
			ISetView<TSource> set, ITwoWayConverter<TSource, TResult> converter) =>
			new SetViewConverter<TSource, TResult>(set, converter);

		public static ISetView<T> CreateUnordered<T>(IEnumerable<T> values, IEqualityComparer<T> comparer = null) =>
			new HashSet<T>(values, comparer).AsSetView();

		public static ISetView<T> CreateOrdered<T>(IEnumerable<T> values, IEqualityComparer<T> comparer = null) =>
			new OrderedHashSet<T>(values, comparer).AsSetView();

		#region Private Classes

		private class EmptySet<T> : ISetView<T>
		{
			private EmptySet()
			{
			}

			public static ISetView<T> Instance { get; } = new EmptySet<T>();

			/// <inheritdoc />
			public int Count => 0;

			/// <inheritdoc />
			public bool Contains(T value) => false;

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator()
			{
				yield break;
			}

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class CollectionWrapper<T> : ISetView<T>
		{
			private readonly ICollection<T> collection;

			public CollectionWrapper(ICollection<T> collection)
			{
				Require.That(collection != null);

				this.collection = collection;
			}

			/// <inheritdoc />
			public int Count => this.collection.Count;

			/// <inheritdoc />
			public bool Contains(T value) => this.collection.Contains(value);

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.collection.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class DictionaryWrapper<TKey, TValue> : ISetView<TKey>
		{
			private readonly IDictionary<TKey, TValue> dictionary;

			public DictionaryWrapper(IDictionary<TKey, TValue> dictionary)
			{
				Require.That(dictionary != null);

				this.dictionary = dictionary;
			}

			/// <inheritdoc />
			public int Count => this.dictionary.Count;

			/// <inheritdoc />
			public bool Contains(TKey value) => this.dictionary.ContainsKey(value);

			/// <inheritdoc />
			public IEnumerator<TKey> GetEnumerator() => this.dictionary.Keys.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class SetViewWrapper<T> : ISetView<T>
		{
			private readonly ISetView<T> set;

			public SetViewWrapper(ISetView<T> set)
			{
				Require.That(set != null);

				this.set = set;
			}

			/// <inheritdoc />
			public int Count => this.set.Count;

			/// <inheritdoc />
			public bool Contains(T value) => this.set.Contains(value);

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.set.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class SetViewConverter<TSource, TResult> : ISetView<TResult>
		{
			private readonly ISetView<TSource> set;

			private readonly ITwoWayConverter<TSource, TResult> converter;

			public SetViewConverter(ISetView<TSource> set, ITwoWayConverter<TSource, TResult> converter)
			{
				Require.That(set != null);
				Require.That(converter != null);

				this.set = set;
				this.converter = converter;
			}

			/// <inheritdoc />
			public int Count => this.set.Count;

			/// <inheritdoc />
			public bool Contains(TResult value) => this.set.Contains(this.converter.Convert(value));

			/// <inheritdoc />
			public IEnumerator<TResult> GetEnumerator()
			{
				foreach (var value in this.set)
				{
					yield return this.converter.Convert(value);
				}
			}

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		#endregion
	}
}
