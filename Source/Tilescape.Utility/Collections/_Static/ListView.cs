﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public static class ListView
	{
		public static IListView<T> Empty<T>() => EmptyList<T>.Instance;

		public static IListView<T> From<T>(IList<T> list) => new ListWrapper<T>(list);

		public static IListView<T> FromView<T>(IListView<T> list) => new ListViewWrapper<T>(list);

		public static IListView<TResult> ConvertView<TSource, TResult>(IListView<TSource> source, Func<TSource, TResult> converter) =>
			new ListViewConverter<TSource, TResult>(source, converter);

		public static IListView<TResult> Convert<TSource, TResult>(IList<TSource> source, Func<TSource, TResult> converter) =>
			new ListConverter<TSource, TResult>(source, converter);

		public static IListView<T> CombineParams<T>(params IListView<T>[] lists) => Combine(lists);

		public static IListView<T> Combine<T>(IEnumerable<IListView<T>> lists) => new ListViewComposite<T>(lists);

		public static IListView<T> Partition<T>(IListView<T> list, int startIndex, int length) =>
			new ListViewPartition<T>(list, startIndex, length);

		#region Private Classes

		private static class EmptyList<T>
		{
			public static IListView<T> Instance { get; } = Factory.EmptyArray<T>().AsListView();
		}

		private class ListWrapper<T> : IListView<T>
		{
			private readonly IList<T> list;

			public ListWrapper(IList<T> list)
			{
				Require.That(list != null);

				this.list = list;
			}

			/// <inheritdoc />
			public int Count => this.list.Count;

			/// <inheritdoc />
			public T this[int index] => this.list[index];

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.list.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class ListViewWrapper<T> : IListView<T>
		{
			private readonly IListView<T> list;

			public ListViewWrapper(IListView<T> list)
			{
				Require.That(list != null);

				this.list = list;
			}

			/// <inheritdoc />
			public int Count => this.list.Count;

			/// <inheritdoc />
			public T this[int index] => this.list[index];

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.list.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class ListViewConverter<TSource, TResult> : IListView<TResult>
		{
			private readonly IListView<TSource> source;

			private readonly Func<TSource, TResult> converter;

			public ListViewConverter(IListView<TSource> source, Func<TSource, TResult> converter)
			{
				Require.That(source != null);
				Require.That(converter != null);

				this.source = source;
				this.converter = converter;
			}

			/// <inheritdoc />
			public int Count => this.source.Count;

			/// <inheritdoc />
			public TResult this[int index] => this.converter(this.source[index]);

			/// <inheritdoc />
			public IEnumerator<TResult> GetEnumerator() => this.source.Select(value => this.converter(value)).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class ListConverter<TSource, TResult> : IListView<TResult>
		{
			private readonly IList<TSource> source;

			private readonly Func<TSource, TResult> converter;

			public ListConverter(IList<TSource> source, Func<TSource, TResult> converter)
			{
				Require.That(source != null);
				Require.That(converter != null);

				this.source = source;
				this.converter = converter;
			}

			/// <inheritdoc />
			public int Count => this.source.Count;

			/// <inheritdoc />
			public TResult this[int index] => this.converter(this.source[index]);

			/// <inheritdoc />
			public IEnumerator<TResult> GetEnumerator() => this.source.Select(value => this.converter(value)).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class ListViewComposite<T> : IListView<T>
		{
			private readonly IEnumerable<IListView<T>> lists;

			public ListViewComposite(IEnumerable<IListView<T>> lists)
			{
				Require.That(lists.AllAndSelfNotNull());

				this.lists = lists;
			}

			/// <inheritdoc />
			public int Count => this.lists.Select(list => list.Count).Sum();

			/// <inheritdoc />
			public T this[int index]
			{
				get
				{
					IListViewContracts.Indexer(this, index);

					foreach (var list in this.lists)
					{
						if (list.IsIndexInViewBounds(index))
						{
							return list[index];
						}
						else
						{
							index -= list.Count;
						}
					}

					throw new UnreachableCodeException("Contracts should guarantee this line is never reached.");
				}
			}

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator()
			{
				foreach (var list in this.lists)
				{
					foreach (var value in list)
					{
						yield return value;
					}
				}
			}

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class ListViewPartition<T> : IListView<T>
		{
			private readonly IListView<T> list;

			private readonly int startIndex;

			public ListViewPartition(IListView<T> list, int startIndex, int length)
			{
				Require.That(list != null);
				Require.That(length >= 0);
				Require.That(list.IsIndexInViewBounds(startIndex));
				Require.That(list.IsIndexInViewBounds(startIndex + length));

				this.list = list;
				this.startIndex = startIndex;
				this.Count = length;
			}

			/// <inheritdoc />
			public int Count { get; }

			/// <inheritdoc />
			public T this[int index]
			{
				get
				{
					IListViewContracts.Indexer(this, index);

					return this.list[index + this.startIndex];
				}
			}

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator()
			{
				for (int index = this.startIndex; index < this.startIndex + this.Count; index++)
				{
					yield return this.list[index];
				}
			}

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		#endregion
	}
}
