﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Collections
{
	public static class CollectionView
	{
		public static ICollectionView<T> Empty<T>() => EmptyCollection<T>.Instance;

		public static ICollectionView<T> From<T>(IEnumerable<T> values, int count) => new EnumerableView<T>(values, count);

		public static ICollectionView<T> From<T>(IEnumerable<T> values)
		{
			Require.That(values != null);

			if (values is ICollection<T> collection)
			{
				return From(collection);
			}

			if (values is ICollectionView<T> view)
			{
				return FromView(view);
			}

			return new EnumerableView<T>(values, values.Count());
		}

		public static ICollectionView<T> From<T>(ICollection<T> values) => new CollectionWrapper<T>(values);

		public static ICollectionView<T> FromView<T>(ICollectionView<T> values) => new CollectionViewWrapper<T>(values);

		public static ICollectionView<TResult> ConvertView<TSource, TResult>(
			ICollectionView<TSource> source, Func<TSource, TResult> converter) =>
			new CollectionViewConverterWrapper<TSource, TResult>(source, converter);

		public static ICollectionView<TResult> Convert<TSource, TResult>(
			ICollection<TSource> source, Func<TSource, TResult> converter) =>
			new CollectionConverterWrapper<TSource, TResult>(source, converter);

		public static ICollectionView<T> CombineParams<T>(params ICollection<T>[] collections) => Combine(collections);

		public static ICollectionView<T> Combine<T>(IEnumerable<ICollection<T>> collections) => new CombinedCollections<T>(collections);

		public static ICollectionView<T> CombineParams<T>(params ICollectionView<T>[] collections) => Combine(collections);

		public static ICollectionView<T> Combine<T>(IEnumerable<ICollectionView<T>> collections) => new CombinedViews<T>(collections);

		public static ICollectionView<T> CombineParams<T>(params IEnumerable<T>[] enumerables) => CombineEnumerables(enumerables);

		public static ICollectionView<T> CombineEnumerables<T>(IEnumerable<IEnumerable<T>> enumerables)
		{
			Require.That(enumerables.AllAndSelfNotNull());

			return new EnumerableView<T>(enumerables.SelectMany(x => x), enumerables.Sum(x => x.CountExtended()));
		}

		public static class Large
		{
			public static ILargeCollectionView<T> Empty<T>() => EmptyCollection<T>.LargeInstance;

			public static ILargeCollectionView<T> From<T>(IEnumerable<T> values, long count) => new LargeEnumerableView<T>(values, count);

			public static ILargeCollectionView<T> From<T>(T[] array) => new LargeEnumerableView<T>(array, array.LongLength);
		}

		#region Private Classes

		private static class EmptyCollection<T>
		{
			public static ICollectionView<T> Instance { get; } = new EnumerableView<T>(Enumerable.Empty<T>(), 0);

			public static ILargeCollectionView<T> LargeInstance { get; } = new LargeEnumerableView<T>(Enumerable.Empty<T>(), 0);
		}

		private class EnumerableView<T> : ICollectionView<T>
		{
			private readonly IEnumerable<T> values;

			public EnumerableView(IEnumerable<T> values, int count)
			{
				Require.That(values != null);
				Require.That(count >= 0);
				Require.That(values.CountExtended() == count);

				this.values = values;
				this.Count = count;
			}

			/// <inheritdoc />
			public int Count { get; }

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.values.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class LargeEnumerableView<T> : ILargeCollectionView<T>
		{
			private readonly IEnumerable<T> values;

			public LargeEnumerableView(IEnumerable<T> values, long count)
			{
				Require.That(values != null);
				Require.That(count >= 0);
				Require.That(values.LongCountExtended() == count);

				this.values = values;
				this.Count = count;
			}

			/// <inheritdoc />
			public long Count { get; }

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.values.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class CombinedCollections<T> : ICollectionView<T>
		{
			private readonly IEnumerable<ICollection<T>> values;

			public CombinedCollections(IEnumerable<ICollection<T>> values)
			{
				Require.That(values.AllAndSelfNotNull());

				this.values = values;
			}

			/// <inheritdoc />
			public int Count => this.values.Sum(x => x.Count);

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.values.SelectMany(x => x).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class CombinedViews<T> : ICollectionView<T>
		{
			private readonly IEnumerable<ICollectionView<T>> values;

			public CombinedViews(IEnumerable<ICollectionView<T>> values)
			{
				Require.That(values.AllAndSelfNotNull());

				this.values = values;
			}

			/// <inheritdoc />
			public int Count => this.values.Sum(x => x.Count);

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.values.SelectMany(x => x).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class CollectionWrapper<T> : ICollectionView<T>
		{
			private readonly ICollection<T> collection;

			public CollectionWrapper(ICollection<T> collection)
			{
				Require.That(collection != null);

				this.collection = collection;
			}

			/// <inheritdoc />
			public int Count => this.collection.Count;

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.collection.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class CollectionViewWrapper<T> : ICollectionView<T>
		{
			private readonly ICollectionView<T> collection;

			public CollectionViewWrapper(ICollectionView<T> collection)
			{
				Require.That(collection != null);

				this.collection = collection;
			}

			/// <inheritdoc />
			public int Count => this.collection.Count;

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.collection.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class CollectionConverterWrapper<TSource, TResult> : ICollectionView<TResult>
		{
			private readonly ICollection<TSource> collection;

			private readonly Func<TSource, TResult> converter;

			public CollectionConverterWrapper(ICollection<TSource> collection, Func<TSource, TResult> converter)
			{
				Require.That(collection != null);
				Require.That(converter != null);

				this.collection = collection;
				this.converter = converter;
			}

			/// <inheritdoc />
			public int Count => this.collection.Count;

			/// <inheritdoc />
			public IEnumerator<TResult> GetEnumerator() => this.collection.Select(x => this.converter(x)).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class CollectionViewConverterWrapper<TSource, TResult> : ICollectionView<TResult>
		{
			private readonly ICollectionView<TSource> collection;

			private readonly Func<TSource, TResult> converter;

			public CollectionViewConverterWrapper(ICollectionView<TSource> collection, Func<TSource, TResult> converter)
			{
				Require.That(collection != null);
				Require.That(converter != null);

				this.collection = collection;
				this.converter = converter;
			}

			/// <inheritdoc />
			public int Count => this.collection.Count;

			/// <inheritdoc />
			public IEnumerator<TResult> GetEnumerator() => this.collection.Select(x => this.converter(x)).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		#endregion
	}
}
