﻿using System;

namespace Tilescape.Development.Tiles
{
	public static class TileShapes
	{
		#region Atlas Guids

		public static Guid AutoCubic00 { get; } = new Guid("1779e899-0a65-4af3-977d-56613f62b4d0");

		public static Guid AutoCubic08 { get; } = new Guid("f8d36df2-9674-428c-a2fc-198f1fd84a26");

		public static Guid AutoCubic09 { get; } = new Guid("cadb2463-41fb-4c67-9727-2b58a789147c");

		public static Guid AutoCubic0A { get; } = new Guid("7c2a6519-01e8-41f0-860e-ffd2a1534a72");

		public static Guid AutoCubic0E { get; } = new Guid("24d75531-3cc5-4122-ae60-d725c33f5748");

		public static Guid AutoCubic0F { get; } = new Guid("af5a03d8-e8dd-4a57-b39e-4a01f195de95");

		public static Guid AutoCubic18 { get; } = new Guid("efadc08c-9167-4ae6-aee2-d771a3634109");

		public static Guid AutoCubic19 { get; } = new Guid("41f4f838-daf1-41d9-b8cc-f83ddcb676b6");

		public static Guid AutoCubic58 { get; } = new Guid("8ea52d63-ecba-4beb-8494-f27e6bee7e48");

		public static Guid AutoCubic80 { get; } = new Guid("a7d1e68d-eb39-444e-a971-d5b66cf2b232");

		public static Guid AutoCubic82 { get; } = new Guid("4146d64d-07a1-4c2e-b832-c7ec016c2a33");

		public static Guid AutoCubic85 { get; } = new Guid("3614d246-9aee-467b-9b3b-ba776adfa8c6");

		public static Guid AutoCubic88 { get; } = new Guid("f4727651-180b-4b35-9163-89e80724f196");

		public static Guid AutoCubic8A { get; } = new Guid("ee5f6f2b-3c3a-47a5-b0a7-646c6bf9d611");

		public static Guid AutoCubic8D { get; } = new Guid("d5b2b220-a876-4a0f-8c1a-1bcf7426a0f8");

		public static Guid AutoCubic8E { get; } = new Guid("44b015d0-a94d-45a5-9dca-7ff2eb8204dd");

		public static Guid AutoCubic8F { get; } = new Guid("6ec8b7fd-b8b4-435f-8c60-a1c8d39b444c");

		public static Guid AutoCubic90 { get; } = new Guid("0e88f3b7-6a09-46b0-adb2-c10fcb6ffe57");

		public static Guid AutoCubic91 { get; } = new Guid("61df0850-7140-44d6-a69f-d56a8506461f");

		public static Guid AutoCubic99 { get; } = new Guid("de73747e-06b5-4777-a294-19354ba826ea");

		public static Guid AutoCubicA0 { get; } = new Guid("df0cc697-ba80-44b6-a12d-2b39c391e218");

		public static Guid AutoCubicA8 { get; } = new Guid("2bc79c6a-696f-4860-8db2-c87b6b6ee72b");

		public static Guid AutoCubicAA { get; } = new Guid("de2332a8-6b6c-46cd-a0b9-f83e7502e3aa");

		public static Guid AutoCubicAF { get; } = new Guid("cb4cbd1a-61b3-48bd-aee3-4ee3b200ba3c");

		public static Guid AutoCubicC3 { get; } = new Guid("28d19a7a-7d27-47e4-aad3-2da4902357ab");

		public static Guid AutoCubicCA { get; } = new Guid("af72b23c-3194-4ccb-a8fb-788286fd6b21");

		public static Guid AutoCubicCE { get; } = new Guid("92e74db5-26fe-4162-ab41-5f6301aa173e");

		public static Guid AutoCubicD8 { get; } = new Guid("6c26a8a4-d4b1-4a41-9758-59e2721aa014");

		public static Guid AutoCubicE0 { get; } = new Guid("dbff29e0-9abb-4aea-9143-a73bb0b63c1c");

		public static Guid AutoCubicE8 { get; } = new Guid("0dccd30d-2420-426c-b651-164d9d14f9f6");

		public static Guid AutoCubicEC { get; } = new Guid("4022966b-2e66-49f1-9f5a-8d6c09254632");

		public static Guid AutoCubicEE { get; } = new Guid("78dc06da-31f4-4b71-8b29-24170eb9ca0b");

		public static Guid AutoCubicEF { get; } = new Guid("ebc595a7-8fd6-45a0-ae69-51f43664ab82");

		public static Guid AutoCubicF0 { get; } = new Guid("77ea96c2-7ab1-4516-8689-5afe87fef98d");

		public static Guid AutoCubicF8 { get; } = new Guid("7a569978-001b-4ef6-ac1c-8c0992a8b558");

		public static Guid AutoCubicFA { get; } = new Guid("abf0e92e-8c93-41cc-95e6-781d48e0915c");

		public static Guid AutoCubicFE { get; } = new Guid("4a003ae1-7608-48b3-a551-56307a9af9a1");

		public static Guid AutoCubicFF { get; } = new Guid("2683ca67-699d-4c8a-8289-a72d3d910355");

		public static Guid AutoShapeBottom00 { get; } = new Guid("8cda300e-8efd-4b05-855d-747f72d005ee");

		public static Guid AutoShapeBottom01 { get; } = new Guid("c40f39cb-4371-4759-b6a8-2982d186b9e7");

		public static Guid AutoShapeBottom02 { get; } = new Guid("32fe494d-76d8-4c4b-b89d-3fcbc41b9d03");

		public static Guid AutoShapeBottom03 { get; } = new Guid("b6ff479e-1ec0-4019-baa8-8e95e365ec5e");

		public static Guid AutoShapeBottom04 { get; } = new Guid("7fb3f60e-1155-418e-8298-9c51ce58bea9");

		public static Guid AutoShapeBottom05 { get; } = new Guid("a2ca87df-31b4-402c-a92d-adafe2f37388");

		public static Guid AutoShapeBottom06 { get; } = new Guid("ea793a53-5c98-43ed-bc63-d49fba6f4512");

		public static Guid AutoShapeBottom07 { get; } = new Guid("bef1ea73-b1d9-4d78-85b4-6f38beb6bd55");

		public static Guid AutoShapeBottom08 { get; } = new Guid("5ae1ff4b-4f7a-4c37-b383-aa80ef01e81b");

		public static Guid AutoShapeBottom09 { get; } = new Guid("8e0ed31f-23c9-4452-bd4a-5bea827142dd");

		public static Guid AutoShapeBottom10 { get; } = new Guid("e8d92018-2ede-40c4-8f74-bbb38658e5f0");

		public static Guid AutoShapeBottom11 { get; } = new Guid("d2997783-fa68-40c8-83b6-7316d4d9073a");

		public static Guid AutoShapeBottom12 { get; } = new Guid("4de86ddc-b3b6-4b0f-83eb-38de826272a7");

		public static Guid AutoShapeBottom13 { get; } = new Guid("6de095b0-2fb9-494b-8366-eebb9a26310f");

		public static Guid AutoShapeSlimBottom0 { get; } = new Guid("a4783110-2a7f-4772-8013-c258e08d80d9");

		public static Guid AutoShapeSlimBottom1 { get; } = new Guid("18b5246e-3cc8-4e0d-a40a-8b5518efc37c");

		public static Guid AutoShapeSlimBottom2 { get; } = new Guid("49f23118-18be-4516-a746-9f91f137b787");

		public static Guid AutoShapeSlimBottom3 { get; } = new Guid("a9332772-ecc1-482a-b9e8-7d8caed803f1");

		public static Guid AutoShapeSlimTop0 { get; } = new Guid("7e144d1d-79c1-4adb-bfa0-bf152270ea7c");

		public static Guid AutoShapeSlimTop1 { get; } = new Guid("6e61723e-8698-420f-930e-d9257d40a102");

		public static Guid AutoShapeSlimTop2 { get; } = new Guid("da0b0290-69d5-4e69-b60a-96ed8088884f");

		public static Guid AutoShapeSlimTop3 { get; } = new Guid("a730c6e8-d04f-48e5-a993-bf0a18c337b7");

		public static Guid AutoShapeTop00 { get; } = new Guid("f4cb5ecb-2784-4ad1-8c52-15314b4ba133");

		public static Guid AutoShapeTop01 { get; } = new Guid("48502060-7f01-4685-8b43-62537a75b232");

		public static Guid AutoShapeTop02 { get; } = new Guid("a9122254-4b8e-4279-8d27-1392a2d3b356");

		public static Guid AutoShapeTop03 { get; } = new Guid("9977458c-cd61-4538-a6ee-6868e9388a21");

		public static Guid AutoShapeTop04 { get; } = new Guid("19785f64-168a-4273-88f5-d4ceb726e69b");

		public static Guid AutoShapeTop05 { get; } = new Guid("82ae4e36-0ebf-42d4-8e01-33ca878f935c");

		public static Guid AutoShapeTop06 { get; } = new Guid("e46002e5-f53a-4ce9-8fac-784a57457b96");

		public static Guid AutoShapeTop07 { get; } = new Guid("335eaa06-6d29-4fe3-aba7-f0e12183126f");

		public static Guid AutoShapeTop08 { get; } = new Guid("b81ae110-8407-4c67-81a4-96fd6034de5f");

		public static Guid AutoShapeTop09 { get; } = new Guid("1eb64cbc-c3ef-4bf2-9bfe-2bcee9d138ca");

		public static Guid AutoShapeTop10 { get; } = new Guid("e9746bf6-96e7-49d5-84c6-20df20198323");

		public static Guid AutoShapeTop11 { get; } = new Guid("06a48a01-3686-4a71-8a96-fa5edd89faf4");

		public static Guid AutoShapeTop12 { get; } = new Guid("1e8f0855-334e-44de-aaf5-4c9ff4d79ced");

		public static Guid AutoShapeTop13 { get; } = new Guid("bf9e6c50-930c-4131-9483-31c87e6dabed");

		public static Guid Axis00_00_00 { get; } = new Guid("641dd574-4df3-44b2-87c8-f9aa86714df2");

		public static Guid Axis00_00_01 { get; } = new Guid("7d29e3b6-7937-4bfa-a66c-72ed834b7e38");

		public static Guid Axis00_00_10 { get; } = new Guid("57b6483f-a3cd-4a30-b9cb-3c40c598d316");

		public static Guid Axis00_00_11 { get; } = new Guid("28b6a271-f086-4bde-bb14-5349f9da93d3");

		public static Guid Axis00_01_00 { get; } = new Guid("3fd1574f-fe31-4275-93e4-6dce888368d6");

		public static Guid Axis00_01_01 { get; } = new Guid("5ff51ef2-89d4-4c1a-ba81-912a50bab6aa");

		public static Guid Axis00_01_10 { get; } = new Guid("83d214fc-a40d-41a2-bb5a-a307e2948e81");

		public static Guid Axis00_01_11 { get; } = new Guid("cb21da44-7c79-4aed-bbb1-f0548f2d359c");

		public static Guid Axis00_10_00 { get; } = new Guid("e56c1c7c-181b-40fe-81d7-7973eab4b2ac");

		public static Guid Axis00_10_01 { get; } = new Guid("df003d2f-fdbf-4e76-a93e-4ecd3df9ce6b");

		public static Guid Axis00_10_10 { get; } = new Guid("09dae0ed-93f4-4e89-8abb-db78f29f65de");

		public static Guid Axis00_10_11 { get; } = new Guid("8d444ea2-d568-44ec-896c-9116110fd64b");

		public static Guid Axis00_11_00 { get; } = new Guid("e40b91d4-599a-4022-bb02-57b9d1541021");

		public static Guid Axis00_11_01 { get; } = new Guid("025b8f69-ea03-4083-a834-b040b5a6a11d");

		public static Guid Axis00_11_10 { get; } = new Guid("9eb67596-ac9d-4d0c-beda-7276c5b8d5bf");

		public static Guid Axis00_11_11 { get; } = new Guid("d40bc7d7-aefc-4b51-8d6b-d258172b444d");

		public static Guid Axis01_00_00 { get; } = new Guid("7c8cb0df-350f-420a-9292-09064e387c20");

		public static Guid Axis01_00_01 { get; } = new Guid("6d3d8909-f801-4ada-985f-5f86f3d85537");

		public static Guid Axis01_00_10 { get; } = new Guid("80ed361b-3379-4de3-af46-06c2f6293721");

		public static Guid Axis01_00_11 { get; } = new Guid("7b409767-d040-4ee3-b27d-a4f3cbe063d3");

		public static Guid Axis01_01_00 { get; } = new Guid("411a75a1-f004-42ea-ad19-7b5a56720d20");

		public static Guid Axis01_01_01 { get; } = new Guid("05afcb62-57b4-4c70-a550-cddc23e0a65b");

		public static Guid Axis01_01_10 { get; } = new Guid("01a93747-efa1-4572-ae3b-a5ff8473cd93");

		public static Guid Axis01_01_11 { get; } = new Guid("1b8a60d8-6507-47d9-9012-4e153e836b22");

		public static Guid Axis01_10_00 { get; } = new Guid("ad566170-f9b2-43d3-a719-1d587babbdf1");

		public static Guid Axis01_10_01 { get; } = new Guid("038390c2-55e2-45b2-8647-c45c17aac77a");

		public static Guid Axis01_10_10 { get; } = new Guid("80f685a4-6c65-414e-8609-ece2cee7a008");

		public static Guid Axis01_10_11 { get; } = new Guid("34c4512a-9680-4cb5-94db-3302a87a9984");

		public static Guid Axis01_11_00 { get; } = new Guid("0d537dfe-a269-4c55-9cd1-0f97eda1130d");

		public static Guid Axis01_11_01 { get; } = new Guid("1ae6c11e-4fa2-4876-8a13-4d7d536b9082");

		public static Guid Axis01_11_10 { get; } = new Guid("f21c5d80-fed3-4e0b-9c54-dc5af98a8eaa");

		public static Guid Axis01_11_11 { get; } = new Guid("0c81eb13-9c7d-4831-80b1-14c3376cc3b5");

		public static Guid Axis10_00_00 { get; } = new Guid("ed0a1c55-27e5-49de-82a6-65898d0cb9de");

		public static Guid Axis10_00_01 { get; } = new Guid("e1c7e35d-02dc-4304-b491-85979176ac6a");

		public static Guid Axis10_00_10 { get; } = new Guid("7daf95cf-b4b0-49ed-9bb3-d18a6f55a64b");

		public static Guid Axis10_00_11 { get; } = new Guid("0bab438e-9c74-46d7-a025-128ccade4813");

		public static Guid Axis10_01_00 { get; } = new Guid("71a9f58d-3210-479b-be76-8d4e3c3d60e1");

		public static Guid Axis10_01_01 { get; } = new Guid("406c3065-91b4-4c55-96f6-0716a171f27e");

		public static Guid Axis10_01_10 { get; } = new Guid("419ecfca-bedc-4ff8-8e8c-52bd1ad1685b");

		public static Guid Axis10_01_11 { get; } = new Guid("3b41d1be-640f-4820-8e1f-4b2513111132");

		public static Guid Axis10_10_00 { get; } = new Guid("f5292a2f-6938-4671-9c95-4822f616bcd8");

		public static Guid Axis10_10_01 { get; } = new Guid("c1db3e82-13d8-41d4-8f6d-a467cf22c48e");

		public static Guid Axis10_10_10 { get; } = new Guid("2a6f7559-87bc-4f9c-aad2-d424c6eefc09");

		public static Guid Axis10_10_11 { get; } = new Guid("5cb862e1-d874-415e-b923-8ec7c51a0ab1");

		public static Guid Axis10_11_00 { get; } = new Guid("7a72f428-1f08-476f-830b-a90620252e20");

		public static Guid Axis10_11_01 { get; } = new Guid("db413de2-67a2-438d-9218-32ea77f5e170");

		public static Guid Axis10_11_10 { get; } = new Guid("148a2a74-3dc5-47d2-81bc-24c9dcb48d8f");

		public static Guid Axis10_11_11 { get; } = new Guid("21833575-f70f-4896-8195-80c264be3e18");

		public static Guid Axis11_00_00 { get; } = new Guid("2a6d34c1-fed1-4068-bab3-6e23ed512412");

		public static Guid Axis11_00_01 { get; } = new Guid("a2ecbf32-d25f-46c1-bcc9-51b62af72f5f");

		public static Guid Axis11_00_10 { get; } = new Guid("f103cd09-9105-4e26-bac7-63e0763e0e88");

		public static Guid Axis11_00_11 { get; } = new Guid("758e1162-fdb3-4a63-987c-e6a8396d313e");

		public static Guid Axis11_01_00 { get; } = new Guid("8e5fb547-d901-4583-a09f-55cc13f0d309");

		public static Guid Axis11_01_01 { get; } = new Guid("f3025b48-a941-4ee1-ab60-3b6693f92f76");

		public static Guid Axis11_01_10 { get; } = new Guid("aa88966a-dce6-4c6d-874e-a3e8b904ed67");

		public static Guid Axis11_01_11 { get; } = new Guid("60b74e2d-cfd4-440f-91ae-98e1ff4baefb");

		public static Guid Axis11_10_00 { get; } = new Guid("1a89a665-7455-4d35-853f-203d44f222c1");

		public static Guid Axis11_10_01 { get; } = new Guid("0cb53354-e00f-4269-b4ef-b1c10b019f7c");

		public static Guid Axis11_10_10 { get; } = new Guid("3c7d2ce8-9735-4134-b361-269e8bba3fab");

		public static Guid Axis11_10_11 { get; } = new Guid("2bec93f2-2708-41ef-8dda-77a063698818");

		public static Guid Axis11_11_00 { get; } = new Guid("57bab438-e783-41b1-a896-e97a88832dc0");

		public static Guid Axis11_11_01 { get; } = new Guid("d99e63d7-8c56-427a-9412-af1ec06d0ae8");

		public static Guid Axis11_11_10 { get; } = new Guid("f0114fb2-8cc9-49d7-b828-49760365fb76");

		public static Guid Axis11_11_11 { get; } = new Guid("e60cc2b1-3422-4301-88a1-703dff84d606");

		public static Guid Chest { get; } = new Guid("992e70b2-1d16-4601-a905-f443ad59adde");

		public static Guid Colors { get; } = new Guid("15a669fb-b2c8-4f2a-92c3-1f907676afb7");

		public static Guid Empty { get; } = new Guid("35d737ad-30cd-40b2-82ab-e90e721ca696");

		public static Guid EyeBlock { get; } = new Guid("14fa27fd-468d-41f0-ba84-c0432c7a275b");

		public static Guid FancyBlock { get; } = new Guid("c605a5fc-27b7-44ef-95a2-cf04c3185c0a");

		public static Guid Grass { get; } = new Guid("6ed80458-fed9-4d42-b2d9-ac00fc38fb3e");

		public static Guid GrassCliff { get; } = new Guid("61f34b76-dab8-46f6-a629-c1997df9c696");

		public static Guid GrassFlower1 { get; } = new Guid("5dacad1c-d36f-41a1-9b2a-4c0e057f5c3f");

		public static Guid GrassFlower2 { get; } = new Guid("1707d90f-caeb-47e9-8331-d2bfd093cdbf");

		public static Guid GrassRock1 { get; } = new Guid("047e6a5b-8a92-4f70-ac09-0e95e6cdaf64");

		public static Guid GrassRock2 { get; } = new Guid("6ce477ef-2f9b-4b3d-96c5-daded8fc47fc");

		public static Guid Greenie { get; } = new Guid("05f250e7-f4a0-42a3-9e0e-5ab3363f7c2c");

		public static Guid RotationCube { get; } = new Guid("b9273c58-608e-4e13-98ad-c3d35d3ae713");

		public static Guid Segment04 { get; } = new Guid("33ca3d29-1c25-4920-bf3a-b619cdc6f1ea");

		public static Guid Segment0C { get; } = new Guid("2d8d0e5b-ee24-4b48-a278-1f710b3c35e8");

		public static Guid Segment0D { get; } = new Guid("7ad5b4cd-2d90-4993-92ba-8f2f8961f397");

		public static Guid Segment0F { get; } = new Guid("32136959-bb9d-4eac-a84c-792364df3ee9");

		public static Guid Segment40 { get; } = new Guid("a8361790-c983-44a2-8b37-6dadb00bb14a");

		public static Guid Segment44 { get; } = new Guid("7226502f-6c1e-4796-b025-ed8358d4546e");

		public static Guid Segment45 { get; } = new Guid("d848b613-9d6b-41be-888d-4a495ec5e1d7");

		public static Guid Segment4D { get; } = new Guid("5de97cbd-3214-4a7d-9328-6200a57b0795");

		public static Guid Segment4F { get; } = new Guid("12701310-43d0-4fb5-a573-06b9a1eabd4f");

		public static Guid Segment54 { get; } = new Guid("44fd67d7-5b24-43b8-bd87-309abe659aba");

		public static Guid SegmentC0 { get; } = new Guid("d4ff3819-21d1-405d-b1ae-4b9685a89647");

		public static Guid SegmentCC { get; } = new Guid("05ef4240-d25a-4a59-b5be-2db11b048261");

		public static Guid SegmentCE { get; } = new Guid("94c63f6b-ca08-4c50-a18f-1f9f43d45172");

		public static Guid SegmentCF { get; } = new Guid("5c16863a-bd66-4ec9-9601-ba4e6992edcf");

		public static Guid SegmentD0 { get; } = new Guid("bf116d9c-f74e-4c44-8fbb-ef5c0a2cebe8");

		public static Guid SegmentD4 { get; } = new Guid("9af1e3a4-af1d-4a16-a860-dfec149d8560");

		public static Guid SegmentDD { get; } = new Guid("52731ff4-5900-44aa-b37b-f8c3f9f025af");

		public static Guid SegmentDF { get; } = new Guid("da3ee44a-5904-4a2e-8f21-f31c0dca2ada");

		public static Guid SegmentEC { get; } = new Guid("0e95e2c5-c676-46a0-8334-b56cd48488dd");

		public static Guid SegmentF0 { get; } = new Guid("df4386f0-6540-49e1-ad66-0498473a550f");

		public static Guid SegmentF4 { get; } = new Guid("28660007-4f95-4969-80e1-446dcbe5b218");

		public static Guid SegmentFC { get; } = new Guid("aae5b31c-a6d7-420e-b175-2e1085f41c14");

		public static Guid SegmentFD { get; } = new Guid("167ae17a-b40c-4561-afca-eeab96203515");

		public static Guid SegmentFF { get; } = new Guid("2e4971b9-b94f-4400-8132-18dbada02f57");

		public static Guid Steps0 { get; } = new Guid("67cc6e04-d34d-45e1-9fd5-b5cc85aa896d");

		public static Guid Steps1 { get; } = new Guid("1caad605-9345-49e9-b2fb-950fe37b2ed3");

		public static Guid Steps2 { get; } = new Guid("a35b4e25-7909-43e6-bed0-1bd8767c2fd0");

		public static Guid Steps3 { get; } = new Guid("06069bc5-f52e-480c-875a-64ff5736f0fa");

		public static Guid StoneBlock { get; } = new Guid("c98bc570-679c-43b6-aa60-20b3c268a97b");

		public static Guid TrimmedBottom0 { get; } = new Guid("bed34cc3-3cf7-4478-9670-38da76b98efe");

		public static Guid TrimmedBottom1 { get; } = new Guid("78fca139-3eda-4d42-aa1c-8ec5e4dd3345");

		public static Guid TrimmedBottom2 { get; } = new Guid("c88c195c-7077-4064-b689-bd7e06543b1d");

		public static Guid TrimmedBottom3 { get; } = new Guid("1d3a1f61-bd77-4301-aa66-8aba188a7100");

		public static Guid TrimmedSlopeBottom0 { get; } = new Guid("d4ba3c07-6860-49a8-8618-452a4c8c283e");

		public static Guid TrimmedSlopeBottom1 { get; } = new Guid("7090268f-9c16-4afb-b3a6-bc856c500067");

		public static Guid TrimmedSlopeBottom2 { get; } = new Guid("c98f4fea-6e3b-4910-b4bc-17ddfa2389c8");

		public static Guid TrimmedSlopeBottom3 { get; } = new Guid("473126f1-e943-4eaf-82b9-3cdcc2d721d7");

		public static Guid TrimmedSlopeTop0 { get; } = new Guid("ab1adf8b-9168-4e05-b750-328e022e58b5");

		public static Guid TrimmedSlopeTop1 { get; } = new Guid("8ce9cf7d-0a9e-49a0-b47c-e546262dadff");

		public static Guid TrimmedSlopeTop2 { get; } = new Guid("f4118005-28b4-44f4-8064-31a999407072");

		public static Guid TrimmedSlopeTop3 { get; } = new Guid("d6c69ae7-70fa-44ab-a845-880e136bfa97");

		public static Guid TrimmedTop0 { get; } = new Guid("3052a637-f60c-4fea-86ba-6d4dad6db5de");

		public static Guid TrimmedTop1 { get; } = new Guid("06049a2e-a518-4ebf-bd2e-fb889940dbdd");

		public static Guid TrimmedTop2 { get; } = new Guid("6727f3eb-d4cb-4eb6-9a57-51bd2ec98726");

		public static Guid TrimmedTop3 { get; } = new Guid("fe178403-cd03-4508-a03d-294cbbb8c2d4");

		#endregion
	}
}
