﻿using System;

namespace Tilescape.Development.Tiles
{
	public static class TileColorPalettes
	{
		#region Atlas Guids

		public static Guid Chest { get; } = new Guid("d3be0119-43e4-4bc6-a65b-d0f0d830cdc5");

		public static Guid Default { get; } = new Guid("b0dae350-78bc-410b-a879-81e0b7c417ce");

		public static Guid Greenie { get; } = new Guid("2672dcd3-e1c2-4794-99aa-6c71eb3aa7b5");

		public static Guid Segment { get; } = new Guid("7e13aa45-2338-437a-a620-22a5b9e2b2fb");

		#endregion
	}
}
