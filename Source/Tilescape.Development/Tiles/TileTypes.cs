﻿using Tilescape.Engine;

namespace Tilescape.Development.Tiles
{
	public static class TileTypes
	{
		public static TileTypeKey Empty { get; } = new TileTypeKey(1);

		public static TileTypeKey Trimmed { get; } = new TileTypeKey(2);

		public static TileTypeKey GrassCliff { get; } = new TileTypeKey(3);

		public static TileTypeKey Grass { get; } = new TileTypeKey(4);

		public static TileTypeKey GrassFlower1 { get; } = new TileTypeKey(5);

		public static TileTypeKey GrassFlower2 { get; } = new TileTypeKey(6);

		public static TileTypeKey GrassRock1 { get; } = new TileTypeKey(7);

		public static TileTypeKey GrassRock2 { get; } = new TileTypeKey(8);

		public static TileTypeKey Greenie { get; } = new TileTypeKey(9);

		public static TileTypeKey Chest { get; } = new TileTypeKey(10);

		public static TileTypeKey EyeBlock { get; } = new TileTypeKey(11);

		public static TileTypeKey StoneBlock { get; } = new TileTypeKey(12);

		public static TileTypeKey FancyBlock { get; } = new TileTypeKey(13);

		public static TileTypeKey RotationCube { get; } = new TileTypeKey(14);

		public static TileTypeKey AutoShape { get; } = new TileTypeKey(15);

		public static TileTypeKey AutoCubic { get; } = new TileTypeKey(16);

		public static TileTypeKey AutoAxes { get; } = new TileTypeKey(17);

		////public static TileTypeKey Segment04 { get; } = new TileTypeKey(17);

		////public static TileTypeKey Segment0C { get; } = new TileTypeKey(18);

		////public static TileTypeKey Segment0D { get; } = new TileTypeKey(19);

		////public static TileTypeKey Segment0F { get; } = new TileTypeKey(20);

		////public static TileTypeKey Segment40 { get; } = new TileTypeKey(21);

		////public static TileTypeKey Segment44 { get; } = new TileTypeKey(22);

		////public static TileTypeKey Segment45 { get; } = new TileTypeKey(23);

		////public static TileTypeKey Segment4D { get; } = new TileTypeKey(24);

		////public static TileTypeKey Segment4F { get; } = new TileTypeKey(25);

		////public static TileTypeKey Segment54 { get; } = new TileTypeKey(26);

		////public static TileTypeKey SegmentC0 { get; } = new TileTypeKey(27);

		////public static TileTypeKey SegmentCC { get; } = new TileTypeKey(28);

		////public static TileTypeKey SegmentCE { get; } = new TileTypeKey(29);

		////public static TileTypeKey SegmentCF { get; } = new TileTypeKey(30);

		////public static TileTypeKey SegmentD0 { get; } = new TileTypeKey(31);

		////public static TileTypeKey SegmentD4 { get; } = new TileTypeKey(32);

		////public static TileTypeKey SegmentDD { get; } = new TileTypeKey(33);

		////public static TileTypeKey SegmentDF { get; } = new TileTypeKey(34);

		////public static TileTypeKey SegmentEC { get; } = new TileTypeKey(35);

		////public static TileTypeKey SegmentF0 { get; } = new TileTypeKey(36);

		////public static TileTypeKey SegmentF4 { get; } = new TileTypeKey(37);

		////public static TileTypeKey SegmentFC { get; } = new TileTypeKey(38);

		////public static TileTypeKey SegmentFD { get; } = new TileTypeKey(39);

		////public static TileTypeKey SegmentFF { get; } = new TileTypeKey(40);
	}
}
