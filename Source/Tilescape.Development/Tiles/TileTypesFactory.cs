﻿using System;
using System.Collections.Generic;
using Tilescape.Engine;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Types;

namespace Tilescape.Development.Tiles
{
	public static class TileTypesFactory
	{
		public static IEnumerable<ITileTypeOLD> CreateTypes(
			IDictionaryView<Guid, ITileShapeView> shapes, IDictionaryView<Guid, ITileColorPalette> colors)
		{
			Require.That(shapes != null);
			Require.That(colors != null);

			var builder = new TileTypeBuilder();
			var colorPalette = colors[TileColorPalettes.Default];

			yield return CreateTrimmed(builder, shapes, colorPalette);
			yield return CreateAutoShapeFull(builder, shapes, colorPalette);
			yield return CreateAutoSegment(builder, shapes, colors[TileColorPalettes.Segment]);
			yield return CreateAutoAxes(builder, shapes, colorPalette);

			foreach (var type in CreateSingleShapes(builder, shapes, colors))
			{
				yield return type;
			}
		}

		private static IEnumerable<ITileTypeOLD> CreateSingleShapes(
			TileTypeBuilder builder, IDictionaryView<Guid, ITileShapeView> shapes, IDictionaryView<Guid, ITileColorPalette> colors)
		{
			Require.That(builder != null);
			Require.That(shapes != null);
			Require.That(colors != null);

			var colorPalette = colors[TileColorPalettes.Default];

			builder.Key = TileTypes.Empty;
			builder.Name = nameof(TileTypes.Empty);
			builder.IsFilled = false;
			builder.Shapes.Add(shapes[TileShapes.Empty]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.RotationCube;
			builder.Name = nameof(TileTypes.RotationCube);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.RotationCube]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.Chest;
			builder.Name = nameof(TileTypes.Chest);
			builder.IsFilled = false;
			builder.Shapes.Add(shapes[TileShapes.Chest]);
			builder.ColorPalettes.Add(colors[TileColorPalettes.Chest]);
			yield return builder.Build();

			builder.Key = TileTypes.Greenie;
			builder.Name = nameof(TileTypes.Greenie);
			builder.IsFilled = false;
			builder.Shapes.Add(shapes[TileShapes.Greenie]);
			builder.ColorPalettes.Add(colors[TileColorPalettes.Greenie]);
			yield return builder.Build();

			builder.Key = TileTypes.EyeBlock;
			builder.Name = nameof(TileTypes.EyeBlock);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.EyeBlock]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.FancyBlock;
			builder.Name = nameof(TileTypes.FancyBlock);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.FancyBlock]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.StoneBlock;
			builder.Name = nameof(TileTypes.StoneBlock);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.StoneBlock]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.GrassCliff;
			builder.Name = nameof(TileTypes.GrassCliff);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.GrassCliff]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.Grass;
			builder.Name = nameof(TileTypes.Grass);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.Grass]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.GrassFlower1;
			builder.Name = nameof(TileTypes.GrassFlower1);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.GrassFlower1]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.GrassFlower2;
			builder.Name = nameof(TileTypes.GrassFlower2);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.GrassFlower2]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.GrassRock1;
			builder.Name = nameof(TileTypes.GrassRock1);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.GrassRock1]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();

			builder.Key = TileTypes.GrassRock2;
			builder.Name = nameof(TileTypes.GrassRock2);
			builder.IsFilled = true;
			builder.Shapes.Add(shapes[TileShapes.GrassRock2]);
			builder.ColorPalettes.Add(colorPalette);
			yield return builder.Build();
		}

		private static ITileTypeOLD CreateTrimmed(
			TileTypeBuilder builder, IDictionaryView<Guid, ITileShapeView> shapes, ITileColorPalette palette)
		{
			Require.That(builder != null);
			Require.That(shapes != null);
			Require.That(palette != null);

			builder.Key = TileTypes.Trimmed;
			builder.Name = nameof(TileTypes.Trimmed);
			builder.IsFilled = true;
			builder.ColorPalettes.Add(palette);

			var autoTop = new HorizontalTileUpdater(
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.TrimmedTop0])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.TrimmedTop1])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.TrimmedTop2])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.TrimmedTop3])),
				(atlas, tile) => atlas.Types[tile.Type].IsFilled);

			var autoBelow = new HorizontalTileUpdater(
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.TrimmedBottom0])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.TrimmedBottom1])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.TrimmedBottom2])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.TrimmedBottom3])),
				(atlas, tile) => atlas.Types[tile.Type].IsFilled);

			builder.Updater = new VerticalTileUpdater(autoTop, autoBelow, (atlas, tile) => atlas.Types[tile.Type].IsFilled);
			return builder.Build();
		}

		private static ITileTypeOLD CreateAutoShapeFull(
			TileTypeBuilder builder, IDictionaryView<Guid, ITileShapeView> shapes, ITileColorPalette palette)
		{
			Require.That(builder != null);
			Require.That(shapes != null);
			Require.That(palette != null);

			builder.Key = TileTypes.AutoShape;
			builder.Name = nameof(TileTypes.AutoShape);
			builder.IsFilled = true;
			builder.ColorPalettes.Add(palette);

			var autoTop = new HorizontalTileUpdater(
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop00])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop01])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop02])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop03])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop04])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop05])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop06])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop07])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop08])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop09])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop10])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop11])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop12])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeTop13])),
				(atlas, tile) => atlas.Types[tile.Type].IsFilled);

			var autoBelow = new HorizontalTileUpdater(
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom00])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom01])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom02])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom03])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom04])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom05])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom06])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom07])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom08])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom09])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom10])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom11])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom12])),
				TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoShapeBottom13])),
				(atlas, tile) => atlas.Types[tile.Type].IsFilled);

			builder.Updater = new VerticalTileUpdater(autoTop, autoBelow, (atlas, tile) => atlas.Types[tile.Type].IsFilled);
			return builder.Build();
		}

		private static ITileTypeOLD CreateAutoSegment(
			TileTypeBuilder builder, IDictionaryView<Guid, ITileShapeView> shapes, ITileColorPalette palette)
		{
			Require.That(builder != null);
			Require.That(shapes != null);
			Require.That(palette != null);

			builder.Key = TileTypes.AutoCubic;
			builder.Name = nameof(TileTypes.AutoCubic);
			builder.IsFilled = true;
			builder.ColorPalettes.Add(palette);

			var cases = new Dictionary<CubicTile, ITileUpdater>()
			{
				[CubicTile.Case00] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic00])),
				[CubicTile.Case08] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic08])),
				[CubicTile.Case09] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic09])),
				[CubicTile.Case0A] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic0A])),
				[CubicTile.Case0E] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic0E])),
				[CubicTile.Case0F] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic0F])),
				[CubicTile.Case18] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic18])),
				[CubicTile.Case19] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic19])),
				[CubicTile.Case58] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic58])),
				[CubicTile.Case80] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic80])),
				[CubicTile.Case82] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic82])),
				[CubicTile.Case85] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic85])),
				[CubicTile.Case88] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic88])),
				[CubicTile.Case8A] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic8A])),
				[CubicTile.Case8D] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic8D])),
				[CubicTile.Case8E] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic8E])),
				[CubicTile.Case8F] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic8F])),
				[CubicTile.Case90] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic90])),
				[CubicTile.Case91] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic91])),
				[CubicTile.Case99] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubic99])),
				[CubicTile.CaseA0] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicA0])),
				[CubicTile.CaseA8] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicA8])),
				[CubicTile.CaseAA] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicAA])),
				[CubicTile.CaseAF] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicAF])),
				[CubicTile.CaseC3] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicC3])),
				[CubicTile.CaseCA] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicCA])),
				[CubicTile.CaseCE] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicCE])),
				[CubicTile.CaseD8] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicD8])),
				[CubicTile.CaseE0] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicE0])),
				[CubicTile.CaseE8] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicE8])),
				[CubicTile.CaseEC] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicEC])),
				[CubicTile.CaseEE] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicEE])),
				[CubicTile.CaseEF] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicEF])),
				[CubicTile.CaseF0] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicF0])),
				[CubicTile.CaseF8] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicF8])),
				[CubicTile.CaseFA] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicFA])),
				[CubicTile.CaseFE] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicFE])),
				[CubicTile.CaseFF] = TileUpdater.SetTo(shape: builder.Shapes.Add(shapes[TileShapes.AutoCubicFF])),
			};

			builder.Updater = new CubicTileUpdater((atlas, tile) => atlas.Types[tile.Type].IsFilled, cases);
			return builder.Build();
		}

		private static ITileTypeOLD CreateAutoAxes(
			TileTypeBuilder builder, IDictionaryView<Guid, ITileShapeView> shapes, ITileColorPalette palette)
		{
			Require.That(builder != null);
			Require.That(shapes != null);
			Require.That(palette != null);

			builder.Key = TileTypes.AutoAxes;
			builder.Name = nameof(TileTypes.AutoAxes);
			builder.IsFilled = false;
			builder.ColorPalettes.Add(palette);

			builder.Updater = new AxesTileUpdater(
				(atlas, tile) => tile.Type == TileTypes.AutoAxes,
				CreateAxisPart(builder, shapes[TileShapes.Axis00_00_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_00_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_00_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_00_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_01_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_01_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_01_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_01_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_10_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_10_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_10_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_10_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_11_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_11_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_11_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis00_11_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_00_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_00_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_00_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_00_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_01_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_01_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_01_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_01_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_10_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_10_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_10_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_10_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_11_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_11_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_11_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis01_11_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_00_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_00_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_00_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_00_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_01_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_01_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_01_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_01_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_10_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_10_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_10_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_10_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_11_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_11_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_11_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis10_11_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_00_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_00_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_00_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_00_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_01_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_01_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_01_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_01_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_10_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_10_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_10_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_10_11]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_11_00]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_11_01]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_11_10]),
				CreateAxisPart(builder, shapes[TileShapes.Axis11_11_11]));
			return builder.Build();
		}

		private static KeyValuePair<Axes, ITileUpdater> CreateAxisPart(TileTypeBuilder builder, ITileShapeView shape)
		{
			Require.That(builder != null);
			Require.That(shape != null);

			return new KeyValuePair<Axes, ITileUpdater>(
				NameToAxes(shape.Name), TileUpdater.SetTo(shape: builder.Shapes.Add(shape)));
		}

		private static Axes NameToAxes(string name)
		{
			Require.That(!name.IsNullOrWhiteSpace());

			var key = name.Substring(5, 8);
			var axes = Axes.None;

			if (key.Substring(0, 1) == "1")
			{
				axes |= Axes.NegativeX;
			}

			if (key.Substring(1, 1) == "1")
			{
				axes |= Axes.PositiveX;
			}

			if (key.Substring(3, 1) == "1")
			{
				// for some reason Y axis has to be reversed
				axes |= Axes.PositiveY;
			}

			if (key.Substring(4, 1) == "1")
			{
				// for some reason Y axis has to be reversed
				axes |= Axes.NegativeY;
			}

			if (key.Substring(6, 1) == "1")
			{
				axes |= Axes.NegativeZ;
			}

			if (key.Substring(7, 1) == "1")
			{
				axes |= Axes.PositiveZ;
			}

			return axes;
		}
	}
}
