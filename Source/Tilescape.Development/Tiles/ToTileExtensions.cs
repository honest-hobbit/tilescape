﻿using System.Collections.Generic;
using System.Linq;
using Tilescape.Engine;

namespace Tilescape.Development.Tiles
{
	public static class ToTileExtensions
	{
		public static Tile ToTile(this IEnumerable<ITileTypeOLD> tiles) => new Tile(tiles.Single().Key);

		public static Tile ToTile(this IEnumerable<ITileTypeOLD> tiles, OrthoAxis orientation) =>
			new Tile(tiles.Single().Key, orientation);

		public static Tile ToTile(this ITileTypeOLD tile, OrthoAxis orientation) => new Tile(tile.Key, orientation);
	}
}
