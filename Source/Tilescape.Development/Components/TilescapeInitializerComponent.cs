﻿using Tilescape.Development.Tiles;
using Tilescape.Engine;
using Tilescape.Engine.Components;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Development.Components
{
	[RequireComponent(typeof(TilescapeStoreComponent))]
	[RequireComponent(typeof(TileStageManagerComponent))]
	public class TilescapeInitializerComponent : MonoBehaviour
	{
		[SerializeField]
		private TileStageModifierComponent modifier = null;

		private void Awake()
		{
			var storeComponent = this.GetComponent<TilescapeStoreComponent>();
			var tileStageManager = this.GetComponent<TileStageManagerComponent>();

			var atlas = TilescapeAtlas.Create(storeComponent, TileTypesFactory.CreateTypes);

			if (this.modifier != null)
			{
				this.modifier.EmptyTileType = atlas.Types[TileTypes.Empty];
			}

			tileStageManager.Initialize(atlas, this.CreatePopulator(atlas.Types));
			tileStageManager.CreateStage("Test Stage");
		}

		private ITileChunkPopulator CreatePopulator(IDictionaryView<TileTypeKey, ITileTypeOLD> atlas)
		{
			Require.That(atlas != null);

			var rotation = OrthoAxis.PosY000Norm_24;
			var emptyTile = atlas[TileTypes.Empty].ToTile(rotation);
			var grassTile = atlas[TileTypes.Grass].ToTile(rotation);
			var random = new[]
			{
				KeyValuePair.New(7.0, atlas[TileTypes.GrassFlower1].ToTile(rotation)),
				KeyValuePair.New(10.0, atlas[TileTypes.GrassFlower2].ToTile(rotation)),
				KeyValuePair.New(3.0, atlas[TileTypes.GrassRock1].ToTile(rotation)),
				KeyValuePair.New(1.0, atlas[TileTypes.GrassRock2].ToTile(rotation)),
			};

			return new HillTileChunkPopulator(0, emptyTile, grassTile, random);
			////return new FlatTileChunkPopulator(grassTile, emptyTile, 8);
		}
	}
}
