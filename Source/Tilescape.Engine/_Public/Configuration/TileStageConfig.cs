﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	public class TileStageConfig : TileConfig, ITileStageConfig
	{
		public TileStageConfig(int tileSizeExponent, int chunkSizeExponent)
			: base(tileSizeExponent)
		{
			// TODO what should maximum chunk size be?
			Require.That(chunkSizeExponent >= 0);

			this.ChunkSizeExponent = chunkSizeExponent;

			this.ChunkLengthInTiles = MathUtility.PowerOf2(chunkSizeExponent);
			this.ChunkLengthInVoxels = this.ChunkLengthInTiles * this.TileLengthInVoxels;

			this.ChunkMaxTileIndex = this.ChunkLengthInTiles - 1;
			this.TileIndexLocalMask = BitMask.CreateIntWithOnesInLowerBits(chunkSizeExponent);

			this.ChunkLength = this.ChunkLengthInTiles * this.TileLength;
		}

		/// <inheritdoc />
		public int ChunkSizeExponent { get; }

		/// <inheritdoc />
		public int ChunkLengthInTiles { get; }

		/// <inheritdoc />
		public int ChunkLengthInVoxels { get; }

		/// <inheritdoc />
		public int ChunkMinTileIndex => 0;

		/// <inheritdoc />
		public int ChunkMaxTileIndex { get; }

		/// <inheritdoc />
		public int TileIndexLocalMask { get; }

		/// <inheritdoc />
		public float ChunkLength { get; }
	}
}
