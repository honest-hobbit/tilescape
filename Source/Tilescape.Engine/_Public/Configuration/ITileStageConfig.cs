﻿namespace Tilescape.Engine
{
	public interface ITileStageConfig : ITileConfig
	{
		int ChunkSizeExponent { get; }

		int ChunkLengthInTiles { get; }

		int ChunkLengthInVoxels { get; }

		int ChunkMinTileIndex { get; }

		int ChunkMaxTileIndex { get; }

		int TileIndexLocalMask { get; }

		float ChunkLength { get; }
	}
}
