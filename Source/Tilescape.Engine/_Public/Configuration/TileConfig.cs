﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	public class TileConfig : ITileConfig
	{
		public TileConfig(int tileSizeExponent)
		{
			Require.That(tileSizeExponent >= 0);
			Require.That(tileSizeExponent <= 7);

			// Maximum TileLengthInVoxels is 128 (2^7). This is requirred because MeshQuadSlim encodes each length as 1 byte
			// and needs TileLengthInVoxels + 1 (max of 129) possible values to encode every possible length.
			// TileLengthInVoxels of 256 (2^8) would overflow a byte when doing TileLengthInVoxels + 1 and break the system.
			this.TileSizeExponent = tileSizeExponent;

			this.TileLengthInVoxels = MathUtility.PowerOf2(tileSizeExponent);
			this.TileMaxVoxelIndex = this.TileLengthInVoxels - 1;
			this.VoxelIndexLocalMask = BitMask.CreateIntWithOnesInLowerBits(tileSizeExponent);

			this.VoxelLength = this.TileLength / this.TileLengthInVoxels;
			this.VoxelHalfLength = this.VoxelLength / 2f;
		}

		/// <inheritdoc />
		public int TileSizeExponent { get; }

		/// <inheritdoc />
		public int TileLengthInVoxels { get; }

		/// <inheritdoc />
		public int TileMinVoxelIndex => 0;

		/// <inheritdoc />
		public int TileMaxVoxelIndex { get; }

		/// <inheritdoc />
		public int VoxelIndexLocalMask { get; }

		/// <inheritdoc />
		public float VoxelLength { get; }

		/// <inheritdoc />
		public float VoxelHalfLength { get; }

		/// <inheritdoc />
		public float TileLength => 1f;

		/// <inheritdoc />
		public float TileHalfLength => .5f;
	}
}
