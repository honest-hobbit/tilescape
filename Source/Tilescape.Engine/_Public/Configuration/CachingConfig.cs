﻿using System.Runtime.InteropServices;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	public class CachingConfig
	{
		private int maxPooledChunks;

		private int maxCachedChunks;

		private double expiryCapacityMultiplier;

		public int MaxPooledChunks
		{
			get => this.maxPooledChunks;
			set
			{
				Require.That(value >= 1);
				this.maxPooledChunks = value;
			}
		}

		public int MaxCachedChunks
		{
			get => this.maxCachedChunks;
			set
			{
				Require.That(value >= 1);
				this.maxCachedChunks = value;
			}
		}

		public double ExpiryCapacityMultiplier
		{
			get => this.expiryCapacityMultiplier;
			set
			{
				Require.That(value >= 1);
				this.expiryCapacityMultiplier = value;
			}
		}

		public void SetMaxPooledChunksInBytes(ITileStageConfig config, long bytes)
		{
			Require.That(config != null);
			Require.That(bytes >= 1);

			long chunkSize = BinaryArray.GetCubeLength(config.ChunkSizeExponent) * Marshal.SizeOf(typeof(Tile));
			this.MaxPooledChunks = MathUtility.DivideRoundUp(bytes, chunkSize).ClampToInt().ClampLower(1);
		}

		public void SetMaxCachedChunksFromMultiplier(double multiplier)
		{
			Require.That(multiplier >= 1);

			this.MaxCachedChunks = (this.MaxPooledChunks * multiplier).ClampToInt().ClampLower(1);
		}
	}
}
