﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public class ResyncUnityConfig
	{
		private int frames = 1;

		private int initialMeshBuilders = 0;

		private int chunksPerFrame = 1;

		public int Frames
		{
			get => this.frames;
			set
			{
				Require.That(value >= 1);
				this.frames = value;
			}
		}

		public int InitialMeshBuilders
		{
			get => this.initialMeshBuilders;
			set
			{
				Require.That(value >= 0);
				this.initialMeshBuilders = value;
			}
		}

		public int ChunksPerFrame
		{
			get => this.chunksPerFrame;
			set
			{
				Require.That(value >= 1);
				this.chunksPerFrame = value;
			}
		}
	}
}
