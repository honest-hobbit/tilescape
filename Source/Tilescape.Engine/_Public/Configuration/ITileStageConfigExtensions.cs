﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using UnityEngine;

namespace Tilescape.Engine
{
	public static class ITileStageConfigExtensions
	{
		public static ITileStageConfig Copy(this ITileStageConfig config)
		{
			Require.That(config != null);

			return new TileStageConfig(config.TileSizeExponent, config.ChunkSizeExponent);
		}

		public static Vector3 GetChunkSizeVector(this ITileStageConfig config)
		{
			Require.That(config != null);

			var length = config.ChunkLength;
			return new Vector3(length, length, length);
		}

		public static Index3D ConvertVoxelToTile(this ITileStageConfig config, Index3D tileVoxelIndex)
		{
			Require.That(config != null);

			return tileVoxelIndex >> config.TileSizeExponent;
		}

		public static Index3D ConvertVoxelToChunk(this ITileStageConfig config, Index3D tileVoxelIndex)
		{
			Require.That(config != null);

			return tileVoxelIndex >> (config.TileSizeExponent + config.ChunkSizeExponent);
		}

		public static Index3D ConvertTileToChunk(this ITileStageConfig config, Index3D tileIndex)
		{
			Require.That(config != null);

			return tileIndex >> config.ChunkSizeExponent;
		}

		// converts a tile index that's local to a chunk to a stage global tile index
		public static Index3D ConvertLocalTileToStage(this ITileStageConfig config, Index3D chunkIndex, Index3D tileIndex)
		{
			Require.That(config != null);

			return (chunkIndex * config.ChunkLengthInTiles) + tileIndex;
		}

		// converts a stage global tile voxel index to the index of that voxel local to that single tile containing the voxel
		public static Index3D ConvertStageVoxelToLocal(this ITileStageConfig config, Index3D stageTileVoxelIndex)
		{
			Require.That(config != null);

			return stageTileVoxelIndex & config.VoxelIndexLocalMask;
		}

		// converts a stage global tile index to the index of that tile local to that single chunk containing the tile
		public static Index3D ConvertStageTileToLocal(this ITileStageConfig config, Index3D stageTileIndex)
		{
			Require.That(config != null);

			return stageTileIndex & config.TileIndexLocalMask;
		}
	}
}
