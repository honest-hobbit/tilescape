﻿using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Engine
{
	public static class ITileConfigExtensions
	{
		public static Vector3 GetVoxelHalfSizeVector(this ITileConfig config)
		{
			Require.That(config != null);

			var length = config.VoxelHalfLength;
			return new Vector3(length, length, length);
		}

		public static Vector3 GetVoxelSizeVector(this ITileConfig config)
		{
			Require.That(config != null);

			var length = config.VoxelLength;
			return new Vector3(length, length, length);
		}

		public static Vector3 GetTileHalfSizeVector(this ITileConfig config)
		{
			Require.That(config != null);

			var length = config.TileHalfLength;
			return new Vector3(length, length, length);
		}

		public static Vector3 GetTileSizeVector(this ITileConfig config)
		{
			Require.That(config != null);

			var length = config.TileLength;
			return new Vector3(length, length, length);
		}
	}
}
