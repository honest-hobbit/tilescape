﻿namespace Tilescape.Engine
{
	public interface ITileConfig
	{
		int TileSizeExponent { get; }

		int TileLengthInVoxels { get; }

		int TileMinVoxelIndex { get; }

		int TileMaxVoxelIndex { get; }

		int VoxelIndexLocalMask { get; }

		float VoxelLength { get; }

		float VoxelHalfLength { get; }

		float TileLength { get; }

		float TileHalfLength { get; }
	}
}
