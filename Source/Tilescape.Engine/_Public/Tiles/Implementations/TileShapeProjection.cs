﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Unity.Types;
using UnityEngine;

namespace Tilescape.Engine
{
	public class TileShapeProjection : ITileShapeProjection
	{
		private readonly CubeArrayProjection<ColorVoxel> voxels = new CubeArrayProjection<ColorVoxel>();

		private readonly OrthogonalTransformer transformer = new OrthogonalTransformer();

		private readonly TileColliders colliders;

		private readonly TileMeshes meshes;

		private TileShapeData sourceData;

		private ITileFaces<ITileFaceMask> faceMasks;

		public TileShapeProjection(ITileConfig config)
			: this(config, CreateLengthsList(config))
		{
		}

		public TileShapeProjection(ITileConfig config, IListView<float> lengths)
		{
			Require.That(config != null);
			Require.That(lengths != null);
			Require.That(lengths.Count == config.TileLengthInVoxels + 1);

			this.SizeExponent = config.TileSizeExponent;
			this.colliders = new TileColliders(this.transformer);
			this.meshes = new TileMeshes(this.transformer, lengths);
		}

		/// <inheritdoc />
		public int SizeExponent { get; }

		/// <inheritdoc />
		public bool IsAssigned => this.sourceData.IsAssigned;

		public ITileShapeView SourceShape => this.sourceData.Shape;

		public IListView<Color32> Colors => this.sourceData.Colors;

		public OrthoAxis Rotation => this.sourceData.Rotation;

		public ICubeArrayView<ColorVoxel> Voxels
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.voxels;
			}
		}

		/// <inheritdoc />
		public ITileColliders Colliders
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.colliders;
			}
		}

		/// <inheritdoc />
		public ITileParts<IListView<MeshQuad>> Meshes
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.meshes;
			}
		}

		/// <inheritdoc />
		public ITileFaces<ITileFaceMask> FaceMasks
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.faceMasks;
			}
		}

		public static IListView<float> CreateLengthsList(ITileConfig config)
		{
			Require.That(config != null);

			return Factory.CreateArray(
				config.TileLengthInVoxels + 1,
				index => (index * config.VoxelLength) - config.TileHalfLength).AsListView();
		}

		/// <inheritdoc />
		public override string ToString() => this.sourceData.ToString();

		public void SetProjection(TileShapeData source)
		{
			Require.That(source.IsAssigned);
			Require.That(source.Shape.Voxels.SizeExponent == this.SizeExponent);

			this.sourceData = source;
			this.voxels.SetProjection(source.Shape.Voxels, source.Rotation);

			// need to set transformer orientation before setting projection of colliders or meshes
			this.transformer.Orientation = source.Rotation;
			this.colliders.SetProjection(source.Shape.Colliders);
			this.meshes.SetProjection(source.Shape.Meshes, source.Colors);

			this.faceMasks = source.Shape.FaceMasks[source.Rotation];
		}

		public void Clear()
		{
			this.sourceData = TileShapeData.Unassigned;
			this.voxels.Clear();
			this.colliders.Clear();
			this.meshes.Clear();
			this.faceMasks = null;
		}

		#region Private Classes

		private class TileColliders : ITileColliders
		{
			private readonly OrthogonalTransformer transformer;

			private ITileColliders colliders;

			public TileColliders(OrthogonalTransformer transformer)
			{
				Require.That(transformer != null);

				this.transformer = transformer;
			}

			/// <inheritdoc />
			public bool IsFullCube
			{
				get
				{
					Require.That(this.colliders != null);
					return this.colliders.IsFullCube;
				}
			}

			/// <inheritdoc />
			public int Count
			{
				get
				{
					Require.That(this.colliders != null);
					return this.colliders.Count;
				}
			}

			/// <inheritdoc />
			public BoxColliderStruct this[int index]
			{
				get
				{
					IListViewContracts.Indexer(this, index);
					Require.That(this.colliders != null);
					return this.RotateBox(this.colliders[index]);
				}
			}

			/// <inheritdoc />
			public IEnumerator<BoxColliderStruct> GetEnumerator()
			{
				Require.That(this.colliders != null);

				foreach (var box in this.colliders)
				{
					yield return this.RotateBox(box);
				}
			}

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

			public void SetProjection(ITileColliders colliders)
			{
				Require.That(colliders != null);

				this.colliders = colliders;
			}

			public void Clear() => this.colliders = null;

			private BoxColliderStruct RotateBox(BoxColliderStruct box) =>
				new BoxColliderStruct(this.transformer.Transform(box.Center), this.transformer.Transform(box.Size).Abs());
		}

		private class TileMeshes : ITileParts<IListView<MeshQuad>>
		{
			private readonly TileFacesProjection<IListView<MeshQuad>> faces = new TileFacesProjection<IListView<MeshQuad>>();

			private readonly OrthogonalTransformer transformer;

			private readonly QuadProjections projections;

			public TileMeshes(OrthogonalTransformer transformer, IListView<float> lengths)
			{
				Require.That(transformer != null);
				Require.That(lengths != null);

				this.transformer = transformer;
				this.projections = new QuadProjections(lengths);
				this.faces.Source = this.projections;
			}

			/// <inheritdoc />
			public IListView<MeshQuad> Center => this.projections.Center;

			/// <inheritdoc />
			public IListView<MeshQuad> NegativeX => this.faces.NegativeX;

			/// <inheritdoc />
			public IListView<MeshQuad> PositiveX => this.faces.PositiveX;

			/// <inheritdoc />
			public IListView<MeshQuad> NegativeY => this.faces.NegativeY;

			/// <inheritdoc />
			public IListView<MeshQuad> PositiveY => this.faces.PositiveY;

			/// <inheritdoc />
			public IListView<MeshQuad> NegativeZ => this.faces.NegativeZ;

			/// <inheritdoc />
			public IListView<MeshQuad> PositiveZ => this.faces.PositiveZ;

			public void SetProjection(ITileParts<IListView<MeshQuadSlim>> meshes, IListView<Color32> colors)
			{
				Require.That(meshes != null);
				Require.That(colors != null);

				this.faces.Orientation = this.transformer.Orientation;
				this.projections.Center.SetTo(meshes.Center, this.transformer, colors);
				this.projections.NegativeX.SetTo(meshes.NegativeX, this.transformer, colors);
				this.projections.PositiveX.SetTo(meshes.PositiveX, this.transformer, colors);
				this.projections.NegativeY.SetTo(meshes.NegativeY, this.transformer, colors);
				this.projections.PositiveY.SetTo(meshes.PositiveY, this.transformer, colors);
				this.projections.NegativeZ.SetTo(meshes.NegativeZ, this.transformer, colors);
				this.projections.PositiveZ.SetTo(meshes.PositiveZ, this.transformer, colors);
			}

			public void Clear()
			{
				this.projections.Center.Clear();
				this.projections.NegativeX.Clear();
				this.projections.PositiveX.Clear();
				this.projections.NegativeY.Clear();
				this.projections.PositiveY.Clear();
				this.projections.NegativeZ.Clear();
				this.projections.PositiveZ.Clear();
			}

			internal class QuadProjections : ITileParts<IListView<MeshQuad>>
			{
				public QuadProjections(IListView<float> lengths)
				{
					Require.That(lengths != null);

					this.Center = new QuadListConverter(lengths);
					this.NegativeX = new QuadListConverter(lengths);
					this.PositiveX = new QuadListConverter(lengths);
					this.NegativeY = new QuadListConverter(lengths);
					this.PositiveY = new QuadListConverter(lengths);
					this.NegativeZ = new QuadListConverter(lengths);
					this.PositiveZ = new QuadListConverter(lengths);
				}

				public QuadListConverter Center { get; }

				public QuadListConverter NegativeX { get; }

				public QuadListConverter PositiveX { get; }

				public QuadListConverter NegativeY { get; }

				public QuadListConverter PositiveY { get; }

				public QuadListConverter NegativeZ { get; }

				public QuadListConverter PositiveZ { get; }

				/// <inheritdoc />
				IListView<MeshQuad> ITileParts<IListView<MeshQuad>>.Center => this.Center;

				/// <inheritdoc />
				IListView<MeshQuad> ITileFaces<IListView<MeshQuad>>.NegativeX => this.NegativeX;

				/// <inheritdoc />
				IListView<MeshQuad> ITileFaces<IListView<MeshQuad>>.PositiveX => this.PositiveX;

				/// <inheritdoc />
				IListView<MeshQuad> ITileFaces<IListView<MeshQuad>>.NegativeY => this.NegativeY;

				/// <inheritdoc />
				IListView<MeshQuad> ITileFaces<IListView<MeshQuad>>.PositiveY => this.PositiveY;

				/// <inheritdoc />
				IListView<MeshQuad> ITileFaces<IListView<MeshQuad>>.NegativeZ => this.NegativeZ;

				/// <inheritdoc />
				IListView<MeshQuad> ITileFaces<IListView<MeshQuad>>.PositiveZ => this.PositiveZ;
			}
		}

		#endregion
	}
}
