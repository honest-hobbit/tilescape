﻿using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public class TileType : ITileTypeOLD
	{
		public TileType(
			TileTypeKey key,
			string name,
			int ordering,
			bool isFilled,
			IEnumerable<ITileShapeView> shapes,
			IEnumerable<ITileColorPalette> colorPalettes,
			ITileUpdater updater)
		{
			Require.That(key != TileTypeKey.Unassigned);
			Require.That(!name.IsNullOrWhiteSpace());
			Require.That(shapes.AllAndSelfNotNull());
			Require.That(shapes.CountExtended().IsIn(1, 256));
			Require.That(colorPalettes != null);
			Require.That(colorPalettes.All(x => x.IsValid()));
			Require.That(colorPalettes.CountExtended().IsIn(1, 256));

			this.Key = key;
			this.Name = name;
			this.Ordering = ordering;
			this.IsFilled = isFilled;
			this.Updater = updater;
			this.Shapes = shapes.ToArray().AsListView();
			this.ColorPalettes = colorPalettes.ToArray().AsListView();
		}

		/// <inheritdoc />
		public TileTypeKey Key { get; }

		/// <inheritdoc />
		public string Name { get; }

		/// <inheritdoc />
		public int Ordering { get; }

		/// <inheritdoc />
		public bool IsFilled { get; }

		/// <inheritdoc />
		public IListView<ITileShapeView> Shapes { get; }

		/// <inheritdoc />
		public IListView<ITileColorPalette> ColorPalettes { get; }

		/// <inheritdoc />
		public ITileUpdater Updater { get; }
	}
}
