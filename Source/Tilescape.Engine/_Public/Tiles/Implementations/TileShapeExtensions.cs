﻿using System.Collections.Generic;
using System.Linq;

namespace Tilescape.Engine
{
	public static class TileShapeExtensions
	{
		public static ITileShapeView AsView(this TileShape shape) => shape;

		public static IEnumerable<ITileShapeView> AsViews(this IEnumerable<TileShape> shapes) => shapes.Select(x => (ITileShapeView)x);
	}
}
