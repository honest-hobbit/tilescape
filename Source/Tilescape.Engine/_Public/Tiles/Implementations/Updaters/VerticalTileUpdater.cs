﻿using System;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public class VerticalTileUpdater : ITileUpdater
	{
		private readonly ITileUpdater topUpdater;

		private readonly ITileUpdater belowUpdater;

		private readonly Func<ITilescapeAtlas, Tile, bool> checkAdjacentTile;

		public VerticalTileUpdater(
			ITileUpdater topUpdater,
			ITileUpdater belowUpdater,
			Func<ITilescapeAtlas, Tile, bool> checkAdjacentTile)
		{
			Require.That(topUpdater != null);
			Require.That(belowUpdater != null);
			Require.That(checkAdjacentTile != null);

			this.topUpdater = topUpdater;
			this.belowUpdater = belowUpdater;
			this.checkAdjacentTile = checkAdjacentTile;
		}

		/// <inheritdoc />
		public bool TryUpdateTile(ITileUpdaterView view, out Tile updatedTile)
		{
			ITileUpdaterContracts.TryUpdateTile(view);

			var selector = this.checkAdjacentTile(view.Atlas, view.Tiles[Adjacency.PosY]) ? this.belowUpdater : this.topUpdater;
			return selector.TryUpdateTile(view, out updatedTile);
		}
	}
}
