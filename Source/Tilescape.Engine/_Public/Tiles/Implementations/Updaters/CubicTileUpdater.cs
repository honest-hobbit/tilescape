﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;

namespace Tilescape.Engine
{
	public class CubicTileUpdater : ITileUpdater
	{
		private static readonly Case[] CasesArray = new Case[]
		{
			// 0
			new Case(CubicTile.Case00, OrthoAxis.PosY000Norm_24),	// all
			new Case(CubicTile.Case08, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.Case08, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.Case0A, OrthoAxis.PosY270Norm_30),	// PosY270Flip
			new Case(CubicTile.Case08, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.Case0A, OrthoAxis.PosY000Norm_24),	// PosY0Flip
			new Case(CubicTile.Case09, OrthoAxis.PosY000Norm_24),	// PosY90Flip, PosY180, PosY270Flip
			new Case(CubicTile.Case0E, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.Case08, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.Case09, OrthoAxis.PosY090Norm_26),	// PosY0Flip, PosY180Flip, PosY270
			new Case(CubicTile.Case0A, OrthoAxis.PosY180Norm_28),	// PosY180Flip
			new Case(CubicTile.Case0E, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.Case0A, OrthoAxis.PosY090Norm_26),	// PosY90Flip
			new Case(CubicTile.Case0E, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.Case0E, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.Case0F, OrthoAxis.PosY000Norm_24),	// all

			// 16
			new Case(CubicTile.Case80, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.Case88, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.Case82, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.Case8A, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.Case82, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.Case8A, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case8E, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.Case18, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.Case19, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.Case85, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.Case8D, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.Case85, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.Case8D, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case8F, OrthoAxis.PosY270Norm_30),	// PosY0Flip

			// 32
			new Case(CubicTile.Case80, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.Case82, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.Case88, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.Case8A, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.Case18, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.Case85, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.Case19, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.Case8D, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.Case82, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case8A, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.Case8E, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.Case85, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case8D, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.Case8F, OrthoAxis.PosY180Norm_28),	// PosY270Flip

			// 48
			new Case(CubicTile.CaseA0, OrthoAxis.PosY270Norm_30),	// PosY270Flip
			new Case(CubicTile.CaseA8, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.CaseA8, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.CaseAA, OrthoAxis.PosY270Norm_30),	// PosY270Flip
			new Case(CubicTile.Case58, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.CaseCA, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseCE, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.Case58, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseCA, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.CaseCE, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.CaseC3, OrthoAxis.PosY180Norm_28),	// PosY0Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseAF, OrthoAxis.PosY270Norm_30),	// PosY270Flip

			// 64
			new Case(CubicTile.Case80, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.Case82, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.Case18, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.Case85, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.Case88, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.Case8A, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.Case19, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.Case8D, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.Case82, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case85, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case8A, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.Case8E, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.Case8D, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.Case8F, OrthoAxis.PosY000Norm_24),	// PosY90Flip

			// 80
			new Case(CubicTile.CaseA0, OrthoAxis.PosY000Norm_24),	// PosY0Flip
			new Case(CubicTile.CaseA8, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.Case58, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.CaseCA, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.CaseA8, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.CaseAA, OrthoAxis.PosY000Norm_24),	// PosY0Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseCE, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.Case58, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseC3, OrthoAxis.PosY270Norm_30),	// PosY90Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseCA, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.CaseCE, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseAF, OrthoAxis.PosY000Norm_24),	// PosY0Flip

			// 96
			new Case(CubicTile.Case90, OrthoAxis.PosY000Norm_24),	// PosY90Flip, PosY180, PosY270Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case91, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case91, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case99, OrthoAxis.PosY000Norm_24),	// PosY90Flip, PosY180, PosY270Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid

			// 112
			new Case(CubicTile.CaseE0, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.CaseE8, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.CaseD8, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.CaseEC, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.CaseD8, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.CaseEC, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEE, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEF, OrthoAxis.PosY270Norm_30),	// PosY0Flip

			// 128
			new Case(CubicTile.Case80, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.Case18, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.Case82, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.Case85, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.Case82, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.Case85, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case88, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.Case19, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.Case8A, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.Case8D, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.Case8A, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.Case8D, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.Case8E, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.Case8F, OrthoAxis.PosY090Norm_26),	// PosY180Flip

			// 144
			new Case(CubicTile.Case90, OrthoAxis.PosY090Norm_26),	// PosY0Flip, PosY180Flip, PosY270
			new Case(CubicTile.Case91, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.Case91, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.Case99, OrthoAxis.PosY090Norm_26),	// PosY0Flip, PosY180Flip, PosY270
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid

			// 160
			new Case(CubicTile.CaseA0, OrthoAxis.PosY180Norm_28),	// PosY180Flip
			new Case(CubicTile.Case58, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.CaseA8, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.CaseCA, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.Case58, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.CaseC3, OrthoAxis.PosY090Norm_26),	// PosY270Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseA8, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseAA, OrthoAxis.PosY180Norm_28),	// PosY180Flip
			new Case(CubicTile.CaseCE, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.CaseCA, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseCE, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.CaseAF, OrthoAxis.PosY180Norm_28),	// PosY180Flip

			// 176
			new Case(CubicTile.CaseE0, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.CaseD8, OrthoAxis.PosY180Flip_29),
			new Case(CubicTile.CaseE8, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.CaseEC, OrthoAxis.PosY180Norm_28),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseD8, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEC, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.CaseEE, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEF, OrthoAxis.PosY180Norm_28),	// PosY270Flip

			// 192
			new Case(CubicTile.CaseA0, OrthoAxis.PosY090Norm_26),	// PosY90Flip
			new Case(CubicTile.Case58, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.Case58, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.CaseC3, OrthoAxis.PosY000Norm_24),	// PosY180Flip
			new Case(CubicTile.CaseA8, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.CaseCA, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseA8, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseCA, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseAA, OrthoAxis.PosY090Norm_26),	// PosY90Flip
			new Case(CubicTile.CaseCE, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.CaseCE, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.CaseAF, OrthoAxis.PosY090Norm_26),	// PosY90Flip

			// 208
			new Case(CubicTile.CaseE0, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.CaseD8, OrthoAxis.PosY270Norm_30),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseE8, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.CaseEC, OrthoAxis.PosY090Flip_27),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseD8, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEC, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.CaseEE, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEF, OrthoAxis.PosY000Norm_24),	// PosY90Flip

			// 224
			new Case(CubicTile.CaseE0, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseD8, OrthoAxis.PosY270Flip_31),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseD8, OrthoAxis.PosY000Norm_24),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseE8, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEC, OrthoAxis.PosY090Norm_26),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEC, OrthoAxis.PosY000Flip_25),
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseEE, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.CaseEF, OrthoAxis.PosY090Norm_26),	// PosY180Flip

			// 240
			new Case(CubicTile.CaseF0, OrthoAxis.PosY000Norm_24),	// all
			new Case(CubicTile.CaseF8, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.CaseF8, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.CaseFA, OrthoAxis.PosY270Norm_30),	// PosY270Flip
			new Case(CubicTile.CaseF8, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.CaseFA, OrthoAxis.PosY000Norm_24),	// PosY0Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFE, OrthoAxis.PosY270Norm_30),	// PosY0Flip
			new Case(CubicTile.CaseF8, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// invalid
			new Case(CubicTile.CaseFA, OrthoAxis.PosY180Norm_28),	// PosY180Flip
			new Case(CubicTile.CaseFE, OrthoAxis.PosY180Norm_28),	// PosY270Flip
			new Case(CubicTile.CaseFA, OrthoAxis.PosY090Norm_26),	// PosY90Flip
			new Case(CubicTile.CaseFE, OrthoAxis.PosY000Norm_24),	// PosY90Flip
			new Case(CubicTile.CaseFE, OrthoAxis.PosY090Norm_26),	// PosY180Flip
			new Case(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24),	// all
		};

		private readonly ITileUpdater[] shapeCases;

		private readonly Func<ITilescapeAtlas, Tile, bool> checkTile;

		public CubicTileUpdater(Func<ITilescapeAtlas, Tile, bool> checkTile, params KeyValuePair<CubicTile, ITileUpdater>[] cases)
			: this(checkTile, (IEnumerable<KeyValuePair<CubicTile, ITileUpdater>>)cases)
		{
		}

		public CubicTileUpdater(Func<ITilescapeAtlas, Tile, bool> checkTile, IEnumerable<KeyValuePair<CubicTile, ITileUpdater>> cases)
		{
			Require.That(checkTile != null);
			Require.That(cases != null);
			Require.That(cases.All(x => Enumeration.IsDefined(x.Key) && x.Value != null));

			this.checkTile = checkTile;
			this.shapeCases = Enumeration.CreateIndexableArray<CubicTile, ITileUpdater>(TileUpdater.None);

			foreach (var pair in cases)
			{
				this.shapeCases[(int)pair.Key] = pair.Value;
			}
		}

		/// <inheritdoc />
		public bool TryUpdateTile(ITileUpdaterView view, out Tile updatedTile)
		{
			ITileUpdaterContracts.TryUpdateTile(view);

			var cubicBits = CubicTileBits.Create(this.checkTile, view);
			var currentCase = CasesArray[cubicBits.Index];

			if (this.shapeCases[currentCase.Index].TryUpdateTile(view, out updatedTile))
			{
				// overwrite the rotation of the updated tile
				updatedTile = updatedTile.SetOrientation(currentCase.Orientation);
				return true;
			}
			else
			{
				return false;
			}
		}

		private struct Case
		{
			public Case(CubicTile index, OrthoAxis orientation)
			{
				this.Index = (byte)index;
				this.Orientation = orientation;
			}

			public readonly byte Index;

			public readonly OrthoAxis Orientation;
		}
	}
}
