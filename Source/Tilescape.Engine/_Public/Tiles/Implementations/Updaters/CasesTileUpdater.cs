﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public class CasesTileUpdater : ITileUpdater
	{
		private readonly ITileUpdater[] cases;

		public CasesTileUpdater(params ITileUpdater[] cases)
			: this((IEnumerable<ITileUpdater>)cases)
		{
		}

		public CasesTileUpdater(IEnumerable<ITileUpdater> cases)
		{
			Require.That(cases.AllAndSelfNotNull());

			this.cases = cases.ToArrayExtended();
		}

		/// <inheritdoc />
		public bool TryUpdateTile(ITileUpdaterView view, out Tile updatedTile)
		{
			ITileUpdaterContracts.TryUpdateTile(view);

			for (int index = 0; index < this.cases.Length; index++)
			{
				if (this.cases[index].TryUpdateTile(view, out updatedTile))
				{
					return true;
				}
			}

			updatedTile = Tile.Unassigned;
			return false;
		}
	}
}
