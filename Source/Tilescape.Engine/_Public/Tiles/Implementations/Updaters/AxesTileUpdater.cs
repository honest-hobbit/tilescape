﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Unity.Types;

namespace Tilescape.Engine
{
	public class AxesTileUpdater : ITileUpdater
	{
		private readonly ITileUpdater[] cases;

		private readonly Func<ITilescapeAtlas, Tile, bool> checkTile;

		public AxesTileUpdater(Func<ITilescapeAtlas, Tile, bool> checkTile, params KeyValuePair<Axes, ITileUpdater>[] cases)
			: this(checkTile, (IEnumerable<KeyValuePair<Axes, ITileUpdater>>)cases)
		{
		}

		public AxesTileUpdater(Func<ITilescapeAtlas, Tile, bool> checkTile, IEnumerable<KeyValuePair<Axes, ITileUpdater>> cases)
		{
			Require.That(checkTile != null);
			Require.That(cases != null);

			this.checkTile = checkTile;
			this.cases = Factory.CreateArray(64, TileUpdater.None);

			foreach (var pair in cases)
			{
				this.cases[(int)pair.Key] = pair.Value;
			}
		}

		/// <inheritdoc />
		public bool TryUpdateTile(ITileUpdaterView view, out Tile updatedTile)
		{
			ITileUpdaterContracts.TryUpdateTile(view);

			var axes = Axes.None;
			if (this.checkTile(view.Atlas, view.Tiles[Adjacency.NegX]))
			{
				axes |= Axes.PositiveX;
			}

			if (this.checkTile(view.Atlas, view.Tiles[Adjacency.PosX]))
			{
				axes |= Axes.NegativeX;
			}

			if (this.checkTile(view.Atlas, view.Tiles[Adjacency.NegY]))
			{
				axes |= Axes.PositiveY;
			}

			if (this.checkTile(view.Atlas, view.Tiles[Adjacency.PosY]))
			{
				axes |= Axes.NegativeY;
			}

			if (this.checkTile(view.Atlas, view.Tiles[Adjacency.NegZ]))
			{
				axes |= Axes.PositiveZ;
			}

			if (this.checkTile(view.Atlas, view.Tiles[Adjacency.PosZ]))
			{
				axes |= Axes.NegativeZ;
			}

			return this.cases[(int)axes].TryUpdateTile(view, out updatedTile);
		}
	}
}
