﻿namespace Tilescape.Engine
{
	public static class TileUpdater
	{
		public static ITileUpdater None { get; } = new NeverUpdater();

		public static ITileUpdater SetTo(
			TileTypeKey? type = null,
			OrthoAxis? orientation = null,
			TileShapeKey? shape = null,
			TileColorPaletteKey? colorPalette = null) => new SetUpdater(type, orientation, shape, colorPalette);

		private class NeverUpdater : ITileUpdater
		{
			/// <inheritdoc />
			public bool TryUpdateTile(ITileUpdaterView view, out Tile updatedTile)
			{
				ITileUpdaterContracts.TryUpdateTile(view);

				updatedTile = Tile.Unassigned;
				return false;
			}
		}

		private class SetUpdater : ITileUpdater
		{
			private readonly TileTypeKey? type;

			private readonly OrthoAxis? orientation;

			private readonly TileShapeKey? shape;

			private readonly TileColorPaletteKey? colorPalette;

			private readonly bool doesUpdate;

			public SetUpdater(
				TileTypeKey? type = null,
				OrthoAxis? orientation = null,
				TileShapeKey? shape = null,
				TileColorPaletteKey? colorPalette = null)
			{
				this.type = type;
				this.orientation = orientation;
				this.shape = shape;
				this.colorPalette = colorPalette;

				this.doesUpdate =
					this.type.HasValue ||
					this.orientation.HasValue ||
					this.shape.HasValue ||
					this.colorPalette.HasValue;
			}

			/// <inheritdoc />
			public bool TryUpdateTile(ITileUpdaterView view, out Tile updatedTile)
			{
				ITileUpdaterContracts.TryUpdateTile(view);

				if (this.doesUpdate)
				{
					updatedTile = view.Tiles[Adjacency.Center].Set(this.type, this.orientation, this.shape, this.colorPalette);
					return true;
				}
				else
				{
					updatedTile = Tile.Unassigned;
					return false;
				}
			}
		}
	}
}
