﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	public class HorizontalTileUpdater : ITileUpdater
	{
		private static readonly Case[] Cases = new Case[]
		{
			new Case(0, OrthoAxis.PosY000Norm_24),		// 0
			new Case(0, OrthoAxis.PosY000Norm_24),		// 1
			new Case(1, OrthoAxis.PosY000Norm_24),		// 2
			new Case(1, OrthoAxis.PosY000Norm_24),		// 3
			new Case(0, OrthoAxis.PosY000Norm_24),		// 4
			new Case(0, OrthoAxis.PosY000Norm_24),		// 5
			new Case(1, OrthoAxis.PosY000Norm_24),		// 6
			new Case(1, OrthoAxis.PosY000Norm_24),		// 7
			new Case(1, OrthoAxis.PosY270Norm_30),		// 8
			new Case(1, OrthoAxis.PosY270Norm_30),		// 9
			new Case(4, OrthoAxis.PosY000Norm_24),		// 10
			new Case(3, OrthoAxis.PosY000Norm_24),		// 11
			new Case(1, OrthoAxis.PosY270Norm_30),		// 12
			new Case(1, OrthoAxis.PosY270Norm_30),		// 13
			new Case(4, OrthoAxis.PosY000Norm_24),		// 14
			new Case(3, OrthoAxis.PosY000Norm_24),		// 15
			new Case(1, OrthoAxis.PosY090Norm_26),		// 16
			new Case(1, OrthoAxis.PosY090Norm_26),		// 17
			new Case(4, OrthoAxis.PosY090Norm_26),		// 18
			new Case(4, OrthoAxis.PosY090Norm_26),		// 19
			new Case(1, OrthoAxis.PosY090Norm_26),		// 20
			new Case(1, OrthoAxis.PosY090Norm_26),		// 21
			new Case(3, OrthoAxis.PosY090Norm_26),		// 22
			new Case(3, OrthoAxis.PosY090Norm_26),		// 23
			new Case(2, OrthoAxis.PosY090Norm_26),		// 24
			new Case(2, OrthoAxis.PosY090Norm_26),		// 25
			new Case(7, OrthoAxis.PosY000Norm_24),		// 26
			new Case(6, OrthoAxis.PosY180Flip_29),	// 27	mirror X
			new Case(2, OrthoAxis.PosY090Norm_26),		// 28
			new Case(2, OrthoAxis.PosY090Norm_26),		// 29
			new Case(6, OrthoAxis.PosY000Norm_24),		// 30
			new Case(5, OrthoAxis.PosY000Norm_24),		// 31
			new Case(0, OrthoAxis.PosY000Norm_24),		// 32
			new Case(0, OrthoAxis.PosY000Norm_24),		// 33
			new Case(1, OrthoAxis.PosY000Norm_24),		// 34
			new Case(1, OrthoAxis.PosY000Norm_24),		// 35
			new Case(0, OrthoAxis.PosY000Norm_24),		// 36
			new Case(0, OrthoAxis.PosY000Norm_24),		// 37
			new Case(1, OrthoAxis.PosY000Norm_24),		// 38
			new Case(1, OrthoAxis.PosY000Norm_24),		// 39
			new Case(1, OrthoAxis.PosY270Norm_30),		// 40
			new Case(1, OrthoAxis.PosY270Norm_30),		// 41
			new Case(4, OrthoAxis.PosY000Norm_24),		// 42
			new Case(3, OrthoAxis.PosY000Norm_24),		// 43
			new Case(1, OrthoAxis.PosY270Norm_30),		// 44
			new Case(1, OrthoAxis.PosY270Norm_30),		// 45
			new Case(4, OrthoAxis.PosY000Norm_24),		// 46
			new Case(3, OrthoAxis.PosY000Norm_24),		// 47
			new Case(1, OrthoAxis.PosY090Norm_26),		// 48
			new Case(1, OrthoAxis.PosY090Norm_26),		// 49
			new Case(4, OrthoAxis.PosY090Norm_26),		// 50
			new Case(4, OrthoAxis.PosY090Norm_26),		// 51
			new Case(1, OrthoAxis.PosY090Norm_26),		// 52
			new Case(1, OrthoAxis.PosY090Norm_26),		// 53
			new Case(3, OrthoAxis.PosY090Norm_26),		// 54
			new Case(3, OrthoAxis.PosY090Norm_26),		// 55
			new Case(2, OrthoAxis.PosY090Norm_26),		// 56
			new Case(2, OrthoAxis.PosY090Norm_26),		// 57
			new Case(7, OrthoAxis.PosY000Norm_24),		// 58
			new Case(6, OrthoAxis.PosY180Flip_29),	// 59	mirror X
			new Case(2, OrthoAxis.PosY090Norm_26),		// 60
			new Case(2, OrthoAxis.PosY090Norm_26),		// 61
			new Case(6, OrthoAxis.PosY000Norm_24),		// 62
			new Case(5, OrthoAxis.PosY000Norm_24),		// 63
			new Case(1, OrthoAxis.PosY180Norm_28),		// 64
			new Case(1, OrthoAxis.PosY180Norm_28),		// 65
			new Case(2, OrthoAxis.PosY000Norm_24),		// 66
			new Case(2, OrthoAxis.PosY000Norm_24),		// 67
			new Case(1, OrthoAxis.PosY180Norm_28),		// 68
			new Case(1, OrthoAxis.PosY180Norm_28),		// 69
			new Case(2, OrthoAxis.PosY000Norm_24),		// 70
			new Case(2, OrthoAxis.PosY000Norm_24),		// 71
			new Case(4, OrthoAxis.PosY270Norm_30),		// 72
			new Case(4, OrthoAxis.PosY270Norm_30),		// 73
			new Case(7, OrthoAxis.PosY270Norm_30),		// 74
			new Case(6, OrthoAxis.PosY270Norm_30),		// 75
			new Case(4, OrthoAxis.PosY270Norm_30),		// 76
			new Case(4, OrthoAxis.PosY270Norm_30),		// 77
			new Case(7, OrthoAxis.PosY270Norm_30),		// 78
			new Case(6, OrthoAxis.PosY270Norm_30),		// 79
			new Case(4, OrthoAxis.PosY180Norm_28),		// 80
			new Case(4, OrthoAxis.PosY180Norm_28),		// 81
			new Case(7, OrthoAxis.PosY090Norm_26),		// 82
			new Case(7, OrthoAxis.PosY090Norm_26),		// 83
			new Case(4, OrthoAxis.PosY180Norm_28),		// 84
			new Case(4, OrthoAxis.PosY180Norm_28),		// 85
			new Case(6, OrthoAxis.PosY090Flip_27),	// 86	mirror Z
			new Case(6, OrthoAxis.PosY090Flip_27),	// 87	mirror Z
			new Case(7, OrthoAxis.PosY180Norm_28),		// 88
			new Case(7, OrthoAxis.PosY180Norm_28),		// 89
			new Case(13, OrthoAxis.PosY000Norm_24),		// 90
			new Case(12, OrthoAxis.PosY090Norm_26),		// 91
			new Case(7, OrthoAxis.PosY180Norm_28),		// 92
			new Case(7, OrthoAxis.PosY180Norm_28),		// 93
			new Case(12, OrthoAxis.PosY180Norm_28),	// 94
			new Case(10, OrthoAxis.PosY180Norm_28),	// 95
			new Case(1, OrthoAxis.PosY180Norm_28),		// 96
			new Case(1, OrthoAxis.PosY180Norm_28),		// 97
			new Case(2, OrthoAxis.PosY000Norm_24),		// 98
			new Case(2, OrthoAxis.PosY000Norm_24),		// 99
			new Case(1, OrthoAxis.PosY180Norm_28),		// 100
			new Case(1, OrthoAxis.PosY180Norm_28),		// 101
			new Case(2, OrthoAxis.PosY000Norm_24),		// 102
			new Case(2, OrthoAxis.PosY000Norm_24),		// 103
			new Case(3, OrthoAxis.PosY270Norm_30),		// 104
			new Case(3, OrthoAxis.PosY270Norm_30),		// 105
			new Case(6, OrthoAxis.PosY270Flip_31),	// 106	mirror Z
			new Case(5, OrthoAxis.PosY270Norm_30),		// 107
			new Case(3, OrthoAxis.PosY270Norm_30),		// 108
			new Case(3, OrthoAxis.PosY270Norm_30),		// 109
			new Case(6, OrthoAxis.PosY270Flip_31),	// 110	mirror Z
			new Case(5, OrthoAxis.PosY270Norm_30),		// 111
			new Case(4, OrthoAxis.PosY180Norm_28),		// 112
			new Case(4, OrthoAxis.PosY180Norm_28),		// 113
			new Case(7, OrthoAxis.PosY090Norm_26),		// 114
			new Case(7, OrthoAxis.PosY090Norm_26),		// 115
			new Case(4, OrthoAxis.PosY180Norm_28),		// 116
			new Case(4, OrthoAxis.PosY180Norm_28),		// 117
			new Case(6, OrthoAxis.PosY090Flip_27),	// 118	mirror Z
			new Case(6, OrthoAxis.PosY090Flip_27),	// 119	mirror Z
			new Case(6, OrthoAxis.PosY180Norm_28),		// 120
			new Case(6, OrthoAxis.PosY180Norm_28),		// 121
			new Case(12, OrthoAxis.PosY000Norm_24),		// 122
			new Case(10, OrthoAxis.PosY090Norm_26),		// 123
			new Case(6, OrthoAxis.PosY180Norm_28),		// 124
			new Case(6, OrthoAxis.PosY180Norm_28),		// 125
			new Case(11, OrthoAxis.PosY000Norm_24),		// 126
			new Case(9, OrthoAxis.PosY180Norm_28),		// 127
			new Case(0, OrthoAxis.PosY000Norm_24),		// 128
			new Case(0, OrthoAxis.PosY000Norm_24),		// 129
			new Case(1, OrthoAxis.PosY000Norm_24),		// 130
			new Case(1, OrthoAxis.PosY000Norm_24),		// 131
			new Case(0, OrthoAxis.PosY000Norm_24),		// 132
			new Case(0, OrthoAxis.PosY000Norm_24),		// 133
			new Case(1, OrthoAxis.PosY000Norm_24),		// 134
			new Case(1, OrthoAxis.PosY000Norm_24),		// 135
			new Case(1, OrthoAxis.PosY270Norm_30),		// 136
			new Case(1, OrthoAxis.PosY270Norm_30),		// 137
			new Case(4, OrthoAxis.PosY000Norm_24),		// 138
			new Case(3, OrthoAxis.PosY000Norm_24),		// 139
			new Case(1, OrthoAxis.PosY270Norm_30),		// 140
			new Case(1, OrthoAxis.PosY270Norm_30),		// 141
			new Case(4, OrthoAxis.PosY000Norm_24),		// 142
			new Case(3, OrthoAxis.PosY000Norm_24),		// 143
			new Case(1, OrthoAxis.PosY090Norm_26),		// 144
			new Case(1, OrthoAxis.PosY090Norm_26),		// 145
			new Case(4, OrthoAxis.PosY090Norm_26),		// 146
			new Case(4, OrthoAxis.PosY090Norm_26),		// 147
			new Case(1, OrthoAxis.PosY090Norm_26),		// 148
			new Case(1, OrthoAxis.PosY090Norm_26),		// 149
			new Case(3, OrthoAxis.PosY090Norm_26),		// 150
			new Case(3, OrthoAxis.PosY090Norm_26),		// 151
			new Case(2, OrthoAxis.PosY090Norm_26),		// 152
			new Case(2, OrthoAxis.PosY090Norm_26),		// 153
			new Case(7, OrthoAxis.PosY000Norm_24),		// 154
			new Case(6, OrthoAxis.PosY180Flip_29),	// 155	mirror X
			new Case(2, OrthoAxis.PosY090Norm_26),		// 156
			new Case(2, OrthoAxis.PosY090Norm_26),		// 157
			new Case(6, OrthoAxis.PosY000Norm_24),		// 158
			new Case(5, OrthoAxis.PosY000Norm_24),		// 159
			new Case(0, OrthoAxis.PosY000Norm_24),		// 160
			new Case(0, OrthoAxis.PosY000Norm_24),		// 161
			new Case(1, OrthoAxis.PosY000Norm_24),		// 162
			new Case(1, OrthoAxis.PosY000Norm_24),		// 163
			new Case(0, OrthoAxis.PosY000Norm_24),		// 164
			new Case(0, OrthoAxis.PosY000Norm_24),		// 165
			new Case(1, OrthoAxis.PosY000Norm_24),		// 166
			new Case(1, OrthoAxis.PosY000Norm_24),		// 167
			new Case(1, OrthoAxis.PosY270Norm_30),		// 168
			new Case(1, OrthoAxis.PosY270Norm_30),		// 169
			new Case(4, OrthoAxis.PosY000Norm_24),		// 170
			new Case(3, OrthoAxis.PosY000Norm_24),		// 171
			new Case(1, OrthoAxis.PosY270Norm_30),		// 172
			new Case(1, OrthoAxis.PosY270Norm_30),		// 173
			new Case(4, OrthoAxis.PosY000Norm_24),		// 174
			new Case(3, OrthoAxis.PosY000Norm_24),		// 175
			new Case(1, OrthoAxis.PosY090Norm_26),		// 176
			new Case(1, OrthoAxis.PosY090Norm_26),		// 177
			new Case(4, OrthoAxis.PosY090Norm_26),		// 178
			new Case(4, OrthoAxis.PosY090Norm_26),		// 179
			new Case(1, OrthoAxis.PosY090Norm_26),		// 180
			new Case(1, OrthoAxis.PosY090Norm_26),		// 181
			new Case(3, OrthoAxis.PosY090Norm_26),		// 182
			new Case(3, OrthoAxis.PosY090Norm_26),		// 183
			new Case(2, OrthoAxis.PosY090Norm_26),		// 184
			new Case(2, OrthoAxis.PosY090Norm_26),		// 185
			new Case(7, OrthoAxis.PosY000Norm_24),		// 186
			new Case(6, OrthoAxis.PosY180Flip_29),	// 187	mirror X
			new Case(2, OrthoAxis.PosY090Norm_26),		// 188
			new Case(2, OrthoAxis.PosY090Norm_26),		// 189
			new Case(6, OrthoAxis.PosY000Norm_24),		// 190
			new Case(5, OrthoAxis.PosY000Norm_24),		// 191
			new Case(1, OrthoAxis.PosY180Norm_28),		// 192
			new Case(1, OrthoAxis.PosY180Norm_28),		// 193
			new Case(2, OrthoAxis.PosY000Norm_24),		// 194
			new Case(2, OrthoAxis.PosY000Norm_24),		// 195
			new Case(1, OrthoAxis.PosY180Norm_28),		// 196
			new Case(1, OrthoAxis.PosY180Norm_28),		// 197
			new Case(2, OrthoAxis.PosY000Norm_24),		// 198
			new Case(2, OrthoAxis.PosY000Norm_24),		// 199
			new Case(4, OrthoAxis.PosY270Norm_30),		// 200
			new Case(4, OrthoAxis.PosY270Norm_30),		// 201
			new Case(7, OrthoAxis.PosY270Norm_30),		// 202
			new Case(6, OrthoAxis.PosY270Norm_30),		// 203
			new Case(4, OrthoAxis.PosY270Norm_30),		// 204
			new Case(4, OrthoAxis.PosY270Norm_30),		// 205
			new Case(7, OrthoAxis.PosY270Norm_30),		// 206
			new Case(6, OrthoAxis.PosY270Norm_30),		// 207
			new Case(3, OrthoAxis.PosY180Norm_28),		// 208
			new Case(3, OrthoAxis.PosY180Norm_28),		// 209
			new Case(6, OrthoAxis.PosY090Norm_26),		// 210
			new Case(6, OrthoAxis.PosY090Norm_26),		// 211
			new Case(3, OrthoAxis.PosY180Norm_28),		// 212
			new Case(3, OrthoAxis.PosY180Norm_28),		// 213
			new Case(5, OrthoAxis.PosY090Norm_26),		// 214
			new Case(5, OrthoAxis.PosY090Norm_26),		// 215
			new Case(6, OrthoAxis.PosY000Flip_25),	// 216	mirror X
			new Case(6, OrthoAxis.PosY000Flip_25),	// 217	mirror X
			new Case(12, OrthoAxis.PosY270Norm_30),	// 218
			new Case(11, OrthoAxis.PosY090Norm_26),		// 219
			new Case(6, OrthoAxis.PosY000Flip_25),	// 220	mirror X
			new Case(6, OrthoAxis.PosY000Flip_25),	// 221	mirror X
			new Case(10, OrthoAxis.PosY270Norm_30),	// 222
			new Case(9, OrthoAxis.PosY270Norm_30),		// 223
			new Case(1, OrthoAxis.PosY180Norm_28),		// 224
			new Case(1, OrthoAxis.PosY180Norm_28),		// 225
			new Case(2, OrthoAxis.PosY000Norm_24),		// 226
			new Case(2, OrthoAxis.PosY000Norm_24),		// 227
			new Case(1, OrthoAxis.PosY180Norm_28),		// 228
			new Case(1, OrthoAxis.PosY180Norm_28),		// 229
			new Case(2, OrthoAxis.PosY000Norm_24),		// 230
			new Case(2, OrthoAxis.PosY000Norm_24),		// 231
			new Case(3, OrthoAxis.PosY270Norm_30),		// 232
			new Case(3, OrthoAxis.PosY270Norm_30),		// 233
			new Case(6, OrthoAxis.PosY270Flip_31),	// 234	mirror Z
			new Case(5, OrthoAxis.PosY270Norm_30),		// 235
			new Case(3, OrthoAxis.PosY270Norm_30),		// 236
			new Case(3, OrthoAxis.PosY270Norm_30),		// 237
			new Case(6, OrthoAxis.PosY270Flip_31),	// 238	mirror Z
			new Case(5, OrthoAxis.PosY270Norm_30),		// 239
			new Case(3, OrthoAxis.PosY180Norm_28),		// 240
			new Case(3, OrthoAxis.PosY180Norm_28),		// 241
			new Case(6, OrthoAxis.PosY090Norm_26),		// 242
			new Case(6, OrthoAxis.PosY090Norm_26),		// 243
			new Case(3, OrthoAxis.PosY180Norm_28),		// 244
			new Case(3, OrthoAxis.PosY180Norm_28),		// 245
			new Case(5, OrthoAxis.PosY090Norm_26),		// 246
			new Case(5, OrthoAxis.PosY090Norm_26),		// 247
			new Case(5, OrthoAxis.PosY180Norm_28),		// 248
			new Case(5, OrthoAxis.PosY180Norm_28),		// 249
			new Case(10, OrthoAxis.PosY000Norm_24),		// 250
			new Case(9, OrthoAxis.PosY090Norm_26),		// 251
			new Case(5, OrthoAxis.PosY180Norm_28),		// 252
			new Case(5, OrthoAxis.PosY180Norm_28),		// 253
			new Case(9, OrthoAxis.PosY000Norm_24),		// 254
			new Case(8, OrthoAxis.PosY000Norm_24)		// 255
		};

		private readonly ITileUpdater[] shapeCases;

		private readonly Func<ITilescapeAtlas, Tile, bool> checkAdjacentTile;

		#region Constructors

		public HorizontalTileUpdater(
			ITileUpdater case0,
			ITileUpdater case1,
			ITileUpdater case2,
			ITileUpdater case3,
			ITileUpdater case4,
			ITileUpdater case5,
			ITileUpdater case6,
			ITileUpdater case7,
			ITileUpdater case8,
			ITileUpdater case9,
			ITileUpdater case10,
			ITileUpdater case11,
			ITileUpdater case12,
			ITileUpdater case13,
			Func<ITilescapeAtlas, Tile, bool> checkAdjacentTile)
		{
			Require.That(checkAdjacentTile != null);

			this.shapeCases = new[]
			{
				case0 ?? TileUpdater.None,
				case1 ?? TileUpdater.None,
				case2 ?? TileUpdater.None,
				case3 ?? TileUpdater.None,
				case4 ?? TileUpdater.None,
				case5 ?? TileUpdater.None,
				case6 ?? TileUpdater.None,
				case7 ?? TileUpdater.None,
				case8 ?? TileUpdater.None,
				case9 ?? TileUpdater.None,
				case10 ?? TileUpdater.None,
				case11 ?? TileUpdater.None,
				case12 ?? TileUpdater.None,
				case13 ?? TileUpdater.None
			};

			this.checkAdjacentTile = checkAdjacentTile;
		}

		public HorizontalTileUpdater(
			ITileUpdater case0,
			ITileUpdater case1,
			ITileUpdater case2,
			ITileUpdater case3,
			Func<ITilescapeAtlas, Tile, bool> checkAdjacentTile)
		{
			Require.That(checkAdjacentTile != null);

			case0 = case0 ?? TileUpdater.None;
			case1 = case1 ?? TileUpdater.None;
			case2 = case2 ?? TileUpdater.None;
			case3 = case3 ?? TileUpdater.None;

			this.shapeCases = new[]
			{
				case0,
				case0,
				case0,
				case1,
				case1,
				case2,
				case2,
				case2,
				case0,
				case3,
				case0,
				case0,
				case0,
				case0
			};

			this.checkAdjacentTile = checkAdjacentTile;
		}

		#endregion

		/// <inheritdoc />
		public bool TryUpdateTile(ITileUpdaterView view, out Tile updatedTile)
		{
			ITileUpdaterContracts.TryUpdateTile(view);

			int index = 0;
			AddCornerBit(-1, -1);
			AddCornerBit(-1, 0);
			AddCornerBit(-1, 1);
			AddCornerBit(0, -1);
			AddCornerBit(0, 1);
			AddCornerBit(1, -1);
			AddCornerBit(1, 0);
			AddCornerBit(1, 1);

			var currentCase = Cases[index];
			if (this.shapeCases[currentCase.Index].TryUpdateTile(view, out updatedTile))
			{
				// overwrite the rotation of the updated tile
				updatedTile = updatedTile.SetOrientation(currentCase.Orientation);
				return true;
			}
			else
			{
				return false;
			}

			void AddCornerBit(int x, int z) =>
				BitPack.Add(ref index, this.checkAdjacentTile(view.Atlas, view.Tiles[new Index3D(x, 0, z)]));
		}

		private struct Case
		{
			public Case(byte index, OrthoAxis orientation)
			{
				this.Index = index;
				this.Orientation = orientation;
			}

			public readonly byte Index;

			public readonly OrthoAxis Orientation;
		}
	}
}
