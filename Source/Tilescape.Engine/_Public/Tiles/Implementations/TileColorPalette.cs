﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using UnityEngine;

namespace Tilescape.Engine
{
	public class TileColorPalette : ITileColorPalette
	{
		/// <inheritdoc />
		public Guid Key { get; internal set; }

		/// <inheritdoc />
		public string Name { get; internal set; }

		/// <inheritdoc />
		public int Count => this.Array.Length;

		/// <inheritdoc />
		public Color32 this[int index] => this.Array[index];

		/// <inheritdoc />
		public IEnumerator<Color32> GetEnumerator() => this.Array.GetEnumerator<Color32>();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		internal Color32[] Array { get; } = new Color32[ColorPalette.RequirredLength];

		internal TileColorPaletteEntity Entity { get; set; }
	}
}
