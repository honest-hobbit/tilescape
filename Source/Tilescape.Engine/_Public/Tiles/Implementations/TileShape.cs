﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public class TileShape : ITileShapeView
	{
		public TileShape(int sizeExponent)
		{
			Require.That(sizeExponent >= 0);

			this.Voxels = new CubeArray<ColorVoxel>(sizeExponent);
			this.Colliders = new TileColliders();
			this.Meshes = new TileMeshes();
			this.FaceMasks = new TileFaceMasks(this.Voxels);
		}

		/// <inheritdoc />
		public Guid Key { get; internal set; }

		/// <inheritdoc />
		public string Name { get; internal set; }

		/// <inheritdoc />
		ICubeArrayView<ColorVoxel> ITileShapeView.Voxels => this.Voxels;

		/// <inheritdoc />
		ITileColliders ITileShapeView.Colliders => this.Colliders;

		/// <inheritdoc />
		ITileParts<IListView<MeshQuadSlim>> ITileShapeView.Meshes => this.Meshes;

		/// <inheritdoc />
		IOriented<ITileFaces<ITileFaceMask>> ITileShapeView.FaceMasks => this.FaceMasks;

		internal CubeArray<ColorVoxel> Voxels { get; }

		internal TileColliders Colliders { get; }

		internal TileMeshes Meshes { get; }

		internal TileFaceMasks FaceMasks { get; }

		internal TileShapeEntity Entity { get; set; }

		/// <inheritdoc />
		public override string ToString() => this.Name;

		internal void Clear()
		{
			this.Key = Guid.Empty;
			this.Name = string.Empty;
			this.Voxels.Array.SetAllTo(ColorVoxel.Empty);
			this.Colliders.Clear();
			this.Meshes.Clear();
			this.FaceMasks.Clear();
		}
	}
}
