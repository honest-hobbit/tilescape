﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public class TileTypeBuilder
	{
		public TileTypeKey Key { get; set; } = TileTypeKey.Unassigned;

		public string Name { get; set; }

		public int Ordering { get; set; }

		public bool IsFilled { get; set; }

		public ITileUpdater Updater { get; set; }

		public ShapesList Shapes { get; } = new ShapesList();

		public ColorPalettesList ColorPalettes { get; } = new ColorPalettesList();

		public void Clear()
		{
			this.Key = TileTypeKey.Unassigned;
			this.Name = null;
			this.Ordering = 0;
			this.IsFilled = false;
			this.Updater = null;
			this.Shapes.Clear();
			this.ColorPalettes.Clear();
		}

		public ITileTypeOLD Build(bool clear = true)
		{
			var result = new TileType(
				this.Key, this.Name, this.Ordering, this.IsFilled, this.Shapes, this.ColorPalettes, this.Updater);
			if (clear)
			{
				this.Clear();
			}

			return result;
		}

		public class ListBuilder<T> : IEnumerable<T>
			where T : IKeyed<Guid>
		{
			private readonly List<T> values = new List<T>();

			private readonly Dictionary<Guid, byte> indices = new Dictionary<Guid, byte>(Equality.StructComparer<Guid>());

			public int Count => this.values.Count;

			public bool Contains(Guid key) => this.indices.ContainsKey(key);

			public void Clear()
			{
				this.values.Clear();
				this.indices.Clear();
			}

			/// <inheritdoc />
			public IEnumerator<T> GetEnumerator() => this.values.GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

			protected byte AddValue(T value)
			{
				Require.That(this.Count < 256);
				Require.That(value != null);
				Require.That(!this.Contains(value.Key));

				byte index = (byte)this.values.Count;
				this.values.Add(value);
				this.indices.Add(value.Key, index);
				return index;
			}

			protected byte GetIndexOfValue(Guid key)
			{
				Require.That(this.Contains(key));

				return this.indices[key];
			}
		}

		public class ShapesList : ListBuilder<ITileShapeView>
		{
			public TileShapeKey Add(ITileShapeView value) => new TileShapeKey(this.AddValue(value));

			public TileShapeKey GetKeyOf(Guid key) => new TileShapeKey(this.GetIndexOfValue(key));

			public TileShapeKey GetKeyOf(ITileShapeView value)
			{
				Require.That(value != null);

				return this.GetKeyOf(value.Key);
			}
		}

		public class ColorPalettesList : ListBuilder<ITileColorPalette>
		{
			public TileColorPaletteKey Add(ITileColorPalette value) => new TileColorPaletteKey(this.AddValue(value));

			public TileColorPaletteKey GetKeyOf(Guid key) => new TileColorPaletteKey(this.GetIndexOfValue(key));

			public TileColorPaletteKey GetKeyOf(ITileColorPalette value)
			{
				Require.That(value != null);

				return this.GetKeyOf(value.Key);
			}
		}
	}
}
