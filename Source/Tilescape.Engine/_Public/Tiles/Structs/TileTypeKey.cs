﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public struct TileTypeKey : IEquatable<TileTypeKey>, IIndexed<ushort>
	{
		public TileTypeKey(ushort index)
		{
			Require.That(index != 0, $"{nameof(index)} of 0 is reserved for Unassigned tiles.");

			this.Index = index;
		}

		public static TileTypeKey Unassigned => default(TileTypeKey);

		/// <inheritdoc />
		public ushort Index { get; }

		public static bool operator ==(TileTypeKey lhs, TileTypeKey rhs) => lhs.Equals(rhs);

		public static bool operator !=(TileTypeKey lhs, TileTypeKey rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public bool Equals(TileTypeKey other) => this.Index == other.Index;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => this.Index.GetHashCode();

		/// <inheritdoc />
		public override string ToString() => this.Index.ToString();
	}
}
