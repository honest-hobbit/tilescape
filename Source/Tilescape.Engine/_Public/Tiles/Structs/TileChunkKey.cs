﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Indexing.Curves;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public struct TileChunkKey : IEquatable<TileChunkKey>
	{
		public TileChunkKey(Guid stageKey, Index3D chunkIndex)
		{
			this.StageKey = stageKey;
			this.Index = chunkIndex;
		}

		public static EqualityComparer<TileChunkKey> MortonCurveComparer { get; } = new KeyEqualityComparer();

		public Guid StageKey { get; }

		public Index3D Index { get; }

		#region Operators

		public static TileChunkKey operator +(TileChunkKey key) => key;

		public static TileChunkKey operator -(TileChunkKey key) => new TileChunkKey(key.StageKey, -key.Index);

		public static TileChunkKey operator +(TileChunkKey key, Index3D index) => new TileChunkKey(key.StageKey, key.Index + index);

		public static TileChunkKey operator -(TileChunkKey key, Index3D index) => new TileChunkKey(key.StageKey, key.Index - index);

		public static TileChunkKey operator *(TileChunkKey key, Index3D index) => new TileChunkKey(key.StageKey, key.Index * index);

		public static TileChunkKey operator *(TileChunkKey key, int scalar) => new TileChunkKey(key.StageKey, key.Index * scalar);

		public static TileChunkKey operator *(int scalar, TileChunkKey key) => new TileChunkKey(key.StageKey, key.Index * scalar);

		public static TileChunkKey operator /(TileChunkKey key, Index3D index) => new TileChunkKey(key.StageKey, key.Index / index);

		public static TileChunkKey operator /(TileChunkKey key, int scalar) => new TileChunkKey(key.StageKey, key.Index / scalar);

		public static TileChunkKey operator %(TileChunkKey key, Index3D index) => new TileChunkKey(key.StageKey, key.Index % index);

		public static TileChunkKey operator %(TileChunkKey key, int scalar) => new TileChunkKey(key.StageKey, key.Index % scalar);

		public static bool operator ==(TileChunkKey lhs, TileChunkKey rhs) => lhs.Equals(rhs);

		public static bool operator !=(TileChunkKey lhs, TileChunkKey rhs) => !lhs.Equals(rhs);

		#endregion

		/// <inheritdoc />
		public bool Equals(TileChunkKey other) => this.StageKey == other.StageKey && this.Index == other.Index;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => HashCode.Start(this.StageKey).And(this.Index);

		/// <inheritdoc />
		public override string ToString() => $"[{this.StageKey.ToString()}: {this.Index.ToString()}]";

		private class KeyEqualityComparer : EqualityComparer<TileChunkKey>
		{
			private static readonly EqualityComparer<Index3D> IndexComparer = MortonCurve.Comparer<Index3D>();

			/// <inheritdoc />
			public override bool Equals(TileChunkKey a, TileChunkKey b) =>
				a.StageKey == b.StageKey && IndexComparer.Equals(a.Index, b.Index);

			/// <inheritdoc />
			public override int GetHashCode(TileChunkKey key) =>
				HashCode.Start(key.StageKey).And(IndexComparer.GetHashCode(key.Index));
		}
	}
}
