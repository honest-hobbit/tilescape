﻿using System;
using System.Runtime.InteropServices;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct Tile : IEquatable<Tile>
	{
		public Tile(
			TileTypeKey type,
			OrthoAxis orientation = OrthoAxis.PosY000Norm_24,
			TileShapeKey shape = default(TileShapeKey),
			TileColorPaletteKey colorPalette = default(TileColorPaletteKey))
		{
			Require.That(type.Index != 0, $"{nameof(type)}.{nameof(type.Index)} of 0 is reserved for Unassigned tiles.");
			Require.That(Enumeration.IsDefined(orientation));

			this.Type = type;
			this.Orientation = orientation;
			this.Shape = shape;
			this.ColorPalette = colorPalette;
		}

		public static Tile Unassigned => default(Tile);

		public TileTypeKey Type { get; }

		public OrthoAxis Orientation { get; }

		public TileShapeKey Shape { get; }

		public TileColorPaletteKey ColorPalette { get; }

		public static bool operator ==(Tile lhs, Tile rhs) => lhs.Equals(rhs);

		public static bool operator !=(Tile lhs, Tile rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public bool Equals(Tile other) =>
			this.Type == other.Type &&
			this.Orientation == other.Orientation &&
			this.Shape == other.Shape &&
			this.ColorPalette == other.ColorPalette;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() =>
			HashCode.Start(this.Type).And(this.Orientation).And(this.Shape).And(this.ColorPalette);

		/// <inheritdoc />
		public override string ToString() =>
			$"[{this.Type.ToString()}, {this.Orientation.ToString()}, {this.Shape.ToString()}, {this.ColorPalette.ToString()}]";

		public Tile SetType(TileTypeKey type) => new Tile(type, this.Orientation, this.Shape, this.ColorPalette);

		public Tile SetOrientation(OrthoAxis orientation) => new Tile(this.Type, orientation, this.Shape, this.ColorPalette);

		public Tile SetShape(TileShapeKey shapeKey) => new Tile(this.Type, this.Orientation, shapeKey, this.ColorPalette);

		public Tile SetColorPalette(TileColorPaletteKey colorKey) => new Tile(this.Type, this.Orientation, this.Shape, colorKey);

		public Tile Set(
			TileTypeKey? type = null,
			OrthoAxis? orientation = null,
			TileShapeKey? shape = null,
			TileColorPaletteKey? colorPalette = null) => new Tile(
				type ?? this.Type,
				orientation ?? this.Orientation,
				shape ?? this.Shape,
				colorPalette ?? this.ColorPalette);
	}
}
