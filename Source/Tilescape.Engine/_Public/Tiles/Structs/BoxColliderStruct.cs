﻿using UnityEngine;

namespace Tilescape.Engine
{
	public struct BoxColliderStruct
	{
		public BoxColliderStruct(Vector3 center, Vector3 size)
		{
			this.Center = center;
			this.Size = size;
		}

		public Vector3 Center { get; }

		public Vector3 Size { get; }
	}
}
