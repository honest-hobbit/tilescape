﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public struct TileShapeData : IEquatable<TileShapeData>
	{
		private readonly ITileShapeView shape;

		private readonly ITileColorPalette colors;

		private readonly OrthoAxis rotation;

		public TileShapeData(ITileShapeView shape, ITileColorPalette colors, OrthoAxis rotation = OrthoAxis.PosY000Norm_24)
		{
			Require.That(shape != null);
			Require.That(colors.IsValid());
			Require.That(Enumeration.IsDefined(rotation));

			this.shape = shape;
			this.colors = colors;
			this.rotation = rotation;
		}

		public static TileShapeData Unassigned => default(TileShapeData);

		public bool IsAssigned => this.shape != null;

		public ITileShapeView Shape
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.shape;
			}
		}

		public ITileColorPalette Colors
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.colors;
			}
		}

		public OrthoAxis Rotation
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.rotation;
			}
		}

		public static bool operator ==(TileShapeData lhs, TileShapeData rhs) => lhs.Equals(rhs);

		public static bool operator !=(TileShapeData lhs, TileShapeData rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public bool Equals(TileShapeData other) =>
			this.rotation == other.rotation &&
			this.shape?.Key == other.shape?.Key &&
			this.colors?.Key == other.colors?.Key;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => HashCode.Start(this.shape).And(this.colors).And(this.rotation);

		/// <inheritdoc />
		public override string ToString() => this.IsAssigned ? $"{this.Shape.ToString()} [{this.Rotation}]" : "Unassigned";

		internal ITileFaces<ITileFaceMask> GetFaceMasks()
		{
			Require.That(this.IsAssigned);

			return this.Shape.FaceMasks[this.Rotation];
		}
	}
}
