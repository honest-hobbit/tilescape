﻿using System;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public struct TileShapeKey : IEquatable<TileShapeKey>, IIndexed<byte>
	{
		public TileShapeKey(byte index)
		{
			this.Index = index;
		}

		/// <inheritdoc />
		public byte Index { get; }

		public static bool operator ==(TileShapeKey lhs, TileShapeKey rhs) => lhs.Equals(rhs);

		public static bool operator !=(TileShapeKey lhs, TileShapeKey rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public bool Equals(TileShapeKey other) => this.Index == other.Index;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => this.Index.GetHashCode();

		/// <inheritdoc />
		public override string ToString() => this.Index.ToString();
	}
}
