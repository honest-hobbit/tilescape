﻿using System;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public struct TileColorPaletteKey : IEquatable<TileColorPaletteKey>, IIndexed<byte>
	{
		public TileColorPaletteKey(byte index)
		{
			this.Index = index;
		}

		/// <inheritdoc />
		public byte Index { get; }

		public static bool operator ==(TileColorPaletteKey lhs, TileColorPaletteKey rhs) => lhs.Equals(rhs);

		public static bool operator !=(TileColorPaletteKey lhs, TileColorPaletteKey rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public bool Equals(TileColorPaletteKey other) => this.Index == other.Index;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => this.Index.GetHashCode();

		/// <inheritdoc />
		public override string ToString() => this.Index.ToString();
	}
}
