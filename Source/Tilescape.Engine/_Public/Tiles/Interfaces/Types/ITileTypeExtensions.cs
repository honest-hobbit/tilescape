﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class ITileTypeExtensions
	{
		public static TileShapeData GetShapeData(this ITileTypeOLD type, Tile tile)
		{
			Require.That(type != null);
			Require.That(type.Key == tile.Type);

			return new TileShapeData(type.Shapes[tile.Shape.Index], type.ColorPalettes[tile.ColorPalette.Index], tile.Orientation);
		}
	}
}
