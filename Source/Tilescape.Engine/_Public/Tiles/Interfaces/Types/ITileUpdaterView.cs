﻿using System;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	public interface ITileUpdaterView
	{
		ITilescapeAtlas Atlas { get; }

		IAdjacencyView<Tile> Tiles { get; }

		Guid StageKey { get; }

		Index3D TileIndex { get; }
	}
}
