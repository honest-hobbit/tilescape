﻿namespace Tilescape.Engine
{
	public static class ITileColorPaletteExtensions
	{
		public static bool IsValid(this ITileColorPalette palette) => ColorPalette.IsValid(palette);
	}
}
