﻿namespace Tilescape.Engine
{
	public interface ITileUpdater
	{
		bool TryUpdateTile(ITileUpdaterView view, out Tile updatedTile);
	}
}
