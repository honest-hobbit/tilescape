﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Types;
using UnityEngine;

namespace Tilescape.Engine
{
	public interface ITileColorPalette : IKeyed<Guid>, INamed, IListView<Color32>
	{
	}
}
