﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public interface ITileTypeOLD : IKeyed<TileTypeKey>, INamed
	{
		// higher value Ordering will overlap lower value Ordering
		int Ordering { get; }

		bool IsFilled { get; }

		IListView<ITileShapeView> Shapes { get; }

		IListView<ITileColorPalette> ColorPalettes { get; }

		ITileUpdater Updater { get; }
	}
}
