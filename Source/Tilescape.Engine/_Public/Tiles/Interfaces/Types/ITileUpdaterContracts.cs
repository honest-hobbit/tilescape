﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class ITileUpdaterContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void TryUpdateTile(ITileUpdaterView view)
		{
			Require.That(view != null);
			Require.That(view.Tiles.AllAssigned());
		}
	}
}
