﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class ITileFaceMaskExtensions
	{
		public static bool IsCoveredBy(this ITileFaceMask self, ITileFaceMask other)
		{
			Require.That(self != null);
			Require.That(other != null);
			Require.That(other.Count == self.Count);

			for (int index = 0; index < self.Count; index++)
			{
				if ((self[index] & ~other[index]) != 0)
				{
					return false;
				}
			}

			return true;
		}

		public static bool IsNegativeXCoveredBy(this ITileFaces<ITileFaceMask> self, ITileFaces<ITileFaceMask> other)
		{
			Require.That(self != null);
			Require.That(other != null);

			return self.NegativeX.IsCoveredBy(other.PositiveX);
		}

		public static bool IsPositiveXCoveredBy(this ITileFaces<ITileFaceMask> self, ITileFaces<ITileFaceMask> other)
		{
			Require.That(self != null);
			Require.That(other != null);

			return self.PositiveX.IsCoveredBy(other.NegativeX);
		}

		public static bool IsNegativeYCoveredBy(this ITileFaces<ITileFaceMask> self, ITileFaces<ITileFaceMask> other)
		{
			Require.That(self != null);
			Require.That(other != null);

			return self.NegativeY.IsCoveredBy(other.PositiveY);
		}

		public static bool IsPositiveYCoveredBy(this ITileFaces<ITileFaceMask> self, ITileFaces<ITileFaceMask> other)
		{
			Require.That(self != null);
			Require.That(other != null);

			return self.PositiveY.IsCoveredBy(other.NegativeY);
		}

		public static bool IsNegativeZCoveredBy(this ITileFaces<ITileFaceMask> self, ITileFaces<ITileFaceMask> other)
		{
			Require.That(self != null);
			Require.That(other != null);

			return self.NegativeZ.IsCoveredBy(other.PositiveZ);
		}

		public static bool IsPositiveZCoveredBy(this ITileFaces<ITileFaceMask> self, ITileFaces<ITileFaceMask> other)
		{
			Require.That(self != null);
			Require.That(other != null);

			return self.PositiveZ.IsCoveredBy(other.NegativeZ);
		}
	}
}
