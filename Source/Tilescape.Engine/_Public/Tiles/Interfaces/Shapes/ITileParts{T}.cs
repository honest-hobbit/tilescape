﻿namespace Tilescape.Engine
{
	public interface ITileParts<T> : ITileFaces<T>
	{
		T Center { get; }
	}
}
