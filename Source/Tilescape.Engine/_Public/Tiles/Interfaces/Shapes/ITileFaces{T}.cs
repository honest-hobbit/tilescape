﻿namespace Tilescape.Engine
{
	public interface ITileFaces<T>
	{
		T NegativeX { get; }

		T PositiveX { get; }

		T NegativeY { get; }

		T PositiveY { get; }

		T NegativeZ { get; }

		T PositiveZ { get; }
	}
}
