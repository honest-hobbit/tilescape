﻿using Tilescape.Utility.Collections;

namespace Tilescape.Engine
{
	public interface ITileColliders : IListView<BoxColliderStruct>
	{
		bool IsFullCube { get; }
	}
}
