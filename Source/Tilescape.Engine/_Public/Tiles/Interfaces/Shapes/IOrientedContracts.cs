﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;

namespace Tilescape.Engine
{
	public static class IOrientedContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer(OrthoAxis orientation)
		{
			Require.That(Enumeration.IsDefined(orientation));
		}
	}
}
