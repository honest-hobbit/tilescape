﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public interface ITileShapeView : IKeyed<Guid>, INamed
	{
		ICubeArrayView<ColorVoxel> Voxels { get; }

		ITileColliders Colliders { get; }

		ITileParts<IListView<MeshQuadSlim>> Meshes { get; }

		IOriented<ITileFaces<ITileFaceMask>> FaceMasks { get; }
	}
}
