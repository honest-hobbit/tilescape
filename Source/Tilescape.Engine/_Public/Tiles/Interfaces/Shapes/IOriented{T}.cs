﻿namespace Tilescape.Engine
{
	public interface IOriented<T>
	{
		T this[OrthoAxis orientation] { get; }
	}
}
