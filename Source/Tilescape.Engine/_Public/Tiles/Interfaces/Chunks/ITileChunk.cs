﻿using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public interface ITileChunk : ITileChunkView
	{
		new Try<Tile> UniformTile { get; set; }

		new CubeArray<Tile> Tiles { get; }
	}
}
