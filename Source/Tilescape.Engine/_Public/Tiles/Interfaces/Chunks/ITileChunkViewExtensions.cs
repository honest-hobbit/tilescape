﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	public static class ITileChunkViewExtensions
	{
		public static Index3D StageTileIndexOfLowerBounds(this ITileChunkView chunk)
		{
			Require.That(chunk != null);

			return chunk.Key.Index << chunk.SizeExponent;
		}

		public static Index3D StageTileIndexOfUpperBounds(this ITileChunkView chunk)
		{
			Require.That(chunk != null);

			return chunk.StageTileIndexOfLowerBounds() + new Index3D(chunk.SideLength - 1);
		}

		public static Tile GetTile(this ITileChunkView chunk, Index3D index)
		{
			Require.That(chunk != null);

			return chunk.IsUniform ? chunk.UniformTile.Value : chunk.Tiles[index];
		}

		public static Tile GetTile(this ITileChunkView chunk, int x, int y, int z)
		{
			Require.That(chunk != null);

			return chunk.IsUniform ? chunk.UniformTile.Value : chunk.Tiles[x, y, z];
		}
	}
}
