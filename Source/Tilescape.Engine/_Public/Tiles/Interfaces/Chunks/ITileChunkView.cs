﻿using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public interface ITileChunkView : IKeyed<TileChunkKey>
	{
		int SizeExponent { get; }

		int SideLength { get; }

		bool IsUniform { get; }

		Try<Tile> UniformTile { get; }

		ICubeArrayView<Tile> Tiles { get; }
	}
}
