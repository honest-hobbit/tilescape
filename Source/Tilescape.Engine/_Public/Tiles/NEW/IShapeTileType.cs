﻿using Tilescape.Utility.Collections;

namespace Tilescape.Engine
{
	public interface IShapeTileType : ITileType
	{
		IListView<ITileShapeView> Shapes { get; }

		IListView<ITileColorPalette> ColorPalettes { get; }
	}
}
