﻿namespace Tilescape.Engine
{
	public interface IShapeTileList : ITileList<ShapeTile>
	{
		void Add(TileTypeKey type, TileShapeKey shape, TileColorPaletteKey colorPalette, OrthoAxis orientation);
	}
}
