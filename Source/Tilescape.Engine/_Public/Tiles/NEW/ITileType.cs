﻿using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public interface ITileType : IKeyed<TileTypeKey>, INamed
	{
		TileTypeClass TypeClass { get; }

		// higher value Ordering will overlap lower value Ordering
		int Ordering { get; }

		// this will be a Trait later
		bool IsFilled { get; }

		// TODO Traits? Updater?
	}
}
