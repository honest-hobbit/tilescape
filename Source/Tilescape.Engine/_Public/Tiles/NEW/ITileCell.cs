﻿namespace Tilescape.Engine
{
	public interface ITileCell : ITileCellView
	{
		new IShapeTileList ShapeTiles { get; }

		void Clear();
	}
}
