﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tilescape.Engine
{
	public struct ShapeTile
	{
		public TileTypeKey Type { get; set; }

		public TileShapeKey Shape { get; set; }

		public TileColorPaletteKey ColorPalette { get; set; }

		public OrthoAxis Orientation { get; set; }

		public void Set(TileShapeKey? shape = null, TileColorPaletteKey? colorPalette = null, OrthoAxis? orientation = null)
		{
		}
	}
}
