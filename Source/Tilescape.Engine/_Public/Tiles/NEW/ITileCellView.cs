﻿using Tilescape.Utility.Collections;

namespace Tilescape.Engine
{
	public interface ITileCellView
	{
		IListView<ShapeTile> ShapeTiles { get; }
	}
}
