﻿using Tilescape.Utility.Collections;

namespace Tilescape.Engine
{
	public interface ITileList<T> : IListView<T>
	{
		new T this[int index] { get; set; }

		void Add(T value);

		void Remove(int index);

		void Clear();
	}
}
