﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Unity.Types;
using UnityEngine;

namespace Tilescape.Engine
{
	public struct MeshQuadSlim
	{
		public MeshQuadSlim(byte aMin, byte aMax, byte bMin, byte bMax, byte c, Direction normal, byte colorIndex)
		{
			Require.That(aMin < aMax);
			Require.That(bMin < bMax);
			Require.That(Enumeration.IsDefined(normal));

			this.AMin = aMin;
			this.AMax = aMax;
			this.BMin = bMin;
			this.BMax = bMax;
			this.C = c;
			this.Normal = normal;
			this.ColorIndex = colorIndex;
		}

		public byte AMin { get; }

		public byte AMax { get; }

		public byte BMin { get; }

		public byte BMax { get; }

		public byte C { get; }

		public byte ColorIndex { get; }

		public Direction Normal { get; }

		public static MeshQuadSlim FacingNegativeX(byte yMin, byte yMax, byte zMin, byte zMax, byte x, byte colorIndex) =>
			new MeshQuadSlim(yMin, yMax, zMin, zMax, x, Direction.NegativeX, colorIndex);

		public static MeshQuadSlim FacingPositiveX(byte yMin, byte yMax, byte zMin, byte zMax, byte x, byte colorIndex) =>
			new MeshQuadSlim(yMin, yMax, zMin, zMax, x, Direction.PositiveX, colorIndex);

		public static MeshQuadSlim FacingNegativeY(byte xMin, byte xMax, byte zMin, byte zMax, byte y, byte colorIndex) =>
			new MeshQuadSlim(xMin, xMax, zMin, zMax, y, Direction.NegativeY, colorIndex);

		public static MeshQuadSlim FacingPositiveY(byte xMin, byte xMax, byte zMin, byte zMax, byte y, byte colorIndex) =>
			new MeshQuadSlim(xMin, xMax, zMin, zMax, y, Direction.PositiveY, colorIndex);

		public static MeshQuadSlim FacingNegativeZ(byte xMin, byte xMax, byte yMin, byte yMax, byte z, byte colorIndex) =>
			new MeshQuadSlim(xMin, xMax, yMin, yMax, z, Direction.NegativeZ, colorIndex);

		public static MeshQuadSlim FacingPositiveZ(byte xMin, byte xMax, byte yMin, byte yMax, byte z, byte colorIndex) =>
			new MeshQuadSlim(xMin, xMax, yMin, yMax, z, Direction.PositiveZ, colorIndex);

		public static MeshQuadSlim From(
			int aMin, int aMax, int bMin, int bMax, int c, Direction normal, byte colorIndex)
		{
			Require.That(aMin.IsIn(byte.MinValue, byte.MaxValue));
			Require.That(aMax.IsIn(byte.MinValue, byte.MaxValue));
			Require.That(bMin.IsIn(byte.MinValue, byte.MaxValue));
			Require.That(bMax.IsIn(byte.MinValue, byte.MaxValue));
			Require.That(c.IsIn(byte.MinValue, byte.MaxValue));

			return new MeshQuadSlim((byte)aMin, (byte)aMax, (byte)bMin, (byte)bMax, (byte)c, normal, colorIndex);
		}

		public MeshQuad Expand(OrthogonalTransformer transformer, IListView<float> lengths, IListView<Color32> colors)
		{
			Require.That(transformer != null);
			Require.That(lengths != null);
			Require.That(colors != null);

			float aMin = lengths[this.AMin];
			float aMax = lengths[this.AMax];
			float bMin = lengths[this.BMin];
			float bMax = lengths[this.BMax];
			float constant = lengths[this.C];

			// corners
			// z | 0   0   1   1  y
			//   |-----------------
			// 1 | 2 - 3   6 - 7		A - B
			//   | |   |   |   |		|   |
			// 0 | 0 - 1   4 - 5		C - D
			//---------------------
			// x   0   1   0   1
			Vector3 a, b, c, d, normal;
			switch (this.Normal)
			{
				case Direction.NegativeX:
					a = new Vector3(constant, aMax, bMax);  // corner 6
					b = new Vector3(constant, aMax, bMin);  // corner 4
					c = new Vector3(constant, aMin, bMax);  // corner 2
					d = new Vector3(constant, aMin, bMin);  // corner 0
					normal = Vector3.left;
					break;

				case Direction.PositiveX:
					a = new Vector3(constant, aMax, bMin);  // corner 5
					b = new Vector3(constant, aMax, bMax);  // corner 7
					c = new Vector3(constant, aMin, bMin);  // corner 1
					d = new Vector3(constant, aMin, bMax);  // corner 3
					normal = Vector3.right;
					break;

				case Direction.NegativeY:
					a = new Vector3(aMin, constant, bMin);  // corner 0
					b = new Vector3(aMax, constant, bMin);  // corner 1
					c = new Vector3(aMin, constant, bMax);  // corner 2
					d = new Vector3(aMax, constant, bMax);  // corner 3
					normal = Vector3.down;
					break;

				case Direction.PositiveY:
					a = new Vector3(aMin, constant, bMax);  // corner 6
					b = new Vector3(aMax, constant, bMax);  // corner 7
					c = new Vector3(aMin, constant, bMin);  // corner 4
					d = new Vector3(aMax, constant, bMin);  // corner 5
					normal = Vector3.up;
					break;

				case Direction.NegativeZ:
					a = new Vector3(aMin, bMax, constant);  // corner 4
					b = new Vector3(aMax, bMax, constant);  // corner 5
					c = new Vector3(aMin, bMin, constant);  // corner 0
					d = new Vector3(aMax, bMin, constant);  // corner 1
					normal = Vector3.back;
					break;

				case Direction.PositiveZ:
					a = new Vector3(aMax, bMax, constant);  // corner 7
					b = new Vector3(aMin, bMax, constant);  // corner 6
					c = new Vector3(aMax, bMin, constant);  // corner 3
					d = new Vector3(aMin, bMin, constant);  // corner 2
					normal = Vector3.forward;
					break;

				default: throw InvalidEnumArgument.CreateException(nameof(this.Normal), this.Normal);
			}

			a = transformer.Transform(a);
			b = transformer.Transform(b);
			c = transformer.Transform(c);
			d = transformer.Transform(d);
			normal = transformer.Transform(normal);
			var color = colors[this.ColorIndex];

			return transformer.IsFlipped ?
				new MeshQuad(b, a, d, c, normal, color) :
				new MeshQuad(a, b, c, d, normal, color);
		}
	}
}
