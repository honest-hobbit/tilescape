﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Pooling;
using Tilescape.Utility.Unity.Meshing;
using UnityEngine;

namespace Tilescape.Engine
{
	public class QuadMultiMeshBuilder : IListView<IMeshDefinition>
	{
		private readonly List<QuadMeshBuilder> builders = new List<QuadMeshBuilder>();

		private readonly IPool<QuadMeshBuilder> pool;

		public QuadMultiMeshBuilder()
			: this(Pool.CreateNew<QuadMeshBuilder>())
		{
		}

		public QuadMultiMeshBuilder(IPool<QuadMeshBuilder> pool)
		{
			Require.That(pool != null);

			this.pool = pool;
		}

		/// <inheritdoc />
		public int Count => this.builders.Count;

		/// <inheritdoc />
		public IMeshDefinition this[int index] => this.builders[index];

		public QuadMultiMeshBuilder Add(IListView<MeshQuad> mesh) => this.Add(mesh, Vector3.zero);

		public QuadMultiMeshBuilder Add(IListView<MeshQuad> mesh, Vector3 offset)
		{
			Require.That(mesh != null);

			if (mesh.Count == 0)
			{
				return this;
			}

			QuadMeshBuilder builder;
			if (this.builders.Count == 0)
			{
				builder = this.pool.Take();
				this.builders.Add(builder);
			}
			else
			{
				builder = this.builders[this.builders.Count - 1];
			}

			builder.Offset = offset;
			for (int index = 0; index < mesh.Count; index++)
			{
				while (!builder.TryAdd(mesh[index]))
				{
					builder = this.pool.Take();
					this.builders.Add(builder);
					builder.Offset = offset;
				}
			}

			return this;
		}

		public void Clear()
		{
			this.builders.ForEach(x =>
			{
				x.Clear();
				this.pool.TryGive(x);
			});

			this.builders.Clear();
		}

		public IMeshDefinition SingleOrEmpty()
		{
			Require.That(this.Count <= 1);

			return this.Count == 1 ? this[0] : MeshDefinition.Empty;
		}

		/// <inheritdoc />
		public IEnumerator<IMeshDefinition> GetEnumerator() => this.builders.Select(x => (IMeshDefinition)x).GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
