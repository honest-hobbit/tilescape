﻿using Tilescape.Utility.Unity.Meshing;
using UnityEngine;

namespace Tilescape.Engine
{
	public class QuadMeshBuilder : AbstractMeshBuilder
	{
		private static readonly int MaxCapacity = MeshConstants.MaxVertices / 4;

		public QuadMeshBuilder()
			: this(10)
		{
		}

		public QuadMeshBuilder(int initialQuadsCapacity)
			: base(initialQuadsCapacity * 6, 0)
		{
			int capacity = initialQuadsCapacity * 4;
			this.VerticesList.Capacity = capacity;
			this.NormalsList.Capacity = capacity;
			this.ColorsList.Capacity = capacity;
		}

		public static QuadMeshBuilder CreateWithMaxVertexCapacity() => new QuadMeshBuilder(MaxCapacity);

		public Vector3 Offset { get; set; } = Vector3.zero;

		public bool TryAdd(MeshQuad quad)
		{
			if (this.VerticesList.Count + 6 > MeshConstants.MaxVertices)
			{
				return false;
			}

			int index = this.VerticesList.Count;

			// C, A, B
			this.TrianglesList.Add(index + 2);
			this.TrianglesList.Add(index);
			this.TrianglesList.Add(index + 1);

			// B, D, C
			this.TrianglesList.Add(index + 1);
			this.TrianglesList.Add(index + 3);
			this.TrianglesList.Add(index + 2);

			// vertices
			this.VerticesList.Add(quad.A + this.Offset);
			this.VerticesList.Add(quad.B + this.Offset);
			this.VerticesList.Add(quad.C + this.Offset);
			this.VerticesList.Add(quad.D + this.Offset);

			this.NormalsList.Add(quad.Normal);
			this.NormalsList.Add(quad.Normal);
			this.NormalsList.Add(quad.Normal);
			this.NormalsList.Add(quad.Normal);

			this.ColorsList.Add(quad.Color);
			this.ColorsList.Add(quad.Color);
			this.ColorsList.Add(quad.Color);
			this.ColorsList.Add(quad.Color);

			return true;
		}
	}
}
