﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class AtlasExtensions
	{
		public static TileShapeData GetTileShapeData(this IDictionaryView<TileTypeKey, ITileTypeOLD> atlas, Tile tile)
		{
			Require.That(atlas != null);

			return atlas[tile.Type].GetShapeData(tile);
		}
	}
}
