﻿using System;
using System.IO;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.IO;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public class AtlasRecord<TValue> : IKeyed<Guid>
		where TValue : IKeyed<Guid>, INamed
	{
		public AtlasRecord(TValue value, string folderPath)
		{
			Require.That(value != null);
			Require.That(PathUtility.IsPathValid(folderPath));

			this.Value = value;
			this.FolderPath = Path.GetFullPath(folderPath);
		}

		/// <inheritdoc />
		public Guid Key => this.Value.Key;

		public TValue Value { get; }

		public string FolderPath { get; }

		/// <inheritdoc />
		public override string ToString() => $"{this.Value.Name} [{this.FolderPath}]";
	}
}
