﻿using System.IO;

namespace Tilescape.Engine
{
	public class TileShapeAtlasRecord : AtlasRecord<TileShape>
	{
		public TileShapeAtlasRecord(TileShape value, string folderPath)
			: base(value, folderPath)
		{
		}

		public TileShape Shape => this.Value;

		public string DataFilePath => Path.Combine(this.FolderPath, AtlasConstants.TileShapes.DataFileName);

		public string ShapeFilePath => Path.Combine(this.FolderPath, AtlasConstants.TileShapes.ShapeFileName);
	}
}
