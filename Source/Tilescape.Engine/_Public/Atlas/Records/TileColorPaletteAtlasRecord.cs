﻿using System.IO;

namespace Tilescape.Engine
{
	public class TileColorPaletteAtlasRecord : AtlasRecord<TileColorPalette>
	{
		public TileColorPaletteAtlasRecord(TileColorPalette value, string folderPath)
			: base(value, folderPath)
		{
		}

		public TileColorPalette Palette => this.Value;

		public string DataFilePath => Path.Combine(this.FolderPath, AtlasConstants.TileColorPalettes.DataFileName);

		public string PaletteFilePath => Path.Combine(this.FolderPath, AtlasConstants.TileColorPalettes.PaletteFileName);
	}
}
