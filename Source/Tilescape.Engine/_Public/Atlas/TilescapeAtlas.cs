﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Engine.Components;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	// TODO this class needs refactoring now that everything is internal/private
	public static class TilescapeAtlas
	{
		public static ITilescapeAtlas Create(
			TilescapeStoreComponent store,
			Func<IDictionaryView<Guid, ITileShapeView>, IDictionaryView<Guid, ITileColorPalette>, IEnumerable<ITileTypeOLD>> getTypes)
		{
			Require.That(store != null);
			Require.That(getTypes != null);

			return Create(store.Store, getTypes);
		}

		private static ITilescapeAtlas Create(
			ITilescapeStore store,
			Func<IDictionaryView<Guid, ITileShapeView>, IDictionaryView<Guid, ITileColorPalette>, IEnumerable<ITileTypeOLD>> getTypes)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(getTypes != null);

			return Create(store.Config.Stages, Shapes.Create(store), Palettes.Create(store), getTypes);
		}

		private static ITilescapeAtlas Create(
			ITileStageConfig config,
			IDictionaryView<Guid, ITileShapeView> shapesAtlas,
			IDictionaryView<Guid, ITileColorPalette> colorPalettes,
			Func<IDictionaryView<Guid, ITileShapeView>, IDictionaryView<Guid, ITileColorPalette>, IEnumerable<ITileTypeOLD>> getTypes)
		{
			Require.That(config != null);
			Require.That(shapesAtlas != null);
			Require.That(colorPalettes != null);
			Require.That(getTypes != null);

			var typesAtlas = Types.Create(getTypes(shapesAtlas, colorPalettes));
			return new TilescapeAtlasImpl(config, shapesAtlas, colorPalettes, typesAtlas);
		}

		private static IDictionaryView<Guid, T> ToDictionaryView<T>(this IEnumerable<T> values)
			where T : IKeyed<Guid>
		{
			Require.That(values.AllAndSelfNotNull());

			var result = new Dictionary<Guid, T>(Equality.StructComparer<Guid>());
			result.AddMany(values.Select(x => KeyValuePair.New(x.Key, x)));
			return result.AsDictionaryView();
		}

		private static class Shapes
		{
			public static IDictionaryView<Guid, ITileShapeView> Create(ITilescapeStore store)
			{
				Require.That(store != null);
				Require.That(!store.IsDisposed);

				return Create(store.Shapes.All());
			}

			public static IDictionaryView<Guid, ITileShapeView> Create(params ITileShapeView[] shapes) => shapes.ToDictionaryView();

			public static IDictionaryView<Guid, ITileShapeView> Create(IEnumerable<ITileShapeView> shapes) => shapes.ToDictionaryView();
		}

		private static class Palettes
		{
			public static IDictionaryView<Guid, ITileColorPalette> Create(ITilescapeStore store)
			{
				Require.That(store != null);
				Require.That(!store.IsDisposed);

				return Create(store.ColorPalettes.All());
			}

			public static IDictionaryView<Guid, ITileColorPalette> Create(params ITileColorPalette[] palettes) =>
				palettes.ToDictionaryView();

			public static IDictionaryView<Guid, ITileColorPalette> Create(IEnumerable<ITileColorPalette> palettes) =>
				palettes.ToDictionaryView();
		}

		private static class Types
		{
			public static IDictionaryView<TileTypeKey, ITileTypeOLD> Create(params ITileTypeOLD[] types) =>
				Create((IEnumerable<ITileTypeOLD>)types);

			public static IDictionaryView<TileTypeKey, ITileTypeOLD> Create(IEnumerable<ITileTypeOLD> types) =>
				new ArrayAtlas<TileTypeKey, ITileTypeOLD>(types);
		}

		private class TilescapeAtlasImpl : ITilescapeAtlas
		{
			public TilescapeAtlasImpl(
				ITileStageConfig config,
				IDictionaryView<Guid, ITileShapeView> shapes,
				IDictionaryView<Guid, ITileColorPalette> colorPalettes,
				IDictionaryView<TileTypeKey, ITileTypeOLD> types)
			{
				Require.That(config != null);
				Require.That(shapes != null);
				Require.That(colorPalettes != null);
				Require.That(types != null);

				this.Config = config;
				this.Shapes = shapes;
				this.ColorPalettes = colorPalettes;
				this.Types = types;
			}

			/// <inheritdoc />
			public ITileStageConfig Config { get; }

			/// <inheritdoc />
			public IDictionaryView<Guid, ITileShapeView> Shapes { get; }

			/// <inheritdoc />
			public IDictionaryView<Guid, ITileColorPalette> ColorPalettes { get; }

			/// <inheritdoc />
			public IDictionaryView<TileTypeKey, ITileTypeOLD> Types { get; }
		}
	}
}
