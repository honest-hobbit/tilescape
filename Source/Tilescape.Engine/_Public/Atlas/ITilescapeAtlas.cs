﻿using System;
using Tilescape.Utility.Collections;

namespace Tilescape.Engine
{
	public interface ITilescapeAtlas
	{
		ITileStageConfig Config { get; }

		IDictionaryView<Guid, ITileShapeView> Shapes { get; }

		IDictionaryView<Guid, ITileColorPalette> ColorPalettes { get; }

		IDictionaryView<TileTypeKey, ITileTypeOLD> Types { get; }
	}
}
