﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Mathematics;
using UnityEngine;

namespace Tilescape.Engine
{
	public static class DirectionExtensions
	{
		private const float MaxThreshold = .99f;

		private const float MinThreshold = 1f - MaxThreshold;

		public static Direction Next(this Direction value)
		{
			Require.That(Enumeration.IsDefined(value));

			int result = (int)value + 1;
			if (result >= 6)
			{
				result -= 6;
			}

			return (Direction)result;
		}

		public static Direction Previous(this Direction value)
		{
			Require.That(Enumeration.IsDefined(value));

			int result = (int)value - 1;
			if (result < 0)
			{
				result += 6;
			}

			return (Direction)result;
		}

		public static Direction Reverse(this Direction value)
		{
			Require.That(Enumeration.IsDefined(value));

			switch (value)
			{
				case Direction.PositiveX: return Direction.NegativeX;
				case Direction.NegativeX: return Direction.PositiveX;
				case Direction.PositiveY: return Direction.NegativeY;
				case Direction.NegativeY: return Direction.PositiveY;
				case Direction.PositiveZ: return Direction.NegativeZ;
				case Direction.NegativeZ: return Direction.PositiveZ;
				default: throw InvalidEnumArgument.CreateException(nameof(value), value);
			}
		}

		public static Direction ToDirection(this Vector3 vector)
		{
			if (vector.x < -MaxThreshold)
			{
				Require.That(vector.y.Abs() < MinThreshold);
				Require.That(vector.z.Abs() < MinThreshold);
				return Direction.NegativeX;
			}

			if (vector.x > MaxThreshold)
			{
				Require.That(vector.y.Abs() < MinThreshold);
				Require.That(vector.z.Abs() < MinThreshold);
				return Direction.PositiveX;
			}

			if (vector.y < -MaxThreshold)
			{
				Require.That(vector.x.Abs() < MinThreshold);
				Require.That(vector.z.Abs() < MinThreshold);
				return Direction.NegativeY;
			}

			if (vector.y > MaxThreshold)
			{
				Require.That(vector.x.Abs() < MinThreshold);
				Require.That(vector.z.Abs() < MinThreshold);
				return Direction.PositiveY;
			}

			if (vector.z < -MaxThreshold)
			{
				Require.That(vector.x.Abs() < MinThreshold);
				Require.That(vector.y.Abs() < MinThreshold);
				return Direction.NegativeZ;
			}

			if (vector.z > MaxThreshold)
			{
				Require.That(vector.x.Abs() < MinThreshold);
				Require.That(vector.y.Abs() < MinThreshold);
				return Direction.PositiveZ;
			}

			throw new ArgumentException("Must be orthogonal.", nameof(vector));
		}

		public static Vector3 ToVector(this Direction direction)
		{
			Require.That(Enumeration.IsDefined(direction));

			switch (direction)
			{
				case Direction.Left: return Vector3.left;
				case Direction.Right: return Vector3.right;
				case Direction.Down: return Vector3.down;
				case Direction.Up: return Vector3.up;
				case Direction.Back: return Vector3.back;
				case Direction.Forward: return Vector3.forward;
				default: throw InvalidEnumArgument.CreateException(nameof(direction), direction);
			}
		}
	}
}
