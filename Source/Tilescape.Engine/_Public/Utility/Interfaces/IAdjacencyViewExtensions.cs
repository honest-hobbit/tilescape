﻿using System;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class IAdjacencyViewExtensions
	{
		public static bool AllNotNull<T>(this IAdjacencyView<T> view)
			where T : class
		{
			Require.That(view != null);

			for (int index = 0; index < view.Length; index++)
			{
				if (view[index] == null)
				{
					return false;
				}
			}

			return true;
		}

		public static bool AllAssigned<T>(this IAdjacencyView<T> view)
			where T : struct, IEquatable<T>
		{
			Require.That(view != null);

			for (int index = 0; index < view.Length; index++)
			{
				if (view[index].Equals(default(T)))
				{
					return false;
				}
			}

			return true;
		}

		public static bool IsCrossNotNull<T>(this IAdjacencyView<T> view)
			where T : class
		{
			Require.That(view != null);

			return view[Adjacency.Center] != null
				&& view[Adjacency.NegX] != null && view[Adjacency.PosX] != null
				&& view[Adjacency.NegY] != null && view[Adjacency.PosY] != null
				&& view[Adjacency.NegZ] != null && view[Adjacency.PosZ] != null;
		}

		public static bool IsCrossAssigned<T>(this IAdjacencyView<T> view)
			where T : struct, IEquatable<T>
		{
			Require.That(view != null);

			return !view[Adjacency.Center].Equals(default(T))
				&& !view[Adjacency.NegX].Equals(default(T)) && !view[Adjacency.PosX].Equals(default(T))
				&& !view[Adjacency.NegY].Equals(default(T)) && !view[Adjacency.PosY].Equals(default(T))
				&& !view[Adjacency.NegZ].Equals(default(T)) && !view[Adjacency.PosZ].Equals(default(T));
		}
	}
}
