﻿using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	public interface ISquareArrayView<T> : IIndexableView<Index2D, T>
	{
		int SizeExponent { get; }

		int SideLength { get; }

		int Length { get; }

		T this[int index] { get; }

		T this[int x, int y] { get; }
	}
}
