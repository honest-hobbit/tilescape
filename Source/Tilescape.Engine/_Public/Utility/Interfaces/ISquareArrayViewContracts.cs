﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class ISquareArrayViewContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<T>(ISquareArrayView<T> instance, int index)
		{
			Require.That(instance != null);
			Require.That(index >= 0);
			Require.That(index < instance.Length);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<T>(ISquareArrayView<T> instance, int x, int y)
		{
			Require.That(instance != null);
			Require.That(x >= 0);
			Require.That(x < instance.SideLength);
			Require.That(y >= 0);
			Require.That(y < instance.SideLength);
		}
	}
}
