﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class ICubeArrayViewContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<T>(ICubeArrayView<T> instance, int index)
		{
			Require.That(instance != null);
			Require.That(index >= 0);
			Require.That(index < instance.Length);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<T>(ICubeArrayView<T> instance, int x, int y, int z)
		{
			Require.That(instance != null);
			Require.That(x >= 0);
			Require.That(x < instance.SideLength);
			Require.That(y >= 0);
			Require.That(y < instance.SideLength);
			Require.That(z >= 0);
			Require.That(z < instance.SideLength);
		}
	}
}
