﻿using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	public interface IAdjacencyView<T>
	{
		int Length { get; }

		T this[int index] { get; }

		T this[int x, int y, int z] { get; }

		T this[Index3D index] { get; }
	}
}
