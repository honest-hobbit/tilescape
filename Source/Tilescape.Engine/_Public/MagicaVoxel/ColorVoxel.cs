﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using UnityEngine;

namespace Tilescape.Engine
{
	public struct ColorVoxel : IEquatable<ColorVoxel>
	{
		public ColorVoxel(byte colorIndex)
		{
			this.ColorIndex = colorIndex;
		}

		public static ColorVoxel Empty => new ColorVoxel(0);

		public byte ColorIndex { get; }

		public bool IsEmpty => this.ColorIndex == 0;

		public static bool operator ==(ColorVoxel lhs, ColorVoxel rhs) => lhs.Equals(rhs);

		public static bool operator !=(ColorVoxel lhs, ColorVoxel rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public bool Equals(ColorVoxel other) => this.ColorIndex == other.ColorIndex;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => this.ColorIndex.GetHashCode();

		/// <inheritdoc />
		public override string ToString() => this.ColorIndex.ToString();

		public Color32 GetColor(IListView<Color32> colors)
		{
			Require.That(colors != null);
			Require.That(colors.Count == ColorPalette.RequirredLength);

			return colors[this.ColorIndex];
		}
	}
}
