﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using UnityEngine;

namespace Tilescape.Engine
{
	public class VoxelVolume
	{
		private readonly List<Frame> frames = new List<Frame>();

		private IListView<Color32> colors = MagicaVoxelImporter.DefaultColors;

		public VoxelVolume(Index3D dimensions)
		{
			Require.That(dimensions.X >= 0);
			Require.That(dimensions.Y >= 0);
			Require.That(dimensions.Z >= 0);

			this.Dimensions = dimensions;
			this.Frames = this.frames.AsListView();
		}

		public Index3D Dimensions { get; }

		public IListView<Color32> Colors
		{
			get => this.colors;
			set
			{
				Require.That(ColorPalette.IsValid(value));

				this.colors = value;
			}
		}

		public IListView<Frame> Frames { get; }

		public Color32 GetColor(ColorVoxel voxel)
		{
			Require.That(this.Colors != null);

			return voxel.GetColor(this.Colors);
		}

		public Frame AddFrame()
		{
			var frame = new Frame(this);
			this.frames.Add(frame);
			return frame;
		}

		public Frame GetOrAddFrame(int index)
		{
			Require.That(index >= 0);

			while (this.frames.Count <= index)
			{
				this.frames.Add(new Frame(this));
			}

			return this.frames[index];
		}

		public void ClearAllFrames() => this.frames.ForEach(x => x.Clear());

		public class Frame
		{
			public Frame(VoxelVolume volume)
			{
				Require.That(volume != null);

				this.Volume = volume;
				this.Voxels = new ColorVoxel[volume.Dimensions.X * volume.Dimensions.Y * volume.Dimensions.Z];
				this.Clear();
			}

			public VoxelVolume Volume { get; }

			public ColorVoxel[] Voxels { get; }

			public ColorVoxel this[int x, int y, int z]
			{
				get => this.Voxels[this.GetIndex(x, y, z)];
				set => this.Voxels[this.GetIndex(x, y, z)] = value;
			}

			public ColorVoxel this[Index3D index]
			{
				get => this.Voxels[this.GetIndex(index.X, index.Y, index.Z)];
				set => this.Voxels[this.GetIndex(index.X, index.Y, index.Z)] = value;
			}

			public Color32 GetColor(ColorVoxel voxel) => this.Volume.GetColor(voxel);

			public void Clear() => this.Voxels.SetAllTo(ColorVoxel.Empty);

			private int GetIndex(int x, int y, int z) => x + (this.Volume.Dimensions.X * (z + (this.Volume.Dimensions.Y * y)));
		}
	}
}
