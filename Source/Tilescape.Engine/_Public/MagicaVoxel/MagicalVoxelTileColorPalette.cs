﻿using System;

namespace Tilescape.Engine
{
	public static class MagicalVoxelTileColorPalette
	{
		public static ITileColorPalette Default { get; } = CreatePalette();

		private static ITileColorPalette CreatePalette()
		{
			var result = new TileColorPalette()
			{
				Name = "MagicaVoxel Default",
				Key = new Guid("113f2da6-a021-4a71-b633-b2ee17ce4e50")
			};

			for (int index = 0; index < ColorPalette.RequirredLength; index++)
			{
				result.Array[index] = MagicaVoxelImporter.DefaultColors[index];
			}

			return result;
		}
	}
}
