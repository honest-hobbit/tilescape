﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public static class ITileStageManagerContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void CreateStage(ITileStageManager instance, string name)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(!name.IsNullOrWhiteSpace());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void TryOpenStage(ITileStageManager instance, Guid key)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(key != Guid.Empty);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Submit(ITileStageManager instance, ITileJob job)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(job != null);
		}
	}
}
