﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Disposables;

namespace Tilescape.Engine
{
	public interface ITileStageManager : IVisiblyDisposable
	{
		ITilescapeAtlas Atlas { get; }

		IDictionaryView<Guid, ITileStage> Stages { get; }

		ITileStage CreateStage(string name);

		bool TryOpenStage(Guid key, out ITileStage stage);

		void Submit(ITileJob job);
	}
}
