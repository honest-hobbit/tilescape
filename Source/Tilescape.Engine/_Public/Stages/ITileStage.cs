﻿using System;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Interests;

namespace Tilescape.Engine
{
	public interface ITileStage : IKeyed<Guid>, IVisiblyDisposable
	{
		string Name { get; }

		ITileStageManager Manager { get; }

		IInterestMapView<Index3D, int> Map { get; }

		IDisposable SubscribeArea(IInterestArea<Index3D, int> area);
	}
}
