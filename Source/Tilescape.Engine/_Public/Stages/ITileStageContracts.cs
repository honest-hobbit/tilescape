﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Unity.Interests;

namespace Tilescape.Engine
{
	public static class ITileStageContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void SubscribeArea(ITileStage instance, IInterestArea<Index3D, int> area)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(area != null);
			Require.That(area.Interest == 1);
		}
	}
}
