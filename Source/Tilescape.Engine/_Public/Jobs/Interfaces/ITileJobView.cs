﻿using System;

namespace Tilescape.Engine
{
	public interface ITileJobView
	{
		ITilescapeAtlas Atlas { get; }

		ITileVolume this[Guid stageKey] { get; }
	}
}
