﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class ITileJobContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void RunInTilescape(ITileJobView view)
		{
			Require.That(view != null);
		}
	}
}
