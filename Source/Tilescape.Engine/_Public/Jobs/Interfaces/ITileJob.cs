﻿namespace Tilescape.Engine
{
	public interface ITileJob
	{
		void RunInTilescape(ITileJobView view);

		void FinishInUnity();
	}
}
