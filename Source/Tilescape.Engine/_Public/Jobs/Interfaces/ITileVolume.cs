﻿using System;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	public interface ITileVolume : IKeyed<Guid>
	{
		ITilescapeAtlas Atlas { get; }

		Tile this[Index3D tileIndex] { get; set; }
	}
}
