﻿using System;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	public class ReplaceSingleTileTask : ITileJob
	{
		public ReplaceSingleTileTask(Guid stageKey, Index3D tileIndex, Tile tile)
		{
			this.StageKey = stageKey;
			this.TileIndex = tileIndex;
			this.NewTile = tile;
		}

		public Guid StageKey { get; }

		public Index3D TileIndex { get; }

		public Tile OldTile { get; private set; } = Tile.Unassigned;

		public Tile NewTile { get; }

		/// <inheritdoc />
		public void RunInTilescape(ITileJobView view)
		{
			ITileJobContracts.RunInTilescape(view);

			var tiles = view[this.StageKey];
			this.OldTile = tiles[this.TileIndex];
			tiles[this.TileIndex] = this.NewTile;
		}

		/// <inheritdoc />
		public void FinishInUnity()
		{
			////Debug.Log($"Tile replaced. Index: {this.TileIndex.ToString()}   Old Tile: {this.OldTile.ToString()}   New Tile: {this.NewTile.ToString()}");
		}
	}
}
