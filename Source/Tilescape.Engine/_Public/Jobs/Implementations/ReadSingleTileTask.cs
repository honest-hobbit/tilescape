﻿using System;
using Tilescape.Utility.Indexing.Indices;
using UnityEngine;

namespace Tilescape.Engine
{
	public class ReadSingleTileTask : ITileJob
	{
		public ReadSingleTileTask(Guid stageKey, Index3D tileIndex)
		{
			this.StageKey = stageKey;
			this.TileIndex = tileIndex;
		}

		public Guid StageKey { get; }

		public Index3D TileIndex { get; }

		public Tile Tile { get; private set; } = Tile.Unassigned;

		/// <inheritdoc />
		public void RunInTilescape(ITileJobView view)
		{
			ITileJobContracts.RunInTilescape(view);

			this.Tile = view[this.StageKey][this.TileIndex];
		}

		/// <inheritdoc />
		public void FinishInUnity()
		{
			Debug.Log($"Tile read. Index: {this.TileIndex.ToString()} Tile: {this.Tile.ToString()}");
		}
	}
}
