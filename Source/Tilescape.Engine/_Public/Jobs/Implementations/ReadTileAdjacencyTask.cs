﻿using System;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	public class ReadTileAdjacencyTask : ITileJob, ITileUpdaterView
	{
		/// <inheritdoc />
		public Guid StageKey { get; set; }

		/// <inheritdoc />
		public Index3D TileIndex { get; set; }

		public AdjacencyCube<Tile> Tiles { get; set; }

		/// <inheritdoc />
		public ITilescapeAtlas Atlas { get; set; }

		/// <inheritdoc />
		IAdjacencyView<Tile> ITileUpdaterView.Tiles => this.Tiles;

		public event Action<ReadTileAdjacencyTask> Completed;

		/// <inheritdoc />
		public void RunInTilescape(ITileJobView view)
		{
			ITileJobContracts.RunInTilescape(view);

			if (this.Tiles == null)
			{
				this.Tiles = new AdjacencyCube<Tile>(Tile.Unassigned);
			}

			var tiles = view[this.StageKey];

			for (int index = 0; index < Adjacency.All.Count; index++)
			{
				var offset = Adjacency.All[index];
				this.Tiles[offset.AsInt] = tiles[this.TileIndex + offset.AsIndex];
			}
		}

		/// <inheritdoc />
		public void FinishInUnity()
		{
			this.Completed?.Invoke(this);
		}
	}
}
