﻿using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	public class CheckerboardTileChunkPopulator : ITileChunkPopulator
	{
		private readonly Tile whiteTile;

		private readonly Tile blackTile;

		private readonly Tile emptyTile;

		public CheckerboardTileChunkPopulator(Tile whiteTile, Tile blackTile, Tile emptyTile)
		{
			this.whiteTile = whiteTile;
			this.blackTile = blackTile;
			this.emptyTile = emptyTile;
		}

		/// <inheritdoc />
		public void Populate(ITileChunk chunk)
		{
			ITileChunkPopulatorContracts.Populate(chunk);

			if (chunk.Key.Index.Y != 0)
			{
				chunk.UniformTile = this.emptyTile;
				return;
			}

			int sideLength = chunk.SideLength;
			int floorHeight = (chunk.Key.Index.X + chunk.Key.Index.Z).IsEven() ? 1 : 2;
			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iZ = 0; iZ < sideLength; iZ++)
				{
					int iY = 0;
					for (; iY < floorHeight; iY++)
					{
						chunk.Tiles[iX, iY, iZ] = (iX + iZ).IsEven() ? this.whiteTile : this.blackTile;
					}

					for (; iY < sideLength; iY++)
					{
						chunk.Tiles[iX, iY, iZ] = this.emptyTile;
					}
				}
			}
		}
	}
}
