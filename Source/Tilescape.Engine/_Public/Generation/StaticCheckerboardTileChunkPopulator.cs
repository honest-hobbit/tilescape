﻿using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Procedural;

namespace Tilescape.Engine
{
	public class StaticCheckerboardTileChunkPopulator : ITileChunkPopulator
	{
		private readonly double chanceFrequency = 13741.121;

		private readonly SimplexNoise noise;

		private readonly Tile staticTile;

		private readonly Tile whiteTile;

		private readonly Tile blackTile;

		private readonly Tile emptyTile;

		public StaticCheckerboardTileChunkPopulator(int seed, Tile staticTile, Tile whiteTile, Tile blackTile, Tile emptyTile)
		{
			this.noise = new SimplexNoise(seed);
			this.staticTile = staticTile;
			this.whiteTile = whiteTile;
			this.blackTile = blackTile;
			this.emptyTile = emptyTile;
		}

		/// <inheritdoc />
		public void Populate(ITileChunk chunk)
		{
			ITileChunkPopulatorContracts.Populate(chunk);

			if (chunk.Key.Index.Y != 0)
			{
				chunk.UniformTile = this.emptyTile;
				return;
			}

			var lowerBounds = chunk.StageTileIndexOfLowerBounds();
			int sideLength = chunk.SideLength;
			int floorHeight = (chunk.Key.Index.X + chunk.Key.Index.Z).IsEven() ? 1 : 2;

			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iZ = 0; iZ < sideLength; iZ++)
				{
					int iY = 0;
					for (; iY < floorHeight; iY++)
					{
						chunk.Tiles[iX, iY, iZ] = (iX + iZ).IsEven() ? this.whiteTile : this.blackTile;
					}

					for (; iY < sideLength; iY++)
					{
						double random = (this.noise.Noise(
							(lowerBounds.X + iX) * this.chanceFrequency,
							iY * this.chanceFrequency,
							(lowerBounds.Z + iZ) * this.chanceFrequency) * 50) + 50;

						if (random < ((double)sideLength - iY) / sideLength * 75.0)
						{
							chunk.Tiles[iX, iY, iZ] = this.staticTile;
						}
						else
						{
							chunk.Tiles[iX, iY, iZ] = this.emptyTile;
						}
					}
				}
			}
		}
	}
}
