﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	public static class ITileChunkPopulatorContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Populate(ITileChunk chunk)
		{
			Require.That(chunk != null);
			Require.That(chunk.Tiles != null);
		}
	}
}
