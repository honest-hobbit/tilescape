﻿namespace Tilescape.Engine
{
	public interface ITileChunkPopulator
	{
		void Populate(ITileChunk chunk);
	}
}
