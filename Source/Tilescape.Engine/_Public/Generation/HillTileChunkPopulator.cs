﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Procedural;

namespace Tilescape.Engine
{
	public class HillTileChunkPopulator : ITileChunkPopulator
	{
		private readonly double frequency = .015;

		private readonly double amplitude = 5;

		private readonly double chanceFrequency = 13741.121;

		private readonly SimplexNoise noise;

		private readonly Tile emptyTile;

		private readonly Tile baseTile;

		private readonly KeyValuePair<double, Tile>[] chanceTiles;

		public HillTileChunkPopulator(
			int seed, Tile emptyTile, Tile baseTile, params KeyValuePair<double, Tile>[] chanceTiles)
		{
			Require.That(chanceTiles != null);

			this.noise = new SimplexNoise(seed);
			this.emptyTile = emptyTile;
			this.baseTile = baseTile;
			this.chanceTiles = chanceTiles;
		}

		/// <inheritdoc />
		public void Populate(ITileChunk chunk)
		{
			ITileChunkPopulatorContracts.Populate(chunk);

			// TODO - this class is not robustly designed at all
			// maybe frequency/amplitude should be passed in and these Y heights should be calculated?
			if (chunk.Key.Index.Y >= 1)
			{
				chunk.UniformTile = this.emptyTile;
				return;
			}

			if (chunk.Key.Index.Y <= -2)
			{
				chunk.UniformTile = this.baseTile;
				return;
			}

			var lowerBounds = chunk.StageTileIndexOfLowerBounds();
			int sideLength = chunk.SideLength;
			bool hasEmpty = false;
			bool hasBase = false;
			bool hasRandom = false;

			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iZ = 0; iZ < sideLength; iZ++)
				{
					int hillHeight = (this.noise.Noise(
						(lowerBounds.X + iX) * this.frequency, (lowerBounds.Z + iZ) * this.frequency) * this.amplitude).ClampToInt();

					for (int iY = 0; iY < sideLength; iY++)
					{
						var tile = this.emptyTile;
						var height = lowerBounds.Y + iY;

						if (height < hillHeight)
						{
							tile = this.baseTile;
							hasBase = true;
						}
						else if (height == hillHeight)
						{
							tile = this.baseTile;
							hasBase = true;

							double chance = 0;
							double random = (this.noise.Noise(
								(lowerBounds.X + iX) * this.chanceFrequency, (lowerBounds.Z + iZ) * this.chanceFrequency) * 50) + 50;

							for (int index = 0; index < this.chanceTiles.Length; index++)
							{
								chance += this.chanceTiles[index].Key;
								if (random < chance)
								{
									tile = this.chanceTiles[index].Value;
									hasRandom = true;
									break;
								}
							}

							////chance += 5;
							////if (tile == this.baseTile && random < chance)
							////{
							////	tile = tile.Set(new ColorPaletteKey(1));
							////}
						}
						else
						{
							hasEmpty = true;
						}

						chunk.Tiles[iX, iY, iZ] = tile;
					}
				}
			}

			if (hasEmpty && !hasBase && !hasRandom)
			{
				chunk.UniformTile = this.emptyTile;
				return;
			}

			if (!hasEmpty && hasBase && !hasRandom)
			{
				chunk.UniformTile = this.baseTile;
				return;
			}
		}
	}
}
