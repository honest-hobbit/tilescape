﻿namespace Tilescape.Engine
{
	public class UniformTileChunkPopulator : ITileChunkPopulator
	{
		private readonly Tile tileValue;

		public UniformTileChunkPopulator(Tile tileValue)
		{
			this.tileValue = tileValue;
		}

		/// <inheritdoc />
		public void Populate(ITileChunk chunk)
		{
			ITileChunkPopulatorContracts.Populate(chunk);

			chunk.UniformTile = this.tileValue;
		}
	}
}
