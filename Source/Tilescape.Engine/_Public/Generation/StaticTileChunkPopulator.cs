﻿using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Procedural;

namespace Tilescape.Engine
{
	public class StaticTileChunkPopulator : ITileChunkPopulator
	{
		private readonly double chanceFrequency = 13741.121;

		private readonly SimplexNoise noise;

		private readonly Tile staticTile;

		private readonly Tile emptyTile;

		public StaticTileChunkPopulator(int seed, Tile staticTile, Tile emptyTile)
		{
			this.noise = new SimplexNoise(seed);
			this.staticTile = staticTile;
			this.emptyTile = emptyTile;
		}

		/// <inheritdoc />
		public void Populate(ITileChunk chunk)
		{
			ITileChunkPopulatorContracts.Populate(chunk);

			if (chunk.Key.Index.Y < -2 || chunk.Key.Index.Y > 1)
			{
				chunk.UniformTile = this.emptyTile;
				return;
			}

			var lowerBounds = chunk.StageTileIndexOfLowerBounds();
			int sideLength = chunk.SideLength;
			double doubleLength = sideLength * 2;

			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iZ = 0; iZ < sideLength; iZ++)
				{
					for (int iY = 0; iY < sideLength; iY++)
					{
						double random = (this.noise.Noise(
							(lowerBounds.X + iX) * this.chanceFrequency,
							(lowerBounds.Y + iY) * this.chanceFrequency,
							(lowerBounds.Z + iZ) * this.chanceFrequency) * 50) + 50;

						var threshold = (1.0 - ((lowerBounds.Y + iY) / doubleLength).Abs()) * 60.0;
						if (random < threshold)
						{
							chunk.Tiles[iX, iY, iZ] = this.staticTile;
						}
						else
						{
							chunk.Tiles[iX, iY, iZ] = this.emptyTile;
						}
					}
				}
			}
		}
	}
}
