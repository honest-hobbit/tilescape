﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	public class FlatTileChunkPopulator : ITileChunkPopulator
	{
		private readonly Tile tile;

		private readonly Tile emptyTile;

		private readonly int thickness;

		public FlatTileChunkPopulator(Tile tile, Tile emptyTile, int thickness = 1)
		{
			Require.That(thickness >= 0);

			this.tile = tile;
			this.emptyTile = emptyTile;
			this.thickness = thickness;
		}

		/// <inheritdoc />
		public void Populate(ITileChunk chunk)
		{
			ITileChunkPopulatorContracts.Populate(chunk);

			if (chunk.Key.Index.Y != 0)
			{
				chunk.UniformTile = this.emptyTile;
				return;
			}

			int sideLength = chunk.SideLength;
			int yMax = this.thickness.ClampUpper(sideLength);

			if (yMax == sideLength)
			{
				chunk.UniformTile = this.tile;
			}

			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iZ = 0; iZ < sideLength; iZ++)
				{
					int iY = 0;
					for (; iY < yMax; iY++)
					{
						chunk.Tiles[iX, iY, iZ] = this.tile;
					}

					for (; iY < sideLength; iY++)
					{
						chunk.Tiles[iX, iY, iZ] = this.emptyTile;
					}
				}
			}
		}
	}
}
