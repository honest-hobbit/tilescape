﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Stores;
using Tilescape.Utility.Unity.Types.Serialization;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class TileColorPaletteStore : AbstractAtlasStore<TileColorPalette, TileColorPaletteEntity>
	{
		public TileColorPaletteStore(ITilescapeStore parent, IStore store)
			: base(parent, store)
		{
		}

		/// <inheritdoc />
		protected override IConfigKey<DateTime> LastUpdatedConfigKey => TilescapeConfigKey.TileColorPaletteAtlasLastUpdated;

		private static IInlineSerDes<Color32[]> ColorsSerializer { get; } =
			new ArraySerializer<Color32>(Color32Serializer.RGBA, SerializeCount.AsConstant(ColorPalette.RequirredLength));

		/// <inheritdoc />
		protected override void Serialize(TileColorPalette palette)
		{
			Require.That(palette != null);
			Require.That(palette.Key != Guid.Empty);

			var entity = palette.Entity;
			if (entity == null)
			{
				entity = new TileColorPaletteEntity();
				palette.Entity = entity;
			}

			entity.KeyGuid = palette.Key;
			entity.Name = palette.Name;
			entity.Colors = ColorsSerializer.Serialize(palette.Array);
		}

		/// <inheritdoc />
		protected override TileColorPalette Deserialize(TileColorPaletteEntity entity)
		{
			Require.That(entity != null);
			Require.That(entity.Key != null);
			Require.That(entity.Name != null);
			Require.That(entity.Colors != null);

			var palette = new TileColorPalette()
			{
				Entity = entity,
				Key = entity.KeyGuid,
				Name = entity.Name
			};

			ColorsSerializer.DeserializeInline(entity.Colors, palette.Array);

			this.ClearData(entity);
			return palette;
		}

		/// <inheritdoc />
		protected override void ClearData(TileColorPaletteEntity entity)
		{
			Require.That(entity != null);

			entity.Colors = null;
		}

		/// <inheritdoc />
		protected override TileColorPaletteEntity GetEntity(TileColorPalette palette)
		{
			Require.That(palette != null);
			Require.That(palette.Entity != null);

			return palette.Entity;
		}
	}
}
