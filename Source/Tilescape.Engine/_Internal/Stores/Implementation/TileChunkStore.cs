﻿using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Stores;

namespace Tilescape.Engine
{
	internal class TileChunkStore : ITileChunkStore
	{
		private readonly IStore store;

		public TileChunkStore(ITilescapeStore parent, IStore store)
		{
			Require.That(parent != null);
			Require.That(store != null);
			Require.That(!store.IsDisposed);

			this.Store = parent;
			this.store = store;
		}

		/// <inheritdoc />
		public ITilescapeStore Store { get; }

		/// <inheritdoc />
		public bool IsDisposed => this.Store.IsDisposed;

		/// <inheritdoc />
		public IInlineSerDes<ITileChunk> Serializer => TileChunkSerializer.RunLengthEncoded;

		/// <inheritdoc />
		public void Add(IEnumerable<TileChunkEntity> chunks)
		{
			ITileChunkStoreContracts.Add(this, chunks);

			// TODO re-enable this at some point
			////this.store.AddMany(chunks.Select(x => (TileChunkEntity)x));
		}

		/// <inheritdoc />
		public void AddOrUpdate(IEnumerable<TileChunkEntity> chunks)
		{
			ITileChunkStoreContracts.AddOrUpdate(this, chunks);

			// TODO re-enable this at some point
			////this.store.AddOrUpdateMany(chunks.Select(x => (TileChunkEntity)x));
		}

		/// <inheritdoc />
		public TileChunkEntity Get(TileChunkKey key)
		{
			ITileChunkStoreContracts.Get(this);

			byte[] stageKey = key.StageKey.ToByteArray();
			int x = key.Index.X;
			int y = key.Index.Y;
			int z = key.Index.Z;

			return this.store.Where((TileChunkEntity entity) =>
				entity.TileStageKey == stageKey && entity.X == x && entity.Y == y && entity.Z == z)
				.SingleOrDefault() ?? new TileChunkEntity() { ChunkKey = key };
		}
	}
}
