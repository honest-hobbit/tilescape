﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Stores;
using Tilescape.Utility.Stores.SQLite;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal abstract class AbstractAtlasStore<TValue, TEntity> : IAtlasStore<Guid, TValue>
		where TValue : IKeyed<Guid>, INamed
		where TEntity : class, IKeyed<byte[]>, INamed, new()
	{
		private readonly IStore store;

		private Try<DateTime> lastUpdated = Try.None<DateTime>();

		public AbstractAtlasStore(ITilescapeStore parent, IStore store)
		{
			Require.That(parent != null);
			Require.That(!parent.IsDisposed);
			Require.That(store != null);
			Require.That(!store.IsDisposed);

			this.Stores = parent;
			this.store = store;
		}

		/// <inheritdoc />
		public ITilescapeStore Stores { get; }

		/// <inheritdoc />
		public bool IsDisposed => this.Stores.IsDisposed;

		/// <inheritdoc />
		public DateTime LastUpdated
		{
			get
			{
				IAtlasStoreContracts.LastUpdated(this);

				if (this.lastUpdated.HasValue)
				{
					return this.lastUpdated.Value;
				}
				else
				{
					this.lastUpdated = this.store.Config().GetOrAddDefault(this.LastUpdatedConfigKey, DateTime.MinValue);
					return this.lastUpdated.Value;
				}
			}
		}

		protected abstract IConfigKey<DateTime> LastUpdatedConfigKey { get; }

		/// <inheritdoc />
		public IEnumerable<TValue> All()
		{
			IAtlasStoreContracts.All(this);

			return this.store.All<TEntity>().Select(this.Deserialize);
		}

		/// <inheritdoc />
		public IEnumerable<TValue> Find(string name)
		{
			IAtlasStoreContracts.Find(this);

			return this.store.Where<TEntity>(x => x.Name == name).Select(this.Deserialize);
		}

		/// <inheritdoc />
		public bool TryGet(Guid key, out TValue value)
		{
			IAtlasStoreContracts.TryGet(this);

			if (this.store.TryGet(key.ToByteArray(), out TEntity entity))
			{
				value = this.Deserialize(entity);
				return true;
			}
			else
			{
				value = default(TValue);
				return false;
			}
		}

		/// <inheritdoc />
		public void AddOrUpdate(TValue value)
		{
			IAtlasStoreContracts.AddOrUpdate(this, value);

			this.Serialize(value);

			this.store.RunInTransaction(store =>
			{
				var entity = this.GetEntity(value);

				store.AddOrUpdate(entity);
				this.SetLastUpdated(store);

				this.ClearData(entity);
			});
		}

		/// <inheritdoc />
		public void AddOrUpdateMany(IEnumerable<TValue> values)
		{
			IAtlasStoreContracts.AddOrUpdateMany(this, values);

			if (values.IsEmpty())
			{
				return;
			}

			values.ForEach(this.Serialize);

			this.store.RunInTransaction(store =>
			{
				store.AddOrUpdateMany(values.Select(this.GetEntity));
				this.SetLastUpdated(store);
			});

			values.Select(this.GetEntity).ForEach(this.ClearData);
		}

		/// <inheritdoc />
		public void Remove(Guid key)
		{
			IAtlasStoreContracts.Remove(this);

			this.store.RunInTransaction(store =>
			{
				store.RemoveKey<byte[], TEntity>(key.ToByteArray());
				this.SetLastUpdated(store);
			});
		}

		/// <inheritdoc />
		public void RemoveMany(IEnumerable<Guid> keys)
		{
			IAtlasStoreContracts.RemoveMany(this, keys);

			if (keys.IsEmpty())
			{
				return;
			}

			this.store.RunInTransaction(store =>
			{
				store.RemoveManyKeys<byte[], TEntity>(keys.Select(x => x.ToByteArray()));
				this.SetLastUpdated(store);
			});
		}

		/// <inheritdoc />
		public void DeleteAtlas()
		{
			IAtlasStoreContracts.DeleteAtlas(this);

			this.store.RunInTransaction(store =>
			{
				store.RemoveAll<TEntity>();
				store.Config().Remove(this.LastUpdatedConfigKey);
				this.lastUpdated = Try.None<DateTime>();
			});
		}

		protected abstract void Serialize(TValue value);

		// this should call ClearData at the end to free up memory
		protected abstract TValue Deserialize(TEntity entity);

		// this will only be called after calling Serialize and should never return null
		protected abstract TEntity GetEntity(TValue value);

		protected abstract void ClearData(TEntity entity);

		private void SetLastUpdated(IStore store)
		{
			Require.That(store != null);

			this.lastUpdated = DateTime.UtcNow;
			store.Config().AddOrUpdate(this.LastUpdatedConfigKey, this.lastUpdated.Value);
		}
	}
}
