﻿using System;
using Tilescape.Utility.Stores;

namespace Tilescape.Engine
{
	internal static class TilescapeConfigKey
	{
		public static IConfigKey<int> TileSizeExponent { get; } = ConfigKey.ForInt(nameof(TileSizeExponent));

		public static IConfigKey<int> TileChunkSizeExponent { get; } = ConfigKey.ForInt(nameof(TileChunkSizeExponent));

		public static IConfigKey<int> TileShapeColliderFillThreshold { get; } =
			ConfigKey.ForInt(nameof(TileShapeColliderFillThreshold));

		public static IConfigKey<DateTime> TileShapeAtlasLastUpdated { get; } =
			ConfigKey.ForDateTimeISO(nameof(TileShapeAtlasLastUpdated));

		public static IConfigKey<DateTime> TileColorPaletteAtlasLastUpdated { get; } =
			ConfigKey.ForDateTimeISO(nameof(TileColorPaletteAtlasLastUpdated));
	}
}
