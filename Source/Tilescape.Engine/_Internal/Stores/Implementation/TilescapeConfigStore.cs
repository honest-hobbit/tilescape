﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Stores;
using Tilescape.Utility.Stores.SQLite;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class TilescapeConfigStore : ITilescapeConfigStore
	{
		private readonly IStore store;

		private Try<int> tileShapeColliderFillThreshold = Try.None<int>();

		public TilescapeConfigStore(ITilescapeStore parent, IStore store, ITileStageConfig config)
		{
			Require.That(parent != null);
			Require.That(!parent.IsDisposed);
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(config != null);

			this.Stores = parent;
			this.store = store;
			this.Stages = config;
		}

		/// <inheritdoc />
		public bool IsDisposed => this.store.IsDisposed;

		/// <inheritdoc />
		public ITilescapeStore Stores { get; }

		/// <inheritdoc />
		public ITileStageConfig Stages { get; }

		/// <inheritdoc />
		public int TileShapeColliderFillThreshold
		{
			get
			{
				ITilescapeConfigStoreContracts.TileShapeColliderFillThresholdGetter(this);

				if (this.tileShapeColliderFillThreshold.HasValue)
				{
					return this.tileShapeColliderFillThreshold.Value;
				}
				else
				{
					this.tileShapeColliderFillThreshold = this.store.Config().GetOrAddDefault(
						TilescapeConfigKey.TileShapeColliderFillThreshold, this.Stages.TileLengthInVoxels / 2);
					return this.tileShapeColliderFillThreshold.Value;
				}
			}

			set
			{
				ITilescapeConfigStoreContracts.TileShapeColliderFillThresholdSetter(this, value);

				if (this.tileShapeColliderFillThreshold != value)
				{
					this.tileShapeColliderFillThreshold = value;
					this.store.Config().AddOrUpdate(TilescapeConfigKey.TileShapeColliderFillThreshold, value);
				}
			}
		}
	}
}
