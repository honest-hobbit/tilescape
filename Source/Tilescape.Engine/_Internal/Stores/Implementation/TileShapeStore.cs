﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Stores;

namespace Tilescape.Engine
{
	internal class TileShapeStore : AbstractAtlasStore<TileShape, TileShapeEntity>
	{
		public TileShapeStore(ITilescapeStore parent, IStore store)
			: base(parent, store)
		{
		}

		/// <inheritdoc />
		protected override IConfigKey<DateTime> LastUpdatedConfigKey => TilescapeConfigKey.TileShapeAtlasLastUpdated;

		private static IInlineSerDes<ColorVoxel[]> VoxelsSerializer => ColorVoxelArraySerializer.RunLengthEncoded;

		private static IInlineSerDes<TileColliders> CollidersSerializer => TileCollidersSerializer.Instance;

		private static IInlineSerDes<TileMeshes> MeshesSerializer => TileMeshesSerializer.Instance;

		/// <inheritdoc />
		protected override void Serialize(TileShape tileShape)
		{
			Require.That(tileShape != null);
			Require.That(tileShape.Key != Guid.Empty);

			var entity = tileShape.Entity;
			if (entity == null)
			{
				entity = new TileShapeEntity();
				tileShape.Entity = entity;
			}

			entity.KeyGuid = tileShape.Key;
			entity.Name = tileShape.Name;
			entity.Voxels = VoxelsSerializer.Serialize(tileShape.Voxels.Array);
			entity.Colliders = CollidersSerializer.Serialize(tileShape.Colliders);
			entity.Meshes = MeshesSerializer.Serialize(tileShape.Meshes);
		}

		/// <inheritdoc />
		protected override TileShape Deserialize(TileShapeEntity entity)
		{
			Require.That(entity != null);
			Require.That(entity.Key != null);
			Require.That(entity.Name != null);
			Require.That(entity.Voxels != null);
			Require.That(entity.Colliders != null);
			Require.That(entity.Meshes != null);

			var tileShape = new TileShape(this.Stores.Config.Stages.TileSizeExponent)
			{
				Entity = entity,
				Key = entity.KeyGuid,
				Name = entity.Name
			};

			VoxelsSerializer.DeserializeInline(entity.Voxels, tileShape.Voxels.Array);
			CollidersSerializer.DeserializeInline(entity.Colliders, tileShape.Colliders);
			MeshesSerializer.DeserializeInline(entity.Meshes, tileShape.Meshes);

			this.ClearData(entity);
			return tileShape;
		}

		/// <inheritdoc />
		protected override void ClearData(TileShapeEntity entity)
		{
			Require.That(entity != null);

			entity.Voxels = null;
			entity.Colliders = null;
			entity.Meshes = null;
		}

		/// <inheritdoc />
		protected override TileShapeEntity GetEntity(TileShape tileShape)
		{
			Require.That(tileShape != null);
			Require.That(tileShape.Entity != null);

			return tileShape.Entity;
		}
	}
}
