﻿using System;
using SQLite;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	[Table("TileColorPalettes")]
	internal class TileColorPaletteEntity : IKeyed<byte[]>, INamed
	{
		[Ignore]
		public Guid KeyGuid
		{
			get => GuidUtility.FromNullableArray(this.Key);
			set
			{
				if (value != this.KeyGuid)
				{
					this.Key = value.ToByteArray();
				}
			}
		}

		/// <inheritdoc />
		[PrimaryKey]
		public byte[] Key { get; set; }

		/// <inheritdoc />
		public string Name { get; set; }

		public byte[] Colors { get; set; }
	}
}
