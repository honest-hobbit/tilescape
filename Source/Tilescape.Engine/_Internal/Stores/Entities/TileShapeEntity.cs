﻿using System;
using SQLite;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	[Table("TileShapes")]
	internal class TileShapeEntity : IKeyed<byte[]>, INamed
	{
		[Ignore]
		public Guid KeyGuid
		{
			get => GuidUtility.FromNullableArray(this.Key);
			set
			{
				if (value != this.KeyGuid)
				{
					this.Key = value.ToByteArray();
				}
			}
		}

		/// <inheritdoc />
		[PrimaryKey]
		public byte[] Key { get; set; }

		/// <inheritdoc />
		public string Name { get; set; }

		public byte[] Voxels { get; set; }

		public byte[] Colliders { get; set; }

		public byte[] Meshes { get; set; }
	}
}
