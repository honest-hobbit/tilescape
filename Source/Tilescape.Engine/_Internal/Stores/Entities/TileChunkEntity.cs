﻿using System;
using SQLite;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	[Table("TileChunks")]
	internal class TileChunkEntity : IKeyed<byte[]>
	{
		[Ignore]
		public Guid KeyGuid
		{
			get => GuidUtility.FromNullableArray(this.Key);
			set
			{
				if (value != this.KeyGuid)
				{
					this.Key = value.ToByteArray();
				}
			}
		}

		/// <inheritdoc />
		[PrimaryKey]
		public byte[] Key { get; set; }

		[Indexed]
		public byte[] TileStageKey { get; set; }

		[Indexed]
		public int X { get; set; }

		[Indexed]
		public int Y { get; set; }

		[Indexed]
		public int Z { get; set; }

		[Ignore]
		public TileChunkKey ChunkKey
		{
			get => new TileChunkKey(GuidUtility.FromNullableArray(this.TileStageKey), new Index3D(this.X, this.Y, this.Z));

			set
			{
				if (value.StageKey != GuidUtility.FromNullableArray(this.TileStageKey))
				{
					this.TileStageKey = GuidUtility.ToNullableByteArray(value.StageKey);
				}

				this.X = value.Index.X;
				this.Y = value.Index.Y;
				this.Z = value.Index.Z;
			}
		}

		public byte[] Data { get; set; }
	}
}
