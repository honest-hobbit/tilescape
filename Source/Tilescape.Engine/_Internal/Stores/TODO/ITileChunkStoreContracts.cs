﻿using System.Collections.Generic;
using System.Diagnostics;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class ITileChunkStoreContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Add(ITileChunkStore instance, IEnumerable<TileChunkEntity> chunks)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(chunks.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void AddOrUpdate(ITileChunkStore instance, IEnumerable<TileChunkEntity> chunks)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(chunks.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Get(ITileChunkStore instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}
	}
}
