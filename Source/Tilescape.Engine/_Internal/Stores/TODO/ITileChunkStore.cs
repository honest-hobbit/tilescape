﻿using System.Collections.Generic;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	internal interface ITileChunkStore : IDisposed
	{
		ITilescapeStore Store { get; }

		IInlineSerDes<ITileChunk> Serializer { get; }

		void Add(IEnumerable<TileChunkEntity> chunks);

		void AddOrUpdate(IEnumerable<TileChunkEntity> chunks);

		TileChunkEntity Get(TileChunkKey key);
	}
}
