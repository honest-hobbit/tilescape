﻿using System;
using System.IO;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.IO;
using Tilescape.Utility.Stores;
using Tilescape.Utility.Stores.SQLite;

namespace Tilescape.Engine
{
	internal static class TilescapeStore
	{
		public static ITilescapeStore Open(string databasePath)
		{
			Require.That(File.Exists(databasePath));

			var store = SQLiteStore.Open(databasePath);
			store = new LockedStore(store);

			var config = new TileStageConfig(
				store.Config().Get(TilescapeConfigKey.TileSizeExponent),
				store.Config().Get(TilescapeConfigKey.TileChunkSizeExponent));

			return new TilescapeStoreImpl(store, config);
		}

		public static ITilescapeStore Create(string databasePath, ITileStageConfig config)
		{
			Require.That(PathUtility.IsPathValid(databasePath));
			Require.That(config != null);

			var store = SQLiteStore.Create(
				databasePath,
				typeof(SQLiteConfigEntity),
				typeof(TileShapeEntity),
				typeof(TileColorPaletteEntity),
				typeof(TileChunkEntity));
			store = new LockedStore(store);

			var resultingConfig = new TileStageConfig(
				store.Config().GetOrAddDefault(TilescapeConfigKey.TileSizeExponent, config.TileSizeExponent),
				store.Config().GetOrAddDefault(TilescapeConfigKey.TileChunkSizeExponent, config.ChunkSizeExponent));

			return new TilescapeStoreImpl(store, resultingConfig);
		}

		private class TilescapeStoreImpl : AbstractDisposable, ITilescapeStore
		{
			private readonly IStore store;

			public TilescapeStoreImpl(IStore store, ITileStageConfig config)
			{
				Require.That(store != null);
				Require.That(config != null);

				this.store = store;
				this.Config = new TilescapeConfigStore(this, store, config);
				this.Shapes = new TileShapeStore(this, store);
				this.ColorPalettes = new TileColorPaletteStore(this, store);
				this.Chunks = new TileChunkStore(this, store);
			}

			/// <inheritdoc />
			public ITilescapeConfigStore Config { get; }

			/// <inheritdoc />
			public IAtlasStore<Guid, TileShape> Shapes { get; }

			/// <inheritdoc />
			public IAtlasStore<Guid, TileColorPalette> ColorPalettes { get; }

			/// <inheritdoc />
			public ITileChunkStore Chunks { get; }

			/// <inheritdoc />
			protected override void ManagedDisposal() => this.store.Dispose();
		}
	}
}
