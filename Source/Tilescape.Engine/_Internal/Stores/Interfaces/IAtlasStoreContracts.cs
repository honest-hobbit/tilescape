﻿using System.Collections.Generic;
using System.Diagnostics;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal static class IAtlasStoreContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void LastUpdated<TKey, TValue>(IAtlasStore<TKey, TValue> instance)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void All<TKey, TValue>(IAtlasStore<TKey, TValue> instance)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Find<TKey, TValue>(IAtlasStore<TKey, TValue> instance)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void TryGet<TKey, TValue>(IAtlasStore<TKey, TValue> instance)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void AddOrUpdate<TKey, TValue>(IAtlasStore<TKey, TValue> instance, TValue value)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(value != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void AddOrUpdateMany<TKey, TValue>(IAtlasStore<TKey, TValue> instance, IEnumerable<TValue> values)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(values.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Remove<TKey, TValue>(IAtlasStore<TKey, TValue> instance)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void RemoveMany<TKey, TValue>(IAtlasStore<TKey, TValue> instance, IEnumerable<TKey> keys)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(keys.AllAndSelfNotNull());
		}

		[Conditional(Require.CompilationSymbol)]
		public static void DeleteAtlas<TKey, TValue>(IAtlasStore<TKey, TValue> instance)
			where TValue : IKeyed<TKey>, INamed
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}
	}
}
