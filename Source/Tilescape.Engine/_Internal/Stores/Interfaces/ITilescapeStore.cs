﻿using System;
using Tilescape.Utility.Disposables;

namespace Tilescape.Engine
{
	internal interface ITilescapeStore : IVisiblyDisposable
	{
		ITilescapeConfigStore Config { get; }

		IAtlasStore<Guid, TileShape> Shapes { get; }

		IAtlasStore<Guid, TileColorPalette> ColorPalettes { get; }

		ITileChunkStore Chunks { get; }
	}
}
