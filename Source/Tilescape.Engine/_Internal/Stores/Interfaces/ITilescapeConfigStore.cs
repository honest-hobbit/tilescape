﻿using Tilescape.Utility.Disposables;

namespace Tilescape.Engine
{
	internal interface ITilescapeConfigStore : IDisposed
	{
		ITilescapeStore Stores { get; }

		ITileStageConfig Stages { get; }

		int TileShapeColliderFillThreshold { get; set; }
	}
}
