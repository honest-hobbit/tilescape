﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class ITilescapeConfigStoreContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void TileShapeColliderFillThresholdGetter(ITilescapeConfigStore instance)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void TileShapeColliderFillThresholdSetter(ITilescapeConfigStore instance, int value)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(value >= 0);
		}
	}
}
