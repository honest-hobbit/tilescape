﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal interface IAtlasStore<TKey, TValue> : IDisposed
		where TValue : IKeyed<TKey>, INamed
	{
		ITilescapeStore Stores { get; }

		DateTime LastUpdated { get; }

		IEnumerable<TValue> All();

		IEnumerable<TValue> Find(string name);

		bool TryGet(TKey key, out TValue value);

		void AddOrUpdate(TValue value);

		void AddOrUpdateMany(IEnumerable<TValue> values);

		void Remove(TKey key);

		void RemoveMany(IEnumerable<TKey> keys);

		void DeleteAtlas();
	}
}
