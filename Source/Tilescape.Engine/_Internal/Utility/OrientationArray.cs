﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class OrientationArray
	{
		public static T[] Create<T>(Func<T, OrthoAxis> getOrientation, params T[] values) =>
			Create(getOrientation, (IEnumerable<T>)values);

		public static T[] Create<T>(Func<T, OrthoAxis> getOrientation, IEnumerable<T> values)
		{
			Require.That(getOrientation != null);
			Require.That(values != null);
			Require.That(values.Count() == Orthogonal.Orientations.Count);

			var result = new T[Orthogonal.Orientations.Count];

			foreach (var value in values)
			{
				result[(int)getOrientation(value)] = value;
			}

			return result;
		}

		public static T[] Create<T>(params KeyValuePair<OrthoAxis, T>[] values) =>
			Create((IEnumerable<KeyValuePair<OrthoAxis, T>>)values);

		public static T[] Create<T>(IEnumerable<KeyValuePair<OrthoAxis, T>> values)
		{
			Require.That(values != null);
			Require.That(values.Count() == Orthogonal.Orientations.Count);

			var result = new T[Orthogonal.Orientations.Count];

			foreach (var pair in values)
			{
				result[(int)pair.Key] = pair.Value;
			}

			return result;
		}
	}
}
