﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Unity.Types;
using UnityEngine;

namespace Tilescape.Engine
{
	internal static class ColorPalette
	{
		public static int RequirredLength => 256;

		public static int EmptyColorIndex => 0;

		public static Color32 EmptyColor => new Color32(0, 0, 0, 0);

		public static bool IsValid(IListView<Color32> colors) =>
			colors != null && colors.Count == RequirredLength && colors[EmptyColorIndex].IsEqualTo(EmptyColor);
	}
}
