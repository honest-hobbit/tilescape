﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class ColliderBuilder
	{
		private const float DefaultScale = 1;

		private const float DefaultOffset = 0;

		private readonly List<BoxStruct> boxColliders = new List<BoxStruct>();

		private readonly CubeArray<int> colliderCount;

		private readonly int sideMax;

		private readonly float scale;

		private readonly float offset;

		private Func<int, int, int, bool> isSolid;

		private int boxMinX;

		private int boxMinY;

		private int boxMinZ;

		private int boxMaxX;

		private int boxMaxY;

		private int boxMaxZ;

		public ColliderBuilder(int sizeExponent, float scale = DefaultScale, float offset = DefaultOffset)
		{
			Require.That(sizeExponent >= 0);
			Require.That(scale > 0);

			this.sideMax = BinaryArray.GetSideLength(sizeExponent) - 1;
			this.colliderCount = new CubeArray<int>(sizeExponent);
			this.scale = scale;
			this.offset = offset;
		}

		public int SizeExponent => this.colliderCount.SizeExponent;

		public void BuildColliders(Func<int, int, int, bool> isSolid, IList<BoxColliderStruct> colliders)
		{
			Require.That(isSolid != null);
			Require.That(colliders != null);

			this.isSolid = isSolid;

			// clear the previous collider data
			this.boxColliders.Clear();
			for (int index = 0; index < this.colliderCount.Array.Length; index++)
			{
				this.colliderCount.Array[index] = 0;
			}

			// purposely biasing towards horizontal expansion first, then vertical
			// because game worls tend to be more horizontally orientated
			for (int iX = 0; iX <= this.sideMax; iX++)
			{
				for (int iZ = 0; iZ <= this.sideMax; iZ++)
				{
					for (int iY = 0; iY <= this.sideMax; iY++)
					{
						this.TryCreateCollider(iX, iY, iZ);
					}
				}
			}

			// sort the colliders so the smallest are first
			this.boxColliders.Sort((a, b) => a.Size - b.Size);

			// skip over any colliders that are completely covered by more than 1 collider
			// add any that can't be skipped to the colliders result
			for (int index = 0; index < this.boxColliders.Count; index++)
			{
				var box = this.boxColliders[index];
				if (!this.TrySkipCollider(box))
				{
					var size = new Vector3(box.MaxX - box.MinX + 1, box.MaxY - box.MinY + 1, box.MaxZ - box.MinZ + 1);
					var origin = new Vector3(box.MinX, box.MinY, box.MinZ);

					if (this.scale != DefaultScale)
					{
						size *= this.scale;
						origin *= this.scale;
					}

					if (this.offset != DefaultOffset)
					{
						origin += new Vector3(this.offset, this.offset, this.offset);
					}

					var center = (size / 2f) + origin;
					colliders.Add(new BoxColliderStruct(center, size));
				}
			}

			this.isSolid = null;
		}

		private bool TrySkipCollider(BoxStruct box)
		{
			for (int iX = box.MinX; iX <= box.MaxX; iX++)
			{
				for (int iY = box.MinY; iY <= box.MaxY; iY++)
				{
					for (int iZ = box.MinZ; iZ <= box.MaxZ; iZ++)
					{
						if (this.colliderCount[iX, iY, iZ] <= 1)
						{
							return false;
						}
					}
				}
			}

			for (int iX = box.MinX; iX <= box.MaxX; iX++)
			{
				for (int iY = box.MinY; iY <= box.MaxY; iY++)
				{
					for (int iZ = box.MinZ; iZ <= box.MaxZ; iZ++)
					{
						this.colliderCount[iX, iY, iZ]--;
					}
				}
			}

			return true;
		}

		private void TryCreateCollider(int x, int y, int z)
		{
			Require.That(x >= 0 && x <= this.sideMax);
			Require.That(y >= 0 && y <= this.sideMax);
			Require.That(z >= 0 && z <= this.sideMax);

			if (!this.CanStartCollider(x, y, z))
			{
				return;
			}

			this.boxMinX = this.boxMaxX = x;
			this.boxMinY = this.boxMaxY = y;
			this.boxMinZ = this.boxMaxZ = z;

			this.ExpandCollider(true);
			this.ExpandCollider(false);

			for (int iX = this.boxMinX; iX <= this.boxMaxX; iX++)
			{
				for (int iY = this.boxMinY; iY <= this.boxMaxY; iY++)
				{
					for (int iZ = this.boxMinZ; iZ <= this.boxMaxZ; iZ++)
					{
						this.colliderCount[iX, iY, iZ]++;
					}
				}
			}

			this.boxColliders.Add(new BoxStruct(this.boxMinX, this.boxMinY, this.boxMinZ, this.boxMaxX, this.boxMaxY, this.boxMaxZ));
		}

		private bool CanStartCollider(int x, int y, int z) => this.colliderCount[x, y, z] == 0 && this.isSolid(x, y, z);

		private void ExpandCollider(bool isIncreasing)
		{
			bool tryPushX = true;
			bool tryPushY = true;
			bool tryPushZ = true;
			bool keepPushing = true;

			while (keepPushing)
			{
				keepPushing = false;

				if (tryPushX)
				{
					tryPushX = TryPushX();
					if (tryPushX)
					{
						keepPushing = true;
					}
				}

				if (tryPushZ)
				{
					tryPushZ = TryPushZ();
					if (tryPushZ)
					{
						keepPushing = true;
					}
				}

				// purposely biasing towards horizontal expansion first, then vertical
				// because game worls tend to be more horizontally orientated
				if (tryPushY)
				{
					tryPushY = TryPushY();
					if (tryPushY)
					{
						keepPushing = true;
					}
				}
			}

			bool TryPushX()
			{
				if (!this.TryGetPush(this.boxMinX, this.boxMaxX, isIncreasing, out int xPush))
				{
					return false;
				}

				for (int iY = this.boxMinY; iY <= this.boxMaxY; iY++)
				{
					for (int iZ = this.boxMinZ; iZ <= this.boxMaxZ; iZ++)
					{
						if (!this.isSolid(xPush, iY, iZ))
						{
							return false;
						}
					}
				}

				if (isIncreasing)
				{
					this.boxMaxX = xPush;
				}
				else
				{
					this.boxMinX = xPush;
				}

				return true;
			}

			bool TryPushY()
			{
				if (!this.TryGetPush(this.boxMinY, this.boxMaxY, isIncreasing, out int yPush))
				{
					return false;
				}

				for (int iX = this.boxMinX; iX <= this.boxMaxX; iX++)
				{
					for (int iZ = this.boxMinZ; iZ <= this.boxMaxZ; iZ++)
					{
						if (!this.isSolid(iX, yPush, iZ))
						{
							return false;
						}
					}
				}

				if (isIncreasing)
				{
					this.boxMaxY = yPush;
				}
				else
				{
					this.boxMinY = yPush;
				}

				return true;
			}

			bool TryPushZ()
			{
				if (!this.TryGetPush(this.boxMinZ, this.boxMaxZ, isIncreasing, out int zPush))
				{
					return false;
				}

				for (int iX = this.boxMinX; iX <= this.boxMaxX; iX++)
				{
					for (int iY = this.boxMinY; iY <= this.boxMaxY; iY++)
					{
						if (!this.isSolid(iX, iY, zPush))
						{
							return false;
						}
					}
				}

				if (isIncreasing)
				{
					this.boxMaxZ = zPush;
				}
				else
				{
					this.boxMinZ = zPush;
				}

				return true;
			}
		}

		private bool TryGetPush(int min, int max, bool isIncreasing, out int push)
		{
			push = 0;
			if (isIncreasing)
			{
				if (max == this.sideMax)
				{
					return false;
				}

				push = max + 1;
			}
			else
			{
				if (min == 0)
				{
					return false;
				}

				push = min - 1;
			}

			return true;
		}

		private struct BoxStruct
		{
			public BoxStruct(int minX, int minY, int minZ, int maxX, int maxY, int maxZ)
			{
				this.MinX = minX;
				this.MinY = minY;
				this.MinZ = minZ;
				this.MaxX = maxX;
				this.MaxY = maxY;
				this.MaxZ = maxZ;
				this.Size = (this.MaxX - this.MinX + 1) * (this.MaxY - this.MinY + 1) * (this.MaxZ - this.MinZ + 1);
			}

			public int MinX { get; }

			public int MinY { get; }

			public int MinZ { get; }

			public int MaxX { get; }

			public int MaxY { get; }

			public int MaxZ { get; }

			public int Size { get; }

			/// <inheritdoc />
			public override string ToString() => this.Size.ToString();
		}
	}
}
