﻿using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	internal static class CubeArray
	{
		public static int ToArrayIndex(int sizeExponent, Index3D index) =>
			index.X | ((index.Y | (index.Z << sizeExponent)) << sizeExponent);

		public static int ToArrayIndex(int sizeExponent, int x, int y, int z) =>
			x | ((y | (z << sizeExponent)) << sizeExponent);

		public static Index3D FromArrayIndex(int sizeExponent, int index)
		{
			int mask = BitMask.CreateIntWithOnesInLowerBits(sizeExponent);
			int x = index & mask;
			index = index >> sizeExponent;
			int y = index & mask;
			int z = index >> sizeExponent;
			return new Index3D(x, y, z);
		}
	}
}
