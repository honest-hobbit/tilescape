﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	internal static class BinaryArray
	{
		public static int GetSideLength(int sizeExponent)
		{
			Require.That(sizeExponent >= 0);

			return MathUtility.PowerOf2(sizeExponent);
		}

		public static int GetSquareLength(int sizeExponent)
		{
			Require.That(sizeExponent >= 0);

			return MathUtility.IntegerPower(GetSideLength(sizeExponent), Index2D.Zero.Rank);
		}

		public static int GetCubeLength(int sizeExponent)
		{
			Require.That(sizeExponent >= 0);

			return MathUtility.IntegerPower(GetSideLength(sizeExponent), Index3D.Zero.Rank);
		}
	}
}
