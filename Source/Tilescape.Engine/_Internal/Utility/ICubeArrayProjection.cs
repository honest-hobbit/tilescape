﻿using System.Collections.Generic;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	internal interface ICubeArrayProjection
	{
		OrthoAxis Orientation { get; }

		T Get<T>(ICubeArrayView<T> array, int x, int y, int z);

		T Get<T>(ICubeArrayView<T> array, Index3D index);

		IEnumerable<IndexValuePair<Index3D, T>> GetEnumerable<T>(ICubeArrayView<T> array);
	}
}
