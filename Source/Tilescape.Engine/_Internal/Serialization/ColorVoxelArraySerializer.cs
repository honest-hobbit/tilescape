﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	internal static class ColorVoxelArraySerializer
	{
		public static IInlineSerDes<ColorVoxel[]> RunLengthEncoded { get; } =
			CreateRunLengthEncoder(SerializeInt.AsByte, byte.MaxValue);

		public static IInlineSerDes<ColorVoxel[]> CreateRunLengthEncoder(IUniformSerDes<int> runLengthSerializer, int maxRunLength)
		{
			Require.That(runLengthSerializer != null);
			Require.That(maxRunLength >= 1);

			return new RunLengthEncodedArraySerializer<ColorVoxel>(
				ColorVoxelSerializer.Instance, runLengthSerializer, maxRunLength, Equality.StructComparer<ColorVoxel>());
		}
	}
}
