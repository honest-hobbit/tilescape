﻿using Tilescape.Utility.Serialization;
using Tilescape.Utility.Unity.Types.Serialization;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class BoxColliderStructSerializer : AbstractCompositeUniformSerializer<BoxColliderStruct, Vector3, Vector3>
	{
		public BoxColliderStructSerializer(IUniformSerDes<Vector3> serDes)
			: base(serDes, serDes)
		{
		}

		public BoxColliderStructSerializer(IUniformSerDes<Vector3> centerSerDes, IUniformSerDes<Vector3> sizeSerDes)
			: base(centerSerDes, sizeSerDes)
		{
		}

		public static IUniformSerDes<BoxColliderStruct> Instance { get; } = new BoxColliderStructSerializer(Vector3Serializer.Instance);

		/// <inheritdoc />
		protected override BoxColliderStruct ComposeValue(Vector3 center, Vector3 size) => new BoxColliderStruct(center, size);

		/// <inheritdoc />
		protected override void DecomposeValue(BoxColliderStruct value, out Vector3 center, out Vector3 size)
		{
			center = value.Center;
			size = value.Size;
		}
	}
}
