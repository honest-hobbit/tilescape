﻿using System;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class TileCollidersSerializer : IInlineSerDes<TileColliders>
	{
		private readonly CollectionSerializer<BoxColliderStruct> boxesSerDes =
			new CollectionSerializer<BoxColliderStruct>(BoxColliderStructSerializer.Instance);

		public static IInlineSerDes<TileColliders> Instance { get; } = new TileCollidersSerializer();

		private TileCollidersSerializer()
		{
		}

		/// <inheritdoc />
		public int GetSerializedLength(TileColliders tile)
		{
			ISerializerContracts.GetSerializedLength(tile);

			int serializedLength = Serializer.Bool.SerializedLength;
			if (!tile.IsFullCube)
			{
				serializedLength += this.boxesSerDes.GetSerializedLength(tile.Boxes);
			}

			return serializedLength;
		}

		/// <inheritdoc />
		public int Serialize(TileColliders tile, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, tile, buffer, index);

			int serializedLength = Serializer.Bool.Serialize(tile.IsFullCube, buffer, ref index);
			if (!tile.IsFullCube)
			{
				serializedLength += this.boxesSerDes.Serialize(tile.Boxes, buffer, ref index);
			}

			return serializedLength;
		}

		/// <inheritdoc />
		public int Serialize(TileColliders tile, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(tile, writeByte);

			int serializedLength = Serializer.Bool.Serialize(tile.IsFullCube, writeByte);
			if (!tile.IsFullCube)
			{
				serializedLength += this.boxesSerDes.Serialize(tile.Boxes, writeByte);
			}

			return serializedLength;
		}

		/// <inheritdoc />
		public void DeserializeInline(byte[] buffer, ref int index, TileColliders result)
		{
			IInlineDeserializerContracts.DeserializeInline(buffer, index, result);

			result.IsFullCube = Serializer.Bool.Deserialize(buffer, ref index);
			if (result.IsFullCube)
			{
				result.Boxes.Clear();
			}
			else
			{
				this.boxesSerDes.DeserializeInline(buffer, ref index, result.Boxes);
			}
		}

		/// <inheritdoc />
		public void DeserializeInline(Func<Try<byte>> readByte, TileColliders result)
		{
			IInlineDeserializerContracts.DeserializeInline(readByte, result);

			result.IsFullCube = Serializer.Bool.Deserialize(readByte);
			if (result.IsFullCube)
			{
				result.Boxes.Clear();
			}
			else
			{
				this.boxesSerDes.DeserializeInline(readByte, result.Boxes);
			}
		}
	}
}
