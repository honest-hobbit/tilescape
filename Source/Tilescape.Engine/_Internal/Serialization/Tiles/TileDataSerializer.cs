﻿using System;
using System.Diagnostics.CodeAnalysis;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	[SuppressMessage("StyleCop", "SA1501", Justification = "More readable on single line.")]
	internal class TileDataSerializer : IUniformSerDes<TileData>
	{
		private static readonly IUniformSerDes<TileTypeKey> TypeSerDes = TileTypeKeySerializer.Instance;

		private static readonly IUniformSerDes<byte> ByteSerDes = Serializer.Byte;

		private readonly int bytesUsed;

		public TileDataSerializer(int bytesUsed)
		{
			Require.That(bytesUsed >= 0 && bytesUsed <= 12);

			this.bytesUsed = bytesUsed;
			this.SerializedLength = TypeSerDes.SerializedLength + bytesUsed;
		}

		/// <inheritdoc />
		public int SerializedLength { get; }

		/// <inheritdoc />
		public int GetSerializedLength(TileData value) => this.SerializedLength;

		/// <inheritdoc />
		public int Serialize(TileData tile, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, tile, buffer, index);

			TypeSerDes.Serialize(tile.Type, buffer, ref index);

			if (this.bytesUsed > 0) { ByteSerDes.Serialize(tile.Data0, buffer, ref index); }
			if (this.bytesUsed > 1) { ByteSerDes.Serialize(tile.Data1, buffer, ref index); }
			if (this.bytesUsed > 2) { ByteSerDes.Serialize(tile.Data2, buffer, ref index); }
			if (this.bytesUsed > 3) { ByteSerDes.Serialize(tile.Data3, buffer, ref index); }
			if (this.bytesUsed > 4) { ByteSerDes.Serialize(tile.Data4, buffer, ref index); }
			if (this.bytesUsed > 5) { ByteSerDes.Serialize(tile.Data5, buffer, ref index); }
			if (this.bytesUsed > 6) { ByteSerDes.Serialize(tile.Data6, buffer, ref index); }
			if (this.bytesUsed > 7) { ByteSerDes.Serialize(tile.Data7, buffer, ref index); }
			if (this.bytesUsed > 8) { ByteSerDes.Serialize(tile.Data8, buffer, ref index); }
			if (this.bytesUsed > 9) { ByteSerDes.Serialize(tile.Data9, buffer, ref index); }
			if (this.bytesUsed > 10) { ByteSerDes.Serialize(tile.Data10, buffer, ref index); }
			if (this.bytesUsed > 11) { ByteSerDes.Serialize(tile.Data11, buffer, ref index); }

			return this.SerializedLength;
		}

		/// <inheritdoc />
		public int Serialize(TileData tile, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(tile, writeByte);

			TypeSerDes.Serialize(tile.Type, writeByte);

			if (this.bytesUsed > 0) { ByteSerDes.Serialize(tile.Data0, writeByte); }
			if (this.bytesUsed > 1) { ByteSerDes.Serialize(tile.Data1, writeByte); }
			if (this.bytesUsed > 2) { ByteSerDes.Serialize(tile.Data2, writeByte); }
			if (this.bytesUsed > 3) { ByteSerDes.Serialize(tile.Data3, writeByte); }
			if (this.bytesUsed > 4) { ByteSerDes.Serialize(tile.Data4, writeByte); }
			if (this.bytesUsed > 5) { ByteSerDes.Serialize(tile.Data5, writeByte); }
			if (this.bytesUsed > 6) { ByteSerDes.Serialize(tile.Data6, writeByte); }
			if (this.bytesUsed > 7) { ByteSerDes.Serialize(tile.Data7, writeByte); }
			if (this.bytesUsed > 8) { ByteSerDes.Serialize(tile.Data8, writeByte); }
			if (this.bytesUsed > 9) { ByteSerDes.Serialize(tile.Data9, writeByte); }
			if (this.bytesUsed > 10) { ByteSerDes.Serialize(tile.Data10, writeByte); }
			if (this.bytesUsed > 11) { ByteSerDes.Serialize(tile.Data11, writeByte); }

			return this.SerializedLength;
		}

		/// <inheritdoc />
		public TileData Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			var typeKey = TypeSerDes.Deserialize(buffer, ref index);

			var data0 = this.bytesUsed > 0 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data1 = this.bytesUsed > 1 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data2 = this.bytesUsed > 2 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data3 = this.bytesUsed > 3 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data4 = this.bytesUsed > 4 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data5 = this.bytesUsed > 5 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data6 = this.bytesUsed > 6 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data7 = this.bytesUsed > 7 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data8 = this.bytesUsed > 8 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data9 = this.bytesUsed > 9 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data10 = this.bytesUsed > 10 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;
			var data11 = this.bytesUsed > 11 ? ByteSerDes.Deserialize(buffer, ref index) : (byte)0;

			return new TileData(typeKey, data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, data10, data11);
		}

		/// <inheritdoc />
		public TileData Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			var typeKey = TypeSerDes.Deserialize(readByte);

			var data0 = this.bytesUsed > 0 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data1 = this.bytesUsed > 1 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data2 = this.bytesUsed > 2 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data3 = this.bytesUsed > 3 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data4 = this.bytesUsed > 4 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data5 = this.bytesUsed > 5 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data6 = this.bytesUsed > 6 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data7 = this.bytesUsed > 7 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data8 = this.bytesUsed > 8 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data9 = this.bytesUsed > 9 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data10 = this.bytesUsed > 10 ? ByteSerDes.Deserialize(readByte) : (byte)0;
			var data11 = this.bytesUsed > 11 ? ByteSerDes.Deserialize(readByte) : (byte)0;

			return new TileData(typeKey, data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, data10, data11);
		}
	}
}
