﻿using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	internal class TileShapeKeySerializer : AbstractCompositeUniformSerializer<TileShapeKey, byte>
	{
		private TileShapeKeySerializer()
			: base(Serializer.Byte)
		{
		}

		public static IUniformSerDes<TileShapeKey> Instance { get; } = new TileShapeKeySerializer();

		/// <inheritdoc />
		protected override TileShapeKey ComposeValue(byte index) => new TileShapeKey(index);

		/// <inheritdoc />
		protected override byte DecomposeValue(TileShapeKey key) => key.Index;
	}
}
