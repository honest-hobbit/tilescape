﻿using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	internal class TileTypeKeySerializer : AbstractCompositeUniformSerializer<TileTypeKey, ushort>
	{
		private TileTypeKeySerializer()
			: base(Serializer.UShort)
		{
		}

		public static IUniformSerDes<TileTypeKey> Instance { get; } = new TileTypeKeySerializer();

		/// <inheritdoc />
		protected override TileTypeKey ComposeValue(ushort index) => new TileTypeKey(index);

		/// <inheritdoc />
		protected override ushort DecomposeValue(TileTypeKey key) => key.Index;
	}
}
