﻿using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	internal class TileSerializer : AbstractCompositeUniformSerializer<
		Tile, TileTypeKey, OrthoAxis, TileShapeKey, TileColorPaletteKey>
	{
		private TileSerializer()
			: base(
				  TileTypeKeySerializer.Instance,
				  OrthoAxisSerializer.Instance,
				  TileShapeKeySerializer.Instance,
				  TileColorPaletteKeySerializer.Instance)
		{
		}

		public static IUniformSerDes<Tile> Instance { get; } = new TileSerializer();

		/// <inheritdoc />
		protected override Tile ComposeValue(
			TileTypeKey type, OrthoAxis orientation, TileShapeKey shape, TileColorPaletteKey colorPalette) =>
			new Tile(type, orientation, shape, colorPalette);

		/// <inheritdoc />
		protected override void DecomposeValue(
			Tile tile, out TileTypeKey type, out OrthoAxis orientation, out TileShapeKey shape, out TileColorPaletteKey colorPalette)
		{
			type = tile.Type;
			orientation = tile.Orientation;
			shape = tile.Shape;
			colorPalette = tile.ColorPalette;
		}
	}
}
