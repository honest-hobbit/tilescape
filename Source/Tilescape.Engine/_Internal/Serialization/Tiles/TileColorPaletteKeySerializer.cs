﻿using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	internal class TileColorPaletteKeySerializer : AbstractCompositeUniformSerializer<TileColorPaletteKey, byte>
	{
		private TileColorPaletteKeySerializer()
			: base(Serializer.Byte)
		{
		}

		public static IUniformSerDes<TileColorPaletteKey> Instance { get; } = new TileColorPaletteKeySerializer();

		/// <inheritdoc />
		protected override TileColorPaletteKey ComposeValue(byte index) => new TileColorPaletteKey(index);

		/// <inheritdoc />
		protected override byte DecomposeValue(TileColorPaletteKey key) => key.Index;
	}
}
