﻿using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	internal class ColorVoxelSerializer : AbstractCompositeUniformSerializer<ColorVoxel, byte>
	{
		private ColorVoxelSerializer()
			: base(Serializer.Byte)
		{
		}

		public static IUniformSerDes<ColorVoxel> Instance { get; } = new ColorVoxelSerializer();

		/// <inheritdoc />
		protected override ColorVoxel ComposeValue(byte colorIndex) => new ColorVoxel(colorIndex);

		/// <inheritdoc />
		protected override byte DecomposeValue(ColorVoxel voxel) => voxel.ColorIndex;
	}
}
