﻿using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	internal class OrthoAxisSerializer : AbstractCompositeUniformSerializer<OrthoAxis, byte>
	{
		private OrthoAxisSerializer()
			: base(Serializer.Byte)
		{
		}

		public static IUniformSerDes<OrthoAxis> Instance { get; } = new OrthoAxisSerializer();

		/// <inheritdoc />
		protected override OrthoAxis ComposeValue(byte value) => (OrthoAxis)value;

		/// <inheritdoc />
		protected override byte DecomposeValue(OrthoAxis value) => (byte)value;
	}
}
