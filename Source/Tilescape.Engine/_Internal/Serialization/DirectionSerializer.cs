﻿using Tilescape.Utility.Serialization;

namespace Tilescape.Engine
{
	public class DirectionSerializer : AbstractCompositeUniformSerializer<Direction, byte>
	{
		private DirectionSerializer()
			: base(Serializer.Byte)
		{
		}

		public static IUniformSerDes<Direction> Instance { get; } = new DirectionSerializer();

		/// <inheritdoc />
		protected override Direction ComposeValue(byte value) => (Direction)value;

		/// <inheritdoc />
		protected override byte DecomposeValue(Direction value) => (byte)value;
	}
}
