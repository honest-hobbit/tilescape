﻿using Tilescape.Utility.Serialization;
using Tilescape.Utility.Unity.Types;
using Tilescape.Utility.Unity.Types.Serialization;

namespace Tilescape.Engine
{
	internal class MeshQuadSlimSerializer :
		AbstractCompositeUniformSerializer<MeshQuadSlim, byte, byte, byte, byte, byte, Direction, byte>
	{
		private MeshQuadSlimSerializer()
			: base(
				  Serializer.Byte,
				  Serializer.Byte,
				  Serializer.Byte,
				  Serializer.Byte,
				  Serializer.Byte,
				  DirectionSerializer.Instance,
				  Serializer.Byte)
		{
		}

		public static IUniformSerDes<MeshQuadSlim> Instance { get; } = new MeshQuadSlimSerializer();

		/// <inheritdoc />
		protected override MeshQuadSlim ComposeValue(
			byte aMin, byte aMax, byte bMin, byte bMax, byte c, Direction normal, byte colorIndex) =>
			new MeshQuadSlim(aMin, aMax, bMin, bMax, c, normal, colorIndex);

		/// <inheritdoc />
		protected override void DecomposeValue(
			MeshQuadSlim quad,
			out byte aMin,
			out byte aMax,
			out byte bMin,
			out byte bMax,
			out byte c,
			out Direction normal,
			out byte colorIndex)
		{
			aMin = quad.AMin;
			aMax = quad.AMax;
			bMin = quad.BMin;
			bMax = quad.BMax;
			c = quad.C;
			normal = quad.Normal;
			colorIndex = quad.ColorIndex;
		}
	}
}
