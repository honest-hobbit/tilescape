﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class TileChunkSerializer : IInlineSerDes<ITileChunk>
	{
		private static readonly ISerDes<Tile> TileSerDes = TileSerializer.Instance;

		private readonly IInlineSerDes<Tile[]> tileArraySerializer;

		public TileChunkSerializer(IInlineSerDes<Tile[]> tileArraySerializer)
		{
			Require.That(tileArraySerializer != null);

			this.tileArraySerializer = tileArraySerializer;
		}

		public static IInlineSerDes<ITileChunk> RunLengthEncoded { get; } = CreateRunLengthEncoder(SerializeInt.AsByte, byte.MaxValue);

		public static IInlineSerDes<ITileChunk> CreateRunLengthEncoder(IUniformSerDes<int> runLengthSerializer, int maxRunLength)
		{
			Require.That(runLengthSerializer != null);
			Require.That(maxRunLength >= 1);

			return new TileChunkSerializer(new RunLengthEncodedArraySerializer<Tile>(
				TileSerDes, runLengthSerializer, maxRunLength, Equality.StructComparer<Tile>()));
		}

		/// <inheritdoc />
		public int GetSerializedLength(ITileChunk chunk)
		{
			ISerializerContracts.GetSerializedLength(chunk);

			if (chunk.IsUniform)
			{
				return Serializer.Bool.SerializedLength + TileSerDes.GetSerializedLength(chunk.UniformTile.Value);
			}
			else
			{
				return Serializer.Bool.SerializedLength + this.tileArraySerializer.GetSerializedLength(chunk.Tiles.Array);
			}
		}

		/// <inheritdoc />
		public int Serialize(ITileChunk chunk, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, chunk, buffer, index);

			bool isUniform = chunk.IsUniform;
			int serializeLength = Serializer.Bool.Serialize(isUniform, buffer, ref index);
			if (isUniform)
			{
				return serializeLength + TileSerDes.Serialize(chunk.UniformTile.Value, buffer, ref index);
			}
			else
			{
				return serializeLength + this.tileArraySerializer.Serialize(chunk.Tiles.Array, buffer, ref index);
			}
		}

		/// <inheritdoc />
		public int Serialize(ITileChunk chunk, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(chunk, writeByte);

			bool isUniform = chunk.IsUniform;
			int serializeLength = Serializer.Bool.Serialize(isUniform, writeByte);
			if (isUniform)
			{
				return serializeLength + TileSerDes.Serialize(chunk.UniformTile.Value, writeByte);
			}
			else
			{
				return serializeLength + this.tileArraySerializer.Serialize(chunk.Tiles.Array, writeByte);
			}
		}

		/// <inheritdoc />
		public void DeserializeInline(byte[] buffer, ref int index, ITileChunk chunk)
		{
			IInlineDeserializerContracts.DeserializeInline(buffer, index, chunk);
			Require.That(chunk.Tiles != null);

			bool isUniform = Serializer.Bool.Deserialize(buffer, ref index);
			if (isUniform)
			{
				chunk.UniformTile = TileSerDes.Deserialize(buffer, ref index);
			}
			else
			{
				this.tileArraySerializer.DeserializeInline(buffer, ref index, chunk.Tiles.Array);
			}
		}

		/// <inheritdoc />
		public void DeserializeInline(Func<Try<byte>> readByte, ITileChunk chunk)
		{
			IInlineDeserializerContracts.DeserializeInline(readByte, chunk);
			Require.That(chunk.Tiles != null);

			bool isUniform = Serializer.Bool.Deserialize(readByte);
			if (isUniform)
			{
				chunk.UniformTile = TileSerDes.Deserialize(readByte);
			}
			else
			{
				this.tileArraySerializer.DeserializeInline(readByte, chunk.Tiles.Array);
			}
		}
	}
}
