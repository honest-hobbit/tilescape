﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class TileMeshesSerializer : IInlineSerDes<TileMeshes>
	{
		private static readonly ISerDes<IListView<MeshQuadSlim>> QuadsSerializer =
			new ListViewSerializer<MeshQuadSlim>(MeshQuadSlimSerializer.Instance);

		public static IInlineSerDes<TileMeshes> Instance { get; } = new TileMeshesSerializer();

		private TileMeshesSerializer()
		{
		}

		/// <inheritdoc />
		public int GetSerializedLength(TileMeshes tile)
		{
			ISerializerContracts.GetSerializedLength(tile);

			int serializedLength = 0;
			serializedLength += QuadsSerializer.GetSerializedLength(tile.Center);
			serializedLength += QuadsSerializer.GetSerializedLength(tile.NegativeX);
			serializedLength += QuadsSerializer.GetSerializedLength(tile.PositiveX);
			serializedLength += QuadsSerializer.GetSerializedLength(tile.NegativeY);
			serializedLength += QuadsSerializer.GetSerializedLength(tile.PositiveY);
			serializedLength += QuadsSerializer.GetSerializedLength(tile.NegativeZ);
			serializedLength += QuadsSerializer.GetSerializedLength(tile.PositiveZ);
			return serializedLength;
		}

		/// <inheritdoc />
		public int Serialize(TileMeshes tile, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, tile, buffer, index);

			int serializedLength = 0;
			serializedLength += QuadsSerializer.Serialize(tile.Center, buffer, ref index);
			serializedLength += QuadsSerializer.Serialize(tile.NegativeX, buffer, ref index);
			serializedLength += QuadsSerializer.Serialize(tile.PositiveX, buffer, ref index);
			serializedLength += QuadsSerializer.Serialize(tile.NegativeY, buffer, ref index);
			serializedLength += QuadsSerializer.Serialize(tile.PositiveY, buffer, ref index);
			serializedLength += QuadsSerializer.Serialize(tile.NegativeZ, buffer, ref index);
			serializedLength += QuadsSerializer.Serialize(tile.PositiveZ, buffer, ref index);
			return serializedLength;
		}

		/// <inheritdoc />
		public int Serialize(TileMeshes tile, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(tile, writeByte);

			int serializedLength = 0;
			serializedLength += QuadsSerializer.Serialize(tile.Center, writeByte);
			serializedLength += QuadsSerializer.Serialize(tile.NegativeX, writeByte);
			serializedLength += QuadsSerializer.Serialize(tile.PositiveX, writeByte);
			serializedLength += QuadsSerializer.Serialize(tile.NegativeY, writeByte);
			serializedLength += QuadsSerializer.Serialize(tile.PositiveY, writeByte);
			serializedLength += QuadsSerializer.Serialize(tile.NegativeZ, writeByte);
			serializedLength += QuadsSerializer.Serialize(tile.PositiveZ, writeByte);
			return serializedLength;
		}

		/// <inheritdoc />
		public void DeserializeInline(byte[] buffer, ref int index, TileMeshes result)
		{
			IInlineDeserializerContracts.DeserializeInline(buffer, index, result);

			result.Center = QuadsSerializer.Deserialize(buffer, ref index);
			result.NegativeX = QuadsSerializer.Deserialize(buffer, ref index);
			result.PositiveX = QuadsSerializer.Deserialize(buffer, ref index);
			result.NegativeY = QuadsSerializer.Deserialize(buffer, ref index);
			result.PositiveY = QuadsSerializer.Deserialize(buffer, ref index);
			result.NegativeZ = QuadsSerializer.Deserialize(buffer, ref index);
			result.PositiveZ = QuadsSerializer.Deserialize(buffer, ref index);
		}

		/// <inheritdoc />
		public void DeserializeInline(Func<Try<byte>> readByte, TileMeshes result)
		{
			IInlineDeserializerContracts.DeserializeInline(readByte, result);

			result.Center = QuadsSerializer.Deserialize(readByte);
			result.NegativeX = QuadsSerializer.Deserialize(readByte);
			result.PositiveX = QuadsSerializer.Deserialize(readByte);
			result.NegativeY = QuadsSerializer.Deserialize(readByte);
			result.PositiveY = QuadsSerializer.Deserialize(readByte);
			result.NegativeZ = QuadsSerializer.Deserialize(readByte);
			result.PositiveZ = QuadsSerializer.Deserialize(readByte);
		}
	}
}
