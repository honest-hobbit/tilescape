﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class QuadListConverter : IListView<MeshQuad>
	{
		private readonly IListView<float> lengths;

		private IListView<MeshQuadSlim> quads;

		private OrthogonalTransformer transformer;

		private IListView<Color32> colors;

		public QuadListConverter(IListView<float> lengths)
		{
			Require.That(lengths != null);

			this.lengths = lengths;
		}

		/// <inheritdoc />
		public int Count => this?.quads.Count ?? 0;

		/// <inheritdoc />
		public MeshQuad this[int index]
		{
			get
			{
				IListViewContracts.Indexer(this, index);

				return this.quads[index].Expand(this.transformer, this.lengths, this.colors);
			}
		}

		/// <inheritdoc />
		public IEnumerator<MeshQuad> GetEnumerator()
		{
			if (this.quads == null)
			{
				yield break;
			}

			for (int index = 0; index < this.quads.Count; index++)
			{
				yield return this.quads[index].Expand(this.transformer, this.lengths, this.colors);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		public void SetTo(IListView<MeshQuadSlim> quads, OrthogonalTransformer transformer, IListView<Color32> colors)
		{
			Require.That(quads != null);
			Require.That(transformer != null);
			Require.That(colors != null);

			this.quads = quads;
			this.transformer = transformer;
			this.colors = colors;
		}

		public void Clear()
		{
			this.quads = null;
			this.transformer = null;
			this.colors = null;
		}
	}
}
