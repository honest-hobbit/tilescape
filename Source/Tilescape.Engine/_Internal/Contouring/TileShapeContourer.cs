﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;

namespace Tilescape.Engine
{
	internal class TileShapeContourer
	{
		private readonly TileShapeQuadContourer quadContourer;

		private readonly TileShapeGreedyContourer greedyContourer;

		public TileShapeContourer(ITileConfig config)
		{
			Require.That(config != null);

			this.quadContourer = new TileShapeQuadContourer(config);
			this.greedyContourer = new TileShapeGreedyContourer(config);
		}

		public int TileSizeExponent => this.quadContourer.TileSizeExponent;

		public void Generate(TileShape shape, MeshingMode mode)
		{
			Require.That(shape != null);
			Require.That(shape.Voxels.SizeExponent == this.TileSizeExponent);
			Require.That(Enumeration.IsDefined(mode));

			switch (mode)
			{
				case MeshingMode.Greedy:
					this.greedyContourer.Generate(shape);
					break;

				case MeshingMode.Quads:
					this.quadContourer.Generate(shape);
					break;

				default: throw InvalidEnumArgument.CreateException(nameof(mode), mode);
			}
		}
	}
}
