﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal abstract class AbstractTileShapeContourer
	{
		public AbstractTileShapeContourer(ITileConfig config)
		{
			Require.That(config != null);

			this.TileSizeExponent = config.TileSizeExponent;
			this.MaxVoxelIndex = config.TileMaxVoxelIndex;
		}

		public int TileSizeExponent { get; }

		protected int MinVoxelIndex => 0;

		protected int MaxVoxelIndex { get; }

		protected List<MeshQuadSlim> QuadsCenter { get; } = new List<MeshQuadSlim>();

		protected List<MeshQuadSlim> QuadsNegativeX { get; } = new List<MeshQuadSlim>();

		protected List<MeshQuadSlim> QuadsPositiveX { get; } = new List<MeshQuadSlim>();

		protected List<MeshQuadSlim> QuadsNegativeY { get; } = new List<MeshQuadSlim>();

		protected List<MeshQuadSlim> QuadsPositiveY { get; } = new List<MeshQuadSlim>();

		protected List<MeshQuadSlim> QuadsNegativeZ { get; } = new List<MeshQuadSlim>();

		protected List<MeshQuadSlim> QuadsPositiveZ { get; } = new List<MeshQuadSlim>();

		protected void Clear()
		{
			this.QuadsCenter.Clear();
			this.QuadsNegativeX.Clear();
			this.QuadsPositiveX.Clear();
			this.QuadsNegativeY.Clear();
			this.QuadsPositiveY.Clear();
			this.QuadsNegativeZ.Clear();
			this.QuadsPositiveZ.Clear();
		}

		protected void CopyQuadsInto(TileShape shape)
		{
			Require.That(shape != null);
			Require.That(shape.Voxels.SizeExponent == this.TileSizeExponent);

			shape.Meshes.Center = this.QuadsCenter.ToArray().AsListView();
			shape.Meshes.NegativeX = this.QuadsNegativeX.ToArray().AsListView();
			shape.Meshes.PositiveX = this.QuadsPositiveX.ToArray().AsListView();
			shape.Meshes.NegativeY = this.QuadsNegativeY.ToArray().AsListView();
			shape.Meshes.PositiveY = this.QuadsPositiveY.ToArray().AsListView();
			shape.Meshes.NegativeZ = this.QuadsNegativeZ.ToArray().AsListView();
			shape.Meshes.PositiveZ = this.QuadsPositiveZ.ToArray().AsListView();
		}
	}
}
