﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Unity.Types;

namespace Tilescape.Engine
{
	internal class TileShapeGreedyContourer : AbstractTileShapeContourer
	{
		private readonly SquareArray<ColorVoxel> slice;

		public TileShapeGreedyContourer(ITileConfig config)
			: base(config)
		{
			Require.That(config != null);

			this.slice = new SquareArray<ColorVoxel>(this.TileSizeExponent);
			this.slice.Array.SetAllTo(ColorVoxel.Empty);
		}

		public void Generate(TileShape shape)
		{
			Require.That(shape != null);
			Require.That(shape.Voxels.SizeExponent == this.TileSizeExponent);

			this.Clear();
			this.GenerateXAxis(shape);
			this.GenerateYAxis(shape);
			this.GenerateZAxis(shape);
			this.CopyQuadsInto(shape);
		}

		private void GenerateXAxis(TileShape shape)
		{
			var voxels = shape.Voxels;

			// negative face
			for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
			{
				for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
				{
					this.slice[iY, iZ] = voxels[this.MinVoxelIndex, iY, iZ];
				}
			}

			this.GenerateSlice(this.MinVoxelIndex - 1, Direction.NegativeX, this.QuadsNegativeX);

			// positive face
			for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
			{
				for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
				{
					this.slice[iY, iZ] = voxels[this.MaxVoxelIndex, iY, iZ];
				}
			}

			this.GenerateSlice(this.MaxVoxelIndex, Direction.PositiveX, this.QuadsPositiveX);

			// center slices
			for (int iX = this.MinVoxelIndex; iX < this.MaxVoxelIndex; iX++)
			{
				// facing negative
				for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
				{
					for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
					{
						var voxel = voxels[iX + 1, iY, iZ];
						if (!voxel.IsEmpty && voxels[iX, iY, iZ].IsEmpty)
						{
							this.slice[iY, iZ] = voxel;
						}
					}
				}

				this.GenerateSlice(iX, Direction.NegativeX, this.QuadsCenter);

				// facing positive
				for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
				{
					for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
					{
						var voxel = voxels[iX, iY, iZ];
						if (!voxel.IsEmpty && voxels[iX + 1, iY, iZ].IsEmpty)
						{
							this.slice[iY, iZ] = voxel;
						}
					}
				}

				this.GenerateSlice(iX, Direction.PositiveX, this.QuadsCenter);
			}
		}

		private void GenerateYAxis(TileShape shape)
		{
			var voxels = shape.Voxels;

			// negative face
			for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
			{
				for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
				{
					this.slice[iX, iZ] = voxels[iX, this.MinVoxelIndex, iZ];
				}
			}

			this.GenerateSlice(this.MinVoxelIndex - 1, Direction.NegativeY, this.QuadsNegativeY);

			// positive face
			for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
			{
				for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
				{
					this.slice[iX, iZ] = voxels[iX, this.MaxVoxelIndex, iZ];
				}
			}

			this.GenerateSlice(this.MaxVoxelIndex, Direction.PositiveY, this.QuadsPositiveY);

			// center slices
			for (int iY = this.MinVoxelIndex; iY < this.MaxVoxelIndex; iY++)
			{
				// facing negative
				for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
				{
					for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
					{
						var voxel = voxels[iX, iY + 1, iZ];
						if (!voxel.IsEmpty && voxels[iX, iY, iZ].IsEmpty)
						{
							this.slice[iX, iZ] = voxel;
						}
					}
				}

				this.GenerateSlice(iY, Direction.NegativeY, this.QuadsCenter);

				// facing positive
				for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
				{
					for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
					{
						var voxel = voxels[iX, iY, iZ];
						if (!voxel.IsEmpty && voxels[iX, iY + 1, iZ].IsEmpty)
						{
							this.slice[iX, iZ] = voxel;
						}
					}
				}

				this.GenerateSlice(iY, Direction.PositiveY, this.QuadsCenter);
			}
		}

		private void GenerateZAxis(TileShape shape)
		{
			var voxels = shape.Voxels;

			// negative face
			for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
			{
				for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
				{
					this.slice[iX, iY] = voxels[iX, iY, this.MinVoxelIndex];
				}
			}

			this.GenerateSlice(this.MinVoxelIndex - 1, Direction.NegativeZ, this.QuadsNegativeZ);

			// positive face
			for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
			{
				for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
				{
					this.slice[iX, iY] = voxels[iX, iY, this.MaxVoxelIndex];
				}
			}

			this.GenerateSlice(this.MaxVoxelIndex, Direction.PositiveZ, this.QuadsPositiveZ);

			// center slices
			for (int iZ = this.MinVoxelIndex; iZ < this.MaxVoxelIndex; iZ++)
			{
				// facing negative
				for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
				{
					for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
					{
						var voxel = voxels[iX, iY, iZ + 1];
						if (!voxel.IsEmpty && voxels[iX, iY, iZ].IsEmpty)
						{
							this.slice[iX, iY] = voxel;
						}
					}
				}

				this.GenerateSlice(iZ, Direction.NegativeZ, this.QuadsCenter);

				// facing positive
				for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
				{
					for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
					{
						var voxel = voxels[iX, iY, iZ];
						if (!voxel.IsEmpty && voxels[iX, iY, iZ + 1].IsEmpty)
						{
							this.slice[iX, iY] = voxel;
						}
					}
				}

				this.GenerateSlice(iZ, Direction.PositiveZ, this.QuadsCenter);
			}
		}

		private void GenerateSlice(int constant, Direction facing, List<MeshQuadSlim> quads)
		{
			Require.That(Enumeration.IsDefined(facing));
			Require.That(quads != null);

			byte c = (byte)(constant + 1);

			for (int iA = this.MinVoxelIndex; iA <= this.MaxVoxelIndex; iA++)
			{
				for (int iB = this.MinVoxelIndex; iB <= this.MaxVoxelIndex;)
				{
					if (this.TryCreateQuad(iA, iB, out int aEnd, out int bEnd, out byte colorIndex))
					{
						quads.Add(MeshQuadSlim.From(iA, aEnd, iB, bEnd, c, facing, colorIndex));
						iB = bEnd;
					}
					else
					{
						iB++;
					}
				}
			}
		}

		private bool TryCreateQuad(int a, int b, out int aEnd, out int bEnd, out byte colorIndex)
		{
			var voxel = this.slice[a, b];
			if (voxel.IsEmpty)
			{
				aEnd = 0;
				bEnd = 0;
				colorIndex = 0;
				return false;
			}

			colorIndex = voxel.ColorIndex;
			int aPush = a;
			int bPush = b;
			bool tryPushA = true;
			bool tryPushB = true;

			// try to expand the quad in both directions as much as possible
			while (tryPushA || tryPushB)
			{
				if (tryPushA)
				{
					tryPushA = TryPushA();
				}

				if (tryPushB)
				{
					tryPushB = TryPushB();
				}
			}

			// clear out the voxels covered by this quad
			for (int iA = a; iA <= aPush; iA++)
			{
				for (int iB = b; iB <= bPush; iB++)
				{
					this.slice[iA, iB] = ColorVoxel.Empty;
				}
			}

			aEnd = aPush + 1;
			bEnd = bPush + 1;
			return true;

			bool TryPushA()
			{
				if (aPush >= this.MaxVoxelIndex)
				{
					return false;
				}

				int push = aPush + 1;
				for (int iB = b; iB <= bPush; iB++)
				{
					if (this.slice[push, iB] != voxel)
					{
						return false;
					}
				}

				aPush = push;
				return true;
			}

			bool TryPushB()
			{
				if (bPush >= this.MaxVoxelIndex)
				{
					return false;
				}

				int push = bPush + 1;
				for (int iA = a; iA <= aPush; iA++)
				{
					if (this.slice[iA, push] != voxel)
					{
						return false;
					}
				}

				bPush = push;
				return true;
			}
		}
	}
}
