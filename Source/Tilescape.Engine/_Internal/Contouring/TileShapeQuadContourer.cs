﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Unity.Types;

namespace Tilescape.Engine
{
	internal class TileShapeQuadContourer : AbstractTileShapeContourer
	{
		public TileShapeQuadContourer(ITileConfig config)
			: base(config)
		{
		}

		public void Generate(TileShape shape)
		{
			Require.That(shape != null);
			Require.That(shape.Voxels.SizeExponent == this.TileSizeExponent);

			this.Clear();
			var voxels = shape.Voxels;

			// X faces
			for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
			{
				for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
				{
					var negVoxel = voxels[this.MinVoxelIndex, iY, iZ];
					var posVoxel = voxels[this.MaxVoxelIndex, iY, iZ];
					this.HandleFaceQuads(
						negVoxel, posVoxel, iY, iZ, Direction.NegativeX, Direction.PositiveX, this.QuadsNegativeX, this.QuadsPositiveX);

					for (int iX = this.MinVoxelIndex; iX < this.MaxVoxelIndex; iX++)
					{
						negVoxel = voxels[iX, iY, iZ];
						posVoxel = voxels[iX + 1, iY, iZ];
						this.HandleSliceQuads(
							negVoxel, posVoxel, iY, iZ, iX, Direction.NegativeX, Direction.PositiveX, this.QuadsCenter);
					}
				}
			}

			// Y faces
			for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
			{
				for (int iZ = this.MinVoxelIndex; iZ <= this.MaxVoxelIndex; iZ++)
				{
					var negVoxel = voxels[iX, this.MinVoxelIndex, iZ];
					var posVoxel = voxels[iX, this.MaxVoxelIndex, iZ];
					this.HandleFaceQuads(
						negVoxel, posVoxel, iX, iZ, Direction.NegativeY, Direction.PositiveY, this.QuadsNegativeY, this.QuadsPositiveY);

					for (int iY = this.MinVoxelIndex; iY < this.MaxVoxelIndex; iY++)
					{
						negVoxel = voxels[iX, iY, iZ];
						posVoxel = voxels[iX, iY + 1, iZ];
						this.HandleSliceQuads(
							negVoxel, posVoxel, iX, iZ, iY, Direction.NegativeY, Direction.PositiveY, this.QuadsCenter);
					}
				}
			}

			// Z faces
			for (int iX = this.MinVoxelIndex; iX <= this.MaxVoxelIndex; iX++)
			{
				for (int iY = this.MinVoxelIndex; iY <= this.MaxVoxelIndex; iY++)
				{
					var negVoxel = voxels[iX, iY, this.MinVoxelIndex];
					var posVoxel = voxels[iX, iY, this.MaxVoxelIndex];
					this.HandleFaceQuads(
						negVoxel, posVoxel, iX, iY, Direction.NegativeZ, Direction.PositiveZ, this.QuadsNegativeZ, this.QuadsPositiveZ);

					for (int iZ = this.MinVoxelIndex; iZ < this.MaxVoxelIndex; iZ++)
					{
						negVoxel = voxels[iX, iY, iZ];
						posVoxel = voxels[iX, iY, iZ + 1];
						this.HandleSliceQuads(
							negVoxel, posVoxel, iX, iY, iZ, Direction.NegativeZ, Direction.PositiveZ, this.QuadsCenter);
					}
				}
			}

			this.CopyQuadsInto(shape);
		}

		private void HandleFaceQuads(
			ColorVoxel negativeVoxel,
			ColorVoxel positiveVoxel,
			int iA,
			int iB,
			Direction negativeAxis,
			Direction positiveAxis,
			List<MeshQuadSlim> negativeFace,
			List<MeshQuadSlim> positiveFace)
		{
			Require.That(negativeFace != null);
			Require.That(positiveFace != null);

			if (negativeVoxel.IsEmpty && positiveVoxel.IsEmpty)
			{
				return;
			}

			if (!negativeVoxel.IsEmpty)
			{
				negativeFace.Add(MeshQuadSlim.From(
					iA, iA + 1, iB, iB + 1, this.MinVoxelIndex, negativeAxis, negativeVoxel.ColorIndex));
			}

			if (!positiveVoxel.IsEmpty)
			{
				positiveFace.Add(MeshQuadSlim.From(
					iA, iA + 1, iB, iB + 1, this.MaxVoxelIndex + 1, positiveAxis, positiveVoxel.ColorIndex));
			}
		}

		private void HandleSliceQuads(
			ColorVoxel negativeVoxel,
			ColorVoxel positiveVoxel,
			int iA,
			int iB,
			int iC,
			Direction negativeAxis,
			Direction positiveAxis,
			List<MeshQuadSlim> centerQuads)
		{
			Require.That(centerQuads != null);

			if (!(negativeVoxel.IsEmpty ^ positiveVoxel.IsEmpty))
			{
				return;
			}

			Direction direction;
			byte colorIndex;
			if (negativeVoxel.IsEmpty)
			{
				direction = negativeAxis;
				colorIndex = positiveVoxel.ColorIndex;
			}
			else
			{
				direction = positiveAxis;
				colorIndex = negativeVoxel.ColorIndex;
			}

			centerQuads.Add(MeshQuadSlim.From(iA, iA + 1, iB, iB + 1, iC + 1, direction, colorIndex));
		}
	}
}
