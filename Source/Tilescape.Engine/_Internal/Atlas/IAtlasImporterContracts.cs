﻿using System;
using System.Diagnostics;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal static class IAtlasImporterContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void UpdateAtlas<TValue, TRecord>(IAtlasImporter<TValue, TRecord> instance)
			where TValue : IKeyed<Guid>, INamed
			where TRecord : AtlasRecord<TValue>
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void RebuildAtlas<TValue, TRecord>(IAtlasImporter<TValue, TRecord> instance)
			where TValue : IKeyed<Guid>, INamed
			where TRecord : AtlasRecord<TValue>
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void DeleteAtlas<TValue, TRecord>(IAtlasImporter<TValue, TRecord> instance)
			where TValue : IKeyed<Guid>, INamed
			where TRecord : AtlasRecord<TValue>
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
		}
	}
}
