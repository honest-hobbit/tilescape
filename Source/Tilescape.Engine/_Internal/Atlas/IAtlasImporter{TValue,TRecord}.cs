﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal interface IAtlasImporter<TValue, TRecord> : IDisposed
		where TValue : IKeyed<Guid>, INamed
		where TRecord : AtlasRecord<TValue>
	{
		IDictionaryView<Guid, TRecord> Records { get; }

		string SourceFolderPath { get; }

		DateTime LastUpdated { get; }

		UniRx.IObservable<TRecord> Added { get; }

		UniRx.IObservable<TRecord> Updated { get; }

		UniRx.IObservable<TValue> Removed { get; }

		UniRx.IObservable<Exception> Error { get; }

		UniRx.IObservable<IDictionaryView<Guid, TRecord>> PassCompleted { get; }

		void UpdateAtlas();

		void RebuildAtlas();

		void DeleteAtlas();
	}
}
