﻿namespace Tilescape.Engine
{
	internal static class AtlasConstants
	{
		public static bool UseJsonPrettyPrinting => true;

		public static class TileShapes
		{
			public static int UseFallbackColliderFillThreshold => -1;

			public static string DataFileName => "data.json";

			public static string ShapeFileName => "shape.vox";
		}

		public static class TileColorPalettes
		{
			public static string DataFileName => "data.json";

			public static string PaletteFileName => "palette.png";
		}
	}
}
