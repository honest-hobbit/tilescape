﻿namespace Tilescape.Engine
{
	internal enum ImportResult
	{
		NothingFound,
		NoUpdate,
		RecordUpdated,
		ValueAdded,
		ValueUpdated,
	}
}
