﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;
using UniRx;

namespace Tilescape.Engine
{
	internal class AtlasStoreBuilder<T> : IDisposed
		where T : IKeyed<Guid>, INamed
	{
		private readonly Subject<T> added = new Subject<T>();

		private readonly Subject<T> updated = new Subject<T>();

		private readonly Subject<T> removed = new Subject<T>();

		private readonly Dictionary<Guid, T> atlas = new Dictionary<Guid, T>(Equality.StructComparer<Guid>());

		private readonly IAtlasStore<Guid, T> store;

		public AtlasStoreBuilder(IAtlasStore<Guid, T> store)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);

			this.store = store;
			this.Atlas = this.atlas.AsDictionaryView();
			this.BatchEdits = new BatchChanges(this);

			this.Added = this.added.AsObservable();
			this.Updated = this.updated.AsObservable();
			this.Removed = this.removed.AsObservable();

			this.atlas.AddMany(this.store.All().Select(x => KeyValuePair.New(x.Key, x)));
		}

		public IDictionaryView<Guid, T> Atlas { get; }

		public BatchChanges BatchEdits { get; }

		public DateTime LastUpdated => this.store.LastUpdated;

		public UniRx.IObservable<T> Added { get; }

		public UniRx.IObservable<T> Updated { get; }

		public UniRx.IObservable<T> Removed { get; }

		/// <inheritdoc />
		public bool IsDisposed => this.store.IsDisposed;

		public void AddOrUpdateMany(IEnumerable<T> values)
		{
			Require.That(!this.IsDisposed);
			Require.That(values != null);

			this.store.AddOrUpdateMany(values);

			foreach (var value in values)
			{
				var isUpdate = this.atlas.ContainsKey(value.Key);
				this.atlas[value.Key] = value;

				if (isUpdate)
				{
					this.updated.OnNext(value);
				}
				else
				{
					this.added.OnNext(value);
				}
			}
		}

		public void RemoveMany(IEnumerable<Guid> keys)
		{
			Require.That(!this.IsDisposed);
			Require.That(keys != null);

			this.store.RemoveMany(keys);

			foreach (var key in keys)
			{
				if (this.atlas.TryGetValue(key, out var value))
				{
					this.atlas.Remove(key);
					this.removed.OnNext(value);
				}
			}
		}

		public void DeleteAtlas()
		{
			Require.That(!this.IsDisposed);

			this.store.DeleteAtlas();

			var values = this.atlas.Values.ToArray();
			this.atlas.Clear();

			foreach (var value in values)
			{
				this.removed.OnNext(value);
			}
		}

		public class BatchChanges
		{
			private readonly List<Guid> removeKeys = new List<Guid>();

			private readonly AtlasStoreBuilder<T> builder;

			public BatchChanges(AtlasStoreBuilder<T> builder)
			{
				Require.That(builder != null);

				this.builder = builder;
			}

			public HashSet<Guid> KeysToKeep { get; } = new HashSet<Guid>(Equality.StructComparer<Guid>());

			public List<T> ValuesToChange { get; } = new List<T>();

			public void CommitChanges()
			{
				Require.That(!this.builder.IsDisposed);

				try
				{
					this.builder.AddOrUpdateMany(this.ValuesToChange);

					foreach (var key in this.builder.Atlas.Keys)
					{
						if (!this.KeysToKeep.Contains(key))
						{
							this.removeKeys.Add(key);
						}
					}

					this.builder.RemoveMany(this.removeKeys);
				}
				finally
				{
					this.ValuesToChange.Clear();
					this.KeysToKeep.Clear();
					this.removeKeys.Clear();
				}
			}
		}
	}
}
