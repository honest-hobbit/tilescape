﻿using System;
using System.Collections.Generic;
using System.IO;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.IO;
using Tilescape.Utility.Types;
using UniRx;

namespace Tilescape.Engine
{
	internal abstract class AbstractAtlasImporter<TValue, TRecord> : IAtlasImporter<TValue, TRecord>
		where TValue : IKeyed<Guid>, INamed
		where TRecord : AtlasRecord<TValue>
	{
		private readonly Subject<TRecord> added = new Subject<TRecord>();

		private readonly Subject<TRecord> updated = new Subject<TRecord>();

		private readonly Subject<Exception> error = new Subject<Exception>();

		private readonly Subject<IDictionaryView<Guid, TRecord>> passCompleted = new Subject<IDictionaryView<Guid, TRecord>>();

		private readonly Dictionary<Guid, TRecord> records = new Dictionary<Guid, TRecord>(Equality.StructComparer<Guid>());

		private readonly AtlasStoreBuilder<TValue> store;

		public AbstractAtlasImporter(IAtlasStore<Guid, TValue> store, string sourceFolderPath)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
			Require.That(PathUtility.IsPathValid(sourceFolderPath));

			this.store = new AtlasStoreBuilder<TValue>(store);
			this.store.Removed.Subscribe(x => this.records.Remove(x.Key));
			this.SourceFolderPath = Path.GetFullPath(sourceFolderPath);

			this.Records = this.records.AsDictionaryView();
			this.Added = this.added.AsObservable();
			this.Updated = this.updated.AsObservable();
			this.Error = this.error.AsObservable();
			this.PassCompleted = this.passCompleted.AsObservable();
		}

		/// <inheritdoc />
		public IDictionaryView<Guid, TRecord> Records { get; }

		/// <inheritdoc />
		public string SourceFolderPath { get; }

		/// <inheritdoc />
		public DateTime LastUpdated => this.store.LastUpdated;

		/// <inheritdoc />
		public bool IsDisposed => this.store.IsDisposed;

		/// <inheritdoc />
		public UniRx.IObservable<TRecord> Added { get; }

		/// <inheritdoc />
		public UniRx.IObservable<TRecord> Updated { get; }

		/// <inheritdoc />
		public UniRx.IObservable<TValue> Removed => this.store.Removed;

		/// <inheritdoc />
		public UniRx.IObservable<Exception> Error { get; }

		/// <inheritdoc />
		public UniRx.IObservable<IDictionaryView<Guid, TRecord>> PassCompleted { get; }

		/// <inheritdoc />
		public void UpdateAtlas()
		{
			IAtlasImporterContracts.UpdateAtlas(this);

			this.ProcessAllFolders(this.LastUpdated);
		}

		/// <inheritdoc />
		public void RebuildAtlas()
		{
			IAtlasImporterContracts.RebuildAtlas(this);

			this.store.DeleteAtlas();
			this.records.Clear();
			this.ProcessAllFolders(DateTime.MinValue);
		}

		/// <inheritdoc />
		public void DeleteAtlas()
		{
			IAtlasImporterContracts.DeleteAtlas(this);

			this.store.DeleteAtlas();
			this.records.Clear();
			this.passCompleted.OnNext(this.Records);
		}

		protected bool TryGetValue(Guid key, out TValue value) => this.store.Atlas.TryGetValue(key, out value);

		protected abstract ImportResult ImportRecord(string folder, DateTime lastUpdated, out TRecord record);

		private void ProcessAllFolders(DateTime lastUpdated)
		{
			this.CheckFolderAndSubfolders(this.SourceFolderPath, lastUpdated);

			try
			{
				this.store.BatchEdits.CommitChanges();
			}
			catch (Exception e)
			{
				this.error.OnNext(e);
			}
			finally
			{
				this.passCompleted.OnNext(this.Records);
			}
		}

		private void CheckFolderAndSubfolders(string folder, DateTime lastUpdated)
		{
			Require.That(!folder.IsNullOrWhiteSpace());

			this.CheckFolder(folder, lastUpdated);
			this.CheckSubfolders(folder, lastUpdated);
		}

		private void CheckSubfolders(string folder, DateTime lastUpdated)
		{
			try
			{
				var folders = Directory.GetDirectories(folder);
				for (int index = 0; index < folders.Length; index++)
				{
					this.CheckFolderAndSubfolders(folders[index], lastUpdated);
				}
			}
			catch (Exception e)
			{
				this.error.OnNext(e);
			}
		}

		private void CheckFolder(string folder, DateTime lastUpdated)
		{
			try
			{
				var result = this.ImportRecord(folder, lastUpdated, out var record);
				if (result == ImportResult.NothingFound)
				{
					return;
				}

				if (record == null)
				{
					throw new ArgumentNullException(nameof(record));
				}

				if (!this.store.BatchEdits.KeysToKeep.Add(record.Key))
				{
					throw new IOException(folder + " contains a duplicated key.");
				}

				this.records[record.Key] = record;

				switch (result)
				{
					case ImportResult.NoUpdate: return;

					case ImportResult.RecordUpdated:
						this.updated.OnNext(record);
						break;

					case ImportResult.ValueUpdated:
						this.updated.OnNext(record);
						this.store.BatchEdits.ValuesToChange.Add(record.Value);
						break;

					case ImportResult.ValueAdded:
						this.added.OnNext(record);
						this.store.BatchEdits.ValuesToChange.Add(record.Value);
						break;

					default: throw InvalidEnumArgument.CreateException(nameof(result), result);
				}
			}
			catch (Exception e)
			{
				this.error.OnNext(e);
			}
		}
	}
}
