﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class ArrayAtlas<TKey, TValue> : IDictionaryView<TKey, TValue>
		where TKey : struct, IIndexed<ushort>
		where TValue : class, IKeyed<TKey>
	{
		private readonly TValue[] values;

		public ArrayAtlas(params TValue[] values)
			: this((IEnumerable<TValue>)values)
		{
		}

		public ArrayAtlas(IEnumerable<TValue> values)
		{
			Require.That(values.AllAndSelfNotNull());

			int size = values.Max(x => x.Key.Index) + 1;
			this.values = new TValue[size];

			foreach (var value in values)
			{
				var index = value.Key.Index;

				Require.That(this.values[index] == null, $"{nameof(values)} must not contain any duplicate keys.");

				this.values[index] = value;
			}

			this.Count = this.values.Where(x => x != null).Count();
			this.Keys = new KeysView(this);
			this.Values = new ValuesView(this);
		}

		/// <inheritdoc />
		public int Count { get; }

		/// <inheritdoc />
		public ISetView<TKey> Keys { get; }

		/// <inheritdoc />
		public ICollectionView<TValue> Values { get; }

		/// <inheritdoc />
		public TValue this[TKey key]
		{
			get
			{
				IDictionaryViewContracts.Indexer(this, key);

				return this.values[key.Index];
			}
		}

		/// <inheritdoc />
		public bool ContainsKey(TKey key)
		{
			IDictionaryViewContracts.ContainsKey(key);

			return this.Keys.Contains(key);
		}

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value)
		{
			IDictionaryViewContracts.TryGetValue(key);

			var index = key.Index;
			if (!this.values.IsIndexInBounds(index))
			{
				value = null;
				return false;
			}

			value = this.values[index];
			return value != null;
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() =>
			this.Values.Select(x => KeyValuePair.New(x.Key, x)).GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		private class KeysView : ISetView<TKey>
		{
			private readonly ArrayAtlas<TKey, TValue> parent;

			public KeysView(ArrayAtlas<TKey, TValue> parent)
			{
				Require.That(parent != null);

				this.parent = parent;
			}

			/// <inheritdoc />
			public int Count => this.parent.Count;

			/// <inheritdoc />
			public bool Contains(TKey key)
			{
				var index = key.Index;
				if (!this.parent.values.IsIndexInBounds(index))
				{
					return false;
				}

				return this.parent.values[index] != null;
			}

			/// <inheritdoc />
			public IEnumerator<TKey> GetEnumerator() =>
				this.parent.values.Where(x => x != null).Select(x => x.Key).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}

		private class ValuesView : ICollectionView<TValue>
		{
			private readonly ArrayAtlas<TKey, TValue> parent;

			public ValuesView(ArrayAtlas<TKey, TValue> parent)
			{
				Require.That(parent != null);

				this.parent = parent;
			}

			/// <inheritdoc />
			public int Count => this.parent.Count;

			/// <inheritdoc />
			public IEnumerator<TValue> GetEnumerator() => this.parent.values.Where(x => x != null).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}
	}
}
