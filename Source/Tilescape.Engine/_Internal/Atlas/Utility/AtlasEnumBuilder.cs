﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.IO;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class AtlasEnumBuilder
	{
		private static readonly int EstimatedHeaderSize = 70;

		private static readonly int EstimatedSizePerEntry = 70;

		private static readonly TextInfo TextInfo = CultureInfo.InvariantCulture.TextInfo;

		private readonly StringBuilder builder = new StringBuilder();

		public string Namespace { get; set; } = string.Empty;

		public string ClassName { get; set; } = string.Empty;

		public SpaceHandling Spacing { get; set; } = SpaceHandling.RemoveSpacesKeepUnderscores;

		public void CreateFile(string filePath, IEnumerable<KeyValuePair<Guid, string>> shapes)
		{
			Require.That(PathUtility.IsPathValid(filePath));
			Require.That(shapes != null);

			File.WriteAllText(filePath, this.BuildString(shapes));
		}

		public string BuildString(IEnumerable<KeyValuePair<Guid, string>> shapes)
		{
			Require.That(shapes != null);

			this.builder.Clear();
			this.builder.EnsureCapacity(EstimatedHeaderSize + (shapes.CountExtended() * EstimatedSizePerEntry));

			if (!this.Namespace.IsNullOrWhiteSpace() && !this.ClassName.IsNullOrWhiteSpace())
			{
				this.builder.AppendLine("using System;");
				this.builder.AppendLine();
				this.builder.AppendLine("namespace " + this.Namespace);
				this.builder.AppendLine("{");
				this.builder.AppendLine("public static class " + this.ClassName);
				this.builder.AppendLine("{");
			}

			this.builder.AppendLine("#region Atlas Guids");
			this.builder.AppendLine();

			foreach (var record in shapes.OrderBy(x => x.Value))
			{
				this.builder.Append("public static Guid ");
				this.AddPropertyName(record.Value);
				this.builder.Append(" { get; } = new Guid(\"");
				this.builder.Append(record.Key.ToString());
				this.builder.AppendLine("\");");
				this.builder.AppendLine();
			}

			this.builder.Append("#endregion");

			if (!this.Namespace.IsNullOrWhiteSpace() && !this.ClassName.IsNullOrWhiteSpace())
			{
				this.builder.AppendLine();
				this.builder.AppendLine("}");
				this.builder.AppendLine("}");
			}

			return this.builder.ToString();
		}

		private void AddPropertyName(string name)
		{
			foreach (char c in TextInfo.ToTitleCase(name))
			{
				if (char.IsLetterOrDigit(c))
				{
					this.builder.Append(c);
				}
				else
				{
					switch (this.Spacing)
					{
						case SpaceHandling.RemoveSpacesAndUnderscores: break;
						case SpaceHandling.RemoveSpacesKeepUnderscores:
							if (c == '_')
							{
								this.builder.Append('_');
							}

							break;

						case SpaceHandling.ReplaceSpacesWithUnderscores:
							if (c == ' ' || c == '_')
							{
								this.builder.Append('_');
							}

							break;

						default: throw InvalidEnumArgument.CreateException(nameof(this.Spacing), this.Spacing);
					}
				}
			}
		}

		public enum SpaceHandling
		{
			RemoveSpacesAndUnderscores,

			RemoveSpacesKeepUnderscores,

			ReplaceSpacesWithUnderscores
		}
	}
}
