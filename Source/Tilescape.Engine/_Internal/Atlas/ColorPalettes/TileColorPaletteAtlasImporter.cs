﻿using System;
using System.Drawing;
using System.IO;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class TileColorPaletteAtlasImporter : AbstractAtlasImporter<TileColorPalette, TileColorPaletteAtlasRecord>
	{
		private readonly TileColorPaletteJsonData jsonData = new TileColorPaletteJsonData();

		public TileColorPaletteAtlasImporter(ITilescapeStore store, string sourceFolder)
			: base(store?.ColorPalettes, sourceFolder)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);
		}

		/// <inheritdoc />
		protected override ImportResult ImportRecord(string folder, DateTime lastUpdated, out TileColorPaletteAtlasRecord record)
		{
			Require.That(!folder.IsNullOrWhiteSpace());

			record = null;

			var dataFilePath = Path.Combine(folder, AtlasConstants.TileColorPalettes.DataFileName);
			var paletteFilePath = Path.Combine(folder, AtlasConstants.TileColorPalettes.PaletteFileName);

			var dataFileExists = File.Exists(dataFilePath);
			var paletteFileExists = File.Exists(paletteFilePath);

			if (!dataFileExists && !paletteFileExists)
			{
				return ImportResult.NothingFound;
			}

			if (dataFileExists && !paletteFileExists)
			{
				throw new FileNotFoundException("No corresponding palette file found.", paletteFilePath);
			}

			var newPalette = false;
			var importPaletteFile = false;
			Guid key;

			// create or read the json data file
			this.jsonData.Clear();
			if (!dataFileExists)
			{
				// create new json data file
				newPalette = true;
				key = Guid.NewGuid();
				this.jsonData.KeyGuid = key;
				this.jsonData.Name = new DirectoryInfo(folder).Name.Trim();
				File.WriteAllText(dataFilePath, JsonUtility.ToJson(this.jsonData, AtlasConstants.UseJsonPrettyPrinting));
			}
			else
			{
				// read in json data file
				this.ValidateDataFile(dataFilePath, out key, out var dataFileChanged);

				if (!dataFileChanged)
				{
					dataFileChanged =
						File.GetLastWriteTimeUtc(dataFilePath) > lastUpdated ||
						File.GetCreationTimeUtc(dataFilePath) > lastUpdated;
				}

				importPaletteFile =
					File.GetLastWriteTimeUtc(paletteFilePath) > lastUpdated ||
					File.GetCreationTimeUtc(paletteFilePath) > lastUpdated;

				if (!dataFileChanged && !importPaletteFile)
				{
					// no change detected in the source files so check what was loaded from the store
					if (this.Records.TryGetValue(key, out record))
					{
						if (record.FolderPath == folder)
						{
							return ImportResult.NoUpdate;
						}
						else
						{
							record = new TileColorPaletteAtlasRecord(record.Value, folder);
							return ImportResult.RecordUpdated;
						}
					}
					else if (this.TryGetValue(key, out var loadedPalette))
					{
						record = new TileColorPaletteAtlasRecord(loadedPalette, folder);
						return ImportResult.NoUpdate;
					}
					else
					{
						newPalette = true;
					}
				}
			}

			// a tile color palette is being created or updated
			if (newPalette || !this.TryGetValue(key, out var palette))
			{
				palette = new TileColorPalette();
				newPalette = true;
				importPaletteFile = true;
			}

			palette.Key = key;
			palette.Name = this.jsonData.Name;

			if (importPaletteFile)
			{
				ImportPaletteFile(paletteFilePath, palette);
			}

			record = new TileColorPaletteAtlasRecord(palette, folder);
			return newPalette ? ImportResult.ValueAdded : ImportResult.ValueUpdated;
		}

		private static void ImportPaletteFile(string paletteFilePath, TileColorPalette palette)
		{
			Require.That(!paletteFilePath.IsNullOrWhiteSpace());
			Require.That(palette != null);

			using (var image = new Bitmap(paletteFilePath))
			{
				if (image.Height != 1 || image.Width != ColorPalette.RequirredLength)
				{
					throw new InvalidOperationException(
						$"Color palette image must be 1 pixel tall and {ColorPalette.RequirredLength} pixels wide.");
				}

				int max = ColorPalette.RequirredLength - 1;
				for (int iX = 0; iX < max;)
				{
					// color read in is stored 1 pixel to the right (0 => 1, 1 => 2, etc)
					// this leaves slot 0 for the empty color, and ignores color 255 from the source image
					var color = image.GetPixel(iX, 0);
					iX++;
					palette.Array[iX] = new Color32(color.R, color.G, color.B, color.A);
				}

				// slot 0 is reserved for the empty voxel / transparent color
				palette.Array[0] = ColorPalette.EmptyColor;
			}
		}

		private void ValidateDataFile(string dataFilePath, out Guid key, out bool dataFileChanged)
		{
			Require.That(!dataFilePath.IsNullOrWhiteSpace());

			dataFileChanged = false;
			JsonUtility.FromJsonOverwrite(File.ReadAllText(dataFilePath), this.jsonData);

			// validate the key
			key = this.jsonData.KeyGuid;
			if (key == Guid.Empty)
			{
				key = Guid.NewGuid();
				this.jsonData.KeyGuid = key;
				dataFileChanged = true;
			}

			// validate the name
			if (this.jsonData.Name.IsNullOrWhiteSpace())
			{
				this.jsonData.Name = "Missing Name";
				dataFileChanged = true;
			}

			if (dataFileChanged)
			{
				File.WriteAllText(dataFilePath, JsonUtility.ToJson(this.jsonData, AtlasConstants.UseJsonPrettyPrinting));
			}
		}
	}
}
