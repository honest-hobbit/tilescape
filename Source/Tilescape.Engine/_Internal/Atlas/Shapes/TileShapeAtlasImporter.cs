﻿using System;
using System.IO;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class TileShapeAtlasImporter : AbstractAtlasImporter<TileShape, TileShapeAtlasRecord>
	{
		private readonly TileShapeJsonData jsonData = new TileShapeJsonData();

		private readonly ITilescapeConfigStore configStore;

		private readonly TileShapeBuilder shapeBuilder;

		private readonly VoxelVolume shapeVolume;

		public TileShapeAtlasImporter(ITilescapeStore store, string sourceFolder)
			: base(store?.Shapes, sourceFolder)
		{
			Require.That(store != null);
			Require.That(!store.IsDisposed);

			this.configStore = store.Config;
			this.shapeBuilder = new TileShapeBuilder(this.configStore.Stages)
			{
				MeshingMode = MeshingMode.Greedy,
			};

			this.shapeVolume = new VoxelVolume(new Index3D(this.configStore.Stages.TileLengthInVoxels));
		}

		public int FallbackColliderFillThreshold
		{
			get => this.configStore.TileShapeColliderFillThreshold;
			set => this.configStore.TileShapeColliderFillThreshold = value;
		}

		/// <inheritdoc />
		protected override ImportResult ImportRecord(string folder, DateTime lastUpdated, out TileShapeAtlasRecord record)
		{
			Require.That(!folder.IsNullOrWhiteSpace());

			record = null;

			var dataFilePath = Path.Combine(folder, AtlasConstants.TileShapes.DataFileName);
			var shapeFilePath = Path.Combine(folder, AtlasConstants.TileShapes.ShapeFileName);

			var dataFileExists = File.Exists(dataFilePath);
			var shapeFileExists = File.Exists(shapeFilePath);

			if (!dataFileExists && !shapeFileExists)
			{
				return ImportResult.NothingFound;
			}

			if (dataFileExists && !shapeFileExists)
			{
				throw new FileNotFoundException("No corresponding shape file found.", shapeFilePath);
			}

			var newShape = false;
			var importShapeFile = false;
			Guid key;

			// create or read the json data file
			this.jsonData.Clear();
			if (!dataFileExists)
			{
				// create new json data file
				newShape = true;
				key = Guid.NewGuid();
				this.jsonData.KeyGuid = key;
				this.jsonData.Name = new DirectoryInfo(folder).Name.Trim();
				File.WriteAllText(dataFilePath, JsonUtility.ToJson(this.jsonData, AtlasConstants.UseJsonPrettyPrinting));
			}
			else
			{
				// read in json data file
				this.ValidateDataFile(dataFilePath, out key, out var dataFileChanged);

				if (!dataFileChanged)
				{
					dataFileChanged =
						File.GetLastWriteTimeUtc(dataFilePath) > lastUpdated ||
						File.GetCreationTimeUtc(dataFilePath) > lastUpdated;
				}

				importShapeFile =
					File.GetLastWriteTimeUtc(shapeFilePath) > lastUpdated ||
					File.GetCreationTimeUtc(shapeFilePath) > lastUpdated;

				if (!dataFileChanged && !importShapeFile)
				{
					// no change detected in the source files so check what was loaded from the store
					if (this.Records.TryGetValue(key, out record))
					{
						if (record.FolderPath == folder)
						{
							return ImportResult.NoUpdate;
						}
						else
						{
							record = new TileShapeAtlasRecord(record.Value, folder);
							return ImportResult.RecordUpdated;
						}
					}
					else if (this.TryGetValue(key, out var loadedShape))
					{
						record = new TileShapeAtlasRecord(loadedShape, folder);
						return ImportResult.NoUpdate;
					}
					else
					{
						newShape = true;
					}
				}
			}

			// a tile shape is being created or updated
			if (newShape || !this.TryGetValue(key, out var shape))
			{
				shape = new TileShape(this.shapeBuilder.TileSizeExponent);
				newShape = true;
				importShapeFile = true;
			}

			shape.Key = key;
			shape.Name = this.jsonData.Name;

			if (importShapeFile)
			{
				this.shapeVolume.ClearAllFrames();
				MagicaVoxelImporter.FillVolume(shapeFilePath, this.shapeVolume);
				this.shapeVolume.Frames[0].Voxels.CopyTo(shape.Voxels.Array, 0);
			}

			this.shapeBuilder.ColliderFillThreshold =
				(this.jsonData.ColliderFillThreshold != AtlasConstants.TileShapes.UseFallbackColliderFillThreshold) ?
				this.jsonData.ColliderFillThreshold : this.FallbackColliderFillThreshold;

			this.shapeBuilder.BuildShape(shape);

			record = new TileShapeAtlasRecord(shape, folder);
			return newShape ? ImportResult.ValueAdded : ImportResult.ValueUpdated;
		}

		private void ValidateDataFile(string dataFilePath, out Guid key, out bool dataFileChanged)
		{
			Require.That(!dataFilePath.IsNullOrWhiteSpace());

			dataFileChanged = false;
			JsonUtility.FromJsonOverwrite(File.ReadAllText(dataFilePath), this.jsonData);

			// validate the key
			key = this.jsonData.KeyGuid;
			if (key == Guid.Empty)
			{
				key = Guid.NewGuid();
				this.jsonData.KeyGuid = key;
				dataFileChanged = true;
			}

			// validate the name
			if (this.jsonData.Name.IsNullOrWhiteSpace())
			{
				this.jsonData.Name = "Missing Name";
				dataFileChanged = true;
			}

			// validate the collider fill threshold
			if (this.jsonData.ColliderFillThreshold < 0 &&
				this.jsonData.ColliderFillThreshold != AtlasConstants.TileShapes.UseFallbackColliderFillThreshold)
			{
				this.jsonData.ColliderFillThreshold = AtlasConstants.TileShapes.UseFallbackColliderFillThreshold;
				dataFileChanged = true;
			}

			if (dataFileChanged)
			{
				File.WriteAllText(dataFilePath, JsonUtility.ToJson(this.jsonData, AtlasConstants.UseJsonPrettyPrinting));
			}
		}
	}
}
