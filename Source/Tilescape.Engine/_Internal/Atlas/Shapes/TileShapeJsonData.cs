﻿using System;
using System.Diagnostics.CodeAnalysis;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	[SuppressMessage("StyleCop", "SA1401:FieldsMustBePrivate", Justification = "JSON serializer requires public fields.")]
	[Serializable]
	internal class TileShapeJsonData
	{
		public string Key = null;

		public string Name = null;

		public int ColliderFillThreshold = AtlasConstants.TileShapes.UseFallbackColliderFillThreshold;

		public Guid KeyGuid
		{
			get => GuidUtility.FromNullableString(this.Key);
			set => this.Key = GuidUtility.ToNullableString(value);
		}

		public void Clear()
		{
			this.Key = null;
			this.Name = null;
			this.ColliderFillThreshold = AtlasConstants.TileShapes.UseFallbackColliderFillThreshold;
		}
	}
}
