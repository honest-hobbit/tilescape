﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	internal class TileChunkModifiedQueue
	{
		private static readonly bool GuaranteeSingleFrame = true;

		private readonly AdjacencyCube<bool> resyncChunks = new AdjacencyCube<bool>(false);

		private readonly List<TileChunk> modifiedChunks = new List<TileChunk>();

		private readonly IListView<TileChunk> modifiedChunksView;

		private readonly TileChunkCache chunkCache;

		private readonly ITileChunkLifespanManager chunkStore;

		private readonly ResyncUnityQueue resyncUnity;

		private readonly int minTileIndex;

		private readonly int maxTileIndex;

		public TileChunkModifiedQueue(TileChunkCache chunkCache, ITileChunkLifespanManager chunkStore, ResyncUnityQueue resyncUnity)
		{
			Require.That(chunkCache != null);
			Require.That(chunkStore != null);
			Require.That(resyncUnity != null);

			this.chunkCache = chunkCache;
			this.chunkStore = chunkStore;
			this.resyncUnity = resyncUnity;

			this.minTileIndex = chunkCache.Config.ChunkMinTileIndex + 1;
			this.maxTileIndex = chunkCache.Config.ChunkMaxTileIndex - 1;

			this.modifiedChunksView = this.modifiedChunks.AsListView();
		}

		public void EnqueueChunkModified(TileChunk chunk)
		{
			Require.That(chunk != null);
			Require.That(chunk.IsInitialized);
			Require.That(!chunk.IsUniform);

			if (chunk.IsModified)
			{
				return;
			}

			((IFlagsForTileChunkModifiedQueue)chunk).IsModified = true;
			this.modifiedChunks.Add(chunk);
		}

		public void ProcessAndSaveModifiedChunks()
		{
			for (int index = 0; index < this.modifiedChunks.Count; index++)
			{
				var chunk = this.modifiedChunks[index];

				this.TryToUnifyChunk(chunk);
				this.ResyncChunkAndAdjacent(chunk);

				// do not clear TileChunk.ModifiedTiles, they're needed by ResyncUnityQueue
				((IFlagsForTileChunkModifiedQueue)chunk).IsModified = false;
			}

			this.chunkStore.Persist(this.modifiedChunksView);
			this.modifiedChunks.Clear();
		}

		private void TryToUnifyChunk(TileChunk chunk)
		{
			Require.That(chunk != null);
			Require.That(chunk.IsInitialized);
			Require.That(!chunk.IsUniform);

			var tile = chunk.Tiles[0];
			for (int index = 1; index < chunk.Tiles.Length; index++)
			{
				if (chunk.Tiles[index] != tile)
				{
					return;
				}
			}

			chunk.UniformTile = tile;
			this.chunkCache.ReturnTilesArrayToPool(chunk);
		}

		private void ResyncChunkAndAdjacent(TileChunk chunk)
		{
			Require.That(chunk != null);
			Require.That(chunk.IsInitialized);

			((IFlagsForTileChunkModifiedQueue)chunk).IsUnitySynchronized = false;
			this.resyncUnity.EnqueueChunkResync(chunk, GuaranteeSingleFrame);

			this.resyncChunks.SetAllTo(false);
			foreach (var tileIndex in chunk.ModifiedTiles.Keys)
			{
				this.CheckTileIndexForResyncingAdjacentChunks(tileIndex);
			}

			for (int index = 0; index < this.resyncChunks.Length; index++)
			{
				if (this.resyncChunks[index])
				{
					this.TryResyncAdjacentChunk(chunk.Key + Adjacency.ToIndex(index));
				}
			}
		}

		private void CheckTileIndexForResyncingAdjacentChunks(Index3D tileIndex)
		{
			if (tileIndex.X <= this.minTileIndex)
			{
				if (tileIndex.Z <= this.minTileIndex)
				{
					// NZN (shared edge)
					this.resyncChunks[Adjacency.NZN] = true;
					this.resyncChunks[Adjacency.ZZN] = true;
					this.resyncChunks[Adjacency.NZZ] = true;

					if (tileIndex.Y <= this.minTileIndex)
					{
						// NNN
						this.resyncChunks[Adjacency.NNN] = true;
						this.resyncChunks[Adjacency.ZNN] = true;
						this.resyncChunks[Adjacency.NNZ] = true;
						this.resyncChunks[Adjacency.ZNZ] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// NPN
						this.resyncChunks[Adjacency.NPN] = true;
						this.resyncChunks[Adjacency.ZPN] = true;
						this.resyncChunks[Adjacency.NPZ] = true;
						this.resyncChunks[Adjacency.ZPZ] = true;
					}
				}
				else if (tileIndex.Z >= this.maxTileIndex)
				{
					// NZP (shared edge)
					this.resyncChunks[Adjacency.NZP] = true;
					this.resyncChunks[Adjacency.NZZ] = true;
					this.resyncChunks[Adjacency.ZZP] = true;

					if (tileIndex.Y <= this.minTileIndex)
					{
						// NNP
						this.resyncChunks[Adjacency.NNP] = true;
						this.resyncChunks[Adjacency.NNZ] = true;
						this.resyncChunks[Adjacency.ZNP] = true;
						this.resyncChunks[Adjacency.ZNZ] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// NPP
						this.resyncChunks[Adjacency.NPP] = true;
						this.resyncChunks[Adjacency.NPZ] = true;
						this.resyncChunks[Adjacency.ZPP] = true;
						this.resyncChunks[Adjacency.ZPZ] = true;
					}
				}
				else
				{
					if (tileIndex.Y <= this.minTileIndex)
					{
						// NNZ
						this.resyncChunks[Adjacency.NNZ] = true;
						this.resyncChunks[Adjacency.NZZ] = true;
						this.resyncChunks[Adjacency.ZNZ] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// NPZ
						this.resyncChunks[Adjacency.NPZ] = true;
						this.resyncChunks[Adjacency.ZPZ] = true;
						this.resyncChunks[Adjacency.NZZ] = true;
					}
					else
					{
						// NZZ
						this.resyncChunks[Adjacency.NZZ] = true;
					}
				}
			}
			else if (tileIndex.X >= this.maxTileIndex)
			{
				if (tileIndex.Z <= this.minTileIndex)
				{
					// PZN (shared edge)
					this.resyncChunks[Adjacency.PZN] = true;
					this.resyncChunks[Adjacency.PZZ] = true;
					this.resyncChunks[Adjacency.ZZN] = true;

					if (tileIndex.Y <= this.minTileIndex)
					{
						// PNN
						this.resyncChunks[Adjacency.PNN] = true;
						this.resyncChunks[Adjacency.ZNN] = true;
						this.resyncChunks[Adjacency.PNZ] = true;
						this.resyncChunks[Adjacency.ZNZ] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// PPN
						this.resyncChunks[Adjacency.PPN] = true;
						this.resyncChunks[Adjacency.ZPN] = true;
						this.resyncChunks[Adjacency.PPZ] = true;
						this.resyncChunks[Adjacency.ZPZ] = true;
					}
				}
				else if (tileIndex.Z >= this.maxTileIndex)
				{
					// PZP (shared edge)
					this.resyncChunks[Adjacency.PZP] = true;
					this.resyncChunks[Adjacency.ZZP] = true;
					this.resyncChunks[Adjacency.PZZ] = true;

					if (tileIndex.Y <= this.minTileIndex)
					{
						// PNP
						this.resyncChunks[Adjacency.PNP] = true;
						this.resyncChunks[Adjacency.PNZ] = true;
						this.resyncChunks[Adjacency.ZNP] = true;
						this.resyncChunks[Adjacency.ZNZ] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// PPP
						this.resyncChunks[Adjacency.PPP] = true;
						this.resyncChunks[Adjacency.ZPP] = true;
						this.resyncChunks[Adjacency.PPZ] = true;
						this.resyncChunks[Adjacency.ZPZ] = true;
					}
				}
				else
				{
					if (tileIndex.Y <= this.minTileIndex)
					{
						// PNZ
						this.resyncChunks[Adjacency.PNZ] = true;
						this.resyncChunks[Adjacency.PZZ] = true;
						this.resyncChunks[Adjacency.ZNZ] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// PPZ
						this.resyncChunks[Adjacency.PPZ] = true;
						this.resyncChunks[Adjacency.ZPZ] = true;
						this.resyncChunks[Adjacency.PZZ] = true;
					}
					else
					{
						// PZZ
						this.resyncChunks[Adjacency.PZZ] = true;
					}
				}
			}
			else
			{
				if (tileIndex.Z <= this.minTileIndex)
				{
					if (tileIndex.Y <= this.minTileIndex)
					{
						// ZNN
						this.resyncChunks[Adjacency.ZNN] = true;
						this.resyncChunks[Adjacency.ZNZ] = true;
						this.resyncChunks[Adjacency.ZZN] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// ZPN
						this.resyncChunks[Adjacency.ZPN] = true;
						this.resyncChunks[Adjacency.ZPZ] = true;
						this.resyncChunks[Adjacency.ZZN] = true;
					}
					else
					{
						// ZZN
						this.resyncChunks[Adjacency.ZZN] = true;
					}
				}
				else if (tileIndex.Z >= this.maxTileIndex)
				{
					if (tileIndex.Y <= this.minTileIndex)
					{
						// ZNP
						this.resyncChunks[Adjacency.ZNP] = true;
						this.resyncChunks[Adjacency.ZNZ] = true;
						this.resyncChunks[Adjacency.ZZP] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// ZPP
						this.resyncChunks[Adjacency.ZPP] = true;
						this.resyncChunks[Adjacency.ZPZ] = true;
						this.resyncChunks[Adjacency.ZZP] = true;
					}
					else
					{
						// ZZP
						this.resyncChunks[Adjacency.ZZP] = true;
					}
				}
				else
				{
					if (tileIndex.Y <= this.minTileIndex)
					{
						// ZNZ
						this.resyncChunks[Adjacency.ZNZ] = true;
					}
					else if (tileIndex.Y >= this.maxTileIndex)
					{
						// ZPZ
						this.resyncChunks[Adjacency.ZPZ] = true;
					}

					// ZZZ (do nothing, center is handled more efficiently)
				}
			}
		}

		private void TryResyncAdjacentChunk(TileChunkKey key)
		{
			if (this.chunkCache.TryGetChunk(key, out var chunk))
			{
				((IFlagsForTileChunkModifiedQueue)chunk).IsUnitySynchronized = false;
				this.resyncUnity.EnqueueChunkResync(chunk, GuaranteeSingleFrame);
			}
		}
	}
}
