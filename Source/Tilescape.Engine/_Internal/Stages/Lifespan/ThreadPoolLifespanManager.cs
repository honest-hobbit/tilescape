﻿using Tilescape.Utility.Unity.Concurrency;

namespace Tilescape.Engine
{
	internal class ThreadPoolLifespanManager : SimpleLifespanManager
	{
		public ThreadPoolLifespanManager(ITileChunkPopulator populator)
			: base(populator)
		{
		}

		/// <inheritdoc />
		public override void Populate(TileChunk chunk)
		{
			ITileChunkLifespanManagerContracts.Populate(this, chunk);

			UnityThreadPool.QueueUserWorkItem(
				state =>
				{
					var tileChunk = (TileChunk)state;
					this.Populator.Populate(tileChunk);
					this.OnChunkPopulated(tileChunk);
				}, chunk);
		}
	}
}
