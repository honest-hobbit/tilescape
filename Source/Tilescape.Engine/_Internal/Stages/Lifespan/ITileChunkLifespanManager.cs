﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Disposables;
using UniRx;

namespace Tilescape.Engine
{
	internal interface ITileChunkLifespanManager : IVisiblyDisposable
	{
		IObservable<TileChunk> ChunkPopulated { get; }

		void Populate(TileChunk chunk);

		void Persist(IListView<TileChunk> chunks);
	}
}
