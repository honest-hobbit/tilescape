﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using UniRx;

namespace Tilescape.Engine
{
	internal class SimpleLifespanManager : AbstractDisposable, ITileChunkLifespanManager
	{
		private readonly Subject<TileChunk> chunkPopulated = new Subject<TileChunk>();

		public SimpleLifespanManager(ITileChunkPopulator populator)
		{
			Require.That(populator != null);

			this.Populator = populator;
			this.ChunkPopulated = this.chunkPopulated.AsObservable();
		}

		/// <inheritdoc />
		public UniRx.IObservable<TileChunk> ChunkPopulated { get; }

		protected ITileChunkPopulator Populator { get; }

		/// <inheritdoc />
		public virtual void Populate(TileChunk chunk)
		{
			ITileChunkLifespanManagerContracts.Populate(this, chunk);

			this.Populator.Populate(chunk);
			this.OnChunkPopulated(chunk);
		}

		/// <inheritdoc />
		public virtual void Persist(IListView<TileChunk> chunks)
		{
			ITileChunkLifespanManagerContracts.Persist(this, chunks);
		}

		/// <inheritdoc />
		protected override void ManagedDisposal()
		{
		}

		protected void OnChunkPopulated(TileChunk chunk)
		{
			Require.That(chunk != null);

			this.chunkPopulated.OnNext(chunk);
		}
	}
}
