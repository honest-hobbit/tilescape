﻿using System.Diagnostics;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class ITileChunkLifespanManagerContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Populate(ITileChunkLifespanManager instance, TileChunk chunk)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(chunk != null);
			Require.That(chunk.Tiles != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Persist(ITileChunkLifespanManager instance, IListView<TileChunk> chunks)
		{
			Require.That(instance != null);
			Require.That(!instance.IsDisposed);
			Require.That(chunks.AllAndSelfNotNull());
		}
	}
}
