﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Unity.Concurrency;
using UniRx;

namespace Tilescape.Engine
{
	internal class ThreadedStoreLifespanManager : AbstractDisposable, ITileChunkLifespanManager
	{
		// KeyValuePair<object, Action<object>> used to avoid creating closures
		private readonly BlockingCollection<KeyValuePair<object, Action<object>>> storeActions =
			new BlockingCollection<KeyValuePair<object, Action<object>>>();

		private readonly Subject<TileChunk> chunkPopulated = new Subject<TileChunk>();

		private readonly ITilescapeStore store;

		private readonly ITileChunkPopulator fallback;

		private readonly bool runFallbackOnThreadPool;

		public ThreadedStoreLifespanManager(ITilescapeStore store, ITileChunkPopulator fallback, bool runFallbackOnThreadPool = false)
		{
			Require.That(store != null);

			this.store = store;
			this.fallback = fallback;
			this.runFallbackOnThreadPool = runFallbackOnThreadPool;
			this.ChunkPopulated = this.chunkPopulated.AsObservable();

			var thread = new Thread(state => this.RunStore());
			thread.Start();
		}

		/// <inheritdoc />
		public UniRx.IObservable<TileChunk> ChunkPopulated { get; }

		/// <inheritdoc />
		public void Populate(TileChunk chunk)
		{
			ITileChunkLifespanManagerContracts.Populate(this, chunk);

			this.storeActions.Add(KeyValuePair.New<object, Action<object>>(chunk, state =>
			{
				var tileChunk = (TileChunk)state;
				var entity = this.store.Chunks.Get(tileChunk.Key);
				tileChunk.Entity = entity;

				if (entity.Data != null)
				{
					this.store.Chunks.Serializer.DeserializeInline(entity.Data, tileChunk);
					entity.Data = null;
					this.chunkPopulated.OnNext(tileChunk);
				}
				else
				{
					if (this.runFallbackOnThreadPool)
					{
						UnityThreadPool.QueueUserWorkItem(
							threadState =>
							{
								var threadTileChunk = (TileChunk)threadState;
								this.fallback.Populate(threadTileChunk);
								this.chunkPopulated.OnNext(threadTileChunk);
							}, tileChunk);
					}
					else
					{
						this.fallback.Populate(tileChunk);
						this.chunkPopulated.OnNext(tileChunk);
					}
				}
			}));
		}

		/// <inheritdoc />
		public void Persist(IListView<TileChunk> chunks)
		{
			ITileChunkLifespanManagerContracts.Persist(this, chunks);

			// serialize chunks while still on the tile main thread to avoid threading problems with the background store thread
			var records = new SaveRecord[chunks.Count];
			for (int index = 0; index < chunks.Count; index++)
			{
				var chunk = chunks[index];
				////chunk.Entity.ChunkKey = chunk.Key;
				records[index] = new SaveRecord(chunk.Entity, this.store.Chunks.Serializer.Serialize(chunk));
			}

			this.storeActions.Add(KeyValuePair.New<object, Action<object>>(records, state =>
			{
				var chunkRecords = (SaveRecord[])state;
				chunkRecords.ForEachListItem(x => x.Entity.Data = x.NewData);
				this.store.Chunks.AddOrUpdate(chunkRecords.Select(pair => pair.Entity));
			}));
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.storeActions.CompleteAdding();

		private void RunStore()
		{
			try
			{
				foreach (var pair in this.storeActions.GetConsumingEnumerable())
				{
					// GetConsumingEnumerable will block until an item is received and end when adding is completed
					pair.Value(pair.Key);
				}
			}
			finally
			{
				this.Dispose();
				this.storeActions.Dispose();
				this.store.Dispose();
			}
		}

		private struct SaveRecord
		{
			public SaveRecord(TileChunkEntity entity, byte[] newData)
			{
				Require.That(entity != null);
				Require.That(newData != null);

				this.Entity = entity;
				this.NewData = newData;
			}

			public TileChunkEntity Entity { get; }

			public byte[] NewData { get; }
		}
	}
}
