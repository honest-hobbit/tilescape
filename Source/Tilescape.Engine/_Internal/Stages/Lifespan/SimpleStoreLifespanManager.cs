﻿using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Unity.Concurrency;
using UniRx;

namespace Tilescape.Engine
{
	internal class SimpleStoreLifespanManager : AbstractDisposable, ITileChunkLifespanManager
	{
		private readonly Subject<TileChunk> chunkPopulated = new Subject<TileChunk>();

		private readonly ITilescapeStore store;

		private readonly ITileChunkPopulator fallback;

		private readonly bool runFallbackOnThreadPool;

		public SimpleStoreLifespanManager(ITilescapeStore store, ITileChunkPopulator fallback, bool runFallbackOnThreadPool = false)
		{
			Require.That(store != null);

			this.store = store;
			this.fallback = fallback;
			this.runFallbackOnThreadPool = runFallbackOnThreadPool;
			this.ChunkPopulated = this.chunkPopulated.AsObservable();
		}

		/// <inheritdoc />
		public UniRx.IObservable<TileChunk> ChunkPopulated { get; }

		/// <inheritdoc />
		public void Populate(TileChunk chunk)
		{
			ITileChunkLifespanManagerContracts.Populate(this, chunk);

			var entity = this.store.Chunks.Get(chunk.Key);
			chunk.Entity = entity;

			if (entity.Data != null)
			{
				this.store.Chunks.Serializer.DeserializeInline(entity.Data, chunk);
				entity.Data = null;
				this.chunkPopulated.OnNext(chunk);
			}
			else
			{
				if (this.runFallbackOnThreadPool)
				{
					UnityThreadPool.QueueUserWorkItem(
						threadState =>
						{
							var threadTileChunk = (TileChunk)threadState;
							this.fallback.Populate(threadTileChunk);
							this.chunkPopulated.OnNext(threadTileChunk);
						}, chunk);
				}
				else
				{
					this.fallback.Populate(chunk);
					this.chunkPopulated.OnNext(chunk);
				}
			}
		}

		/// <inheritdoc />
		public void Persist(IListView<TileChunk> chunks)
		{
			ITileChunkLifespanManagerContracts.Persist(this, chunks);

			for (int index = 0; index < chunks.Count; index++)
			{
				var chunk = chunks[index];
				////chunk.Entity.ChunkKey = chunk.Key;
				chunk.Entity.Data = this.store.Chunks.Serializer.Serialize(chunk);
			}

			this.store.Chunks.AddOrUpdate(chunks.Select(x => x.Entity));
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.store.Dispose();
	}
}
