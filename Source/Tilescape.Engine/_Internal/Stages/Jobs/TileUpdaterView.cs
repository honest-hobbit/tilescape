﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	internal class TileUpdaterView : ITileUpdaterView
	{
		private readonly TilesView tilesView = new TilesView();

		public TileUpdaterView(ITilescapeAtlas atlas)
		{
			Require.That(atlas != null);

			this.Atlas = atlas;
		}

		/// <inheritdoc />
		public ITilescapeAtlas Atlas { get; }

		/// <inheritdoc />
		public IAdjacencyView<Tile> Tiles => this.tilesView;

		/// <inheritdoc />
		public Guid StageKey => this.tilesView.StageKey;

		/// <inheritdoc />
		public Index3D TileIndex => this.tilesView.TileIndex;

		public void SetView(ITileVolume readView, Index3D tileIndex) => this.tilesView.SetView(readView, tileIndex);

		public void Clear() => this.tilesView.Clear();

		private class TilesView : IAdjacencyView<Tile>
		{
			private readonly AdjacencyCube<Tile> tiles = new AdjacencyCube<Tile>(Tile.Unassigned);

			private ITileVolume readView;

			public Guid StageKey => this.readView?.Key ?? Guid.Empty;

			public Index3D TileIndex { get; private set; }

			/// <inheritdoc />
			public int Length => this.tiles.Length;

			/// <inheritdoc />
			public Tile this[int index]
			{
				get
				{
					Require.That(this.readView != null);

					var result = this.tiles[index];
					if (result == Tile.Unassigned)
					{
						result = this.readView[this.TileIndex + Adjacency.ToIndex(index)];
						this.tiles[index] = result;
					}

					return result;
				}
			}

			/// <inheritdoc />
			public Tile this[Index3D index] => this[index.X, index.Y, index.Z];

			/// <inheritdoc />
			public Tile this[int x, int y, int z]
			{
				get
				{
					Require.That(this.readView != null);

					var result = this.tiles[x, y, z];
					if (result == Tile.Unassigned)
					{
						result = this.readView[
							new Index3D(this.TileIndex.X + x, this.TileIndex.Y + y, this.TileIndex.Z + z)];
						this.tiles[x, y, z] = result;
					}

					return result;
				}
			}

			public void SetView(ITileVolume readView, Index3D tileIndex)
			{
				Require.That(readView != null);

				this.readView = readView;
				this.TileIndex = tileIndex;
				this.tiles.SetAllTo(Tile.Unassigned);
			}

			public void Clear()
			{
				this.readView = null;
				this.tiles.SetAllTo(Tile.Unassigned);
			}
		}
	}
}
