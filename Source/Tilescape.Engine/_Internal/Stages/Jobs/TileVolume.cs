﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	internal class TileVolume : ITileVolume
	{
		private readonly TileJobViewManager parent;

		private TileChunk currentChunk;

		public TileVolume(TileJobViewManager parent, Guid stageKey)
		{
			Require.That(parent != null);
			Require.That(stageKey != Guid.Empty);

			this.parent = parent;
			this.Key = stageKey;
		}

		/// <inheritdoc />
		public Guid Key { get; }

		/// <inheritdoc />
		public ITilescapeAtlas Atlas => this.parent.Atlas;

		/// <inheritdoc />
		public Tile this[Index3D stageTileIndex]
		{
			get
			{
				this.HandleSettingCurrentChunk(stageTileIndex);
				return this.currentChunk.GetTile(this.Atlas.Config.ConvertStageTileToLocal(stageTileIndex));
			}

			set
			{
				this.HandleSettingCurrentChunk(stageTileIndex);
				Tile previousTile;
				Index3D chunkTileIndex;

				// get the previous tile value and the index of the tile local to its chunk
				if (this.currentChunk.IsUniform)
				{
					previousTile = this.currentChunk.UniformTile.Value;
					if (previousTile == value)
					{
						return;
					}

					this.parent.AssignTilesArrayToChunk(this.currentChunk);
					chunkTileIndex = this.Atlas.Config.ConvertStageTileToLocal(stageTileIndex);
				}
				else
				{
					chunkTileIndex = this.Atlas.Config.ConvertStageTileToLocal(stageTileIndex);
					previousTile = this.currentChunk.Tiles[chunkTileIndex];
					if (previousTile == value)
					{
						return;
					}
				}

				// modify the tile
				this.currentChunk.Tiles[chunkTileIndex] = value;
				if (!this.currentChunk.ModifiedTiles.ContainsKey(chunkTileIndex))
				{
					this.currentChunk.ModifiedTiles.Add(chunkTileIndex, previousTile);
				}

				this.parent.EnqueueChunkModified(this.currentChunk, stageTileIndex, this);
			}
		}

		private void HandleSettingCurrentChunk(Index3D tileIndex)
		{
			var chunkIndex = this.Atlas.Config.ConvertTileToChunk(tileIndex);
			if (chunkIndex != this.currentChunk?.Key.Index)
			{
				this.currentChunk = this.parent.GetChunk(new TileChunkKey(this.Key, chunkIndex));
			}

			this.currentChunk.WaitUntilPopulated();
		}
	}
}
