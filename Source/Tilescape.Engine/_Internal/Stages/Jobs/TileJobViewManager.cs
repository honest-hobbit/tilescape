﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Curves;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using UniRx;

namespace Tilescape.Engine
{
	internal class TileJobViewManager
	{
		private readonly List<TileChunkCache.CachedChunk> chunkPins = new List<TileChunkCache.CachedChunk>();

		private readonly HashSet<Index3D> tileIndicesToUpdate = new HashSet<Index3D>(MortonCurve.Comparer<Index3D>());

		private readonly Queue<IndexValuePair<Index3D, TileVolume>> tilesToUpdate = new Queue<IndexValuePair<Index3D, TileVolume>>();

		private readonly Queue<UpdatedTile> tileUpdatesToApply = new Queue<UpdatedTile>();

		private readonly Subject<int> updateLooped = new Subject<int>();

		private readonly TileChunkCache chunkCache;

		private readonly TileChunkModifiedQueue chunkModifiedQueue;

		private readonly TileUpdaterView updaterView;

		private readonly TileJobView jobView;

		public TileJobViewManager(ITilescapeAtlas atlas, TileChunkCache chunkCache, TileChunkModifiedQueue chunkModifiedQueue)
		{
			Require.That(atlas != null);
			Require.That(chunkCache != null);
			Require.That(chunkModifiedQueue != null);

			this.Atlas = atlas;
			this.chunkCache = chunkCache;
			this.chunkModifiedQueue = chunkModifiedQueue;

			this.updaterView = new TileUpdaterView(atlas);
			this.jobView = new TileJobView(this);

			this.UpdateLooped = this.updateLooped.AsObservable();
		}

		public ITilescapeAtlas Atlas { get; }

		public ITileJobView View => this.jobView;

		public UniRx.IObservable<int> UpdateLooped { get; }

		public void Clear()
		{
			this.chunkPins.ForEach(pin => pin.Unpin(this.chunkCache));
			this.chunkPins.Clear();
			this.jobView.Clear();
		}

		public void UpdateTiles()
		{
			int loops = 0;
			while (this.tileIndicesToUpdate.Count > 0)
			{
				// process all pending tile updates
				while (this.tilesToUpdate.TryDequeue(out var pair))
				{
					var updater = this.Atlas.Types[pair.Value[pair.Index].Type].Updater;
					if (updater != null)
					{
						this.updaterView.SetView(pair.Value, pair.Index);
						if (updater.TryUpdateTile(this.updaterView, out var updatedTile))
						{
							// updated tile values are stored to be applied after running all tile updaters
							// this way all tile updaters see the same consistent version of the tiles
							// regardless of which order they run in
							this.tileUpdatesToApply.Enqueue(new UpdatedTile(pair.Index, updatedTile, pair.Value));
						}
					}
				}

				this.tileIndicesToUpdate.Clear();

				// apply updated tile values
				while (this.tileUpdatesToApply.TryDequeue(out var updated))
				{
					updated.Volume[updated.Index] = updated.Tile;
				}

				loops++;
				this.updateLooped.OnNext(loops);
			}

			this.updaterView.Clear();
		}

		public void AssignTilesArrayToChunk(TileChunk chunk) => this.chunkCache.AssignTilesArrayFromPool(chunk);

		public TileChunk GetChunk(TileChunkKey chunkKey)
		{
			var pin = this.chunkCache.PinAndGetChunk(chunkKey);
			this.chunkPins.Add(pin);
			return pin.TileChunk;
		}

		public void EnqueueChunkModified(TileChunk chunk, Index3D modifiedTileStageIndex, TileVolume volume)
		{
			Require.That(chunk != null);
			Require.That(volume != null);

			this.chunkModifiedQueue.EnqueueChunkModified(chunk);
			this.EnqueueAdjacentTilesForUpdating(modifiedTileStageIndex, volume);
		}

		private void EnqueueAdjacentTilesForUpdating(Index3D tileIndex, TileVolume volume)
		{
			Require.That(volume != null);

			this.EnqueueTileForUpdating(tileIndex, volume);
			for (int index = 0; index < Adjacency.Neighbors.Count; index++)
			{
				this.EnqueueTileForUpdating(tileIndex + Adjacency.Neighbors[index].AsIndex, volume);
			}
		}

		private void EnqueueTileForUpdating(Index3D tileIndex, TileVolume volume)
		{
			Require.That(volume != null);

			if (this.tileIndicesToUpdate.Add(tileIndex))
			{
				this.tilesToUpdate.Enqueue(IndexValuePair.New(tileIndex, volume));
			}
		}

		private struct UpdatedTile
		{
			public UpdatedTile(Index3D index, Tile tile, TileVolume volume)
			{
				Require.That(volume != null);

				this.Index = index;
				this.Tile = tile;
				this.Volume = volume;
			}

			public Index3D Index { get; }

			public Tile Tile { get; }

			public TileVolume Volume { get; }
		}

		private class TileJobView : ITileJobView
		{
			private readonly Dictionary<Guid, TileVolume> volumes = new Dictionary<Guid, TileVolume>(Equality.StructComparer<Guid>());

			private readonly TileJobViewManager parent;

			public TileJobView(TileJobViewManager parent)
			{
				Require.That(parent != null);

				this.parent = parent;
			}

			/// <inheritdoc />
			public ITilescapeAtlas Atlas => this.parent.Atlas;

			/// <inheritdoc />
			public ITileVolume this[Guid stageKey] => this.volumes.GetOrAdd(stageKey, key => new TileVolume(this.parent, key));

			public void Clear() => this.volumes.Clear();
		}
	}
}
