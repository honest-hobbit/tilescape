﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Interests;
using Tilescape.Utility.Unity.Reactive;
using UniRx;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class TileStageManager : AbstractDisposable, ITileStageManager
	{
		private readonly Dictionary<Guid, ITileStage> stages = new Dictionary<Guid, ITileStage>();

		private readonly AutoResetEvent signal = new AutoResetEvent(false);

		private readonly ConcurrentQueue<ITileJob> jobs = new ConcurrentQueue<ITileJob>();

		private readonly ConcurrentQueue<KeyValuePair<TileChunkKey, Status>> setChunkActivity =
			new ConcurrentQueue<KeyValuePair<TileChunkKey, Status>>();

		private readonly ConcurrentQueue<TileChunk> chunksPopulated = new ConcurrentQueue<TileChunk>();

		private readonly TileChunkCache chunkCache;

		private readonly TileJobViewManager jobViewManager;

		private readonly TileChunkModifiedQueue chunkModifiedQueue;

		private readonly ResyncUnityQueue resyncUnityQueue;

		public TileStageManager(
			ITilescapeAtlas atlas,
			CachingConfig cachingConfig,
			ResyncUnityConfig resyncConfig,
			ITileChunkLifespanManager lifespanManager)
		{
			Require.That(atlas != null);
			Require.That(cachingConfig != null);
			Require.That(resyncConfig != null);
			Require.That(lifespanManager != null);

			this.Atlas = atlas;
			this.Stages = this.stages.AsDictionaryView();

			lifespanManager.ChunkPopulated.Subscribe(chunk =>
			{
				// runs on background store thread
				chunk.SetPopulated();
				this.chunksPopulated.Enqueue(chunk);
				this.signal.Set();
			});

			this.chunkCache = new TileChunkCache(atlas.Config, cachingConfig, lifespanManager);
			this.resyncUnityQueue = new ResyncUnityQueue(resyncConfig, atlas, this.chunkCache);
			this.chunkModifiedQueue = new TileChunkModifiedQueue(this.chunkCache, lifespanManager, this.resyncUnityQueue);
			this.jobViewManager = new TileJobViewManager(this.Atlas, this.chunkCache, this.chunkModifiedQueue);
			this.jobViewManager.UpdateLooped.Subscribe(loops =>
			{
				// runs on main tile thread
				if (loops > 10)
				{
					Debug.LogWarning("Tile updates are looping many times. Loop count: " + loops);
				}
			});

			var thread = new Thread(state => this.Run());
			thread.Start();
		}

		public UniRx.IObservable<IResyncFrame> ResyncFrameReady => this.resyncUnityQueue.ResyncFrameReady;

		/// <inheritdoc />
		public ITilescapeAtlas Atlas { get; }

		/// <inheritdoc />
		public IDictionaryView<Guid, ITileStage> Stages { get; }

		/// <inheritdoc />
		public ITileStage CreateStage(string name)
		{
			ITileStageManagerContracts.CreateStage(this, name);

			var stage = new TileStage(this, Guid.NewGuid(), name);
			this.stages[stage.Key] = stage;
			return stage;
		}

		/// <inheritdoc />
		public bool TryOpenStage(Guid key, out ITileStage stage)
		{
			ITileStageManagerContracts.TryOpenStage(this, key);

			// TODO this isn't fully implemented yet
			return this.stages.TryGetValue(key, out stage);
		}

		/// <inheritdoc />
		public void Submit(ITileJob job)
		{
			ITileStageManagerContracts.Submit(this, job);

			this.jobs.Enqueue(job);
			this.signal.Set();
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.signal.Set();

		private void Run()
		{
			try
			{
				while (!this.IsDisposed)
				{
					this.signal.WaitOne();

					this.ProcessSetChunkActivity();
					this.ProcessChunksPopulated();
					this.ProcessJobsAndSaveChanges();

					if (!this.resyncUnityQueue.TryResyncEverythingToUnity())
					{
						// if there is more to resync to Unity then set the signal immediately so this will be repeated
						this.signal.Set();
					}

					// unpinning chunks must be done after all other systems have ran
					this.chunkCache.ProcessUnpinningChunks();
				}
			}
			finally
			{
				// TODO what do about diposing stages?
				////foreach (var stage in this.stages.Values.ToList())
				////{
				////	stage.Dispose();
				////}

				this.signal.Close();
				this.chunkCache.Dispose();
				this.resyncUnityQueue.Dispose();
			}
		}

		private void ProcessSetChunkActivity()
		{
			while (this.setChunkActivity.TryDequeue(out var pair))
			{
				if (pair.Value == Status.Active)
				{
					var chunk = this.chunkCache.ActivateAndGetChunk(pair.Key);
					this.resyncUnityQueue.EnqueueChunkResync(chunk, guaranteeSingleFrame: false);
				}
				else
				{
					this.chunkCache.DeactivateChunk(pair.Key);
				}
			}
		}

		private void ProcessChunksPopulated()
		{
			while (this.chunksPopulated.TryDequeue(out var chunk))
			{
				((IFlagsForTileStageManager)chunk).IsInitialized = true;
				if (chunk.IsUniform)
				{
					this.chunkCache.ReturnTilesArrayToPool(chunk);
				}

				this.resyncUnityQueue.EnqueueChunkAndAdjacentResync(chunk, guaranteeSingleFrame: false);
			}
		}

		private void ProcessJobsAndSaveChanges()
		{
			while (this.jobs.TryDequeue(out var job))
			{
				job.RunInTilescape(this.jobViewManager.View);
				this.resyncUnityQueue.EnqueueJobRanResync(job);
				this.jobViewManager.UpdateTiles();
			}

			this.chunkModifiedQueue.ProcessAndSaveModifiedChunks();
			this.jobViewManager.Clear();
		}

		private class TileStage : AbstractDisposable, ITileStage
		{
			private readonly SingleSignalObservable isDisposed = new SingleSignalObservable();

			private readonly IInterestMap<Index3D, int> map =
				new InterestMap<Index3D, int>(SingleCountingInterest.Merger, Equality.StructComparer<Index3D>());

			private readonly TileStageManager manager;

			public TileStage(TileStageManager manager, Guid key, string name)
			{
				Require.That(manager != null);
				Require.That(key != Guid.Empty);
				Require.That(!name.IsNullOrWhiteSpace());

				this.manager = manager;
				this.Key = key;
				this.Name = name;
				this.Map = this.map.AsView();
				this.map.InterestsChanged.Activity().Subscribe(pair =>
				{
					this.manager.setChunkActivity.Enqueue(KeyValuePair.New(new TileChunkKey(this.Key, pair.Key), pair.Value));
					this.manager.signal.Set();
				});
			}

			/// <inheritdoc />
			public ITileStageManager Manager => this.manager;

			/// <inheritdoc />
			public Guid Key { get; }

			/// <inheritdoc />
			public string Name { get; }

			/// <inheritdoc />
			public IInterestMapView<Index3D, int> Map { get; }

			/// <inheritdoc />
			public IDisposable SubscribeArea(IInterestArea<Index3D, int> area)
			{
				ITileStageContracts.SubscribeArea(this, area);

				return this.map.SubscribeArea(area, this.isDisposed);
			}

			/// <inheritdoc />
			protected override void ManagedDisposal()
			{
				this.isDisposed.Dispose();
				this.manager.stages.Remove(this.Key);
			}
		}
	}
}
