﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Caching;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Pooling;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class TileChunkCache : AbstractDisposable
	{
		private readonly Queue<ICachePin<TileChunkKey, TileChunk>> unpinAtEndOfFrame = new Queue<ICachePin<TileChunkKey, TileChunk>>();

		private readonly Pool<AdjacentTileChunks> adjacencyCubes = Pool.CreateNew<AdjacentTileChunks>(x => x.ClearAll());

		private readonly ExpiryPool<CubeArray<Tile>> arrayPool;

		private readonly TileChunkCacheValueFactory chunkFactory;

		private readonly ICache<TileChunkKey, TileChunk> chunkCache;

		public TileChunkCache(ITileStageConfig tilesConfig, CachingConfig cachingConfig, ITileChunkLifespanManager lifespanManager)
		{
			Require.That(tilesConfig != null);
			Require.That(cachingConfig != null);
			Require.That(lifespanManager != null);

			this.Config = tilesConfig;
			Func<CubeArray<Tile>> arrayFactory = () => new CubeArray<Tile>(tilesConfig.ChunkSizeExponent);
			this.arrayPool = Expiry.CreatePool(
				arrayFactory, cachingConfig.MaxPooledChunks, expiryCapacityMultiplier: cachingConfig.ExpiryCapacityMultiplier);
			this.arrayPool.GiveUntilFull(arrayFactory);

			var availableChunks = Expiry.CreateCounter(cachingConfig.MaxCachedChunks, cachingConfig.ExpiryCapacityMultiplier);

			this.chunkFactory = new TileChunkCacheValueFactory(tilesConfig, this.arrayPool, availableChunks, lifespanManager);
			this.chunkCache = new Cache<TileChunkKey, TileChunk>(this.chunkFactory.Create, TileChunkKey.MortonCurveComparer);
		}

		public ITileStageConfig Config { get; }

		// does not pin
		public bool TryGetChunk(TileChunkKey key, out TileChunk chunk)
		{
			Require.That(!this.IsDisposed);

			if (this.chunkCache.TryGet(key, out var pin))
			{
				chunk = pin.Value;
				return true;
			}
			else
			{
				chunk = null;
				return false;
			}
		}

		// does not pin
		public bool TryGetInitializedAdjacentChunks(TileChunk chunk, out AdjacentTileChunks adjacent)
		{
			Require.That(!this.IsDisposed);
			Require.That(chunk != null);

			var key = chunk.Key;
			adjacent = this.adjacencyCubes.Take();
			adjacent[Adjacency.Center] = chunk;

			for (int index = 0; index < Adjacency.Neighbors.Count; index++)
			{
				var offset = Adjacency.Neighbors[index].AsIndex;
				if (this.chunkCache.TryGet(key + offset, out var pin) && pin.Value.IsInitialized)
				{
					adjacent[offset] = pin.Value;
				}
				else
				{
					this.adjacencyCubes.Give(adjacent);
					adjacent = null;
					return false;
				}
			}

			return true;
		}

		// pins only the single chunk being returned
		public CachedChunk PinAndGetChunk(TileChunkKey key)
		{
			Require.That(!this.IsDisposed);

			var result = this.chunkCache.PinAndGet(key);
			((IFlagsForTileChunkCache)result.Value).IsToBeUnpinned = false;
			return new CachedChunk(result);
		}

		// this method pins the chunk and the chunks adjacent to it
		public TileChunk ActivateAndGetChunk(TileChunkKey key)
		{
			Require.That(!this.IsDisposed);

			var result = this.chunkCache.PinAndGet(key).Value;
			var chunk = (IFlagsForTileChunkCache)result;
			chunk.IsActiveInUnity = true;
			chunk.IsToBeUnpinned = false;

			for (int index = 0; index < Adjacency.Neighbors.Count; index++)
			{
				((IFlagsForTileChunkCache)this.chunkCache.PinAndGet(
					key + Adjacency.Neighbors[index].AsIndex).Value).IsToBeUnpinned = false;
			}

			return result;
		}

		// this method unpins the chunk and the chunks adjacent to it (or queues for unpinning)
		public void DeactivateChunk(TileChunkKey key)
		{
			Require.That(!this.IsDisposed);

			var pin = this.chunkCache[key];
			var chunk = (IFlagsForTileChunkCache)pin.Value;
			chunk.IsActiveInUnity = false;
			chunk.IsUnitySynchronized = false;
			this.HandleUnpinning(pin);

			for (int index = 0; index < Adjacency.Neighbors.Count; index++)
			{
				this.HandleUnpinning(key + Adjacency.Neighbors[index].AsIndex);
			}
		}

		public void ProcessUnpinningChunks()
		{
			Require.That(!this.IsDisposed);

			// using the queue's count at the start guarantees that each chunk in the queue will be checked once and only once
			// while allowing chunks that can't be unpinned to be re-enqueued without causing an infinite loop
			for (int count = this.unpinAtEndOfFrame.Count; count > 0; count--)
			{
				var pin = this.unpinAtEndOfFrame.Dequeue();
				var chunk = pin.Value;

				if (chunk.IsToBeUnpinned)
				{
					Assert.That(pin.PinCount == 1, "Chunk to be unpinned should have only 1 pin left before unpinning.");

					// Reasons why the chunks are unable to be unpinned -
					// Populating the chunk is done on a backgroun thread so it isn't safe to unpin it until it is initialized.
					// Chunks can sit in the resync queue for multiple frames so wait until that system releases the chunk.
					if (chunk.IsInitialized && !chunk.IsResyncingUnity)
					{
						pin.Unpin();
					}
					else
					{
						// unable to unpin chunk because it is still in use by another system
						// so re-enqueue it to try and unpin it at the end of a later frame
						this.unpinAtEndOfFrame.Enqueue(pin);
					}
				}
			}
		}

		public void AssignTilesArrayFromPool(TileChunk chunk)
		{
			Require.That(!this.IsDisposed);
			Require.That(chunk != null);
			Require.That(chunk.IsUniform);

			chunk.Tiles = this.arrayPool.Take();
			chunk.Tiles.Array.SetAllTo(chunk.UniformTile.Value);
			chunk.UniformTile = Try.None<Tile>();
		}

		public void ReturnTilesArrayToPool(TileChunk chunk)
		{
			Require.That(!this.IsDisposed);
			Require.That(chunk != null);
			Require.That(chunk.IsUniform);

			this.arrayPool.TryGive(chunk.Tiles);
			chunk.Tiles = null;
		}

		public void ReturnAdjacencyCubeToPool(AdjacentTileChunks adjacent)
		{
			Require.That(!this.IsDisposed);
			Require.That(adjacent != null);

			this.adjacencyCubes.Give(adjacent);
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.chunkFactory.Dispose();

		private void HandleUnpinning(TileChunkKey key) => this.HandleUnpinning(this.chunkCache[key]);

		private void HandleUnpinning(ICachePin<TileChunkKey, TileChunk> pin)
		{
			Require.That(!this.IsDisposed);
			Require.That(pin != null);
			Require.That(!pin.IsDisposed);
			Require.That(pin.IsPinned);

			if (pin.PinCount == 1)
			{
				Assert.That(!pin.Value.IsToBeUnpinned, "Unpin was called more times than Pin.");

				((IFlagsForTileChunkCache)pin.Value).IsToBeUnpinned = true;
				this.unpinAtEndOfFrame.Enqueue(pin);
			}
			else
			{
				pin.Unpin();
			}
		}

		public struct CachedChunk
		{
			private readonly ICachePin<TileChunkKey, TileChunk> pin;

			public CachedChunk(ICachePin<TileChunkKey, TileChunk> pin)
			{
				Require.That(pin != null);
				Require.That(!pin.IsDisposed);
				Require.That(pin.IsPinned);

				this.pin = pin;
			}

			public TileChunk TileChunk => this.pin.Value;

			// unpins only the single chunk
			public void Unpin(TileChunkCache cache)
			{
				Require.That(cache != null);
				Require.That(!cache.IsDisposed);

				cache.HandleUnpinning(this.pin);
			}
		}
	}
}
