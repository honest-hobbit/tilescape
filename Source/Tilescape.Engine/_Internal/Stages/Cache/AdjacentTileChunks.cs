﻿using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Engine
{
	internal class AdjacentTileChunks : AdjacencyCube<TileChunk>, IAdjacencyView<ITileChunkView>
	{
		ITileChunkView IAdjacencyView<ITileChunkView>.this[int index] => this[index];

		ITileChunkView IAdjacencyView<ITileChunkView>.this[Index3D index] => this[index];

		ITileChunkView IAdjacencyView<ITileChunkView>.this[int x, int y, int z] => this[x, y, z];
	}
}
