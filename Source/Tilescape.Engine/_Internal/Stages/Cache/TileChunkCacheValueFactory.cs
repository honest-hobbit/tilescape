﻿using Tilescape.Utility.Caching;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Pooling;

namespace Tilescape.Engine
{
	internal class TileChunkCacheValueFactory : AbstractDisposable
	{
		private readonly IPool<CacheValue> chunkPool;

		private readonly ExpiryPool<CubeArray<Tile>> arrayPool;

		private readonly ExpiryCounter availableChunks;

		private readonly ITileChunkLifespanManager lifespanManager;

		public TileChunkCacheValueFactory(
			ITileStageConfig config,
			ExpiryPool<CubeArray<Tile>> arrayPool,
			ExpiryCounter availableChunks,
			ITileChunkLifespanManager lifespanManager)
		{
			Require.That(config != null);
			Require.That(arrayPool != null);
			Require.That(availableChunks != null);
			Require.That(lifespanManager != null);

			this.arrayPool = arrayPool;
			this.availableChunks = availableChunks;
			this.lifespanManager = lifespanManager;

			this.chunkPool = new Pool<CacheValue>(
				() => new CacheValue(this, new TileChunk(config.ChunkSizeExponent)), value => value.Reset());
		}

		public int CachedChunksCount => this.availableChunks.MaxCount - this.availableChunks.Count;

		public ICacheValue<TileChunk> Create(TileChunkKey key)
		{
			var cacheValue = this.chunkPool.Take();
			cacheValue.Value.Key = key;
			this.lifespanManager.Populate(cacheValue.Value);
			this.availableChunks.Decrement();
			return cacheValue;
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.lifespanManager.Dispose();

		private class CacheValue : ICacheValue<TileChunk>
		{
			private readonly TileChunkCacheValueFactory parent;

			private readonly TileChunk chunk;

			public CacheValue(TileChunkCacheValueFactory parent, TileChunk chunk)
			{
				Require.That(parent != null);
				Require.That(chunk != null);

				this.parent = parent;
				this.chunk = chunk;
			}

			/// <inheritdoc />
			public bool IsDisposed { get; private set; } = true;

			/// <inheritdoc />
			public TileChunk Value
			{
				get
				{
					IDisposableValueContracts.Value(this);
					return this.chunk;
				}
			}

			/// <inheritdoc />
			public void AddExpiration(ExpiryToken expiration)
			{
				ICacheValueContracts.AddExpiration(this);

				this.parent.availableChunks.AddExpiration(expiration);

				if (!this.chunk.IsUniform)
				{
					this.parent.arrayPool.AddExpiration(expiration);
				}
			}

			/// <inheritdoc />
			public void Dispose()
			{
				if (this.IsDisposed)
				{
					return;
				}

				this.IsDisposed = true;
				this.parent.availableChunks.Increment();

				if (!this.chunk.IsUniform)
				{
					this.parent.arrayPool.TryGive(this.chunk.Tiles);
				}

				this.parent.chunkPool.TryGive(this);
			}

			public void Reset()
			{
				this.chunk.Reset(this.parent.arrayPool.Take());
				this.IsDisposed = false;
			}
		}
	}
}
