﻿namespace Tilescape.Engine
{
	internal static class VisibleTilePartExtensions
	{
		public static bool Has(this VisibleTilePart current, VisibleTilePart flag) => (current & flag) == flag;

		public static bool HasAny(this VisibleTilePart current, VisibleTilePart flags) => (current & flags) != 0;

		public static VisibleTilePart Add(this VisibleTilePart current, VisibleTilePart flags) => current | flags;

		public static VisibleTilePart Remove(this VisibleTilePart current, VisibleTilePart flags) => current & ~flags;

		public static VisibleTilePart Set(this VisibleTilePart current, VisibleTilePart flags, bool value) =>
			value ? current.Add(flags) : current.Remove(flags);
	}
}
