﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Unity.Reactive;
using UniRx;

namespace Tilescape.Engine
{
	internal class ResyncUnityQueue : AbstractDisposable
	{
		private readonly Queue<AdjacentTileChunks> resyncSingleFrame = new Queue<AdjacentTileChunks>();

		private readonly Queue<AdjacentTileChunks> resyncVariableFrames = new Queue<AdjacentTileChunks>();

		private readonly List<ITileJob> jobsRan = new List<ITileJob>();

		private readonly Subject<IResyncFrame> frameReady = new Subject<IResyncFrame>();

		private readonly int chunksPerFrame;

		private readonly ResyncUnity frames;

		private readonly TileChunkCache chunkCache;

		private readonly ResyncFrameBuilder frameBuilder;

		public ResyncUnityQueue(ResyncUnityConfig config, ITilescapeAtlas atlas, TileChunkCache chunkCache)
		{
			Require.That(config != null);
			Require.That(atlas != null);
			Require.That(chunkCache != null);

			this.chunksPerFrame = config.ChunksPerFrame;
			this.frames = new ResyncUnity(config);
			this.frameBuilder = new ResyncFrameBuilder(atlas);
			this.chunkCache = chunkCache;
			this.ResyncFrameReady = this.frameReady.AsObservable();
		}

		public UniRx.IObservable<IResyncFrame> ResyncFrameReady { get; }

		public void EnqueueChunkAndAdjacentResync(TileChunk chunk, bool guaranteeSingleFrame)
		{
			Require.That(chunk != null);

			if (chunk.IsToBeUnpinned)
			{
				return;
			}

			this.EnqueueChunkResync(chunk, guaranteeSingleFrame);

			for (int index = 0; index < Adjacency.Neighbors.Count; index++)
			{
				// chech adjacent chunks for resyncing
				if (this.chunkCache.TryGetChunk(chunk.Key + Adjacency.Neighbors[index].AsIndex, out var adjacentChunk))
				{
					this.EnqueueChunkResync(adjacentChunk, guaranteeSingleFrame);
				}
			}
		}

		public void EnqueueChunkResync(TileChunk chunk, bool guaranteeSingleFrame)
		{
			Require.That(chunk != null);

			if (!chunk.IsActiveInUnity || !chunk.IsInitialized || chunk.IsResyncingUnity || chunk.IsUnitySynchronized)
			{
				chunk.ModifiedTiles.Clear();
				return;
			}

			if (this.chunkCache.TryGetInitializedAdjacentChunks(chunk, out var adjacent))
			{
				((IFlagsForResyncUnityQueue)adjacent[Index3D.Zero]).IsResyncingUnity = true;
				if (guaranteeSingleFrame)
				{
					this.resyncSingleFrame.Enqueue(adjacent);
				}
				else
				{
					this.resyncVariableFrames.Enqueue(adjacent);
				}
			}
		}

		public void EnqueueJobRanResync(ITileJob job)
		{
			Require.That(job != null);

			this.jobsRan.Add(job);
		}

		public bool TryResyncEverythingToUnity()
		{
			var frame = this.frames.GetNextFrame();

			// add all jobs to the first frame only
			this.jobsRan.ForEach(frame.AddJobRan);
			this.jobsRan.Clear();

			// process guaranteed single frame resyncing
			int chunksResynced = 0;
			while (this.resyncSingleFrame.TryDequeue(out var adjacent))
			{
				if (this.TryResyncChunk(adjacent, frame))
				{
					chunksResynced++;
				}
			}

			if (TryNextFrameReady())
			{
				return this.resyncVariableFrames.Count == 0;
			}

			// process variable frame resyncing
			while (this.resyncVariableFrames.TryDequeue(out var adjacent))
			{
				if (this.TryResyncChunk(adjacent, frame))
				{
					chunksResynced++;
					if (TryNextFrameReady())
					{
						return this.resyncVariableFrames.Count == 0;
					}
				}
			}

			if (!frame.IsEmpty)
			{
				this.frameReady.OnNext(frame);
			}

			return true;

			bool TryNextFrameReady()
			{
				if (chunksResynced >= this.chunksPerFrame && !frame.IsEmpty)
				{
					this.frameReady.OnNext(frame);
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.frames.Dispose();

		private bool TryResyncChunk(AdjacentTileChunks adjacent, ResyncUnity.Frame frame)
		{
			Require.That(adjacent != null);
			Require.That(frame != null);

			var chunk = adjacent[Adjacency.Center];
			((IFlagsForResyncUnityQueue)chunk).IsResyncingUnity = false;

			if (!chunk.IsActiveInUnity)
			{
				chunk.ModifiedTiles.Clear();
				this.chunkCache.ReturnAdjacencyCubeToPool(adjacent);
				return false;
			}

			var resyncChunk = frame.GetChunkBuilder(chunk.Key);
			this.CopyModifiedTiles(chunk, resyncChunk);
			this.frameBuilder.RebuildChunk(adjacent, resyncChunk);

			// Unity won't have put it into the scene yet, but the resync data will have been sent to Unity
			((IFlagsForResyncUnityQueue)chunk).IsUnitySynchronized = true;
			this.chunkCache.ReturnAdjacencyCubeToPool(adjacent);
			return true;
		}

		private void CopyModifiedTiles(TileChunk chunk, ResyncChunk resyncChunk)
		{
			Require.That(chunk != null);
			Require.That(resyncChunk != null);

			resyncChunk.ModifiedTilesBuilder.EnsureCapacity(chunk.ModifiedTiles.Count);
			if (chunk.IsUniform)
			{
				var uniformTile = chunk.UniformTile.Value;
				foreach (var pair in chunk.ModifiedTiles)
				{
					resyncChunk.ModifiedTilesBuilder.Add(
						KeyValuePair.New(pair.Key, SequentialPair.New(pair.Value, uniformTile)));
				}
			}
			else
			{
				foreach (var pair in chunk.ModifiedTiles)
				{
					resyncChunk.ModifiedTilesBuilder.Add(
						KeyValuePair.New(pair.Key, SequentialPair.New(pair.Value, chunk.Tiles[pair.Key])));
				}
			}

			chunk.ModifiedTiles.Clear();
		}
	}
}
