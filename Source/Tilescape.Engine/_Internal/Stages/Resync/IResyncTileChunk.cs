﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Meshing;
using Tilescape.Utility.Unity.Reactive;

namespace Tilescape.Engine
{
	internal interface IResyncTileChunk : IKeyed<Index3D>
	{
		IListView<KeyValuePair<Index3D, SequentialPair<Tile>>> ModifiedTiles { get; }

		IListView<IMeshDefinition> Meshes { get; }

		IListView<BoxColliderStruct> Colliders { get; }

		bool IsEmpty { get; }
	}
}
