﻿using Tilescape.Utility.Collections;

namespace Tilescape.Engine
{
	internal interface IResyncFrame
	{
		IListView<IResyncTileStage> Stages { get; }

		IListView<ITileJob> JobsRan { get; }

		void CompleteResyncing();
	}
}
