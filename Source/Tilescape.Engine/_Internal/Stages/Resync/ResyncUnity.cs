﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Pooling;

namespace Tilescape.Engine
{
	internal class ResyncUnity : AbstractDisposable
	{
		// shared by both the tile main thread and the Unity main thread
		private readonly BlockingCollection<Frame> pendingFrames = new BlockingCollection<Frame>();

		// owned by the tile main thread only
		private readonly Stack<Frame> availableFrames = new Stack<Frame>();

		// owned by the tile main thread only
		private readonly IPool<ResyncChunk> resyncChunks;

		// owned by the tile main thread only
		private readonly IPool<Frame.Stage> stageFrames;

		public ResyncUnity(ResyncUnityConfig config)
		{
			Require.That(config != null);

			for (int count = 0; count < config.Frames; count++)
			{
				this.availableFrames.Push(new Frame(this));
			}

			var meshBuilders = new Pool<QuadMeshBuilder>(QuadMeshBuilder.CreateWithMaxVertexCapacity);
			meshBuilders.GiveMany(Factory.CreateMany(config.InitialMeshBuilders, () => new QuadMeshBuilder()));

			this.resyncChunks = new Pool<ResyncChunk>(() => new ResyncChunk(meshBuilders), x => x.Clear());
			this.stageFrames = new Pool<Frame.Stage>(() => new Frame.Stage(this), x => x.Clear());
		}

		// only called by the tile main thread while rebuilding chunks
		public Frame GetNextFrame()
		{
			// free up as many frame resources as possible
			this.ClearPendingFrames();

			// return the next frame if able
			if (this.availableFrames.TryPop(out var nextFrame))
			{
				return nextFrame;
			}

			// otherwise wait for at least 1 frame to become available and clear it (Take is a blocking call)
			nextFrame = this.pendingFrames.Take();
			nextFrame.Clear();

			// double check if any other frames became available while waiting in order to free up more resources
			this.ClearPendingFrames();

			return nextFrame;
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.pendingFrames.Dispose();

		private void ClearPendingFrames()
		{
			while (this.pendingFrames.TryTake(out var frame))
			{
				frame.Clear();
				this.availableFrames.Push(frame);
			}
		}

		public class Frame : IResyncFrame
		{
			private readonly List<ITileJob> jobsRan = new List<ITileJob>();

			private readonly Dictionary<Guid, Stage> stagesDictionary = new Dictionary<Guid, Stage>(Equality.StructComparer<Guid>());

			private readonly List<IResyncTileStage> stages = new List<IResyncTileStage>();

			private readonly ResyncUnity resyncUnity;

			public Frame(ResyncUnity resyncUnity)
			{
				Require.That(resyncUnity != null);

				this.resyncUnity = resyncUnity;
				this.Stages = this.stages.AsListView();
				this.JobsRan = this.jobsRan.AsListView();
			}

			/// <inheritdoc />
			public IListView<IResyncTileStage> Stages { get; }

			/// <inheritdoc />
			public IListView<ITileJob> JobsRan { get; }

			public bool IsEmpty => this.Stages.Count == 0 && this.JobsRan.Count == 0;

			/// <inheritdoc />
			public void CompleteResyncing()
			{
				// only called by Unity's main thread
				this.resyncUnity.pendingFrames.Add(this);
			}

			// only called by the tile main thread while rebuilding chunks
			public void AddJobRan(ITileJob job)
			{
				Require.That(job != null);

				this.jobsRan.Add(job);
			}

			// only called by the tile main thread while rebuilding chunks
			public ResyncChunk GetChunkBuilder(TileChunkKey key)
			{
				if (!this.stagesDictionary.TryGetValue(key.StageKey, out var stageFrame))
				{
					// no frame exists for the chunk's stage so get a frame for the stage
					stageFrame = this.resyncUnity.stageFrames.Take();
					stageFrame.ResyncFrame = this;
					stageFrame.Key = key.StageKey;
					this.stagesDictionary[key.StageKey] = stageFrame;
					this.stages.Add(stageFrame);
				}

				return stageFrame.GetChunkBuilder(key);
			}

			// only called by the tile main thread just before rebuilding chunks
			public void Clear()
			{
				this.stages.ForEach(x => this.resyncUnity.stageFrames.Give((Stage)x));
				this.stages.Clear();
				this.stagesDictionary.Clear();
				this.jobsRan.Clear();
			}

			public class Stage : IResyncTileStage
			{
				private readonly List<ResyncChunk> chunks = new List<ResyncChunk>();

				private readonly ResyncUnity resyncUnity;

				public Stage(ResyncUnity resyncUnity)
				{
					Require.That(resyncUnity != null);

					this.resyncUnity = resyncUnity;
					this.Chunks = ListView.Convert(this.chunks, x => (IResyncTileChunk)x);
				}

				public Frame ResyncFrame { get; set; }

				/// <inheritdoc />
				public Guid Key { get; set; }

				/// <inheritdoc />
				public IListView<IResyncTileChunk> Chunks { get; }

				// only called by the tile main thread just before rebuilding chunks
				public void Clear()
				{
					this.chunks.ForEach(x => this.resyncUnity.resyncChunks.Give(x));
					this.chunks.Clear();
				}

				// only called by the tile main thread while rebuilding chunks
				public ResyncChunk GetChunkBuilder(TileChunkKey key)
				{
					Require.That(key.StageKey == this.Key);
					Require.That(
						!this.chunks.Select(x => x.Key).Contains(key.Index),
						"A chunk should only be resynced once per frame.");

					var builder = this.resyncUnity.resyncChunks.Take();
					builder.Key = key.Index;
					this.chunks.Add(builder);
					return builder;
				}
			}
		}
	}
}
