﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Pooling;
using Tilescape.Utility.Unity.Meshing;
using Tilescape.Utility.Unity.Reactive;

namespace Tilescape.Engine
{
	internal class ResyncChunk : IResyncTileChunk
	{
		public ResyncChunk(IPool<QuadMeshBuilder> meshBuilders)
		{
			Require.That(meshBuilders != null);

			this.ModifiedTiles = this.ModifiedTilesBuilder.AsListView();
			this.MeshBuilder = new QuadMultiMeshBuilder(meshBuilders);
			this.Colliders = this.ColliderBuilder.AsListView();
		}

		/// <inheritdoc />
		public Index3D Key { get; set; }

		// TODO should this check the modified tiles list?
		/// <inheritdoc />
		public bool IsEmpty => this.MeshBuilder.Count == 0 && this.ColliderBuilder.Count == 0;

		/// <inheritdoc />
		public IListView<KeyValuePair<Index3D, SequentialPair<Tile>>> ModifiedTiles { get; }

		/// <inheritdoc />
		public IListView<IMeshDefinition> Meshes => this.MeshBuilder;

		/// <inheritdoc />
		public IListView<BoxColliderStruct> Colliders { get; }

		public List<KeyValuePair<Index3D, SequentialPair<Tile>>> ModifiedTilesBuilder { get; } =
			new List<KeyValuePair<Index3D, SequentialPair<Tile>>>();

		public QuadMultiMeshBuilder MeshBuilder { get; }

		public List<BoxColliderStruct> ColliderBuilder { get; } = new List<BoxColliderStruct>();

		public void Clear()
		{
			this.ModifiedTilesBuilder.Clear();
			this.MeshBuilder.Clear();
			this.ColliderBuilder.Clear();
		}
	}
}
