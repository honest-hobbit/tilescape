﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class ResyncColliderBuilder
	{
		private readonly TileNeighbors shapes;

		private readonly ColliderBuilder builder;

		private readonly BoxColliderStruct fullChunkCollider;

		private List<BoxColliderStruct> colliders;

		public ResyncColliderBuilder(TileNeighbors shapes)
		{
			Require.That(shapes != null);

			this.shapes = shapes;
			this.builder = new ColliderBuilder(shapes.Config.ChunkSizeExponent);

			this.fullChunkCollider = new BoxColliderStruct(
				center: shapes.Config.GetChunkSizeVector() / 2f,
				size: shapes.Config.GetChunkSizeVector());
		}

		public void RebuildColliders(List<BoxColliderStruct> colliders)
		{
			Require.That(this.shapes.IsPopulated);
			Require.That(colliders != null);

			this.colliders = colliders;

			if (this.shapes.Center.Uniform.HasValue)
			{
				this.BuildCollidersUniformTiles();
			}
			else
			{
				// build tile full cube colliders into larger combined box colliders
				this.builder.BuildColliders(this.IsSolid, colliders);

				// build unique tile colliders as separate small box colliders
				this.BuildCollidersNonuniformTiles();
			}

			this.colliders = null;
		}

		private static bool IsSolid(IListView<ITileProjection> tiles)
		{
			Require.That(tiles != null);

			for (int index = 0; index < tiles.Count; index++)
			{
				if (tiles[index].Shape.Colliders.IsFullCube)
				{
					return true;
				}
			}

			return false;
		}

		private static bool IsEmpty(IListView<ITileProjection> tiles)
		{
			Require.That(tiles != null);

			for (int index = 0; index < tiles.Count; index++)
			{
				if (tiles[index].Shape.Colliders.Count > 0)
				{
					return false;
				}
			}

			return true;
		}

		private bool IsSolid(int x, int y, int z) => IsSolid(this.shapes.Center.Values[x, y, z].Tiles);

		private void BuildCollidersUniformTiles()
		{
			Require.That(this.shapes.IsPopulated);
			Require.That(this.shapes.Center.Uniform.HasValue);

			var uniformTiles = this.shapes.Center.Uniform.Value.Tiles;
			if (IsSolid(uniformTiles))
			{
				this.colliders.Add(this.fullChunkCollider);
				return;
			}

			if (IsEmpty(uniformTiles))
			{
				return;
			}

			var sideLength = this.shapes.Config.ChunkLengthInTiles;

			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iY = 0; iY < sideLength; iY++)
				{
					for (int iZ = 0; iZ < sideLength; iZ++)
					{
						this.BuildTileColliders(uniformTiles, iX, iY, iZ);
					}
				}
			}
		}

		private void BuildCollidersNonuniformTiles()
		{
			Require.That(this.shapes.IsPopulated);
			Require.That(!this.shapes.Center.Uniform.HasValue);

			var sideLength = this.shapes.Config.ChunkLengthInTiles;
			var centerShapes = this.shapes.Center.Values;

			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iY = 0; iY < sideLength; iY++)
				{
					for (int iZ = 0; iZ < sideLength; iZ++)
					{
						this.BuildTileColliders(centerShapes[iX, iY, iZ].Tiles, iX, iY, iZ);
					}
				}
			}
		}

		private void BuildTileColliders(IListView<ITileProjection> tiles, int x, int y, int z)
		{
			Require.That(tiles != null);

			for (int tileIndex = 0; tileIndex < tiles.Count; tileIndex++)
			{
				var tileColliders = tiles[tileIndex].Shape.Colliders;
				if (tileColliders.IsFullCube)
				{
					return;
				}

				for (int colliderIndex = 0; colliderIndex < tileColliders.Count; colliderIndex++)
				{
					var box = tileColliders[colliderIndex];
					this.colliders.Add(new BoxColliderStruct(box.Center + new Vector3(x + .5f, y + .5f, z + .5f), box.Size));
				}
			}
		}
	}
}
