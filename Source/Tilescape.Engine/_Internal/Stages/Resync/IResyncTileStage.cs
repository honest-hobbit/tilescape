﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal interface IResyncTileStage : IKeyed<Guid>
	{
		IListView<IResyncTileChunk> Chunks { get; }
	}
}
