﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Engine
{
	internal class ResyncMeshBuilder
	{
		private readonly TileMeshBuilder builder;

		private readonly AdjacencyCrossProcessor<ITileProjectionsView> processor;

		public ResyncMeshBuilder(TileNeighbors shapes)
		{
			Require.That(shapes != null);

			this.builder = new TileMeshBuilder();
			this.processor = new AdjacencyCrossProcessor<ITileProjectionsView>(shapes, this.builder);
		}

		public void RebuildMeshes(QuadMultiMeshBuilder meshBuilder)
		{
			Require.That(this.processor.IsPopulated);
			Require.That(meshBuilder != null);

			this.builder.MeshBuilder = meshBuilder;
			this.processor.Process();
			this.builder.MeshBuilder = null;
		}

		private class TileMeshBuilder : IAdjacencyViewChunkProcessor<ITileProjectionsView>
		{
			private readonly UniformShapeBuilder uniformShape;

			private Vector3 meshOffset;

			public TileMeshBuilder()
			{
				this.uniformShape = new UniformShapeBuilder(this);
			}

			public QuadMultiMeshBuilder MeshBuilder { get; set; }

			/// <inheritdoc />
			public AdjacencyCube<ITileProjectionsView> View { get; } = new AdjacencyCube<ITileProjectionsView>();

			/// <inheritdoc />
			public bool CanSkipEntireChunkUniform() => this.CanSkipPartOfChunkUniform();

			/// <inheritdoc />
			public bool CanSkipPartOfChunkUniform()
			{
				this.uniformShape.DetermineVisibileParts();
				return !this.uniformShape.IsVisible;
			}

			/// <inheritdoc />
			public void ProcessViewEntirelyUniform(int x, int y, int z) => this.ProcessViewPartlyUniform(x, y, z);

			/// <inheritdoc />
			public void ProcessViewPartlyUniform(int x, int y, int z)
			{
				this.SetMeshPosition(x, y, z);
				this.uniformShape.AddMeshes();
			}

			/// <inheritdoc />
			public void ProcessViewNonuniform(int x, int y, int z)
			{
				this.SetMeshPosition(x, y, z);
				this.AddTileMeshes();
			}

			public void AddMesh(IListView<MeshQuad> mesh)
			{
				Require.That(this.MeshBuilder != null);
				Require.That(mesh != null);

				this.MeshBuilder.Add(mesh, this.meshOffset);
			}

			// tiles are 1f long and centered around 0 so .5f needs to be added to realign them with the chunks' edges
			private void SetMeshPosition(int x, int y, int z) => this.meshOffset = new Vector3(x + .5f, y + .5f, z + .5f);

			private void AddTileMeshes()
			{
				Require.That(this.View.IsCrossNotNull());

				var tiles = this.View[Adjacency.Center].Tiles;
				for (int index = 0; index < tiles.Count; index++)
				{
					var shape = tiles[index].Shape;
					this.AddMesh(shape.Meshes.Center);

					// X-axis
					if (shape.IsVisibleNegativeX(this.View[Adjacency.NegX].FaceMasks))
					{
						this.AddMesh(shape.Meshes.NegativeX);
					}

					if (shape.IsVisiblePositiveX(this.View[Adjacency.PosX].FaceMasks))
					{
						this.AddMesh(shape.Meshes.PositiveX);
					}

					// Y-axis
					if (shape.IsVisibleNegativeY(this.View[Adjacency.NegY].FaceMasks))
					{
						this.AddMesh(shape.Meshes.NegativeY);
					}

					if (shape.IsVisiblePositiveY(this.View[Adjacency.PosY].FaceMasks))
					{
						this.AddMesh(shape.Meshes.PositiveY);
					}

					// Z-axis
					if (shape.IsVisibleNegativeZ(this.View[Adjacency.NegZ].FaceMasks))
					{
						this.AddMesh(shape.Meshes.NegativeZ);
					}

					if (shape.IsVisiblePositiveZ(this.View[Adjacency.PosZ].FaceMasks))
					{
						this.AddMesh(shape.Meshes.PositiveZ);
					}
				}
			}
		}

		private class UniformShapeBuilder
		{
			private readonly TileMeshBuilder parent;

			private readonly List<VisibleTilePart> tilesVisibility = new List<VisibleTilePart>();

			public UniformShapeBuilder(TileMeshBuilder parent)
			{
				Require.That(parent != null);

				this.parent = parent;
			}

			public bool IsVisible { get; private set; } = false;

			public void DetermineVisibileParts()
			{
				Require.That(this.parent.View.IsCrossNotNull());

				this.tilesVisibility.Clear();
				this.IsVisible = false;

				var view = this.parent.View;
				var tiles = view[Adjacency.Center].Tiles;

				for (int index = 0; index < tiles.Count; index++)
				{
					var shape = tiles[index].Shape;
					var visibleParts = VisibleTilePart.None;

					if (shape.Meshes.Center.Count > 0)
					{
						visibleParts = visibleParts.Add(VisibleTilePart.Center);
					}

					if (shape.IsVisibleNegativeX(view[Adjacency.NegX].FaceMasks))
					{
						visibleParts = visibleParts.Add(VisibleTilePart.NegativeX);
					}

					if (shape.IsVisiblePositiveX(view[Adjacency.PosX].FaceMasks))
					{
						visibleParts = visibleParts.Add(VisibleTilePart.PositiveX);
					}

					if (shape.IsVisibleNegativeY(view[Adjacency.NegY].FaceMasks))
					{
						visibleParts = visibleParts.Add(VisibleTilePart.NegativeY);
					}

					if (shape.IsVisiblePositiveY(view[Adjacency.PosY].FaceMasks))
					{
						visibleParts = visibleParts.Add(VisibleTilePart.PositiveY);
					}

					if (shape.IsVisibleNegativeZ(view[Adjacency.NegZ].FaceMasks))
					{
						visibleParts = visibleParts.Add(VisibleTilePart.NegativeZ);
					}

					if (shape.IsVisiblePositiveZ(view[Adjacency.PosZ].FaceMasks))
					{
						visibleParts = visibleParts.Add(VisibleTilePart.PositiveZ);
					}

					this.tilesVisibility.Add(visibleParts);
					if (visibleParts != VisibleTilePart.None)
					{
						this.IsVisible = true;
					}
				}
			}

			public void AddMeshes()
			{
				var tiles = this.parent.View[Adjacency.Center].Tiles;
				for (int index = 0; index < tiles.Count; index++)
				{
					var visibleParts = this.tilesVisibility[index];
					if (visibleParts == VisibleTilePart.None)
					{
						// skip ahead to the next tile because the current tile isn't visible
						continue;
					}

					var shape = tiles[index].Shape;
					if (visibleParts.Has(VisibleTilePart.Center))
					{
						this.parent.AddMesh(shape.Meshes.Center);
					}

					// X-axis
					if (visibleParts.Has(VisibleTilePart.NegativeX))
					{
						this.parent.AddMesh(shape.Meshes.NegativeX);
					}

					if (visibleParts.Has(VisibleTilePart.PositiveX))
					{
						this.parent.AddMesh(shape.Meshes.PositiveX);
					}

					// Y-axis
					if (visibleParts.Has(VisibleTilePart.NegativeY))
					{
						this.parent.AddMesh(shape.Meshes.NegativeY);
					}

					if (visibleParts.Has(VisibleTilePart.PositiveY))
					{
						this.parent.AddMesh(shape.Meshes.PositiveY);
					}

					// Z-axis
					if (visibleParts.Has(VisibleTilePart.NegativeZ))
					{
						this.parent.AddMesh(shape.Meshes.NegativeZ);
					}

					if (visibleParts.Has(VisibleTilePart.PositiveZ))
					{
						this.parent.AddMesh(shape.Meshes.PositiveZ);
					}
				}
			}
		}
	}
}
