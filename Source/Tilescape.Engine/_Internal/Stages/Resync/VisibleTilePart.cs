﻿using System;

namespace Tilescape.Engine
{
	[Flags]
	internal enum VisibleTilePart : byte
	{
		None = 0,

		Center = 1,

		NegativeX = 1 << 1,

		PositiveX = 1 << 2,

		NegativeY = 1 << 3,

		PositiveY = 1 << 4,

		NegativeZ = 1 << 5,

		PositiveZ = 1 << 6,
	}
}
