﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal class ResyncFrameBuilder
	{
		private readonly TileNeighbors shapes;

		private readonly ResyncMeshBuilder meshBuilder;

		private readonly ResyncColliderBuilder colliderBuilder;

		public ResyncFrameBuilder(ITilescapeAtlas atlas)
		{
			Require.That(atlas != null);

			this.shapes = new TileNeighbors(atlas);
			this.meshBuilder = new ResyncMeshBuilder(this.shapes);
			this.colliderBuilder = new ResyncColliderBuilder(this.shapes);
		}

		public void RebuildChunk(IAdjacencyView<ITileChunkView> adjacent, ResyncChunk resyncChunk)
		{
			Require.That(adjacent != null);
			Require.That(adjacent.AllNotNull());
			Require.That(resyncChunk != null);

			this.shapes.Populate(adjacent);
			this.meshBuilder.RebuildMeshes(resyncChunk.MeshBuilder);
			this.colliderBuilder.RebuildColliders(resyncChunk.ColliderBuilder);
			this.shapes.Clear();
		}
	}
}
