﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal class AdjacencyCrossProcessor<T> : ITileNeighbors<T>
	{
		private readonly ITileCrossNeighbors<T> neighbors;

		private readonly IAdjacencyViewChunkProcessor<T> processor;

		private readonly AdjacencyCrossAssigner assigner;

		private readonly CrossCenterProcessor<T> center;

		private readonly CrossCornerProcessor<T> corners;

		private readonly CrossFaceNegXProcessor<T> faceNegX;

		private readonly CrossFacePosXProcessor<T> facePosX;

		private readonly CrossFaceNegYProcessor<T> faceNegY;

		private readonly CrossFacePosYProcessor<T> facePosY;

		private readonly CrossFaceNegZProcessor<T> faceNegZ;

		private readonly CrossFacePosZProcessor<T> facePosZ;

		private readonly CrossEdgeXNegYNegZProcessor<T> edgeXNegYNegZ;

		private readonly CrossEdgeXPosYNegZProcessor<T> edgeXPosYNegZ;

		private readonly CrossEdgeXNegYPosZProcessor<T> edgeXNegYPosZ;

		private readonly CrossEdgeXPosYPosZProcessor<T> edgeXPosYPosZ;

		private readonly CrossEdgeYNegXNegZProcessor<T> edgeYNegXNegZ;

		private readonly CrossEdgeYPosXNegZProcessor<T> edgeYPosXNegZ;

		private readonly CrossEdgeYNegXPosZProcessor<T> edgeYNegXPosZ;

		private readonly CrossEdgeYPosXPosZProcessor<T> edgeYPosXPosZ;

		private readonly CrossEdgeZNegXNegYProcessor<T> edgeZNegXNegY;

		private readonly CrossEdgeZPosXNegYProcessor<T> edgeZPosXNegY;

		private readonly CrossEdgeZNegXPosYProcessor<T> edgeZNegXPosY;

		private readonly CrossEdgeZPosXPosYProcessor<T> edgeZPosXPosY;

		public AdjacencyCrossProcessor(ITileCrossNeighbors<T> neighbors, IAdjacencyViewChunkProcessor<T> processor)
		{
			Require.That(neighbors != null);
			Require.That(processor != null);

			this.neighbors = neighbors;
			this.processor = processor;
			this.assigner = new AdjacencyCrossAssigner(neighbors.Config.ChunkSizeExponent);

			this.center = new CrossCenterProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.corners = new CrossCornerProcessor<T>(this.assigner, processor) { Neighbors = neighbors };

			this.faceNegX = new CrossFaceNegXProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.facePosX = new CrossFacePosXProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.faceNegY = new CrossFaceNegYProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.facePosY = new CrossFacePosYProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.faceNegZ = new CrossFaceNegZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.facePosZ = new CrossFacePosZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };

			this.edgeXNegYNegZ = new CrossEdgeXNegYNegZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeXPosYNegZ = new CrossEdgeXPosYNegZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeXNegYPosZ = new CrossEdgeXNegYPosZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeXPosYPosZ = new CrossEdgeXPosYPosZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };

			this.edgeYNegXNegZ = new CrossEdgeYNegXNegZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeYPosXNegZ = new CrossEdgeYPosXNegZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeYNegXPosZ = new CrossEdgeYNegXPosZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeYPosXPosZ = new CrossEdgeYPosXPosZProcessor<T>(this.assigner, processor) { Neighbors = neighbors };

			this.edgeZNegXNegY = new CrossEdgeZNegXNegYProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeZPosXNegY = new CrossEdgeZPosXNegYProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeZNegXPosY = new CrossEdgeZNegXPosYProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
			this.edgeZPosXPosY = new CrossEdgeZPosXPosYProcessor<T>(this.assigner, processor) { Neighbors = neighbors };
		}

		/// <inheritdoc />
		public ITileStageConfig Config => this.neighbors.Config;

		/// <inheritdoc />
		public bool IsPopulated => this.neighbors.IsPopulated;

		/// <inheritdoc />
		public void Clear() => this.neighbors.Clear();

		/// <inheritdoc />
		public void Populate(IAdjacencyView<ITileChunkView> chunks) => this.neighbors.Populate(chunks);

		public void Process()
		{
			Require.That(this.IsPopulated);

			if (this.neighbors.TryGetUniformValue(out var value))
			{
				this.processor.View.SetCrossTo(value);
				if (this.processor.CanSkipEntireChunkUniform())
				{
					return;
				}

				var sideMax = this.assigner.SideMax;
				for (int iX = 0; iX <= sideMax; iX++)
				{
					for (int iY = 0; iY <= sideMax; iY++)
					{
						for (int iZ = 0; iZ <= sideMax; iZ++)
						{
							this.processor.ProcessViewEntirelyUniform(iX, iY, iZ);
						}
					}
				}

				return;
			}

			this.center.Process();
			this.corners.Process();

			this.faceNegX.Process();
			this.facePosX.Process();
			this.faceNegY.Process();
			this.facePosY.Process();
			this.faceNegZ.Process();
			this.facePosZ.Process();

			this.edgeXNegYNegZ.Process();
			this.edgeXPosYNegZ.Process();
			this.edgeXNegYPosZ.Process();
			this.edgeXPosYPosZ.Process();

			this.edgeYNegXNegZ.Process();
			this.edgeYPosXNegZ.Process();
			this.edgeYNegXPosZ.Process();
			this.edgeYPosXPosZ.Process();

			this.edgeZNegXNegY.Process();
			this.edgeZPosXNegY.Process();
			this.edgeZNegXPosY.Process();
			this.edgeZPosXPosY.Process();
		}
	}
}
