﻿namespace Tilescape.Engine
{
	internal class CrossFacePosXProcessor<T> : AbstractFacePosXProcessor<T>
	{
		public CrossFacePosXProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.PosX] = this.Face.Uniform.Value;
		}

		protected override void SetNonuniformFaceTo(int iY, int iZ)
		{
			this.View[Adjacency.PosX] = this.Face.Values[iY, iZ];
		}

		protected override void StartNonuniformCenter(int iY, int iZ)
		{
			this.Assigner.SetFacePosX(this.Center.Values, iY, iZ, this.View);
			this.View[Adjacency.PosX] = this.Face.GetValue(iY, iZ);
		}

		protected override void ShiftNonuniformCenter(int iY, int iZ)
		{
			var center = this.Center.Values;

			this.View.ShiftCrossTowardsPositiveY();
			this.View[Adjacency.PosY] = center[this.X.Val, iY + 1, iZ];

			this.View[Adjacency.NegX] = center[this.X.Adj, iY, iZ];
			this.View[Adjacency.PosX] = this.Face.GetValue(iY, iZ);
			this.View[Adjacency.NegZ] = center[this.X.Val, iY, iZ - 1];
			this.View[Adjacency.PosZ] = center[this.X.Val, iY, iZ + 1];
		}
	}
}
