﻿namespace Tilescape.Engine
{
	internal abstract class AbstractFacePosYProcessor<T> : AbstractFaceProcessor<T>
	{
		public AbstractFacePosYProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override IChunkFaceView<T> Face => this.Chunks.FacePosY;

		protected Boundary Y => this.Assigner.PositiveSide;

		protected override void ProcessViewUniform(int iX, int iZ) => this.ProcessViewUniform(iX, this.Y.Val, iZ);

		protected override void ProcessViewNonuniform(int iX, int iZ) => this.ProcessViewNonuniform(iX, this.Y.Val, iZ);
	}
}
