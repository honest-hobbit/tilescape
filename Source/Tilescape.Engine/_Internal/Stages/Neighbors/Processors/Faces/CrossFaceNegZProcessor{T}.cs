﻿namespace Tilescape.Engine
{
	internal class CrossFaceNegZProcessor<T> : AbstractFaceNegZProcessor<T>
	{
		public CrossFaceNegZProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.NegZ] = this.Face.Uniform.Value;
		}

		protected override void SetNonuniformFaceTo(int iX, int iY)
		{
			this.View[Adjacency.NegZ] = this.Face.Values[iX, iY];
		}

		protected override void StartNonuniformCenter(int iX, int iY)
		{
			this.Assigner.SetFaceNegZ(this.Center.Values, iX, iY, this.View);
			this.View[Adjacency.NegZ] = this.Face.GetValue(iX, iY);
		}

		protected override void ShiftNonuniformCenter(int iX, int iY)
		{
			var center = this.Center.Values;

			this.View.ShiftCrossTowardsPositiveX();
			this.View[Adjacency.PosX] = center[iX + 1, iY, this.Z.Val];

			this.View[Adjacency.NegY] = center[iX, iY - 1, this.Z.Val];
			this.View[Adjacency.PosY] = center[iX, iY + 1, this.Z.Val];
			this.View[Adjacency.NegZ] = this.Face.GetValue(iX, iY);
			this.View[Adjacency.PosZ] = center[iX, iY, this.Z.Adj];
		}
	}
}
