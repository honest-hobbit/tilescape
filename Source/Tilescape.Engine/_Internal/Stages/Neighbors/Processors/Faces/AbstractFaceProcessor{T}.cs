﻿namespace Tilescape.Engine
{
	internal abstract class AbstractFaceProcessor<T> : AbstractAdjacencyProcessor<T>
	{
		public AbstractFaceProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public ITileCrossNeighbors<T> Neighbors { get; set; }

		protected override ITileCrossNeighbors<T> Chunks => this.Neighbors;

		protected abstract IChunkFaceView<T> Face { get; }

		public override void Process()
		{
			if (this.Center.Uniform.HasValue)
			{
				if (this.Face.Uniform.HasValue)
				{
					// both uniform
					this.SetUniform();
					if (this.CanSkipUniform())
					{
						return;
					}

					for (int iB = 1; iB < this.SideMax; iB++)
					{
						for (int iA = 1; iA < this.SideMax; iA++)
						{
							this.ProcessViewUniform(iA, iB);
						}
					}
				}
				else
				{
					// uniform center, nonuniform face
					this.Assigner.SetTo(this.Center.Uniform.Value, this.View);

					// TODO this could be handled more efficiently by using the shifting pattern
					for (int iB = 1; iB < this.SideMax; iB++)
					{
						for (int iA = 1; iA < this.SideMax; iA++)
						{
							this.SetNonuniformFaceTo(iA, iB);
							this.ProcessViewNonuniform(iA, iB);
						}
					}
				}
			}
			else
			{
				// nonuniform center
				for (int iB = 1; iB < this.SideMax; iB++)
				{
					int iA = 1;
					this.StartNonuniformCenter(iA, iB);
					this.ProcessViewNonuniform(iA, iB);

					for (iA = 2; iA < this.SideMax; iA++)
					{
						this.ShiftNonuniformCenter(iA, iB);
						this.ProcessViewNonuniform(iA, iB);
					}
				}
			}
		}

		protected abstract void ProcessViewUniform(int iA, int iB);

		protected abstract void ProcessViewNonuniform(int iA, int iB);

		protected abstract void SetUniform();

		protected abstract void SetNonuniformFaceTo(int iA, int iB);

		protected abstract void StartNonuniformCenter(int iA, int iB);

		protected abstract void ShiftNonuniformCenter(int iA, int iB);
	}
}
