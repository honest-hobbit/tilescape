﻿namespace Tilescape.Engine
{
	internal abstract class AbstractFacePosXProcessor<T> : AbstractFaceProcessor<T>
	{
		public AbstractFacePosXProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override IChunkFaceView<T> Face => this.Chunks.FacePosX;

		protected Boundary X => this.Assigner.PositiveSide;

		protected override void ProcessViewUniform(int iY, int iZ) => this.ProcessViewUniform(this.X.Val, iY, iZ);

		protected override void ProcessViewNonuniform(int iY, int iZ) => this.ProcessViewNonuniform(this.X.Val, iY, iZ);
	}
}
