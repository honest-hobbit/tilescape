﻿namespace Tilescape.Engine
{
	internal abstract class AbstractFaceNegXProcessor<T> : AbstractFaceProcessor<T>
	{
		public AbstractFaceNegXProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override IChunkFaceView<T> Face => this.Chunks.FaceNegX;

		protected Boundary X => this.Assigner.NegativeSide;

		protected override void ProcessViewUniform(int iY, int iZ) => this.ProcessViewUniform(this.X.Val, iY, iZ);

		protected override void ProcessViewNonuniform(int iY, int iZ) => this.ProcessViewNonuniform(this.X.Val, iY, iZ);
	}
}
