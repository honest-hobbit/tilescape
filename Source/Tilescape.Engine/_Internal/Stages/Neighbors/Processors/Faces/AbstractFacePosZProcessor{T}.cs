﻿namespace Tilescape.Engine
{
	internal abstract class AbstractFacePosZProcessor<T> : AbstractFaceProcessor<T>
	{
		public AbstractFacePosZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override IChunkFaceView<T> Face => this.Chunks.FacePosZ;

		protected Boundary Z => this.Assigner.PositiveSide;

		protected override void ProcessViewUniform(int iX, int iY) => this.ProcessViewUniform(iX, iY, this.Z.Val);

		protected override void ProcessViewNonuniform(int iX, int iY) => this.ProcessViewNonuniform(iX, iY, this.Z.Val);
	}
}
