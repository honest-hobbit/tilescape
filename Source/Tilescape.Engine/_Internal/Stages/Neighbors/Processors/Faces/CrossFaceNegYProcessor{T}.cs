﻿namespace Tilescape.Engine
{
	internal class CrossFaceNegYProcessor<T> : AbstractFaceNegYProcessor<T>
	{
		public CrossFaceNegYProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.NegY] = this.Face.Uniform.Value;
		}

		protected override void SetNonuniformFaceTo(int iX, int iZ)
		{
			this.View[Adjacency.NegY] = this.Face.Values[iX, iZ];
		}

		protected override void StartNonuniformCenter(int iX, int iZ)
		{
			this.Assigner.SetFaceNegY(this.Center.Values, iX, iZ, this.View);
			this.View[Adjacency.NegY] = this.Face.GetValue(iX, iZ);
		}

		protected override void ShiftNonuniformCenter(int iX, int iZ)
		{
			var center = this.Center.Values;

			this.View.ShiftCrossTowardsPositiveX();
			this.View[Adjacency.PosX] = center[iX + 1, this.Y.Val, iZ];

			this.View[Adjacency.NegY] = this.Face.GetValue(iX, iZ);
			this.View[Adjacency.PosY] = center[iX, this.Y.Adj, iZ];
			this.View[Adjacency.NegZ] = center[iX, this.Y.Val, iZ - 1];
			this.View[Adjacency.PosZ] = center[iX, this.Y.Val, iZ + 1];
		}
	}
}
