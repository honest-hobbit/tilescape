﻿namespace Tilescape.Engine
{
	internal abstract class AbstractFaceNegYProcessor<T> : AbstractFaceProcessor<T>
	{
		public AbstractFaceNegYProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override IChunkFaceView<T> Face => this.Chunks.FaceNegY;

		protected Boundary Y => this.Assigner.NegativeSide;

		protected override void ProcessViewUniform(int iX, int iZ) => this.ProcessViewUniform(iX, this.Y.Val, iZ);

		protected override void ProcessViewNonuniform(int iX, int iZ) => this.ProcessViewNonuniform(iX, this.Y.Val, iZ);
	}
}
