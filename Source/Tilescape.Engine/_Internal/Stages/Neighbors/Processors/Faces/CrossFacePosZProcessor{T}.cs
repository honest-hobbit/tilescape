﻿namespace Tilescape.Engine
{
	internal class CrossFacePosZProcessor<T> : AbstractFacePosZProcessor<T>
	{
		public CrossFacePosZProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.PosZ] = this.Face.Uniform.Value;
		}

		protected override void SetNonuniformFaceTo(int iX, int iY)
		{
			this.View[Adjacency.PosZ] = this.Face.Values[iX, iY];
		}

		protected override void StartNonuniformCenter(int iX, int iY)
		{
			this.Assigner.SetFacePosZ(this.Center.Values, iX, iY, this.View);
			this.View[Adjacency.PosZ] = this.Face.GetValue(iX, iY);
		}

		protected override void ShiftNonuniformCenter(int iX, int iY)
		{
			var center = this.Center.Values;

			this.View.ShiftCrossTowardsPositiveX();
			this.View[Adjacency.PosX] = center[iX + 1, iY, this.Z.Val];

			this.View[Adjacency.NegY] = center[iX, iY - 1, this.Z.Val];
			this.View[Adjacency.PosY] = center[iX, iY + 1, this.Z.Val];
			this.View[Adjacency.NegZ] = center[iX, iY, this.Z.Adj];
			this.View[Adjacency.PosZ] = this.Face.GetValue(iX, iY);
		}
	}
}
