﻿namespace Tilescape.Engine
{
	internal abstract class AbstractFaceNegZProcessor<T> : AbstractFaceProcessor<T>
	{
		public AbstractFaceNegZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override IChunkFaceView<T> Face => this.Chunks.FaceNegZ;

		protected Boundary Z => this.Assigner.NegativeSide;

		protected override void ProcessViewUniform(int iX, int iY) => this.ProcessViewUniform(iX, iY, this.Z.Val);

		protected override void ProcessViewNonuniform(int iX, int iY) => this.ProcessViewNonuniform(iX, iY, this.Z.Val);
	}
}
