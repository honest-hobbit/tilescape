﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeZPosXPosYProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeZPosXPosYProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected IChunkFaceView<T> FacePosX => this.Chunks.FacePosX;

		protected IChunkFaceView<T> FacePosY => this.Chunks.FacePosY;

		protected Boundary X => this.Assigner.PositiveSide;

		protected Boundary Y => this.Assigner.PositiveSide;

		protected override bool IsUniform() => this.FacePosX.Uniform.HasValue && this.FacePosY.Uniform.HasValue;

		protected override void ProcessViewUniform(int iZ) => this.ProcessViewUniform(this.X.Val, this.Y.Val, iZ);

		protected override void ProcessViewNonuniform(int iZ) => this.ProcessViewNonuniform(this.X.Val, this.Y.Val, iZ);
	}
}
