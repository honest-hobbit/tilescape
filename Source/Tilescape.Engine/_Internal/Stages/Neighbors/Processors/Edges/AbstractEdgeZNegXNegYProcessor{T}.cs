﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeZNegXNegYProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeZNegXNegYProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FaceNegX => this.Chunks.FaceNegX;

		public IChunkFaceView<T> FaceNegY => this.Chunks.FaceNegY;

		protected Boundary X => this.Assigner.NegativeSide;

		protected Boundary Y => this.Assigner.NegativeSide;

		protected override bool IsUniform() => this.FaceNegX.Uniform.HasValue && this.FaceNegY.Uniform.HasValue;

		protected override void ProcessViewUniform(int iZ) => this.ProcessViewUniform(this.X.Val, this.Y.Val, iZ);

		protected override void ProcessViewNonuniform(int iZ) => this.ProcessViewNonuniform(this.X.Val, this.Y.Val, iZ);
	}
}
