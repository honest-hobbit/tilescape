﻿namespace Tilescape.Engine
{
	internal class CrossEdgeYNegXNegZProcessor<T> : AbstractEdgeYNegXNegZProcessor<T>
	{
		public CrossEdgeYNegXNegZProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public ITileCrossNeighbors<T> Neighbors { get; set; }

		protected override ITileCrossNeighbors<T> Chunks => this.Neighbors;

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.NegX] = this.FaceNegX.Uniform.Value;
			this.View[Adjacency.NegZ] = this.FaceNegZ.Uniform.Value;
		}

		protected override void SetNonuniformEdgeTo(int iY)
		{
			this.View[Adjacency.NegX] = this.FaceNegX.GetValue(iY, this.Z.Val);
			this.View[Adjacency.NegZ] = this.FaceNegZ.GetValue(this.X.Val, iY);
		}

		protected override void StartNonuniformCenter(int iY)
		{
			this.Assigner.SetEdgeYNegXNegZ(this.Center.Values, iY, this.View);
			this.SetNonuniformEdgeTo(iY);
		}

		protected override void ShiftNonuniformCenter(int iY)
		{
			var center = this.Center.Values;
			this.View.ShiftCrossTowardsPositiveY();
			this.View[Adjacency.PosY] = center[this.X.Val, iY + 1, this.Z.Val];

			this.View[Adjacency.PosX] = center[this.X.Adj, iY, this.Z.Val];
			this.View[Adjacency.PosZ] = center[this.X.Val, iY, this.Z.Adj];
			this.SetNonuniformEdgeTo(iY);
		}
	}
}
