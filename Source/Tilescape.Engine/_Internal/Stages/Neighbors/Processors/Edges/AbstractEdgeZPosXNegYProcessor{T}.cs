﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeZPosXNegYProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeZPosXNegYProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected IChunkFaceView<T> FacePosX => this.Chunks.FacePosX;

		protected IChunkFaceView<T> FaceNegY => this.Chunks.FaceNegY;

		protected Boundary X => this.Assigner.PositiveSide;

		protected Boundary Y => this.Assigner.NegativeSide;

		protected override bool IsUniform() => this.FacePosX.Uniform.HasValue && this.FaceNegY.Uniform.HasValue;

		protected override void ProcessViewUniform(int iZ) => this.ProcessViewUniform(this.X.Val, this.Y.Val, iZ);

		protected override void ProcessViewNonuniform(int iZ) => this.ProcessViewNonuniform(this.X.Val, this.Y.Val, iZ);
	}
}
