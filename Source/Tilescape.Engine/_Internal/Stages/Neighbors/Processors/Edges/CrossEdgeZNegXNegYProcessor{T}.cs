﻿namespace Tilescape.Engine
{
	internal class CrossEdgeZNegXNegYProcessor<T> : AbstractEdgeZNegXNegYProcessor<T>
	{
		public CrossEdgeZNegXNegYProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public ITileCrossNeighbors<T> Neighbors { get; set; }

		protected override ITileCrossNeighbors<T> Chunks => this.Neighbors;

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.NegX] = this.FaceNegX.Uniform.Value;
			this.View[Adjacency.NegY] = this.FaceNegY.Uniform.Value;
		}

		protected override void SetNonuniformEdgeTo(int iZ)
		{
			this.View[Adjacency.NegX] = this.FaceNegX.GetValue(this.Y.Val, iZ);
			this.View[Adjacency.NegY] = this.FaceNegY.GetValue(this.X.Val, iZ);
		}

		protected override void StartNonuniformCenter(int iZ)
		{
			this.Assigner.SetEdgeZNegXNegY(this.Center.Values, iZ, this.View);
			this.SetNonuniformEdgeTo(iZ);
		}

		protected override void ShiftNonuniformCenter(int iZ)
		{
			var center = this.Center.Values;
			this.View.ShiftCrossTowardsPositiveZ();
			this.View[Adjacency.PosZ] = center[this.X.Val, this.Y.Val, iZ + 1];

			this.View[Adjacency.PosX] = center[this.X.Adj, this.Y.Val, iZ];
			this.View[Adjacency.PosY] = center[this.X.Val, this.Y.Adj, iZ];
			this.SetNonuniformEdgeTo(iZ);
		}
	}
}
