﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeYNegXPosZProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeYNegXPosZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FaceNegX => this.Chunks.FaceNegX;

		public IChunkFaceView<T> FacePosZ => this.Chunks.FacePosZ;

		protected Boundary X => this.Assigner.NegativeSide;

		protected Boundary Z => this.Assigner.PositiveSide;

		protected override bool IsUniform() => this.FaceNegX.Uniform.HasValue && this.FacePosZ.Uniform.HasValue;

		protected override void ProcessViewUniform(int iY) => this.ProcessViewUniform(this.X.Val, iY, this.Z.Val);

		protected override void ProcessViewNonuniform(int iY) => this.ProcessViewNonuniform(this.X.Val, iY, this.Z.Val);
	}
}
