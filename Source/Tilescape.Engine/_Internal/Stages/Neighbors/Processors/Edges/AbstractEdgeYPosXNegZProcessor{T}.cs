﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeYPosXNegZProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeYPosXNegZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FacePosX => this.Chunks.FacePosX;

		public IChunkFaceView<T> FaceNegZ => this.Chunks.FaceNegZ;

		protected Boundary X => this.Assigner.PositiveSide;

		protected Boundary Z => this.Assigner.NegativeSide;

		protected override bool IsUniform() => this.FacePosX.Uniform.HasValue && this.FaceNegZ.Uniform.HasValue;

		protected override void ProcessViewUniform(int iY) => this.ProcessViewUniform(this.X.Val, iY, this.Z.Val);

		protected override void ProcessViewNonuniform(int iY) => this.ProcessViewNonuniform(this.X.Val, iY, this.Z.Val);
	}
}
