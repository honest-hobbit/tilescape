﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeXNegYNegZProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeXNegYNegZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FaceNegY => this.Chunks.FaceNegY;

		public IChunkFaceView<T> FaceNegZ => this.Chunks.FaceNegZ;

		protected Boundary Y => this.Assigner.NegativeSide;

		protected Boundary Z => this.Assigner.NegativeSide;

		protected override bool IsUniform() => this.FaceNegY.Uniform.HasValue && this.FaceNegZ.Uniform.HasValue;

		protected override void ProcessViewUniform(int iX) => this.ProcessViewUniform(iX, this.Y.Val, this.Z.Val);

		protected override void ProcessViewNonuniform(int iX) => this.ProcessViewNonuniform(iX, this.Y.Val, this.Z.Val);
	}
}
