﻿namespace Tilescape.Engine
{
	internal class CrossEdgeYPosXNegZProcessor<T> : AbstractEdgeYPosXNegZProcessor<T>
	{
		public CrossEdgeYPosXNegZProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public ITileCrossNeighbors<T> Neighbors { get; set; }

		protected override ITileCrossNeighbors<T> Chunks => this.Neighbors;

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.PosX] = this.FacePosX.Uniform.Value;
			this.View[Adjacency.NegZ] = this.FaceNegZ.Uniform.Value;
		}

		protected override void SetNonuniformEdgeTo(int iY)
		{
			this.View[Adjacency.PosX] = this.FacePosX.GetValue(iY, this.Z.Val);
			this.View[Adjacency.NegZ] = this.FaceNegZ.GetValue(this.X.Val, iY);
		}

		protected override void StartNonuniformCenter(int iY)
		{
			this.Assigner.SetEdgeYPosXNegZ(this.Center.Values, iY, this.View);
			this.SetNonuniformEdgeTo(iY);
		}

		protected override void ShiftNonuniformCenter(int iY)
		{
			var center = this.Center.Values;
			this.View.ShiftCrossTowardsPositiveY();
			this.View[Adjacency.PosY] = center[this.X.Val, iY + 1, this.Z.Val];

			this.View[Adjacency.NegX] = center[this.X.Adj, iY, this.Z.Val];
			this.View[Adjacency.PosZ] = center[this.X.Val, iY, this.Z.Adj];
			this.SetNonuniformEdgeTo(iY);
		}
	}
}
