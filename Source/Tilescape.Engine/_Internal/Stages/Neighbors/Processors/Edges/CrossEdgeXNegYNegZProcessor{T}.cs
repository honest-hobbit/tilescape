﻿namespace Tilescape.Engine
{
	internal class CrossEdgeXNegYNegZProcessor<T> : AbstractEdgeXNegYNegZProcessor<T>
	{
		public CrossEdgeXNegYNegZProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public ITileCrossNeighbors<T> Neighbors { get; set; }

		protected override ITileCrossNeighbors<T> Chunks => this.Neighbors;

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.NegY] = this.FaceNegY.Uniform.Value;
			this.View[Adjacency.NegZ] = this.FaceNegZ.Uniform.Value;
		}

		protected override void SetNonuniformEdgeTo(int iX)
		{
			this.View[Adjacency.NegY] = this.FaceNegY.GetValue(iX, this.Z.Val);
			this.View[Adjacency.NegZ] = this.FaceNegZ.GetValue(iX, this.Y.Val);
		}

		protected override void StartNonuniformCenter(int iX)
		{
			this.Assigner.SetEdgeXNegYNegZ(this.Center.Values, iX, this.View);
			this.SetNonuniformEdgeTo(iX);
		}

		protected override void ShiftNonuniformCenter(int iX)
		{
			var center = this.Center.Values;
			this.View.ShiftCrossTowardsPositiveX();
			this.View[Adjacency.PosX] = center[iX + 1, this.Y.Val, this.Z.Val];

			this.View[Adjacency.PosY] = center[iX, this.Y.Adj, this.Z.Val];
			this.View[Adjacency.PosZ] = center[iX, this.Y.Val, this.Z.Adj];
			this.SetNonuniformEdgeTo(iX);
		}
	}
}
