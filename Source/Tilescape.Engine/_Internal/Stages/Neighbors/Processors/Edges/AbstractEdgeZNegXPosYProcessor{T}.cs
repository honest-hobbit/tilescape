﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeZNegXPosYProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeZNegXPosYProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected IChunkFaceView<T> FaceNegX => this.Chunks.FaceNegX;

		protected IChunkFaceView<T> FacePosY => this.Chunks.FacePosY;

		protected Boundary X => this.Assigner.NegativeSide;

		protected Boundary Y => this.Assigner.PositiveSide;

		protected override bool IsUniform() => this.FaceNegX.Uniform.HasValue && this.FacePosY.Uniform.HasValue;

		protected override void ProcessViewUniform(int iZ) => this.ProcessViewUniform(this.X.Val, this.Y.Val, iZ);

		protected override void ProcessViewNonuniform(int iZ) => this.ProcessViewNonuniform(this.X.Val, this.Y.Val, iZ);
	}
}
