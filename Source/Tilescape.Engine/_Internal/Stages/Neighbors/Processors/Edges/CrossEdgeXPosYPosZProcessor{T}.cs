﻿namespace Tilescape.Engine
{
	internal class CrossEdgeXPosYPosZProcessor<T> : AbstractEdgeXPosYPosZProcessor<T>
	{
		public CrossEdgeXPosYPosZProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public ITileCrossNeighbors<T> Neighbors { get; set; }

		protected override ITileCrossNeighbors<T> Chunks => this.Neighbors;

		protected override void SetUniform()
		{
			this.View.SetCrossTo(this.Center.Uniform.Value);
			this.View[Adjacency.PosY] = this.FacePosY.Uniform.Value;
			this.View[Adjacency.PosZ] = this.FacePosZ.Uniform.Value;
		}

		protected override void SetNonuniformEdgeTo(int iX)
		{
			this.View[Adjacency.PosY] = this.FacePosY.GetValue(iX, this.Z.Val);
			this.View[Adjacency.PosZ] = this.FacePosZ.GetValue(iX, this.Y.Val);
		}

		protected override void StartNonuniformCenter(int iX)
		{
			this.Assigner.SetEdgeXPosYPosZ(this.Center.Values, iX, this.View);
			this.SetNonuniformEdgeTo(iX);
		}

		protected override void ShiftNonuniformCenter(int iX)
		{
			var center = this.Center.Values;
			this.View.ShiftCrossTowardsPositiveX();
			this.View[Adjacency.PosX] = center[iX + 1, this.Y.Val, this.Z.Val];

			this.View[Adjacency.NegY] = center[iX, this.Y.Adj, this.Z.Val];
			this.View[Adjacency.NegZ] = center[iX, this.Y.Val, this.Z.Adj];
			this.SetNonuniformEdgeTo(iX);
		}
	}
}
