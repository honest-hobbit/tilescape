﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeYNegXNegZProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeYNegXNegZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FaceNegX => this.Chunks.FaceNegX;

		public IChunkFaceView<T> FaceNegZ => this.Chunks.FaceNegZ;

		protected Boundary X => this.Assigner.NegativeSide;

		protected Boundary Z => this.Assigner.NegativeSide;

		protected override bool IsUniform() => this.FaceNegX.Uniform.HasValue && this.FaceNegZ.Uniform.HasValue;

		protected override void ProcessViewUniform(int iY) => this.ProcessViewUniform(this.X.Val, iY, this.Z.Val);

		protected override void ProcessViewNonuniform(int iY) => this.ProcessViewNonuniform(this.X.Val, iY, this.Z.Val);
	}
}
