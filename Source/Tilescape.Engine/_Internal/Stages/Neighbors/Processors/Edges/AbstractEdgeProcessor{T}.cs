﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeProcessor<T> : AbstractAdjacencyProcessor<T>
	{
		public AbstractEdgeProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public override void Process()
		{
			if (this.Center.Uniform.HasValue)
			{
				if (this.IsUniform())
				{
					// all uniform
					this.SetUniform();
					if (this.CanSkipUniform())
					{
						return;
					}

					for (int iA = 1; iA < this.SideMax; iA++)
					{
						this.ProcessViewUniform(iA);
					}
				}
				else
				{
					// uniform center, nonuniform face
					this.Assigner.SetTo(this.Center.Uniform.Value, this.View);

					// TODO this could be handled more efficiently by using the shifting pattern
					for (int iA = 1; iA < this.SideMax; iA++)
					{
						this.SetNonuniformEdgeTo(iA);
						this.ProcessViewNonuniform(iA);
					}
				}
			}
			else
			{
				// nonuniform center
				int iA = 1;
				this.StartNonuniformCenter(iA);
				this.ProcessViewNonuniform(iA);

				for (iA = 2; iA < this.SideMax; iA++)
				{
					this.ShiftNonuniformCenter(iA);
					this.ProcessViewNonuniform(iA);
				}
			}
		}

		protected abstract bool IsUniform();

		protected abstract void ProcessViewUniform(int iA);

		protected abstract void ProcessViewNonuniform(int iA);

		protected abstract void SetUniform();

		protected abstract void SetNonuniformEdgeTo(int iA);

		protected abstract void StartNonuniformCenter(int iA);

		protected abstract void ShiftNonuniformCenter(int iA);
	}
}
