﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeYPosXPosZProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeYPosXPosZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FacePosX => this.Chunks.FacePosX;

		public IChunkFaceView<T> FacePosZ => this.Chunks.FacePosZ;

		protected Boundary X => this.Assigner.PositiveSide;

		protected Boundary Z => this.Assigner.PositiveSide;

		protected override bool IsUniform() => this.FacePosX.Uniform.HasValue && this.FacePosZ.Uniform.HasValue;

		protected override void ProcessViewUniform(int iY) => this.ProcessViewUniform(this.X.Val, iY, this.Z.Val);

		protected override void ProcessViewNonuniform(int iY) => this.ProcessViewNonuniform(this.X.Val, iY, this.Z.Val);
	}
}
