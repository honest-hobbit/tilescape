﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeXNegYPosZProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeXNegYPosZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FaceNegY => this.Chunks.FaceNegY;

		public IChunkFaceView<T> FacePosZ => this.Chunks.FacePosZ;

		protected Boundary Y => this.Assigner.NegativeSide;

		protected Boundary Z => this.Assigner.PositiveSide;

		protected override bool IsUniform() => this.FaceNegY.Uniform.HasValue && this.FacePosZ.Uniform.HasValue;

		protected override void ProcessViewUniform(int iX) => this.ProcessViewUniform(iX, this.Y.Val, this.Z.Val);

		protected override void ProcessViewNonuniform(int iX) => this.ProcessViewNonuniform(iX, this.Y.Val, this.Z.Val);
	}
}
