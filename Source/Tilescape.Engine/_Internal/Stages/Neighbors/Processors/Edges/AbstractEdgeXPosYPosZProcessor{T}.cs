﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeXPosYPosZProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeXPosYPosZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FacePosY => this.Chunks.FacePosY;

		public IChunkFaceView<T> FacePosZ => this.Chunks.FacePosZ;

		protected Boundary Y => this.Assigner.PositiveSide;

		protected Boundary Z => this.Assigner.PositiveSide;

		protected override bool IsUniform() => this.FacePosY.Uniform.HasValue && this.FacePosZ.Uniform.HasValue;

		protected override void ProcessViewUniform(int iX) => this.ProcessViewUniform(iX, this.Y.Val, this.Z.Val);

		protected override void ProcessViewNonuniform(int iX) => this.ProcessViewNonuniform(iX, this.Y.Val, this.Z.Val);
	}
}
