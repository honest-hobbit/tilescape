﻿namespace Tilescape.Engine
{
	internal abstract class AbstractEdgeXPosYNegZProcessor<T> : AbstractEdgeProcessor<T>
	{
		public AbstractEdgeXPosYNegZProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public IChunkFaceView<T> FacePosY => this.Chunks.FacePosY;

		public IChunkFaceView<T> FaceNegZ => this.Chunks.FaceNegZ;

		protected Boundary Y => this.Assigner.PositiveSide;

		protected Boundary Z => this.Assigner.NegativeSide;

		protected override bool IsUniform() => this.FacePosY.Uniform.HasValue && this.FaceNegZ.Uniform.HasValue;

		protected override void ProcessViewUniform(int iX) => this.ProcessViewUniform(iX, this.Y.Val, this.Z.Val);

		protected override void ProcessViewNonuniform(int iX) => this.ProcessViewNonuniform(iX, this.Y.Val, this.Z.Val);
	}
}
