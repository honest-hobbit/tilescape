﻿namespace Tilescape.Engine
{
	internal abstract class AbstractCenterProcessor<T> : AbstractAdjacencyProcessor<T>
	{
		public AbstractCenterProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public ITileCrossNeighbors<T> Neighbors { get; set; }

		protected override ITileCrossNeighbors<T> Chunks => this.Neighbors;

		public override void Process()
		{
			if (this.Center.Uniform.HasValue)
			{
				// uniform center
				this.Assigner.SetTo(this.Center.Uniform.Value, this.View);

				if (this.CanSkipUniform())
				{
					return;
				}

				for (int iX = 1; iX < this.SideMax; iX++)
				{
					for (int iY = 1; iY < this.SideMax; iY++)
					{
						for (int iZ = 1; iZ < this.SideMax; iZ++)
						{
							this.ProcessViewUniform(iX, iY, iZ);
						}
					}
				}
			}
			else
			{
				// nonuniform center
				for (int iZ = 1; iZ < this.SideMax; iZ++)
				{
					for (int iY = 1; iY < this.SideMax; iY++)
					{
						int iX = 1;
						this.CenterStart(iX, iY, iZ);
						this.ProcessViewNonuniform(iX, iY, iZ);

						for (iX = 2; iX < this.SideMax; iX++)
						{
							this.CenterShiftTowardsPositiveX(iX, iY, iZ);
							this.ProcessViewNonuniform(iX, iY, iZ);
						}
					}
				}
			}
		}

		protected abstract void CenterStart(int iX, int iY, int iZ);

		protected abstract void CenterShiftTowardsPositiveX(int iX, int iY, int iZ);
	}
}
