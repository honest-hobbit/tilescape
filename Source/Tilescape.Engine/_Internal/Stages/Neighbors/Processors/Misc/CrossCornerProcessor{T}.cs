﻿namespace Tilescape.Engine
{
	internal class CrossCornerProcessor<T> : AbstractAdjacencyProcessor<T>
	{
		public CrossCornerProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		public ITileCrossNeighbors<T> Neighbors { get; set; }

		protected override ITileCrossNeighbors<T> Chunks => this.Neighbors;

		private IChunkFaceView<T> FaceNegX => this.Neighbors.FaceNegX;

		private IChunkFaceView<T> FacePosX => this.Neighbors.FacePosX;

		private IChunkFaceView<T> FaceNegY => this.Neighbors.FaceNegY;

		private IChunkFaceView<T> FacePosY => this.Neighbors.FacePosY;

		private IChunkFaceView<T> FaceNegZ => this.Neighbors.FaceNegZ;

		private IChunkFaceView<T> FacePosZ => this.Neighbors.FacePosZ;

		public override void Process()
		{
			this.ProcessCornerNegXNegYNegZ();
			this.ProcessCornerPosXNegYNegZ();
			this.ProcessCornerNegXPosYNegZ();
			this.ProcessCornerPosXPosYNegZ();

			this.ProcessCornerNegXNegYPosZ();
			this.ProcessCornerPosXNegYPosZ();
			this.ProcessCornerNegXPosYPosZ();
			this.ProcessCornerPosXPosYPosZ();
		}

		public void ProcessCornerNegXNegYNegZ()
		{
			if (this.Center.Uniform.HasValue)
			{
				this.View.SetCrossTo(this.Center.Uniform.Value);
			}
			else
			{
				this.Assigner.SetCornerNegXNegYNegZ(this.Center.Values, this.View);
			}

			// constants
			var x = this.Assigner.NegativeSide;
			var y = this.Assigner.NegativeSide;
			var z = this.Assigner.NegativeSide;

			this.View[Adjacency.NegX] = this.FaceNegX.GetValue(y.Val, z.Val);
			this.View[Adjacency.NegY] = this.FaceNegY.GetValue(x.Val, z.Val);
			this.View[Adjacency.NegZ] = this.FaceNegZ.GetValue(x.Val, y.Val);
			this.ProcessViewNonuniform(x.Val, y.Val, z.Val);
		}

		public void ProcessCornerPosXNegYNegZ()
		{
			if (this.Center.Uniform.HasValue)
			{
				this.View.SetCrossTo(this.Center.Uniform.Value);
			}
			else
			{
				this.Assigner.SetCornerPosXNegYNegZ(this.Center.Values, this.View);
			}

			// constants
			var x = this.Assigner.PositiveSide;
			var y = this.Assigner.NegativeSide;
			var z = this.Assigner.NegativeSide;

			this.View[Adjacency.PosX] = this.FacePosX.GetValue(y.Val, z.Val);
			this.View[Adjacency.NegY] = this.FaceNegY.GetValue(x.Val, z.Val);
			this.View[Adjacency.NegZ] = this.FaceNegZ.GetValue(x.Val, y.Val);
			this.ProcessViewNonuniform(x.Val, y.Val, z.Val);
		}

		public void ProcessCornerNegXPosYNegZ()
		{
			if (this.Center.Uniform.HasValue)
			{
				this.View.SetCrossTo(this.Center.Uniform.Value);
			}
			else
			{
				this.Assigner.SetCornerNegXPosYNegZ(this.Center.Values, this.View);
			}

			// constants
			var x = this.Assigner.NegativeSide;
			var y = this.Assigner.PositiveSide;
			var z = this.Assigner.NegativeSide;

			this.View[Adjacency.NegX] = this.FaceNegX.GetValue(y.Val, z.Val);
			this.View[Adjacency.PosY] = this.FacePosY.GetValue(x.Val, z.Val);
			this.View[Adjacency.NegZ] = this.FaceNegZ.GetValue(x.Val, y.Val);
			this.ProcessViewNonuniform(x.Val, y.Val, z.Val);
		}

		public void ProcessCornerPosXPosYNegZ()
		{
			if (this.Center.Uniform.HasValue)
			{
				this.View.SetCrossTo(this.Center.Uniform.Value);
			}
			else
			{
				this.Assigner.SetCornerPosXPosYNegZ(this.Center.Values, this.View);
			}

			// constants
			var x = this.Assigner.PositiveSide;
			var y = this.Assigner.PositiveSide;
			var z = this.Assigner.NegativeSide;

			this.View[Adjacency.PosX] = this.FacePosX.GetValue(y.Val, z.Val);
			this.View[Adjacency.PosY] = this.FacePosY.GetValue(x.Val, z.Val);
			this.View[Adjacency.NegZ] = this.FaceNegZ.GetValue(x.Val, y.Val);
			this.ProcessViewNonuniform(x.Val, y.Val, z.Val);
		}

		public void ProcessCornerNegXNegYPosZ()
		{
			if (this.Center.Uniform.HasValue)
			{
				this.View.SetCrossTo(this.Center.Uniform.Value);
			}
			else
			{
				this.Assigner.SetCornerNegXNegYPosZ(this.Center.Values, this.View);
			}

			// constants
			var x = this.Assigner.NegativeSide;
			var y = this.Assigner.NegativeSide;
			var z = this.Assigner.PositiveSide;

			this.View[Adjacency.NegX] = this.FaceNegX.GetValue(y.Val, z.Val);
			this.View[Adjacency.NegY] = this.FaceNegY.GetValue(x.Val, z.Val);
			this.View[Adjacency.PosZ] = this.FacePosZ.GetValue(x.Val, y.Val);
			this.ProcessViewNonuniform(x.Val, y.Val, z.Val);
		}

		public void ProcessCornerPosXNegYPosZ()
		{
			if (this.Center.Uniform.HasValue)
			{
				this.View.SetCrossTo(this.Center.Uniform.Value);
			}
			else
			{
				this.Assigner.SetCornerPosXNegYPosZ(this.Center.Values, this.View);
			}

			// constants
			var x = this.Assigner.PositiveSide;
			var y = this.Assigner.NegativeSide;
			var z = this.Assigner.PositiveSide;

			this.View[Adjacency.PosX] = this.FacePosX.GetValue(y.Val, z.Val);
			this.View[Adjacency.NegY] = this.FaceNegY.GetValue(x.Val, z.Val);
			this.View[Adjacency.PosZ] = this.FacePosZ.GetValue(x.Val, y.Val);
			this.ProcessViewNonuniform(x.Val, y.Val, z.Val);
		}

		public void ProcessCornerNegXPosYPosZ()
		{
			if (this.Center.Uniform.HasValue)
			{
				this.View.SetCrossTo(this.Center.Uniform.Value);
			}
			else
			{
				this.Assigner.SetCornerNegXPosYPosZ(this.Center.Values, this.View);
			}

			// constants
			var x = this.Assigner.NegativeSide;
			var y = this.Assigner.PositiveSide;
			var z = this.Assigner.PositiveSide;

			this.View[Adjacency.NegX] = this.FaceNegX.GetValue(y.Val, z.Val);
			this.View[Adjacency.PosY] = this.FacePosY.GetValue(x.Val, z.Val);
			this.View[Adjacency.PosZ] = this.FacePosZ.GetValue(x.Val, y.Val);
			this.ProcessViewNonuniform(x.Val, y.Val, z.Val);
		}

		public void ProcessCornerPosXPosYPosZ()
		{
			if (this.Center.Uniform.HasValue)
			{
				this.View.SetCrossTo(this.Center.Uniform.Value);
			}
			else
			{
				this.Assigner.SetCornerPosXPosYPosZ(this.Center.Values, this.View);
			}

			// constants
			var x = this.Assigner.PositiveSide;
			var y = this.Assigner.PositiveSide;
			var z = this.Assigner.PositiveSide;

			this.View[Adjacency.PosX] = this.FacePosX.GetValue(y.Val, z.Val);
			this.View[Adjacency.PosY] = this.FacePosY.GetValue(x.Val, z.Val);
			this.View[Adjacency.PosZ] = this.FacePosZ.GetValue(x.Val, y.Val);
			this.ProcessViewNonuniform(x.Val, y.Val, z.Val);
		}
	}
}
