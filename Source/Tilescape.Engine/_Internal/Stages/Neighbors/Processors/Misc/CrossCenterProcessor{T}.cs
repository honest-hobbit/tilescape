﻿namespace Tilescape.Engine
{
	internal class CrossCenterProcessor<T> : AbstractCenterProcessor<T>
	{
		public CrossCenterProcessor(AdjacencyCrossAssigner assigner, IAdjacencyViewProcessor<T> processor)
			: base(assigner, processor)
		{
		}

		protected override void CenterStart(int iX, int iY, int iZ)
		{
			this.Assigner.SetInside(this.Center.Values, iX, iY, iZ, this.View);
		}

		protected override void CenterShiftTowardsPositiveX(int iX, int iY, int iZ)
		{
			var center = this.Center.Values;
			this.View.ShiftCrossTowardsPositiveX();
			this.View[Adjacency.PosX] = center[iX + 1, iY, iZ];

			this.View[Adjacency.NegY] = center[iX, iY - 1, iZ];
			this.View[Adjacency.PosY] = center[iX, iY + 1, iZ];
			this.View[Adjacency.NegZ] = center[iX, iY, iZ - 1];
			this.View[Adjacency.PosZ] = center[iX, iY, iZ + 1];
		}
	}
}
