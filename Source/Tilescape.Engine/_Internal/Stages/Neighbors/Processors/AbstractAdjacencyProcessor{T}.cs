﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal abstract class AbstractAdjacencyProcessor<T>
	{
		private readonly IAdjacencyViewProcessor<T> processor;

		public AbstractAdjacencyProcessor(AbstractAdjacencyAssigner assigner, IAdjacencyViewProcessor<T> processor)
		{
			Require.That(assigner != null);
			Require.That(processor != null);

			this.Assigner = assigner;
			this.processor = processor;
		}

		public int SizeExponent => this.Assigner.SizeExponent;

		public int SideLength => this.Assigner.SideLength;

		public int SideMax => this.Assigner.SideMax;

		protected AbstractAdjacencyAssigner Assigner { get; }

		protected AdjacencyCube<T> View => this.processor.View;

		protected IChunkView<T> Center => this.Chunks.Center;

		protected abstract ITileCrossNeighbors<T> Chunks { get; }

		public abstract void Process();

		protected bool CanSkipUniform() => this.processor.CanSkipPartOfChunkUniform();

		protected void ProcessViewUniform(int x, int y, int z) => this.processor.ProcessViewPartlyUniform(x, y, z);

		protected void ProcessViewNonuniform(int x, int y, int z) => this.processor.ProcessViewNonuniform(x, y, z);
	}
}
