﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indexables;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Pooling;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class TileNeighbors : ITileCrossNeighbors<ITileProjectionsView>
	{
		private readonly Chunk buildersCenter;

		private readonly ChunkFace buildersNegativeX;

		private readonly ChunkFace buildersPositiveX;

		private readonly ChunkFace buildersNegativeY;

		private readonly ChunkFace buildersPositiveY;

		private readonly ChunkFace buildersNegativeZ;

		private readonly ChunkFace buildersPositiveZ;

		private TileProjectionsViewBuilder fullyUniformBuilder = null;

		public TileNeighbors(ITilescapeAtlas atlas)
		{
			Require.That(atlas != null);

			this.Config = atlas.Config;
			var pool = CreateTileProjectionPool(atlas.Config);
			this.buildersCenter = new Chunk(atlas, pool);
			this.buildersNegativeX = new ChunkFace(atlas, pool);
			this.buildersPositiveX = new ChunkFace(atlas, pool);
			this.buildersNegativeY = new ChunkFace(atlas, pool);
			this.buildersPositiveY = new ChunkFace(atlas, pool);
			this.buildersNegativeZ = new ChunkFace(atlas, pool);
			this.buildersPositiveZ = new ChunkFace(atlas, pool);
		}

		/// <inheritdoc />
		public ITileStageConfig Config { get; }

		/// <inheritdoc />
		public IChunkView<ITileProjectionsView> Center
		{
			get
			{
				ITileCrossNeighborsContracts.Center(this);
				return this.buildersCenter;
			}
		}

		/// <inheritdoc />
		public IChunkFaceView<ITileProjectionsView> FaceNegX
		{
			get
			{
				ITileCrossNeighborsContracts.FaceNegX(this);
				return this.buildersNegativeX;
			}
		}

		/// <inheritdoc />
		public IChunkFaceView<ITileProjectionsView> FacePosX
		{
			get
			{
				ITileCrossNeighborsContracts.FacePosX(this);
				return this.buildersPositiveX;
			}
		}

		/// <inheritdoc />
		public IChunkFaceView<ITileProjectionsView> FaceNegY
		{
			get
			{
				ITileCrossNeighborsContracts.FaceNegY(this);
				return this.buildersNegativeY;
			}
		}

		/// <inheritdoc />
		public IChunkFaceView<ITileProjectionsView> FacePosY
		{
			get
			{
				ITileCrossNeighborsContracts.FacePosY(this);
				return this.buildersPositiveY;
			}
		}

		/// <inheritdoc />
		public IChunkFaceView<ITileProjectionsView> FaceNegZ
		{
			get
			{
				ITileCrossNeighborsContracts.FaceNegZ(this);
				return this.buildersNegativeZ;
			}
		}

		/// <inheritdoc />
		public IChunkFaceView<ITileProjectionsView> FacePosZ
		{
			get
			{
				ITileCrossNeighborsContracts.FacePosZ(this);
				return this.buildersPositiveZ;
			}
		}

		/// <inheritdoc />
		public bool IsPopulated { get; private set; }

		/// <inheritdoc />
		public void Clear()
		{
			ITileNeighborsContracts.Clear(this);

			this.IsPopulated = false;
			this.fullyUniformBuilder = null;
			this.buildersCenter.Clear();
			this.buildersNegativeX.Clear();
			this.buildersPositiveX.Clear();
			this.buildersNegativeY.Clear();
			this.buildersPositiveY.Clear();
			this.buildersNegativeZ.Clear();
			this.buildersPositiveZ.Clear();
		}

		/// <inheritdoc />
		public void Populate(IAdjacencyView<ITileChunkView> chunks)
		{
			ITileNeighborsContracts.Populate(this, chunks);

			this.IsPopulated = true;
			this.PopulateCenter(chunks[Adjacency.Center]);

			int sideMax = this.Config.ChunkLengthInTiles - 1;
			this.PopulateFaceX(chunks[Adjacency.NegX], this.buildersNegativeX, sideMax);
			this.PopulateFaceX(chunks[Adjacency.PosX], this.buildersPositiveX, 0);
			this.PopulateFaceY(chunks[Adjacency.NegY], this.buildersNegativeY, sideMax);
			this.PopulateFaceY(chunks[Adjacency.PosY], this.buildersPositiveY, 0);
			this.PopulateFaceZ(chunks[Adjacency.NegZ], this.buildersNegativeZ, sideMax);
			this.PopulateFaceZ(chunks[Adjacency.PosZ], this.buildersPositiveZ, 0);

			this.CheckUniform();
		}

		/// <inheritdoc />
		public bool TryGetUniformValue(out ITileProjectionsView uniformValue)
		{
			ITileCrossNeighborsContracts.TryGetUniformValue(this);

			uniformValue = this.fullyUniformBuilder;
			return uniformValue != null;
		}

		private static Pool<TileProjection> CreateTileProjectionPool(ITileStageConfig config)
		{
			Require.That(config != null);

			var lengths = TileShapeProjection.CreateLengthsList(config);
			Func<TileProjection> factory = () => new TileProjection(config, lengths);
			var pool = new Pool<TileProjection>(factory);

			var tilesPerChunk = MathUtility.IntegerPower(config.ChunkLengthInTiles, 3);
			var tilesPerChunkFace = MathUtility.IntegerPower(config.ChunkLengthInTiles, 2);
			var totalTiles = tilesPerChunk + (tilesPerChunkFace * 6);
			pool.GiveMany(Factory.CreateMany(totalTiles, factory));
			return pool;
		}

		private void PopulateCenter(ITileChunkView view)
		{
			Require.That(view != null);

			if (view.IsUniform)
			{
				this.buildersCenter.UniformBuilder.Add(view.UniformTile.Value);
				this.buildersCenter.UniformBuilder.Sort();
				this.buildersCenter.IsUniform = true;
			}
			else
			{
				var tiles = view.Tiles;
				for (int index = 0; index < tiles.Length; index++)
				{
					var builder = this.buildersCenter.Builders[index];
					builder.Add(view.Tiles[index]);
					builder.Sort();
				}
			}
		}

		private void PopulateFaceX(ITileChunkView view, ChunkFace face, int fixedIndex)
		{
			Require.That(view != null);
			Require.That(face != null);

			if (view.IsUniform)
			{
				face.UniformBuilder.Add(view.UniformTile.Value);
				face.UniformBuilder.Sort();
				face.IsUniform = true;
			}
			else
			{
				var sideLength = this.Config.ChunkLengthInTiles;
				for (int iY = 0; iY < sideLength; iY++)
				{
					for (int iZ = 0; iZ < sideLength; iZ++)
					{
						var builder = face.Builders[iY, iZ];
						builder.Add(view.Tiles[fixedIndex, iY, iZ]);
						builder.Sort();
					}
				}

				face.CheckUniform();
			}
		}

		private void PopulateFaceY(ITileChunkView view, ChunkFace face, int fixedIndex)
		{
			Require.That(view != null);
			Require.That(face != null);

			if (view.IsUniform)
			{
				face.UniformBuilder.Add(view.UniformTile.Value);
				face.UniformBuilder.Sort();
				face.IsUniform = true;
			}
			else
			{
				var sideLength = this.Config.ChunkLengthInTiles;
				for (int iX = 0; iX < sideLength; iX++)
				{
					for (int iZ = 0; iZ < sideLength; iZ++)
					{
						var builder = face.Builders[iX, iZ];
						builder.Add(view.Tiles[iX, fixedIndex, iZ]);
						builder.Sort();
					}
				}

				face.CheckUniform();
			}
		}

		private void PopulateFaceZ(ITileChunkView view, ChunkFace face, int fixedIndex)
		{
			Require.That(view != null);
			Require.That(face != null);

			if (view.IsUniform)
			{
				face.UniformBuilder.Add(view.UniformTile.Value);
				face.UniformBuilder.Sort();
				face.IsUniform = true;
			}
			else
			{
				var sideLength = this.Config.ChunkLengthInTiles;
				for (int iX = 0; iX < sideLength; iX++)
				{
					for (int iY = 0; iY < sideLength; iY++)
					{
						var builder = face.Builders[iX, iY];
						builder.Add(view.Tiles[iX, iY, fixedIndex]);
						builder.Sort();
					}
				}

				face.CheckUniform();
			}
		}

		private void CheckUniform()
		{
			if (!this.buildersCenter.IsUniform ||
				!this.buildersNegativeX.IsUniform || !this.buildersPositiveX.IsUniform ||
				!this.buildersNegativeY.IsUniform || !this.buildersPositiveY.IsUniform ||
				!this.buildersNegativeZ.IsUniform || !this.buildersPositiveZ.IsUniform)
			{
				return;
			}

			var uniformBuilder = this.buildersCenter.UniformBuilder;
			if (this.buildersNegativeX.UniformBuilder != uniformBuilder ||
				this.buildersPositiveX.UniformBuilder != uniformBuilder ||
				this.buildersNegativeY.UniformBuilder != uniformBuilder ||
				this.buildersPositiveY.UniformBuilder != uniformBuilder ||
				this.buildersNegativeZ.UniformBuilder != uniformBuilder ||
				this.buildersPositiveZ.UniformBuilder != uniformBuilder)
			{
				return;
			}

			this.fullyUniformBuilder = this.buildersCenter.UniformBuilder;
		}

		#region Private Classes

		private class Chunk : IChunkView<ITileProjectionsView>
		{
			private readonly CubeArray builders;

			public Chunk(ITilescapeAtlas atlas, IPool<TileProjection> pool)
			{
				Require.That(atlas != null);
				Require.That(pool != null);

				this.builders = new CubeArray(atlas, pool);
				this.UniformBuilder = new TileProjectionsViewBuilder(atlas, pool);
				this.IsUniform = false;
			}

			/// <inheritdoc />
			public int SizeExponent => this.builders.SizeExponent;

			/// <inheritdoc />
			public ICubeArrayView<ITileProjectionsView> Values
			{
				get
				{
					IChunkViewContracts.Values(this);
					return this.builders;
				}
			}

			public ICubeArrayView<TileProjectionsViewBuilder> Builders => this.builders;

			public TileProjectionsViewBuilder UniformBuilder { get; }

			public bool IsUniform
			{
				get => this.Uniform.HasValue;
				set => this.Uniform = value ? Try.Value<ITileProjectionsView>(this.UniformBuilder) : Try.None<ITileProjectionsView>();
			}

			/// <inheritdoc />
			public Try<ITileProjectionsView> Uniform { get; private set; }

			public void Clear()
			{
				if (this.IsUniform)
				{
					this.IsUniform = false;
					this.UniformBuilder.Clear();
				}
				else
				{
					this.builders.Array.ForEachListItem(x => x.Clear());
				}
			}

			private class CubeArray : CubeArray<TileProjectionsViewBuilder>, ICubeArrayView<ITileProjectionsView>
			{
				public CubeArray(ITilescapeAtlas atlas, IPool<TileProjection> pool)
					: base(atlas.Config.ChunkSizeExponent)
				{
					this.Array.SetAllTo(index => new TileProjectionsViewBuilder(atlas, pool));
				}

				ITileProjectionsView ICubeArrayView<ITileProjectionsView>.this[int index] => this[index];

				ITileProjectionsView IIndexableView<Index3D, ITileProjectionsView>.this[Index3D index] => this[index];

				ITileProjectionsView ICubeArrayView<ITileProjectionsView>.this[int x, int y, int z] => this[x, y, z];

				IEnumerator<IndexValuePair<Index3D, ITileProjectionsView>>
					IEnumerable<IndexValuePair<Index3D, ITileProjectionsView>>.GetEnumerator()
				{
					foreach (var x in this)
					{
						yield return IndexValuePair.New(x.Index, (ITileProjectionsView)x.Value);
					}
				}
			}
		}

		private class ChunkFace : IChunkFaceView<ITileProjectionsView>
		{
			private readonly SquareArray builders;

			public ChunkFace(ITilescapeAtlas atlas, IPool<TileProjection> pool)
			{
				Require.That(atlas != null);
				Require.That(pool != null);

				this.builders = new SquareArray(atlas, pool);
				this.UniformBuilder = new TileProjectionsViewBuilder(atlas, pool);
				this.IsUniform = false;
			}

			/// <inheritdoc />
			public int SizeExponent => this.builders.SizeExponent;

			/// <inheritdoc />
			public ISquareArrayView<ITileProjectionsView> Values
			{
				get
				{
					IChunkFaceViewContracts.Values(this);
					return this.builders;
				}
			}

			public ISquareArrayView<TileProjectionsViewBuilder> Builders => this.builders;

			public TileProjectionsViewBuilder UniformBuilder { get; }

			public bool IsUniform
			{
				get => this.Uniform.HasValue;
				set => this.Uniform = value ? Try.Value<ITileProjectionsView>(this.UniformBuilder) : Try.None<ITileProjectionsView>();
			}

			/// <inheritdoc />
			public Try<ITileProjectionsView> Uniform { get; private set; }

			public void CheckUniform()
			{
				Require.That(!this.IsUniform);

				var first = this.builders[0];
				for (int index = 1; index < this.builders.Array.Length; index++)
				{
					if (this.builders.Array[index] != first)
					{
						return;
					}
				}

				// tiles being copied into uniform builder have already been sorted so no need to sort them again
				this.IsUniform = true;
				for (int index = 0; index < first.Tiles.Count; index++)
				{
					this.UniformBuilder.Add(first.Tiles[index].Tile);
				}

				// chunk is uniform now so Clear will not clear the builders array, so clear it now instead
				this.builders.Array.ForEachListItem(x => x.Clear());
			}

			public void Clear()
			{
				if (this.IsUniform)
				{
					this.IsUniform = false;
					this.UniformBuilder.Clear();
				}
				else
				{
					this.builders.Array.ForEachListItem(x => x.Clear());
				}
			}

			private class SquareArray : SquareArray<TileProjectionsViewBuilder>, ISquareArrayView<ITileProjectionsView>
			{
				public SquareArray(ITilescapeAtlas atlas, IPool<TileProjection> pool)
					: base(atlas.Config.ChunkSizeExponent)
				{
					this.Array.SetAllTo(index => new TileProjectionsViewBuilder(atlas, pool));
				}

				ITileProjectionsView ISquareArrayView<ITileProjectionsView>.this[int index] => this[index];

				ITileProjectionsView IIndexableView<Index2D, ITileProjectionsView>.this[Index2D index] => this[index];

				ITileProjectionsView ISquareArrayView<ITileProjectionsView>.this[int x, int y] => this[x, y];

				IEnumerator<IndexValuePair<Index2D, ITileProjectionsView>>
					IEnumerable<IndexValuePair<Index2D, ITileProjectionsView>>.GetEnumerator()
				{
					foreach (var x in this)
					{
						yield return IndexValuePair.New(x.Index, (ITileProjectionsView)x.Value);
					}
				}
			}
		}

		#endregion
	}
}
