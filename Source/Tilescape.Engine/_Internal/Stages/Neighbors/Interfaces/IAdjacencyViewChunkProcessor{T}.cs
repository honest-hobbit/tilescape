﻿namespace Tilescape.Engine
{
	internal interface IAdjacencyViewChunkProcessor<T> : IAdjacencyViewProcessor<T>
	{
		// This is only called if the entire chunk (and its adjacent tiles) are uniform.
		// The uniform values will be stored in the View before this method is called.
		// If this is called then CanSkipPartOfChunkUniform will not be called.
		// Return true if iterating the chunk can be skipped.
		// Return false if the chunk should still be iterated with the uniform value
		// using the ProcessViewEntirelyUniform method.
		bool CanSkipEntireChunkUniform();

		// This is only called if CanSkipEntireChunkUniform is called and returns false.
		void ProcessViewEntirelyUniform(int x, int y, int z);
	}
}
