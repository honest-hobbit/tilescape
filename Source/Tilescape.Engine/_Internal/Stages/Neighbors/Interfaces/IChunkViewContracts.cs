﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class IChunkViewContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Values<T>(IChunkView<T> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.Uniform.HasValue);
		}
	}
}
