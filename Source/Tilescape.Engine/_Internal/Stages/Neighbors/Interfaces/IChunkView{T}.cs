﻿using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal interface IChunkView<T>
	{
		int SizeExponent { get; }

		ICubeArrayView<T> Values { get; }

		Try<T> Uniform { get; }
	}
}
