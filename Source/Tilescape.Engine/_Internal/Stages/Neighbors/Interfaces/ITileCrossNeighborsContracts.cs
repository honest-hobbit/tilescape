﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class ITileCrossNeighborsContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Center<T>(ITileCrossNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void FaceNegX<T>(ITileCrossNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void FacePosX<T>(ITileCrossNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void FaceNegY<T>(ITileCrossNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void FacePosY<T>(ITileCrossNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void FaceNegZ<T>(ITileCrossNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void FacePosZ<T>(ITileCrossNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void TryGetUniformValue<T>(ITileCrossNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}
	}
}
