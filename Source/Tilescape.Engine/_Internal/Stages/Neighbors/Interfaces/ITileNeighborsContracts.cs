﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class ITileNeighborsContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Clear<T>(ITileNeighbors<T> instance)
		{
			Require.That(instance != null);
			Require.That(instance.IsPopulated);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void Populate<T>(ITileNeighbors<T> instance, IAdjacencyView<ITileChunkView> chunks)
		{
			Require.That(instance != null);
			Require.That(!instance.IsPopulated);
			Require.That(chunks != null);
			Require.That(chunks.AllNotNull());
		}
	}
}
