﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class IChunkFaceViewContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Values<T>(IChunkFaceView<T> instance)
		{
			Require.That(instance != null);
			Require.That(!instance.Uniform.HasValue);
		}
	}
}
