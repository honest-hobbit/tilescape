﻿namespace Tilescape.Engine
{
	internal interface ITileCrossNeighbors<T> : ITileNeighbors<T>
	{
		IChunkView<T> Center { get; }

		IChunkFaceView<T> FaceNegX { get; }

		IChunkFaceView<T> FacePosX { get; }

		IChunkFaceView<T> FaceNegY { get; }

		IChunkFaceView<T> FacePosY { get; }

		IChunkFaceView<T> FaceNegZ { get; }

		IChunkFaceView<T> FacePosZ { get; }

		bool TryGetUniformValue(out T uniformValue);
	}
}
