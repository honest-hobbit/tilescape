﻿using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal interface IChunkFaceView<T>
	{
		int SizeExponent { get; }

		ISquareArrayView<T> Values { get; }

		Try<T> Uniform { get; }
	}
}
