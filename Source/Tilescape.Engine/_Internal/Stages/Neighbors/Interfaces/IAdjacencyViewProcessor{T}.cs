﻿namespace Tilescape.Engine
{
	internal interface IAdjacencyViewProcessor<T>
	{
		AdjacencyCube<T> View { get; }

		// This is called if a portion of the chunk is uniform.
		// The uniform values will be stored in the View before this method is called.
		// Return true if iterating this portion of the chunk can be skipped.
		// Return false if the chunk should still be iterated with the uniform value
		// using the ProcessViewPartlyUniform method.
		bool CanSkipPartOfChunkUniform();

		// This is only called if CanSkipPartOfChunkUniform is called and returns false.
		void ProcessViewPartlyUniform(int x, int y, int z);

		// This is only called if CanSkipPartOfChunkUniform or CanSkipEntireChunkUniform
		// are not called because the chunk is not uniform.
		void ProcessViewNonuniform(int x, int y, int z);
	}
}
