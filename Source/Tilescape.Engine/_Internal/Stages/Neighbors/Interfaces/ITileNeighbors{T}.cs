﻿namespace Tilescape.Engine
{
	internal interface ITileNeighbors<T>
	{
		ITileStageConfig Config { get; }

		bool IsPopulated { get; }

		void Clear();

		void Populate(IAdjacencyView<ITileChunkView> chunks);
	}
}
