﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class IChunkFaceViewExtensions
	{
		public static T GetValue<T>(this IChunkFaceView<T> face, int a, int b)
		{
			Require.That(face != null);

			return face.Uniform.HasValue ? face.Uniform.Value : face.Values[a, b];
		}
	}
}
