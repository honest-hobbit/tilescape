﻿namespace Tilescape.Engine
{
	internal struct Boundary
	{
		public Boundary(int value, int adjacent)
		{
			this.Val = value;
			this.Adj = adjacent;
		}

		public readonly int Val;

		public readonly int Adj;
	}
}
