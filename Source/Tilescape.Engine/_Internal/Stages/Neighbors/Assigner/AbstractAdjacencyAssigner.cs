﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal abstract class AbstractAdjacencyAssigner
	{
		public AbstractAdjacencyAssigner(int sizeExponent)
		{
			Require.That(sizeExponent >= 0);

			this.SizeExponent = sizeExponent;
			this.SideLength = BinaryArray.GetSideLength(sizeExponent);
			this.SideMax = this.SideLength - 1;
			this.NegativeSide = new Boundary(0, 1);
			this.PositiveSide = new Boundary(this.SideMax, this.SideMax - 1);
		}

		public int SizeExponent { get; }

		public int SideLength { get; }

		public int SideMax { get; }

		public Boundary NegativeSide { get; }

		public Boundary PositiveSide { get; }

		public abstract void SetTo<T>(T value, AdjacencyCube<T> cross);

		public abstract void SetInside<T>(ICubeArrayView<T> cube, int x, int y, int z, AdjacencyCube<T> cross);

		#region Set Face

		public abstract void SetFaceNegX<T>(ICubeArrayView<T> cube, int y, int z, AdjacencyCube<T> cross);

		public abstract void SetFacePosX<T>(ICubeArrayView<T> cube, int y, int z, AdjacencyCube<T> cross);

		public abstract void SetFaceNegY<T>(ICubeArrayView<T> cube, int x, int z, AdjacencyCube<T> cross);

		public abstract void SetFacePosY<T>(ICubeArrayView<T> cube, int x, int z, AdjacencyCube<T> cross);

		public abstract void SetFaceNegZ<T>(ICubeArrayView<T> cube, int x, int y, AdjacencyCube<T> cross);

		public abstract void SetFacePosZ<T>(ICubeArrayView<T> cube, int x, int y, AdjacencyCube<T> cross);

		#endregion

		#region Set Edge

		public abstract void SetEdgeXNegYNegZ<T>(ICubeArrayView<T> cube, int x, AdjacencyCube<T> cross);

		public abstract void SetEdgeXPosYNegZ<T>(ICubeArrayView<T> cube, int x, AdjacencyCube<T> cross);

		public abstract void SetEdgeXNegYPosZ<T>(ICubeArrayView<T> cube, int x, AdjacencyCube<T> cross);

		public abstract void SetEdgeXPosYPosZ<T>(ICubeArrayView<T> cube, int x, AdjacencyCube<T> cross);

		public abstract void SetEdgeYNegXNegZ<T>(ICubeArrayView<T> cube, int y, AdjacencyCube<T> cross);

		public abstract void SetEdgeYPosXNegZ<T>(ICubeArrayView<T> cube, int y, AdjacencyCube<T> cross);

		public abstract void SetEdgeYNegXPosZ<T>(ICubeArrayView<T> cube, int y, AdjacencyCube<T> cross);

		public abstract void SetEdgeYPosXPosZ<T>(ICubeArrayView<T> cube, int y, AdjacencyCube<T> cross);

		public abstract void SetEdgeZNegXNegY<T>(ICubeArrayView<T> cube, int z, AdjacencyCube<T> cross);

		public abstract void SetEdgeZPosXNegY<T>(ICubeArrayView<T> cube, int z, AdjacencyCube<T> cross);

		public abstract void SetEdgeZNegXPosY<T>(ICubeArrayView<T> cube, int z, AdjacencyCube<T> cross);

		public abstract void SetEdgeZPosXPosY<T>(ICubeArrayView<T> cube, int z, AdjacencyCube<T> cross);

		#endregion

		#region Set Corner

		public abstract void SetCornerNegXNegYNegZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross);

		public abstract void SetCornerPosXNegYNegZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross);

		public abstract void SetCornerNegXPosYNegZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross);

		public abstract void SetCornerPosXPosYNegZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross);

		public abstract void SetCornerNegXNegYPosZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross);

		public abstract void SetCornerPosXNegYPosZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross);

		public abstract void SetCornerNegXPosYPosZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross);

		public abstract void SetCornerPosXPosYPosZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross);

		#endregion

		#region Contracts

		[Conditional(Require.CompilationSymbol)]
		protected void Contracts<T>(AdjacencyCube<T> cross)
		{
			Require.That(cross != null);
		}

		[Conditional(Require.CompilationSymbol)]
		protected void Contracts<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			Require.That(cube != null);
			Require.That(cube.SizeExponent == this.SizeExponent);
			Require.That(cross != null);
		}

		[Conditional(Require.CompilationSymbol)]
		protected void Contracts<T>(ICubeArrayView<T> cube, int a, AdjacencyCube<T> cross)
		{
			Require.That(cube != null);
			Require.That(cube.SizeExponent == this.SizeExponent);
			Require.That(a > 0 && a < this.SideMax);
			Require.That(cross != null);
		}

		[Conditional(Require.CompilationSymbol)]
		protected void Contracts<T>(ICubeArrayView<T> cube, int a, int b, AdjacencyCube<T> cross)
		{
			Require.That(cube != null);
			Require.That(cube.SizeExponent == this.SizeExponent);
			Require.That(a > 0 && a < this.SideMax);
			Require.That(b > 0 && b < this.SideMax);
			Require.That(cross != null);
		}

		[Conditional(Require.CompilationSymbol)]
		protected void Contracts<T>(ICubeArrayView<T> cube, int a, int b, int c, AdjacencyCube<T> cross)
		{
			Require.That(cube != null);
			Require.That(cube.SizeExponent == this.SizeExponent);
			Require.That(a > 0 && a < this.SideMax);
			Require.That(b > 0 && b < this.SideMax);
			Require.That(c > 0 && c < this.SideMax);
			Require.That(cross != null);
		}

		#endregion
	}
}
