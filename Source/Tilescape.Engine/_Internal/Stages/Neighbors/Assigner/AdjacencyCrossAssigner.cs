﻿namespace Tilescape.Engine
{
	internal class AdjacencyCrossAssigner : AbstractAdjacencyAssigner
	{
		public AdjacencyCrossAssigner(int sizeExponent)
			: base(sizeExponent)
		{
		}

		public override void SetTo<T>(T value, AdjacencyCube<T> cross)
		{
			this.Contracts(cross);

			cross.SetCrossTo(value);
		}

		public override void SetInside<T>(ICubeArrayView<T> cube, int x, int y, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, y, z, cross);

			cross[Adjacency.Center] = cube[x, y, z];
			cross[Adjacency.NegX] = cube[x - 1, y, z];
			cross[Adjacency.PosX] = cube[x + 1, y, z];
			cross[Adjacency.NegY] = cube[x, y - 1, z];
			cross[Adjacency.PosY] = cube[x, y + 1, z];
			cross[Adjacency.NegZ] = cube[x, y, z - 1];
			cross[Adjacency.PosZ] = cube[x, y, z + 1];
		}

		#region Set Face

		public override void SetFaceNegX<T>(ICubeArrayView<T> cube, int y, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, y, z, cross);

			var x = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y, z];
			cross[Adjacency.PosX] = cube[x.Adj, y, z];
			cross[Adjacency.NegY] = cube[x.Val, y - 1, z];
			cross[Adjacency.PosY] = cube[x.Val, y + 1, z];
			cross[Adjacency.NegZ] = cube[x.Val, y, z - 1];
			cross[Adjacency.PosZ] = cube[x.Val, y, z + 1];
		}

		public override void SetFacePosX<T>(ICubeArrayView<T> cube, int y, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, y, z, cross);

			var x = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y, z];
			cross[Adjacency.NegX] = cube[x.Adj, y, z];
			cross[Adjacency.NegY] = cube[x.Val, y - 1, z];
			cross[Adjacency.PosY] = cube[x.Val, y + 1, z];
			cross[Adjacency.NegZ] = cube[x.Val, y, z - 1];
			cross[Adjacency.PosZ] = cube[x.Val, y, z + 1];
		}

		public override void SetFaceNegY<T>(ICubeArrayView<T> cube, int x, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, z, cross);

			var y = this.NegativeSide;

			cross[Adjacency.Center] = cube[x, y.Val, z];
			cross[Adjacency.NegX] = cube[x - 1, y.Val, z];
			cross[Adjacency.PosX] = cube[x + 1, y.Val, z];
			cross[Adjacency.PosY] = cube[x, y.Adj, z];
			cross[Adjacency.NegZ] = cube[x, y.Val, z - 1];
			cross[Adjacency.PosZ] = cube[x, y.Val, z + 1];
		}

		public override void SetFacePosY<T>(ICubeArrayView<T> cube, int x, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, z, cross);

			var y = this.PositiveSide;

			cross[Adjacency.Center] = cube[x, y.Val, z];
			cross[Adjacency.NegX] = cube[x - 1, y.Val, z];
			cross[Adjacency.PosX] = cube[x + 1, y.Val, z];
			cross[Adjacency.NegY] = cube[x, y.Adj, z];
			cross[Adjacency.NegZ] = cube[x, y.Val, z - 1];
			cross[Adjacency.PosZ] = cube[x, y.Val, z + 1];
		}

		public override void SetFaceNegZ<T>(ICubeArrayView<T> cube, int x, int y, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, y, cross);

			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x, y, z.Val];
			cross[Adjacency.NegX] = cube[x - 1, y, z.Val];
			cross[Adjacency.PosX] = cube[x + 1, y, z.Val];
			cross[Adjacency.NegY] = cube[x, y - 1, z.Val];
			cross[Adjacency.PosY] = cube[x, y + 1, z.Val];
			cross[Adjacency.PosZ] = cube[x, y, z.Adj];
		}

		public override void SetFacePosZ<T>(ICubeArrayView<T> cube, int x, int y, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, y, cross);

			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x, y, z.Val];
			cross[Adjacency.NegX] = cube[x - 1, y, z.Val];
			cross[Adjacency.PosX] = cube[x + 1, y, z.Val];
			cross[Adjacency.NegY] = cube[x, y - 1, z.Val];
			cross[Adjacency.PosY] = cube[x, y + 1, z.Val];
			cross[Adjacency.NegZ] = cube[x, y, z.Adj];
		}

		#endregion

		#region Set Edge

		public override void SetEdgeXNegYNegZ<T>(ICubeArrayView<T> cube, int x, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, cross);

			var y = this.NegativeSide;
			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x, y.Val, z.Val];
			cross[Adjacency.NegX] = cube[x - 1, y.Val, z.Val];
			cross[Adjacency.PosX] = cube[x + 1, y.Val, z.Val];
			cross[Adjacency.PosY] = cube[x, y.Adj, z.Val];
			cross[Adjacency.PosZ] = cube[x, y.Val, z.Adj];
		}

		public override void SetEdgeXPosYNegZ<T>(ICubeArrayView<T> cube, int x, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, cross);

			var y = this.PositiveSide;
			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x, y.Val, z.Val];
			cross[Adjacency.NegX] = cube[x - 1, y.Val, z.Val];
			cross[Adjacency.PosX] = cube[x + 1, y.Val, z.Val];
			cross[Adjacency.NegY] = cube[x, y.Adj, z.Val];
			cross[Adjacency.PosZ] = cube[x, y.Val, z.Adj];
		}

		public override void SetEdgeXNegYPosZ<T>(ICubeArrayView<T> cube, int x, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, cross);

			var y = this.NegativeSide;
			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x, y.Val, z.Val];
			cross[Adjacency.NegX] = cube[x - 1, y.Val, z.Val];
			cross[Adjacency.PosX] = cube[x + 1, y.Val, z.Val];
			cross[Adjacency.PosY] = cube[x, y.Adj, z.Val];
			cross[Adjacency.NegZ] = cube[x, y.Val, z.Adj];
		}

		public override void SetEdgeXPosYPosZ<T>(ICubeArrayView<T> cube, int x, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, x, cross);

			var y = this.PositiveSide;
			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x, y.Val, z.Val];
			cross[Adjacency.NegX] = cube[x - 1, y.Val, z.Val];
			cross[Adjacency.PosX] = cube[x + 1, y.Val, z.Val];
			cross[Adjacency.NegY] = cube[x, y.Adj, z.Val];
			cross[Adjacency.NegZ] = cube[x, y.Val, z.Adj];
		}

		public override void SetEdgeYNegXNegZ<T>(ICubeArrayView<T> cube, int y, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, y, cross);

			var x = this.NegativeSide;
			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y, z.Val];
			cross[Adjacency.PosX] = cube[x.Adj, y, z.Val];
			cross[Adjacency.NegY] = cube[x.Val, y - 1, z.Val];
			cross[Adjacency.PosY] = cube[x.Val, y + 1, z.Val];
			cross[Adjacency.PosZ] = cube[x.Val, y, z.Adj];
		}

		public override void SetEdgeYPosXNegZ<T>(ICubeArrayView<T> cube, int y, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, y, cross);

			var x = this.PositiveSide;
			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y, z.Val];
			cross[Adjacency.NegX] = cube[x.Adj, y, z.Val];
			cross[Adjacency.NegY] = cube[x.Val, y - 1, z.Val];
			cross[Adjacency.PosY] = cube[x.Val, y + 1, z.Val];
			cross[Adjacency.PosZ] = cube[x.Val, y, z.Adj];
		}

		public override void SetEdgeYNegXPosZ<T>(ICubeArrayView<T> cube, int y, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, y, cross);

			var x = this.NegativeSide;
			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y, z.Val];
			cross[Adjacency.PosX] = cube[x.Adj, y, z.Val];
			cross[Adjacency.NegY] = cube[x.Val, y - 1, z.Val];
			cross[Adjacency.PosY] = cube[x.Val, y + 1, z.Val];
			cross[Adjacency.NegZ] = cube[x.Val, y, z.Adj];
		}

		public override void SetEdgeYPosXPosZ<T>(ICubeArrayView<T> cube, int y, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, y, cross);

			var x = this.PositiveSide;
			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y, z.Val];
			cross[Adjacency.NegX] = cube[x.Adj, y, z.Val];
			cross[Adjacency.NegY] = cube[x.Val, y - 1, z.Val];
			cross[Adjacency.PosY] = cube[x.Val, y + 1, z.Val];
			cross[Adjacency.NegZ] = cube[x.Val, y, z.Adj];
		}

		public override void SetEdgeZNegXNegY<T>(ICubeArrayView<T> cube, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, z, cross);

			var x = this.NegativeSide;
			var y = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z];
			cross[Adjacency.PosX] = cube[x.Adj, y.Val, z];
			cross[Adjacency.PosY] = cube[x.Val, y.Adj, z];
			cross[Adjacency.NegZ] = cube[x.Val, y.Val, z - 1];
			cross[Adjacency.PosZ] = cube[x.Val, y.Val, z + 1];
		}

		public override void SetEdgeZPosXNegY<T>(ICubeArrayView<T> cube, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, z, cross);

			var x = this.PositiveSide;
			var y = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z];
			cross[Adjacency.NegX] = cube[x.Adj, y.Val, z];
			cross[Adjacency.PosY] = cube[x.Val, y.Adj, z];
			cross[Adjacency.NegZ] = cube[x.Val, y.Val, z - 1];
			cross[Adjacency.PosZ] = cube[x.Val, y.Val, z + 1];
		}

		public override void SetEdgeZNegXPosY<T>(ICubeArrayView<T> cube, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, z, cross);

			var x = this.NegativeSide;
			var y = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z];
			cross[Adjacency.PosX] = cube[x.Adj, y.Val, z];
			cross[Adjacency.NegY] = cube[x.Val, y.Adj, z];
			cross[Adjacency.NegZ] = cube[x.Val, y.Val, z - 1];
			cross[Adjacency.PosZ] = cube[x.Val, y.Val, z + 1];
		}

		public override void SetEdgeZPosXPosY<T>(ICubeArrayView<T> cube, int z, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, z, cross);

			var x = this.PositiveSide;
			var y = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z];
			cross[Adjacency.NegX] = cube[x.Adj, y.Val, z];
			cross[Adjacency.NegY] = cube[x.Val, y.Adj, z];
			cross[Adjacency.NegZ] = cube[x.Val, y.Val, z - 1];
			cross[Adjacency.PosZ] = cube[x.Val, y.Val, z + 1];
		}

		#endregion

		#region Set Corner

		public override void SetCornerNegXNegYNegZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, cross);

			var x = this.NegativeSide;
			var y = this.NegativeSide;
			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z.Val];
			cross[Adjacency.PosX] = cube[x.Adj, y.Val, z.Val];
			cross[Adjacency.PosY] = cube[x.Val, y.Adj, z.Val];
			cross[Adjacency.PosZ] = cube[x.Val, y.Val, z.Adj];
		}

		public override void SetCornerPosXNegYNegZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, cross);

			var x = this.PositiveSide;
			var y = this.NegativeSide;
			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z.Val];
			cross[Adjacency.NegX] = cube[x.Adj, y.Val, z.Val];
			cross[Adjacency.PosY] = cube[x.Val, y.Adj, z.Val];
			cross[Adjacency.PosZ] = cube[x.Val, y.Val, z.Adj];
		}

		public override void SetCornerNegXPosYNegZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, cross);

			var x = this.NegativeSide;
			var y = this.PositiveSide;
			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z.Val];
			cross[Adjacency.PosX] = cube[x.Adj, y.Val, z.Val];
			cross[Adjacency.NegY] = cube[x.Val, y.Adj, z.Val];
			cross[Adjacency.PosZ] = cube[x.Val, y.Val, z.Adj];
		}

		public override void SetCornerPosXPosYNegZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, cross);

			var x = this.PositiveSide;
			var y = this.PositiveSide;
			var z = this.NegativeSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z.Val];
			cross[Adjacency.NegX] = cube[x.Adj, y.Val, z.Val];
			cross[Adjacency.NegY] = cube[x.Val, y.Adj, z.Val];
			cross[Adjacency.PosZ] = cube[x.Val, y.Val, z.Adj];
		}

		public override void SetCornerNegXNegYPosZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, cross);

			var x = this.NegativeSide;
			var y = this.NegativeSide;
			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z.Val];
			cross[Adjacency.PosX] = cube[x.Adj, y.Val, z.Val];
			cross[Adjacency.PosY] = cube[x.Val, y.Adj, z.Val];
			cross[Adjacency.NegZ] = cube[x.Val, y.Val, z.Adj];
		}

		public override void SetCornerPosXNegYPosZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, cross);

			var x = this.PositiveSide;
			var y = this.NegativeSide;
			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z.Val];
			cross[Adjacency.NegX] = cube[x.Adj, y.Val, z.Val];
			cross[Adjacency.PosY] = cube[x.Val, y.Adj, z.Val];
			cross[Adjacency.NegZ] = cube[x.Val, y.Val, z.Adj];
		}

		public override void SetCornerNegXPosYPosZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, cross);

			var x = this.NegativeSide;
			var y = this.PositiveSide;
			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z.Val];
			cross[Adjacency.PosX] = cube[x.Adj, y.Val, z.Val];
			cross[Adjacency.NegY] = cube[x.Val, y.Adj, z.Val];
			cross[Adjacency.NegZ] = cube[x.Val, y.Val, z.Adj];
		}

		public override void SetCornerPosXPosYPosZ<T>(ICubeArrayView<T> cube, AdjacencyCube<T> cross)
		{
			this.Contracts(cube, cross);

			var x = this.PositiveSide;
			var y = this.PositiveSide;
			var z = this.PositiveSide;

			cross[Adjacency.Center] = cube[x.Val, y.Val, z.Val];
			cross[Adjacency.NegX] = cube[x.Adj, y.Val, z.Val];
			cross[Adjacency.NegY] = cube[x.Val, y.Adj, z.Val];
			cross[Adjacency.NegZ] = cube[x.Val, y.Val, z.Adj];
		}

		#endregion
	}
}
