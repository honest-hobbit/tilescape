﻿namespace Tilescape.Engine
{
	internal interface ITileProjection
	{
		int SizeExponent { get; }

		bool IsAssigned { get; }

		Tile Tile { get; }

		ITileTypeOLD Type { get; }

		ITileShapeProjection Shape { get; }
	}
}
