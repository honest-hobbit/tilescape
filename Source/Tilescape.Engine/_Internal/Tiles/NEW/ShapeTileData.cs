﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;

namespace Tilescape.Engine
{
	internal struct ShapeTileData
	{
		public ShapeTileData(TileTypeKey type, TileShapeKey shape, TileColorPaletteKey colorPalette, OrthoAxis orientation)
		{
			Require.That(type.Index != 0, $"{nameof(type)}.{nameof(type.Index)} of 0 is reserved for Unassigned tiles.");
			Require.That(Enumeration.IsDefined(orientation));

			this.Type = type;
			this.Shape = shape;
			this.ColorPalette = colorPalette;
			this.Orientation = orientation;
		}

		public static ShapeTileData Unassigned => default(ShapeTileData);

		public TileTypeKey Type { get; }

		public TileShapeKey Shape { get; }

		public TileColorPaletteKey ColorPalette { get; }

		public OrthoAxis Orientation { get; }

		/// <inheritdoc />
		public override string ToString() =>
			$"[{this.Type.ToString()}, {this.Shape.ToString()}, {this.ColorPalette.ToString()}, {this.Orientation.ToString()}]";

		public TileData ToTileData() =>
			new TileData(this.Type, this.Shape.Index, this.ColorPalette.Index, (byte)this.Orientation);

		public ShapeTileData SetType(TileTypeKey type) =>
			new ShapeTileData(type, this.Shape, this.ColorPalette, this.Orientation);

		public ShapeTileData SetShape(TileShapeKey shape) =>
			new ShapeTileData(this.Type, shape, this.ColorPalette, this.Orientation);

		public ShapeTileData SetColorPalette(TileColorPaletteKey colorPalette) =>
			new ShapeTileData(this.Type, this.Shape, colorPalette, this.Orientation);

		public ShapeTileData SetOrientation(OrthoAxis orientation) =>
			new ShapeTileData(this.Type, this.Shape, this.ColorPalette, orientation);

		public ShapeTileData Set(
			TileTypeKey? type = null,
			TileShapeKey? shape = null,
			TileColorPaletteKey? colorPalette = null,
			OrthoAxis? orientation = null) => new ShapeTileData(
				type ?? this.Type,
				shape ?? this.Shape,
				colorPalette ?? this.ColorPalette,
				orientation ?? this.Orientation);
	}
}
