﻿using System;
using System.Runtime.InteropServices;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal struct TileData : IEquatable<TileData>
	{
		public TileData(
			TileTypeKey type,
			byte data0 = 0,
			byte data1 = 0,
			byte data2 = 0,
			byte data3 = 0,
			byte data4 = 0,
			byte data5 = 0,
			byte data6 = 0,
			byte data7 = 0,
			byte data8 = 0,
			byte data9 = 0,
			byte data10 = 0,
			byte data11 = 0)
		{
			Require.That(type.Index != 0, $"{nameof(type)}.{nameof(type.Index)} of 0 is reserved for Unassigned tiles.");

			this.Type = type;
			this.Data0 = data0;
			this.Data1 = data1;
			this.Data2 = data2;
			this.Data3 = data3;
			this.Data4 = data4;
			this.Data5 = data5;
			this.Data6 = data6;
			this.Data7 = data7;
			this.Data8 = data8;
			this.Data9 = data9;
			this.Data10 = data10;
			this.Data11 = data11;
		}

		public static TileData Unassigned => default(TileData);

		public TileTypeKey Type { get; }

		public byte Data0 { get; }

		public byte Data1 { get; }

		public byte Data2 { get; }

		public byte Data3 { get; }

		public byte Data4 { get; }

		public byte Data5 { get; }

		public byte Data6 { get; }

		public byte Data7 { get; }

		public byte Data8 { get; }

		public byte Data9 { get; }

		public byte Data10 { get; }

		public byte Data11 { get; }

		public static bool operator ==(TileData lhs, TileData rhs) => lhs.Equals(rhs);

		public static bool operator !=(TileData lhs, TileData rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public bool Equals(TileData other) =>
			this.Type == other.Type &&
			this.Data0 == other.Data0 &&
			this.Data1 == other.Data1 &&
			this.Data2 == other.Data2 &&
			this.Data3 == other.Data3 &&
			this.Data4 == other.Data4 &&
			this.Data5 == other.Data5 &&
			this.Data6 == other.Data6 &&
			this.Data7 == other.Data7 &&
			this.Data8 == other.Data8 &&
			this.Data9 == other.Data9 &&
			this.Data10 == other.Data10 &&
			this.Data11 == other.Data11;

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => HashCode.Start(this.Type)
			.And(this.Data0).And(this.Data1).And(this.Data2).And(this.Data3).And(this.Data4).And(this.Data5)
			.And(this.Data6).And(this.Data7).And(this.Data8).And(this.Data9).And(this.Data10).And(this.Data11);

		/// <inheritdoc />
		public override string ToString() =>
			$"[{this.Type.ToString()}: {this.Data0.ToString()}, {this.Data1.ToString()}, {this.Data2.ToString()}, {this.Data3.ToString()}, {this.Data4.ToString()}, {this.Data5.ToString()}, {this.Data6.ToString()}, {this.Data7.ToString()}, {this.Data8.ToString()}, {this.Data9.ToString()}, {this.Data10.ToString()}, {this.Data11.ToString()}]";

		public ShapeTileData ToShapeTile() => new ShapeTileData(
			this.Type, new TileShapeKey(this.Data0), new TileColorPaletteKey(this.Data1), (OrthoAxis)this.Data2);
	}
}
