﻿using System.Runtime.InteropServices;

namespace Tilescape.Engine
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal struct TileDataNode
	{
		public TileDataNode(TileData tile, ushort nextIndex = ushort.MaxValue)
		{
			this.Tile = tile;
			this.NextIndex = nextIndex;
		}

		public TileData Tile { get; }

		public ushort NextIndex { get; }
	}
}
