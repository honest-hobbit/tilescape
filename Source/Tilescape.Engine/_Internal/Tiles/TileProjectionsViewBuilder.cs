﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Pooling;

namespace Tilescape.Engine
{
	internal sealed class TileProjectionsViewBuilder : ITileProjectionsView, IEquatable<TileProjectionsViewBuilder>
	{
		private readonly List<TileProjection> tiles = new List<TileProjection>();

		private readonly IDictionaryView<TileTypeKey, ITileTypeOLD> atlas;

		private readonly IPool<TileProjection> pool;

		private readonly Faces faces;

		public TileProjectionsViewBuilder(ITilescapeAtlas atlas, IPool<TileProjection> pool)
		{
			Require.That(atlas != null);
			Require.That(pool != null);

			this.atlas = atlas.Types;
			this.pool = pool;
			this.SizeExponent = atlas.Config.TileSizeExponent;
			this.Tiles = ListView.Convert(this.tiles, x => (ITileProjection)x);
			this.faces = new Faces(this);
		}

		public int SizeExponent { get; }

		/// <inheritdoc />
		public IListView<ITileProjection> Tiles { get; }

		/// <inheritdoc />
		public ITileFaces<ITileFaceMask> FaceMasks => this.faces;

		public static bool operator ==(TileProjectionsViewBuilder lhs, TileProjectionsViewBuilder rhs) => lhs.Equals(rhs);

		public static bool operator !=(TileProjectionsViewBuilder lhs, TileProjectionsViewBuilder rhs) => !lhs.Equals(rhs);

		public void Add(Tile tile)
		{
			Require.That(tile != Tile.Unassigned);

			var projection = this.pool.Take();
			projection.SetProjection(this.atlas, tile);
			this.tiles.Add(projection);
		}

		public void Sort()
		{
			if (this.tiles.Count > 1)
			{
				// sorts tiles with lower Ordering to come before tiles with higher Ordering
				// earlier tiles will be drawn over by later tiles
				this.tiles.Sort((a, b) => a.Type.Ordering - b.Type.Ordering);
			}
		}

		public void Clear()
		{
			for (int index = 0; index < this.tiles.Count; index++)
			{
				var projection = this.tiles[index];
				projection.Clear();
				this.pool.Give(projection);
			}

			this.tiles.Clear();
			this.faces.Clear();
		}

		/// <inheritdoc />
		public bool Equals(TileProjectionsViewBuilder other)
		{
			if ((object)other == null)
			{
				return false;
			}

			if (other.tiles.Count != this.tiles.Count)
			{
				return false;
			}

			for (int index = 0; index < this.tiles.Count; index++)
			{
				if (this.tiles[index].Tile != other.tiles[index].Tile)
				{
					return false;
				}
			}

			return true;
		}

		/// <inheritdoc />
		public override bool Equals(object obj) => obj is TileProjectionsViewBuilder other ? this.Equals(other) : false;

		/// <inheritdoc />
		public override int GetHashCode()
		{
			if (this.tiles.Count == 0)
			{
				return 0;
			}

			var hash = HashCode.Start(this.tiles[0].Tile);
			for (int index = 1; index < this.tiles.Count; index++)
			{
				hash = hash.And(this.tiles[index].Tile);
			}

			return hash;
		}

		private class Faces : ITileFaces<ITileFaceMask>
		{
			private readonly FaceMaskCombiner negativeX;

			private readonly FaceMaskCombiner positiveX;

			private readonly FaceMaskCombiner negativeY;

			private readonly FaceMaskCombiner positiveY;

			private readonly FaceMaskCombiner negativeZ;

			private readonly FaceMaskCombiner positiveZ;

			public Faces(TileProjectionsViewBuilder parent)
			{
				Require.That(parent != null);

				int length = TileFaceMask.GetMaskLength(parent.SizeExponent);
				this.negativeX = new FaceMaskCombiner(length, parent.tiles, faces => faces.NegativeX);
				this.positiveX = new FaceMaskCombiner(length, parent.tiles, faces => faces.PositiveX);
				this.negativeY = new FaceMaskCombiner(length, parent.tiles, faces => faces.NegativeY);
				this.positiveY = new FaceMaskCombiner(length, parent.tiles, faces => faces.PositiveY);
				this.negativeZ = new FaceMaskCombiner(length, parent.tiles, faces => faces.NegativeZ);
				this.positiveZ = new FaceMaskCombiner(length, parent.tiles, faces => faces.PositiveZ);
			}

			/// <inheritdoc />
			public ITileFaceMask NegativeX => this.negativeX.AssignedMask;

			/// <inheritdoc />
			public ITileFaceMask PositiveX => this.positiveX.AssignedMask;

			/// <inheritdoc />
			public ITileFaceMask NegativeY => this.negativeY.AssignedMask;

			/// <inheritdoc />
			public ITileFaceMask PositiveY => this.positiveY.AssignedMask;

			/// <inheritdoc />
			public ITileFaceMask NegativeZ => this.negativeZ.AssignedMask;

			/// <inheritdoc />
			public ITileFaceMask PositiveZ => this.positiveZ.AssignedMask;

			public void Clear()
			{
				this.negativeX.Clear();
				this.positiveX.Clear();
				this.negativeY.Clear();
				this.positiveY.Clear();
				this.negativeZ.Clear();
				this.positiveZ.Clear();
			}

			private class FaceMaskCombiner : ITileFaceMask
			{
				private readonly ulong[] combinedMask;

				private readonly IList<TileProjection> tiles;

				private readonly Func<ITileFaces<ITileFaceMask>, ITileFaceMask> selectFace;

				private ITileFaceMask assignedMask;

				public FaceMaskCombiner(
					int length, List<TileProjection> tiles, Func<ITileFaces<ITileFaceMask>, ITileFaceMask> selectFace)
				{
					Require.That(length >= 1);
					Require.That(tiles != null);
					Require.That(selectFace != null);

					this.combinedMask = new ulong[length];
					this.tiles = tiles;
					this.selectFace = selectFace;
				}

				public ITileFaceMask AssignedMask
				{
					get
					{
						this.AssignMask();
						return this.assignedMask;
					}
				}

				/// <inheritdoc />
				public int Count => this.combinedMask.Length;

				private bool IsAssigned => this.assignedMask != null;

				private bool IsCombined => this.assignedMask == this;

				/// <inheritdoc />
				public ulong this[int index]
				{
					get
					{
						Require.That(this.IsCombined);
						IListViewContracts.Indexer(this, index);

						return this.combinedMask[index];
					}
				}

				public void Clear()
				{
					if (this.IsCombined)
					{
						this.combinedMask.SetAllTo(0UL);
					}

					this.assignedMask = null;
				}

				/// <inheritdoc />
				public IEnumerator<ulong> GetEnumerator()
				{
					Require.That(this.IsCombined);

					return this.combinedMask.GetEnumerator<ulong>();
				}

				/// <inheritdoc />
				IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

				private void AssignMask()
				{
					if (this.IsAssigned)
					{
						return;
					}

					if (this.tiles.Count == 1)
					{
						this.assignedMask = this.selectFace(this.tiles[0].Shape.FaceMasks);
						return;
					}

					for (int index = 0; index < this.tiles.Count; index++)
					{
						this.CombineMask(this.selectFace(this.tiles[index].Shape.FaceMasks));
					}

					this.assignedMask = this;
				}

				private void CombineMask(ITileFaceMask mask)
				{
					Require.That(mask != null);
					Require.That(mask.Count == this.Count);

					for (int index = 0; index < this.Count; index++)
					{
						this.combinedMask[index] |= mask[index];
					}
				}
			}
		}
	}
}
