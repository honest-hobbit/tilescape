﻿using Tilescape.Utility.Collections;

namespace Tilescape.Engine
{
	internal interface ITileProjectionsView
	{
		IListView<ITileProjection> Tiles { get; }

		ITileFaces<ITileFaceMask> FaceMasks { get; }
	}
}
