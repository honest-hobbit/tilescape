﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal class TileProjection : ITileProjection
	{
		private readonly TileShapeProjection shape;

		private Tile tile = Tile.Unassigned;

		private ITileTypeOLD type = null;

		public TileProjection(ITileConfig config)
		{
			Require.That(config != null);

			this.shape = new TileShapeProjection(config);
		}

		public TileProjection(ITileConfig config, IListView<float> lengths)
		{
			Require.That(config != null);
			Require.That(lengths != null);

			this.shape = new TileShapeProjection(config, lengths);
		}

		public int SizeExponent => this.shape.SizeExponent;

		public bool IsAssigned { get; private set; } = false;

		public Tile Tile
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.tile;
			}
		}

		public ITileTypeOLD Type
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.type;
			}
		}

		public ITileShapeProjection Shape
		{
			get
			{
				Require.That(this.IsAssigned);
				return this.shape;
			}
		}

		public void Clear()
		{
			this.IsAssigned = false;
			this.tile = Tile.Unassigned;
			this.type = null;
			this.shape.Clear();
		}

		public void SetProjection(IDictionaryView<TileTypeKey, ITileTypeOLD> atlas, Tile tile)
		{
			Require.That(atlas != null);
			Require.That(tile != Tile.Unassigned);

			this.IsAssigned = true;
			this.tile = tile;

			this.type = atlas[tile.Type];
			this.shape.SetProjection(this.type.GetShapeData(tile));
		}

		////public void SetProjection(Tile tile, ITileType type)
		////{
		////	Require.That(tile != Tile.Unassigned);
		////	Require.That(type != null);

		////	this.IsAssigned = true;
		////	this.tile = tile;
		////	this.type = type;
		////	this.shape.SetProjection();
		////}
	}
}
