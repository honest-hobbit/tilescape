﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Engine
{
	internal struct CubicTileBits
	{
		private static readonly int Mask0 = BitPack.GetReadMask(0);

		private static readonly int Mask1 = BitPack.GetReadMask(1);

		private static readonly int Mask2 = BitPack.GetReadMask(2);

		private static readonly int Mask3 = BitPack.GetReadMask(3);

		private static readonly int Mask4 = BitPack.GetReadMask(4);

		private static readonly int Mask5 = BitPack.GetReadMask(5);

		private static readonly int Mask6 = BitPack.GetReadMask(6);

		private static readonly int Mask7 = BitPack.GetReadMask(7);

		public CubicTileBits(byte index)
		{
			this.Index = index;
		}

		public CubicTileBits(
			bool corner0, bool corner1, bool corner2, bool corner3, bool corner4, bool corner5, bool corner6, bool corner7)
		{
			// corners are encoded into the index as follows; 76543210
			// z | 0   0   1   1  y
			//   |-----------------
			// 1 | 2 - 3   6 - 7
			//   | |   |   |   |
			// 0 | 0 - 1   4 - 5
			//---------------------
			// x   0   1   0   1
			int index = 0;
			BitPack.Add(ref index, corner7);
			BitPack.Add(ref index, corner6);
			BitPack.Add(ref index, corner5);
			BitPack.Add(ref index, corner4);
			BitPack.Add(ref index, corner3);
			BitPack.Add(ref index, corner2);
			BitPack.Add(ref index, corner1);
			BitPack.Add(ref index, corner0);
			this.Index = (byte)index;
		}

		public static Index3D IndexOfCorner0 => new Index3D(0, 0, 0);

		public static Index3D IndexOfCorner1 => new Index3D(1, 0, 0);

		public static Index3D IndexOfCorner2 => new Index3D(0, 0, 1);

		public static Index3D IndexOfCorner3 => new Index3D(1, 0, 1);

		public static Index3D IndexOfCorner4 => new Index3D(0, 1, 0);

		public static Index3D IndexOfCorner5 => new Index3D(1, 1, 0);

		public static Index3D IndexOfCorner6 => new Index3D(0, 1, 1);

		public static Index3D IndexOfCorner7 => new Index3D(1, 1, 1);

		public byte Index { get; }

		public bool Corner0 => BitPack.ReadUsingMask(this.Index, Mask0);

		public bool Corner1 => BitPack.ReadUsingMask(this.Index, Mask1);

		public bool Corner2 => BitPack.ReadUsingMask(this.Index, Mask2);

		public bool Corner3 => BitPack.ReadUsingMask(this.Index, Mask3);

		public bool Corner4 => BitPack.ReadUsingMask(this.Index, Mask4);

		public bool Corner5 => BitPack.ReadUsingMask(this.Index, Mask5);

		public bool Corner6 => BitPack.ReadUsingMask(this.Index, Mask6);

		public bool Corner7 => BitPack.ReadUsingMask(this.Index, Mask7);

		public static CubicTileBits Create(Func<ITilescapeAtlas, Tile, bool> checkTile, ITileUpdaterView view)
		{
			Require.That(checkTile != null);
			Require.That(view != null);

			bool corner0 = true;
			bool corner1 = true;
			bool corner2 = true;
			bool corner3 = true;
			bool corner4 = true;
			bool corner5 = true;
			bool corner6 = true;
			bool corner7 = true;

			if (!checkTile(view.Atlas, view.Tiles[Adjacency.NZZ]))
			{
				corner0 = false;
				corner2 = false;
				corner4 = false;
				corner6 = false;
			}

			if (!checkTile(view.Atlas, view.Tiles[Adjacency.PZZ]))
			{
				corner1 = false;
				corner3 = false;
				corner5 = false;
				corner7 = false;
			}

			if (!checkTile(view.Atlas, view.Tiles[Adjacency.ZZN]))
			{
				corner0 = false;
				corner1 = false;
				corner4 = false;
				corner5 = false;
			}

			if (!checkTile(view.Atlas, view.Tiles[Adjacency.ZZP]))
			{
				corner2 = false;
				corner3 = false;
				corner6 = false;
				corner7 = false;
			}

			if (!checkTile(view.Atlas, view.Tiles[Adjacency.ZNZ]))
			{
				corner0 = false;
				corner1 = false;
				corner2 = false;
				corner3 = false;
			}

			if (!checkTile(view.Atlas, view.Tiles[Adjacency.ZPZ]))
			{
				corner4 = false;
				corner5 = false;
				corner6 = false;
				corner7 = false;
			}

			if ((corner0 || corner2) && !checkTile(view.Atlas, view.Tiles[Adjacency.NNZ]))
			{
				corner0 = false;
				corner2 = false;
			}

			if ((corner1 || corner3) && !checkTile(view.Atlas, view.Tiles[Adjacency.PNZ]))
			{
				corner1 = false;
				corner3 = false;
			}

			if ((corner4 || corner6) && !checkTile(view.Atlas, view.Tiles[Adjacency.NPZ]))
			{
				corner4 = false;
				corner6 = false;
			}

			if ((corner5 || corner7) && !checkTile(view.Atlas, view.Tiles[Adjacency.PPZ]))
			{
				corner5 = false;
				corner7 = false;
			}

			if ((corner0 || corner4) && !checkTile(view.Atlas, view.Tiles[Adjacency.NZN]))
			{
				corner0 = false;
				corner4 = false;
			}

			if ((corner1 || corner5) && !checkTile(view.Atlas, view.Tiles[Adjacency.PZN]))
			{
				corner1 = false;
				corner5 = false;
			}

			if ((corner2 || corner6) && !checkTile(view.Atlas, view.Tiles[Adjacency.NZP]))
			{
				corner2 = false;
				corner6 = false;
			}

			if ((corner3 || corner7) && !checkTile(view.Atlas, view.Tiles[Adjacency.PZP]))
			{
				corner3 = false;
				corner7 = false;
			}

			if ((corner0 || corner1) && !checkTile(view.Atlas, view.Tiles[Adjacency.ZNN]))
			{
				corner0 = false;
				corner1 = false;
			}

			if ((corner4 || corner5) && !checkTile(view.Atlas, view.Tiles[Adjacency.ZPN]))
			{
				corner4 = false;
				corner5 = false;
			}

			if ((corner2 || corner3) && !checkTile(view.Atlas, view.Tiles[Adjacency.ZNP]))
			{
				corner2 = false;
				corner3 = false;
			}

			if ((corner6 || corner7) && !checkTile(view.Atlas, view.Tiles[Adjacency.ZPP]))
			{
				corner6 = false;
				corner7 = false;
			}

			if (corner0 && !checkTile(view.Atlas, view.Tiles[Adjacency.NNN]))
			{
				corner0 = false;
			}

			if (corner1 && !checkTile(view.Atlas, view.Tiles[Adjacency.PNN]))
			{
				corner1 = false;
			}

			if (corner2 && !checkTile(view.Atlas, view.Tiles[Adjacency.NNP]))
			{
				corner2 = false;
			}

			if (corner3 && !checkTile(view.Atlas, view.Tiles[Adjacency.PNP]))
			{
				corner3 = false;
			}

			if (corner4 && !checkTile(view.Atlas, view.Tiles[Adjacency.NPN]))
			{
				corner4 = false;
			}

			if (corner5 && !checkTile(view.Atlas, view.Tiles[Adjacency.PPN]))
			{
				corner5 = false;
			}

			if (corner6 && !checkTile(view.Atlas, view.Tiles[Adjacency.NPP]))
			{
				corner6 = false;
			}

			if (corner7 && !checkTile(view.Atlas, view.Tiles[Adjacency.PPP]))
			{
				corner7 = false;
			}

			return new CubicTileBits(corner0, corner1, corner2, corner3, corner4, corner5, corner6, corner7);
		}
	}
}
