﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal class TileFaceMasks : IOriented<ITileFaces<ITileFaceMask>>
	{
		private readonly CubeArrayProjection<ColorVoxel> voxels = new CubeArrayProjection<ColorVoxel>();

		private readonly OrientedTileFaces[] tileFaces = new OrientedTileFaces[Orthogonal.Orientations.Count];

		public TileFaceMasks(ICubeArrayView<ColorVoxel> voxels)
		{
			Require.That(voxels != null);

			this.voxels.Source = voxels;
		}

		/// <inheritdoc />
		public ITileFaces<ITileFaceMask> this[OrthoAxis orientation]
		{
			get
			{
				IOrientedContracts.Indexer(orientation);

				int index = (int)orientation;
				var result = this.tileFaces[index];
				if (result != null)
				{
					return result;
				}

				this.voxels.Orientation = orientation;
				result = new OrientedTileFaces(this.voxels);
				this.tileFaces[index] = result;
				return result;
			}
		}

		public void Clear()
		{
			for (int index = 0; index < this.tileFaces.Length; index++)
			{
				this.tileFaces[index] = null;
			}
		}

		private class OrientedTileFaces : ITileFaces<ITileFaceMask>
		{
			public OrientedTileFaces(ICubeArrayView<ColorVoxel> voxels)
			{
				Require.That(voxels != null);

				this.NegativeX = this.CreateFaceX(voxels, isNegative: true);
				this.PositiveX = this.CreateFaceX(voxels, isNegative: false);
				this.NegativeY = this.CreateFaceY(voxels, isNegative: true);
				this.PositiveY = this.CreateFaceY(voxels, isNegative: false);
				this.NegativeZ = this.CreateFaceZ(voxels, isNegative: true);
				this.PositiveZ = this.CreateFaceZ(voxels, isNegative: false);
			}

			/// <inheritdoc />
			public ITileFaceMask NegativeX { get; }

			/// <inheritdoc />
			public ITileFaceMask PositiveX { get; }

			/// <inheritdoc />
			public ITileFaceMask NegativeY { get; }

			/// <inheritdoc />
			public ITileFaceMask PositiveY { get; }

			/// <inheritdoc />
			public ITileFaceMask NegativeZ { get; }

			/// <inheritdoc />
			public ITileFaceMask PositiveZ { get; }

			private ITileFaceMask CreateFaceX(ICubeArrayView<ColorVoxel> voxels, bool isNegative)
			{
				Require.That(voxels != null);

				var mask = new TileFaceMask(voxels.SizeExponent);
				mask.SetMask(this.EnumerateFaceX(voxels, isNegative));
				return mask;
			}

			private ITileFaceMask CreateFaceY(ICubeArrayView<ColorVoxel> voxels, bool isNegative)
			{
				Require.That(voxels != null);

				var mask = new TileFaceMask(voxels.SizeExponent);
				mask.SetMask(this.EnumerateFaceY(voxels, isNegative));
				return mask;
			}

			private ITileFaceMask CreateFaceZ(ICubeArrayView<ColorVoxel> voxels, bool isNegative)
			{
				Require.That(voxels != null);

				var mask = new TileFaceMask(voxels.SizeExponent);
				mask.SetMask(this.EnumerateFaceZ(voxels, isNegative));
				return mask;
			}

			private IEnumerable<bool> EnumerateFaceX(ICubeArrayView<ColorVoxel> voxels, bool isNegative)
			{
				Require.That(voxels != null);

				int sideMax = voxels.SideLength - 1;
				int x = isNegative ? 0 : sideMax;

				for (int iZ = 0; iZ <= sideMax; iZ++)
				{
					for (int iY = 0; iY <= sideMax; iY++)
					{
						yield return !voxels[x, iY, iZ].IsEmpty;
					}
				}
			}

			private IEnumerable<bool> EnumerateFaceY(ICubeArrayView<ColorVoxel> voxels, bool isNegative)
			{
				Require.That(voxels != null);

				int sideMax = voxels.SideLength - 1;
				int y = isNegative ? 0 : sideMax;

				for (int iZ = 0; iZ <= sideMax; iZ++)
				{
					for (int iX = 0; iX <= sideMax; iX++)
					{
						yield return !voxels[iX, y, iZ].IsEmpty;
					}
				}
			}

			private IEnumerable<bool> EnumerateFaceZ(ICubeArrayView<ColorVoxel> voxels, bool isNegative)
			{
				Require.That(voxels != null);

				int sideMax = voxels.SideLength - 1;
				int z = isNegative ? 0 : sideMax;

				for (int iY = 0; iY <= sideMax; iY++)
				{
					for (int iX = 0; iX <= sideMax; iX++)
					{
						yield return !voxels[iX, iY, z].IsEmpty;
					}
				}
			}
		}
	}
}
