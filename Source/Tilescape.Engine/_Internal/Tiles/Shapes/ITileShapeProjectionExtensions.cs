﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal static class ITileShapeProjectionExtensions
	{
		public static bool IsVisibleNegativeX(this ITileShapeProjection shape, ITileFaces<ITileFaceMask> other)
		{
			IsEmptyOrCoveredByContracts(shape, other);

			return shape.Meshes.NegativeX.Count > 0 && !shape.FaceMasks.IsNegativeXCoveredBy(other);
		}

		public static bool IsVisiblePositiveX(this ITileShapeProjection shape, ITileFaces<ITileFaceMask> other)
		{
			IsEmptyOrCoveredByContracts(shape, other);

			return shape.Meshes.PositiveX.Count > 0 && !shape.FaceMasks.IsPositiveXCoveredBy(other);
		}

		public static bool IsVisibleNegativeY(this ITileShapeProjection shape, ITileFaces<ITileFaceMask> other)
		{
			IsEmptyOrCoveredByContracts(shape, other);

			return shape.Meshes.NegativeY.Count > 0 && !shape.FaceMasks.IsNegativeYCoveredBy(other);
		}

		public static bool IsVisiblePositiveY(this ITileShapeProjection shape, ITileFaces<ITileFaceMask> other)
		{
			IsEmptyOrCoveredByContracts(shape, other);

			return shape.Meshes.PositiveY.Count > 0 && !shape.FaceMasks.IsPositiveYCoveredBy(other);
		}

		public static bool IsVisibleNegativeZ(this ITileShapeProjection shape, ITileFaces<ITileFaceMask> other)
		{
			IsEmptyOrCoveredByContracts(shape, other);

			return shape.Meshes.NegativeZ.Count > 0 && !shape.FaceMasks.IsNegativeZCoveredBy(other);
		}

		public static bool IsVisiblePositiveZ(this ITileShapeProjection shape, ITileFaces<ITileFaceMask> other)
		{
			IsEmptyOrCoveredByContracts(shape, other);

			return shape.Meshes.PositiveZ.Count > 0 && !shape.FaceMasks.IsPositiveZCoveredBy(other);
		}

		[Conditional(Require.CompilationSymbol)]
		private static void IsEmptyOrCoveredByContracts(ITileShapeProjection shape, ITileFaces<ITileFaceMask> other)
		{
			Require.That(shape != null);
			Require.That(shape.IsAssigned);
			Require.That(other != null);
		}
	}
}
