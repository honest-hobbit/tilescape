﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;

namespace Tilescape.Engine
{
	internal static class TileFacesProjection
	{
		private static readonly ITileFacesProjection[] Instances = OrientationArray.Create<ITileFacesProjection>(
			x => x.Orientation,
			new ProjectionNegX000Norm_00(),
			new ProjectionNegX000Flip_01(),
			new ProjectionNegX090Norm_02(),
			new ProjectionNegX090Flip_03(),
			new ProjectionNegX180Norm_04(),
			new ProjectionNegX180Flip_05(),
			new ProjectionNegX270Norm_06(),
			new ProjectionNegX270Flip_07(),
			new ProjectionPosX000Norm_08(),
			new ProjectionPosX000Flip_09(),
			new ProjectionPosX090Norm_10(),
			new ProjectionPosX090Flip_11(),
			new ProjectionPosX180Norm_12(),
			new ProjectionPosX180Flip_13(),
			new ProjectionPosX270Norm_14(),
			new ProjectionPosX270Flip_15(),
			new ProjectionNegY000Norm_16(),
			new ProjectionNegY000Flip_17(),
			new ProjectionNegY090Norm_18(),
			new ProjectionNegY090Flip_19(),
			new ProjectionNegY180Norm_20(),
			new ProjectionNegY180Flip_21(),
			new ProjectionNegY270Norm_22(),
			new ProjectionNegY270Flip_23(),
			new ProjectionPosY000Norm_24(),
			new ProjectionPosY000Flip_25(),
			new ProjectionPosY090Norm_26(),
			new ProjectionPosY090Flip_27(),
			new ProjectionPosY180Norm_28(),
			new ProjectionPosY180Flip_29(),
			new ProjectionPosY270Norm_30(),
			new ProjectionPosY270Flip_31(),
			new ProjectionNegZ000Norm_32(),
			new ProjectionNegZ000Flip_33(),
			new ProjectionNegZ090Norm_34(),
			new ProjectionNegZ090Flip_35(),
			new ProjectionNegZ180Norm_36(),
			new ProjectionNegZ180Flip_37(),
			new ProjectionNegZ270Norm_38(),
			new ProjectionNegZ270Flip_39(),
			new ProjectionPosZ000Norm_40(),
			new ProjectionPosZ000Flip_41(),
			new ProjectionPosZ090Norm_42(),
			new ProjectionPosZ090Flip_43(),
			new ProjectionPosZ180Norm_44(),
			new ProjectionPosZ180Flip_45(),
			new ProjectionPosZ270Norm_46(),
			new ProjectionPosZ270Flip_47());

		public static ITileFacesProjection Neutral => Get(OrthoAxis.PosY000Norm_24);

		public static ITileFacesProjection Get(OrthoAxis orientation)
		{
			Require.That(Enumeration.IsDefined(orientation));

			return Instances[(int)orientation];
		}

		#region Private Classes

		private abstract class AbstractProjection
		{
			public abstract OrthoAxis Orientation { get; }

			/// <inheritdoc />
			public override string ToString() => this.Orientation.ToString();
		}

		private class ProjectionNegX090Norm_02 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegX090Norm_02;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveZ;
		}

		// flip z
		private class ProjectionNegX270Flip_07 : ProjectionNegX090Norm_02
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegX270Flip_07;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionNegX180Norm_04 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegX180Norm_04;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeY;
		}

		// flip x
		private class ProjectionNegX180Flip_05 : ProjectionNegX180Norm_04
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegX180Flip_05;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionNegX270Norm_06 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegX270Norm_06;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeZ;
		}

		// flip z
		private class ProjectionNegX090Flip_03 : ProjectionNegX270Norm_06
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegX090Flip_03;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionNegX000Norm_00 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegX000Norm_00;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveY;
		}

		// flip x
		private class ProjectionNegX000Flip_01 : ProjectionNegX000Norm_00
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegX000Flip_01;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionPosX270Norm_14 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosX270Norm_14;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveZ;
		}

		// flip z
		private class ProjectionPosX090Flip_11 : ProjectionPosX270Norm_14
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosX090Flip_11;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionPosX000Norm_08 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosX000Norm_08;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveY;
		}

		// flip x
		private class ProjectionPosX000Flip_09 : ProjectionPosX000Norm_08
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosX000Flip_09;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionPosX090Norm_10 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosX090Norm_10;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeZ;
		}

		// flip z
		private class ProjectionPosX270Flip_15 : ProjectionPosX090Norm_10
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosX270Flip_15;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionPosX180Norm_12 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosX180Norm_12;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeY;
		}

		// flip x
		private class ProjectionPosX180Flip_13 : ProjectionPosX180Norm_12
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosX180Flip_13;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionNegY000Norm_16 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegY000Norm_16;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeZ;
		}

		// flip z
		private class ProjectionNegY180Flip_21 : ProjectionNegY000Norm_16
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegY180Flip_21;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionNegY090Norm_18 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegY090Norm_18;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeX;
		}

		// flip x
		private class ProjectionNegY090Flip_19 : ProjectionNegY090Norm_18
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegY090Flip_19;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionNegY180Norm_20 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegY180Norm_20;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveZ;
		}

		// flip z
		private class ProjectionNegY000Flip_17 : ProjectionNegY180Norm_20
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegY000Flip_17;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionNegY270Norm_22 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegY270Norm_22;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveX;
		}

		// flip x
		private class ProjectionNegY270Flip_23 : ProjectionNegY270Norm_22
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegY270Flip_23;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionPosY000Norm_24 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosY000Norm_24;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveZ;
		}

		// flip z
		private class ProjectionPosY180Flip_29 : ProjectionPosY000Norm_24
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosY180Flip_29;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionPosY090Norm_26 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosY090Norm_26;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeX;
		}

		// flip x
		private class ProjectionPosY090Flip_27 : ProjectionPosY090Norm_26
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosY090Flip_27;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionPosY180Norm_28 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosY180Norm_28;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeZ;
		}

		// flip z
		private class ProjectionPosY000Flip_25 : ProjectionPosY180Norm_28
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosY000Flip_25;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionPosY270Norm_30 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosY270Norm_30;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveX;
		}

		// flip x
		private class ProjectionPosY270Flip_31 : ProjectionPosY270Norm_30
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosY270Flip_31;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionNegZ000Norm_32 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegZ000Norm_32;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveY;
		}

		// flip x
		private class ProjectionNegZ000Flip_33 : ProjectionNegZ000Norm_32
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegZ000Flip_33;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionNegZ090Norm_34 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegZ090Norm_34;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeX;
		}

		// flip z
		private class ProjectionNegZ270Flip_39 : ProjectionNegZ090Norm_34
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegZ270Flip_39;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionNegZ180Norm_36 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegZ180Norm_36;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeY;
		}

		// flip x
		private class ProjectionNegZ180Flip_37 : ProjectionNegZ180Norm_36
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegZ180Flip_37;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionNegZ270Norm_38 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegZ270Norm_38;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveX;
		}

		// flip z
		private class ProjectionNegZ090Flip_35 : ProjectionNegZ270Norm_38
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.NegZ090Flip_35;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionPosZ180Norm_44 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosZ180Norm_44;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeY;
		}

		// flip x
		private class ProjectionPosZ180Flip_45 : ProjectionPosZ180Norm_44
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosZ180Flip_45;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionPosZ270Norm_46 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosZ270Norm_46;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.NegativeX;
		}

		// flip z
		private class ProjectionPosZ090Flip_43 : ProjectionPosZ270Norm_46
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosZ090Flip_43;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		private class ProjectionPosZ000Norm_40 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosZ000Norm_40;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.PositiveX;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveY;
		}

		// flip x
		private class ProjectionPosZ000Flip_41 : ProjectionPosZ000Norm_40
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosZ000Flip_41;

			/// <inheritdoc />
			public override T GetNegativeX<T>(ITileFaces<T> faces) => base.GetPositiveX(faces);

			/// <inheritdoc />
			public override T GetPositiveX<T>(ITileFaces<T> faces) => base.GetNegativeX(faces);
		}

		private class ProjectionPosZ090Norm_42 : AbstractProjection, ITileFacesProjection
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosZ090Norm_42;

			/// <inheritdoc />
			public virtual T GetNegativeX<T>(ITileFaces<T> faces) => faces.NegativeY;

			/// <inheritdoc />
			public virtual T GetPositiveX<T>(ITileFaces<T> faces) => faces.PositiveY;

			/// <inheritdoc />
			public virtual T GetNegativeY<T>(ITileFaces<T> faces) => faces.NegativeZ;

			/// <inheritdoc />
			public virtual T GetPositiveY<T>(ITileFaces<T> faces) => faces.PositiveZ;

			/// <inheritdoc />
			public virtual T GetNegativeZ<T>(ITileFaces<T> faces) => faces.NegativeX;

			/// <inheritdoc />
			public virtual T GetPositiveZ<T>(ITileFaces<T> faces) => faces.PositiveX;
		}

		// flip z
		private class ProjectionPosZ270Flip_47 : ProjectionPosZ090Norm_42
		{
			/// <inheritdoc />
			public override OrthoAxis Orientation => OrthoAxis.PosZ270Flip_47;

			/// <inheritdoc />
			public override T GetNegativeZ<T>(ITileFaces<T> faces) => base.GetPositiveZ(faces);

			/// <inheritdoc />
			public override T GetPositiveZ<T>(ITileFaces<T> faces) => base.GetNegativeZ(faces);
		}

		#endregion
	}
}
