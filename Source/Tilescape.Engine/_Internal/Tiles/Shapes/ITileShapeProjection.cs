﻿using Tilescape.Utility.Collections;

namespace Tilescape.Engine
{
	internal interface ITileShapeProjection
	{
		bool IsAssigned { get; }

		int SizeExponent { get; }

		ITileColliders Colliders { get; }

		ITileParts<IListView<MeshQuad>> Meshes { get; }

		ITileFaces<ITileFaceMask> FaceMasks { get; }
	}
}
