﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal class TileMeshes : ITileParts<IListView<MeshQuadSlim>>
	{
		/// <inheritdoc />
		public IListView<MeshQuadSlim> Center { get; set; }

		/// <inheritdoc />
		public IListView<MeshQuadSlim> NegativeX { get; set; }

		/// <inheritdoc />
		public IListView<MeshQuadSlim> PositiveX { get; set; }

		/// <inheritdoc />
		public IListView<MeshQuadSlim> NegativeY { get; set; }

		/// <inheritdoc />
		public IListView<MeshQuadSlim> PositiveY { get; set; }

		/// <inheritdoc />
		public IListView<MeshQuadSlim> NegativeZ { get; set; }

		/// <inheritdoc />
		public IListView<MeshQuadSlim> PositiveZ { get; set; }

		// this does not perform a deep clone!
		public void SetTo(ITileParts<IListView<MeshQuadSlim>> parts)
		{
			Require.That(parts != null);

			this.Center = parts.Center;
			this.NegativeX = parts.NegativeX;
			this.PositiveX = parts.PositiveX;
			this.NegativeY = parts.NegativeY;
			this.PositiveY = parts.PositiveY;
			this.NegativeZ = parts.NegativeZ;
			this.PositiveZ = parts.PositiveZ;
		}

		public void Clear()
		{
			this.Center = null;
			this.NegativeX = null;
			this.PositiveX = null;
			this.NegativeY = null;
			this.PositiveY = null;
			this.NegativeZ = null;
			this.PositiveZ = null;
		}
	}
}
