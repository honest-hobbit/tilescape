﻿using System.Collections;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class TileFaceMask : ITileFaceMask
	{
		private readonly ulong[] mask;

		public TileFaceMask(int sizeExponent)
		{
			Require.That(sizeExponent >= 0);

			this.mask = new ulong[GetMaskLength(sizeExponent)];
		}

		/// <inheritdoc />
		public int Count => this.mask.Length;

		/// <inheritdoc />
		public ulong this[int index] => this.mask[index];

		public static int GetMaskLength(int sizeExponent)
		{
			Require.That(sizeExponent >= 0);

			return MathUtility.DivideRoundUp(BinaryArray.GetSquareLength(sizeExponent), Length.OfULong.InBits);
		}

		public void SetMask(IEnumerable<bool> voxels)
		{
			Require.That(voxels != null);
			Require.That(voxels.CountExtended() <= this.mask.Length * Length.OfULong.InBits);

			for (int clearIndex = 0; clearIndex < this.mask.Length; clearIndex++)
			{
				this.mask[clearIndex] = 0;
			}

			int index = 0;
			int count = 0;
			ulong mask = 0;
			foreach (var isSolid in voxels)
			{
				if (count == Length.OfULong.InBits)
				{
					this.mask[index] = mask;
					mask = 0;
					index++;
					count = 0;
				}

				mask = mask << 1;
				if (isSolid)
				{
					mask |= 1;
				}

				count++;
			}

			this.mask[index] = mask;
		}

		/// <inheritdoc />
		public IEnumerator<ulong> GetEnumerator() => this.mask.GetEnumerator<ulong>();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
