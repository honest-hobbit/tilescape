﻿using System.Collections;
using System.Collections.Generic;

namespace Tilescape.Engine
{
	internal class TileColliders : ITileColliders
	{
		public List<BoxColliderStruct> Boxes { get; } = new List<BoxColliderStruct>();

		/// <inheritdoc />
		public bool IsFullCube { get; set; } = false;

		/// <inheritdoc />
		public int Count => this.Boxes.Count;

		/// <inheritdoc />
		public BoxColliderStruct this[int index] => this.Boxes[index];

		/// <inheritdoc />
		public IEnumerator<BoxColliderStruct> GetEnumerator() => this.Boxes.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		public void Clear()
		{
			this.Boxes.Clear();
			this.IsFullCube = false;
		}
	}
}
