﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal class TileShapeBuilder
	{
		private readonly TileColliderBuilder colliderBuilder;

		private readonly TileShapeContourer contourer;

		private int colliderFillThreshold = 0;

		public TileShapeBuilder(ITileConfig config)
		{
			Require.That(config != null);

			this.colliderBuilder = new TileColliderBuilder(config);
			this.contourer = new TileShapeContourer(config);
		}

		public int TileSizeExponent => this.contourer.TileSizeExponent;

		public MeshingMode MeshingMode { get; set; } = MeshingMode.Greedy;

		public int ColliderFillThreshold
		{
			get => this.colliderFillThreshold;
			set
			{
				Require.That(value >= 0);

				this.colliderFillThreshold = value;
			}
		}

		public void BuildShape(TileShape shape)
		{
			Require.That(shape != null);
			Require.That(shape.Voxels.SizeExponent == this.TileSizeExponent);

			shape.FaceMasks.Clear();
			this.colliderBuilder.BuildColliders(shape, this.ColliderFillThreshold);
			this.contourer.Generate(shape, this.MeshingMode);
		}
	}
}
