﻿namespace Tilescape.Engine
{
	internal interface ITileFacesProjection
	{
		OrthoAxis Orientation { get; }

		T GetNegativeX<T>(ITileFaces<T> faces);

		T GetPositiveX<T>(ITileFaces<T> faces);

		T GetNegativeY<T>(ITileFaces<T> faces);

		T GetPositiveY<T>(ITileFaces<T> faces);

		T GetNegativeZ<T>(ITileFaces<T> faces);

		T GetPositiveZ<T>(ITileFaces<T> faces);
	}
}
