﻿namespace Tilescape.Engine
{
	internal class TileFacesProjection<T> : ITileFaces<T>
	{
		private ITileFacesProjection projection = TileFacesProjection.Neutral;

		/// <inheritdoc />
		public T NegativeX => this.projection.GetNegativeX(this.Source);

		/// <inheritdoc />
		public T PositiveX => this.projection.GetPositiveX(this.Source);

		/// <inheritdoc />
		public T NegativeY => this.projection.GetNegativeY(this.Source);

		/// <inheritdoc />
		public T PositiveY => this.projection.GetPositiveY(this.Source);

		/// <inheritdoc />
		public T NegativeZ => this.projection.GetNegativeZ(this.Source);

		/// <inheritdoc />
		public T PositiveZ => this.projection.GetPositiveZ(this.Source);

		public ITileFaces<T> Source { get; set; }

		public OrthoAxis Orientation
		{
			get => this.projection.Orientation;
			set => this.projection = TileFacesProjection.Get(value);
		}

		public void SetProjection(ITileFaces<T> source, OrthoAxis orientation)
		{
			this.Source = source;
			this.Orientation = orientation;
		}

		public void Clear()
		{
			this.Source = null;
			this.projection = TileFacesProjection.Neutral;
		}
	}
}
