﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Engine
{
	internal class TileColliderBuilder
	{
		private readonly ColliderReducer reducer;

		private readonly ColliderBuilder builder;

		public TileColliderBuilder(ITileConfig config)
		{
			Require.That(config != null);

			this.TileSizeExponent = config.TileSizeExponent;
			this.reducer = new ColliderReducer(config.TileSizeExponent);
			this.builder = new ColliderBuilder(config.TileSizeExponent, scale: config.VoxelLength, offset: -.5f);
		}

		public int TileSizeExponent { get; }

		public void BuildColliders(TileShape shape, int threshold)
		{
			Require.That(shape != null);
			Require.That(shape.Voxels.SizeExponent == this.TileSizeExponent);
			Require.That(threshold >= 0);

			shape.Colliders.Boxes.Clear();
			if (this.IsFullCube(shape))
			{
				shape.Colliders.IsFullCube = true;
			}
			else
			{
				this.reducer.SetTo(shape.Voxels, threshold);
				if (this.reducer.IsFullCube())
				{
					shape.Colliders.IsFullCube = true;
				}
				else
				{
					shape.Colliders.IsFullCube = false;
					this.builder.BuildColliders(this.reducer.IsSolid, shape.Colliders.Boxes);
				}
			}
		}

		private bool IsFullCube(TileShape tile)
		{
			Require.That(tile != null);

			for (int index = 0; index < tile.Voxels.Length; index++)
			{
				if (tile.Voxels[index].IsEmpty)
				{
					return false;
				}
			}

			return true;
		}
	}
}
