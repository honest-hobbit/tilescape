﻿namespace Tilescape.Engine
{
	internal interface IFlagsForTileChunkModifiedQueue
	{
		bool IsModified { get; set; }

		bool IsUnitySynchronized { get; set; }
	}
}
