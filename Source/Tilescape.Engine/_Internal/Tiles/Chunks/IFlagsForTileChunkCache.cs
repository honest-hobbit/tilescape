﻿namespace Tilescape.Engine
{
	internal interface IFlagsForTileChunkCache
	{
		bool IsActiveInUnity { get; set; }

		bool IsUnitySynchronized { get; set; }

		bool IsToBeUnpinned { get; set; }
	}
}
