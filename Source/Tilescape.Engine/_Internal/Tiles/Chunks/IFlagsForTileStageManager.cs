﻿namespace Tilescape.Engine
{
	internal interface IFlagsForTileStageManager
	{
		bool IsInitialized { get; set; }
	}
}
