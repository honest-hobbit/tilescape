﻿namespace Tilescape.Engine
{
	internal static class TileChunkFlagExtensions
	{
		/// <summary>
		/// Determines whether the current flags value contains the specified flag or flags (all of them).
		/// This implementation is faster than the Enum.HasFlag method because it avoids boxing of values.
		/// </summary>
		/// <param name="current">The current value to check for containing flags.</param>
		/// <param name="flag">The flag or combined flags to check for.</param>
		/// <returns>True if the current value contains the flag(s); otherwise false.</returns>
		public static bool Has(this TileChunkFlag current, TileChunkFlag flag) => (current & flag) == flag;

		/// <summary>
		/// Determines whether the current flags value contains any of the specified flags.
		/// </summary>
		/// <param name="current">The current value to check for containing flags.</param>
		/// <param name="flags">The flags to check for.</param>
		/// <returns>True if the current value contains any of the flags; otherwise false.</returns>
		public static bool HasAny(this TileChunkFlag current, TileChunkFlag flags) => (current & flags) != 0;

		public static TileChunkFlag Add(this TileChunkFlag current, TileChunkFlag flags) => current | flags;

		public static TileChunkFlag Remove(this TileChunkFlag current, TileChunkFlag flags) => current & ~flags;

		public static TileChunkFlag Set(this TileChunkFlag current, TileChunkFlag flags, bool value) =>
			value ? current.Add(flags) : current.Remove(flags);
	}
}
