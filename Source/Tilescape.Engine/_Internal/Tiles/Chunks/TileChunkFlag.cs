﻿using System;

namespace Tilescape.Engine
{
	// might need to add flag for whether FallbackPopulator was used
	// might need to add flag for whether Modified tile tracking is on
	// both of these are to support properly updating the tile mapping count tracking
	[Flags]
	internal enum TileChunkFlag
	{
		None = 0,

		// only set by TileStageManager.ProcessChunksPopulated once the chunk is populated
		IsInitialized = 1,

		// only set by TileChunkModifiedQueue when chunk is enqueued,
		// used to not process the same chunk multiple times in a single frame
		IsModified = 1 << 1,

		// only set by TileChunkCache, indicates if chunk should appear inside Unity
		IsActiveInUnity = 1 << 2,

		// only set by ResyncUnityQueue when chunk is enqueued, used to not resync same chunk multiple times in a single frame
		IsResyncingUnity = 1 << 3,

		// set to true by ResyncUnityQueue once the updated chunk data has been sent to the unity dispatcher
		// set to false by TileChunkCache.DeactivateChunk
		// set to false by TileChunkModifiedQueue when chunk is enqueued
		IsUnitySynchronized = 1 << 4,

		// only set by TileChunkCache, indicates chunk should be unpinned at the end of the frame
		IsToBeUnpinned = 1 << 5
	}
}
