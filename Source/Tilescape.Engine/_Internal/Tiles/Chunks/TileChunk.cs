﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Concurrency;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Curves;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;

namespace Tilescape.Engine
{
	internal class TileChunk :
		ITileChunk,
		IFlagsForTileStageManager,
		IFlagsForTileChunkModifiedQueue,
		IFlagsForTileChunkCache,
		IFlagsForResyncUnityQueue
	{
		private readonly Signal isPopulated = new Signal();

		private TileChunkFlag statusFlags;

		public TileChunk(int sizeExponent)
		{
			Require.That(sizeExponent >= 0);

			this.SizeExponent = sizeExponent;
		}

		/// <inheritdoc />
		public TileChunkKey Key { get; set; }

		/// <inheritdoc />
		public int SizeExponent { get; }

		/// <inheritdoc />
		public int SideLength => BinaryArray.GetSideLength(this.SizeExponent);

		public TileChunkFlag Status => this.statusFlags;

		#region Bool Status Flag Properties

		public bool IsInitialized
		{
			get { return this.statusFlags.Has(TileChunkFlag.IsInitialized); }
			private set { this.statusFlags = this.statusFlags.Set(TileChunkFlag.IsInitialized, value); }
		}

		public bool IsModified
		{
			get { return this.statusFlags.Has(TileChunkFlag.IsModified); }
			private set { this.statusFlags = this.statusFlags.Set(TileChunkFlag.IsModified, value); }
		}

		public bool IsActiveInUnity
		{
			get { return this.statusFlags.Has(TileChunkFlag.IsActiveInUnity); }
			private set { this.statusFlags = this.statusFlags.Set(TileChunkFlag.IsActiveInUnity, value); }
		}

		public bool IsResyncingUnity
		{
			get { return this.statusFlags.Has(TileChunkFlag.IsResyncingUnity); }
			private set { this.statusFlags = this.statusFlags.Set(TileChunkFlag.IsResyncingUnity, value); }
		}

		public bool IsUnitySynchronized
		{
			get { return this.statusFlags.Has(TileChunkFlag.IsUnitySynchronized); }
			private set { this.statusFlags = this.statusFlags.Set(TileChunkFlag.IsUnitySynchronized, value); }
		}

		public bool IsToBeUnpinned
		{
			get { return this.statusFlags.Has(TileChunkFlag.IsToBeUnpinned); }
			private set { this.statusFlags = this.statusFlags.Set(TileChunkFlag.IsToBeUnpinned, value); }
		}

		/// <inheritdoc />
		bool IFlagsForTileStageManager.IsInitialized
		{
			get => this.IsInitialized;
			set => this.IsInitialized = value;
		}

		/// <inheritdoc />
		bool IFlagsForTileChunkModifiedQueue.IsModified
		{
			get => this.IsModified;
			set => this.IsModified = value;
		}

		/// <inheritdoc />
		bool IFlagsForTileChunkModifiedQueue.IsUnitySynchronized
		{
			get => this.IsUnitySynchronized;
			set => this.IsUnitySynchronized = value;
		}

		/// <inheritdoc />
		bool IFlagsForTileChunkCache.IsActiveInUnity
		{
			get => this.IsActiveInUnity;
			set => this.IsActiveInUnity = value;
		}

		/// <inheritdoc />
		bool IFlagsForTileChunkCache.IsUnitySynchronized
		{
			get => this.IsUnitySynchronized;
			set => this.IsUnitySynchronized = value;
		}

		/// <inheritdoc />
		bool IFlagsForTileChunkCache.IsToBeUnpinned
		{
			get => this.IsToBeUnpinned;
			set => this.IsToBeUnpinned = value;
		}

		/// <inheritdoc />
		bool IFlagsForResyncUnityQueue.IsResyncingUnity
		{
			get => this.IsResyncingUnity;
			set => this.IsResyncingUnity = value;
		}

		/// <inheritdoc />
		bool IFlagsForResyncUnityQueue.IsUnitySynchronized
		{
			get => this.IsUnitySynchronized;
			set => this.IsUnitySynchronized = value;
		}

		#endregion

		/// <inheritdoc />
		public bool IsUniform => this.UniformTile.HasValue;

		/// <inheritdoc />
		public Try<Tile> UniformTile { get; set; }

		/// <inheritdoc />
		public CubeArray<Tile> Tiles { get; set; }

		/// <inheritdoc />
		ICubeArrayView<Tile> ITileChunkView.Tiles => this.Tiles;

		public TileChunkEntity Entity { get; set; }

		public Dictionary<Index3D, Tile> ModifiedTiles { get; } = new Dictionary<Index3D, Tile>(MortonCurve.Comparer<Index3D>());

		// only called on background store thread
		public void SetPopulated() => this.isPopulated.Set();

		// only called on main tile thread
		public void WaitUntilPopulated()
		{
			if (this.IsInitialized)
			{
				return;
			}

			this.isPopulated.Wait();
			this.IsInitialized = true;
		}

		// only called on main tile thread
		public void Reset(CubeArray<Tile> tiles)
		{
			Require.That(tiles != null);

			this.Key = new TileChunkKey(Guid.Empty, Index3D.Zero);
			this.Tiles = tiles;
			this.UniformTile = Try.None<Tile>();
			this.ModifiedTiles.Clear();
			this.Entity = null;
			this.statusFlags = TileChunkFlag.None;
			this.isPopulated.Reset();
		}
	}
}
