﻿namespace Tilescape.Engine
{
	internal interface IFlagsForResyncUnityQueue
	{
		bool IsResyncingUnity { get; set; }

		bool IsUnitySynchronized { get; set; }
	}
}
