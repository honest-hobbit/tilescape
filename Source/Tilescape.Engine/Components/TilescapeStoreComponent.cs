﻿using System;
using System.IO;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Editor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[ExecuteInEditMode]
	public class TilescapeStoreComponent : MonoBehaviour
	{
		private static readonly string DefaultDatabasePath = "Tilescape.db";

		private ITilescapeStore store;

		[SerializeField]
		private string databasePath = DefaultDatabasePath;

		[SerializeField]
		private int tileSizeExponent = 4;

		[SerializeField]
		private int tileChunkSizeExponent = 3;

		[SerializeField]
		private bool disposeStoreOnDestroy = true;

		[SerializeField]
		[ReadOnlyField]
		private int tileLengthInVoxels = 0;

		[SerializeField]
		[ReadOnlyField]
		private int tileChunkLengthInTiles = 0;

		public TilescapeStoreComponent()
		{
			this.OnValidate();
		}

		public string FullDatabasePath => Path.GetFullPath(Path.Combine(Application.streamingAssetsPath, this.databasePath));

		public string DatabasePath
		{
			get => this.databasePath;
			set
			{
				Require.That(!value.IsNullOrWhiteSpace());
				this.databasePath = value;
			}
		}

		public int TileSizeExponent
		{
			get => this.tileSizeExponent;
			set
			{
				Require.That(value >= 0);
				this.tileSizeExponent = value;
			}
		}

		public int TileChunkSizeExponent
		{
			get => this.tileChunkSizeExponent;
			set
			{
				Require.That(value >= 0);
				this.tileChunkSizeExponent = value;
			}
		}

		public bool DisposeStoreOnDestroy
		{
			get => this.disposeStoreOnDestroy;
			set => this.disposeStoreOnDestroy = value;
		}

		internal ITilescapeStore Store
		{
			get
			{
				if (this.store == null)
				{
					if (this.databasePath.IsNullOrWhiteSpace())
					{
						throw new ArgumentException(
							"Must be a valid path including file name and extension.", nameof(this.databasePath));
					}

					this.store = TilescapeStore.Create(this.FullDatabasePath, this.GetOrCreateConfig());
					this.RefreshInspector();
				}

				return this.store;
			}
		}

		private void OnValidate()
		{
			if (this.databasePath.IsNullOrWhiteSpace())
			{
				this.databasePath = DefaultDatabasePath;
			}

			if (this.store == null)
			{
				if (this.tileSizeExponent < 0)
				{
					this.tileSizeExponent = 0;
				}

				if (this.tileChunkSizeExponent < 0)
				{
					this.tileChunkSizeExponent = 0;
				}
			}
			else
			{
				this.tileSizeExponent = this.store.Config.Stages.TileSizeExponent;
				this.tileChunkSizeExponent = this.store.Config.Stages.ChunkSizeExponent;
			}

			this.RefreshInspector();
		}

		private void RefreshInspector()
		{
			var config = this.GetOrCreateConfig();
			this.tileLengthInVoxels = config.TileLengthInVoxels;
			this.tileChunkLengthInTiles = config.ChunkLengthInTiles;
		}

		private ITileStageConfig GetOrCreateConfig() =>
			this.store?.Config.Stages ?? new TileStageConfig(this.tileSizeExponent, this.tileChunkSizeExponent);

		private void OnDestroy()
		{
			if (this.disposeStoreOnDestroy && this.store != null)
			{
				this.store.Dispose();
				this.store = null;
			}
		}
	}
}
