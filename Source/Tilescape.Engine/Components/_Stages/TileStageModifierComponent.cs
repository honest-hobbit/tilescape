﻿using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Unity.Components;
using Tilescape.Utility.Unity.Types;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[RequireComponent(typeof(Camera))]
	public class TileStageModifierComponent : MonoBehaviour
	{
		private readonly ReadTileAdjacencyTask readTask = new ReadTileAdjacencyTask();

		private LayerMask layerMask;

		[SerializeField]
		private Material tileMaterial = null;

		private new Camera camera;

		private float tileScale;

		private TileShapeComponent shapePreview;

		private bool isDeleting = false;

		private ITilescapeAtlas atlas;

		private ListIterator<ITileTypeOLD> tileTypes;

		private Direction direction = Direction.PositiveY;

		private Rotation rotation = Rotation.Clockwise0;

		private bool isFlipped = false;

		private bool isWaitingForReadToComplete = false;

		public ITileTypeOLD EmptyTileType { get; set; }

		private TileTypeKey EmptyTileKey => this.EmptyTileType.Key;

		private void Awake()
		{
			this.camera = this.GetComponent<Camera>();
			this.layerMask = LayerMask.NameToLayer("Everything") & ~LayerMask.NameToLayer("Overlay Default");
		}

		private void Start()
		{
			Require.That(this.EmptyTileType != null);

			var manager = Singleton.Of<TileStageManagerComponent>();
			this.tileScale = manager.TileScale;
			this.atlas = manager.Atlas;

			this.tileTypes = new ListIterator<ITileTypeOLD>(
				this.atlas.Types.Select(x => x.Value).Where(x => x.Key != this.EmptyTileType.Key).OrderBy(x => x.Name)
				.ToArray().AsListView());

			this.readTask.Atlas = this.atlas;
			this.readTask.Completed += this.TilesRead;

			var shapePreviewObject = new GameObject("Tile Shape Preview")
			{
				layer = LayerMask.NameToLayer("Overlay Default")
			};
			shapePreviewObject.transform.localScale = new Vector3(this.tileScale, this.tileScale, this.tileScale);
			this.shapePreview = shapePreviewObject.AddComponent<TileShapeComponent>();
			this.shapePreview.TileMaterial = this.tileMaterial;
		}

		private void Update()
		{
			if (Input.GetMouseButtonDown(1))
			{
				// right clicked
				this.isDeleting = !this.isDeleting;
				if (this.isDeleting)
				{
					this.shapePreview.gameObject.SetActive(false);
				}
			}

			if (Input.GetKeyDown(KeyCode.Alpha1))
			{
				if (this.tileTypes.TryMovePrevious())
				{
					this.RefreshTileShapePreview();
				}
			}

			if (Input.GetKeyDown(KeyCode.Alpha2))
			{
				if (this.tileTypes.TryMoveNext())
				{
					this.RefreshTileShapePreview();
				}
			}

			if (Input.GetKeyDown(KeyCode.Alpha3))
			{
				this.direction = this.direction.Next();
				this.RefreshTileShapePreview();
			}

			if (Input.GetKeyDown(KeyCode.Alpha4))
			{
				this.rotation = this.rotation.Next();
				this.RefreshTileShapePreview();
			}

			if (Input.GetKeyDown(KeyCode.Alpha5))
			{
				this.isFlipped = !this.isFlipped;
				this.RefreshTileShapePreview();
			}

			// left click
			bool leftClicked = Input.GetMouseButtonDown(0);
			if (leftClicked || !this.isWaitingForReadToComplete)
			{
				if (this.TryRaycastTiles(out var hit, out var chunk))
				{
					if (leftClicked)
					{
						var tileIndex = Index3D.Zero;
						var tile = Tile.Unassigned;
						if (this.isDeleting)
						{
							tileIndex = chunk.GetTileIndex(hit.point, hit.normal);
							tile = new Tile(this.EmptyTileKey);
						}
						else
						{
							tileIndex = chunk.GetTileAdjacentIndex(hit.point, hit.normal);
							tile = this.GetTile();
						}

						chunk.Manager.Submit(new ReplaceSingleTileTask(chunk.Stage.Key, tileIndex, tile));
					}

					if (!this.isDeleting && !this.isWaitingForReadToComplete)
					{
						var newTileIndex = chunk.GetTileAdjacentIndex(hit.point, hit.normal);

						if (newTileIndex != this.readTask.TileIndex)
						{
							this.isWaitingForReadToComplete = true;
							this.readTask.StageKey = chunk.Stage.Key;
							this.readTask.TileIndex = newTileIndex;
							chunk.Manager.Submit(this.readTask);
						}
						else
						{
							this.shapePreview.gameObject.SetActive(true);
						}
					}
				}
				else
				{
					if (!this.isDeleting)
					{
						this.shapePreview.gameObject.SetActive(false);
					}
				}
			}
		}

		private bool TryRaycastTiles(out RaycastHit hit, out TileStageChunkComponent chunk)
		{
			var ray = new Ray(this.camera.transform.position, this.camera.transform.forward);
			if (Physics.Raycast(ray, out hit, 40f, this.layerMask))
			{
				chunk = hit.transform.parent?.GetComponent<TileStageChunkComponent>();
				if (chunk != null)
				{
					return true;
				}
			}

			chunk = null;
			return false;
		}

		private void TilesRead(ReadTileAdjacencyTask completedTask)
		{
			Require.That(completedTask != null);

			this.isWaitingForReadToComplete = false;
			this.shapePreview.transform.position = (completedTask.TileIndex.ToVector() + new Vector3(.5f, .5f, .5f)) * this.tileScale;
			this.RefreshTileShapePreview();

			if (!this.isDeleting)
			{
				this.shapePreview.gameObject.SetActive(true);
			}
		}

		private void RefreshTileShapePreview()
		{
			var tile = this.GetTile();

			var updater = this.tileTypes.Value.Updater;
			if (updater != null)
			{
				this.readTask.Tiles[Adjacency.Center] = tile;
				if (updater.TryUpdateTile(this.readTask, out var updatedTile))
				{
					tile = updatedTile;
				}
			}

			this.shapePreview.TileShape = this.atlas.Types.GetTileShapeData(tile);
		}

		private Tile GetTile() => new Tile(
			this.tileTypes.Value.Key, Orthogonal.GetOrthoAxis(this.direction, this.rotation, this.isFlipped));
	}
}
