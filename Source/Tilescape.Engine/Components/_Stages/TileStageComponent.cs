﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Unity.Interests;
using UniRx;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	public class TileStageComponent : MonoBehaviour, ITileStageComponent
	{
		private readonly IDictionary<Index3D, TileStageChunkComponent> chunks =
			new Dictionary<Index3D, TileStageChunkComponent>(Equality.StructComparer<Index3D>());

		private ITileStageConfig config;

		/// <inheritdoc />
		public TileStageManagerComponent Manager { get; private set; }

		/// <inheritdoc />
		public ITileStage Stage { get; private set; }

		/// <inheritdoc />
		public Index3D GetTileIndex(Vector3 hitPoint, Vector3 hitNormal) =>
			this.GetIndex(hitPoint, hitNormal, this.config.TileLength);

		/// <inheritdoc />
		public Index3D GetTileAdjacentIndex(Vector3 hitPoint, Vector3 hitNormal) =>
			this.GetAdjacentIndex(hitPoint, hitNormal, this.config.TileLength);

		/// <inheritdoc />
		public Index3D GetTileVoxelIndex(Vector3 hitPoint, Vector3 hitNormal) =>
			this.GetIndex(hitPoint, hitNormal, this.config.VoxelLength);

		/// <inheritdoc />
		public Index3D GetTileVoxelAdjacentIndex(Vector3 hitPoint, Vector3 hitNormal) =>
			this.GetIndex(hitPoint, -hitNormal, this.config.VoxelLength);

		internal void Initialize(TileStageManagerComponent manager, ITileStage stage)
		{
			Require.That(manager != null);
			Require.That(stage != null);

			this.Manager = manager;
			this.Stage = stage;
			this.config = manager.Atlas.Config;

			this.Stage.Map.InterestsChanged.Closed().Select(pair => pair.Key).Subscribe(this.CloseAndRemoveChunk);

			// TODO testing only
			this.timer.Start();
		}

		// TODO testing only
		private readonly Stopwatch timer = new Stopwatch();

		internal void ResyncStage(IResyncTileStage frame)
		{
			Require.That(frame != null);
			Require.That(frame.Key == this.Stage.Key);

			for (int chunkIndex = 0; chunkIndex < frame.Chunks.Count; chunkIndex++)
			{
				var resyncChunk = frame.Chunks[chunkIndex];
				if (!resyncChunk.IsEmpty && this.Stage.Map.HasAnyInterests(resyncChunk.Key))
				{
					if (this.chunks.TryGetValue(resyncChunk.Key, out var chunkComponent))
					{
						chunkComponent.CloseChunk();
						chunkComponent.Initialize(resyncChunk);
					}
					else
					{
						var chunkGameObject = this.Manager.TileStageGameObjects.Chunks.Take();
						chunkComponent = chunkGameObject.GetComponent<TileStageChunkComponent>();
						chunkComponent.Initialize(this, resyncChunk);
						this.chunks[resyncChunk.Key] = chunkComponent;
					}

					// TODO testing only
					////for (int modifiedIndex = 0; modifiedIndex < resyncChunk.ModifiedTiles.Count; modifiedIndex++)
					////{
					////	var pair = resyncChunk.ModifiedTiles[modifiedIndex];
					////	var variants = this.Manager.Atlas.Variants;
					////	var tileIndex = variants.Config.ConvertLocalTileToStage(resyncChunk.Key, pair.Key).ToString();
					////	var previousType = variants[pair.Value.Previous.Key].Type.Name;
					////	var nextType = variants[pair.Value.Next.Key].Type.Name;
					////	UnityEngine.Debug.Log($"Index {tileIndex} was {previousType} and is now {nextType}.");
					////}
				}
				else
				{
					this.CloseAndRemoveChunk(resyncChunk.Key);
				}
			}

			// TODO testing only
			int limit = 784;
			////int limit = 5945;
			if (this.timer.IsRunning && this.chunks.Count == limit)
			{
				this.timer.Stop();
				UnityEngine.Debug.Log($"Finished generating chunks in {this.timer.Elapsed}");
			}

			////UnityEngine.Debug.Log(this.chunks.Count);
		}

		// TODO what do?
		////public void CloseStage()
		////{
		////	foreach (var chunk in this.chunks.Values)
		////	{
		////		chunk.CloseChunk();
		////	}

		////	this.chunks.Clear();
		////	this.Manager.CloseTileStage(this.Stage);
		////}

		private void CloseAndRemoveChunk(Index3D index)
		{
			if (this.chunks.TryGetValue(index, out var chunk))
			{
				chunk.CloseAndReturnChunkToPool();
				this.chunks.Remove(index);
			}
		}

		private Index3D GetIndex(Vector3 hitPoint, Vector3 hitNormal, float length)
		{
			hitPoint = this.transform.InverseTransformPoint(hitPoint);
			hitNormal = this.transform.InverseTransformDirection(hitNormal);

			hitPoint -= hitNormal * this.config.VoxelHalfLength;
			return this.HitPointToIndex(hitPoint, length);
		}

		private Index3D GetAdjacentIndex(Vector3 hitPoint, Vector3 hitNormal, float length)
		{
			hitPoint = this.transform.InverseTransformPoint(hitPoint);
			hitNormal = this.transform.InverseTransformDirection(hitNormal);

			hitPoint += hitNormal * (this.config.TileHalfLength - this.config.VoxelHalfLength);
			return this.HitPointToIndex(hitPoint, length);
		}

		private Index3D HitPointToIndex(Vector3 hitPoint, float length)
		{
			Require.That(length > 0);

			if (length != 1f)
			{
				hitPoint /= length;
			}

			return new Index3D((int)Mathf.Floor(hitPoint.x), (int)Mathf.Floor(hitPoint.y), (int)Mathf.Floor(hitPoint.z));
		}
	}
}
