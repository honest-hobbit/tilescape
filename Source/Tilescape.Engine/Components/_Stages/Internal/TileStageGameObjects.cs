﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Pooling;
using Tilescape.Utility.Unity.Types;
using UnityEngine;
using UnityEngine.Rendering;

namespace Tilescape.Engine.Components
{
	internal class TileStageGameObjects
	{
		////private readonly IPool<GameObject> stages = GameObjectPool.Create(
		////	() =>
		////	{
		////		var stage = new GameObject("TileStage");
		////		stage.AddComponent<TileStageComponent>();
		////		return stage;
		////	});

		private readonly IPool<GameObject> chunks = GameObjectPool.Create(
			() =>
			{
				var chunk = new GameObject("TileStageChunk");
				chunk.AddComponent<TileStageChunkComponent>();
				return chunk;
			});

		private readonly IPool<GameObject> chunkBoxColliders = GameObjectPool.Create(
			() =>
			{
				var collider = new GameObject("TileStageChunk Collider");
				collider.AddComponent<Rigidbody>().isKinematic = true;
				collider.AddComponent<BoxCollider>();
				return collider;
			});

		private readonly IPool<GameObject> chunkMeshes;

		public TileStageGameObjects(Material tileChunkMaterial)
		{
			Require.That(tileChunkMaterial != null);

			this.chunkMeshes = GameObjectPool.Create(
				() =>
				{
					var chunkMesh = new GameObject("TileStageChunk Mesh");
					chunkMesh.AddComponent<MeshFilter>().sharedMesh = new Mesh();
					var renderer = chunkMesh.AddComponent<MeshRenderer>();
					renderer.material = tileChunkMaterial;
					renderer.shadowCastingMode = ShadowCastingMode.TwoSided;
					renderer.receiveShadows = true;
					return chunkMesh;
				});
		}

		////public IPool<GameObject> Stages => this.stages;

		public IPool<GameObject> Chunks => this.chunks;

		public IPool<GameObject> ChunkBoxColliders => this.chunkBoxColliders;

		public IPool<GameObject> ChunkMeshes => this.chunkMeshes;

		public void DestroyAll()
		{
			foreach (var gameObject in this.chunks.TakeAll())
			{
				Object.Destroy(gameObject);
			}

			foreach (var gameObject in this.chunkBoxColliders.TakeAll())
			{
				Object.Destroy(gameObject);
			}

			foreach (var gameObject in this.chunkMeshes.TakeAll())
			{
				Object.Destroy(gameObject);
			}
		}
	}
}
