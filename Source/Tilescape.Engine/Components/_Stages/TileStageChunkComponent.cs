﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Meshing;
using Tilescape.Utility.Unity.Types;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	public class TileStageChunkComponent : MonoBehaviour, ITileStageComponent, IKeyed<Index3D>
	{
		public TileStageComponent Parent { get; private set; }

		/// <inheritdoc />
		public Index3D Key { get; private set; }

		/// <inheritdoc />
		public TileStageManagerComponent Manager => this.Parent.Manager;

		/// <inheritdoc />
		public ITileStage Stage => this.Parent.Stage;

		/// <inheritdoc />
		public Index3D GetTileIndex(Vector3 hitPoint, Vector3 hitNormal) =>
			this.Parent.GetTileIndex(hitPoint, hitNormal);

		/// <inheritdoc />
		public Index3D GetTileAdjacentIndex(Vector3 hitPoint, Vector3 hitNormal) =>
			this.Parent.GetTileAdjacentIndex(hitPoint, hitNormal);

		/// <inheritdoc />
		public Index3D GetTileVoxelIndex(Vector3 hitPoint, Vector3 hitNormal) =>
			this.Parent.GetTileVoxelIndex(hitPoint, hitNormal);

		/// <inheritdoc />
		public Index3D GetTileVoxelAdjacentIndex(Vector3 hitPoint, Vector3 hitNormal) =>
			this.Parent.GetTileVoxelAdjacentIndex(hitPoint, hitNormal);

		internal void Initialize(TileStageComponent stage, IResyncTileChunk resyncChunk)
		{
			Require.That(stage != null);
			Require.That(resyncChunk != null);

			this.Key = resyncChunk.Key;
			this.Parent = stage;

			this.gameObject.name = "TileStageChunk " + resyncChunk.Key;
			this.transform.position = resyncChunk.Key.ToVector() * this.Stage.Manager.Atlas.Config.ChunkLength;
			this.transform.SetParent(this.Parent.transform, false);

			this.Initialize(resyncChunk);
		}

		internal void Initialize(IResyncTileChunk resyncChunk)
		{
			Require.That(resyncChunk != null);
			Require.That(resyncChunk.Key == this.Key);

			for (int index = 0; index < resyncChunk.Meshes.Count; index++)
			{
				var chunkMesh = this.Parent.Manager.TileStageGameObjects.ChunkMeshes.Take();
				chunkMesh.transform.SetParent(this.transform, false);
				resyncChunk.Meshes[index].BuildMesh(chunkMesh.GetComponent<MeshFilter>().sharedMesh);
			}

			for (int index = 0; index < resyncChunk.Colliders.Count; index++)
			{
				var chunkCollider = this.Parent.Manager.TileStageGameObjects.ChunkBoxColliders.Take();
				chunkCollider.transform.SetParent(this.transform, false);
				var box = chunkCollider.GetComponent<BoxCollider>();
				var boxInfo = resyncChunk.Colliders[index];
				box.center = boxInfo.Center;
				box.size = boxInfo.Size;
			}
		}

		internal void CloseChunk()
		{
			var meshes = this.transform.GetComponentsInChildren<MeshFilter>();
			for (int index = 0; index < meshes.Length; index++)
			{
				var meshFilter = meshes[index];
				meshFilter.sharedMesh.Clear();
				this.Parent.Manager.TileStageGameObjects.ChunkMeshes.Give(meshFilter.gameObject);
			}

			var colliders = this.transform.GetComponentsInChildren<BoxCollider>();
			for (int index = 0; index < colliders.Length; index++)
			{
				this.Parent.Manager.TileStageGameObjects.ChunkBoxColliders.Give(colliders[index].gameObject);
			}
		}

		internal void CloseAndReturnChunkToPool()
		{
			this.CloseChunk();
			this.Parent.Manager.TileStageGameObjects.Chunks.Give(this.gameObject);
		}
	}
}
