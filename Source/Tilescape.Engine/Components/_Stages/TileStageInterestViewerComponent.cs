﻿using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Pooling;
using Tilescape.Utility.Unity.Interests;
using Tilescape.Utility.Unity.Types;
using UniRx;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[RequireComponent(typeof(TileStageComponent))]
	public class TileStageInterestViewerComponent : MonoBehaviour
	{
		private static IPool<GameObject> chunkPool;

		private readonly IDictionary<Index3D, GameObject> chunks = new Dictionary<Index3D, GameObject>(Equality.StructComparer<Index3D>());

		private ITileStageConfig config;

		private void Start()
		{
			if (chunkPool == null)
			{
				var chunkPrefab = Resources.Load<GameObject>("Prefabs/Chunk Outline");
				chunkPool = GameObjectPool.Create(() => Instantiate(chunkPrefab));
			}

			var stage = this.GetComponent<TileStageComponent>().Stage;
			this.config = stage.Manager.Atlas.Config;
			stage.Map.InterestsChanged.Opened().Select(pair => pair.Key).Subscribe(this.OpenChunk);
			stage.Map.InterestsChanged.Closed().Select(pair => pair.Key).Subscribe(this.CloseChunk);
		}

		private void OpenChunk(Index3D index)
		{
			var chunk = chunkPool.Take();
			chunk.transform.localScale = this.config.GetChunkSizeVector();
			chunk.transform.position = (index.ToVector() * this.config.ChunkLength) + (this.config.GetChunkSizeVector() / 2);
			chunk.transform.SetParent(this.transform, false);
			this.chunks[index] = chunk;
		}

		private void CloseChunk(Index3D index)
		{
			if (this.chunks.TryGetValue(index, out var chunk))
			{
				chunkPool.Give(chunk);
				this.chunks.Remove(index);
			}
		}
	}
}
