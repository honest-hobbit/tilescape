﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Shapes;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Unity.Components;
using Tilescape.Utility.Unity.Interests;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[RequireComponent(typeof(ObservablePosition))]
	public class TilescapeInterestAreaComponent : MonoBehaviour
	{
		private readonly Dictionary<ITileStage, IDisposable> subscriptions =
			new Dictionary<ITileStage, IDisposable>(IdentityEquality.Comparer<ITileStage>());

		private TileStageManagerComponent manager;

		private ResizableInterestArea3D<int> area;

		[SerializeField]
		private Shape3D shape = Shape3D.Column;

		[SerializeField]
		private int radius = 1;

		[SerializeField]
		private int minRadius = 1;

		[SerializeField]
		private int maxRadius = 10;

		public void AddTileStage(ITileStage stage)
		{
			Require.That(this.area != null);
			Require.That(stage != null);
			Require.That(!stage.IsDisposed);
			Require.That(!this.subscriptions.ContainsKey(stage));

			this.subscriptions[stage] = stage.SubscribeArea(this.area);
		}

		public void RemoveTileStage(ITileStage stage)
		{
			Require.That(this.area != null);
			Require.That(stage != null);
			Require.That(this.subscriptions.ContainsKey(stage));

			this.subscriptions[stage].Dispose();
			this.subscriptions.Remove(stage);
		}

		private void OnValidate()
		{
			this.minRadius = this.minRadius.ClampLower(1);
			this.maxRadius = this.maxRadius.ClampLower(this.minRadius);
			this.radius = this.radius.Clamp(this.minRadius, this.maxRadius);
		}

		private void Awake()
		{
			this.area = ResizableInterestArea.Create(1, this.shape);
			this.area.Radius = this.radius;
		}

		private void Start()
		{
			this.manager = Singleton.Of<TileStageManagerComponent>();
			float chunkLength = this.manager.Atlas.Config.ChunkLengthInTiles * this.manager.TileScale;
			this.GetComponent<ObservablePosition>().PositionChanged.ToChunkIndex(chunkLength).Subscribe(this.area);
			this.manager.AddInterestArea(this);
		}

		// TODO what do about diposing stages?
		////private void OnDestroy()
		////{
		////	foreach (var disposable in this.subscriptions.Values)
		////	{
		////		disposable.Dispose();
		////	}

		////	this.subscriptions.Clear();
		////	this.manager?.RemoveInterestArea(this);
		////}

		////private void OnEnable()
		////{
		////	foreach (var stage in this.subscriptions.Keys.ToList())
		////	{
		////		this.subscriptions[stage] = stage.SubscribeArea(this.area);
		////	}
		////}

		////private void OnDisable()
		////{
		////	foreach (var disposble in this.subscriptions.Values)
		////	{
		////		disposble.Dispose();
		////	}
		////}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.KeypadPlus))
			{
				this.area.Radius = (this.area.Radius + 1).ClampUpper(this.maxRadius);
			}

			if (Input.GetKeyDown(KeyCode.KeypadMinus))
			{
				this.area.Radius = (this.area.Radius - 1).ClampLower(this.minRadius);
			}
		}
	}
}
