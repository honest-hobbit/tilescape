﻿using Tilescape.Utility.Indexing.Indices;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	public interface ITileStageComponent
	{
		TileStageManagerComponent Manager { get; }

		ITileStage Stage { get; }

		// these methods all return global stage indices
		Index3D GetTileIndex(Vector3 hitPoint, Vector3 hitNormal);

		Index3D GetTileAdjacentIndex(Vector3 hitPoint, Vector3 hitNormal);

		Index3D GetTileVoxelIndex(Vector3 hitPoint, Vector3 hitNormal);

		Index3D GetTileVoxelAdjacentIndex(Vector3 hitPoint, Vector3 hitNormal);
	}
}
