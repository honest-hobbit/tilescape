﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Concurrency;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Components;
using UniRx;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	public class TileStageManagerComponent : MonoBehaviour, IDisposed
	{
		private readonly IDispatchProcessor processor = new DispatchProcessor();

		private readonly HashSet<TilescapeInterestAreaComponent> interestAreas = new HashSet<TilescapeInterestAreaComponent>();

		private readonly Dictionary<Guid, TileStageComponent> stages =
			new Dictionary<Guid, TileStageComponent>(Equality.StructComparer<Guid>());

		[SerializeField]
		private TilescapeStoreComponent store = null;

		[SerializeField]
		private Material tileChunkMaterial = null;

		[SerializeField]
		private int cacheSizeInMegabytes = 500;

		[SerializeField]
		private float tileScale = 1;

		private TileStageManager stageManager;

		public float TileScale => this.tileScale;

		public bool IsInitialized { get; private set; }

		/// <inheritdoc />
		public bool IsDisposed => this.stageManager?.IsDisposed ?? false;

		public ITilescapeAtlas Atlas { get; private set; }

		internal TileStageGameObjects TileStageGameObjects { get; private set; }

		public void Initialize(ITilescapeAtlas atlas, ITileChunkPopulator chunkPopulator)
		{
			Require.That(!this.IsInitialized);
			Require.That(atlas != null);
			Require.That(chunkPopulator != null);

			this.IsInitialized = true;
			this.Atlas = atlas;

			this.TileStageGameObjects = new TileStageGameObjects(this.tileChunkMaterial);

			this.store = this.store ?? Singleton.Of<TilescapeStoreComponent>();
			this.store.DisposeStoreOnDestroy = false;

			var store = this.store.Store;

			var cachingConfig = new CachingConfig()
			{
				ExpiryCapacityMultiplier = 2
			};
			cachingConfig.SetMaxPooledChunksInBytes(store.Config.Stages, Bytes.PerMegabyte * this.cacheSizeInMegabytes);
			cachingConfig.SetMaxCachedChunksFromMultiplier(2);

			var resyncConfig = new ResyncUnityConfig()
			{
				Frames = 4,
				ChunksPerFrame = 16,
				InitialMeshBuilders = 100
			};

			var storeLifespan = new ThreadedStoreLifespanManager(store, chunkPopulator, runFallbackOnThreadPool: false);
			this.stageManager = new TileStageManager(this.Atlas, cachingConfig, resyncConfig, storeLifespan);
			this.stageManager.ResyncFrameReady.Subscribe(frame => this.processor.Dispatcher.Submit(frame, this.ProcessResyncFrame));
		}

		public void AddInterestArea(TilescapeInterestAreaComponent area)
		{
			Require.That(this.IsInitialized);
			Require.That(!this.IsDisposed);
			Require.That(area != null);

			this.interestAreas.Add(area);
			foreach (var stage in this.stageManager.Stages.Values)
			{
				area.AddTileStage(stage);
			}
		}

		public void RemoveInterestArea(TilescapeInterestAreaComponent area)
		{
			// deliberately does not require that this IsInitialized
			Require.That(area != null);

			this.interestAreas.Remove(area);
		}

		public void CreateStage(string name)
		{
			Require.That(this.IsInitialized);
			Require.That(!this.IsDisposed);
			Require.That(!name.IsNullOrWhiteSpace());

			var stage = this.stageManager.CreateStage(name);
			var stageGameObject = new GameObject("Tile Stage " + stage.Key);
			stageGameObject.transform.localScale = new Vector3(this.tileScale, this.tileScale, this.tileScale);
			var stageComponent = stageGameObject.AddComponent<TileStageComponent>();
			stageComponent.Initialize(this, stage);

			// TODO testing to show which chunks are set to active
			////stageGameObject.AddComponent<TileStageInterestViewerComponent>();

			// TODO need to add something to handle closing stages as well
			this.stages[stage.Key] = stageComponent;

			foreach (var area in this.interestAreas)
			{
				area.AddTileStage(stage);
			}
		}

		public void Submit(ITileJob job) => this.stageManager.Submit(job);

		// TODO what do about diposing stages?
		////public void CloseTileStage(ITileStage stage)
		////{
		////	Require.That(this.IsInitialized);
		////	Require.That(stage != null);

		////	stage.Dispose();
		////	foreach (var area in this.interestAreas)
		////	{
		////		area.RemoveTileStage(stage);
		////	}
		////}

		private void OnValidate()
		{
			if (this.cacheSizeInMegabytes <= 0)
			{
				this.cacheSizeInMegabytes = 1;
			}

			if (this.tileScale < 0)
			{
				this.tileScale = 1;
			}
		}

		private void Update()
		{
			this.processor.InvokeAll();
		}

		private void OnDestroy()
		{
			this.stageManager?.Dispose();

			// TODO what do about diposing stages?
			////this.Dispatcher.Submit(() =>
			////{
			////	this.stageManager.Dispose();
			////	////this.stageGameObjects.DestroyAll();
			////});

			////this.stageManager.Dispose();
			////this.stageGameObjects.DestroyAll();
		}

		private void ProcessResyncFrame(IResyncFrame frame)
		{
			Require.That(frame != null);

			frame.Stages.ForEach(stageFrame =>
			{
				if (this.stages.TryGetValue(stageFrame.Key, out var stageComponent))
				{
					stageComponent.ResyncStage(stageFrame);
				}
			});

			frame.JobsRan.ForEachListViewItem(task => task.FinishInUnity());
			frame.CompleteResyncing();
		}
	}
}
