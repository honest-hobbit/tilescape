﻿using System.Collections.Generic;
using Tilescape.Utility.Unity.Meshing;
using Tilescape.Utility.Unity.Types;
using UnityEngine;
using UnityEngine.Rendering;

namespace Tilescape.Engine.Components
{
	public class TileShapeComponent : AbstractTileShapeComponent
	{
		private readonly List<GameObject> parts = new List<GameObject>();

		protected override void ClearMeshes()
		{
			var filter = this.gameObject.GetComponent<MeshFilter>();
			if (filter != null)
			{
				var mesh = filter.sharedMesh;
				if (mesh != null)
				{
					mesh.Clear();
				}
			}
		}

		protected override void RecreateTile(TileShapeProjection tileProjection, QuadMultiMeshBuilder meshBuilder)
		{
			var meshes = tileProjection.Meshes;
			meshBuilder.Add(meshes.Center);
			meshBuilder.Add(meshes.NegativeX);
			meshBuilder.Add(meshes.PositiveX);
			meshBuilder.Add(meshes.NegativeY);
			meshBuilder.Add(meshes.PositiveY);
			meshBuilder.Add(meshes.NegativeZ);
			meshBuilder.Add(meshes.PositiveZ);

			var filter = this.gameObject.GetOrAddComponent<MeshFilter>();
			filter.sharedMesh = meshBuilder.SingleOrEmpty().BuildMesh(filter.sharedMesh ?? new Mesh());

			var renderer = this.gameObject.GetOrAddComponent<MeshRenderer>();
			renderer.material = this.TileMaterial;
			renderer.shadowCastingMode = ShadowCastingMode.TwoSided;
			renderer.receiveShadows = true;
		}
	}
}
