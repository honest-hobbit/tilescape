﻿using UnityEditor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[CustomEditor(typeof(TileShapeTransformerComponent))]
	public class TileShapeTransformerComponentInspector : Editor
	{
		private static readonly int SpacingPixels = 10;

		public override void OnInspectorGUI()
		{
			this.DrawDefaultInspector();
			var transformer = (TileShapeTransformerComponent)this.target;

			if (GUILayout.Button("Rotate X"))
			{
				transformer.RotateX();
			}

			if (GUILayout.Button("Rotate Y"))
			{
				transformer.RotateY();
			}

			if (GUILayout.Button("Rotate Z"))
			{
				transformer.RotateZ();
			}

			GUILayout.Space(SpacingPixels);

			if (GUILayout.Button("Mirror X"))
			{
				transformer.MirrorX();
			}

			if (GUILayout.Button("Mirror Y"))
			{
				transformer.MirrorY();
			}

			if (GUILayout.Button("Mirror Z"))
			{
				transformer.MirrorZ();
			}

			GUILayout.Space(SpacingPixels);

			if (GUILayout.Button("Reset Rotation"))
			{
				transformer.ResetRotation();
			}
		}
	}
}
