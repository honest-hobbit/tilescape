﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	public class CubicTileShapeTesterComponent : MonoBehaviour
	{
		private static ITileColorPalette Colors => MagicalVoxelTileColorPalette.Default;

		private static readonly IEnumerable<OrthoAxis> AllowedRotations = new OrthoAxis[]
		{
			OrthoAxis.PosY000Norm_24, OrthoAxis.PosY180Flip_29,
			OrthoAxis.PosY090Norm_26, OrthoAxis.PosY090Flip_27,
			OrthoAxis.PosY180Norm_28, OrthoAxis.PosY000Flip_25,
			OrthoAxis.PosY270Norm_30, OrthoAxis.PosY270Flip_31,
		};

		private Index3D voxelCorner0;

		private Index3D voxelCorner1;

		private Index3D voxelCorner2;

		private Index3D voxelCorner3;

		private Index3D voxelCorner4;

		private Index3D voxelCorner5;

		private Index3D voxelCorner6;

		private Index3D voxelCorner7;

		private List<TileShape> shapes = null;

		[SerializeField]
		private TilescapeStoreComponent tilescapeStore = null;

		[SerializeField]
		private List<string> shapeNames = null;

		[SerializeField]
		private string resultFilePath = null;

		private string FullResultFilePath => Path.GetFullPath(Path.Combine(Application.dataPath, this.resultFilePath));

		public void GenerateTableFile()
		{
			if (!this.TryLoadShapes())
			{
				Debug.LogWarning("Unable to load Tile Shapes.");
				return;
			}

			var shapeProjection = new TileShapeProjection(this.tilescapeStore.Store.Config.Stages);
			var fileText = new StringBuilder(1000);
			var matches = new List<Match>();
			int invalidCount = 0;

			fileText.AppendLine("		private static readonly Case[] CasesArray = new Case[]");
			fileText.AppendLine("		{");

			for (int cornerCase = 0; cornerCase <= byte.MaxValue; cornerCase++)
			{
				var cubicBits = new CubicTileBits((byte)cornerCase);

				for (int shapeIndex = 0; shapeIndex < this.shapes.Count; shapeIndex++)
				{
					foreach (var rotation in AllowedRotations)
					{
						shapeProjection.SetProjection(new TileShapeData(this.shapes[shapeIndex], Colors, rotation));

						if (shapeProjection.Voxels[this.voxelCorner0].IsEmpty != cubicBits.Corner0 &&
							shapeProjection.Voxels[this.voxelCorner1].IsEmpty != cubicBits.Corner1 &&
							shapeProjection.Voxels[this.voxelCorner2].IsEmpty != cubicBits.Corner2 &&
							shapeProjection.Voxels[this.voxelCorner3].IsEmpty != cubicBits.Corner3 &&
							shapeProjection.Voxels[this.voxelCorner4].IsEmpty != cubicBits.Corner4 &&
							shapeProjection.Voxels[this.voxelCorner5].IsEmpty != cubicBits.Corner5 &&
							shapeProjection.Voxels[this.voxelCorner6].IsEmpty != cubicBits.Corner6 &&
							shapeProjection.Voxels[this.voxelCorner7].IsEmpty != cubicBits.Corner7)
						{
							matches.Add(new Match((CubicTile)shapeIndex, rotation));
						}
					}
				}

				if (cornerCase.IsDivisibleBy(16))
				{
					if (cornerCase != 0)
					{
						fileText.AppendLine();
					}

					fileText.AppendLine("			// " + cornerCase);
				}

				this.AddCaseLine(matches, fileText, ref invalidCount);
				matches.Clear();
			}

			fileText.AppendLine("		};");

			var path = this.FullResultFilePath;
			Directory.CreateDirectory(Path.GetDirectoryName(path));
			File.WriteAllText(path, fileText.ToString());

			Debug.Log("Successfully generated table cases file. Invalid cases count: " + invalidCount);
		}

		private bool TryLoadShapes()
		{
			if (this.shapeNames == null || this.shapeNames.Count == 0 || this.tilescapeStore == null)
			{
				return false;
			}

			if (this.shapes != null && this.shapes.Count == this.shapeNames.Count)
			{
				return true;
			}

			this.shapes = new List<TileShape>(this.shapeNames.Count);
			try
			{
				foreach (var name in this.shapeNames)
				{
					this.shapes.Add(this.tilescapeStore.Store.Shapes.Find(name).Single());
				}
			}
			catch (Exception)
			{
				this.shapes.Clear();
				throw;
			}

			int tileLength = this.tilescapeStore.Store.Config.Stages.TileLengthInVoxels - 1;
			this.voxelCorner0 = CubicTileBits.IndexOfCorner0 * tileLength;
			this.voxelCorner1 = CubicTileBits.IndexOfCorner1 * tileLength;
			this.voxelCorner2 = CubicTileBits.IndexOfCorner2 * tileLength;
			this.voxelCorner3 = CubicTileBits.IndexOfCorner3 * tileLength;
			this.voxelCorner4 = CubicTileBits.IndexOfCorner4 * tileLength;
			this.voxelCorner5 = CubicTileBits.IndexOfCorner5 * tileLength;
			this.voxelCorner6 = CubicTileBits.IndexOfCorner6 * tileLength;
			this.voxelCorner7 = CubicTileBits.IndexOfCorner7 * tileLength;

			return true;
		}

		private void AddCaseLine(List<Match> matches, StringBuilder fileText, ref int invalidCount)
		{
			Require.That(matches != null);
			Require.That(fileText != null);

			if (matches.GroupBy(x => x.Case).Count() > 1)
			{
				fileText.AppendLine("More than 1 matching CubicTile case found. This shouldn't be possible!");
				return;
			}

			switch (matches.Count)
			{
				case 0:
					fileText.AppendLine(this.GetCaseText(CubicTile.CaseFF, OrthoAxis.PosY000Norm_24, "invalid"));
					invalidCount++;
					break;

				case 1:
					fileText.AppendLine(this.GetCaseText(matches[0]));
					break;

				case 2:
					var match1 = matches[0];
					var match2 = matches[1];
					if (match1.Rotation.GetIsFlipped() && !match2.Rotation.GetIsFlipped())
					{
						fileText.AppendLine(this.GetCaseText(match2, match1.Rotation.ToString()));
					}
					else if (!match1.Rotation.GetIsFlipped() && match2.Rotation.GetIsFlipped())
					{
						fileText.AppendLine(this.GetCaseText(match1, match2.Rotation.ToString()));
					}
					else
					{
						fileText.AppendLine("There are 2 rotations and no obvious way to pick between them.");
					}

					break;

				case 8:
					fileText.AppendLine(this.GetCaseText(matches[0].Case, OrthoAxis.PosY000Norm_24, "all"));
					break;

				default:
					var match = matches.Where(x => !x.Rotation.GetIsFlipped()).OrderBy(x => x.Rotation).First();
					var comment = matches.Select(x => x.Rotation).Where(x => x != match.Rotation).ToJoinString(", ");
					fileText.AppendLine(this.GetCaseText(match, comment));
					break;
			}
		}

		private string GetCaseText(Match match, string comment = null) => this.GetCaseText(match.Case, match.Rotation, comment);

		private string GetCaseText(CubicTile tileCase, OrthoAxis rotation, string comment = null)
		{
			if (comment == null)
			{
				return $"			new Case(CubicTile.{tileCase.ToString()}, OrthoAxis.{rotation.ToString()}),";
			}
			else
			{
				return $"			new Case(CubicTile.{tileCase.ToString()}, OrthoAxis.{rotation.ToString()}),	// {comment}";
			}
		}

		private struct Match
		{
			public Match(CubicTile tileCase, OrthoAxis rotation)
			{
				this.Case = tileCase;
				this.Rotation = rotation;
			}

			public CubicTile Case { get; }

			public OrthoAxis Rotation { get; }

			public override string ToString() => $"{this.Case.ToString()} {this.Rotation.ToString()}";
		}
	}
}
