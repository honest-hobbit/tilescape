﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(TilescapeStoreComponent))]
	public class TileShapeOrientationsViewerComponent : MonoBehaviour
	{
		private static ITileColorPalette Colors => MagicalVoxelTileColorPalette.Default;

		private readonly TileShapeComponent[] shapes = new TileShapeComponent[Orthogonal.Orientations.Count * 2];

		[SerializeField]
		private Material tileMaterial = null;

		[SerializeField]
		private string tileShapeName = null;

		[SerializeField]
		private float spacing = 1;

		[SerializeField]
		private bool createVoxelClones = true;

		private TileShapeBuilder builder;

		private TileShape shapeToBuild;

		private TileShapeProjection projection;

		public void CreateShapes()
		{
			if (this.tileMaterial == null)
			{
				throw new InvalidOperationException($"{nameof(this.tileMaterial)} must not be null.");
			}

			var store = this.GetComponent<TilescapeStoreComponent>().Store;
			if (store.Shapes.Find(this.tileShapeName).TryGetSingle(out var shape))
			{
				var config = store.Config.Stages;
				if (this.builder == null || this.builder.TileSizeExponent != config.TileSizeExponent)
				{
					this.builder = new TileShapeBuilder(config)
					{
						MeshingMode = MeshingMode.Greedy
					};

					this.shapeToBuild = new TileShape(config.TileSizeExponent);
					this.projection = new TileShapeProjection(config);
				}

				for (int index = 0; index < Orthogonal.Orientations.Count; index++)
				{
					var shapeData = new TileShapeData(shape, Colors, Orthogonal.Orientations[index]);
					var name = $"[{((int)shapeData.Rotation).ToString()}] {shapeData.Rotation.ToString()}";
					this.CreateShape(shapeData, index, false, name);

					if (this.createVoxelClones)
					{
						this.projection.SetProjection(shapeData);

						this.projection.Voxels.ForEach(x => this.shapeToBuild.Voxels[x.Index] = x.Value);
						////for (int voxelIndex = 0; voxelIndex < this.projection.Voxels.Length; voxelIndex++)
						////{
						////	this.shapeToBuild.Voxels[voxelIndex] = this.projection.Voxels[voxelIndex];
						////}

						this.builder.BuildShape(this.shapeToBuild);
						this.CreateShape(
							new TileShapeData(this.shapeToBuild, Colors, OrthoAxis.PosY000Norm_24), index, true, name + " Cloned");
					}
				}

				if (!this.createVoxelClones)
				{
					for (int index = Orthogonal.Orientations.Count; index < this.shapes.Length; index++)
					{
						var destroyShape = this.shapes[index];
						if (destroyShape != null)
						{
							this.shapes[index] = null;
							DestroyImmediate(destroyShape.gameObject);
						}
					}
				}
			}
			else
			{
				throw new InvalidOperationException($"Unable to get TileShape [{this.tileShapeName}].");
			}
		}

		private void CreateShape(TileShapeData shapeData, int index, bool isOffset, string name)
		{
			Require.That(shapeData != TileShapeData.Unassigned);
			Require.That(index >= 0 && index < Orthogonal.Orientations.Count);
			Require.That(!name.IsNullOrWhiteSpace());

			int shapesIndex = index + (isOffset ? Orthogonal.Orientations.Count : 0);
			var child = this.shapes[shapesIndex];

			if (child == null)
			{
				var childObject = new GameObject(name)
				{
					hideFlags = HideFlags.DontSaveInEditor
				};

				childObject.transform.SetParent(this.transform);
				child = childObject.AddComponent<TileShapeComponent>();
				child.TileMaterial = this.tileMaterial;
				this.shapes[shapesIndex] = child;
			}

			int z = MathUtility.Divide(index, 8, out int x);
			int y = isOffset ? -1 : 0;
			child.transform.position = new Vector3(x, y, z) * this.spacing;
			child.TileShape = shapeData;
		}

		private void Start()
		{
			if (this.tileMaterial != null && !this.tileShapeName.IsNullOrWhiteSpace())
			{
				this.CreateShapes();
			}
		}
	}
}
