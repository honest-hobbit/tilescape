﻿using UnityEditor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[CustomEditor(typeof(CubicTileShapeTesterComponent))]
	public class CubicTileShapeTesterComponentInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			this.DrawDefaultInspector();
			var tester = (CubicTileShapeTesterComponent)this.target;

			if (GUILayout.Button("Generate Table File"))
			{
				tester.GenerateTableFile();
			}
		}
	}
}
