﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Unity.Components;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	public class TileShapeLoaderComponent : MonoBehaviour
	{
		[SerializeField]
		private Material tileMaterial = null;

		[SerializeField]
		private TilescapeStoreComponent tilescapeStore = null;

		[SerializeField]
		private string tileShapeName = null;

		public Material TileMaterial
		{
			get => this.tileMaterial;
			set => this.tileMaterial = value;
		}

		public TilescapeStoreComponent TilescapeStore
		{
			get => this.tilescapeStore;
			set => this.tilescapeStore = value;
		}

		public string TileShapeName
		{
			get => this.tileShapeName;
			set => this.tileShapeName = value;
		}

		public TileShape LoadTileShape()
		{
			if (this.GetStore().Find(this.tileShapeName).TryGetSingle(out var shape))
			{
				return shape;
			}
			else
			{
				throw new InvalidOperationException($"Unable to load TileShape [{this.tileShapeName}].");
			}
		}

		public bool TryLoadTileShape(out TileShape shape)
		{
			return this.GetStore().Find(this.tileShapeName).TryGetSingle(out shape);
		}

		private IAtlasStore<Guid, TileShape> GetStore()
		{
			if (this.tilescapeStore != null)
			{
				return this.tilescapeStore.Store.Shapes;
			}

			var store = this.GetComponent<TilescapeStoreComponent>();
			if (store != null)
			{
				return store.Store.Shapes;
			}

			return Singleton.Of<TilescapeStoreComponent>().Store.Shapes;
		}
	}
}
