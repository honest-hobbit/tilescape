﻿using UnityEditor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[CustomEditor(typeof(TileShapeOrientationsViewerComponent))]
	public class TileShapeOrientationsViewerComponentInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			this.DrawDefaultInspector();
			var viewer = (TileShapeOrientationsViewerComponent)this.target;

			if (GUILayout.Button("Create Oriented Shapes"))
			{
				viewer.CreateShapes();
			}
		}
	}
}
