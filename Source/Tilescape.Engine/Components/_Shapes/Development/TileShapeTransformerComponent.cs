﻿using UnityEditor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(TileShapeLoaderComponent))]
	public class TileShapeTransformerComponent : MonoBehaviour
	{
		private static ITileColorPalette Colors => MagicalVoxelTileColorPalette.Default;

		private TileShapeComponent shapeViewer;

		[SerializeField]
		private OrthoAxis resetTo = OrthoAxis.PosY000Norm_24;

		[SerializeField]
		private OrthoAxis orientation = OrthoAxis.PosY000Norm_24;

		[SerializeField]
		private Rotation rotation = Rotation.Clockwise90;

		public void RotateX()
		{
			this.orientation = this.orientation.RotateX(this.rotation);
			this.ApplyTransformation();
		}

		public void RotateY()
		{
			this.orientation = this.orientation.RotateY(this.rotation);
			this.ApplyTransformation();
		}

		public void RotateZ()
		{
			this.orientation = this.orientation.RotateZ(this.rotation);
			this.ApplyTransformation();
		}

		public void MirrorX()
		{
			this.orientation = this.orientation.MirrorX();
			this.ApplyTransformation();
		}

		public void MirrorY()
		{
			this.orientation = this.orientation.MirrorY();
			this.ApplyTransformation();
		}

		public void MirrorZ()
		{
			this.orientation = this.orientation.MirrorZ();
			this.ApplyTransformation();
		}

		public void ResetRotation()
		{
			this.orientation = this.resetTo;
			this.ApplyTransformation();
		}

		private void ApplyTransformation()
		{
			if (this.shapeViewer == null)
			{
				var shapeViewerObject = new GameObject("Tile Shape Viewer")
				{
					hideFlags = HideFlags.DontSaveInEditor
				};

				shapeViewerObject.transform.SetParent(this.transform, false);
				this.shapeViewer = shapeViewerObject.AddComponent<TileShapeComponent>();
			}

			var loader = this.GetComponent<TileShapeLoaderComponent>();
			if (loader.TileMaterial == null)
			{
				Debug.LogWarning("Must assign a Material to TileShapeLoaderComponent.TileMaterial");
				return;
			}

			if (this.shapeViewer.TileMaterial == null)
			{
				this.shapeViewer.TileMaterial = loader.TileMaterial;
			}

			var current = this.shapeViewer.TileShape;
			if (!current.IsAssigned || current.Shape.Name != loader.TileShapeName)
			{
				if (!loader.TryLoadTileShape(out var shape))
				{
					Debug.LogWarning($"Unable to load TileShape [{loader.TileShapeName}]", this);
					return;
				}

				this.shapeViewer.TileShape = new TileShapeData(shape, Colors, this.orientation);
			}
			else
			{
				this.shapeViewer.TileShape = new TileShapeData(current.Shape, Colors, this.orientation);
			}
		}

		private void Start()
		{
			this.ApplyTransformation();
		}

		private void OnValidate()
		{
			EditorApplication.delayCall += this.ApplyTransformation;
		}
	}
}
