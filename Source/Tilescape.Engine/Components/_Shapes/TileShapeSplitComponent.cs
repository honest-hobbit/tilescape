﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Meshing;
using Tilescape.Utility.Unity.Types;
using UnityEngine;
using UnityEngine.Rendering;

namespace Tilescape.Engine.Components
{
	public class TileShapeSplitComponent : AbstractTileShapeComponent
	{
		public GameObject Center { get; private set; }

		public GameObject FaceNegativeX { get; private set; }

		public GameObject FacePositiveX { get; private set; }

		public GameObject FaceNegativeY { get; private set; }

		public GameObject FacePositiveY { get; private set; }

		public GameObject FaceNegativeZ { get; private set; }

		public GameObject FacePositiveZ { get; private set; }

		protected override void ClearMeshes()
		{
			if (this.Center == null)
			{
				return;
			}

			ClearMesh(this.Center);
			ClearMesh(this.FaceNegativeX);
			ClearMesh(this.FacePositiveX);
			ClearMesh(this.FaceNegativeY);
			ClearMesh(this.FacePositiveY);
			ClearMesh(this.FaceNegativeZ);
			ClearMesh(this.FacePositiveZ);
		}

		protected override void RecreateTile(TileShapeProjection tileProjection, QuadMultiMeshBuilder meshBuilder)
		{
			if (this.Center == null)
			{
				this.CreateParts();
			}

			this.RecreateTilePart(this.Center, meshBuilder.Add(tileProjection.Meshes.Center));
			this.RecreateTilePart(this.FaceNegativeX, meshBuilder.Add(tileProjection.Meshes.NegativeX));
			this.RecreateTilePart(this.FacePositiveX, meshBuilder.Add(tileProjection.Meshes.PositiveX));
			this.RecreateTilePart(this.FaceNegativeY, meshBuilder.Add(tileProjection.Meshes.NegativeY));
			this.RecreateTilePart(this.FacePositiveY, meshBuilder.Add(tileProjection.Meshes.PositiveY));
			this.RecreateTilePart(this.FaceNegativeZ, meshBuilder.Add(tileProjection.Meshes.NegativeZ));
			this.RecreateTilePart(this.FacePositiveZ, meshBuilder.Add(tileProjection.Meshes.PositiveZ));
		}

		private static void ClearMesh(GameObject part)
		{
			var filter = part.GetComponent<MeshFilter>();
			if (filter == null)
			{
				return;
			}

			var mesh = filter.sharedMesh;
			if (mesh == null)
			{
				return;
			}

			mesh.Clear();
		}

		private void CreateParts()
		{
			this.Center = this.CreatePart(nameof(this.Center));
			this.FaceNegativeX = this.CreatePart(nameof(this.FaceNegativeX));
			this.FacePositiveX = this.CreatePart(nameof(this.FacePositiveX));
			this.FaceNegativeY = this.CreatePart(nameof(this.FaceNegativeY));
			this.FacePositiveY = this.CreatePart(nameof(this.FacePositiveY));
			this.FaceNegativeZ = this.CreatePart(nameof(this.FaceNegativeZ));
			this.FacePositiveZ = this.CreatePart(nameof(this.FacePositiveZ));
		}

		private GameObject CreatePart(string name)
		{
			Require.That(!name.IsNullOrWhiteSpace());

			var partGameObject = new GameObject(name)
			{
				layer = this.gameObject.layer,
				hideFlags = this.gameObject.hideFlags
			};
			partGameObject.transform.SetParent(this.gameObject.transform, false);
			return partGameObject;
		}

		private void RecreateTilePart(GameObject part, QuadMultiMeshBuilder meshBuilder)
		{
			Require.That(part != null);
			Require.That(meshBuilder != null);

			if (meshBuilder.Count == 0)
			{
				return;
			}

			var filter = part.GetOrAddComponent<MeshFilter>();
			filter.sharedMesh = meshBuilder.SingleOrEmpty().BuildMesh(filter.sharedMesh ?? new Mesh());
			meshBuilder.Clear();

			var renderer = part.GetOrAddComponent<MeshRenderer>();
			renderer.material = this.TileMaterial;
			renderer.shadowCastingMode = ShadowCastingMode.TwoSided;
			renderer.receiveShadows = true;
		}
	}
}
