﻿using UnityEngine;

namespace Tilescape.Engine.Components
{
	public abstract class AbstractTileShapeComponent : MonoBehaviour
	{
		private static readonly QuadMultiMeshBuilder MeshBuilder = new QuadMultiMeshBuilder();

		private TileShapeProjection shapeProjection = null;

		private TileShapeData tileShape = TileShapeData.Unassigned;

		[SerializeField]
		private Material tileMaterial = null;

		public Material TileMaterial
		{
			get => this.tileMaterial;
			set
			{
				this.tileMaterial = value;
				this.RecreateTile();
			}
		}

		public TileShapeData TileShape
		{
			get => this.tileShape;
			set
			{
				if (value == this.tileShape)
				{
					return;
				}

				this.tileShape = value;
				this.RecreateTile();
			}
		}

		public void SetTileShape(ITileShapeView shape, ITileColorPalette colors, OrthoAxis rotation = OrthoAxis.PosY000Norm_24) =>
			this.TileShape = new TileShapeData(shape, colors, rotation);

		public void Clear()
		{
			this.TileShape = TileShapeData.Unassigned;
			this.ClearMeshes();
		}

		protected abstract void ClearMeshes();

		protected abstract void RecreateTile(TileShapeProjection tileProjection, QuadMultiMeshBuilder meshBuilder);

		private void RecreateTile()
		{
			if (!this.TileShape.IsAssigned || this.TileMaterial == null)
			{
				return;
			}

			int tileSizeExponent = this.TileShape.Shape.Voxels.SizeExponent;
			if (this.shapeProjection == null || this.shapeProjection.SizeExponent != tileSizeExponent)
			{
				this.shapeProjection = new TileShapeProjection(new TileConfig(tileSizeExponent));
			}

			this.ClearMeshes();
			this.shapeProjection.SetProjection(this.TileShape);
			this.RecreateTile(this.shapeProjection, MeshBuilder);
			MeshBuilder.Clear();
			this.shapeProjection.Clear();
		}
	}
}
