﻿using UnityEditor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[CustomEditor(typeof(TileColorPaletteAtlasImporterComponent))]
	public class TileColorPaletteAtlasImporterComponentInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			this.DrawDefaultInspector();
			var atlasImporter = (TileColorPaletteAtlasImporterComponent)this.target;

			if (GUILayout.Button("Update Tile Shape Atlas"))
			{
				atlasImporter.UpdateAtlas();
			}

			if (GUILayout.Button("Rebuild Tile Shape Atlas"))
			{
				atlasImporter.RebuildAtlas();
			}

			if (GUILayout.Button("Delete Tile Shape Atlas"))
			{
				atlasImporter.DeleteAtlas();
			}
		}
	}
}
