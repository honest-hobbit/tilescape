﻿using UnityEngine;

namespace Tilescape.Engine.Components
{
	[ExecuteInEditMode]
	public class TileColorPaletteAtlasImporterComponent : AbstractAtlasImporterComponent<TileColorPalette, TileColorPaletteAtlasRecord>
	{
		protected override FileSystemAtlasImporter<TileColorPalette, TileColorPaletteAtlasRecord> CreateImporter(
			TilescapeStoreComponent store, string atlasFolderPath) =>
			new FileSystemAtlasImporter<TileColorPalette, TileColorPaletteAtlasRecord>(
				new TileColorPaletteAtlasImporter(store.Store, atlasFolderPath));
	}
}
