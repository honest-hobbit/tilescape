﻿using UnityEditor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[CustomEditor(typeof(TileShapeAtlasImporterComponent))]
	public class TileShapeAtlasImporterComponentInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			this.DrawDefaultInspector();
			var atlasImporter = (TileShapeAtlasImporterComponent)this.target;

			if (GUILayout.Button("Update Tile Shape Atlas"))
			{
				atlasImporter.UpdateAtlas();
			}

			if (GUILayout.Button("Rebuild Tile Shape Atlas"))
			{
				atlasImporter.RebuildAtlas();
			}

			if (GUILayout.Button("Delete Tile Shape Atlas"))
			{
				atlasImporter.DeleteAtlas();
			}
		}
	}
}
