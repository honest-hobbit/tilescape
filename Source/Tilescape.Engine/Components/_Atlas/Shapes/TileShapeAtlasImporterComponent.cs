﻿using UnityEngine;

namespace Tilescape.Engine.Components
{
	[ExecuteInEditMode]
	public class TileShapeAtlasImporterComponent : AbstractAtlasImporterComponent<TileShape, TileShapeAtlasRecord>
	{
		protected override FileSystemAtlasImporter<TileShape, TileShapeAtlasRecord> CreateImporter(
			TilescapeStoreComponent store, string atlasFolderPath) =>
			new FileSystemAtlasImporter<TileShape, TileShapeAtlasRecord>(
				new TileShapeAtlasImporter(store.Store, atlasFolderPath));
	}
}
