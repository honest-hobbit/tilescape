﻿using System;
using System.Diagnostics;
using System.IO;
using Tilescape.Utility.IO;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	public class TileShapeEditorComponent : MonoBehaviour
	{
		public TileShapeAtlasEditorComponent Atlas { get; set; }

		public TileShape Shape { get; set; }

		public string ShapeFilePath { get; set; }

		public void OpenFolder()
		{
			this.ValidatePath();
			using (Process.Start(Path.GetDirectoryName(this.ShapeFilePath)))
			{
			}
		}

		public void OpenInMagicaVoxel()
		{
			if (this.Atlas == null)
			{
				throw new ArgumentNullException(nameof(this.Atlas), "Must be not null.");
			}

			this.ValidatePath();
			using (Process.Start(this.Atlas.MagicaVoxelExecutablePath, $"\"{this.ShapeFilePath}\""))
			{
			}
		}

		private void ValidatePath()
		{
			if (!PathUtility.IsPathValid(this.ShapeFilePath))
			{
				throw new ArgumentException(
					"Must be a valid path including file name and extension.", nameof(this.ShapeFilePath));
			}
		}
	}
}
