﻿using Tilescape.Utility.Unity.Editor;
using UnityEditor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[CustomEditor(typeof(TileShapeEditorComponent))]
	public class TileShapeEditorComponentInspector : Editor
	{
		private static readonly float LabelScale = .3f;

		public override void OnInspectorGUI()
		{
			var shapeEditor = (TileShapeEditorComponent)this.target;

			InspectorUtility.SelectableTextFieldWithLabel("Name", shapeEditor.Shape.Name, LabelScale);
			InspectorUtility.SelectableTextFieldWithLabel("Key", shapeEditor.Shape.Key.ToString(), LabelScale);
			InspectorUtility.SelectableTextFieldWithLabel("Path", shapeEditor.ShapeFilePath, LabelScale);

			if (GUILayout.Button("Open Folder"))
			{
				shapeEditor.OpenFolder();
			}

			if (GUILayout.Button("Open In MagicaVoxel"))
			{
				shapeEditor.OpenInMagicaVoxel();
			}
		}
	}
}
