﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Indexing.Utility;
using Tilescape.Utility.Mathematics;
using UniRx;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(TileShapeAtlasImporterComponent))]
	public class TileShapeAtlasEditorComponent : MonoBehaviour
	{
		private static ITileColorPalette Colors => MagicalVoxelTileColorPalette.Default;

		private readonly Dictionary<Guid, GameObject> shapes = new Dictionary<Guid, GameObject>(Equality.StructComparer<Guid>());

		private IEnumerator<Index2D> spiral = new Spiral2D().GetEnumerator();

		private TileShapeAtlasImporterComponent importer;

		[SerializeField]
		private string magicaVoxelPath = null;

		[SerializeField]
		private float spacing = 2;

		[SerializeField]
		private Material tileMaterial = null;

		public string MagicaVoxelExecutablePath => Path.GetFullPath(Path.Combine(Application.dataPath, this.magicaVoxelPath));

		private void OnValidate()
		{
			this.spacing = this.spacing.ClampLower(1);
		}

		private void Awake()
		{
			this.importer = this.GetComponent<TileShapeAtlasImporterComponent>();
			foreach (var record in this.importer.Importer.Records.Values.OrderBy(x => x.Shape.Name))
			{
				this.CreateShape(record);
			}

			this.importer.Importer.Added.Subscribe(this.CreateShape);
			this.importer.Importer.Updated.Subscribe(this.CreateOrUpdateShape);
			this.importer.Importer.Removed.Subscribe(this.DeleteShape);
			this.importer.Importer.PassCompleted.Subscribe(x => this.SortTileShapes());
		}

		private void CreateShape(TileShapeAtlasRecord record)
		{
			Require.That(record != null);

			var shapeObject = new GameObject(record.Shape.Name)
			{
				hideFlags = HideFlags.DontSaveInEditor
			};
			shapeObject.transform.parent = this.transform;
			shapeObject.transform.localPosition = this.GetNextPosition();

			var shape = shapeObject.AddComponent<TileShapeComponent>();
			shape.TileMaterial = this.tileMaterial;
			shape.SetTileShape(record.Shape, Colors);

			var editor = shapeObject.AddComponent<TileShapeEditorComponent>();
			editor.Atlas = this;
			editor.ShapeFilePath = record.ShapeFilePath;
			editor.Shape = record.Shape;

			this.shapes[record.Key] = shapeObject;
		}

		private void CreateOrUpdateShape(TileShapeAtlasRecord record)
		{
			Require.That(record != null);

			if (this.shapes.TryGetValue(record.Key, out var shapeObject))
			{
				if (shapeObject.name != record.Shape.Name)
				{
					shapeObject.name = record.Shape.Name;
				}

				shapeObject.GetComponent<TileShapeComponent>().SetTileShape(record.Shape, Colors);
				var editor = shapeObject.GetComponent<TileShapeEditorComponent>();
				editor.ShapeFilePath = record.ShapeFilePath;
				editor.Shape = record.Shape;
			}
			else
			{
				this.CreateShape(record);
			}
		}

		private void DeleteShape(TileShape shape)
		{
			Require.That(shape != null);

			if (this.shapes.TryGetValue(shape.Key, out var shapeObject))
			{
				this.shapes.Remove(shape.Key);
				DestroyImmediate(shapeObject);
			}
		}

		private Vector3 GetNextPosition()
		{
			this.spiral.MoveNext();
			var position = this.spiral.Current;
			return new Vector3(position.X, 0, position.Y) * this.spacing;
		}

		private void SortTileShapes()
		{
			this.spiral = new Spiral2D().GetEnumerator();
			foreach (var shapeObject in this.shapes.Values.OrderBy(x => x.name))
			{
				shapeObject.transform.localPosition = this.GetNextPosition();
			}
		}
	}
}
