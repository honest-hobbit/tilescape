﻿using System;
using System.IO;
using Tilescape.Utility.Concurrency;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;
using UniRx;
using UnityEditor;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	public class FileSystemAtlasImporter<TValue, TRecord> : AbstractDisposable
		where TValue : IKeyed<Guid>, INamed
		where TRecord : AtlasRecord<TValue>
	{
		private readonly ConcurrentBool updateInProgress = new ConcurrentBool(false);

		private FileSystemWatcher watcher;

		internal FileSystemAtlasImporter(IAtlasImporter<TValue, TRecord> importer)
		{
			Require.That(importer != null);
			Require.That(!importer.IsDisposed);

			this.Importer = importer;

			this.Importer.Added.Subscribe(x => Debug.Log($"{typeof(TValue).Name} {x.Value.Name} added."));
			this.Importer.Updated.Subscribe(x => Debug.Log($"{typeof(TValue).Name} {x.Value.Name} updated."));
			this.Importer.Removed.Subscribe(x => Debug.Log($"{typeof(TValue).Name} {x.Name} removed."));
			this.Importer.Error.Subscribe(error => Debug.LogException(error));
			this.Importer.UpdateAtlas();

			this.watcher = new FileSystemWatcher()
			{
				Path = this.Importer.SourceFolderPath,
				Filter = string.Empty,
				IncludeSubdirectories = true,
				NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName
			};

			this.watcher.Created += (sender, e) => this.UpdateAtlasFromFileSystem();
			this.watcher.Changed += (sender, e) => this.UpdateAtlasFromFileSystem();
			this.watcher.Renamed += (sender, e) => this.UpdateAtlasFromFileSystem();
			this.watcher.Deleted += (sender, e) => this.UpdateAtlasFromFileSystem();

			this.watcher.EnableRaisingEvents = true;
		}

		internal IAtlasImporter<TValue, TRecord> Importer { get; }

		/// <inheritdoc />
		protected override void ManagedDisposal()
		{
			this.watcher.Dispose();
		}

		private void UpdateAtlasFromFileSystem()
		{
			if (this.updateInProgress.TryToggleIfFalse())
			{
				// delayCall is used to get the atlas update back onto the main thread from whatever background thread
				// the file system watcher event was raised on
				EditorApplication.delayCall += () =>
				{
					this.Importer.UpdateAtlas();
					this.updateInProgress.SetFalse();
				};
			}
		}
	}
}
