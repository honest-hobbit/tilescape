﻿using System;
using System.IO;
using Tilescape.Utility.IO;
using Tilescape.Utility.Types;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[ExecuteInEditMode]
	public abstract class AbstractAtlasImporterComponent<TValue, TRecord> : MonoBehaviour
		where TValue : IKeyed<Guid>, INamed
		where TRecord : AtlasRecord<TValue>
	{
		private FileSystemAtlasImporter<TValue, TRecord> importer;

		[SerializeField]
		private TilescapeStoreComponent store = null;

		[SerializeField]
		private string sourcePath = string.Empty;

		internal IAtlasImporter<TValue, TRecord> Importer
		{
			get
			{
				this.Initialize();
				return this.importer.Importer;
			}
		}

		public void UpdateAtlas()
		{
			this.Initialize();
			this.importer.Importer.UpdateAtlas();
		}

		public void RebuildAtlas()
		{
			this.Initialize();
			this.importer.Importer.RebuildAtlas();
		}

		public void DeleteAtlas()
		{
			this.Initialize();
			this.importer.Importer.DeleteAtlas();
		}

		protected abstract FileSystemAtlasImporter<TValue, TRecord> CreateImporter(TilescapeStoreComponent store, string atlasFolderPath);

		private void OnDestroy()
		{
			this.importer?.Dispose();
		}

		private void Initialize()
		{
			if (this.importer != null)
			{
				return;
			}

			if (this.store == null)
			{
				throw new ArgumentNullException(nameof(this.store));
			}

			var fullSourcePath = Path.GetFullPath(Path.Combine(Application.dataPath, this.sourcePath));
			if (!PathUtility.IsPathValid(fullSourcePath))
			{
				throw new ArgumentException("Must be a valid path.", nameof(this.sourcePath));
			}

			this.importer = this.CreateImporter(this.store, fullSourcePath);
		}
	}
}
