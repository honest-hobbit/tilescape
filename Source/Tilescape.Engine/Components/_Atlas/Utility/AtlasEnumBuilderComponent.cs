﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using UniRx;
using UnityEngine;

namespace Tilescape.Engine.Components
{
	[ExecuteInEditMode]
	public class AtlasEnumBuilderComponent : MonoBehaviour
	{
		private readonly AtlasEnumBuilder enumBuilder = new AtlasEnumBuilder();

		[SerializeField]
		private string enumFilePath = string.Empty;

		[SerializeField]
		private string enumFileNamespace = string.Empty;

		[SerializeField]
		private string enumFileClassName = string.Empty;

		[SerializeField]
		private AtlasEnumBuilder.SpaceHandling enumSpaceHandling = AtlasEnumBuilder.SpaceHandling.RemoveSpacesKeepUnderscores;

		private string FullEnumFilePath => Path.GetFullPath(Path.Combine(Application.dataPath, this.enumFilePath));

		private void Awake()
		{
			if (this.TrySubscribe(this.GetComponent<TileShapeAtlasImporterComponent>()))
			{
				return;
			}

			if (this.TrySubscribe(this.GetComponent<TileColorPaletteAtlasImporterComponent>()))
			{
				return;
			}
		}

		private bool TrySubscribe<TValue, TRecord>(AbstractAtlasImporterComponent<TValue, TRecord> importer)
			where TValue : IKeyed<Guid>, INamed
			where TRecord : AtlasRecord<TValue>
		{
			if (importer == null)
			{
				return false;
			}

			importer.Importer.PassCompleted.Subscribe(
				x => this.RebuildEnumFile(x.Values.Select(y => KeyValuePair.New(y.Key, y.Value.Name))));

			this.RebuildEnumFile(importer.Importer.Records.Values.Select(y => KeyValuePair.New(y.Key, y.Value.Name)));
			return true;
		}

		private void RebuildEnumFile(IEnumerable<KeyValuePair<Guid, string>> values)
		{
			Require.That(values != null);

			if (this.enumFilePath.IsNullOrWhiteSpace())
			{
				return;
			}

			this.enumBuilder.Namespace = this.enumFileNamespace;
			this.enumBuilder.ClassName = this.enumFileClassName;
			this.enumBuilder.Spacing = this.enumSpaceHandling;

			var path = this.FullEnumFilePath;
			Directory.CreateDirectory(Path.GetDirectoryName(path));

			this.enumBuilder.CreateFile(path, values);
		}
	}
}
