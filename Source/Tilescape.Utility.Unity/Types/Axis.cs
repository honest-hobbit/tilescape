﻿namespace Tilescape.Utility.Unity.Types
{
	public enum Axis
	{
		X = 0,

		Y = 1,

		Z = 2
	}
}
