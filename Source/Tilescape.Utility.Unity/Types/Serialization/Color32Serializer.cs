﻿using Tilescape.Utility.Serialization;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types.Serialization
{
	public static class Color32Serializer
	{
		public static IUniformSerDes<Color32> RGBA => Color32RGBASerializer.Instance;

		public static IUniformSerDes<Color32> RGB => Color32RGBSerializer.Instance;
	}
}
