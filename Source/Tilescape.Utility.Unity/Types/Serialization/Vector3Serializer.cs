﻿using Tilescape.Utility.Serialization;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types.Serialization
{
	public class Vector3Serializer : AbstractCompositeUniformSerializer<Vector3, float, float, float>
	{
		public Vector3Serializer(IUniformSerDes<float> serializer)
			: base(serializer, serializer, serializer)
		{
		}

		public Vector3Serializer(
			IUniformSerDes<float> xSerializer, IUniformSerDes<float> ySerializer, IUniformSerDes<float> zSerializer)
			: base(xSerializer, ySerializer, zSerializer)
		{
		}

		public static IUniformSerDes<Vector3> Instance { get; } = new Vector3Serializer(Serializer.Float);

		/// <inheritdoc />
		protected override Vector3 ComposeValue(float x, float y, float z) => new Vector3(x, y, z);

		/// <inheritdoc />
		protected override void DecomposeValue(Vector3 value, out float x, out float y, out float z)
		{
			x = value.x;
			y = value.y;
			z = value.z;
		}
	}
}
