﻿using Tilescape.Utility.Serialization;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types.Serialization
{
	public class Color32RGBASerializer : AbstractCompositeUniformSerializer<Color32, byte, byte, byte, byte>
	{
		public Color32RGBASerializer(IUniformSerDes<byte> serializer)
			: base(serializer, serializer, serializer, serializer)
		{
		}

		public Color32RGBASerializer(
			IUniformSerDes<byte> redSerializer,
			IUniformSerDes<byte> greenSerializer,
			IUniformSerDes<byte> blueSerializer,
			IUniformSerDes<byte> alphaSerializer)
			: base(redSerializer, greenSerializer, blueSerializer, alphaSerializer)
		{
		}

		public static IUniformSerDes<Color32> Instance { get; } = new Color32RGBASerializer(Serializer.Byte);

		/// <inheritdoc />
		protected override Color32 ComposeValue(byte red, byte green, byte blue, byte alpha) => new Color32(red, green, blue, alpha);

		/// <inheritdoc />
		protected override void DecomposeValue(Color32 value, out byte red, out byte green, out byte blue, out byte alpha)
		{
			red = value.r;
			green = value.g;
			blue = value.b;
			alpha = value.a;
		}
	}
}
