﻿using Tilescape.Utility.Serialization;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types.Serialization
{
	public class Vector4Serializer : AbstractCompositeUniformSerializer<Vector4, float, float, float, float>
	{
		public Vector4Serializer(IUniformSerDes<float> serializer)
			: base(serializer, serializer, serializer, serializer)
		{
		}

		public Vector4Serializer(
			IUniformSerDes<float> xSerializer,
			IUniformSerDes<float> ySerializer,
			IUniformSerDes<float> zSerializer,
			IUniformSerDes<float> wSerializer)
			: base(xSerializer, ySerializer, zSerializer, wSerializer)
		{
		}

		public static IUniformSerDes<Vector4> Instance { get; } = new Vector4Serializer(Serializer.Float);

		/// <inheritdoc />
		protected override Vector4 ComposeValue(float x, float y, float z, float w) => new Vector4(x, y, z, w);

		/// <inheritdoc />
		protected override void DecomposeValue(Vector4 value, out float x, out float y, out float z, out float w)
		{
			x = value.x;
			y = value.y;
			z = value.z;
			w = value.w;
		}
	}
}
