﻿using Tilescape.Utility.Serialization;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types.Serialization
{
	public class Vector2Serializer : AbstractCompositeUniformSerializer<Vector2, float, float>
	{
		public Vector2Serializer(IUniformSerDes<float> serializer)
			: base(serializer, serializer)
		{
		}

		public Vector2Serializer(IUniformSerDes<float> xSerializer, IUniformSerDes<float> ySerializer)
			: base(xSerializer, ySerializer)
		{
		}

		public static IUniformSerDes<Vector2> Instance { get; } = new Vector2Serializer(Serializer.Float);

		/// <inheritdoc />
		protected override Vector2 ComposeValue(float x, float y) => new Vector2(x, y);

		/// <inheritdoc />
		protected override void DecomposeValue(Vector2 value, out float x, out float y)
		{
			x = value.x;
			y = value.y;
		}
	}
}
