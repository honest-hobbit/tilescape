﻿using Tilescape.Utility.Serialization;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types.Serialization
{
	public class Color32RGBSerializer : AbstractCompositeUniformSerializer<Color32, byte, byte, byte>
	{
		private readonly byte alpha;

		public Color32RGBSerializer(IUniformSerDes<byte> serializer, byte alpha = byte.MaxValue)
			: base(serializer, serializer, serializer)
		{
			this.alpha = alpha;
		}

		public Color32RGBSerializer(
			IUniformSerDes<byte> redSerializer,
			IUniformSerDes<byte> greenSerializer,
			IUniformSerDes<byte> blueSerializer,
			byte alpha = byte.MaxValue)
			: base(redSerializer, greenSerializer, blueSerializer)
		{
			this.alpha = alpha;
		}

		public static IUniformSerDes<Color32> Instance { get; } = new Color32RGBSerializer(Serializer.Byte);

		/// <inheritdoc />
		protected override Color32 ComposeValue(byte red, byte green, byte blue) => new Color32(red, green, blue, this.alpha);

		/// <inheritdoc />
		protected override void DecomposeValue(Color32 value, out byte red, out byte green, out byte blue)
		{
			red = value.r;
			green = value.g;
			blue = value.b;
		}
	}
}
