﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Pooling;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types
{
	public static class GameObjectPool
	{
		public static void Give(this IPool<GameObject> pool, GameObject value)
		{
			Require.That(pool != null);
			Require.That(value != null);

			if (!pool.TryGive(value))
			{
				UnityEngine.Object.Destroy(value);
			}
		}

		public static IPool<GameObject> Create(
			Func<GameObject> factory,
			Action<GameObject> initialize = null,
			Action<GameObject> reset = null,
			int boundedCapacity = Capacity.Unbounded)
		{
			Require.That(factory != null);
			Require.That(boundedCapacity >= 0 || boundedCapacity == Capacity.Unbounded);

			return new Pool<GameObject>(
				factory,
				value =>
				{
					value.SetActive(true);
					value.hideFlags = HideFlags.None;
					initialize?.Invoke(value);
				},
				value =>
				{
					Require.That(value != null);

					reset?.Invoke(value);
					value.SetActive(false);
					value.transform.parent = null;
					value.transform.position = Vector3.zero;
					value.transform.localScale = Vector3.one;
					value.transform.Rotate(0, 0, 0);
					value.hideFlags = HideFlags.HideInHierarchy;
				},
				boundedCapacity);
		}
	}
}
