﻿using Tilescape.Utility.Indexing.Indices;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types
{
	public static class IndexExtensions
	{
		public static Vector2 ToVector(this Index2D index) => new Vector2(index.X, index.Y);

		public static Vector3 ToVector(this Index3D index) => new Vector3(index.X, index.Y, index.Z);

		public static Vector4 ToVector(this Index4D index) => new Vector4(index.X, index.Y, index.Z, index.W);
	}
}
