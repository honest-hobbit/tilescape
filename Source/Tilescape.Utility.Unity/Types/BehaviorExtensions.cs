﻿using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types
{
	public static class BehaviorExtensions
	{
		public static void ToggleEnabled(this Behaviour behavior)
		{
			Require.That(behavior != null);

			behavior.enabled = !behavior.enabled;
		}
	}
}
