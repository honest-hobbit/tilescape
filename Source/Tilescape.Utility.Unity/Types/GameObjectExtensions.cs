﻿using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types
{
	public static class GameObjectExtensions
	{
		public static T GetOrAddComponent<T>(this GameObject gameObject)
			where T : Component
		{
			Require.That(gameObject != null);

			var component = gameObject.GetComponent<T>();
			if (component == null)
			{
				component = gameObject.AddComponent<T>();
			}

			return component;
		}

		public static void Destroy<T>(this GameObject gameObject)
			where T : Component
		{
			Require.That(gameObject != null);

			var component = gameObject.GetComponent<T>();
			if (component != null)
			{
				Object.Destroy(component);
			}
		}

		public static void ToggleActive(this GameObject gameObject)
		{
			Require.That(gameObject != null);

			gameObject.SetActive(!gameObject.activeSelf);
		}
	}
}
