﻿using Tilescape.Utility.Mathematics;
using UnityEngine;

namespace Tilescape.Utility.Unity.Types
{
	public static class VectorExtensions
	{
		public static Vector2 Abs(this Vector2 vec) => new Vector2(vec.x.Abs(), vec.y.Abs());

		public static Vector3 Abs(this Vector3 vec) => new Vector3(vec.x.Abs(), vec.y.Abs(), vec.z.Abs());

		public static Vector4 Abs(this Vector4 vec) => new Vector4(vec.x.Abs(), vec.y.Abs(), vec.z.Abs(), vec.w.Abs());
	}
}
