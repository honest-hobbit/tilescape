﻿using UnityEngine;

namespace Tilescape.Utility.Unity.Types
{
	public static class Color32Extensions
	{
		public static bool IsOpaque(this Color32 color) => color.a == byte.MaxValue;

		public static bool IsInvisible(this Color32 color) => color.a == byte.MinValue;

		public static bool IsVisible(this Color32 color) => color.a != byte.MinValue;

		public static bool IsTransparent(this Color32 color) => color.a != byte.MaxValue;

		public static bool IsSemiTransparent(this Color32 color) => color.a != byte.MaxValue && color.a != byte.MinValue;

		public static bool IsEqualTo(this Color32 color, Color32 other) =>
			color.r == other.r && color.g == other.g && color.b == other.b && color.a == other.a;

		public static bool IsRGBEqualTo(this Color32 color, Color32 other) =>
			color.r == other.r && color.g == other.g && color.b == other.b;
	}
}
