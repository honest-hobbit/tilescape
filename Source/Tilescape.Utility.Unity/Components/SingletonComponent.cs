﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Unity.Types;
using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public class SingletonComponent : AbstractSingletonComponent<SingletonComponent>
	{
		public T Of<T>()
			where T : Component
		{
			return this.gameObject.GetOrAddComponent<T>();
		}

		public T Of<T>(Func<T, bool> where)
			where T : Component
		{
			Require.That(where != null);

			var components = this.gameObject.GetComponents<T>();
			for (int index = 0; index < components.Length; index++)
			{
				if (where(components[index]))
				{
					return components[index];
				}
			}

			throw new InvalidOperationException("No matching component found.");
		}

		public T Of<T>(Func<T, bool> where, Action<T> initialize)
			where T : Component
		{
			Require.That(where != null);
			Require.That(initialize != null);

			var components = this.gameObject.GetComponents<T>();
			for (int index = 0; index < components.Length; index++)
			{
				if (where(components[index]))
				{
					return components[index];
				}
			}

			var result = this.gameObject.AddComponent<T>();
			initialize(result);
			return result;
		}
	}
}
