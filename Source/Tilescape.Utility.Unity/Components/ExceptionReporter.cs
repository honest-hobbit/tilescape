﻿using System;
using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public class ExceptionReporter : MonoBehaviour
	{
		static ExceptionReporter()
		{
			AppDomain.CurrentDomain.UnhandledException += (sender, args) => Debug.LogError(args.ExceptionObject);
		}
	}
}
