﻿using Tilescape.Utility.Mathematics;
using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public class OrthographicCamera : MonoBehaviour
	{
		public enum Projection
		{
			Overhead, SideScrolling, Both
		}

		[SerializeField]
		private GameObject follow = null;

		[SerializeField]
		private bool initiallyOverhead = true;

		[SerializeField]
		private Projection projection = Projection.Both;

		[SerializeField]
		private float cameraDistance = 20;

		[SerializeField]
		private float initialZoomDistance = 0;

		[SerializeField]
		private float minZoomDistance = 1;

		[SerializeField]
		private float maxZoomDistance = 100;

		[SerializeField]
		private float zoomAmount = 1;

		[SerializeField]
		private float zoomSpeed = 1;

		[Range(0, 360)]
		[SerializeField]
		private float initialRotation = 0;

		[Range(0, 360)]
		[SerializeField]
		private float rotateAmount = 45;

		[SerializeField]
		private float rotateSpeed = 1;

		[SerializeField]
		private float toggleVerticalSpeed = 1;

		private new Camera camera;

		private bool overhead;

		private float height;

		private float targetZoom;

		private float rotation;

		private float targetRotation;

		private float GetTargetHeight()
		{
			return this.overhead ? 1 : 0;
		}

		private void OnValidate()
		{
			if (this.projection == Projection.Overhead)
			{
				this.initiallyOverhead = true;
			}

			if (this.projection == Projection.SideScrolling)
			{
				this.initiallyOverhead = false;
			}

			if (this.cameraDistance <= 0)
			{
				this.cameraDistance = 1;
			}

			if (this.minZoomDistance <= 0)
			{
				this.minZoomDistance = 1;
			}

			if (this.maxZoomDistance < this.minZoomDistance)
			{
				this.maxZoomDistance = this.minZoomDistance;
			}

			if (this.zoomAmount <= 0)
			{
				this.zoomAmount = 1;
			}

			if (this.zoomSpeed <= 0)
			{
				this.zoomSpeed = 1;
			}

			if (this.rotateSpeed <= 0)
			{
				this.rotateSpeed = 1;
			}

			if (this.toggleVerticalSpeed <= 0)
			{
				this.toggleVerticalSpeed = 1;
			}
		}

		private void Start()
		{
			this.targetRotation = this.initialRotation;
			this.overhead = this.initiallyOverhead;
			this.height = this.GetTargetHeight();

			this.initialZoomDistance = this.initialZoomDistance.Clamp(this.minZoomDistance, this.maxZoomDistance);
			this.targetZoom = this.initialZoomDistance;

			this.camera = this.GetComponent<Camera>();
			this.camera.orthographicSize = this.initialZoomDistance;
		}

		private void LateUpdate()
		{
			if (this.projection == Projection.Both && Input.GetButtonDown("Toggle Camera"))
			{
				this.overhead = !this.overhead;
			}

			if (Input.GetButtonDown("Rotate Camera"))
			{
				this.targetRotation += this.rotateAmount * (int)Input.GetAxis("Rotate Camera");
			}

			if (this.zoomAmount > 0)
			{
				this.targetZoom += -Input.GetAxis("Mouse ScrollWheel") * this.zoomAmount;
				this.targetZoom = this.targetZoom.Clamp(this.minZoomDistance, this.maxZoomDistance);
				this.camera.orthographicSize = Mathf.Lerp(this.camera.orthographicSize, this.targetZoom, Time.deltaTime * this.zoomSpeed);
			}

			this.height = Mathf.LerpAngle(this.height, this.GetTargetHeight(), Time.deltaTime * this.toggleVerticalSpeed);
			this.rotation = Mathf.LerpAngle(this.rotation, this.targetRotation, Time.deltaTime * this.rotateSpeed);

			var radians = this.rotation * Mathf.Deg2Rad;
			var cameraDirection = new Vector3(Mathf.Cos(radians), this.height, Mathf.Sin(radians));

			this.transform.position = this.follow.transform.position + (cameraDirection * this.cameraDistance);
			this.transform.LookAt(this.follow.transform);
		}
	}
}
