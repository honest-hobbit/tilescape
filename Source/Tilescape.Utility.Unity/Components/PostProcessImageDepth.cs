﻿using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public class PostProcessImageDepth : MonoBehaviour
	{
		[SerializeField]
		private Material material = null;

		private new Camera camera;

		private void Start()
		{
			if (!this.material.shader.isSupported)
			{
				this.enabled = false;
			}

			this.camera = this.GetComponent<Camera>();
			this.camera.depthTextureMode = DepthTextureMode.Depth;
		}

		private void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			// mat is the material which contains the shader
			// we are passing the destination RenderTexture to
			Graphics.Blit(source, destination, this.material);
		}
	}
}
