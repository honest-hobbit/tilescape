﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public abstract class AbstractSingletonComponent<T> : MonoBehaviour
		where T : AbstractSingletonComponent<T>
	{
		private static T instance;

		public static T Instance
		{
			get
			{
				if (instance != null)
				{
					return instance;
				}

				var found = FindObjectsOfType<T>();

				if (found.Length == 0)
				{
					var gameObject = new GameObject(typeof(T).Name + " (Singleton)");
					SetSingletonTo(gameObject.AddComponent<T>());
					return instance;
				}

				if (found.Length == 1)
				{
					SetSingletonTo(found[0]);
					return instance;
				}

				throw new InvalidOperationException(
					"More than 1 instance of " + typeof(T) + " was found. Only a single instance is allowed.");
			}
		}

		protected virtual void Awake()
		{
			if (instance == null)
			{
				SetSingletonTo((T)this);
			}
			else
			{
				if (!instance.EqualsByReferenceNullSafe(this))
				{
					throw new InvalidOperationException(
						"Instance of " + typeof(T) + " was already assigned. Only a single instance is allowed.");
				}
			}
		}

		private static void SetSingletonTo(T value)
		{
			Require.That(value != null);

			if (Application.isPlaying)
			{
				DontDestroyOnLoad(value.gameObject);
			}

			instance = value;
		}
	}
}
