﻿using System.Collections.Generic;
using Tilescape.Utility.Unity.Types;
using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public class ToggleObjects : MonoBehaviour
	{
		[SerializeField]
		private KeyCode toggleKey = KeyCode.None;

		[SerializeField]
		private List<GameObject> objects = null;

		[SerializeField]
		private List<Behaviour> components = null;

		private void Update()
		{
			if (Input.GetKeyDown(this.toggleKey) && this.objects != null)
			{
				foreach (var gameObject in this.objects)
				{
					gameObject.ToggleActive();
				}

				foreach (var component in this.components)
				{
					component.ToggleEnabled();
				}
			}
		}
	}
}
