﻿using Tilescape.Utility.Unity.Types;
using UniRx;
using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public class ObservablePosition : MonoBehaviour
	{
		private readonly ReplaySubject<Vector3> positionChanged = new ReplaySubject<Vector3>(1);

		public ObservablePosition()
		{
			this.PositionChanged = this.positionChanged.DistinctUntilChanged(Vector3Equality.Comparer);
		}

		public UniRx.IObservable<Vector3> PositionChanged { get; }

		private void Update() => this.positionChanged.OnNext(this.transform.position);
	}
}
