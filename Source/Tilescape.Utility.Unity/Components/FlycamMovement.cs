﻿using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public class FlycamMovement : MonoBehaviour
	{
		/*
		EXTENDED FLYCAM
			Desi Quintans (CowfaceGames.com), 17 August 2012.
			Based on FlyThrough.js by Slin (http://wiki.unity3d.com/index.php/FlyThrough), 17 May 2011.

		LICENSE
			Free as in speech, and free as in beer.

		FEATURES
			WASD/Arrows:    Movement
					  E:    Climb
					  Q:    Drop
						  Shift:    Move faster
						Control:    Move slower
							Tab:    Toggle cursor locking to screen (you can also press Ctrl+P to toggle play mode on and off).
		*/

		[SerializeField]
		private float cameraSensitivity = 90;

		[SerializeField]
		private float climbSpeed = 10;

		[SerializeField]
		private float normalMoveSpeed = 10;

		[SerializeField]
		private float slowMoveFactor = 0.25f;

		[SerializeField]
		private float fastMoveFactor = 4;

		private float rotationX = 0.0f;

		private float rotationY = 0.0f;

		private void Start()
		{
			Cursor.lockState = CursorLockMode.Locked;
		}

		private void Update()
		{
			this.rotationX += Input.GetAxis("Mouse X") * this.cameraSensitivity * Time.deltaTime;
			this.rotationY += Input.GetAxis("Mouse Y") * this.cameraSensitivity * Time.deltaTime;
			this.rotationY = Mathf.Clamp(this.rotationY, -90, 90);

			this.transform.localRotation = Quaternion.AngleAxis(this.rotationX, Vector3.up);
			this.transform.localRotation *= Quaternion.AngleAxis(this.rotationY, Vector3.left);

			var moveSpeed = this.normalMoveSpeed;
			var climbSpeed = this.climbSpeed;

			if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
			{
				moveSpeed *= this.fastMoveFactor;
				climbSpeed *= this.fastMoveFactor;
			}
			else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
			{
				moveSpeed *= this.slowMoveFactor;
				climbSpeed *= this.slowMoveFactor;
			}

			this.transform.position += this.transform.forward * moveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
			this.transform.position += this.transform.right * moveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;

			if (Input.GetKey(KeyCode.E))
			{
				this.transform.position += this.transform.up * climbSpeed * Time.deltaTime;
			}
			else if (Input.GetKey(KeyCode.Q))
			{
				this.transform.position -= this.transform.up * climbSpeed * Time.deltaTime;
			}

			if (Input.GetKeyDown(KeyCode.Tab))
			{
				Cursor.lockState = (Cursor.lockState == CursorLockMode.Locked) ? CursorLockMode.None : CursorLockMode.Locked;
			}
		}
	}
}
