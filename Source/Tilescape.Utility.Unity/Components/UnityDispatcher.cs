﻿using System;
using System.Collections;
using System.Diagnostics;
using Tilescape.Utility.Concurrency;
using Tilescape.Utility.Mathematics;
using UnityEngine;

namespace Tilescape.Utility.Unity.Components
{
	public class UnityDispatcher : MonoBehaviour
	{
		private const int FallbackFrameRateMin = 1;

		private const int FallbackFrameRateMax = 120;

		private const float PercentMin = 0;

		private const float PercentMax = 1;

		private readonly IDispatchProcessor dispatcher = new DispatchProcessor();

		private readonly Stopwatch frameTimer = new Stopwatch();

		[Range(FallbackFrameRateMin, FallbackFrameRateMax)]
		[SerializeField]
		private int fallbackFrameRate = 60;

		// a percentage to scale back the expected extra time by to help improve the framerate in case of performance spikes
		[Range(PercentMin, PercentMax)]
		[SerializeField]
		private double refrain = .7;

		[Range(PercentMin, PercentMax)]
		[SerializeField]
		private double nextFrameWeight = .5;

		private double averageFrameTime;

		private double targetTimePerFrame;

		public IDispatcher Dispatcher => this.dispatcher.Dispatcher;

		private void Start()
		{
			int targetFrameRate = Application.targetFrameRate;
			if (targetFrameRate <= 0)
			{
				targetFrameRate = this.fallbackFrameRate.Clamp(FallbackFrameRateMin, FallbackFrameRateMax);
			}

			this.targetTimePerFrame = 1d / targetFrameRate;
			this.refrain = this.refrain.Clamp(PercentMin, PercentMax);
			this.nextFrameWeight = this.nextFrameWeight.Clamp(PercentMin, PercentMax);

			this.StartCoroutine(this.RunDispatcher());
		}

		private void Update()
		{
			bool isTimerRunning = this.frameTimer.IsRunning;
			if (isTimerRunning)
			{
				this.frameTimer.Stop();
			}

			var invokeTime = ((this.targetTimePerFrame - this.averageFrameTime) * this.refrain).ClampLower(0);
			this.dispatcher.InvokeMany(TimeSpan.FromSeconds(invokeTime));

			if (isTimerRunning)
			{
				this.frameTimer.Start();
			}
		}

		private void FixedUpdate()
		{
			if (!this.frameTimer.IsRunning)
			{
				this.frameTimer.Reset();
				this.frameTimer.Start();
			}
		}

		private IEnumerator RunDispatcher()
		{
			var previousFrameWeight = 1d - this.nextFrameWeight;
			var endOfFrame = new WaitForEndOfFrame();

			while (true)
			{
				yield return endOfFrame;
				if (this.frameTimer.IsRunning)
				{
					this.frameTimer.Stop();
					this.averageFrameTime =
						(this.frameTimer.Elapsed.TotalSeconds * this.nextFrameWeight) + (this.averageFrameTime * previousFrameWeight);
				}
			}
		}
	}
}
