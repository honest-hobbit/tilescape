﻿using System;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Indexing.Shapes;
using Tilescape.Utility.Mathematics;
using UniRx;
using UnityEngine;

namespace Tilescape.Utility.Unity.Interests
{
	/// <summary>
	/// Provides extension methods for converting an <see cref="UniRx.IObservable{Vector3}"/> to <see cref="UniRx.IObservable{Index3D}"/>.
	/// </summary>
	/// <remarks>
	/// The observable sequence returned by these methods do more than just convert the vectors to indices and
	/// filter out things like multiple duplicate values in a row. It handles throttling the index updates so
	/// they only occur when the vector has moved across an entire cell's worth of distance. This means that
	/// when a vector crosses the border of a cell, it returns a new index as expected, but then if the vector
	/// immediately backs up into the previous cell, it does not immediately return that index and cause the
	/// area of interest to snap back. Instead, the previous index is only returned once the vector travels all
	/// the way back across the previous cell. This throttling prevents the constant snapping back and forth an
	/// area of interest would incur if a vector is straddling the border between two adjacent cells.
	/// </remarks>
	public static class ObservableChunkIndex
	{
		public enum Threshold
		{
			TwoChunks,

			ThreeChunks
		}

		public static Threshold ToThreshold(this int length) => length.IsEven() ? Threshold.TwoChunks : Threshold.ThreeChunks;

		public static UniRx.IObservable<Index3D> ToChunkIndex(this UniRx.IObservable<Vector3> positionChanged, float chunkLength) =>
			positionChanged.ToChunkIndex(new Vector3(chunkLength, chunkLength, chunkLength));

		public static UniRx.IObservable<Index3D> ToChunkIndex(this UniRx.IObservable<Vector3> positionChanged, Vector3 chunkDimensions)
		{
			Require.That(positionChanged != null);
			Require.That(chunkDimensions.x > 0);
			Require.That(chunkDimensions.y > 0);
			Require.That(chunkDimensions.z > 0);

			return positionChanged.Select(vector => new Index3D(
				(int)Math.Floor(vector.x / chunkDimensions.x),
				(int)Math.Floor(vector.y / chunkDimensions.y),
				(int)Math.Floor(vector.z / chunkDimensions.z))).DistinctUntilChanged(Equality.StructComparer<Index3D>());
		}

		public static UniRx.IObservable<Index3D> FilteredThreshold(this UniRx.IObservable<Index3D> chunkIndices, IIndexShape<Index3D> shape)
		{
			Require.That(chunkIndices != null);
			Require.That(shape != null);

			return chunkIndices.FilteredThreshold(
				shape.Dimensions.X.ToThreshold(), shape.Dimensions.Y.ToThreshold(), shape.Dimensions.Z.ToThreshold());
		}

		public static UniRx.IObservable<Index3D> FilteredThreshold(this UniRx.IObservable<Index3D> chunkIndices, Threshold threshold) =>
			chunkIndices.FilteredThreshold(threshold, threshold, threshold);

		public static UniRx.IObservable<Index3D> FilteredThreshold(
			this UniRx.IObservable<Index3D> chunkIndices, Threshold xThreshold, Threshold yThreshold, Threshold zThreshold)
		{
			Require.That(chunkIndices != null);
			Require.That(Enumeration.IsDefined(xThreshold));
			Require.That(Enumeration.IsDefined(yThreshold));
			Require.That(Enumeration.IsDefined(zThreshold));

			var handleAxisX = GetHandleAxis(xThreshold);
			var handleAxisY = GetHandleAxis(yThreshold);
			var handleAxisZ = GetHandleAxis(zThreshold);

			// Accumulator below holds the index that defines the location of the 'active region'
			// - For even sized area of interst -
			// Defining Index: the current top right cell of the 'active region'
			// Active Region: the 2 by 2 region of cells that make up the center of the area of interest
			// - For odd sized area of interst -
			// Defining Index: the center cell of the 'active region'
			// Active Region: the 3 by 3 region of cells that make up the center of the area of interest
			return chunkIndices.Scan((accumulator, index) => new Index3D(
				handleAxisX(accumulator.X, index.X),
				handleAxisY(accumulator.Y, index.Y),
				handleAxisZ(accumulator.Z, index.Z))).DistinctUntilChanged(Equality.StructComparer<Index3D>());
		}

		#region Private Helpers

		private static Func<int, int, int> GetHandleAxis(Threshold threshold) =>
			threshold == Threshold.TwoChunks ? (Func<int, int, int>)HandleAxisEven : HandleAxisOdd;

		/// <summary>
		/// Determines what the index value of the 'active region' is based off of the current active value and
		/// a possible new value. This method operates independently on each axis of the index.
		/// </summary>
		/// <param name="activeValue">
		/// The current value of the particular axis of the top right index of the 'active region'.
		/// </param>
		/// <param name="newValue">The possible new value for the 'active region' index.</param>
		/// <returns>
		/// The value determined to be the top right index of the 'active region' for the particular axis.
		/// This may be a new value or it could stay the same as what was passed into this method.
		/// </returns>
		private static int HandleAxisEven(int activeValue, int newValue)
		{
			if (newValue == activeValue - 1 || newValue == activeValue)
			{
				// if the index is in the 'active region' then don't change the active region index
				return activeValue;
			}
			else
			{
				if (newValue == activeValue - 2)
				{
					// if the index is adjacent to the left hand side
					// then move the active region index over 1 index to the left
					return activeValue - 1;
				}
				else
				{
					if (newValue == activeValue + 1)
					{
						// if the index is adjacent to the right hand side
						// then move the active region index over 1 index to the right
						return activeValue + 1;
					}
					else
					{
						// If the index is not in the active region nor adjacent to either of its sides
						// then completely resynchronize the active region index with the index (don't just
						// shift it over by 1 index). This occurs if the incoming index 'teleported' instead
						// of continuous smooth movement.
						return newValue;
					}
				}
			}
		}

		/// <summary>
		/// Determines what the index value of the 'active region' is based off of the current active value and
		/// a possible new value. This method operates independently on each axis of the index.
		/// </summary>
		/// <param name="activeValue">The current value of the particular axis of the center index of the 'active region'.</param>
		/// <param name="newValue">The possible new value for the 'active region' index.</param>
		/// <returns>
		/// The value determined to be the center index of the 'active region' for the particular axis.
		/// This may be a new value or it could stay the same as what was passed into this method.
		/// </returns>
		private static int HandleAxisOdd(int activeValue, int newValue)
		{
			if (newValue == activeValue + 1 || newValue == activeValue - 1 || newValue == activeValue)
			{
				// if the index is in the 'active region' then don't change the active region index
				return activeValue;
			}
			else
			{
				if (newValue == activeValue - 2)
				{
					// if the index is adjacent to the left hand side
					// then move the active region index over 1 index to the left
					return activeValue - 1;
				}
				else
				{
					if (newValue == activeValue + 2)
					{
						// if the index is adjacent to the right hand side
						// then move the active region index over 1 index to the right
						return activeValue + 1;
					}
					else
					{
						// If the index is not in the active region nor adjacent to either of its sides
						// then completely resynchronize the active region index with the index (don't just
						// shift it over by 1 index). This occurs if the incoming index 'teleported' instead
						// of continuous smooth movement.
						return newValue;
					}
				}
			}
		}

		#endregion
	}
}
