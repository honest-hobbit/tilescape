﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MiscUtil;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public class InterestArea<TKey, TInterest, TPriority> : InterestArea<TKey, TInterest>, IInterestArea<TKey, TInterest, TPriority>
	{
		public InterestArea(
			TInterest interest,
			Func<TKey, TPriority> getPriority,
			UniRx.IObservable<TKey> originChanged,
			UniRx.IObservable<ISetView<TKey>> keysChanged)
			: base(interest, originChanged, keysChanged)
		{
			Require.That(getPriority != null);
			Require.That(originChanged != null);
			Require.That(keysChanged != null);

			this.PrioritiesChanged = originChanged.CombineLatest(
				keysChanged, (origin, keys) => (IDictionaryView<TKey, TPriority>)new PriorityDictionary(origin, keys, getPriority));
		}

		public InterestArea(
			TInterest interest, Func<TKey, TPriority> getPriority, UniRx.IObservable<TKey> originChanged, ISetView<TKey> keys)
			: this(interest, getPriority, originChanged, Observable.Return(keys))
		{
			Require.That(keys != null);
		}

		/// <inheritdoc />
		public UniRx.IObservable<IDictionaryView<TKey, TPriority>> PrioritiesChanged { get; }

		protected class PriorityDictionary : IDictionaryView<TKey, TPriority>
		{
			private readonly TKey origin;

			private readonly ISetView<TKey> relativeKeys;

			private readonly Func<TKey, TPriority> getPriority;

			public PriorityDictionary(TKey origin, ISetView<TKey> relativeKeys, Func<TKey, TPriority> getPriority)
			{
				Require.That(relativeKeys != null);
				Require.That(getPriority != null);

				this.origin = origin;
				this.relativeKeys = relativeKeys;
				this.getPriority = getPriority;
				this.Keys = new OffsetSet(origin, relativeKeys);
				this.Values = CollectionView.From(relativeKeys.Select(key => this.getPriority(key)), relativeKeys.Count);
			}

			/// <inheritdoc />
			public int Count => this.Keys.Count;

			/// <inheritdoc />
			public ISetView<TKey> Keys { get; }

			/// <inheritdoc />
			public ICollectionView<TPriority> Values { get; }

			/// <inheritdoc />
			public TPriority this[TKey key]
			{
				get
				{
					IDictionaryViewContracts.Indexer(this, key);

					return this.getPriority(Operator.Subtract(key, this.origin));
				}
			}

			/// <inheritdoc />
			public bool ContainsKey(TKey key)
			{
				IDictionaryViewContracts.ContainsKey(key);

				return this.Keys.Contains(key);
			}

			/// <inheritdoc />
			public bool TryGetValue(TKey key, out TPriority value)
			{
				IDictionaryViewContracts.TryGetValue(key);

				if (this.ContainsKey(key))
				{
					value = this[key];
					return true;
				}
				else
				{
					value = default(TPriority);
					return false;
				}
			}

			/// <inheritdoc />
			public IEnumerator<KeyValuePair<TKey, TPriority>> GetEnumerator() => this.relativeKeys.Select(
				key => KeyValuePair.New(Operator.Add(key, this.origin), this.getPriority(key))).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}
	}
}
