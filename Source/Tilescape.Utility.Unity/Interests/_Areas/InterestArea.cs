﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Indices;
using Tilescape.Utility.Mathematics;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public static class InterestArea
	{
		public static IInterestArea<Index3D, TInterest> Create<TInterest>(
			TInterest interest, UniRx.IObservable<ISetView<Index3D>> areaChanged, UniRx.IObservable<Index3D> originChanged)
		{
			Require.That(areaChanged != null);
			Require.That(originChanged != null);

			return new InterestArea<Index3D, TInterest>(interest, originChanged, areaChanged);
		}

		public static IInterestArea<Index3D, TInterest> Create<TInterest>(
			TInterest interest, ISetView<Index3D> area, UniRx.IObservable<Index3D> originChanged)
		{
			Require.That(area != null);
			Require.That(originChanged != null);

			return new InterestArea<Index3D, TInterest>(interest, originChanged, area);
		}

		public static class WithPriorities
		{
			public static int FromOffset(Index3D key) =>
				Math.Max(Math.Max(key.X.Abs(), key.Y.Abs()), key.Z.Abs());

			public static IInterestArea<Index3D, TInterest, int> Create<TInterest>(
				TInterest interest, UniRx.IObservable<ISetView<Index3D>> areaChanged, UniRx.IObservable<Index3D> originChanged)
			{
				Require.That(areaChanged != null);
				Require.That(originChanged != null);

				return new InterestArea<Index3D, TInterest, int>(interest, FromOffset, originChanged, areaChanged);
			}

			public static IInterestArea<Index3D, TInterest, int> Create<TInterest>(
				TInterest interest, ISetView<Index3D> area, UniRx.IObservable<Index3D> originChanged)
			{
				Require.That(area != null);
				Require.That(originChanged != null);

				return new InterestArea<Index3D, TInterest, int>(interest, FromOffset, originChanged, area);
			}
		}
	}
}
