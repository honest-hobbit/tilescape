﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MiscUtil;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public class InterestArea<TKey, TInterest> : IInterestArea<TKey, TInterest>
	{
		public InterestArea(TInterest interest, UniRx.IObservable<TKey> originChanged, UniRx.IObservable<ISetView<TKey>> keysChanged)
		{
			Require.That(originChanged != null);
			Require.That(keysChanged != null);

			this.Interest = interest;
			this.KeysChanged = originChanged.CombineLatest(keysChanged, (origin, keys) => (ISetView<TKey>)new OffsetSet(origin, keys));
		}

		public InterestArea(TInterest interest, UniRx.IObservable<TKey> originChanged, ISetView<TKey> keys)
			: this(interest, originChanged, Observable.Return(keys))
		{
			Require.That(keys != null);
		}

		/// <inheritdoc />
		public TInterest Interest { get; }

		/// <inheritdoc />
		public UniRx.IObservable<ISetView<TKey>> KeysChanged { get; }

		protected class OffsetSet : ISetView<TKey>
		{
			private readonly TKey origin;

			private readonly ISetView<TKey> relativeKeys;

			public OffsetSet(TKey origin, ISetView<TKey> relativeKeys)
			{
				Require.That(relativeKeys != null);

				this.origin = origin;
				this.relativeKeys = relativeKeys;
			}

			/// <inheritdoc />
			public int Count => this.relativeKeys.Count;

			/// <inheritdoc />
			public bool Contains(TKey key) => this.relativeKeys.Contains(Operator.Subtract(key, this.origin));

			/// <inheritdoc />
			public IEnumerator<TKey> GetEnumerator() => this.relativeKeys.Select(key => Operator.Add(key, this.origin)).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}
	}
}
