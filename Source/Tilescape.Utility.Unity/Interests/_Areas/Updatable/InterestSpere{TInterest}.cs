﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Indexing.Shapes;

namespace Tilescape.Utility.Unity.Interests
{
	public class InterestSpere<TInterest> : AbstractUpdatableInterestArea3D<TInterest>
	{
		private int radius = 0;

		public InterestSpere(TInterest interest)
			: base(interest, ObservableChunkIndex.Threshold.TwoChunks)
		{
		}

		public int Radius
		{
			get { return this.radius; }

			set
			{
				Require.That(value >= 1);

				if (value == this.radius)
				{
					return;
				}

				this.radius = value;
				this.SetArea(IndexShape.CreateSphere(value * 2));
			}
		}
	}
}
