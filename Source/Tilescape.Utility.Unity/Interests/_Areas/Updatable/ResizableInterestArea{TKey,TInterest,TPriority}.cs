﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Unity.Interests
{
	public class ResizableInterestArea<TKey, TInterest, TPriority> : AbstractUpdatableInterestArea<TKey, TInterest, TPriority>
	{
		private readonly Func<int, ISetView<TKey>> createArea;

		private int radius = 0;

		public ResizableInterestArea(
			TInterest interest,
			Func<int, ISetView<TKey>> createArea,
			Func<TKey, TPriority> getPriority,
			Func<UniRx.IObservable<TKey>, UniRx.IObservable<TKey>> filterKeys = null)
			: base(interest, getPriority, filterKeys)
		{
			Require.That(createArea != null);

			this.createArea = createArea;
		}

		public int Radius
		{
			get { return this.radius; }

			set
			{
				Require.That(value >= 1);

				if (value == this.radius)
				{
					return;
				}

				this.radius = value;
				this.SetArea(this.createArea(value));
			}
		}
	}
}
