﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;

namespace Tilescape.Utility.Unity.Interests
{
	public abstract class AbstractVerticalInterestArea<TInterest> : AbstractUpdatableInterestArea3D<TInterest>
	{
		private int radialWidth = 0;

		private int radialHeight = 0;

		public AbstractVerticalInterestArea(TInterest interest)
			: base(interest, ObservableChunkIndex.Threshold.TwoChunks)
		{
		}

		public int RadialWidth
		{
			get { return this.radialWidth; }

			set
			{
				Require.That(value >= 1);

				if (value == this.radialWidth)
				{
					return;
				}

				this.radialWidth = value;
				this.UpdateArea();
			}
		}

		public int RadialHeight
		{
			get { return this.radialHeight; }

			set
			{
				Require.That(value >= 1);

				if (value == this.radialHeight)
				{
					return;
				}

				this.radialHeight = value;
				this.UpdateArea();
			}
		}

		public void IncreaseDimensionsBy(int amount) => this.SetDimensions(this.RadialWidth + amount, this.RadialHeight + amount);

		public void DecreaseDimensionsBy(int amount) =>
			this.SetDimensions((this.RadialWidth - amount).ClampLower(1), (this.RadialHeight - amount).ClampLower(1));

		public void SetDimensions(int radius) => this.SetDimensions(radius, radius);

		public void SetDimensions(int radialWidth, int radialHeight)
		{
			Require.That(radialWidth >= 1);
			Require.That(radialHeight >= 1);

			if (radialWidth == this.radialWidth && radialHeight == this.radialHeight)
			{
				return;
			}

			this.radialWidth = radialWidth;
			this.radialHeight = radialHeight;
			this.UpdateArea();
		}

		protected abstract void UpdateArea();
	}
}
