﻿using Tilescape.Utility.Indexing.Shapes;

namespace Tilescape.Utility.Unity.Interests
{
	public class InterestColumn<TInterest> : AbstractVerticalInterestArea<TInterest>
	{
		public InterestColumn(TInterest interest)
			: base(interest)
		{
		}

		protected override void UpdateArea() => this.SetArea(IndexShape.CreateColumn(this.RadialWidth * 2, this.RadialHeight * 2));
	}
}
