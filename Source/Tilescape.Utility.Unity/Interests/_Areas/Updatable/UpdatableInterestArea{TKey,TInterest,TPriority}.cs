﻿using System;
using Tilescape.Utility.Collections;

namespace Tilescape.Utility.Unity.Interests
{
	public class UpdatableInterestArea<TKey, TInterest, TPriority> : AbstractUpdatableInterestArea<TKey, TInterest, TPriority>
	{
		public UpdatableInterestArea(
			TInterest interest,
			Func<TKey, TPriority> getPriority,
			Func<UniRx.IObservable<TKey>, UniRx.IObservable<TKey>> filterKeys = null)
			: base(interest, getPriority, filterKeys)
		{
		}

		public new void SetArea(ISetView<TKey> area) => base.SetArea(area);
	}
}
