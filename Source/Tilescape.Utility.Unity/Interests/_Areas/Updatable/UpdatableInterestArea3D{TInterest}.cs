﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Unity.Interests
{
	public class UpdatableInterestArea3D<TInterest> : AbstractUpdatableInterestArea3D<TInterest>
	{
		public UpdatableInterestArea3D(TInterest interest, ObservableChunkIndex.Threshold threshold)
			: base(interest, threshold)
		{
		}

		public new void SetArea(ISetView<Index3D> area) => base.SetArea(area);
	}
}
