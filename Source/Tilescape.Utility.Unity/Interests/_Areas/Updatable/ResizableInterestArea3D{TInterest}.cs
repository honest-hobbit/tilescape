﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Indexing.Indices;

namespace Tilescape.Utility.Unity.Interests
{
	public class ResizableInterestArea3D<TInterest> : ResizableInterestArea<Index3D, TInterest, int>
	{
		public ResizableInterestArea3D(
			TInterest interest,
			Func<int, ISetView<Index3D>> createArea,
			ObservableChunkIndex.Threshold threshold = ObservableChunkIndex.Threshold.TwoChunks)
			: base(interest, createArea, InterestArea.WithPriorities.FromOffset, indices => indices.FilteredThreshold(threshold))
		{
		}
	}
}
