﻿using System;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public abstract class AbstractUpdatableInterestArea<TKey, TInterest, TPriority> :
		IInterestArea<TKey, TInterest, TPriority>, UniRx.IObserver<TKey>
	{
		private readonly ReplaySubject<TKey> originChanged = new ReplaySubject<TKey>(1);

		private readonly ReplaySubject<ISetView<TKey>> areaChanged = new ReplaySubject<ISetView<TKey>>(1);

		private readonly IInterestArea<TKey, TInterest, TPriority> areaOfInterest;

		public AbstractUpdatableInterestArea(
			TInterest interest,
			Func<TKey, TPriority> getPriority,
			Func<UniRx.IObservable<TKey>,
				UniRx.IObservable<TKey>> filterKeys = null)
		{
			Require.That(getPriority != null);

			filterKeys = filterKeys ?? Observable.DistinctUntilChanged;

			this.areaOfInterest = new InterestArea<TKey, TInterest, TPriority>(
				interest, getPriority, filterKeys(this.originChanged.AsObservable()), this.areaChanged.DistinctUntilChanged());
		}

		/// <inheritdoc />
		public TInterest Interest => this.areaOfInterest.Interest;

		/// <inheritdoc />
		public UniRx.IObservable<ISetView<TKey>> KeysChanged => this.areaOfInterest.KeysChanged;

		/// <inheritdoc />
		public UniRx.IObservable<IDictionaryView<TKey, TPriority>> PrioritiesChanged => this.areaOfInterest.PrioritiesChanged;

		public bool IsCompleted { get; private set; } = false;

		public void SetOrigin(TKey origin)
		{
			if (!this.IsCompleted)
			{
				this.originChanged.OnNext(origin);
			}
		}

		public void Complete()
		{
			if (this.IsCompleted)
			{
				return;
			}

			this.IsCompleted = true;
			this.originChanged.OnCompleted();
			this.areaChanged.OnCompleted();
		}

		/// <inheritdoc />
		void UniRx.IObserver<TKey>.OnNext(TKey value) => this.SetOrigin(value);

		/// <inheritdoc />
		void UniRx.IObserver<TKey>.OnError(Exception error)
		{
			if (!this.IsCompleted)
			{
				this.originChanged.OnError(error);
			}
		}

		/// <inheritdoc />
		void UniRx.IObserver<TKey>.OnCompleted() => this.Complete();

		protected void SetArea(ISetView<TKey> area)
		{
			Require.That(area != null);

			if (!this.IsCompleted)
			{
				this.areaChanged.OnNext(area);
			}
		}
	}
}
