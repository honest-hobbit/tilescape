﻿using Tilescape.Utility.Contracts;
using Tilescape.Utility.Enums;
using Tilescape.Utility.Indexing.Shapes;

namespace Tilescape.Utility.Unity.Interests
{
	public static class ResizableInterestArea
	{
		public static ResizableInterestArea3D<TInterest> CreateColumn<TInterest>(TInterest interest) =>
			new ResizableInterestArea3D<TInterest>(interest, radius => IndexShape.CreateColumn(radius * 2, radius * 2));

		public static ResizableInterestArea3D<TInterest> CreateCylinder<TInterest>(TInterest interest) =>
			new ResizableInterestArea3D<TInterest>(interest, radius => IndexShape.CreateCylinder(radius * 2, radius * 2));

		public static ResizableInterestArea3D<TInterest> CreateSphere<TInterest>(TInterest interest) =>
			new ResizableInterestArea3D<TInterest>(interest, radius => IndexShape.CreateSphere(radius * 2));

		public static ResizableInterestArea3D<TInterest> Create<TInterest>(TInterest interest, Shape3D shape)
		{
			Require.That(Enumeration.IsDefined(shape));

			switch (shape)
			{
				case Shape3D.Column: return CreateColumn(interest);
				case Shape3D.Cylinder: return CreateCylinder(interest);
				case Shape3D.Sphere: return CreateSphere(interest);
				default: throw InvalidEnumArgument.CreateException(nameof(shape), shape);
			}
		}
	}
}
