﻿using System.Collections.Generic;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Reactive;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public interface IInterestMapView<TKey, TInterest>
	{
		IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>> InterestsChanged { get; }

		Try<TInterest> this[TKey key] { get; }
	}
}
