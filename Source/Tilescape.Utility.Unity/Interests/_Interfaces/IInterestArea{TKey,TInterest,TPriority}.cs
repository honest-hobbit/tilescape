﻿using Tilescape.Utility.Collections;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public interface IInterestArea<TKey, TInterest, TPriority> : IInterestArea<TKey, TInterest>
	{
		IObservable<IDictionaryView<TKey, TPriority>> PrioritiesChanged { get; }
	}
}
