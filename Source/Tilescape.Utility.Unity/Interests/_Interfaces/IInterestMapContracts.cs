﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Unity.Interests
{
	public static class IInterestMapContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void AddInterest<TKey>(TKey key)
		{
			Require.That(key != null);
		}

		[Conditional(Require.CompilationSymbol)]
		public static void RemoveInterest<TKey>(TKey key)
		{
			Require.That(key != null);
		}
	}
}
