﻿using Tilescape.Utility.Collections;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public interface IInterestArea<TKey, TInterest>
	{
		TInterest Interest { get; }

		IObservable<ISetView<TKey>> KeysChanged { get; }
	}
}
