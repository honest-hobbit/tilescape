﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Unity.Interests
{
	public static class IInterestMapViewContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void Indexer<TKey>(TKey key)
		{
			Require.That(key != null);
		}
	}
}
