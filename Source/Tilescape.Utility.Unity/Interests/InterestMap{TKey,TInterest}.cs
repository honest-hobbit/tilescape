﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Reactive;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public class InterestMap<TKey, TInterest> : IInterestMap<TKey, TInterest>
	{
		private readonly Subject<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>> interestsChanged =
			new Subject<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>>();

		private readonly IInterestMerger<TInterest> merger;

		private readonly IDictionary<TKey, TInterest> map;

		public InterestMap(IInterestMerger<TInterest> merger, IEqualityComparer<TKey> comparer = null)
			: this(merger, new Dictionary<TKey, TInterest>(comparer))
		{
		}

		public InterestMap(IInterestMerger<TInterest> merger, IDictionary<TKey, TInterest> map)
		{
			Require.That(merger != null);
			Require.That(map != null);

			this.merger = merger;
			this.map = map;
			this.InterestsChanged = this.interestsChanged.AsObservable();
		}

		/// <inheritdoc />
		public UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>> InterestsChanged { get; }

		/// <inheritdoc />
		public Try<TInterest> this[TKey key]
		{
			get
			{
				IInterestMapViewContracts.Indexer(key);

				return this.map.TryGetValue(key);
			}
		}

		/// <inheritdoc />
		public void AddInterest(TKey key, TInterest interest)
		{
			IInterestMapContracts.AddInterest(key);

			Try<TInterest> previous = this[key];
			if (this.merger.TryAdd(previous, interest, out var next))
			{
				this.HandleInterestsChanged(key, previous, next);
			}
		}

		/// <inheritdoc />
		public void RemoveInterest(TKey key, TInterest interest)
		{
			IInterestMapContracts.RemoveInterest(key);

			Try<TInterest> previous = this[key];
			if (this.merger.TryRemove(previous, interest, out var next))
			{
				this.HandleInterestsChanged(key, previous, next);
			}
		}

		private void HandleInterestsChanged(TKey key, Try<TInterest> previous, Try<TInterest> next)
		{
			if (next.HasValue)
			{
				this.map[key] = next.Value;
			}
			else
			{
				this.map.Remove(key);
			}

			this.interestsChanged.OnNext(KeyValuePair.New(key, SequentialPair.New(previous, next)));
		}
	}
}
