﻿using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Reactive;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	/// <summary>
	/// Provides extension methods for <see cref="IInterestMapView{TKey, TInterest}.InterestsChanged"/>.
	/// </summary>
	public static class InterestsChangedExtensions
	{
		public static UniRx.IObservable<KeyValuePair<TKey, Status>> Activity<TKey, TInterests>(
			this UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
		{
			Require.That(interestsChanged != null);

			return interestsChanged.Opened().Select(opened => KeyValuePair.New(opened.Key, Status.Active))
				.Merge(interestsChanged.Closed().Select(closed => KeyValuePair.New(closed.Key, Status.Inactive)));
		}

		// KeyValuePair.Value is the first interest value
		public static UniRx.IObservable<KeyValuePair<TKey, TInterests>> Opened<TKey, TInterests>(
			this UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
		{
			Require.That(interestsChanged != null);

			return interestsChanged
				.Where(changed => !changed.Value.Previous.HasValue && changed.Value.Next.HasValue)
				.Select(changed => KeyValuePair.New(changed.Key, changed.Value.Next.Value));
		}

		// KeyValuePair.Value is the last interest value
		public static UniRx.IObservable<KeyValuePair<TKey, TInterests>> Closed<TKey, TInterests>(
			this UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
		{
			Require.That(interestsChanged != null);

			return interestsChanged
				.Where(changed => changed.Value.Previous.HasValue && !changed.Value.Next.HasValue)
				.Select(changed => KeyValuePair.New(changed.Key, changed.Value.Previous.Value));
		}

		public static UniRx.IObservable<KeyValuePair<TKey, SequentialPair<TInterests>>> HasBoth<TKey, TInterests>(
			this UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
		{
			Require.That(interestsChanged != null);

			return interestsChanged
				.Where(changed => changed.Value.Previous.HasValue && changed.Value.Next.HasValue)
				.Select(changed => KeyValuePair.New(
					changed.Key, SequentialPair.New(changed.Value.Previous.Value, changed.Value.Next.Value)));
		}

		public static UniRx.IObservable<KeyValuePair<TKey, TInterests>> HasPrevious<TKey, TInterests>(
			this UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
		{
			Require.That(interestsChanged != null);

			return interestsChanged
				.Where(changed => changed.Value.Previous.HasValue)
				.Select(changed => KeyValuePair.New(changed.Key, changed.Value.Previous.Value));
		}

		public static UniRx.IObservable<KeyValuePair<TKey, TInterests>> HasNext<TKey, TInterests>(
			this UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
		{
			Require.That(interestsChanged != null);

			return interestsChanged
				.Where(changed => changed.Value.Next.HasValue)
				.Select(changed => KeyValuePair.New(changed.Key, changed.Value.Next.Value));
		}

		public static UniRx.IObservable<KeyValuePair<TKey, Try<TInterests>>> SelectPrevious<TKey, TInterests>(
			this UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
		{
			Require.That(interestsChanged != null);

			return interestsChanged.Select(changed => KeyValuePair.New(changed.Key, changed.Value.Previous));
		}

		public static UniRx.IObservable<KeyValuePair<TKey, Try<TInterests>>> SelectNext<TKey, TInterests>(
			this UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
		{
			Require.That(interestsChanged != null);

			return interestsChanged.Select(changed => KeyValuePair.New(changed.Key, changed.Value.Next));
		}
	}
}
