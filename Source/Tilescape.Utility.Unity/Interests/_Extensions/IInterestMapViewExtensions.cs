﻿using Tilescape.Utility.Contracts;

namespace Tilescape.Utility.Unity.Interests
{
	/// <summary>
	/// Provides extension methods for <see cref="IInterestMapView{TKey, TInterest}"/>.
	/// </summary>
	public static class IInterestMapViewExtensions
	{
		public static bool HasAnyInterests<TKey, TInterest>(this IInterestMapView<TKey, TInterest> map, TKey key)
		{
			Require.That(map != null);

			return map[key].HasValue;
		}
	}
}
