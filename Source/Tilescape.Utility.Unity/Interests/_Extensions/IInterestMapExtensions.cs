﻿using System;
using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Reactive;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	/// <summary>
	/// Provides extension methods for <see cref="IInterestMap{TKey, TInterest}"/>.
	/// </summary>
	public static class IInterestMapExtensions
	{
		public static void AddInterests<TKey, TInterest>(
			this IInterestMap<TKey, TInterest> map, TInterest interest, IEnumerable<TKey> addTo)
		{
			Require.That(map != null);
			Require.That(addTo.AllAndSelfNotNull());

			foreach (var key in addTo)
			{
				map.AddInterest(key, interest);
			}
		}

		public static void RemoveInterests<TKey, TInterest>(
			this IInterestMap<TKey, TInterest> map, TInterest interest, IEnumerable<TKey> removeFrom)
		{
			Require.That(map != null);
			Require.That(removeFrom.AllAndSelfNotNull());

			foreach (var key in removeFrom)
			{
				map.RemoveInterest(key, interest);
			}
		}

		public static void UpdateInterests<TKey, TInterest>(
			this IInterestMap<TKey, TInterest> map, TInterest interest, ISetView<TKey> addTo, ISetView<TKey> removeFrom)
		{
			Require.That(map != null);
			Require.That(addTo.AllAndSelfNotNull());
			Require.That(removeFrom.AllAndSelfNotNull());

			if (addTo.Count != 0 && removeFrom.Count == 0)
			{
				map.AddInterests(interest, addTo);
				return;
			}

			if (addTo.Count == 0 && removeFrom.Count != 0)
			{
				map.RemoveInterests(interest, removeFrom);
				return;
			}

			foreach (var key in addTo)
			{
				if (!removeFrom.Contains(key))
				{
					map.AddInterest(key, interest);
				}
			}

			foreach (var key in removeFrom)
			{
				if (!addTo.Contains(key))
				{
					map.RemoveInterest(key, interest);
				}
			}
		}

		public static IDisposable SubscribeArea<TKey, TInterest>(
			this IInterestMap<TKey, TInterest> map, IInterestArea<TKey, TInterest> area)
		{
			Require.That(map != null);
			Require.That(area != null);

			return map.DoSubscribeArea(area, null);
		}

		public static IDisposable SubscribeArea<TKey, TInterest>(
			this IInterestMap<TKey, TInterest> map, IInterestArea<TKey, TInterest> area, UniRx.IObservable<Nothing> linkDisposalTo)
		{
			Require.That(map != null);
			Require.That(area != null);
			Require.That(linkDisposalTo != null);

			return map.DoSubscribeArea(area, linkDisposalTo);
		}

		public static IInterestMapView<TKey, TInterest> AsView<TKey, TInterest>(this IInterestMap<TKey, TInterest> map)
		{
			Require.That(map != null);

			return new InterestMapView<TKey, TInterest>(map);
		}

		private static IDisposable DoSubscribeArea<TKey, TInterest>(
			this IInterestMap<TKey, TInterest> map, IInterestArea<TKey, TInterest> area, UniRx.IObservable<Nothing> linkDisposalTo)
		{
			Require.That(map != null);
			Require.That(area != null);

			// isDisposed is used to make the observable complete when the subscription is disposed
			// thus causing the area of interest to be removed from the interest map
			var isDisposed = new SingleSignalObservable();
			var keysChanged = area.KeysChanged.TakeUntil(isDisposed);

			if (linkDisposalTo != null)
			{
				// linking disposal will cause the area of interest to auto unsubscribe when the linked disposal completes
				keysChanged = keysChanged.TakeUntil(linkDisposalTo);
			}

			keysChanged.PairLatest().Subscribe(keys => map.UpdateInterests(
				area.Interest,
				addTo: keys.Next.ToValueOrDefault(SetView.Empty<TKey>()),
				removeFrom: keys.Previous.ToValueOrDefault(SetView.Empty<TKey>())));

			return isDisposed;
		}
	}
}
