﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public class InterestMapPriorityManager<TKey, TInterest, TPriority>
	{
		private readonly Subject<IDictionaryView<TKey, TPriority>> prioritiesChanged = new Subject<IDictionaryView<TKey, TPriority>>();

		private readonly IInterestMap<TKey, TInterest> interestMap;

		public InterestMapPriorityManager(IInterestMap<TKey, TInterest> interestMap)
		{
			Require.That(interestMap != null);

			this.interestMap = interestMap;
			this.InterestMap = interestMap.AsView();
			this.PriorityChanged = this.prioritiesChanged
				.SelectMany(priorities => priorities.Select(pair => KeyValuePair.New(pair.Key, Try.Value(pair.Value))))
				.Merge(this.interestMap.InterestsChanged.Closed().Select(pair => KeyValuePair.New(pair.Key, Try.None<TPriority>())));
		}

		public UniRx.IObservable<KeyValuePair<TKey, Try<TPriority>>> PriorityChanged { get; }

		public IInterestMapView<TKey, TInterest> InterestMap { get; }

		public IDisposable SubscribeArea(IInterestArea<TKey, TInterest, TPriority> area)
		{
			Require.That(area != null);

			return DisposableFactory.Combine(
				area.PrioritiesChanged.Subscribe(this.prioritiesChanged),
				this.interestMap.SubscribeArea(area));
		}
	}
}
