﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using Tilescape.Utility.Unity.Reactive;
using UniRx;

namespace Tilescape.Utility.Unity.Interests
{
	public class InterestMapView<TKey, TInterest> : IInterestMapView<TKey, TInterest>
	{
		private readonly IInterestMap<TKey, TInterest> map;

		public InterestMapView(IInterestMap<TKey, TInterest> map)
		{
			Require.That(map != null);

			this.map = map;
		}

		/// <inheritdoc />
		public UniRx.IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>> InterestsChanged => this.map.InterestsChanged;

		/// <inheritdoc />
		public Try<TInterest> this[TKey key] => this.map[key];
	}
}
