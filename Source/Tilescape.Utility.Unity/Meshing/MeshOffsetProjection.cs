﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public class MeshOffsetProjection : IMeshProjection
	{
		private IMeshDefinition mesh;

		public MeshOffsetProjection()
		{
			this.Vertices = new VerticesList(this);
			this.Mesh = MeshDefinition.Empty;
		}

		/// <inheritdoc />
		public IListView<int> Triangles { get; private set; }

		/// <inheritdoc />
		public IListView<Vector3> Vertices { get; }

		/// <inheritdoc />
		public IListView<Vector3> Normals { get; private set; }

		/// <inheritdoc />
		public IListView<Vector4> Tangents { get; private set; }

		/// <inheritdoc />
		public IListView<Color32> Colors { get; private set; }

		/// <inheritdoc />
		public IListView<Vector2> UV { get; private set; }

		/// <inheritdoc />
		public IMeshDefinition Mesh
		{
			get => this.mesh;
			set
			{
				if (value == null)
				{
					value = MeshDefinition.Empty;
				}

				this.Triangles = value.Triangles;
				this.Normals = value.Normals;
				this.Tangents = value.Tangents;
				this.Colors = value.Colors;
				this.UV = value.UV;

				this.mesh = value;
			}
		}

		public Vector3 Position { get; set; }

		/// <inheritdoc />
		public Mesh BuildMesh(Mesh mesh, bool calculateBounds)
		{
			IMeshDefinitionContracts.BuildMesh(mesh);
			return this.DeepClone().BuildMesh(mesh, calculateBounds);
		}

		private class VerticesList : IListView<Vector3>
		{
			private readonly MeshOffsetProjection parent;

			public VerticesList(MeshOffsetProjection parent)
			{
				Require.That(parent != null);
				this.parent = parent;
			}

			/// <inheritdoc />
			public Vector3 this[int index]
			{
				get
				{
					IListViewContracts.Indexer(this, index);
					return this.parent.mesh.Vertices[index] + this.parent.Position;
				}
			}

			/// <inheritdoc />
			public int Count => this.parent.mesh.Vertices.Count;

			/// <inheritdoc />
			public IEnumerator<Vector3> GetEnumerator() =>
				this.parent.mesh.Vertices.Select(x => x + this.parent.Position).GetEnumerator();

			/// <inheritdoc />
			IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
		}
	}
}
