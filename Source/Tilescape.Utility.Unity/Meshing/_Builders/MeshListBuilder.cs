﻿using System.Collections.Generic;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public class MeshListBuilder : AbstractMeshBuilder
	{
		public MeshListBuilder()
			: base()
		{
		}

		public MeshListBuilder(int vertexCapacity)
			: base(vertexCapacity)
		{
		}

		public MeshListBuilder(int triangleCapacity, int vertexCapacity)
			: base(triangleCapacity, vertexCapacity)
		{
		}

		public new List<int> TrianglesList => base.TrianglesList;

		public new List<Vector3> VerticesList => base.VerticesList;

		public new List<Vector3> NormalsList => base.NormalsList;

		public new List<Vector4> TangentsList => base.TangentsList;

		public new List<Color32> ColorsList => base.ColorsList;

		public new List<Vector2> UVList => base.UVList;
	}
}
