﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public class MeshBuilder : AbstractMeshBuilder
	{
		private readonly int initialVertexCapacity;

		public MeshBuilder()
			: base()
		{
		}

		public MeshBuilder(int initialVertexCapacity)
			: base(initialVertexCapacity)
		{
		}

		public MeshBuilder(int initialTriangleCapacity, int initialVertexCapacity)
			: base(initialTriangleCapacity, 0)
		{
			// vertices and triangles are always required so set their initial capacities immediately
			// all other components are optional so only set their capacities when used
			this.VerticesList.Capacity = initialVertexCapacity;
			this.initialVertexCapacity = initialVertexCapacity;
		}

		public void SetTriangles(IEnumerable<int> triangles)
		{
			Require.That(triangles != null);

			// required component so initial capacity was already set
			this.TrianglesList.Clear();
			this.TrianglesList.AddMany(triangles);
		}

		public void SetVertices(IEnumerable<Vector3> vertices)
		{
			Require.That(vertices != null);
			Require.That(vertices.CountExtended() <= MeshConstants.MaxVertices);

			// required component so initial capacity was already set
			this.VerticesList.Clear();
			this.VerticesList.AddManyMaxCapacity(vertices, MeshConstants.MaxVertices);
		}

		public void SetNormals(IEnumerable<Vector3> normals)
		{
			Require.That(normals != null);
			Require.That(normals.CountExtended() <= MeshConstants.MaxVertices);

			this.NormalsList.Clear();
			this.CopyInOptional(this.NormalsList, normals);
		}

		public void SetTangents(IEnumerable<Vector4> tangents)
		{
			Require.That(tangents != null);
			Require.That(tangents.CountExtended() <= MeshConstants.MaxVertices);

			this.TangentsList.Clear();
			this.CopyInOptional(this.TangentsList, tangents);
		}

		public void SetColors(IEnumerable<Color32> colors)
		{
			Require.That(colors != null);
			Require.That(colors.CountExtended() <= MeshConstants.MaxVertices);

			this.ColorsList.Clear();
			this.CopyInOptional(this.ColorsList, colors);
		}

		public void SetUV(IEnumerable<Vector2> uv)
		{
			Require.That(uv != null);
			Require.That(uv.CountExtended() <= MeshConstants.MaxVertices);

			this.UVList.Clear();
			this.CopyInOptional(this.UVList, uv);
		}

		public void SetTo(Mesh mesh, OptionalMeshPart parts)
		{
			Require.That(mesh != null);

			this.Clear();
			mesh.GetTriangles(this.TrianglesList, 0);

			this.VerticesList.EnsureCapacity(mesh.vertexCount);
			mesh.GetVertices(this.VerticesList);

			if (parts.Has(OptionalMeshPart.Normals))
			{
				this.NormalsList.EnsureCapacity(mesh.vertexCount);
				mesh.GetNormals(this.NormalsList);
			}

			if (parts.Has(OptionalMeshPart.Tangents))
			{
				this.TangentsList.EnsureCapacity(mesh.vertexCount);
				mesh.GetTangents(this.TangentsList);
			}

			if (parts.Has(OptionalMeshPart.Colors))
			{
				this.ColorsList.EnsureCapacity(mesh.vertexCount);
				mesh.GetColors(this.ColorsList);
			}

			if (parts.Has(OptionalMeshPart.UV))
			{
				this.UVList.EnsureCapacity(mesh.vertexCount);
				mesh.GetUVs(0, this.UVList);
			}
		}

		public void SetTo(IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			this.Clear();
			if (!this.TryAdd(mesh))
			{
				throw new ArgumentException("Mesh is larger than the capacity of this builder.", nameof(mesh));
			}
		}

		public bool TryAdd(IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			if (mesh.IsEmpty())
			{
				return true;
			}

			if (this.Vertices.Count + mesh.Vertices.Count > MeshConstants.MaxVertices)
			{
				return false;
			}

			var previousVertexCount = this.Vertices.Count;
			this.TrianglesList.AddMany(mesh.Triangles.Select(x => x + previousVertexCount));
			this.VerticesList.AddManyMaxCapacity(mesh.Vertices, MeshConstants.MaxVertices);
			this.CopyInOptional(this.ColorsList, mesh.Colors);
			this.CopyInOptional(this.NormalsList, mesh.Normals);
			this.CopyInOptional(this.TangentsList, mesh.Tangents);
			this.CopyInOptional(this.UVList, mesh.UV);
			return true;
		}

		public void Add(IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			if (!this.TryAdd(mesh))
			{
				throw new InvalidOperationException(
					$"Unable to add mesh. Total number of vertices would be greater than {MeshConstants.MaxVertices}.");
			}
		}

		private void CopyInOptional<T>(List<T> current, IListView<T> add)
		{
			Require.That(current != null);
			Require.That(add != null);

			if (add.Count == 0)
			{
				return;
			}

			this.CopyInOptional(current, (IEnumerable<T>)add);
		}

		private void CopyInOptional<T>(List<T> current, IEnumerable<T> add)
		{
			Require.That(current != null);
			Require.That(add != null);

			current.EnsureCapacity(this.initialVertexCapacity);
			current.AddManyMaxCapacity(add, MeshConstants.MaxVertices);
		}
	}
}
