﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public abstract class AbstractMeshBuilder : IMeshDefinition
	{
		public AbstractMeshBuilder()
			: this(MeshConstants.MaxVertices)
		{
		}

		public AbstractMeshBuilder(int initialVertexCapacity)
			: this(initialVertexCapacity, initialVertexCapacity)
		{
		}

		public AbstractMeshBuilder(int initialTriangleCapacity, int initialVertexCapacity)
		{
			Require.That(initialTriangleCapacity >= 0);
			Require.That(initialVertexCapacity >= 0);
			Require.That(initialVertexCapacity <= MeshConstants.MaxVertices);

			this.TrianglesList = new List<int>(initialTriangleCapacity);
			this.VerticesList = new List<Vector3>(initialVertexCapacity);
			this.NormalsList = new List<Vector3>(initialVertexCapacity);
			this.TangentsList = new List<Vector4>(initialVertexCapacity);
			this.ColorsList = new List<Color32>(initialVertexCapacity);
			this.UVList = new List<Vector2>(initialVertexCapacity);

			this.Triangles = this.TrianglesList.AsListView();
			this.Vertices = this.VerticesList.AsListView();
			this.Normals = this.NormalsList.AsListView();
			this.Tangents = this.TangentsList.AsListView();
			this.Colors = this.ColorsList.AsListView();
			this.UV = this.UVList.AsListView();
		}

		/// <inheritdoc />
		public IListView<int> Triangles { get; }

		/// <inheritdoc />
		public IListView<Vector3> Vertices { get; }

		/// <inheritdoc />
		public IListView<Vector3> Normals { get; }

		/// <inheritdoc />
		public IListView<Vector4> Tangents { get; }

		/// <inheritdoc />
		public IListView<Color32> Colors { get; }

		/// <inheritdoc />
		public IListView<Vector2> UV { get; }

		protected List<int> TrianglesList { get; }

		protected List<Vector3> VerticesList { get; }

		protected List<Vector3> NormalsList { get; }

		protected List<Vector4> TangentsList { get; }

		protected List<Color32> ColorsList { get; }

		protected List<Vector2> UVList { get; }

		/// <inheritdoc />
		public Mesh BuildMesh(Mesh mesh, bool calculateBounds)
		{
			IMeshDefinitionContracts.BuildMesh(mesh);

			// vertices has to be assigned first
			mesh.SetVertices(this.VerticesList);

			// optional components
			if (this.ColorsList.Count > 0)
			{
				mesh.SetColors(this.ColorsList);
			}

			if (this.NormalsList.Count > 0)
			{
				mesh.SetNormals(this.NormalsList);
			}

			if (this.TangentsList.Count > 0)
			{
				mesh.SetTangents(this.TangentsList);
			}

			if (this.UVList.Count > 0)
			{
				mesh.SetUVs(0, this.UVList);
			}

			// triangles must be set last to avoid index out of bounds exceptions
			mesh.SetTriangles(this.TrianglesList, 0, calculateBounds);
			return mesh;
		}

		public virtual void Clear()
		{
			this.TrianglesList.Clear();
			this.VerticesList.Clear();
			this.NormalsList.Clear();
			this.TangentsList.Clear();
			this.ColorsList.Clear();
			this.UVList.Clear();
		}

		/// <inheritdoc />
		public override string ToString() => this.ToDebugString();
	}
}
