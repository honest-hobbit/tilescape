﻿using System.Collections.Generic;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public class MeshCompactor : AbstractMeshBuilder
	{
		private readonly Dictionary<int, int> oldToNewIndices = new Dictionary<int, int>();

		public MeshCompactor()
			: base()
		{
		}

		public MeshCompactor(int vertexCapacity)
			: base(vertexCapacity)
		{
		}

		public MeshCompactor(int triangleCapacity, int vertexCapacity)
			: base(triangleCapacity, vertexCapacity)
		{
		}

		/// <inheritdoc />
		public override void Clear()
		{
			this.oldToNewIndices.Clear();
			base.Clear();
		}

		public OptionalComponent AddTriangle(int a, int b, int c, Vector3 vertexA, Vector3 vertexB, Vector3 vertexC)
		{
			Require.That(
				this.IsValid(),
				"Must complete adding optional components of previous triangle before adding another triangle.");
			Require.That(a >= 0 && a <= MeshConstants.MaxVertices);
			Require.That(b >= 0 && b <= MeshConstants.MaxVertices);
			Require.That(c >= 0 && c <= MeshConstants.MaxVertices);
			Require.That(a != b && b != c);

			var isNewA = this.MapIndex(a, vertexA);
			var isNewB = this.MapIndex(b, vertexB);
			var isNewC = this.MapIndex(c, vertexC);
			return new OptionalComponent(this, isNewA, isNewB, isNewC);
		}

		// returns true if new mapping, false if index was already mapped
		private bool MapIndex(int index, Vector3 vertex)
		{
			var isNewMapping = !this.oldToNewIndices.TryGetValue(index, out var remappedIndex);
			if (isNewMapping)
			{
				remappedIndex = this.oldToNewIndices.Count;
				this.oldToNewIndices[index] = remappedIndex;
				this.VerticesList.Add(vertex);
			}

			this.TrianglesList.Add(remappedIndex);
			return isNewMapping;
		}

		public struct OptionalComponent
		{
			private readonly MeshCompactor compactor;

			private readonly bool isNewA;

			private readonly bool isNewB;

			private readonly bool isNewC;

			internal OptionalComponent(MeshCompactor compactor, bool isNewA, bool isNewB, bool isNewC)
			{
				Require.That(compactor != null);

				this.compactor = compactor;
				this.isNewA = isNewA;
				this.isNewB = isNewB;
				this.isNewC = isNewC;
			}

			public OptionalComponent SetNormals(Vector3 a, Vector3 b, Vector3 c)
			{
				this.Set(this.compactor.NormalsList, a, b, c);
				return this;
			}

			public OptionalComponent SetTangents(Vector4 a, Vector4 b, Vector4 c)
			{
				this.Set(this.compactor.TangentsList, a, b, c);
				return this;
			}

			public OptionalComponent SetColors(Color32 a, Color32 b, Color32 c)
			{
				this.Set(this.compactor.ColorsList, a, b, c);
				return this;
			}

			public OptionalComponent SetUV(Vector2 a, Vector2 b, Vector2 c)
			{
				this.Set(this.compactor.UVList, a, b, c);
				return this;
			}

			private void Set<T>(List<T> values, T a, T b, T c)
			{
				Require.That(values != null);
				Require.That(
					values.Count <= this.compactor.Vertices.Count,
					$"Optional component of type {typeof(T).Name} has already been set.");
				Require.That(values.Count + this.PendingVerticesCount() == this.compactor.Vertices.Count);

				if (this.isNewA)
				{
					values.Add(a);
				}

				if (this.isNewB)
				{
					values.Add(b);
				}

				if (this.isNewC)
				{
					values.Add(c);
				}
			}

			private int PendingVerticesCount()
			{
				int count = 0;
				count += this.isNewA ? 1 : 0;
				count += this.isNewB ? 1 : 0;
				count += this.isNewC ? 1 : 0;
				return count;
			}
		}
	}
}
