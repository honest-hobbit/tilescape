﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Pooling;

namespace Tilescape.Utility.Unity.Meshing
{
	public class MultiMeshBuilder : IListView<IMeshDefinition>
	{
		private readonly List<MeshBuilder> builders = new List<MeshBuilder>();

		private readonly Func<MeshBuilder> factory;

		private readonly IPool<MeshBuilder> pool = null;

		public MultiMeshBuilder()
			: this(() => new MeshBuilder())
		{
		}

		public MultiMeshBuilder(int vertexCapacity)
			: this(() => new MeshBuilder(vertexCapacity))
		{
			Require.That(vertexCapacity >= 0);
			Require.That(vertexCapacity <= MeshConstants.MaxVertices);
		}

		public MultiMeshBuilder(Func<MeshBuilder> factory)
		{
			Require.That(factory != null);

			this.factory = factory;
		}

		public MultiMeshBuilder(IPool<MeshBuilder> pool)
		{
			Require.That(pool != null);

			this.pool = pool;
			this.factory = pool.Take;
		}

		/// <inheritdoc />
		public int Count => this.builders.Count;

		/// <inheritdoc />
		public IMeshDefinition this[int index]
		{
			get
			{
				IListViewContracts.Indexer(this, index);

				return this.builders[index];
			}
		}

		public void AddMany(IListView<IMeshDefinition> meshes)
		{
			Require.That(meshes.AllAndSelfNotNull());

			for (int index = 0; index < meshes.Count; index++)
			{
				this.Add(meshes[index]);
			}
		}

		public void Add(IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			if (mesh.IsEmpty())
			{
				return;
			}

			for (int index = 0; index < this.builders.Count; index++)
			{
				if (this.builders[index].TryAdd(mesh))
				{
					return;
				}
			}

			var newBuilder = this.factory();
			newBuilder.SetTo(mesh);
			this.builders.Add(newBuilder);
		}

		public void Clear() => this.builders.ForEach(x => x.Clear());

		public void ReleaseBuilders()
		{
			if (this.pool != null)
			{
				this.builders.ForEach(x =>
				{
					x.Clear();
					this.pool.TryGive(x);
				});
			}
			else
			{
				this.Clear();
			}

			this.builders.Clear();
		}

		/// <inheritdoc />
		public IEnumerator<IMeshDefinition> GetEnumerator() => this.builders.Select(x => (IMeshDefinition)x).GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
