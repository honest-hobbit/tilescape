﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public class MeshDefinition : IMeshDefinition
	{
		private readonly int[] triangles;

		private readonly Vector3[] vertices;

		private readonly Vector3[] normals;

		private readonly Vector4[] tangents;

		private readonly Color32[] colors;

		private readonly Vector2[] uv;

		public MeshDefinition(
			int[] triangles,
			Vector3[] vertices,
			Vector3[] normals = null,
			Vector4[] tangents = null,
			Color32[] colors = null,
			Vector2[] uv = null)
		{
			Require.That(triangles != null);
			Require.That(vertices != null);
			Require.That(vertices.Length <= MeshConstants.MaxVertices);

			this.triangles = triangles;
			this.vertices = vertices;
			this.normals = normals ?? Factory.EmptyArray<Vector3>();
			this.tangents = tangents ?? Factory.EmptyArray<Vector4>();
			this.colors = colors ?? Factory.EmptyArray<Color32>();
			this.uv = uv ?? Factory.EmptyArray<Vector2>();

			this.Triangles = triangles.AsListView();
			this.Vertices = vertices.AsListView();
			this.Normals = GetListView(normals);
			this.Tangents = GetListView(tangents);
			this.Colors = GetListView(colors);
			this.UV = GetListView(uv);

			Require.That(this.IsValid());
		}

		public static IMeshDefinition Empty { get; } = new MeshDefinition(Factory.EmptyArray<int>(), Factory.EmptyArray<Vector3>());

		/// <inheritdoc />
		public IListView<int> Triangles { get; }

		/// <inheritdoc />
		public IListView<Vector3> Vertices { get; }

		/// <inheritdoc />
		public IListView<Vector3> Normals { get; }

		/// <inheritdoc />
		public IListView<Vector4> Tangents { get; }

		/// <inheritdoc />
		public IListView<Color32> Colors { get; }

		/// <inheritdoc />
		public IListView<Vector2> UV { get; }

		/// <inheritdoc />
		public Mesh BuildMesh(Mesh mesh, bool calculateBounds)
		{
			IMeshDefinitionContracts.BuildMesh(mesh);

			// vertices has to be assigned first
			mesh.vertices = this.vertices;

			// optional components
			if (this.normals.Length > 0)
			{
				mesh.normals = this.normals;
			}

			if (this.tangents.Length > 0)
			{
				mesh.tangents = this.tangents;
			}

			if (this.colors.Length > 0)
			{
				mesh.colors32 = this.colors;
			}

			if (this.uv.Length > 0)
			{
				mesh.uv = this.uv;
			}

			// triangles must be set last to avoid index out of bounds exceptions
			mesh.SetTriangles(this.triangles, 0, calculateBounds);
			return mesh;
		}

		/// <inheritdoc />
		public override string ToString() => this.ToDebugString();

		private static IListView<T> GetListView<T>(T[] component) => component?.AsListView() ?? ListView.Empty<T>();
	}
}
