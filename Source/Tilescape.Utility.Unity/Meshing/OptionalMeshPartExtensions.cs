﻿namespace Tilescape.Utility.Unity.Meshing
{
	public static class OptionalMeshPartExtensions
	{
		/// <summary>
		/// Determines whether the current flags value contains the specified flag or flags (all of them).
		/// This implementation is faster than the Enum.HasFlag method because it avoids boxing of values.
		/// </summary>
		/// <param name="current">The current value to check for containing flags.</param>
		/// <param name="flag">The flag or combined flags to check for.</param>
		/// <returns>True if the current value contains the flag(s); otherwise false.</returns>
		public static bool Has(this OptionalMeshPart current, OptionalMeshPart flag) => (current & flag) == flag;
	}
}
