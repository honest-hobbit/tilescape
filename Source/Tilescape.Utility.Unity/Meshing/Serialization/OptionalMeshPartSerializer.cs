﻿using Tilescape.Utility.Serialization;

namespace Tilescape.Utility.Unity.Meshing.Serialization
{
	public class OptionalMeshPartSerializer : AbstractCompositeUniformSerializer<OptionalMeshPart, byte>
	{
		private OptionalMeshPartSerializer()
			: base(Serializer.Byte)
		{
		}

		public static IUniformSerDes<OptionalMeshPart> Instance { get; } = new OptionalMeshPartSerializer();

		/// <inheritdoc />
		protected override OptionalMeshPart ComposeValue(byte value) => (OptionalMeshPart)value;

		/// <inheritdoc />
		protected override byte DecomposeValue(OptionalMeshPart value) => (byte)value;
	}
}
