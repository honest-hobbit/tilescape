﻿using System;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Serialization;
using Tilescape.Utility.Types;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing.Serialization
{
	public class MeshDefinitionSerializer : ISerDes<IMeshDefinition>
	{
		private static readonly IUniformSerDes<int> TriangleCountSerializer = Serializer.Int;

		private static readonly IUniformSerDes<int> VertexIndexSerializer = SerializeInt.AsUShort;

		private readonly IUniformSerDes<Vector3> vertexSerializer;

		private readonly IUniformSerDes<Vector3> normalSerializer;

		private readonly IUniformSerDes<Vector4> tangentSerializer;

		private readonly IUniformSerDes<Color32> colorSerializer;

		private readonly IUniformSerDes<Vector2> uvSerializer;

		public MeshDefinitionSerializer(
			IUniformSerDes<Vector3> vertexSerializer,
			IUniformSerDes<Vector3> normalSerializer = null,
			IUniformSerDes<Vector4> tangentSerializer = null,
			IUniformSerDes<Color32> colorSerializer = null,
			IUniformSerDes<Vector2> uvSerializer = null)
		{
			Require.That(vertexSerializer != null);

			this.vertexSerializer = vertexSerializer;
			this.normalSerializer = normalSerializer;
			this.tangentSerializer = tangentSerializer;
			this.colorSerializer = colorSerializer;
			this.uvSerializer = uvSerializer;

			var parts = OptionalMeshPart.None;
			parts |= normalSerializer != null ? OptionalMeshPart.Normals : OptionalMeshPart.None;
			parts |= tangentSerializer != null ? OptionalMeshPart.Tangents : OptionalMeshPart.None;
			parts |= colorSerializer != null ? OptionalMeshPart.Colors : OptionalMeshPart.None;
			parts |= uvSerializer != null ? OptionalMeshPart.UV : OptionalMeshPart.None;
			this.RequiredParts = parts;
		}

		public OptionalMeshPart RequiredParts { get; }

		/// <inheritdoc />
		public int GetSerializedLength(IMeshDefinition mesh)
		{
			ISerializerContracts.GetSerializedLength(mesh);
			Require.That(mesh.IsEmpty() || mesh.OptionalParts() == this.RequiredParts);

			// triangle and vertex counts
			int serializedLength = TriangleCountSerializer.SerializedLength;
			serializedLength += VertexIndexSerializer.SerializedLength;

			// triangles
			serializedLength += VertexIndexSerializer.SerializedLength * mesh.Triangles.Count;

			// vertices
			int vertexLength = this.vertexSerializer.SerializedLength;
			vertexLength += this.normalSerializer?.SerializedLength ?? 0;
			vertexLength += this.tangentSerializer?.SerializedLength ?? 0;
			vertexLength += this.colorSerializer?.SerializedLength ?? 0;
			vertexLength += this.uvSerializer?.SerializedLength ?? 0;

			serializedLength += vertexLength * mesh.Vertices.Count;
			return serializedLength;
		}

		/// <inheritdoc />
		public int Serialize(IMeshDefinition mesh, byte[] buffer, ref int index)
		{
			ISerializerContracts.Serialize(this, mesh, buffer, index);
			Require.That(mesh.IsEmpty() || mesh.OptionalParts() == this.RequiredParts);

			int serializedLength = TriangleCountSerializer.Serialize(mesh.Triangles.Count, buffer, ref index);
			serializedLength += VertexIndexSerializer.Serialize(mesh.Vertices.Count, buffer, ref index);

			serializedLength += VertexIndexSerializer.SerializeMany(mesh.Triangles, buffer, ref index);
			serializedLength += this.vertexSerializer.SerializeMany(mesh.Vertices, buffer, ref index);

			serializedLength += this.normalSerializer?.SerializeMany(mesh.Normals, buffer, ref index) ?? 0;
			serializedLength += this.tangentSerializer?.SerializeMany(mesh.Tangents, buffer, ref index) ?? 0;
			serializedLength += this.colorSerializer?.SerializeMany(mesh.Colors, buffer, ref index) ?? 0;
			serializedLength += this.uvSerializer?.SerializeMany(mesh.UV, buffer, ref index) ?? 0;
			return serializedLength;
		}

		/// <inheritdoc />
		public int Serialize(IMeshDefinition mesh, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(mesh, writeByte);
			Require.That(mesh.IsEmpty() || mesh.OptionalParts() == this.RequiredParts);

			int serializedLength = TriangleCountSerializer.Serialize(mesh.Triangles.Count, writeByte);
			serializedLength += VertexIndexSerializer.Serialize(mesh.Vertices.Count, writeByte);

			serializedLength += VertexIndexSerializer.SerializeMany(mesh.Triangles, writeByte);
			serializedLength += this.vertexSerializer.SerializeMany(mesh.Vertices, writeByte);

			serializedLength += this.normalSerializer?.SerializeMany(mesh.Normals, writeByte) ?? 0;
			serializedLength += this.tangentSerializer?.SerializeMany(mesh.Tangents, writeByte) ?? 0;
			serializedLength += this.colorSerializer?.SerializeMany(mesh.Colors, writeByte) ?? 0;
			serializedLength += this.uvSerializer?.SerializeMany(mesh.UV, writeByte) ?? 0;
			return serializedLength;
		}

		/// <inheritdoc />
		public IMeshDefinition Deserialize(byte[] buffer, ref int index)
		{
			IDeserializerContracts.Deserialize(buffer, index);

			int triangleCount = TriangleCountSerializer.Deserialize(buffer, ref index);
			int verticesCount = VertexIndexSerializer.Deserialize(buffer, ref index);

			var triangles = VertexIndexSerializer.DeserializeMany(triangleCount, buffer, ref index);
			var vertices = this.vertexSerializer.DeserializeMany(verticesCount, buffer, ref index);

			var normals = this.normalSerializer?.DeserializeMany(verticesCount, buffer, ref index);
			var tangents = this.tangentSerializer?.DeserializeMany(verticesCount, buffer, ref index);
			var colors = this.colorSerializer?.DeserializeMany(verticesCount, buffer, ref index);
			var uv = this.uvSerializer?.DeserializeMany(verticesCount, buffer, ref index);

			return new MeshDefinition(triangles, vertices, normals, tangents, colors, uv);
		}

		/// <inheritdoc />
		public IMeshDefinition Deserialize(Func<Try<byte>> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			int triangleCount = TriangleCountSerializer.Deserialize(readByte);
			int verticesCount = VertexIndexSerializer.Deserialize(readByte);

			var triangles = VertexIndexSerializer.DeserializeMany(triangleCount, readByte);
			var vertices = this.vertexSerializer.DeserializeMany(verticesCount, readByte);

			var normals = this.normalSerializer?.DeserializeMany(verticesCount, readByte);
			var tangents = this.tangentSerializer?.DeserializeMany(verticesCount, readByte);
			var colors = this.colorSerializer?.DeserializeMany(verticesCount, readByte);
			var uv = this.uvSerializer?.DeserializeMany(verticesCount, readByte);

			return new MeshDefinition(triangles, vertices, normals, tangents, colors, uv);
		}
	}
}
