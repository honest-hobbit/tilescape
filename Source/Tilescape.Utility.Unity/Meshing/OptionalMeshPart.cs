﻿using System;

namespace Tilescape.Utility.Unity.Meshing
{
	[Flags]
	public enum OptionalMeshPart
	{
		None = 0,

		Normals = 1,

		Tangents = 1 << 1,

		Colors = 1 << 2,

		UV = 1 << 3
	}
}
