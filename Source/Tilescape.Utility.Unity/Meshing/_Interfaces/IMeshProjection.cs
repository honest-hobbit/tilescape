﻿namespace Tilescape.Utility.Unity.Meshing
{
	public interface IMeshProjection : IMeshDefinition
	{
		IMeshDefinition Mesh { get; set; }
	}
}
