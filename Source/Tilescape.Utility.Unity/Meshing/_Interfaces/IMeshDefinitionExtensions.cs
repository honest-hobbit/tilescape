﻿using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Mathematics;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public static class IMeshDefinitionExtensions
	{
		public static string ToDebugString(this IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			return $"Indices: {mesh.Triangles.Count} Vertices: {mesh.Vertices.Count} Parts: {mesh.OptionalParts()}";
		}

		public static IMeshDefinition DeepClone(this IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			return new MeshDefinition(
				mesh.Triangles.ToArrayExtended(),
				mesh.Vertices.ToArrayExtended(),
				mesh.Normals.CloneOptional(),
				mesh.Tangents.CloneOptional(),
				mesh.Colors.CloneOptional(),
				mesh.UV.CloneOptional());
		}

		public static Mesh BuildMesh(this IMeshDefinition definition, Mesh mesh)
		{
			Require.That(definition != null);
			Require.That(mesh != null);

			return definition.BuildMesh(mesh, calculateBounds: true);
		}

		public static Mesh BuildMesh(this IMeshDefinition definition, bool calculateBounds)
		{
			Require.That(definition != null);

			return definition.BuildMesh(new Mesh(), calculateBounds);
		}

		public static Mesh BuildMesh(this IMeshDefinition definition)
		{
			Require.That(definition != null);

			return definition.BuildMesh(new Mesh(), calculateBounds: true);
		}

		public static OptionalMeshPart OptionalParts(this IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			OptionalMeshPart parts = OptionalMeshPart.None;
			if (mesh.Normals.Count > 0)
			{
				parts |= OptionalMeshPart.Normals;
			}

			if (mesh.Tangents.Count > 0)
			{
				parts |= OptionalMeshPart.Tangents;
			}

			if (mesh.Colors.Count > 0)
			{
				parts |= OptionalMeshPart.Colors;
			}

			if (mesh.UV.Count > 0)
			{
				parts |= OptionalMeshPart.UV;
			}

			return parts;
		}

		public static bool IsEmpty(this IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			return mesh.Triangles.Count == 0;
		}

		public static bool IsValid(this IMeshDefinition mesh)
		{
			Require.That(mesh != null);

			return mesh.Triangles.Count.IsDivisibleBy(3)
				&& mesh.Vertices.Count <= MeshConstants.MaxVertices
				&& mesh.IsOptionalComponentValid(mesh.Normals)
				&& mesh.IsOptionalComponentValid(mesh.Tangents)
				&& mesh.IsOptionalComponentValid(mesh.Colors)
				&& mesh.IsOptionalComponentValid(mesh.UV);
		}

		private static bool IsOptionalComponentValid<T>(this IMeshDefinition mesh, IListView<T> component)
		{
			Require.That(mesh != null);
			Require.That(component != null);

			return component.Count == 0 || component.Count == mesh.Vertices.Count;
		}

		private static T[] CloneOptional<T>(this IListView<T> component)
		{
			Require.That(component != null);

			return component.Count == 0 ? Factory.EmptyArray<T>() : component.ToArrayExtended();
		}
	}
}
