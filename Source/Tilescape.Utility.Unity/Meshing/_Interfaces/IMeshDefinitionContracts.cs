﻿using System.Diagnostics;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public static class IMeshDefinitionContracts
	{
		[Conditional(Require.CompilationSymbol)]
		public static void BuildMesh(Mesh mesh)
		{
			Require.That(mesh != null);
		}
	}
}
