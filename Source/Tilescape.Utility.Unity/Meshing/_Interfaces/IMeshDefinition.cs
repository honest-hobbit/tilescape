﻿using Tilescape.Utility.Collections;
using UnityEngine;

namespace Tilescape.Utility.Unity.Meshing
{
	public interface IMeshDefinition
	{
		IListView<int> Triangles { get; }

		IListView<Vector3> Vertices { get; }

		IListView<Vector3> Normals { get; }

		IListView<Vector4> Tangents { get; }

		IListView<Color32> Colors { get; }

		IListView<Vector2> UV { get; }

		// only call this from Unity's main thread
		Mesh BuildMesh(Mesh mesh, bool calculateBounds);
	}
}
