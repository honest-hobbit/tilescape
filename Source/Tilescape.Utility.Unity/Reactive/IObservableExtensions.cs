﻿using System.Collections.Generic;
using Tilescape.Utility.Collections;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Types;
using UniRx;

namespace Tilescape.Utility.Unity.Reactive
{
	/// <summary>
	/// Provides extension methods for <see cref="IObservable{T}"/>.
	/// </summary>
	public static class IObservableExtensions
	{
		public static UniRx.IObservable<SequentialPair<Try<T>>> PairLatest<T>(this UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			var pairs = source.Scan(
				SequentialPair.New(Try.None<T>(), Try.None<T>()),
				(accumulator, current) => SequentialPair.New(accumulator.Next, Try.Value(current)));
			return pairs.Merge(pairs.TakeLast(1).Select(final => SequentialPair.New(final.Next, Try.None<T>())));
		}

		#region Ignore methods

		public static UniRx.IObservable<T> IgnoreError<T>(this UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			return Observable.Create<T>(observer => source.Subscribe(observer.OnNext, observer.OnCompleted));
		}

		public static UniRx.IObservable<T> IgnoreCompletion<T>(this UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			return Observable.Create<T>(observer => source.Subscribe(observer.OnNext, observer.OnError));
		}

		public static UniRx.IObservable<T> IgnoreTermination<T>(this UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			return Observable.Create<T>(observer => source.Subscribe(observer.OnNext));
		}

		#endregion

		#region Only methods

		public static UniRx.IObservable<T> OnlyError<T>(this UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			return Observable.Create<T>(observer => source.Subscribe(value => { }, observer.OnError));
		}

		public static UniRx.IObservable<T> OnlyCompletion<T>(this UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			return Observable.Create<T>(observer => source.Subscribe(value => { }, observer.OnCompleted));
		}

		// equivalent to IgnoreElements
		public static UniRx.IObservable<T> OnlyTermination<T>(this UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			return source.IgnoreElements();
		}

		// equivalent to IgnoreTermination
		public static UniRx.IObservable<T> OnlyElements<T>(this UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			return source.IgnoreTermination();
		}

		#endregion

		#region WhereSelect Try methods (equivalent methods for IEnumerable too in other project)

		public static UniRx.IObservable<T> WhereHasValueSelect<T>(this UniRx.IObservable<Try<T>> values)
		{
			Require.That(values != null);

			return values.Where(x => x.HasValue).Select(x => x.Value);
		}

		public static UniRx.IObservable<KeyValuePair<TKey, TValue>> WhereHasValueSelectPair<TKey, TValue>(
			this UniRx.IObservable<KeyValuePair<TKey, Try<TValue>>> values)
		{
			Require.That(values != null);

			return values.Where(x => x.Value.HasValue).Select(x => KeyValuePair.New(x.Key, x.Value.Value));
		}

		public static UniRx.IObservable<TKey> WhereHasValueSelectKey<TKey, TValue>(this UniRx.IObservable<KeyValuePair<TKey, Try<TValue>>> values)
		{
			Require.That(values != null);

			return values.Where(x => x.Value.HasValue).Select(x => x.Key);
		}

		public static UniRx.IObservable<TKey> WhereNoValueSelectKey<TKey, TValue>(this UniRx.IObservable<KeyValuePair<TKey, Try<TValue>>> values)
		{
			Require.That(values != null);

			return values.Where(x => !x.Value.HasValue).Select(x => x.Key);
		}

		#endregion
	}
}
