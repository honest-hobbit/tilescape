﻿using System;
using Tilescape.Utility.Disposables;
using Tilescape.Utility.Types;
using UniRx;

namespace Tilescape.Utility.Unity.Reactive
{
	public class SingleSignalObservable : AbstractDisposable, UniRx.IObservable<Nothing>
	{
		private readonly Subject<Nothing> isSignaled = new Subject<Nothing>();

		/// <inheritdoc />
		public IDisposable Subscribe(UniRx.IObserver<Nothing> observer) => this.isSignaled.Subscribe(observer);

		/// <inheritdoc />
		protected override void ManagedDisposal()
		{
			this.isSignaled.OnNext(Nothing.Default);
			this.isSignaled.OnCompleted();
		}
	}
}
