﻿using System;
using Tilescape.Utility.Concurrency;
using Tilescape.Utility.Contracts;
using Tilescape.Utility.Disposables;
using UniRx;

namespace Tilescape.Utility.Unity.Reactive
{
	public class SubscriptionCounter<T>
	{
		private readonly AtomicInt count = new AtomicInt(0);

		public SubscriptionCounter(UniRx.IObservable<T> source)
		{
			Require.That(source != null);

			this.CountedSource = Observable.Create<T>(observer =>
			{
				this.count.Increment();
				return new Subscription(source.Subscribe(observer), this.count);
			});
		}

		public int SubscriberCount => this.count.Read();

		public UniRx.IObservable<T> CountedSource { get; }

		private class Subscription : AbstractDisposable
		{
			private readonly IDisposable subscription;

			private readonly AtomicInt count;

			public Subscription(IDisposable subscription, AtomicInt count)
			{
				Require.That(subscription != null);
				Require.That(count != null);

				this.subscription = subscription;
				this.count = count;
			}

			/// <inheritdoc />
			protected override void ManagedDisposal()
			{
				this.subscription.Dispose();
				this.count.Decrement();
			}
		}
	}
}
