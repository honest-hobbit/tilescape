﻿using System;
using Tilescape.Utility.Mathematics;
using Tilescape.Utility.Types;

namespace Tilescape.Utility.Unity.Reactive
{
	/// <summary>
	/// Represents a previous-next pair of values from a sequence.
	/// </summary>
	/// <typeparam name="T">The type of the values.</typeparam>
	public struct SequentialPair<T> : IEquatable<SequentialPair<T>>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SequentialPair{T}"/> struct.
		/// </summary>
		/// <param name="previous">The previous value.</param>
		/// <param name="next">The next value.</param>
		public SequentialPair(T previous, T next)
		{
			this.Previous = previous;
			this.Next = next;
		}

		/// <summary>
		/// Gets the previous value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public T Previous { get; }

		/// <summary>
		/// Gets the next value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public T Next { get; }

		public static bool operator ==(SequentialPair<T> lhs, SequentialPair<T> rhs) => lhs.Equals(rhs);

		public static bool operator !=(SequentialPair<T> lhs, SequentialPair<T> rhs) => !lhs.Equals(rhs);

		/// <inheritdoc />
		public bool Equals(SequentialPair<T> other) =>
			this.Previous.EqualsNullSafe(other.Previous) && this.Next.EqualsNullSafe(other.Next);

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public override int GetHashCode() => HashCode.Start(this.Previous).And(this.Next);

		/// <inheritdoc />
		public override string ToString() => $"({this.Previous.ToStringNullSafe()}, {this.Next.ToStringNullSafe()})";
	}
}
