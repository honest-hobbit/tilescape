﻿using System;
using System.Threading;
using Tilescape.Utility.Contracts;
using UnityEngine;

namespace Tilescape.Utility.Unity.Concurrency
{
	public static class UnityThreadPool
	{
		public static bool QueueUserWorkItem(Action callBack) => QueueUserWorkItem(callBack, e => Debug.LogError(e));

		public static bool QueueUserWorkItem(Action callBack, Action<Exception> onError)
		{
			Require.That(callBack != null);
			Require.That(onError != null);

			return ThreadPool.QueueUserWorkItem(
				callbackState =>
				{
					try
					{
						callBack();
					}
					catch (Exception e)
					{
						onError(e);
					}
				});
		}

		public static bool QueueUserWorkItem(WaitCallback callBack) => QueueUserWorkItem(callBack, e => Debug.LogError(e));

		public static bool QueueUserWorkItem(WaitCallback callBack, Action<Exception> onError) =>
			QueueUserWorkItem(callBack, null, onError);

		public static bool QueueUserWorkItem(WaitCallback callBack, object state) =>
			QueueUserWorkItem(callBack, state, e => Debug.LogError(e));

		public static bool QueueUserWorkItem(WaitCallback callBack, object state, Action<Exception> onError)
		{
			Require.That(callBack != null);
			Require.That(onError != null);

			return ThreadPool.QueueUserWorkItem(
				callbackState =>
				{
					try
					{
						callBack(callbackState);
					}
					catch (Exception e)
					{
						onError(e);
					}
				}, state);
		}
	}
}
