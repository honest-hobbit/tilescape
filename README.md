Tilescape Engine for Unity3D
============================
An open source hybrid tile/voxel engine using C# and the Unity3D game engine.
Still in the early stages of development and no longer being actively worked on.
Released under the MIT License.

The game environment is composed of 3D tiles that are themselves made up of voxels.
This combination of 3D tiles made up of voxels is essentially the modern 3D equivalent
of how retro pixel games used 2D tiles made up of pixels for efficient storage and rendering
and enables the use of voxels in a much more compact and efficient form while imposing
some artistic limitations.