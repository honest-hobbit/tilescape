call :joinpath %2 "Unity\TilescapeDevelopment\Assets\Plugins\Build\\"

robocopy %1 "%Result%" *.dll *.pdb *.xml /XF UnityEngine.* UnityEditor.* /NFL /NDL /NJH /NJS /nc /ns /np
set rce=%errorlevel%
if %rce%==1 exit 0
if %rce%==2 exit 0
if %rce%==3 exit 0
if %rce%==4 exit 0
if %rce%==5 exit 0
if %rce%==6 exit 0
if %rce%==7 exit 0
exit %rce%

goto :eof

:joinpath
set Path1=%~1
set Path2=%~2
if {%Path1:~-1,1%}=={\} (set Result=%Path1%%Path2%) else (set Result=%Path1%\%Path2%)
goto :eof